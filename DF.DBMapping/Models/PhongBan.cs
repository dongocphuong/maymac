using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class PhongBan
    {
        public System.Guid PhongBanID { get; set; }
        public string TenPhong { get; set; }
        public bool IsActive { get; set; }
    }
}
