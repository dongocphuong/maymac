using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class CongDoanThanhPham
    {
        public System.Guid CongDoanThanhPhamID { get; set; }
        public System.Guid CongDoanHangMucID { get; set; }
        public System.Guid ThanhPhamID { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public bool IsActive { get; set; }
    }
}
