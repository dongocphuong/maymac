using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChiTietPhieuNhanDonHang
    {
        public System.Guid ChiTietNhapDonHangID { get; set; }
        public Nullable<System.Guid> DonHangID { get; set; }
        public System.Guid SanPhamID { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public Nullable<int> TinhTrang { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
        public System.Guid PhieuNhanDonHang { get; set; }
    }
}
