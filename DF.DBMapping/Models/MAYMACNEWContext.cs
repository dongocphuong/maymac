using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DF.DBMapping.Models.Mapping;

namespace DF.DBMapping.Models
{
    public partial class MAYMACNEWContext : DbContext
    {
        static MAYMACNEWContext()
        {
            Database.SetInitializer<MAYMACNEWContext>(null);
        }

        public MAYMACNEWContext()
            : base("Name=MAYMACNEWContext")
        {
        }

        public DbSet<BangXacNhanMau> BangXacNhanMaus { get; set; }
        public DbSet<BinhLuanDonHang> BinhLuanDonHangs { get; set; }
        public DbSet<ChamCong> ChamCongs { get; set; }
        public DbSet<ChamCongSanPham> ChamCongSanPhams { get; set; }
        public DbSet<ChiTietDeNghiNguyenLieu> ChiTietDeNghiNguyenLieux { get; set; }
        public DbSet<ChiTietDonHang> ChiTietDonHangs { get; set; }
        public DbSet<ChiTietDonHangKhachHangDat> ChiTietDonHangKhachHangDats { get; set; }
        public DbSet<ChiTietMauMay> ChiTietMauMays { get; set; }
        public DbSet<ChiTietPhieuNhanDonHang> ChiTietPhieuNhanDonHangs { get; set; }
        public DbSet<ChiTietPhieuNhapNguyenLieu> ChiTietPhieuNhapNguyenLieux { get; set; }
        public DbSet<ChiTietPhieuNhapSanPham> ChiTietPhieuNhapSanPhams { get; set; }
        public DbSet<ChiTietPhieuNhapThanhPham> ChiTietPhieuNhapThanhPhams { get; set; }
        public DbSet<ChiTietPhieuXuatNguyenLieu> ChiTietPhieuXuatNguyenLieux { get; set; }
        public DbSet<ChiTietPhieuXuatSanPham> ChiTietPhieuXuatSanPhams { get; set; }
        public DbSet<ChiTietPhieuXuatThanhPham> ChiTietPhieuXuatThanhPhams { get; set; }
        public DbSet<ChiTietYeuCauKhachHang> ChiTietYeuCauKhachHangs { get; set; }
        public DbSet<ChucVu> ChucVus { get; set; }
        public DbSet<CongDoanHangMuc> CongDoanHangMucs { get; set; }
        public DbSet<CongDoanSanPhamDinhNghia> CongDoanSanPhamDinhNghias { get; set; }
        public DbSet<CongDoanThanhPham> CongDoanThanhPhams { get; set; }
        public DbSet<CongDoanThanhPhamDinhNghia> CongDoanThanhPhamDinhNghias { get; set; }
        public DbSet<DeNghiNguyenLieu> DeNghiNguyenLieux { get; set; }
        public DbSet<DinhNghiaSanPham> DinhNghiaSanPhams { get; set; }
        public DbSet<DinhNghiaThanhPham> DinhNghiaThanhPhams { get; set; }
        public DbSet<DMCongDoanHangMuc> DMCongDoanHangMucs { get; set; }
        public DbSet<DMCongDoanThanhPham> DMCongDoanThanhPhams { get; set; }
        public DbSet<DMHangMucSanXuat> DMHangMucSanXuats { get; set; }
        public DbSet<DoiTac> DoiTacs { get; set; }
        public DbSet<DonHang> DonHangs { get; set; }
        public DbSet<DonVi> DonVis { get; set; }
        public DbSet<GiaTriThuocTinhSanPham> GiaTriThuocTinhSanPhams { get; set; }
        public DbSet<HangMucSanXuat> HangMucSanXuats { get; set; }
        public DbSet<HinhAnhDonHang> HinhAnhDonHangs { get; set; }
        public DbSet<HinhAnhVanBan> HinhAnhVanBans { get; set; }
        public DbSet<LichSuSuaChiTietYeuCau> LichSuSuaChiTietYeuCaus { get; set; }
        public DbSet<MauMay> MauMays { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<NganHang> NganHangs { get; set; }
        public DbSet<NguyenLieu> NguyenLieux { get; set; }
        public DbSet<NguyenLieuChiTietDonHangKhachHangDat> NguyenLieuChiTietDonHangKhachHangDats { get; set; }
        public DbSet<NguyenLieuYeuCauKhachHang> NguyenLieuYeuCauKhachHangs { get; set; }
        public DbSet<NhanVien> NhanViens { get; set; }
        public DbSet<NhanVienPhongBan> NhanVienPhongBans { get; set; }
        public DbSet<NhanVienPhongChat> NhanVienPhongChats { get; set; }
        public DbSet<NhomNguyenLieu> NhomNguyenLieux { get; set; }
        public DbSet<PhieuBaoGiaDonHang> PhieuBaoGiaDonHangs { get; set; }
        public DbSet<PhieuNhanDonHang> PhieuNhanDonHangs { get; set; }
        public DbSet<PhieuNhapCongDoanThanhPham> PhieuNhapCongDoanThanhPhams { get; set; }
        public DbSet<PhieuNhapNguyenLieu> PhieuNhapNguyenLieux { get; set; }
        public DbSet<PhieuNhapSanPham> PhieuNhapSanPhams { get; set; }
        public DbSet<PhieuNhapThanhPham> PhieuNhapThanhPhams { get; set; }
        public DbSet<PhieuTaoDonHang> PhieuTaoDonHangs { get; set; }
        public DbSet<PhieuXuatDonHang> PhieuXuatDonHangs { get; set; }
        public DbSet<PhieuXuatNguyenLieu> PhieuXuatNguyenLieux { get; set; }
        public DbSet<PhieuXuatSanPham> PhieuXuatSanPhams { get; set; }
        public DbSet<PhieuXuatThanhPham> PhieuXuatThanhPhams { get; set; }
        public DbSet<PhongBan> PhongBans { get; set; }
        public DbSet<QuanLySanXuat> QuanLySanXuats { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<SanPham> SanPhams { get; set; }
        public DbSet<SanPhamCha> SanPhamChas { get; set; }
        public DbSet<tblGroup> tblGroups { get; set; }
        public DbSet<tblGroupUser> tblGroupUsers { get; set; }
        public DbSet<tblLanguage> tblLanguages { get; set; }
        public DbSet<ThanhPham> ThanhPhams { get; set; }
        public DbSet<ThanhToanTien> ThanhToanTiens { get; set; }
        public DbSet<ThongSoSize> ThongSoSizes { get; set; }
        public DbSet<ThuocTinhSanPham> ThuocTinhSanPhams { get; set; }
        public DbSet<TrangThaiPhongChatMessage> TrangThaiPhongChatMessages { get; set; }
        public DbSet<UpdateManager> UpdateManagers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<VanBan> VanBans { get; set; }
        public DbSet<VanBanDinhKem> VanBanDinhKems { get; set; }
        public DbSet<YeuCauKhachHang> YeuCauKhachHangs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BangXacNhanMauMap());
            modelBuilder.Configurations.Add(new BinhLuanDonHangMap());
            modelBuilder.Configurations.Add(new ChamCongMap());
            modelBuilder.Configurations.Add(new ChamCongSanPhamMap());
            modelBuilder.Configurations.Add(new ChiTietDeNghiNguyenLieuMap());
            modelBuilder.Configurations.Add(new ChiTietDonHangMap());
            modelBuilder.Configurations.Add(new ChiTietDonHangKhachHangDatMap());
            modelBuilder.Configurations.Add(new ChiTietMauMayMap());
            modelBuilder.Configurations.Add(new ChiTietPhieuNhanDonHangMap());
            modelBuilder.Configurations.Add(new ChiTietPhieuNhapNguyenLieuMap());
            modelBuilder.Configurations.Add(new ChiTietPhieuNhapSanPhamMap());
            modelBuilder.Configurations.Add(new ChiTietPhieuNhapThanhPhamMap());
            modelBuilder.Configurations.Add(new ChiTietPhieuXuatNguyenLieuMap());
            modelBuilder.Configurations.Add(new ChiTietPhieuXuatSanPhamMap());
            modelBuilder.Configurations.Add(new ChiTietPhieuXuatThanhPhamMap());
            modelBuilder.Configurations.Add(new ChiTietYeuCauKhachHangMap());
            modelBuilder.Configurations.Add(new ChucVuMap());
            modelBuilder.Configurations.Add(new CongDoanHangMucMap());
            modelBuilder.Configurations.Add(new CongDoanSanPhamDinhNghiaMap());
            modelBuilder.Configurations.Add(new CongDoanThanhPhamMap());
            modelBuilder.Configurations.Add(new CongDoanThanhPhamDinhNghiaMap());
            modelBuilder.Configurations.Add(new DeNghiNguyenLieuMap());
            modelBuilder.Configurations.Add(new DinhNghiaSanPhamMap());
            modelBuilder.Configurations.Add(new DinhNghiaThanhPhamMap());
            modelBuilder.Configurations.Add(new DMCongDoanHangMucMap());
            modelBuilder.Configurations.Add(new DMCongDoanThanhPhamMap());
            modelBuilder.Configurations.Add(new DMHangMucSanXuatMap());
            modelBuilder.Configurations.Add(new DoiTacMap());
            modelBuilder.Configurations.Add(new DonHangMap());
            modelBuilder.Configurations.Add(new DonViMap());
            modelBuilder.Configurations.Add(new GiaTriThuocTinhSanPhamMap());
            modelBuilder.Configurations.Add(new HangMucSanXuatMap());
            modelBuilder.Configurations.Add(new HinhAnhDonHangMap());
            modelBuilder.Configurations.Add(new HinhAnhVanBanMap());
            modelBuilder.Configurations.Add(new LichSuSuaChiTietYeuCauMap());
            modelBuilder.Configurations.Add(new MauMayMap());
            modelBuilder.Configurations.Add(new MessageMap());
            modelBuilder.Configurations.Add(new NganHangMap());
            modelBuilder.Configurations.Add(new NguyenLieuMap());
            modelBuilder.Configurations.Add(new NguyenLieuChiTietDonHangKhachHangDatMap());
            modelBuilder.Configurations.Add(new NguyenLieuYeuCauKhachHangMap());
            modelBuilder.Configurations.Add(new NhanVienMap());
            modelBuilder.Configurations.Add(new NhanVienPhongBanMap());
            modelBuilder.Configurations.Add(new NhanVienPhongChatMap());
            modelBuilder.Configurations.Add(new NhomNguyenLieuMap());
            modelBuilder.Configurations.Add(new PhieuBaoGiaDonHangMap());
            modelBuilder.Configurations.Add(new PhieuNhanDonHangMap());
            modelBuilder.Configurations.Add(new PhieuNhapCongDoanThanhPhamMap());
            modelBuilder.Configurations.Add(new PhieuNhapNguyenLieuMap());
            modelBuilder.Configurations.Add(new PhieuNhapSanPhamMap());
            modelBuilder.Configurations.Add(new PhieuNhapThanhPhamMap());
            modelBuilder.Configurations.Add(new PhieuTaoDonHangMap());
            modelBuilder.Configurations.Add(new PhieuXuatDonHangMap());
            modelBuilder.Configurations.Add(new PhieuXuatNguyenLieuMap());
            modelBuilder.Configurations.Add(new PhieuXuatSanPhamMap());
            modelBuilder.Configurations.Add(new PhieuXuatThanhPhamMap());
            modelBuilder.Configurations.Add(new PhongBanMap());
            modelBuilder.Configurations.Add(new QuanLySanXuatMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new SanPhamMap());
            modelBuilder.Configurations.Add(new SanPhamChaMap());
            modelBuilder.Configurations.Add(new tblGroupMap());
            modelBuilder.Configurations.Add(new tblGroupUserMap());
            modelBuilder.Configurations.Add(new tblLanguageMap());
            modelBuilder.Configurations.Add(new ThanhPhamMap());
            modelBuilder.Configurations.Add(new ThanhToanTienMap());
            modelBuilder.Configurations.Add(new ThongSoSizeMap());
            modelBuilder.Configurations.Add(new ThuocTinhSanPhamMap());
            modelBuilder.Configurations.Add(new TrangThaiPhongChatMessageMap());
            modelBuilder.Configurations.Add(new UpdateManagerMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new VanBanMap());
            modelBuilder.Configurations.Add(new VanBanDinhKemMap());
            modelBuilder.Configurations.Add(new YeuCauKhachHangMap());
        }
    }
}
