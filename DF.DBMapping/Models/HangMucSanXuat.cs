using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class HangMucSanXuat
    {
        public System.Guid HangMucSanXuatID { get; set; }
        public System.Guid SanPhamID { get; set; }
        public Nullable<double> GiaTriDinhMuc { get; set; }
        public Nullable<double> HaoHut { get; set; }
        public Nullable<double> ThoiGianHoanThanh { get; set; }
        public System.Guid QuanLySanXuatID { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> ViTri { get; set; }
    }
}
