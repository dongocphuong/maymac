using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class Role
    {
        public System.Guid RoleID { get; set; }
        public string Name { get; set; }
        public long Code { get; set; }
    }
}
