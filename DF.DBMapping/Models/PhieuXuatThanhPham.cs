using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class PhieuXuatThanhPham
    {
        public System.Guid PhieuXuatThanhPhamID { get; set; }
        public Nullable<System.DateTime> ThoiGian { get; set; }
        public System.Guid NguoiTaoID { get; set; }
        public string MaPhieuXuat { get; set; }
        public System.Guid DonHangID { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.Guid> DonHangNhanID { get; set; }
        public string DanhSachHinhAnhs { get; set; }
        public Nullable<System.Guid> DoiTacID { get; set; }
        public Nullable<System.Guid> PhieuNhapThanhPhamID { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
    }
}
