using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class CongDoanSanPhamDinhNghia
    {
        public System.Guid CongDoanSanPhamDinhNghiaID { get; set; }
        public Nullable<System.Guid> QuanLySanXuatID { get; set; }
        public Nullable<System.Guid> ThanhPhamID { get; set; }
        public Nullable<double> SoLuong { get; set; }
    }
}
