using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class PhieuNhapCongDoanThanhPham
    {
        public System.Guid PhieuNhapCongDoanThanhPhamID { get; set; }
        public System.Guid CongDoanThanhPhamID { get; set; }
        public System.Guid PhieuNhapThanhPhamID { get; set; }
        public bool IsActive { get; set; }
    }
}
