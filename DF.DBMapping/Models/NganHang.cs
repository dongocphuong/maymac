using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class NganHang
    {
        public System.Guid NganHangID { get; set; }
        public string TenNganHang { get; set; }
        public bool IsActive { get; set; }
    }
}
