using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class DMCongDoanThanhPham
    {
        public System.Guid DMCongDoanThanhPhamID { get; set; }
        public System.Guid DMCongDoanHangMucID { get; set; }
        public System.Guid ThanhPhamID { get; set; }
        public double SoLuong { get; set; }
        public bool IsActive { get; set; }
    }
}
