using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class SanPhamCha
    {
        public System.Guid SanPhamChaID { get; set; }
        public string MaSanPham { get; set; }
        public string TenSanPham { get; set; }
        public Nullable<int> GioiTinh { get; set; }
        public Nullable<int> Loai { get; set; }
        public string AnhDaiHien { get; set; }
        public string DanhSachHinhAnh { get; set; }
    }
}
