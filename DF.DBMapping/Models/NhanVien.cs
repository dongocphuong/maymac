using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class NhanVien
    {
        public System.Guid NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public string MaNhanVien { get; set; }
        public System.DateTime NgaySinh { get; set; }
        public string DiaChi { get; set; }
        public string SDT { get; set; }
        public string CMT { get; set; }
        public string AnhDaiDien { get; set; }
        public int GioiTinh { get; set; }
        public int TinhTrang { get; set; }
        public bool IsActive { get; set; }
        public string Avatar { get; set; }
        public Nullable<System.Guid> DoiTacID { get; set; }
    }
}
