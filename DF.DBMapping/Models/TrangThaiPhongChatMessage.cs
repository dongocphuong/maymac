using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class TrangThaiPhongChatMessage
    {
        public System.Guid TrangThaiPhongChatMessageID { get; set; }
        public System.Guid DoiTacID { get; set; }
        public System.DateTime LastRead { get; set; }
        public System.Guid NhanVienID { get; set; }
    }
}
