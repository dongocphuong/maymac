using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class GiaTriThuocTinhSanPham
    {
        public System.Guid GiaTriThuocTinhID { get; set; }
        public Nullable<System.Guid> ThuocTinhSanPhamID { get; set; }
        public string GiaTri { get; set; }
        public Nullable<System.Guid> SanPhamID { get; set; }
    }
}
