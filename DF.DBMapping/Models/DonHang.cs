using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class DonHang
    {
        public System.Guid DonHangID { get; set; }
        public string TenDonHang { get; set; }
        public string MaDonHang { get; set; }
        public System.Guid DoiTacID { get; set; }
        public Nullable<System.DateTime> ThoiGian { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> TrangThai { get; set; }
        public string DiaDiemGiaoHang { get; set; }
        public Nullable<System.DateTime> ThoiGianThanhToan { get; set; }
        public Nullable<double> SoTienDaThanhToan { get; set; }
        public Nullable<int> LoaiHinhThanhToan { get; set; }
        public string YeuCauKhac { get; set; }
        public string HinhAnh { get; set; }
        public Nullable<System.DateTime> CapNhatLanCuoi { get; set; }
        public Nullable<System.Guid> YeuCauKhachHangID { get; set; }
        public Nullable<System.Guid> NhanVienTaoID { get; set; }
        public string EmailKeToan { get; set; }
        public Nullable<System.DateTime> ThoiGianDatCoc { get; set; }
        public Nullable<System.DateTime> ThoiGianTao { get; set; }
        public Nullable<System.DateTime> ThoiGianDuKienSanXuat { get; set; }
        public Nullable<System.DateTime> ThoiGianDuKienGiaoHang { get; set; }
    }
}
