using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class DMHangMucSanXuat
    {
        public System.Guid DMHangMucSanXuatID { get; set; }
        public System.Guid SanPhamID { get; set; }
        public Nullable<double> GiaTriDinhMuc { get; set; }
        public Nullable<double> HaoHut { get; set; }
        public Nullable<double> ThoiGianHoanThanh { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> ViTri { get; set; }
        public string MoTa { get; set; }
        public Nullable<double> GiaTri { get; set; }
        public string DsHinhAnh { get; set; }
    }
}
