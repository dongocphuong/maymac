using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChiTietYeuCauKhachHang
    {
        public System.Guid ChiTietYeuCauID { get; set; }
        public Nullable<System.Guid> YeuCauKhachHangID { get; set; }
        public string TenSanPham { get; set; }
        public string Size { get; set; }
        public string Mau { get; set; }
        public Nullable<double> SoLuongDat { get; set; }
        public Nullable<double> GiaBan { get; set; }
        public string YeuCauKhac { get; set; }
        public Nullable<int> IsActive { get; set; }
        public string DacDiem { get; set; }
        public string ChatLieu { get; set; }
        public string MauPhoi { get; set; }
        public Nullable<System.DateTime> ThoiGianGiaoHang { get; set; }
    }
}
