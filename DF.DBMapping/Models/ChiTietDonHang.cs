using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChiTietDonHang
    {
        public System.Guid ChiTietDonHangID { get; set; }
        public System.Guid DonHangID { get; set; }
        public System.Guid SanPhamID { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<int> TrangThai { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
        public Nullable<double> SoLuongGiao { get; set; }
        public string TenCongDoan { get; set; }
        public string Mau { get; set; }
        public string GhiChu { get; set; }
        public Nullable<double> TiLeSize { get; set; }
    }
}
