using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class NhanVienPhongBan
    {
        public System.Guid NhanVienPhongBanID { get; set; }
        public System.Guid NhanVienID { get; set; }
        public System.Guid ChucVuID { get; set; }
        public System.Guid PhongBanID { get; set; }
        public Nullable<System.DateTime> ThoiGian { get; set; }
        public bool IsActive { get; set; }
    }
}
