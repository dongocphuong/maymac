using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChiTietDeNghiNguyenLieu
    {
        public System.Guid ChiTietDeNghiNguyenLieuID { get; set; }
        public System.Guid DeNghiNguyenLieuID { get; set; }
        public System.Guid NguyenLieuID { get; set; }
        public double SoLuong { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<double> SoLuongDuyet { get; set; }
    }
}
