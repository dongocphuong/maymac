using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class DMCongDoanHangMuc
    {
        public System.Guid DMCongDoanHangMucID { get; set; }
        public string TenCongDoan { get; set; }
        public System.Guid DMHangMucSanXuatID { get; set; }
        public bool IsActive { get; set; }
    }
}
