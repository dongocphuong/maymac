using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChiTietPhieuXuatThanhPham
    {
        public System.Guid ChiTietPhieuXuatThanhPhamID { get; set; }
        public System.Guid PhieuXuatThanhPhamID { get; set; }
        public System.Guid ThanhPhamID { get; set; }
        public double SoLuong { get; set; }
        public int TinhTrang { get; set; }
        public bool IsActive { get; set; }
    }
}
