using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class NguyenLieuChiTietDonHangKhachHangDat
    {
        public System.Guid NguyenLieuChiTietDonHangKhachHangDatID { get; set; }
        public string Ten { get; set; }
        public string NoiDung { get; set; }
        public Nullable<System.Guid> ChiTietDonHangKhachHangDatID { get; set; }
    }
}
