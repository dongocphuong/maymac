using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ThuocTinhSanPham
    {
        public System.Guid ThuocTinhSanPhamID { get; set; }
        public string TenThuocTinh { get; set; }
        public Nullable<int> KieuDuLieu { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
        public Nullable<int> LoaiHinhSanXuat { get; set; }
        public Nullable<int> ViTri { get; set; }
    }
}
