using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class NguyenLieuYeuCauKhachHang
    {
        public System.Guid NguyenLieuYeuCauKhachHangID { get; set; }
        public Nullable<System.Guid> NguyenLieuID { get; set; }
        public Nullable<System.Guid> YeuCauKhachHangID { get; set; }
        public Nullable<System.Guid> SanPhamID { get; set; }
        public Nullable<int> LoaiVai { get; set; }
    }
}
