using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class Message
    {
        public System.Guid MessageID { get; set; }
        public string NoiDung { get; set; }
        public string DanhSachDinhKems { get; set; }
        public System.Guid NguoiGuiID { get; set; }
        public System.Guid PhongChatID { get; set; }
        public System.DateTime ThoiGian { get; set; }
        public Nullable<System.Guid> YeuCauKhachHangID { get; set; }
        public int TrangThai { get; set; }
        public string Video { get; set; }
        public string Thumb { get; set; }
    }
}
