using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class HinhAnhDonHang
    {
        public System.Guid HinhAnhDonHangID { get; set; }
        public string TenFile { get; set; }
        public string FileMaHoa { get; set; }
        public System.Guid DonHangID { get; set; }
        public bool IsActive { get; set; }
    }
}
