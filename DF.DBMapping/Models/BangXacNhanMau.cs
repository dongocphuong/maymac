using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class BangXacNhanMau
    {
        public System.Guid BangXacNhanMauID { get; set; }
        public Nullable<System.Guid> MauMayID { get; set; }
        public string CapMay { get; set; }
        public string NhanVienThietKe { get; set; }
        public string ThietKeLan { get; set; }
        public string LenNen { get; set; }
        public string LenNenF1 { get; set; }
        public string LenNenF2 { get; set; }
        public string LenNenF3 { get; set; }
        public string LyNen { get; set; }
        public string LyNenF1 { get; set; }
        public string LyNenF2 { get; set; }
        public string LyNenF3 { get; set; }
        public string LyNenNep { get; set; }
        public string LyNenTong { get; set; }
        public string TT { get; set; }
        public string TS { get; set; }
        public string Tay { get; set; }
        public string Co { get; set; }
        public string Nep { get; set; }
        public string Tong { get; set; }
        public string MatDo1 { get; set; }
        public string MatDo2 { get; set; }
        public string MatDo3 { get; set; }
        public string TocDoTK { get; set; }
        public string TocDoMay { get; set; }
        public string Mo1 { get; set; }
        public string Mo2 { get; set; }
        public string Mo3 { get; set; }
        public string Mo4 { get; set; }
        public string Mo5 { get; set; }
        public string GhiChu1 { get; set; }
        public string LK { get; set; }
        public string VS { get; set; }
        public string GM { get; set; }
        public string LingkingNep { get; set; }
        public string LingkingTong { get; set; }
        public string GhiChu2 { get; set; }
        public string GhiChu3 { get; set; }
        public string GhiChu4 { get; set; }
        public string GhiChu5 { get; set; }
        public string May1 { get; set; }
        public string May2 { get; set; }
        public string May3 { get; set; }
        public string GiatSay1 { get; set; }
        public string GiatSay2 { get; set; }
        public string GiatSay3 { get; set; }
        public string DuyetMau { get; set; }
        public string NhanXet1 { get; set; }
        public string NhanXet2 { get; set; }
        public string NhanXet3 { get; set; }
        public string TrongLuongPhoiThanhPham { get; set; }
    }
}
