using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class User
    {
        public System.Guid UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Nullable<System.Guid> NhanVienId { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> FullRole { get; set; }
    }
}
