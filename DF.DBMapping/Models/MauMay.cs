using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class MauMay
    {
        public System.Guid MauMayID { get; set; }
        public Nullable<System.Guid> YeuCauKhachHangID { get; set; }
        public string MaMauMay { get; set; }
        public string TenMauMay { get; set; }
        public System.DateTime ThoiGian { get; set; }
        public string DanhSachHinhAnhs { get; set; }
        public int IsActive { get; set; }
        public Nullable<int> LoaiHinhSanXuat { get; set; }
    }
}
