using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class PhieuBaoGiaDonHang
    {
        public System.Guid PhieuBaoGiaDonHangID { get; set; }
        public System.Guid DonHangID { get; set; }
        public System.DateTime ThoiGian { get; set; }
        public System.Guid NhanVienBaoGiaID { get; set; }
        public string NoiDung { get; set; }
        public string DanhSachHinhAnhs { get; set; }
    }
}
