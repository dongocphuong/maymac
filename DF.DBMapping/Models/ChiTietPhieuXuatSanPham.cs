using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChiTietPhieuXuatSanPham
    {
        public System.Guid ChiTietPhieuXuatSanPhamID { get; set; }
        public System.Guid PhieuXuatSanPhamID { get; set; }
        public System.Guid SanPhamID { get; set; }
        public double SoLuong { get; set; }
        public int TinhTrang { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
    }
}
