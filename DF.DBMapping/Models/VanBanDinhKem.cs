using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class VanBanDinhKem
    {
        public System.Guid VanBanID { get; set; }
        public string TieuDe { get; set; }
        public Nullable<int> LoaiVanBan { get; set; }
        public string NoiDung { get; set; }
        public string FileDinhKem { get; set; }
        public System.Guid NguoiTaoID { get; set; }
        public System.DateTime ThoiGianTao { get; set; }
        public bool IsActive { get; set; }
    }
}
