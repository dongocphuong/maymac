using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class NhanVienPhongChat
    {
        public System.Guid NhanVienPhongChatID { get; set; }
        public System.Guid NhanVienID { get; set; }
        public System.Guid DoiTacID { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.Guid> ChucVuPhongChatID { get; set; }
    }
}
