using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class QuanLySanXuat
    {
        public System.Guid QuanLySanXuatID { get; set; }
        public System.Guid DonHangID { get; set; }
        public System.Guid SanPhamID { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> ThoiGian { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
        public string TenCongDoan { get; set; }
        public Nullable<int> TrangThaiCongDoanCuoi { get; set; }
        public Nullable<System.DateTime> ThoiGianBatDauCongDoanCuoi { get; set; }
        public string LyDoTamDungCongDoanCuoi { get; set; }
    }
}
