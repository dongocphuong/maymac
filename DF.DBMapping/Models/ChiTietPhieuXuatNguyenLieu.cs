using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChiTietPhieuXuatNguyenLieu
    {
        public System.Guid ChiTietPhieuXuatNguyenLieuID { get; set; }
        public System.Guid PhieuXuatNguyenLieu { get; set; }
        public System.Guid NguyenLieuID { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public bool IsActive { get; set; }
    }
}
