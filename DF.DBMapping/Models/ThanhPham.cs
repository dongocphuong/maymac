using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ThanhPham
    {
        public System.Guid ThanhPhamID { get; set; }
        public string TenThanhPham { get; set; }
        public string MaThanhPham { get; set; }
        public string DanhSachHinhAnhs { get; set; }
        public System.Guid DonViID { get; set; }
        public string AnhDaiDien { get; set; }
        public bool IsActive { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<System.Guid> SanPhamID { get; set; }
    }
}
