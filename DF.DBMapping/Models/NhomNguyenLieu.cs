using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class NhomNguyenLieu
    {
        public System.Guid NhomNguyenLieuID { get; set; }
        public Nullable<System.Guid> NguyenLieuID { get; set; }
        public Nullable<int> IsActive { get; set; }
        public string TenNhomNguyenLieu { get; set; }
    }
}
