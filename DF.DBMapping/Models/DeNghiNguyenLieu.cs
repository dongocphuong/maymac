using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class DeNghiNguyenLieu
    {
        public System.Guid DeNghiNguyenLieuID { get; set; }
        public System.Guid DonHangID { get; set; }
        public int LoaiHinh { get; set; }
        public int TrangThai { get; set; }
        public System.Guid SanPhamID { get; set; }
        public System.Guid NhanVienID { get; set; }
        public string LichSuDeNghis { get; set; }
        public Nullable<System.Guid> NguoiDuyet { get; set; }
        public Nullable<System.DateTime> ThoiGianDuyet { get; set; }
        public string MaDeNghi { get; set; }
        public string NoiDung { get; set; }
        public Nullable<System.DateTime> ThoiGian { get; set; }
    }
}
