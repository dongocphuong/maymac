using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class NguyenLieu
    {
        public System.Guid NguyenLieuID { get; set; }
        public string TenNguyenLieu { get; set; }
        public string MaNguyenLieu { get; set; }
        public string DanhSachHinhAnhs { get; set; }
        public System.Guid DonViID { get; set; }
        public string AnhDaiDien { get; set; }
        public bool IsActive { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<double> HeSoQuyDoi { get; set; }
        public Nullable<int> Type { get; set; }
        public Nullable<System.Guid> NhomNguyenLieuID { get; set; }
        public string MaMau { get; set; }
    }
}
