using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class UpdateManager
    {
        public string PlatformKey { get; set; }
        public double VersionCode { get; set; }
        public string Url { get; set; }
        public bool ForceUpdate { get; set; }
    }
}
