using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class VanBan
    {
        public System.Guid VanBanID { get; set; }
        public Nullable<System.Guid> TenVanBan { get; set; }
        public Nullable<System.Guid> PhongBanID { get; set; }
    }
}
