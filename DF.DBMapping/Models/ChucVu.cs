using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChucVu
    {
        public System.Guid ChucVuId { get; set; }
        public int MaChucVu { get; set; }
        public string TenChucVu { get; set; }
        public Nullable<int> CapDo { get; set; }
        public bool IsActive { get; set; }
    }
}
