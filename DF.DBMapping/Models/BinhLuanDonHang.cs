using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class BinhLuanDonHang
    {
        public System.Guid BinhLuanDonHangID { get; set; }
        public System.Guid YeuCauDonHangID { get; set; }
        public System.DateTime ThoiGian { get; set; }
        public string DanhSachHinhAnhs { get; set; }
        public System.Guid NhanVienID { get; set; }
        public string NoiDung { get; set; }
        public Nullable<System.Guid> YeuCauKhachHangID { get; set; }
    }
}
