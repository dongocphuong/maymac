using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class DinhNghiaSanPham
    {
        public System.Guid DinhNghiaSanPhamID { get; set; }
        public System.Guid SanPhamID { get; set; }
        public System.Guid ThanhPhamID { get; set; }
        public double SoLuongThanhPham { get; set; }
        public bool IsActive { get; set; }
        public Nullable<double> ThoiGianHoanThanh { get; set; }
        public string MoTa { get; set; }
        public Nullable<double> GiaTri { get; set; }
        public string DsHinhAnh { get; set; }
    }
}
