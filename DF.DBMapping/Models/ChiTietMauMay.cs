using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChiTietMauMay
    {
        public System.Guid ChiTietMauMayID { get; set; }
        public System.Guid MauMayID { get; set; }
        public int STT { get; set; }
        public string NoiDungChiTiet { get; set; }
        public Nullable<System.Guid> NguoiThucHien { get; set; }
        public Nullable<System.Guid> NguoiPhuTrach { get; set; }
        public string YeuCauPAThucHien { get; set; }
        public double ThoiGianThucHien { get; set; }
        public string GhiChu { get; set; }
        public int IsActive { get; set; }
    }
}
