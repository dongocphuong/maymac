using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class PhieuNhapSanPham
    {
        public System.Guid PhieuNhapSanPhamID { get; set; }
        public System.DateTime ThoiGian { get; set; }
        public Nullable<System.Guid> DoiTacID { get; set; }
        public System.Guid NguoiTaoID { get; set; }
        public string MaPhieuNhap { get; set; }
        public System.Guid DonHangID { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.Guid> PhieuXuatSanPhamID { get; set; }
        public string DanhSachHinhAnhs { get; set; }
    }
}
