using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class tblLanguage
    {
        public string langkey { get; set; }
        public string vn { get; set; }
        public string cn { get; set; }
        public string en { get; set; }
    }
}
