using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class CongDoanThanhPhamDinhNghia
    {
        public Nullable<System.Guid> DonHangID { get; set; }
        public System.Guid CongDoanThanhPhamDinhNghiaID { get; set; }
        public Nullable<System.Guid> ThanhPhamID { get; set; }
        public Nullable<System.Guid> ID { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<System.Guid> CongDoanThanhPhamID { get; set; }
        public Nullable<int> LoaiVai { get; set; }
    }
}
