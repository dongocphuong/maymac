using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChiTietPhieuNhapThanhPham
    {
        public System.Guid ChiTietPhieuNhapThanhPhamID { get; set; }
        public System.Guid PhieuNhapThanhPhamID { get; set; }
        public System.Guid ThanhPhamID { get; set; }
        public double GiaNhap { get; set; }
        public double SoLuong { get; set; }
        public int TinhTrang { get; set; }
        public bool IsActive { get; set; }
        public string LyDoHong { get; set; }
    }
}
