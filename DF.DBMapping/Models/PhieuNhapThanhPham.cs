using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class PhieuNhapThanhPham
    {
        public System.Guid PhieuNhapThanhPhamID { get; set; }
        public Nullable<System.DateTime> ThoiGian { get; set; }
        public Nullable<System.Guid> DoiTacID { get; set; }
        public System.Guid NguoiTaoID { get; set; }
        public string MaPhieuNhap { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.Guid> PhieuXuatThanhPhamID { get; set; }
        public string DanhSachHinhAnhs { get; set; }
        public Nullable<System.Guid> DonHangID { get; set; }
    }
}
