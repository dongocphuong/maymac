using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class PhieuNhanDonHang
    {
        public System.Guid PhieuNhanDonHangID { get; set; }
        public System.DateTime ThoiGian { get; set; }
        public System.Guid NhanVienID { get; set; }
        public int TrangThaiDuyet { get; set; }
        public bool IsActive { get; set; }
        public string NoiDung { get; set; }
        public Nullable<System.Guid> DoiTacID { get; set; }
        public Nullable<System.Guid> DonHangID { get; set; }
        public string DanhSachHinhAnhs { get; set; }
        public Nullable<System.Guid> NguoiDuyetID { get; set; }
        public Nullable<System.DateTime> ThoiGianDuyet { get; set; }
        public string MaPhieu { get; set; }
        public Nullable<System.Guid> DonHangXuatID { get; set; }
    }
}
