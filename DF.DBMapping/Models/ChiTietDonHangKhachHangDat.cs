using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChiTietDonHangKhachHangDat
    {
        public System.Guid ChiTietDonHangKhachHangDatID { get; set; }
        public string Size { get; set; }
        public string Mau { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<int> GioiTinh { get; set; }
        public string Ten { get; set; }
        public Nullable<System.Guid> DonHangID { get; set; }
        public Nullable<int> Loai { get; set; }
        public string DSHinhAnh { get; set; }
    }
}
