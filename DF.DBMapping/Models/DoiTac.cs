using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class DoiTac
    {
        public System.Guid DoiTacID { get; set; }
        public string TenDoiTac { get; set; }
        public Nullable<int> KieuDoiTac { get; set; }
        public Nullable<System.Guid> NganHangID { get; set; }
        public string DiaChi { get; set; }
        public string SDT { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string MaDoiTac { get; set; }
        public string Email { get; set; }
        public string MaSoThue { get; set; }
        public string SoTaiKhoan { get; set; }
    }
}
