using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class HinhAnhVanBan
    {
        public System.Guid HinhAnhVanBanID { get; set; }
        public string TenFile { get; set; }
        public string FileMaHoa { get; set; }
        public System.Guid VanBanID { get; set; }
        public bool IsActive { get; set; }
    }
}
