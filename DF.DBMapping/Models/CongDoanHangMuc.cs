using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class CongDoanHangMuc
    {
        public System.Guid CongDoanHangMucID { get; set; }
        public string TenCongDoan { get; set; }
        public System.Guid HangMucSanXuatID { get; set; }
        public Nullable<System.DateTime> ThoiGianBatDau { get; set; }
        public string LyDoTamDung { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> TrangThai { get; set; }
    }
}
