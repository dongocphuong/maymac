using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class YeuCauKhachHang
    {
        public System.Guid YeuCauKhachHangID { get; set; }
        public string NoiDung { get; set; }
        public string HinhAnh { get; set; }
        public System.DateTime ThoiGian { get; set; }
        public System.Guid DoiTacID { get; set; }
        public string LichSuDuyet { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> TinhTrang { get; set; }
        public Nullable<int> LoaiHinhSanXuat { get; set; }
        public Nullable<System.Guid> NguoiTaoID { get; set; }
        public string NoiDung1 { get; set; }
        public string MaYeuCau { get; set; }
        public Nullable<int> STT { get; set; }
        public Nullable<double> GiaBan { get; set; }
        public Nullable<double> SoLuongDat { get; set; }
        public Nullable<System.DateTime> ThoiGianGiaoHang { get; set; }
    }
}
