using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class SanPham
    {
        public System.Guid SanPhamID { get; set; }
        public string TenSanPham { get; set; }
        public string MaSanPham { get; set; }
        public string DanhSachHinhAnhs { get; set; }
        public System.Guid DonViID { get; set; }
        public string AnhDaiDien { get; set; }
        public Nullable<double> DonGia { get; set; }
        public bool IsActive { get; set; }
        public string Size { get; set; }
        public string SizeMauGoc { get; set; }
        public string TiLeSize { get; set; }
        public Nullable<System.Guid> MauMayID { get; set; }
        public string Mau { get; set; }
        public Nullable<int> Loai { get; set; }
        public Nullable<int> GioiTinh { get; set; }
        public Nullable<System.DateTime> ThoiGian { get; set; }
        public Nullable<int> LoaiHinhSanXuat { get; set; }
    }
}
