using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class DinhNghiaThanhPham
    {
        public System.Guid DinhNghiaThanhPhamID { get; set; }
        public System.Guid ThanhPhamID { get; set; }
        public System.Guid ID { get; set; }
        public int LoaiHinh { get; set; }
        public double SoLuong { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> LoaiVai { get; set; }
    }
}
