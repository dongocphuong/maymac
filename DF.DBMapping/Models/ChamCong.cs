using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChamCong
    {
        public System.Guid ChamCongID { get; set; }
        public Nullable<System.Guid> NhanVienID { get; set; }
        public Nullable<double> SoCong { get; set; }
        public Nullable<System.DateTime> ThoiGian { get; set; }
    }
}
