using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ThongSoSize
    {
        public string Size { get; set; }
        public string TenThongSo { get; set; }
        public string GiaTri { get; set; }
        public System.Guid ID { get; set; }
    }
}
