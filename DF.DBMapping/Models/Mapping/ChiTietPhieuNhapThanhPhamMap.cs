using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChiTietPhieuNhapThanhPhamMap : EntityTypeConfiguration<ChiTietPhieuNhapThanhPham>
    {
        public ChiTietPhieuNhapThanhPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.ChiTietPhieuNhapThanhPhamID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ChiTietPhieuNhapThanhPham");
            this.Property(t => t.ChiTietPhieuNhapThanhPhamID).HasColumnName("ChiTietPhieuNhapThanhPhamID");
            this.Property(t => t.PhieuNhapThanhPhamID).HasColumnName("PhieuNhapThanhPhamID");
            this.Property(t => t.ThanhPhamID).HasColumnName("ThanhPhamID");
            this.Property(t => t.GiaNhap).HasColumnName("GiaNhap");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.TinhTrang).HasColumnName("TinhTrang");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.LyDoHong).HasColumnName("LyDoHong");
        }
    }
}
