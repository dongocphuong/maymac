using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChiTietPhieuNhapSanPhamMap : EntityTypeConfiguration<ChiTietPhieuNhapSanPham>
    {
        public ChiTietPhieuNhapSanPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.ChiTietPhieuNhapSanPhamID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ChiTietPhieuNhapSanPham");
            this.Property(t => t.ChiTietPhieuNhapSanPhamID).HasColumnName("ChiTietPhieuNhapSanPhamID");
            this.Property(t => t.PhieuNhapSanPhamID).HasColumnName("PhieuNhapSanPhamID");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
            this.Property(t => t.GiaNhap).HasColumnName("GiaNhap");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.TinhTrang).HasColumnName("TinhTrang");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
            this.Property(t => t.LyDoHong).HasColumnName("LyDoHong");
        }
    }
}
