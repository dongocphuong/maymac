using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class DeNghiNguyenLieuMap : EntityTypeConfiguration<DeNghiNguyenLieu>
    {
        public DeNghiNguyenLieuMap()
        {
            // Primary Key
            this.HasKey(t => t.DeNghiNguyenLieuID);

            // Properties
            this.Property(t => t.LichSuDeNghis)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("DeNghiNguyenLieu");
            this.Property(t => t.DeNghiNguyenLieuID).HasColumnName("DeNghiNguyenLieuID");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
            this.Property(t => t.TrangThai).HasColumnName("TrangThai");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
            this.Property(t => t.NhanVienID).HasColumnName("NhanVienID");
            this.Property(t => t.LichSuDeNghis).HasColumnName("LichSuDeNghis");
            this.Property(t => t.NguoiDuyet).HasColumnName("NguoiDuyet");
            this.Property(t => t.ThoiGianDuyet).HasColumnName("ThoiGianDuyet");
            this.Property(t => t.MaDeNghi).HasColumnName("MaDeNghi");
            this.Property(t => t.NoiDung).HasColumnName("NoiDung");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
        }
    }
}
