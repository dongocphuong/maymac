using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class MessageMap : EntityTypeConfiguration<Message>
    {
        public MessageMap()
        {
            // Primary Key
            this.HasKey(t => t.MessageID);

            // Properties
            this.Property(t => t.NoiDung)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("Message");
            this.Property(t => t.MessageID).HasColumnName("MessageID");
            this.Property(t => t.NoiDung).HasColumnName("NoiDung");
            this.Property(t => t.DanhSachDinhKems).HasColumnName("DanhSachDinhKems");
            this.Property(t => t.NguoiGuiID).HasColumnName("NguoiGuiID");
            this.Property(t => t.PhongChatID).HasColumnName("PhongChatID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.YeuCauKhachHangID).HasColumnName("YeuCauKhachHangID");
            this.Property(t => t.TrangThai).HasColumnName("TrangThai");
            this.Property(t => t.Video).HasColumnName("Video");
            this.Property(t => t.Thumb).HasColumnName("Thumb");
        }
    }
}
