using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class NhanVienMap : EntityTypeConfiguration<NhanVien>
    {
        public NhanVienMap()
        {
            // Primary Key
            this.HasKey(t => t.NhanVienID);

            // Properties
            this.Property(t => t.TenNhanVien)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.MaNhanVien)
                .HasMaxLength(200);

            this.Property(t => t.DiaChi)
                .HasMaxLength(500);

            this.Property(t => t.SDT)
                .HasMaxLength(20);

            this.Property(t => t.CMT)
                .HasMaxLength(20);

            this.Property(t => t.AnhDaiDien)
                .HasMaxLength(500);

            this.Property(t => t.Avatar)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("NhanVien");
            this.Property(t => t.NhanVienID).HasColumnName("NhanVienID");
            this.Property(t => t.TenNhanVien).HasColumnName("TenNhanVien");
            this.Property(t => t.MaNhanVien).HasColumnName("MaNhanVien");
            this.Property(t => t.NgaySinh).HasColumnName("NgaySinh");
            this.Property(t => t.DiaChi).HasColumnName("DiaChi");
            this.Property(t => t.SDT).HasColumnName("SDT");
            this.Property(t => t.CMT).HasColumnName("CMT");
            this.Property(t => t.AnhDaiDien).HasColumnName("AnhDaiDien");
            this.Property(t => t.GioiTinh).HasColumnName("GioiTinh");
            this.Property(t => t.TinhTrang).HasColumnName("TinhTrang");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.Avatar).HasColumnName("Avatar");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
        }
    }
}
