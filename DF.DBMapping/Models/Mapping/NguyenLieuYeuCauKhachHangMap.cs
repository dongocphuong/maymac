using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class NguyenLieuYeuCauKhachHangMap : EntityTypeConfiguration<NguyenLieuYeuCauKhachHang>
    {
        public NguyenLieuYeuCauKhachHangMap()
        {
            // Primary Key
            this.HasKey(t => t.NguyenLieuYeuCauKhachHangID);

            // Properties
            // Table & Column Mappings
            this.ToTable("NguyenLieuYeuCauKhachHang");
            this.Property(t => t.NguyenLieuYeuCauKhachHangID).HasColumnName("NguyenLieuYeuCauKhachHangID");
            this.Property(t => t.NguyenLieuID).HasColumnName("NguyenLieuID");
            this.Property(t => t.YeuCauKhachHangID).HasColumnName("YeuCauKhachHangID");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
            this.Property(t => t.LoaiVai).HasColumnName("LoaiVai");
        }
    }
}
