using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class NguyenLieuChiTietDonHangKhachHangDatMap : EntityTypeConfiguration<NguyenLieuChiTietDonHangKhachHangDat>
    {
        public NguyenLieuChiTietDonHangKhachHangDatMap()
        {
            // Primary Key
            this.HasKey(t => t.NguyenLieuChiTietDonHangKhachHangDatID);

            // Properties
            this.Property(t => t.Ten)
                .HasMaxLength(255);

            this.Property(t => t.NoiDung)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("NguyenLieuChiTietDonHangKhachHangDat");
            this.Property(t => t.NguyenLieuChiTietDonHangKhachHangDatID).HasColumnName("NguyenLieuChiTietDonHangKhachHangDatID");
            this.Property(t => t.Ten).HasColumnName("Ten");
            this.Property(t => t.NoiDung).HasColumnName("NoiDung");
            this.Property(t => t.ChiTietDonHangKhachHangDatID).HasColumnName("ChiTietDonHangKhachHangDatID");
        }
    }
}
