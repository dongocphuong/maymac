using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class DonViMap : EntityTypeConfiguration<DonVi>
    {
        public DonViMap()
        {
            // Primary Key
            this.HasKey(t => t.DonViId);

            // Properties
            this.Property(t => t.TenDonVi)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("DonVi");
            this.Property(t => t.DonViId).HasColumnName("DonViId");
            this.Property(t => t.TenDonVi).HasColumnName("TenDonVi");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
