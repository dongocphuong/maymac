using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChamCongSanPhamMap : EntityTypeConfiguration<ChamCongSanPham>
    {
        public ChamCongSanPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.ChamCongSanPhamID);

            // Properties
            this.Property(t => t.MaSanPham)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("ChamCongSanPham");
            this.Property(t => t.ChamCongSanPhamID).HasColumnName("ChamCongSanPhamID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.NhanVienID).HasColumnName("NhanVienID");
            this.Property(t => t.MaSanPham).HasColumnName("MaSanPham");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.KhoangThoiGian).HasColumnName("KhoangThoiGian");
            this.Property(t => t.DonGia).HasColumnName("DonGia");
        }
    }
}
