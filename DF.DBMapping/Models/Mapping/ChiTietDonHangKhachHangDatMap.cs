using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChiTietDonHangKhachHangDatMap : EntityTypeConfiguration<ChiTietDonHangKhachHangDat>
    {
        public ChiTietDonHangKhachHangDatMap()
        {
            // Primary Key
            this.HasKey(t => t.ChiTietDonHangKhachHangDatID);

            // Properties
            this.Property(t => t.Size)
                .HasMaxLength(50);

            this.Property(t => t.Mau)
                .HasMaxLength(50);

            this.Property(t => t.Ten)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("ChiTietDonHangKhachHangDat");
            this.Property(t => t.ChiTietDonHangKhachHangDatID).HasColumnName("ChiTietDonHangKhachHangDatID");
            this.Property(t => t.Size).HasColumnName("Size");
            this.Property(t => t.Mau).HasColumnName("Mau");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.GioiTinh).HasColumnName("GioiTinh");
            this.Property(t => t.Ten).HasColumnName("Ten");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.Loai).HasColumnName("Loai");
            this.Property(t => t.DSHinhAnh).HasColumnName("DSHinhAnh");
        }
    }
}
