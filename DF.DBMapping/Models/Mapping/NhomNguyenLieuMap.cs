using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class NhomNguyenLieuMap : EntityTypeConfiguration<NhomNguyenLieu>
    {
        public NhomNguyenLieuMap()
        {
            // Primary Key
            this.HasKey(t => t.NhomNguyenLieuID);

            // Properties
            this.Property(t => t.TenNhomNguyenLieu)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("NhomNguyenLieu");
            this.Property(t => t.NhomNguyenLieuID).HasColumnName("NhomNguyenLieuID");
            this.Property(t => t.NguyenLieuID).HasColumnName("NguyenLieuID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.TenNhomNguyenLieu).HasColumnName("TenNhomNguyenLieu");
        }
    }
}
