using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class CongDoanHangMucMap : EntityTypeConfiguration<CongDoanHangMuc>
    {
        public CongDoanHangMucMap()
        {
            // Primary Key
            this.HasKey(t => t.CongDoanHangMucID);

            // Properties
            this.Property(t => t.TenCongDoan)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.LyDoTamDung)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("CongDoanHangMuc");
            this.Property(t => t.CongDoanHangMucID).HasColumnName("CongDoanHangMucID");
            this.Property(t => t.TenCongDoan).HasColumnName("TenCongDoan");
            this.Property(t => t.HangMucSanXuatID).HasColumnName("HangMucSanXuatID");
            this.Property(t => t.ThoiGianBatDau).HasColumnName("ThoiGianBatDau");
            this.Property(t => t.LyDoTamDung).HasColumnName("LyDoTamDung");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.TrangThai).HasColumnName("TrangThai");
        }
    }
}
