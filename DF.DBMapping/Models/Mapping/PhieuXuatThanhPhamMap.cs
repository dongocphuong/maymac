using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class PhieuXuatThanhPhamMap : EntityTypeConfiguration<PhieuXuatThanhPham>
    {
        public PhieuXuatThanhPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.PhieuXuatThanhPhamID);

            // Properties
            this.Property(t => t.MaPhieuXuat)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("PhieuXuatThanhPham");
            this.Property(t => t.PhieuXuatThanhPhamID).HasColumnName("PhieuXuatThanhPhamID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.NguoiTaoID).HasColumnName("NguoiTaoID");
            this.Property(t => t.MaPhieuXuat).HasColumnName("MaPhieuXuat");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.DonHangNhanID).HasColumnName("DonHangNhanID");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.PhieuNhapThanhPhamID).HasColumnName("PhieuNhapThanhPhamID");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
        }
    }
}
