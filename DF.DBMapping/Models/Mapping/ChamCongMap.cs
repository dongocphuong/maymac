using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChamCongMap : EntityTypeConfiguration<ChamCong>
    {
        public ChamCongMap()
        {
            // Primary Key
            this.HasKey(t => t.ChamCongID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ChamCong");
            this.Property(t => t.ChamCongID).HasColumnName("ChamCongID");
            this.Property(t => t.NhanVienID).HasColumnName("NhanVienID");
            this.Property(t => t.SoCong).HasColumnName("SoCong");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
        }
    }
}
