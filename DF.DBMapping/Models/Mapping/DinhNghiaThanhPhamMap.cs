using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class DinhNghiaThanhPhamMap : EntityTypeConfiguration<DinhNghiaThanhPham>
    {
        public DinhNghiaThanhPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.DinhNghiaThanhPhamID);

            // Properties
            // Table & Column Mappings
            this.ToTable("DinhNghiaThanhPham");
            this.Property(t => t.DinhNghiaThanhPhamID).HasColumnName("DinhNghiaThanhPhamID");
            this.Property(t => t.ThanhPhamID).HasColumnName("ThanhPhamID");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.LoaiVai).HasColumnName("LoaiVai");
        }
    }
}
