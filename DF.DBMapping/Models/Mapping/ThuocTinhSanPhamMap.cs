using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ThuocTinhSanPhamMap : EntityTypeConfiguration<ThuocTinhSanPham>
    {
        public ThuocTinhSanPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.ThuocTinhSanPhamID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ThuocTinhSanPham");
            this.Property(t => t.ThuocTinhSanPhamID).HasColumnName("ThuocTinhSanPhamID");
            this.Property(t => t.TenThuocTinh).HasColumnName("TenThuocTinh");
            this.Property(t => t.KieuDuLieu).HasColumnName("KieuDuLieu");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
            this.Property(t => t.LoaiHinhSanXuat).HasColumnName("LoaiHinhSanXuat");
            this.Property(t => t.ViTri).HasColumnName("ViTri");
        }
    }
}
