using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class HinhAnhDonHangMap : EntityTypeConfiguration<HinhAnhDonHang>
    {
        public HinhAnhDonHangMap()
        {
            // Primary Key
            this.HasKey(t => t.HinhAnhDonHangID);

            // Properties
            this.Property(t => t.TenFile)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.FileMaHoa)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("HinhAnhDonHang");
            this.Property(t => t.HinhAnhDonHangID).HasColumnName("HinhAnhDonHangID");
            this.Property(t => t.TenFile).HasColumnName("TenFile");
            this.Property(t => t.FileMaHoa).HasColumnName("FileMaHoa");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
