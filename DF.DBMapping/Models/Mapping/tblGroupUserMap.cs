using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class tblGroupUserMap : EntityTypeConfiguration<tblGroupUser>
    {
        public tblGroupUserMap()
        {
            // Primary Key
            this.HasKey(t => new { t.GroupGuid, t.UserGuid });

            // Properties
            // Table & Column Mappings
            this.ToTable("tblGroupUser");
            this.Property(t => t.GroupGuid).HasColumnName("GroupGuid");
            this.Property(t => t.UserGuid).HasColumnName("UserGuid");
        }
    }
}
