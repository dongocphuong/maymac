using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class DMCongDoanThanhPhamMap : EntityTypeConfiguration<DMCongDoanThanhPham>
    {
        public DMCongDoanThanhPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.DMCongDoanThanhPhamID);

            // Properties
            // Table & Column Mappings
            this.ToTable("DMCongDoanThanhPham");
            this.Property(t => t.DMCongDoanThanhPhamID).HasColumnName("DMCongDoanThanhPhamID");
            this.Property(t => t.DMCongDoanHangMucID).HasColumnName("DMCongDoanHangMucID");
            this.Property(t => t.ThanhPhamID).HasColumnName("ThanhPhamID");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
