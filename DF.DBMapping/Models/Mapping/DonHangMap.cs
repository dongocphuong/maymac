using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class DonHangMap : EntityTypeConfiguration<DonHang>
    {
        public DonHangMap()
        {
            // Primary Key
            this.HasKey(t => t.DonHangID);

            // Properties
            this.Property(t => t.TenDonHang)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.MaDonHang)
                .HasMaxLength(50);

            this.Property(t => t.DiaDiemGiaoHang)
                .HasMaxLength(255);

            this.Property(t => t.EmailKeToan)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("DonHang");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.TenDonHang).HasColumnName("TenDonHang");
            this.Property(t => t.MaDonHang).HasColumnName("MaDonHang");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.TrangThai).HasColumnName("TrangThai");
            this.Property(t => t.DiaDiemGiaoHang).HasColumnName("DiaDiemGiaoHang");
            this.Property(t => t.ThoiGianThanhToan).HasColumnName("ThoiGianThanhToan");
            this.Property(t => t.SoTienDaThanhToan).HasColumnName("SoTienDaThanhToan");
            this.Property(t => t.LoaiHinhThanhToan).HasColumnName("LoaiHinhThanhToan");
            this.Property(t => t.YeuCauKhac).HasColumnName("YeuCauKhac");
            this.Property(t => t.HinhAnh).HasColumnName("HinhAnh");
            this.Property(t => t.CapNhatLanCuoi).HasColumnName("CapNhatLanCuoi");
            this.Property(t => t.YeuCauKhachHangID).HasColumnName("YeuCauKhachHangID");
            this.Property(t => t.NhanVienTaoID).HasColumnName("NhanVienTaoID");
            this.Property(t => t.EmailKeToan).HasColumnName("EmailKeToan");
            this.Property(t => t.ThoiGianDatCoc).HasColumnName("ThoiGianDatCoc");
            this.Property(t => t.ThoiGianTao).HasColumnName("ThoiGianTao");
            this.Property(t => t.ThoiGianDuKienSanXuat).HasColumnName("ThoiGianDuKienSanXuat");
            this.Property(t => t.ThoiGianDuKienGiaoHang).HasColumnName("ThoiGianDuKienGiaoHang");
        }
    }
}
