using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class NganHangMap : EntityTypeConfiguration<NganHang>
    {
        public NganHangMap()
        {
            // Primary Key
            this.HasKey(t => t.NganHangID);

            // Properties
            this.Property(t => t.TenNganHang)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("NganHang");
            this.Property(t => t.NganHangID).HasColumnName("NganHangID");
            this.Property(t => t.TenNganHang).HasColumnName("TenNganHang");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
