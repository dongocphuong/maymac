using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class PhieuXuatNguyenLieuMap : EntityTypeConfiguration<PhieuXuatNguyenLieu>
    {
        public PhieuXuatNguyenLieuMap()
        {
            // Primary Key
            this.HasKey(t => t.PhieuXuatNguyenLieuID);

            // Properties
            this.Property(t => t.MaPhieuXuat)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("PhieuXuatNguyenLieu");
            this.Property(t => t.PhieuXuatNguyenLieuID).HasColumnName("PhieuXuatNguyenLieuID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.NguoiTaoID).HasColumnName("NguoiTaoID");
            this.Property(t => t.MaPhieuXuat).HasColumnName("MaPhieuXuat");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.PhieuNhapNguyenLieuID).HasColumnName("PhieuNhapNguyenLieuID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.DonHangNhanID).HasColumnName("DonHangNhanID");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
            this.Property(t => t.DeNghiNguyenLieuID).HasColumnName("DeNghiNguyenLieuID");
        }
    }
}
