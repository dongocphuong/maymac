using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class UpdateManagerMap : EntityTypeConfiguration<UpdateManager>
    {
        public UpdateManagerMap()
        {
            // Primary Key
            this.HasKey(t => t.PlatformKey);

            // Properties
            this.Property(t => t.PlatformKey)
                .IsRequired()
                .HasMaxLength(50);

            this.Property(t => t.Url)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("UpdateManager");
            this.Property(t => t.PlatformKey).HasColumnName("PlatformKey");
            this.Property(t => t.VersionCode).HasColumnName("VersionCode");
            this.Property(t => t.Url).HasColumnName("Url");
            this.Property(t => t.ForceUpdate).HasColumnName("ForceUpdate");
        }
    }
}
