using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChiTietDonHangMap : EntityTypeConfiguration<ChiTietDonHang>
    {
        public ChiTietDonHangMap()
        {
            // Primary Key
            this.HasKey(t => t.ChiTietDonHangID);

            // Properties
            this.Property(t => t.TenCongDoan)
                .HasMaxLength(250);

            this.Property(t => t.Mau)
                .HasMaxLength(200);

            this.Property(t => t.GhiChu)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ChiTietDonHang");
            this.Property(t => t.ChiTietDonHangID).HasColumnName("ChiTietDonHangID");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.DonGia).HasColumnName("DonGia");
            this.Property(t => t.TrangThai).HasColumnName("TrangThai");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
            this.Property(t => t.SoLuongGiao).HasColumnName("SoLuongGiao");
            this.Property(t => t.TenCongDoan).HasColumnName("TenCongDoan");
            this.Property(t => t.Mau).HasColumnName("Mau");
            this.Property(t => t.GhiChu).HasColumnName("GhiChu");
            this.Property(t => t.TiLeSize).HasColumnName("TiLeSize");
        }
    }
}
