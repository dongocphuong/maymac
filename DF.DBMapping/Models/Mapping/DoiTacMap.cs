using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class DoiTacMap : EntityTypeConfiguration<DoiTac>
    {
        public DoiTacMap()
        {
            // Primary Key
            this.HasKey(t => t.DoiTacID);

            // Properties
            this.Property(t => t.TenDoiTac)
                .HasMaxLength(200);

            this.Property(t => t.DiaChi)
                .HasMaxLength(500);

            this.Property(t => t.SDT)
                .HasMaxLength(50);

            this.Property(t => t.MaDoiTac)
                .HasMaxLength(50);

            this.Property(t => t.Email)
                .HasMaxLength(250);

            this.Property(t => t.MaSoThue)
                .HasMaxLength(250);

            this.Property(t => t.SoTaiKhoan)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("DoiTac");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.TenDoiTac).HasColumnName("TenDoiTac");
            this.Property(t => t.KieuDoiTac).HasColumnName("KieuDoiTac");
            this.Property(t => t.NganHangID).HasColumnName("NganHangID");
            this.Property(t => t.DiaChi).HasColumnName("DiaChi");
            this.Property(t => t.SDT).HasColumnName("SDT");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.MaDoiTac).HasColumnName("MaDoiTac");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.MaSoThue).HasColumnName("MaSoThue");
            this.Property(t => t.SoTaiKhoan).HasColumnName("SoTaiKhoan");
        }
    }
}
