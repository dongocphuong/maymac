using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class LichSuSuaChiTietYeuCauMap : EntityTypeConfiguration<LichSuSuaChiTietYeuCau>
    {
        public LichSuSuaChiTietYeuCauMap()
        {
            // Primary Key
            this.HasKey(t => t.LichSuSuaChiTietYeuCau1);

            // Properties
            this.Property(t => t.TenSanPham)
                .HasMaxLength(500);

            this.Property(t => t.Size)
                .HasMaxLength(50);

            this.Property(t => t.Mau)
                .HasMaxLength(50);

            this.Property(t => t.YeuCauKhac)
                .HasMaxLength(4000);

            this.Property(t => t.DacDiem)
                .HasMaxLength(500);

            this.Property(t => t.ChatLieu)
                .HasMaxLength(500);

            this.Property(t => t.MauPhoi)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("LichSuSuaChiTietYeuCau");
            this.Property(t => t.LichSuSuaChiTietYeuCau1).HasColumnName("LichSuSuaChiTietYeuCau");
            this.Property(t => t.YeuCauKhachHangID).HasColumnName("YeuCauKhachHangID");
            this.Property(t => t.TenSanPham).HasColumnName("TenSanPham");
            this.Property(t => t.Size).HasColumnName("Size");
            this.Property(t => t.Mau).HasColumnName("Mau");
            this.Property(t => t.SoLuongDat).HasColumnName("SoLuongDat");
            this.Property(t => t.GiaBan).HasColumnName("GiaBan");
            this.Property(t => t.YeuCauKhac).HasColumnName("YeuCauKhac");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.NguoiSuaID).HasColumnName("NguoiSuaID");
            this.Property(t => t.Times).HasColumnName("Times");
            this.Property(t => t.DacDiem).HasColumnName("DacDiem");
            this.Property(t => t.ChatLieu).HasColumnName("ChatLieu");
            this.Property(t => t.MauPhoi).HasColumnName("MauPhoi");
            this.Property(t => t.ThoiGianGiaoHang).HasColumnName("ThoiGianGiaoHang");
        }
    }
}
