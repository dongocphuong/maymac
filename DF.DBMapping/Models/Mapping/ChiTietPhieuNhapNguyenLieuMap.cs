using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChiTietPhieuNhapNguyenLieuMap : EntityTypeConfiguration<ChiTietPhieuNhapNguyenLieu>
    {
        public ChiTietPhieuNhapNguyenLieuMap()
        {
            // Primary Key
            this.HasKey(t => t.ChiTietPhieuNhapNguyenLieuID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ChiTietPhieuNhapNguyenLieu");
            this.Property(t => t.ChiTietPhieuNhapNguyenLieuID).HasColumnName("ChiTietPhieuNhapNguyenLieuID");
            this.Property(t => t.PhieuNhapNguyenLieuID).HasColumnName("PhieuNhapNguyenLieuID");
            this.Property(t => t.NguyenLieuID).HasColumnName("NguyenLieuID");
            this.Property(t => t.GiaNhap).HasColumnName("GiaNhap");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.DonViID).HasColumnName("DonViID");
        }
    }
}
