using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class YeuCauKhachHangMap : EntityTypeConfiguration<YeuCauKhachHang>
    {
        public YeuCauKhachHangMap()
        {
            // Primary Key
            this.HasKey(t => t.YeuCauKhachHangID);

            // Properties
            this.Property(t => t.NoiDung)
                .IsRequired();

            this.Property(t => t.MaYeuCau)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("YeuCauKhachHang");
            this.Property(t => t.YeuCauKhachHangID).HasColumnName("YeuCauKhachHangID");
            this.Property(t => t.NoiDung).HasColumnName("NoiDung");
            this.Property(t => t.HinhAnh).HasColumnName("HinhAnh");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.LichSuDuyet).HasColumnName("LichSuDuyet");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.TinhTrang).HasColumnName("TinhTrang");
            this.Property(t => t.LoaiHinhSanXuat).HasColumnName("LoaiHinhSanXuat");
            this.Property(t => t.NguoiTaoID).HasColumnName("NguoiTaoID");
            this.Property(t => t.NoiDung1).HasColumnName("NoiDung1");
            this.Property(t => t.MaYeuCau).HasColumnName("MaYeuCau");
            this.Property(t => t.STT).HasColumnName("STT");
            this.Property(t => t.GiaBan).HasColumnName("GiaBan");
            this.Property(t => t.SoLuongDat).HasColumnName("SoLuongDat");
            this.Property(t => t.ThoiGianGiaoHang).HasColumnName("ThoiGianGiaoHang");
        }
    }
}
