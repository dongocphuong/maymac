using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class PhieuNhapNguyenLieuMap : EntityTypeConfiguration<PhieuNhapNguyenLieu>
    {
        public PhieuNhapNguyenLieuMap()
        {
            // Primary Key
            this.HasKey(t => t.PhieuNhapNguyenLieuID);

            // Properties
            this.Property(t => t.MaPhieuNhap)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("PhieuNhapNguyenLieu");
            this.Property(t => t.PhieuNhapNguyenLieuID).HasColumnName("PhieuNhapNguyenLieuID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.NguoiTaoID).HasColumnName("NguoiTaoID");
            this.Property(t => t.MaPhieuNhap).HasColumnName("MaPhieuNhap");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.PhieuXuatNguyenLieuID).HasColumnName("PhieuXuatNguyenLieuID");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
            this.Property(t => t.DeNghiNguyenLieuID).HasColumnName("DeNghiNguyenLieuID");
        }
    }
}
