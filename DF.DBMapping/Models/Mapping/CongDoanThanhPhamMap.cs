using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class CongDoanThanhPhamMap : EntityTypeConfiguration<CongDoanThanhPham>
    {
        public CongDoanThanhPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.CongDoanThanhPhamID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CongDoanThanhPham");
            this.Property(t => t.CongDoanThanhPhamID).HasColumnName("CongDoanThanhPhamID");
            this.Property(t => t.CongDoanHangMucID).HasColumnName("CongDoanHangMucID");
            this.Property(t => t.ThanhPhamID).HasColumnName("ThanhPhamID");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
