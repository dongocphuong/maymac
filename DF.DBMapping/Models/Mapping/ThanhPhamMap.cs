using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ThanhPhamMap : EntityTypeConfiguration<ThanhPham>
    {
        public ThanhPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.ThanhPhamID);

            // Properties
            this.Property(t => t.TenThanhPham)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.MaThanhPham)
                .HasMaxLength(200);

            this.Property(t => t.DanhSachHinhAnhs)
                .HasMaxLength(500);

            this.Property(t => t.AnhDaiDien)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("ThanhPham");
            this.Property(t => t.ThanhPhamID).HasColumnName("ThanhPhamID");
            this.Property(t => t.TenThanhPham).HasColumnName("TenThanhPham");
            this.Property(t => t.MaThanhPham).HasColumnName("MaThanhPham");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
            this.Property(t => t.DonViID).HasColumnName("DonViID");
            this.Property(t => t.AnhDaiDien).HasColumnName("AnhDaiDien");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.DonGia).HasColumnName("DonGia");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
        }
    }
}
