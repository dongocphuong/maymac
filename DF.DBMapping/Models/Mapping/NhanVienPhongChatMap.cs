using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class NhanVienPhongChatMap : EntityTypeConfiguration<NhanVienPhongChat>
    {
        public NhanVienPhongChatMap()
        {
            // Primary Key
            this.HasKey(t => t.NhanVienPhongChatID);

            // Properties
            // Table & Column Mappings
            this.ToTable("NhanVienPhongChat");
            this.Property(t => t.NhanVienPhongChatID).HasColumnName("NhanVienPhongChatID");
            this.Property(t => t.NhanVienID).HasColumnName("NhanVienID");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.ChucVuPhongChatID).HasColumnName("ChucVuPhongChatID");
        }
    }
}
