using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChiTietPhieuNhanDonHangMap : EntityTypeConfiguration<ChiTietPhieuNhanDonHang>
    {
        public ChiTietPhieuNhanDonHangMap()
        {
            // Primary Key
            this.HasKey(t => t.ChiTietNhapDonHangID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ChiTietPhieuNhanDonHang");
            this.Property(t => t.ChiTietNhapDonHangID).HasColumnName("ChiTietNhapDonHangID");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.DonGia).HasColumnName("DonGia");
            this.Property(t => t.TinhTrang).HasColumnName("TinhTrang");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
            this.Property(t => t.PhieuNhanDonHang).HasColumnName("PhieuNhanDonHang");
        }
    }
}
