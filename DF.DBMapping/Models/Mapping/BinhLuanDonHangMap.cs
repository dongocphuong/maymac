using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class BinhLuanDonHangMap : EntityTypeConfiguration<BinhLuanDonHang>
    {
        public BinhLuanDonHangMap()
        {
            // Primary Key
            this.HasKey(t => t.BinhLuanDonHangID);

            // Properties
            this.Property(t => t.DanhSachHinhAnhs)
                .HasMaxLength(500);

            this.Property(t => t.NoiDung)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("BinhLuanDonHang");
            this.Property(t => t.BinhLuanDonHangID).HasColumnName("BinhLuanDonHangID");
            this.Property(t => t.YeuCauDonHangID).HasColumnName("YeuCauDonHangID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
            this.Property(t => t.NhanVienID).HasColumnName("NhanVienID");
            this.Property(t => t.NoiDung).HasColumnName("NoiDung");
            this.Property(t => t.YeuCauKhachHangID).HasColumnName("YeuCauKhachHangID");
        }
    }
}
