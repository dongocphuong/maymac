using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ThanhToanTienMap : EntityTypeConfiguration<ThanhToanTien>
    {
        public ThanhToanTienMap()
        {
            // Primary Key
            this.HasKey(t => t.ThanhToanTienID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ThanhToanTien");
            this.Property(t => t.ThanhToanTienID).HasColumnName("ThanhToanTienID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.NoiDung).HasColumnName("NoiDung");
            this.Property(t => t.NhanVienID).HasColumnName("NhanVienID");
            this.Property(t => t.SoTien).HasColumnName("SoTien");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
        }
    }
}
