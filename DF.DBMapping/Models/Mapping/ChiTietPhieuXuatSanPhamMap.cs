using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChiTietPhieuXuatSanPhamMap : EntityTypeConfiguration<ChiTietPhieuXuatSanPham>
    {
        public ChiTietPhieuXuatSanPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.ChiTietPhieuXuatSanPhamID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ChiTietPhieuXuatSanPham");
            this.Property(t => t.ChiTietPhieuXuatSanPhamID).HasColumnName("ChiTietPhieuXuatSanPhamID");
            this.Property(t => t.PhieuXuatSanPhamID).HasColumnName("PhieuXuatSanPhamID");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.TinhTrang).HasColumnName("TinhTrang");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
        }
    }
}
