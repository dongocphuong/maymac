using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class SanPhamMap : EntityTypeConfiguration<SanPham>
    {
        public SanPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.SanPhamID);

            // Properties
            this.Property(t => t.TenSanPham)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.MaSanPham)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.DanhSachHinhAnhs)
                .HasMaxLength(500);

            this.Property(t => t.AnhDaiDien)
                .HasMaxLength(500);

            this.Property(t => t.Size)
                .HasMaxLength(50);

            this.Property(t => t.SizeMauGoc)
                .HasMaxLength(50);

            this.Property(t => t.TiLeSize)
                .HasMaxLength(200);

            this.Property(t => t.Mau)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("SanPham");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
            this.Property(t => t.TenSanPham).HasColumnName("TenSanPham");
            this.Property(t => t.MaSanPham).HasColumnName("MaSanPham");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
            this.Property(t => t.DonViID).HasColumnName("DonViID");
            this.Property(t => t.AnhDaiDien).HasColumnName("AnhDaiDien");
            this.Property(t => t.DonGia).HasColumnName("DonGia");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.Size).HasColumnName("Size");
            this.Property(t => t.SizeMauGoc).HasColumnName("SizeMauGoc");
            this.Property(t => t.TiLeSize).HasColumnName("TiLeSize");
            this.Property(t => t.MauMayID).HasColumnName("MauMayID");
            this.Property(t => t.Mau).HasColumnName("Mau");
            this.Property(t => t.Loai).HasColumnName("Loai");
            this.Property(t => t.GioiTinh).HasColumnName("GioiTinh");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.LoaiHinhSanXuat).HasColumnName("LoaiHinhSanXuat");
        }
    }
}
