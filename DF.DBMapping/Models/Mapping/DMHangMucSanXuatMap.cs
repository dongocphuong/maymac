using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class DMHangMucSanXuatMap : EntityTypeConfiguration<DMHangMucSanXuat>
    {
        public DMHangMucSanXuatMap()
        {
            // Primary Key
            this.HasKey(t => t.DMHangMucSanXuatID);

            // Properties
            // Table & Column Mappings
            this.ToTable("DMHangMucSanXuat");
            this.Property(t => t.DMHangMucSanXuatID).HasColumnName("DMHangMucSanXuatID");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
            this.Property(t => t.GiaTriDinhMuc).HasColumnName("GiaTriDinhMuc");
            this.Property(t => t.HaoHut).HasColumnName("HaoHut");
            this.Property(t => t.ThoiGianHoanThanh).HasColumnName("ThoiGianHoanThanh");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.ViTri).HasColumnName("ViTri");
            this.Property(t => t.MoTa).HasColumnName("MoTa");
            this.Property(t => t.GiaTri).HasColumnName("GiaTri");
            this.Property(t => t.DsHinhAnh).HasColumnName("DsHinhAnh");
        }
    }
}
