using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChiTietPhieuXuatNguyenLieuMap : EntityTypeConfiguration<ChiTietPhieuXuatNguyenLieu>
    {
        public ChiTietPhieuXuatNguyenLieuMap()
        {
            // Primary Key
            this.HasKey(t => t.ChiTietPhieuXuatNguyenLieuID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ChiTietPhieuXuatNguyenLieu");
            this.Property(t => t.ChiTietPhieuXuatNguyenLieuID).HasColumnName("ChiTietPhieuXuatNguyenLieuID");
            this.Property(t => t.PhieuXuatNguyenLieu).HasColumnName("PhieuXuatNguyenLieu");
            this.Property(t => t.NguyenLieuID).HasColumnName("NguyenLieuID");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
