using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class MauMayMap : EntityTypeConfiguration<MauMay>
    {
        public MauMayMap()
        {
            // Primary Key
            this.HasKey(t => t.MauMayID);

            // Properties
            this.Property(t => t.MaMauMay)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.TenMauMay)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.DanhSachHinhAnhs)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("MauMay");
            this.Property(t => t.MauMayID).HasColumnName("MauMayID");
            this.Property(t => t.YeuCauKhachHangID).HasColumnName("YeuCauKhachHangID");
            this.Property(t => t.MaMauMay).HasColumnName("MaMauMay");
            this.Property(t => t.TenMauMay).HasColumnName("TenMauMay");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.LoaiHinhSanXuat).HasColumnName("LoaiHinhSanXuat");
        }
    }
}
