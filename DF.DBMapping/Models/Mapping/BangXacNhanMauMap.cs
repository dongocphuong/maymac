using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class BangXacNhanMauMap : EntityTypeConfiguration<BangXacNhanMau>
    {
        public BangXacNhanMauMap()
        {
            // Primary Key
            this.HasKey(t => t.BangXacNhanMauID);

            // Properties
            this.Property(t => t.CapMay)
                .HasMaxLength(50);

            this.Property(t => t.NhanVienThietKe)
                .HasMaxLength(500);

            this.Property(t => t.ThietKeLan)
                .HasMaxLength(50);

            this.Property(t => t.LenNen)
                .HasMaxLength(500);

            this.Property(t => t.LenNenF1)
                .HasMaxLength(500);

            this.Property(t => t.LenNenF2)
                .HasMaxLength(500);

            this.Property(t => t.LenNenF3)
                .HasMaxLength(500);

            this.Property(t => t.LyNen)
                .HasMaxLength(500);

            this.Property(t => t.LyNenF1)
                .HasMaxLength(500);

            this.Property(t => t.LyNenF2)
                .HasMaxLength(500);

            this.Property(t => t.LyNenF3)
                .HasMaxLength(500);

            this.Property(t => t.LyNenNep)
                .HasMaxLength(500);

            this.Property(t => t.LyNenTong)
                .HasMaxLength(500);

            this.Property(t => t.TT)
                .HasMaxLength(100);

            this.Property(t => t.TS)
                .HasMaxLength(100);

            this.Property(t => t.Tay)
                .HasMaxLength(100);

            this.Property(t => t.Co)
                .HasMaxLength(100);

            this.Property(t => t.Nep)
                .HasMaxLength(100);

            this.Property(t => t.Tong)
                .HasMaxLength(100);

            this.Property(t => t.MatDo1)
                .HasMaxLength(200);

            this.Property(t => t.MatDo2)
                .HasMaxLength(200);

            this.Property(t => t.MatDo3)
                .HasMaxLength(200);

            this.Property(t => t.TocDoTK)
                .HasMaxLength(200);

            this.Property(t => t.TocDoMay)
                .HasMaxLength(200);

            this.Property(t => t.Mo1)
                .HasMaxLength(200);

            this.Property(t => t.Mo2)
                .HasMaxLength(200);

            this.Property(t => t.Mo3)
                .HasMaxLength(200);

            this.Property(t => t.Mo4)
                .HasMaxLength(200);

            this.Property(t => t.Mo5)
                .HasMaxLength(200);

            this.Property(t => t.GhiChu1)
                .HasMaxLength(200);

            this.Property(t => t.LK)
                .HasMaxLength(200);

            this.Property(t => t.VS)
                .HasMaxLength(200);

            this.Property(t => t.GM)
                .HasMaxLength(200);

            this.Property(t => t.LingkingNep)
                .HasMaxLength(200);

            this.Property(t => t.LingkingTong)
                .HasMaxLength(200);

            this.Property(t => t.GhiChu2)
                .HasMaxLength(200);

            this.Property(t => t.GhiChu3)
                .HasMaxLength(200);

            this.Property(t => t.GhiChu4)
                .HasMaxLength(200);

            this.Property(t => t.GhiChu5)
                .HasMaxLength(200);

            this.Property(t => t.May1)
                .HasMaxLength(200);

            this.Property(t => t.May2)
                .HasMaxLength(200);

            this.Property(t => t.May3)
                .HasMaxLength(200);

            this.Property(t => t.GiatSay1)
                .HasMaxLength(200);

            this.Property(t => t.GiatSay2)
                .HasMaxLength(200);

            this.Property(t => t.GiatSay3)
                .HasMaxLength(200);

            this.Property(t => t.DuyetMau)
                .HasMaxLength(200);

            this.Property(t => t.NhanXet1)
                .HasMaxLength(200);

            this.Property(t => t.NhanXet2)
                .HasMaxLength(200);

            this.Property(t => t.NhanXet3)
                .HasMaxLength(200);

            this.Property(t => t.TrongLuongPhoiThanhPham)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("BangXacNhanMau");
            this.Property(t => t.BangXacNhanMauID).HasColumnName("BangXacNhanMauID");
            this.Property(t => t.MauMayID).HasColumnName("MauMayID");
            this.Property(t => t.CapMay).HasColumnName("CapMay");
            this.Property(t => t.NhanVienThietKe).HasColumnName("NhanVienThietKe");
            this.Property(t => t.ThietKeLan).HasColumnName("ThietKeLan");
            this.Property(t => t.LenNen).HasColumnName("LenNen");
            this.Property(t => t.LenNenF1).HasColumnName("LenNenF1");
            this.Property(t => t.LenNenF2).HasColumnName("LenNenF2");
            this.Property(t => t.LenNenF3).HasColumnName("LenNenF3");
            this.Property(t => t.LyNen).HasColumnName("LyNen");
            this.Property(t => t.LyNenF1).HasColumnName("LyNenF1");
            this.Property(t => t.LyNenF2).HasColumnName("LyNenF2");
            this.Property(t => t.LyNenF3).HasColumnName("LyNenF3");
            this.Property(t => t.LyNenNep).HasColumnName("LyNenNep");
            this.Property(t => t.LyNenTong).HasColumnName("LyNenTong");
            this.Property(t => t.TT).HasColumnName("TT");
            this.Property(t => t.TS).HasColumnName("TS");
            this.Property(t => t.Tay).HasColumnName("Tay");
            this.Property(t => t.Co).HasColumnName("Co");
            this.Property(t => t.Nep).HasColumnName("Nep");
            this.Property(t => t.Tong).HasColumnName("Tong");
            this.Property(t => t.MatDo1).HasColumnName("MatDo1");
            this.Property(t => t.MatDo2).HasColumnName("MatDo2");
            this.Property(t => t.MatDo3).HasColumnName("MatDo3");
            this.Property(t => t.TocDoTK).HasColumnName("TocDoTK");
            this.Property(t => t.TocDoMay).HasColumnName("TocDoMay");
            this.Property(t => t.Mo1).HasColumnName("Mo1");
            this.Property(t => t.Mo2).HasColumnName("Mo2");
            this.Property(t => t.Mo3).HasColumnName("Mo3");
            this.Property(t => t.Mo4).HasColumnName("Mo4");
            this.Property(t => t.Mo5).HasColumnName("Mo5");
            this.Property(t => t.GhiChu1).HasColumnName("GhiChu1");
            this.Property(t => t.LK).HasColumnName("LK");
            this.Property(t => t.VS).HasColumnName("VS");
            this.Property(t => t.GM).HasColumnName("GM");
            this.Property(t => t.LingkingNep).HasColumnName("LingkingNep");
            this.Property(t => t.LingkingTong).HasColumnName("LingkingTong");
            this.Property(t => t.GhiChu2).HasColumnName("GhiChu2");
            this.Property(t => t.GhiChu3).HasColumnName("GhiChu3");
            this.Property(t => t.GhiChu4).HasColumnName("GhiChu4");
            this.Property(t => t.GhiChu5).HasColumnName("GhiChu5");
            this.Property(t => t.May1).HasColumnName("May1");
            this.Property(t => t.May2).HasColumnName("May2");
            this.Property(t => t.May3).HasColumnName("May3");
            this.Property(t => t.GiatSay1).HasColumnName("GiatSay1");
            this.Property(t => t.GiatSay2).HasColumnName("GiatSay2");
            this.Property(t => t.GiatSay3).HasColumnName("GiatSay3");
            this.Property(t => t.DuyetMau).HasColumnName("DuyetMau");
            this.Property(t => t.NhanXet1).HasColumnName("NhanXet1");
            this.Property(t => t.NhanXet2).HasColumnName("NhanXet2");
            this.Property(t => t.NhanXet3).HasColumnName("NhanXet3");
            this.Property(t => t.TrongLuongPhoiThanhPham).HasColumnName("TrongLuongPhoiThanhPham");
        }
    }
}
