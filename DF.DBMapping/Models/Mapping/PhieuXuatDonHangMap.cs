using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class PhieuXuatDonHangMap : EntityTypeConfiguration<PhieuXuatDonHang>
    {
        public PhieuXuatDonHangMap()
        {
            // Primary Key
            this.HasKey(t => t.PhieuXuatDonHangID);

            // Properties
            this.Property(t => t.DanhSachHinhAnhs)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("PhieuXuatDonHang");
            this.Property(t => t.PhieuXuatDonHangID).HasColumnName("PhieuXuatDonHangID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.NhanVienID).HasColumnName("NhanVienID");
            this.Property(t => t.NguoiNhanID).HasColumnName("NguoiNhanID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.TrangThaiDuyet).HasColumnName("TrangThaiDuyet");
            this.Property(t => t.ThoiGianDuyet).HasColumnName("ThoiGianDuyet");
            this.Property(t => t.NguoiDuyetID).HasColumnName("NguoiDuyetID");
            this.Property(t => t.NoiDung).HasColumnName("NoiDung");
        }
    }
}
