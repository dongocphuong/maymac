using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class PhieuNhapThanhPhamMap : EntityTypeConfiguration<PhieuNhapThanhPham>
    {
        public PhieuNhapThanhPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.PhieuNhapThanhPhamID);

            // Properties
            this.Property(t => t.MaPhieuNhap)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("PhieuNhapThanhPham");
            this.Property(t => t.PhieuNhapThanhPhamID).HasColumnName("PhieuNhapThanhPhamID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.NguoiTaoID).HasColumnName("NguoiTaoID");
            this.Property(t => t.MaPhieuNhap).HasColumnName("MaPhieuNhap");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.PhieuXuatThanhPhamID).HasColumnName("PhieuXuatThanhPhamID");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
        }
    }
}
