using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class CongDoanSanPhamDinhNghiaMap : EntityTypeConfiguration<CongDoanSanPhamDinhNghia>
    {
        public CongDoanSanPhamDinhNghiaMap()
        {
            // Primary Key
            this.HasKey(t => t.CongDoanSanPhamDinhNghiaID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CongDoanSanPhamDinhNghia");
            this.Property(t => t.CongDoanSanPhamDinhNghiaID).HasColumnName("CongDoanSanPhamDinhNghiaID");
            this.Property(t => t.QuanLySanXuatID).HasColumnName("QuanLySanXuatID");
            this.Property(t => t.ThanhPhamID).HasColumnName("ThanhPhamID");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
        }
    }
}
