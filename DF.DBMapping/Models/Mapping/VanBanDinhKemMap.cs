using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class VanBanDinhKemMap : EntityTypeConfiguration<VanBanDinhKem>
    {
        public VanBanDinhKemMap()
        {
            // Primary Key
            this.HasKey(t => t.VanBanID);

            // Properties
            this.Property(t => t.TieuDe)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.NoiDung)
                .HasMaxLength(2000);

            this.Property(t => t.FileDinhKem)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("VanBanDinhKem");
            this.Property(t => t.VanBanID).HasColumnName("VanBanID");
            this.Property(t => t.TieuDe).HasColumnName("TieuDe");
            this.Property(t => t.LoaiVanBan).HasColumnName("LoaiVanBan");
            this.Property(t => t.NoiDung).HasColumnName("NoiDung");
            this.Property(t => t.FileDinhKem).HasColumnName("FileDinhKem");
            this.Property(t => t.NguoiTaoID).HasColumnName("NguoiTaoID");
            this.Property(t => t.ThoiGianTao).HasColumnName("ThoiGianTao");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
