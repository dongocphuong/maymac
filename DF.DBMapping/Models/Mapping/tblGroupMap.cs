using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class tblGroupMap : EntityTypeConfiguration<tblGroup>
    {
        public tblGroupMap()
        {
            // Primary Key
            this.HasKey(t => t.GroupGuid);

            // Properties
            this.Property(t => t.GroupName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.UrlLink)
                .HasMaxLength(250);

            this.Property(t => t.KeyLanguage)
                .HasMaxLength(150);

            this.Property(t => t.MoTa)
                .HasMaxLength(50);

            this.Property(t => t.Icon)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("tblGroup");
            this.Property(t => t.GroupGuid).HasColumnName("GroupGuid");
            this.Property(t => t.GroupName).HasColumnName("GroupName");
            this.Property(t => t.RuleType).HasColumnName("RuleType");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.UrlLink).HasColumnName("UrlLink");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
            this.Property(t => t.Nhom).HasColumnName("Nhom");
            this.Property(t => t.GroupParent).HasColumnName("GroupParent");
            this.Property(t => t.KeyLanguage).HasColumnName("KeyLanguage");
            this.Property(t => t.MoTa).HasColumnName("MoTa");
            this.Property(t => t.Icon).HasColumnName("Icon");
            this.Property(t => t.IsHeader).HasColumnName("IsHeader");
            this.Property(t => t.STT).HasColumnName("STT");
        }
    }
}
