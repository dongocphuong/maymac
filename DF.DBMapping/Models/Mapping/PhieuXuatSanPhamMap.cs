using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class PhieuXuatSanPhamMap : EntityTypeConfiguration<PhieuXuatSanPham>
    {
        public PhieuXuatSanPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.PhieuXuatSanPhamID);

            // Properties
            this.Property(t => t.MaPhieuXuat)
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("PhieuXuatSanPham");
            this.Property(t => t.PhieuXuatSanPhamID).HasColumnName("PhieuXuatSanPhamID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.NguoiTaoID).HasColumnName("NguoiTaoID");
            this.Property(t => t.MaPhieuXuat).HasColumnName("MaPhieuXuat");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.PhieuNhapSanPhamID).HasColumnName("PhieuNhapSanPhamID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.DonHangNhanID).HasColumnName("DonHangNhanID");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
        }
    }
}
