using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class PhongBanMap : EntityTypeConfiguration<PhongBan>
    {
        public PhongBanMap()
        {
            // Primary Key
            this.HasKey(t => t.PhongBanID);

            // Properties
            this.Property(t => t.TenPhong)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("PhongBan");
            this.Property(t => t.PhongBanID).HasColumnName("PhongBanID");
            this.Property(t => t.TenPhong).HasColumnName("TenPhong");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
