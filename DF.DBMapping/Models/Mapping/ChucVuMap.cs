using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChucVuMap : EntityTypeConfiguration<ChucVu>
    {
        public ChucVuMap()
        {
            // Primary Key
            this.HasKey(t => t.ChucVuId);

            // Properties
            this.Property(t => t.TenChucVu)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ChucVu");
            this.Property(t => t.ChucVuId).HasColumnName("ChucVuId");
            this.Property(t => t.MaChucVu).HasColumnName("MaChucVu");
            this.Property(t => t.TenChucVu).HasColumnName("TenChucVu");
            this.Property(t => t.CapDo).HasColumnName("CapDo");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
