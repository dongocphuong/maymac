using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class PhieuBaoGiaDonHangMap : EntityTypeConfiguration<PhieuBaoGiaDonHang>
    {
        public PhieuBaoGiaDonHangMap()
        {
            // Primary Key
            this.HasKey(t => t.PhieuBaoGiaDonHangID);

            // Properties
            this.Property(t => t.NoiDung)
                .IsRequired();

            this.Property(t => t.DanhSachHinhAnhs)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("PhieuBaoGiaDonHang");
            this.Property(t => t.PhieuBaoGiaDonHangID).HasColumnName("PhieuBaoGiaDonHangID");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.NhanVienBaoGiaID).HasColumnName("NhanVienBaoGiaID");
            this.Property(t => t.NoiDung).HasColumnName("NoiDung");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
        }
    }
}
