using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class DMCongDoanHangMucMap : EntityTypeConfiguration<DMCongDoanHangMuc>
    {
        public DMCongDoanHangMucMap()
        {
            // Primary Key
            this.HasKey(t => t.DMCongDoanHangMucID);

            // Properties
            this.Property(t => t.TenCongDoan)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("DMCongDoanHangMuc");
            this.Property(t => t.DMCongDoanHangMucID).HasColumnName("DMCongDoanHangMucID");
            this.Property(t => t.TenCongDoan).HasColumnName("TenCongDoan");
            this.Property(t => t.DMHangMucSanXuatID).HasColumnName("DMHangMucSanXuatID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
