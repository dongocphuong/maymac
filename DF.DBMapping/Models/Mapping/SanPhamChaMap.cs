using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class SanPhamChaMap : EntityTypeConfiguration<SanPhamCha>
    {
        public SanPhamChaMap()
        {
            // Primary Key
            this.HasKey(t => t.SanPhamChaID);

            // Properties
            this.Property(t => t.MaSanPham)
                .HasMaxLength(50);

            this.Property(t => t.TenSanPham)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("SanPhamCha");
            this.Property(t => t.SanPhamChaID).HasColumnName("SanPhamChaID");
            this.Property(t => t.MaSanPham).HasColumnName("MaSanPham");
            this.Property(t => t.TenSanPham).HasColumnName("TenSanPham");
            this.Property(t => t.GioiTinh).HasColumnName("GioiTinh");
            this.Property(t => t.Loai).HasColumnName("Loai");
            this.Property(t => t.AnhDaiHien).HasColumnName("AnhDaiHien");
            this.Property(t => t.DanhSachHinhAnh).HasColumnName("DanhSachHinhAnh");
        }
    }
}
