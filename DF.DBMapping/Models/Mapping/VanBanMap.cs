using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class VanBanMap : EntityTypeConfiguration<VanBan>
    {
        public VanBanMap()
        {
            // Primary Key
            this.HasKey(t => t.VanBanID);

            // Properties
            // Table & Column Mappings
            this.ToTable("VanBan");
            this.Property(t => t.VanBanID).HasColumnName("VanBanID");
            this.Property(t => t.TenVanBan).HasColumnName("TenVanBan");
            this.Property(t => t.PhongBanID).HasColumnName("PhongBanID");
        }
    }
}
