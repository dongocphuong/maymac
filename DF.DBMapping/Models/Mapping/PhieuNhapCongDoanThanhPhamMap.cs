using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class PhieuNhapCongDoanThanhPhamMap : EntityTypeConfiguration<PhieuNhapCongDoanThanhPham>
    {
        public PhieuNhapCongDoanThanhPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.PhieuNhapCongDoanThanhPhamID);

            // Properties
            // Table & Column Mappings
            this.ToTable("PhieuNhapCongDoanThanhPham");
            this.Property(t => t.PhieuNhapCongDoanThanhPhamID).HasColumnName("PhieuNhapCongDoanThanhPhamID");
            this.Property(t => t.CongDoanThanhPhamID).HasColumnName("CongDoanThanhPhamID");
            this.Property(t => t.PhieuNhapThanhPhamID).HasColumnName("PhieuNhapThanhPhamID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
