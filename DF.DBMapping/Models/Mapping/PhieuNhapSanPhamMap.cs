using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class PhieuNhapSanPhamMap : EntityTypeConfiguration<PhieuNhapSanPham>
    {
        public PhieuNhapSanPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.PhieuNhapSanPhamID);

            // Properties
            this.Property(t => t.MaPhieuNhap)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("PhieuNhapSanPham");
            this.Property(t => t.PhieuNhapSanPhamID).HasColumnName("PhieuNhapSanPhamID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.NguoiTaoID).HasColumnName("NguoiTaoID");
            this.Property(t => t.MaPhieuNhap).HasColumnName("MaPhieuNhap");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.PhieuXuatSanPhamID).HasColumnName("PhieuXuatSanPhamID");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
        }
    }
}
