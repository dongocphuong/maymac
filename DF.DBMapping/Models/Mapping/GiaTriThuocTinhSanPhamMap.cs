using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class GiaTriThuocTinhSanPhamMap : EntityTypeConfiguration<GiaTriThuocTinhSanPham>
    {
        public GiaTriThuocTinhSanPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.GiaTriThuocTinhID);

            // Properties
            // Table & Column Mappings
            this.ToTable("GiaTriThuocTinhSanPham");
            this.Property(t => t.GiaTriThuocTinhID).HasColumnName("GiaTriThuocTinhID");
            this.Property(t => t.ThuocTinhSanPhamID).HasColumnName("ThuocTinhSanPhamID");
            this.Property(t => t.GiaTri).HasColumnName("GiaTri");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
        }
    }
}
