using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class TrangThaiPhongChatMessageMap : EntityTypeConfiguration<TrangThaiPhongChatMessage>
    {
        public TrangThaiPhongChatMessageMap()
        {
            // Primary Key
            this.HasKey(t => t.TrangThaiPhongChatMessageID);

            // Properties
            // Table & Column Mappings
            this.ToTable("TrangThaiPhongChatMessage");
            this.Property(t => t.TrangThaiPhongChatMessageID).HasColumnName("TrangThaiPhongChatMessageID");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.LastRead).HasColumnName("LastRead");
            this.Property(t => t.NhanVienID).HasColumnName("NhanVienID");
        }
    }
}
