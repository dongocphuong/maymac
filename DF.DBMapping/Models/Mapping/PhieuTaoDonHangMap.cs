using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class PhieuTaoDonHangMap : EntityTypeConfiguration<PhieuTaoDonHang>
    {
        public PhieuTaoDonHangMap()
        {
            // Primary Key
            this.HasKey(t => t.PhieuTaoDonHangID);

            // Properties
            this.Property(t => t.DanhSachHinhAnhs)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("PhieuTaoDonHang");
            this.Property(t => t.PhieuTaoDonHangID).HasColumnName("PhieuTaoDonHangID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.TrangThaiDuyet).HasColumnName("TrangThaiDuyet");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.NoiDung).HasColumnName("NoiDung");
            this.Property(t => t.DoiTacID).HasColumnName("DoiTacID");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
            this.Property(t => t.NguoiDuyetID).HasColumnName("NguoiDuyetID");
            this.Property(t => t.ThoiGianDuyet).HasColumnName("ThoiGianDuyet");
        }
    }
}
