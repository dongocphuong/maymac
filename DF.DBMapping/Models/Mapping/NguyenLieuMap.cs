using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class NguyenLieuMap : EntityTypeConfiguration<NguyenLieu>
    {
        public NguyenLieuMap()
        {
            // Primary Key
            this.HasKey(t => t.NguyenLieuID);

            // Properties
            this.Property(t => t.TenNguyenLieu)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.MaNguyenLieu)
                .HasMaxLength(200);

            this.Property(t => t.DanhSachHinhAnhs)
                .HasMaxLength(500);

            this.Property(t => t.AnhDaiDien)
                .HasMaxLength(200);

            this.Property(t => t.MaMau)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("NguyenLieu");
            this.Property(t => t.NguyenLieuID).HasColumnName("NguyenLieuID");
            this.Property(t => t.TenNguyenLieu).HasColumnName("TenNguyenLieu");
            this.Property(t => t.MaNguyenLieu).HasColumnName("MaNguyenLieu");
            this.Property(t => t.DanhSachHinhAnhs).HasColumnName("DanhSachHinhAnhs");
            this.Property(t => t.DonViID).HasColumnName("DonViID");
            this.Property(t => t.AnhDaiDien).HasColumnName("AnhDaiDien");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.DonGia).HasColumnName("DonGia");
            this.Property(t => t.HeSoQuyDoi).HasColumnName("HeSoQuyDoi");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.NhomNguyenLieuID).HasColumnName("NhomNguyenLieuID");
            this.Property(t => t.MaMau).HasColumnName("MaMau");
        }
    }
}
