using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class HinhAnhVanBanMap : EntityTypeConfiguration<HinhAnhVanBan>
    {
        public HinhAnhVanBanMap()
        {
            // Primary Key
            this.HasKey(t => t.HinhAnhVanBanID);

            // Properties
            this.Property(t => t.TenFile)
                .IsRequired()
                .HasMaxLength(200);

            this.Property(t => t.FileMaHoa)
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("HinhAnhVanBan");
            this.Property(t => t.HinhAnhVanBanID).HasColumnName("HinhAnhVanBanID");
            this.Property(t => t.TenFile).HasColumnName("TenFile");
            this.Property(t => t.FileMaHoa).HasColumnName("FileMaHoa");
            this.Property(t => t.VanBanID).HasColumnName("VanBanID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
