using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ThongSoSizeMap : EntityTypeConfiguration<ThongSoSize>
    {
        public ThongSoSizeMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Size)
                .HasMaxLength(50);

            this.Property(t => t.TenThongSo)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ThongSoSize");
            this.Property(t => t.Size).HasColumnName("Size");
            this.Property(t => t.TenThongSo).HasColumnName("TenThongSo");
            this.Property(t => t.GiaTri).HasColumnName("GiaTri");
            this.Property(t => t.ID).HasColumnName("ID");
        }
    }
}
