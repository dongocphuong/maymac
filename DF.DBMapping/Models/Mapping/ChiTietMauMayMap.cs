using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChiTietMauMayMap : EntityTypeConfiguration<ChiTietMauMay>
    {
        public ChiTietMauMayMap()
        {
            // Primary Key
            this.HasKey(t => new { t.ChiTietMauMayID, t.MauMayID, t.STT, t.NoiDungChiTiet, t.ThoiGianThucHien, t.IsActive });

            // Properties
            this.Property(t => t.STT)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.NoiDungChiTiet)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.YeuCauPAThucHien)
                .HasMaxLength(2000);

            this.Property(t => t.GhiChu)
                .HasMaxLength(250);

            this.Property(t => t.IsActive)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("ChiTietMauMay");
            this.Property(t => t.ChiTietMauMayID).HasColumnName("ChiTietMauMayID");
            this.Property(t => t.MauMayID).HasColumnName("MauMayID");
            this.Property(t => t.STT).HasColumnName("STT");
            this.Property(t => t.NoiDungChiTiet).HasColumnName("NoiDungChiTiet");
            this.Property(t => t.NguoiThucHien).HasColumnName("NguoiThucHien");
            this.Property(t => t.NguoiPhuTrach).HasColumnName("NguoiPhuTrach");
            this.Property(t => t.YeuCauPAThucHien).HasColumnName("YeuCauPAThucHien");
            this.Property(t => t.ThoiGianThucHien).HasColumnName("ThoiGianThucHien");
            this.Property(t => t.GhiChu).HasColumnName("GhiChu");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
