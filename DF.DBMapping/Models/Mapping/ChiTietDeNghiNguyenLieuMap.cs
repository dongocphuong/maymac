using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChiTietDeNghiNguyenLieuMap : EntityTypeConfiguration<ChiTietDeNghiNguyenLieu>
    {
        public ChiTietDeNghiNguyenLieuMap()
        {
            // Primary Key
            this.HasKey(t => t.ChiTietDeNghiNguyenLieuID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ChiTietDeNghiNguyenLieu");
            this.Property(t => t.ChiTietDeNghiNguyenLieuID).HasColumnName("ChiTietDeNghiNguyenLieuID");
            this.Property(t => t.DeNghiNguyenLieuID).HasColumnName("DeNghiNguyenLieuID");
            this.Property(t => t.NguyenLieuID).HasColumnName("NguyenLieuID");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.DonGia).HasColumnName("DonGia");
            this.Property(t => t.SoLuongDuyet).HasColumnName("SoLuongDuyet");
        }
    }
}
