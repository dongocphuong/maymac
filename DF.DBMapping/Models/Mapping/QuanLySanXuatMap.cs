using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class QuanLySanXuatMap : EntityTypeConfiguration<QuanLySanXuat>
    {
        public QuanLySanXuatMap()
        {
            // Primary Key
            this.HasKey(t => t.QuanLySanXuatID);

            // Properties
            this.Property(t => t.TenCongDoan)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("QuanLySanXuat");
            this.Property(t => t.QuanLySanXuatID).HasColumnName("QuanLySanXuatID");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
            this.Property(t => t.TenCongDoan).HasColumnName("TenCongDoan");
            this.Property(t => t.TrangThaiCongDoanCuoi).HasColumnName("TrangThaiCongDoanCuoi");
            this.Property(t => t.ThoiGianBatDauCongDoanCuoi).HasColumnName("ThoiGianBatDauCongDoanCuoi");
            this.Property(t => t.LyDoTamDungCongDoanCuoi).HasColumnName("LyDoTamDungCongDoanCuoi");
        }
    }
}
