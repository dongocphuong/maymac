using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class tblLanguageMap : EntityTypeConfiguration<tblLanguage>
    {
        public tblLanguageMap()
        {
            // Primary Key
            this.HasKey(t => t.langkey);

            // Properties
            this.Property(t => t.langkey)
                .IsRequired()
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("tblLanguage");
            this.Property(t => t.langkey).HasColumnName("langkey");
            this.Property(t => t.vn).HasColumnName("vn");
            this.Property(t => t.cn).HasColumnName("cn");
            this.Property(t => t.en).HasColumnName("en");
        }
    }
}
