using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class DinhNghiaSanPhamMap : EntityTypeConfiguration<DinhNghiaSanPham>
    {
        public DinhNghiaSanPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.DinhNghiaSanPhamID);

            // Properties
            // Table & Column Mappings
            this.ToTable("DinhNghiaSanPham");
            this.Property(t => t.DinhNghiaSanPhamID).HasColumnName("DinhNghiaSanPhamID");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
            this.Property(t => t.ThanhPhamID).HasColumnName("ThanhPhamID");
            this.Property(t => t.SoLuongThanhPham).HasColumnName("SoLuongThanhPham");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.ThoiGianHoanThanh).HasColumnName("ThoiGianHoanThanh");
            this.Property(t => t.MoTa).HasColumnName("MoTa");
            this.Property(t => t.GiaTri).HasColumnName("GiaTri");
            this.Property(t => t.DsHinhAnh).HasColumnName("DsHinhAnh");
        }
    }
}
