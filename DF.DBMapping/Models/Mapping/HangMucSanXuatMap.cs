using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class HangMucSanXuatMap : EntityTypeConfiguration<HangMucSanXuat>
    {
        public HangMucSanXuatMap()
        {
            // Primary Key
            this.HasKey(t => t.HangMucSanXuatID);

            // Properties
            // Table & Column Mappings
            this.ToTable("HangMucSanXuat");
            this.Property(t => t.HangMucSanXuatID).HasColumnName("HangMucSanXuatID");
            this.Property(t => t.SanPhamID).HasColumnName("SanPhamID");
            this.Property(t => t.GiaTriDinhMuc).HasColumnName("GiaTriDinhMuc");
            this.Property(t => t.HaoHut).HasColumnName("HaoHut");
            this.Property(t => t.ThoiGianHoanThanh).HasColumnName("ThoiGianHoanThanh");
            this.Property(t => t.QuanLySanXuatID).HasColumnName("QuanLySanXuatID");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.ViTri).HasColumnName("ViTri");
        }
    }
}
