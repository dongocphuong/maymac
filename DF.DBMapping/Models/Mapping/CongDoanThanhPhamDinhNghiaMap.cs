using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class CongDoanThanhPhamDinhNghiaMap : EntityTypeConfiguration<CongDoanThanhPhamDinhNghia>
    {
        public CongDoanThanhPhamDinhNghiaMap()
        {
            // Primary Key
            this.HasKey(t => t.CongDoanThanhPhamDinhNghiaID);

            // Properties
            // Table & Column Mappings
            this.ToTable("CongDoanThanhPhamDinhNghia");
            this.Property(t => t.DonHangID).HasColumnName("DonHangID");
            this.Property(t => t.CongDoanThanhPhamDinhNghiaID).HasColumnName("CongDoanThanhPhamDinhNghiaID");
            this.Property(t => t.ThanhPhamID).HasColumnName("ThanhPhamID");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.LoaiHinh).HasColumnName("LoaiHinh");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.CongDoanThanhPhamID).HasColumnName("CongDoanThanhPhamID");
            this.Property(t => t.LoaiVai).HasColumnName("LoaiVai");
        }
    }
}
