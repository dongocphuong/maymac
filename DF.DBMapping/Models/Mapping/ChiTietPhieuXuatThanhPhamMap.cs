using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class ChiTietPhieuXuatThanhPhamMap : EntityTypeConfiguration<ChiTietPhieuXuatThanhPham>
    {
        public ChiTietPhieuXuatThanhPhamMap()
        {
            // Primary Key
            this.HasKey(t => t.ChiTietPhieuXuatThanhPhamID);

            // Properties
            // Table & Column Mappings
            this.ToTable("ChiTietPhieuXuatThanhPham");
            this.Property(t => t.ChiTietPhieuXuatThanhPhamID).HasColumnName("ChiTietPhieuXuatThanhPhamID");
            this.Property(t => t.PhieuXuatThanhPhamID).HasColumnName("PhieuXuatThanhPhamID");
            this.Property(t => t.ThanhPhamID).HasColumnName("ThanhPhamID");
            this.Property(t => t.SoLuong).HasColumnName("SoLuong");
            this.Property(t => t.TinhTrang).HasColumnName("TinhTrang");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
