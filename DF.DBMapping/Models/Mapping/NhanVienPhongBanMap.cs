using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace DF.DBMapping.Models.Mapping
{
    public class NhanVienPhongBanMap : EntityTypeConfiguration<NhanVienPhongBan>
    {
        public NhanVienPhongBanMap()
        {
            // Primary Key
            this.HasKey(t => t.NhanVienPhongBanID);

            // Properties
            // Table & Column Mappings
            this.ToTable("NhanVienPhongBan");
            this.Property(t => t.NhanVienPhongBanID).HasColumnName("NhanVienPhongBanID");
            this.Property(t => t.NhanVienID).HasColumnName("NhanVienID");
            this.Property(t => t.ChucVuID).HasColumnName("ChucVuID");
            this.Property(t => t.PhongBanID).HasColumnName("PhongBanID");
            this.Property(t => t.ThoiGian).HasColumnName("ThoiGian");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}
