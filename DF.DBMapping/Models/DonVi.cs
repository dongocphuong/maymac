using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class DonVi
    {
        public System.Guid DonViId { get; set; }
        public string TenDonVi { get; set; }
        public bool IsActive { get; set; }
    }
}
