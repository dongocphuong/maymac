using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChiTietPhieuNhapSanPham
    {
        public System.Guid ChiTietPhieuNhapSanPhamID { get; set; }
        public System.Guid PhieuNhapSanPhamID { get; set; }
        public System.Guid SanPhamID { get; set; }
        public Nullable<double> GiaNhap { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<int> TinhTrang { get; set; }
        public bool IsActive { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
        public string LyDoHong { get; set; }
    }
}
