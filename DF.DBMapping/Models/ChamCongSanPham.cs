using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChamCongSanPham
    {
        public System.Guid ChamCongSanPhamID { get; set; }
        public Nullable<System.DateTime> ThoiGian { get; set; }
        public Nullable<System.Guid> NhanVienID { get; set; }
        public string MaSanPham { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<double> KhoangThoiGian { get; set; }
        public Nullable<double> DonGia { get; set; }
    }
}
