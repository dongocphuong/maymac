using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ThanhToanTien
    {
        public System.Guid ThanhToanTienID { get; set; }
        public Nullable<System.DateTime> ThoiGian { get; set; }
        public string NoiDung { get; set; }
        public Nullable<System.Guid> NhanVienID { get; set; }
        public Nullable<double> SoTien { get; set; }
        public Nullable<System.Guid> DoiTacID { get; set; }
        public int LoaiHinh { get; set; }
    }
}
