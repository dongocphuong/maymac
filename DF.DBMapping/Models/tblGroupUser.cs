using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class tblGroupUser
    {
        public System.Guid GroupGuid { get; set; }
        public System.Guid UserGuid { get; set; }
    }
}
