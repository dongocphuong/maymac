using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class ChiTietPhieuNhapNguyenLieu
    {
        public System.Guid ChiTietPhieuNhapNguyenLieuID { get; set; }
        public System.Guid PhieuNhapNguyenLieuID { get; set; }
        public System.Guid NguyenLieuID { get; set; }
        public double GiaNhap { get; set; }
        public double SoLuong { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.Guid> DonViID { get; set; }
    }
}
