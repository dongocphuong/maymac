using System;
using System.Collections.Generic;

namespace DF.DBMapping.Models
{
    public partial class tblGroup
    {
        public System.Guid GroupGuid { get; set; }
        public string GroupName { get; set; }
        public Nullable<int> RuleType { get; set; }
        public Nullable<long> Code { get; set; }
        public string UrlLink { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
        public Nullable<int> Nhom { get; set; }
        public Nullable<System.Guid> GroupParent { get; set; }
        public string KeyLanguage { get; set; }
        public string MoTa { get; set; }
        public string Icon { get; set; }
        public bool IsHeader { get; set; }
        public Nullable<int> STT { get; set; }
    }
}
