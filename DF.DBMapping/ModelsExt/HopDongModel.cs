﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt
{
    public class HopDongModel
    {
        public Nullable<Guid> HopDongID { get; set; }
        public string TenHopDong { get; set; }
        public Nullable<int> LoaiHopDong { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
        public string DinhKem { get; set; }
        public Nullable<Guid> DonViTao { get; set; }
        public System.DateTime ThoiGianTao { get; set; }
        public Nullable<DateTime> NgayBatDau { get; set; }
        public Nullable<DateTime> NgayKetThuc { get; set; }
        public Nullable<Guid> DonViLienQuan { get; set; }
        public string TenDonViTao { get; set; }
        public string TenDonViLienQuan { get; set; }
        public string TenDoiTac { get; set; }
        public Nullable<Guid> DoiTacId { get; set; }
        public bool IsActive { get; set; }
        public System.Guid NhanVienID { get; set; }
        public System.Guid ThietBiID { get; set; }
        public System.Guid CCDCID { get; set; }
        public System.Guid TaiSanID { get; set; }
        public System.Guid PhieuXuatVatTuID { get; set; }
        public System.Guid CongTrinhId { get; set; }
        public List<string> CongTrinh { get; set; }
        public List<string> PhongBan { get; set; }
        public string strThoiGian { get; set; }
        public string strThoiGianBD { get; set; }
        public string strThoiGianKT { get; set; }
        public Nullable<int> CapDoBaoMat { get; set; }
        public Nullable<int> LoaiHopDongNhanVien { get; set; }
        public Nullable<int> Dept { get; set; }
        public string NgayThoiViec { get; set; }
        public List<lstImageHopDong> ListHinhAnh { get; set; }
        public Nullable<double> GiaTriHopDong { get; set; }

    }

    public class lstImageHopDong
    {
        public string img { get; set; }
        public string name { get; set; }
    }

    public class HopDongEdit
    {
        public string HopDongID { get; set; }
        public int LoaiHopDong { get; set; }
        public List<itemCongTrinh> CT { get; set; }
        public List<itemCongTrinh> PB { get; set; }
    }

    public class itemCongTrinh
    {
        public Guid CongTrinhID { get; set; }
        public string TenCongTrinh { get; set; }
    }

    public class HopDongModelAdd
    {
        public string HopDongID { get; set; }
        public string TenHopDong { get; set; }
        public string strThoiGian { get; set; }
        public List<lstImageHopDong> ListHinhAnh { get; set; }

    }
}
