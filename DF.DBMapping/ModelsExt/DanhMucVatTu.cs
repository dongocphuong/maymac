﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt
{
    public class DanhMucVatTu
    {
            public Nullable<Guid> VatTuID { get; set; }
            public Nullable<double> DonGiaKhoan { get; set; }
            public Guid DonViId { get; set; }
            public Guid NhomVatTuID { get; set; }
            public string TenVatTu { get; set; }
            public string TenVietTat { get; set; }
            public string TenNhom { get; set; }
            public string TenDonVi { get; set; }
            public bool IsActive { get; set; }
    }
}
