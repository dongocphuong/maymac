﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt
{
    public class DonHangModel
    {
        public Nullable<System.Guid> DonHangID { get; set; }
        public string TenDonHang { get; set; }
        public string MaDonHang { get; set; }
        public System.Guid DoiTacID { get; set; }
        public Nullable<System.DateTime> ThoiGian { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.Guid> ChiTietDonHangID { get; set; }
        public float DonGia { get; set; }
        public float SoLuong { get; set; }
        public int TrangThai { get; set; }
        public int LoaiHinh { get; set; }
        public System.Guid SanPhamID { get; set; }
        public string TenDoiTac { get; set; }
        public string MaSanPham { get; set; }
        public string TenSanPham { get; set; }
        public string TenDonVi { get; set; }
    }
}
