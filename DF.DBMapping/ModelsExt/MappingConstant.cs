﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt
{
    public class MappingConstant
    {
        public const string CONGTRINHDOITACDITHUE = "b0baeb25-1f1e-4fdf-8823-f6a8d5c6ff23";
        public const string CONGTRINHGUI = "dad564ac-6748-49e5-9347-cc4d08c9e833";
        public const string CONGTRINHTONG = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
        public const string XUONGCONGTY = "1525AC01-0387-4D66-8FB5-B3A3DD15FC85";
        public const string CONGTRINHCHOVIEC = "00000000-0000-0000-0000-000000000000";

        public const string LEDONG = "AB3031D0-49BC-481E-81D7-E594E8981056";
        public const string DANGKIEN = "AF3CC397-DA63-4154-8B70-8277B3FDC345";
    }
}
