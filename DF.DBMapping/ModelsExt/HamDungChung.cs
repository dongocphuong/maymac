﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt
{
    public class HamDungChung
    {
        public double TruThoiGian(DateTime TuNgay,DateTime DenNgay)
        {
            TimeSpan khoangthoigian = DenNgay - TuNgay;
            return Math.Abs(khoangthoigian.Days) + 1;
        }
        public double TruNgay(DateTime TuNgay, DateTime DenNgay)
        {
            DateTime tn = DateTime.ParseExact(TuNgay.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture), "dd/MM/yyyy", null);
            DateTime dn = DateTime.ParseExact(DenNgay.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture), "dd/MM/yyyy", null);
            TimeSpan khoangthoigian = dn - tn;
            return Math.Abs(khoangthoigian.Days);
        }
        public double LamNguyenSoThapPhan(double sothapphan)
        {
            return Math.Round(sothapphan, 0);
        }
    }
}
