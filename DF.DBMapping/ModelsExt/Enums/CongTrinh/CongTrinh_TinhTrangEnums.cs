﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.Enums.CongTrinh
{
    public enum DeNghi_TrangThaiEnums
    {
        CHUA_DUYET = 1,
        DA_DUYET = 2,
        DA_XUAT = 3,
        DA_XONG = 5,
        TU_CHOI = 4,
    }
    public enum CongTrinh_TinhTrangEnums
    {
        CHUA_THI_CONG = 0,
        DANG_THI_CONG = 1,
        TAM_DUNG = 2, 
        DA_HOAN_THIEN= 3,
        DA_QUYET_TOAN = 4,
        DANG_BAO_GIA = 5,
        DA_BAO_GIA = 6
    }

    public enum CongTrinh_HopDong_LoaiHopDongEnums {
        LAO_DONG = 1,
        MUA_BAN = 2,
        SUA_CHUA = 3,
        KINH_TE = 4,
        THI_CONG = 5,
        CONG_VAN_DEN = 6,
        CONG_VAN_DI = 7,
        TAI_LIEU = 8,
        BIEU_MAU = 9,
        BIEN_BAN = 10
    }

}
