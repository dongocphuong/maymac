﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt
{
    public class Define
    {
        public const string HeThong = "10";
        public const string WebThietBiXemTheoDoi = "18";
        public const string WebThietBi = "11";
        public const string WebKTKH = "12";
        public const string WebNhanSu = "16";
        public const string WebPhongKeToan = "17";
        public const string WebPhongKyThuat = "13";
        public const string WebPhongVatTu = "14";
        public const string WebPhongKTKH = "12";
        public const string WebBanGiamDoc = "15";
        public const string WebCCDC = "21";
        public const string WebDeNghiSuaChua = "22";
        public const string NhapBCBanGiamDocRole = "23";

        public const string QuanLyThuNhapRole = "15";
        public const string QLLeDong = "15";
        public const string QLDangKien = "15";
        public const string QUngLuong = "15";

        // Nhận thông báo mobile
        public const int ThongBao_CongTrinh = 30;
        public const int ThongBao_VatTu = 31;
        public const int ThongBao_CCDC = 32;
        public const int ThongBao_HanhChinh = 33;
        public const int ThongBao_ThietBi = 34;

        public const int LaiMay = 5;

        public const string CONGTRINHDOITACDITHUE = "b0baeb25-1f1e-4fdf-8823-f6a8d5c6ff23";
        public const string CONGTRINHGUI = "dad564ac-6748-49e5-9347-cc4d08c9e833";
        public const string CONGTRINHTONG = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
        public const string XUONGCONGTY = "1525AC01-0387-4D66-8FB5-B3A3DD15FC85";
        public const string CONGTRINHCHOVIEC = "00000000-0000-0000-0000-000000000000";

        public const string HOANHOADON = "B6F0E64F-DD46-458A-A635-6DA6184B4BE5";
        public const string PHONGANTOANLAODONG = "445C6C78-2CCA-47C9-B79C-DF549631ABB9";

        public const string LeDongID = "577948C2-54C5-47E8-B045-87B4828BB157";
        public const string DangKienID = "A8827C06-388B-44C2-B9F0-5283AFBB1636";
        public const string LeDongText = "Lê Đông";
        public const string DangKienText = "Đăng Kiên";
    }

}
