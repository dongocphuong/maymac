﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.NghiepVu
{
    public class HoaDonSanPhamModel
    {
        public int LoaiHinh { get; set; }
        public string MaSanPham { get; set; }
        public string TenSanPham { get; set; }
        public Guid SanPhamID { get; set; }
        public string TenDonVi { get; set; }
        public string AnhDaiDien { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<double> SoLuongGiao { get; set; }
        public Nullable<double> DonGia { get; set; }
        public string Size { get; set; }
        public string Mau { get; set; }
        public string GhiChu { get; set; }
    }
    public class HoaDonChiTietModel
    {
        public Nullable<Guid> ChiTietDonHangKhachHangDatID { get; set; }
        public string Size { get; set; }
        public string Ten { get; set; }
        public int Loai { get; set; }
        public string Mau { get; set; }
        public int GioiTinh { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public List<HoaDonChiTietNguyenLieuModel> items { get; set; }
    }
    public class HoaDonChiTietNguyenLieuModel
    {
        public string Ten { get; set; }
        public string NoiDung { get; set; }
      
    }
    public class DonHangModelAdd
    {
        public Nullable<Guid> DonHangID { get; set; }
        public string TenDonHang { get; set; }
        public string ThoiGian { get; set; }
        public string ThoiGianThanhToan { get; set; }
        public string ThoiGianDuKienSanXuat { get; set; }
        public string ThoiGianDuKienGiaoHang { get; set; }
        public Nullable<double> SoTienDaThanhToan { get; set; }
        public int LoaiHinhThanhToan { get; set; }
        public string YeuCauKhac { get; set; }
        public int TrangThai { get; set; }
        public string DiaDiemGiaoHang { get; set; }
        public string MaDonHang { get; set; }
        public Guid DoiTacID { get; set; }
        public Nullable<Guid> YeuCauKhachHangID { get; set; }
        public List<lstImageHopDong> ListHinhAnh { get; set; }
        public string EmailKeToan { get; set; }
        public string ThoiGianDatCoc { get; set; }
        public string GhiChu { get; set; }

    }

}
