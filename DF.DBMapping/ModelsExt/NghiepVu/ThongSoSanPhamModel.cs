﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.NghiepVu
{
    public class ThongSoSanPhamModel
    {
        public List<string> Sizes { get; set; }
        public string lstMaSanPham { get; set; }
        public Nullable<int> LoaiHinhSanXuat { set; get; }
        public List<ThongSoSize> thongsos { get; set;}

    }
    public class ChiTietYeuCauModel
    {
        public Nullable<System.Guid> ChiTietYeuCauID { get; set; }
        public Nullable<System.Guid> YeuCauKhachHangID { get; set; }
        public string TenSanPham { get; set; }
        public string Size { get; set; }
        public string Mau { get; set; }
        public Nullable<double> SoLuongDat { get; set; }
        public Nullable<double> GiaBan { get; set; }
        public string YeuCauKhac { get; set; }
        public Nullable<int> IsActive { get; set; }
        public string DacDiem { get; set; }
        public string ChatLieu { get; set; }
        public string MauPhoi { get; set; }
        public string ThoiGianGiaoHang { get; set; }
    }
}
