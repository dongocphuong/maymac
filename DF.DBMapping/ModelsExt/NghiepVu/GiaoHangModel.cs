﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace DF.DBMapping.ModelsExt.NghiepVu
{
    public class GiaoHangModel
    {
        public string TenDoiTac { get; set; }
        public string DiaChi { get; set; }
        public string SoTienBangChu { get; set; }
        public List<GiaoHangItemModel> item { get; set; }
    }

    public class GiaoHangItemModel
    {
        public string TenSanPham { get; set; }
        public string MaSanPham { get; set; }
        public string DonVi { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public double ThanhTien { get; set; }
    }
}
