﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.NghiepVu
{
    public class CongDoanSanPhamModel
    {
        public string TenCongDoan { get; set; }
        public Nullable<Guid> DMHangMucSanXuatID { get; set; }
        public Nullable<double> HaoHut { get; set; }
        public Nullable<double> ThoiGianHoanThanh { get; set; }
        public Nullable<int> ViTri { get; set; }
        public string MoTa { get; set; }
        public List<CongDoanSanPhamItemModel> item { get; set; }
        public Nullable<double> GiaTri { get; set; }
        public string DsHinhAnh { get; set; }

    }
    public class CongDoanSanPhamItemModel
    {
        public Nullable<Guid> ThanhPhamID { get; set; }
        public string TenThanhPham { get; set; }
        public string MaThanhPham { get; set; }
        public string TenDonVi { get; set; }
        public string AnhDaiDien { get; set; }
        public Nullable<double> SoLuong { get; set; }
       

    }
    public class CongDoanNguyenLieuDinhNghiaModel
    {
        public Guid NguyenLieuID { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<int> LoaiVai { get; set; }

    }
    public class ThuocTinhSanPhamModel
    {
        public Nullable<Guid> ThuocTinhSanPhamID { get; set; }
        public Nullable<Guid> SanPhamID { get; set; }
        public string TenThuocTinh { get; set; }
        public string GiaTri { get; set; }
        public int LoaiHinh { get; set; }
        public int KieuDuLieu { get; set; }

    }
}
