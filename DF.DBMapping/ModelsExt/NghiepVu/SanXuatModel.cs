﻿using DF.DBMapping.ModelsExt.NghiepVu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.NghiepVu
{
    public class SanXuatModel
    {
        public Guid DonHangID { get; set; }
        public Guid SanPhamID { get; set; }
        public string MaDonHang { get; set; }
        public string TenKhachHang { get; set; }
        public string TenDonHang { get; set; }
        public string AnhDaiDien { get; set; }
        public string MaSanPham { get; set; }
        public string TenSanPham { get; set; }
        public string Size { get; set; }
        public string GioiTinh { get; set; }
        public string Loai { get; set; }
        public string SizeMauGoc { get; set; }
        public string ThoiGian { get; set; }
        public string TiLeSize { get; set; }
        public double SoLuong { get; set; }
        public double GiaTriDonHang { get; set; }
        public List<CongDoanModel> lstCongDoan { get; set; }
        public CongDoanCuoiModel congDoanCuoi { get; set; }
    }
    public class LenhSanXuatModel
    {
        public Guid DonHangID { get; set; }
        public Guid SanPhamID { get; set; }
        public string MaDonHang { get; set; }
        public string TenKhachHang { get; set; }
        public string TenDonHang { get; set; }
        public string AnhDaiDien { get; set; }
        public string MaSanPham { get; set; }
        public string TenSanPham { get; set; }
        public string Size { get; set; }
        public string GioiTinh { get; set; }
        public string Loai { get; set; }
        public string SizeMauGoc { get; set; }
        public string ThoiGian { get; set; }
        public string TiLeSize { get; set; }
        public double SoLuong { get; set; }
        public double GiaTriDonHang { get; set; }
        public List<LenhSanXuatChiTietModel> items { get; set; }

    }
    public class LenhSanXuatChiTietModel
    {
        public System.Guid DinhNghiaThanhPhamID { get; set; }

        public string Size { get; set; }

        public string Mau { get; set; }

        public double SoLuong { get; set; }

        public System.Nullable<int> LoaiVai { get; set; }

        public string TenLoaiVai { get; set; }

        public string MaNguyenLieu { get; set; }

        public string TenNguyenLieu { get; set; }

        public System.Nullable<System.Guid> NguyenLieuID { get; set; }

        public string TenDonVi { get; set; }

        public string AnhDaiDien { get; set; }

    }
    public class PhieuSanXuatThanhPhamModel
    {
        public Guid DonHangID { get; set; }
        public Guid SanPhamID { get; set; }
        public string MaDonHang { get; set; }
        public string TenKhachHang { get; set; }
        public string TenDonHang { get; set; }
        public string AnhDaiDien { get; set; }
        public string MaSanPham { get; set; }
        public string TenSanPham { get; set; }
        public double SoLuong { get; set; }
        public string Size { get; set; }
        public string GioiTinh { get; set; }
        public string Loai { get; set; }
        public string SizeMauGoc { get; set; }
        public string ThoiGian { get; set; }
        public Nullable<double> TiLeSize { get; set; }
        public double SoLuongDaSanXuat { get; set; }
        public double GiaTriDonHang { get; set; }
        public List<CongDoanNguyenLieuModel> lstNguyenLieu { get; set; }
    }

    public class CongDoanModel
    {
        public string TenCongDoan { get; set; }
        public string LyDoTamDung { get; set; }
        public string MoTa { get; set; }
        public Guid HangMucSanXuatID { get; set; }
        public double ThoiGianHienTai { get; set; }
        public double ThoiGianDinhMuc { get; set; }
        public int TrangThai { get; set; }
        public Nullable<int> ViTri { get; set; }
        public List<CongDoanThanhPhamModel> lstThanhPham { get; set; }
    }
    public class CongDoanThanhPhamModel
    {
        public string AnhDaiDien { get; set; }
        public string MaThanhPham { get; set; }
        public string Size { get; set; }
        public string Mau { get; set; }
        public string TenThanhPham { get; set; }
        public Nullable<Guid> ThanhPhamID { get; set; }
        public string DonVi { get; set; }
        public double SoLuongTong { get; set; }
        public Nullable<double> SoLuongHong { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public double SoLuongDaHoanThien { get; set; }
        public int LoaiHinh { get; set; }
        public string LyDoHong { get; set; }
        public List<CongDoanNguyenLieuModel> lstNguyenLieu { get; set; }
    }

    public class CongDoanThanhPhamCuoiModel
    {
        public string AnhDaiDien { get; set; }
        public string MaSanPham { get; set; }
        public string Size { get; set; }
        public string Mau { get; set; }
        public string TenSanPham { get; set; }
        public Nullable<Guid> SanPhamID { get; set; }
        public string DonVi { get; set; }
        public double SoLuongTong { get; set; }
        public Nullable<double> SoLuongHong { get; set; }
        public double SoLuongDaHoanThien { get; set; }
        public int LoaiHinh { get; set; }
        public string LyDoHong { get; set; }
        public List<CongDoanThanhPhamModel> lstNguyenLieu { get; set; }
    }

    public class CongDoanNguyenLieuModel
    {
        public string AnhDaiDien { get; set; }
        public string MaNguyenLieu { get; set; }
        public Nullable<Guid> NguyenLieuID { get; set; }
        public string TenNguyenLieu { get; set; }
        public string DonVi { get; set; }
        public double SoLuong { get; set; }
        public int LoaiHinh { get; set; }
        public Nullable<int> LoaiVai { get; set; }
        public Nullable<double> SoLuongDaCo { get; set; }
    }
    public class CongDoanCuoiModel
    {
        public double ThoiGianHienTai { get; set; }
        public double ThoiGianDinhMuc { get; set; }
        public string MoTa { get; set; }
        public string LyDoTamDung { get; set; }
        public double SoLuong { get; set; }
        public double SoLuongDaCo { get; set; }
        public Nullable<int> TrangThai { get; set; }
        public List<CongDoanThanhPhamModel> lstThanhPham { get; set; }
    }
    public class SanXuatThanhPhamModel
    {
        public Nullable<Guid> ThanhPhamID { get; set; }
        public Nullable<double> SoLuongCan { get; set; }
        public string LyDoHong { get; set; }
        public Nullable<double> SoLuongHong { get; set; }
        public Nullable<double> SoLuongTong { get; set; }
        public Nullable<double> SoLuongSanXuat { get; set; }
        public Nullable<double> SoLuongDaHoanThien { get; set; }
    }
    public class SanXuatSanPhamNhomModel
    {
        public Nullable<Guid> SanPhamID { get; set; }
        public Nullable<double> SoLuongSanXuat { get; set; }
        public Nullable<double> SoLuongHong { get; set; }
        public string LyDoHong { get; set; }
    }
    public class SanXuatNguyenLieuModel
    {
        public Nullable<Guid> NguyenLieuID { get; set; }
        public int LoaiHinh { get; set; }
        public double SoLuongCan { get; set; }
    }
    public class SizeModel
    {
        public string Size { get; set; }
        public List<MauModel> Mau { get; set; }
    }
    public class MauModel
    {
        public string LoaiVai { get; set; }
        public Nullable<Guid> NguyenLieuID { get; set; }
    }

    public class SanXuatNhomModel
    {
        public Guid DonHangID { get; set; }
        public Guid MauMayID { get; set; }
        public string MaDonHang { get; set; }
        public string TenKhachHang { get; set; }
        public string TenDonHang { get; set; }
        public string AnhDaiDien { get; set; }
        public string MaSanPham { get; set; }
        public string TenSanPham { get; set; }
        public string Size { get; set; }
        public string GioiTinh { get; set; }
        public string Loai { get; set; }
        public string SizeMauGoc { get; set; }
        public string ThoiGian { get; set; }
        public string TiLeSize { get; set; }
        public double SoLuong { get; set; }
        public double GiaTriDonHang { get; set; }
        public List<CongDoanModel> lstCongDoan { get; set; }
        public CongDoanCuoiNhomModel congDoanCuoi { get; set; }
    }
    public class CongDoanCuoiNhomModel
    {
        public double ThoiGianHienTai { get; set; }
        public double ThoiGianDinhMuc { get; set; }
        public string MoTa { get; set; }
        public string LyDoTamDung { get; set; }
        public Nullable<int> TrangThai { get; set; }
        public List<CongDoanThanhPhamCuoiModel> lstThanhPham { get; set; }
    }
}
