﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.VatTu
{
    public class CongNoDoiTacVatTuResult
    {
        public Guid DoiTacID { get; set; }
        public string TenDoiTac { get; set; }
        public double GiaTriPhatSinh { get; set; }
        public double TongGiaTri { get; set; }
    }
}
