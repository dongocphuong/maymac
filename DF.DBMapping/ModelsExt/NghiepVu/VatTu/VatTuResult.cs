﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.VatTu
{
    public class TonVatTuResult
    {
        public Guid VatTuID { get; set; }
        public string TenVatTu { get; set; }
        public string TenVietTat { get; set; }
        public string TenDonVi { get; set; }
        public double SoLuongNhap { get; set; }
        public double NhapDauTu { get; set; }
        public double NhapDieuChuyen { get; set; }
        public double SoLuongXuat { get; set; }
        public double TonHienTai { get; set; }
        public string GhiChu { get; set; }
        public bool IsActive { get; set; }
        public Guid TonKhoVatTuID { get; set; }
        public double SoLuongTon { get; set; }
        public double? GiaBan { get; set; }
        public Guid? DoiThiCongID { get; set; }
        public Guid CongTrinhID { get; set; }
        public Nullable<double> ThanhTien { get; set; }
        public Nullable<double> TienXuat { get; set; }
        public Nullable<double> TienNhap { get; set; }
    }
    public class XacNhanNhapVatTuResult
    {
        public Guid PhieuXuatVatTuID { get; set; }
        public DateTime ThoiGian { get; set; }
        public string TenCongTrinhDi { get; set; }
        public string TenDoiThiCongDi { get; set; }
        public string MaPhieu { get; set; }
        public Nullable<double> TongGiaTri { get; set; }
    }
    public class LichSuXuatVatTuResult
    {
        public Guid PhieuXuatVatTuID { get; set; }
        public DateTime ThoiGian { get; set; }
        public int LoaiHinhXuat { get; set; }
        public string NhanVienNhap { get; set; }
        public string TenVatTu { get; set; }
        public string TenVietTat { get; set; }
        public string TenDonVi { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public double ThanhTien { get; set; }
        public string CongTrinhDen { get; set; }
        public string DoiThiCongDen { get; set; }
        public string MaPhieu { get; set; }
        public int TrangThaiDuyet { get; set; }
        public Nullable<int> Lever { get; set; }
        public string DsHinhAnh { get; set; }
        public string GhiChu { get; set; }
    }
    public class LichSuNhapVatTuResult
    {
        public Guid PhieuNhapVatTuID { get; set; }
        public DateTime ThoiGian { get; set; }
        public string NhanVienNhap { get; set; }
        public string TenVatTu { get; set; }
        public string TenVietTat { get; set; }
        public string TenDonVi { get; set; }
        public double SoLuong { get; set; }
        public double SoLuongXuat { get; set; }
        public double SoLuongTon { get; set; }
        public double DonGia { get; set; }
        public double ThanhTien { get; set; }
        public string NoiDung { get; set; }
        public string GhiChu { get; set; }
        public string DoiTac { get; set; }
        public string MaPhieu { get; set; }
        public Nullable<Guid> CongTrinhDieuChuyenID { get; set; }
        public Nullable<Guid> DoiThiCongDieuChuyenID { get; set; }
        public Nullable<Guid> VatTuID { get; set; }
        public string TenCongTrinhDieuChuyen { get; set; }
        public string TenDoiThiCongDieuChuyen { get; set; }
        public Nullable<Guid> ChiTietNhapXuatVatTuID { get; set; }
        public Nullable<Guid> PhieuXuatVatTuID { get; set; }
        public Nullable<int> LoaiHinhNhap { get; set; }
        public Nullable<int> Lever { get; set; }
        public string DsHinhAnh { get; set; }

    }
    public class ThongTinPhieuXuatVatTuChuaXacNhanNhapResult
    {
        public Guid PhieuXuatVatTuID { get; set; }
        public Nullable<Guid> PhieuNhapVatTuID { get; set; }
        public DateTime ThoiGian { get; set; }
        public string KhoDi { get; set; }
        public string NhanVienTao { get; set; }
        public string GhiChu { get; set; }
        public string DSHinhAnh { get; set; }
    }
    public class ChiTietPhieuXuatChuaXacNhanNhapResult
    {
        public Guid VatTuID { get; set; }
        public string TenVatTu { get; set; }
        public string TenVietTat { get; set; }
        public string TenDonVi { get; set; }
        public double DonGia { get; set; }
        public double SoLuong { get; set; }
        public double? HeSo { get; set; }
    }
    public class ChiTietPhieuNhapNguyenLieuRequest
    {
        public Guid NguyenLieuID { get; set; }
        public Nullable<Guid> ChiTietPhieuNhapNguyenLieuID { get; set; }
        public Nullable<Guid> DonViID { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public Nullable<int> TinhTrang { get; set; }
    }
    public class ChiTietPhieuXuatNguyenLieuRequest
    {
        public Guid NguyenLieuID { get; set; }
        public Nullable<Guid> ChiTietPhieuXuatNguyenLieuID { get; set; }
        public double SoLuong { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<int> TinhTrang { get; set; }
    }
    public class ChiTietPhieuNhapVatTuResult
    {
        public Guid PhieuNhapVatTuID { get; set; }
        public DateTime ThoiGianNhap { get; set; }
        public Nullable<Guid> DoiTacID { get; set; }
        public Nullable<Guid> PhieuXuatNguyenLieuID { get; set; }
        public Nullable<Guid> HoaDonDieuChuyenID { get; set; }
        public string DSHinhAnh { get; set; }
    }

    public class ChiTietPhieuNhapThanhPhamResult
    {
        public Guid PhieuNhapVatTuID { get; set; }
        public DateTime ThoiGianNhap { get; set; }
        public Nullable<Guid> DoiTacID { get; set; }
        public Nullable<Guid> PhieuXuatThanhPhamID { get; set; }
        public Nullable<Guid> HoaDonDieuChuyenID { get; set; }
        public string DSHinhAnh { get; set; }
    }
    public class ChiTietPhieuNhapSanPhamResult
    {
        public Guid PhieuNhapVatTuID { get; set; }
        public DateTime ThoiGianNhap { get; set; }
        public Nullable<Guid> DoiTacID { get; set; }
        public Nullable<Guid> PhieuXuatSanPhamID { get; set; }
        public Nullable<Guid> HoaDonDieuChuyenID { get; set; }
        public string DSHinhAnh { get; set; }
    }
    public class ChiTietPhieuXuatThanhPhamRequest
    {
        public Guid ThanhPhamID { get; set; }
        public Nullable<Guid> ChiTietPhieuXuatThanhPhamID { get; set; }
        public double SoLuong { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<int> TinhTrang { get; set; }
    }
    public class ChiTietPhieuNhapThanhPhamRequest
    {
        public Guid ThanhPhamID { get; set; }
        public Nullable<Guid> ChiTietPhieuNhapThanhPhamID { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public Nullable<int> TinhTrang { get; set; }
    }


    public class ChiTietPhieuXuatSanPhamRequest
    {
        public Guid SanPhamID { get; set; }
        public Nullable<Guid> ChiTietPhieuXuatSanPhamID { get; set; }
        public double SoLuong { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<int> TinhTrang { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
    }
    public class ChiTietPhieuNhapSanPhamRequest
    {
        public Guid SanPhamID { get; set; }
        public Nullable<Guid> ChiTietPhieuNhapSanPhamID { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public Nullable<int> TinhTrang { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
    }

    public class ChiTietPhieuXuatVatTuResult
    {
        public Nullable<Guid> PhieuXuatVatTuID { get; set; }
        public int LoaiHinhXuat { get; set; }
        public Nullable<Guid> CongTrinhNhanID { get; set; }
        public Nullable<Guid> DoiThiCongNhanID { get; set; }
        public DateTime ThoiGian { get; set; }
        public Guid NhanVienXuatID { get; set; }
        public string DanhSachHinhAnh { get; set; }
    }
    public class ChiTietPhieuNhapSanPhamModel
    {
        public Nullable<System.Guid> ChiTietPhieuNhapSanPhamID { get; set; }
        public Nullable<System.Guid> PhieuNhapSanPhamID { get; set; }
        public Nullable<System.Guid> SanPhamID { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<int> TinhTrang { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
        public string LyDoHong { get; set; }
    }
}
