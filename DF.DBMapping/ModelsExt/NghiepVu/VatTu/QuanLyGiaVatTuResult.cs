﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.VatTu
{
    public class QuanLyGiaVatTuResult
    {
        public Nullable<Guid> VatTuID { get; set; }
        public Nullable<Guid> DoiTacID { get; set; }
        public Nullable<Guid> DonGiaVatTuID { get; set; }
        public string MaVatTu { get; set; }
        public string TenVatTu { get; set; }
        public string TenDonVi { get; set; }
        public string TenDoiTac { get; set; }
        public double DonGiaHienTai { get; set; }
        public Nullable<DateTime> ThoiGianCapNhat { get; set; }
    }
}
