﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.VatTu
{
    public partial class QuanLyPhieuNhapVatTuResult
    {
        public string TenNhanVien { get; set; }
        public string TenVatTu { get; set; }
        public string TenDonVi { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public double ThanhTien { get; set; }
        public int TrangThaiDuyet { get; set; }
        public string NoiDung { get; set; }
        public string TenCongTrinh { get; set; }
        public string TenDoi { get; set; }

        public Guid CongTrinhID { get; set; }
        public Guid DoiThiCongID { get; set; }
        public Guid PhieuNhapVatTuID { get; set; }
        public string TenDoiTac { get; set; }
        public DateTime ThoiGian { get; set; }
        
    }
    public partial class VatTuResult
    {
        public string TenVatTu { get; set; }
        public Guid VatTuID { get; set; }
        public string TenDonVi { get; set; }
        public double GiaNhap { get; set; }
        public double SoLuong { get; set; }
    }
}
