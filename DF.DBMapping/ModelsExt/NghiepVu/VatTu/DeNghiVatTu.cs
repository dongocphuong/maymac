﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.NghiepVu.VatTu
{

    public class DeNghiNguyenLieuModel
    {
        public Nullable<Guid> DonHangID { get; set; }
        public Nullable<Guid> SanPhamID { get; set; }
        public Nullable<Guid> DeNghiNguyenLieuID { get; set; }
        public string NoiDung { get; set;}
        public List<ChiTietDeNghiNguyenLieuModel> items { get; set; }
    }
    public class ChiTietDeNghiNguyenLieuModel
    {
        public Nullable<Guid> NguyenLieuID { get; set; }
        public string TenNguyenLieu { get; set; }
        public string MaNguyenLieu { get; set; }
        public string TenDonVi { get; set; }
        public Nullable<double> SoLuong{get;set;}
        public Nullable<double> DonGia { get; set; }
        public Nullable<Guid> DonHangID { get; set; }
        public Nullable<Guid> SanPhamID { get; set; }
    }

    public class ThongTinPhieuDeNghi
    {
        public Nullable<Guid> DonHangID { get; set; }
        public Nullable<Guid> SanPhamID { get; set; }
        public Nullable<Guid> DeNghiNguyenLieuID { get; set; }
        public string MaDeNghi { get; set; }
        public string TenNhanVien { get; set; }
        public string TenDonHang { get; set; }
        public string MaDonHang { get; set; }
        public string NoiDungTuChoi { get; set; }
        public string TenSanPham { get; set; }
        public string MaSanPham { get; set; }
        public string NoiDung { get; set; }
        public string LichSuDeNghi { get; set; }
        public Nullable<DateTime> ThoiGian { get; set; }
    }
}
