﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt
{
    public class QuanLyMauMayModel
    {
        public System.Guid? MauMayID { get; set; }
        public System.Guid? ChiTietMauMayID { get; set; }
        public string MaMauMay { get; set; }
        public string TenMauMay { get; set; }
        public System.DateTime ThoiGian { get; set; }
        public int STT { get; set; }
        public string NoiDung { get; set; }
        public string NguoiThucHien { get; set; }
        public string NguoiPhuTrach { get; set; }
        public string YeuCauPAThucHien { get; set; }
        public string ThoiGianThucHien { get; set; }
        public string GhiChu { get; set; }
        public string DanhSachHinhAnhs { get; set; }
        public int IsActive { get; set; }
    }
}
