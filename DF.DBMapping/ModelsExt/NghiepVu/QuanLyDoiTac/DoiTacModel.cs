﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.NghiepVu.QuanLyDoiTac
{
   public class DoiTacModel
    {
        public Guid DoiTacID { get; set; }
        public string TenDoiTac { get; set; }
        public string TenNhomDoiTac { get; set; }
        public double? TongThu { get; set; }
        public double? TongChi { get; set; }
        public double? ChenhLenh { get; set; }
        public int KieuDoiTac { get; set; }
        public string MaDoiTac { get; set; }
        public int LoaiHinhThanhToan { get; set; }
        public double ThanhToanCongNo { get; set; }
    }
    public class TaiChinhDoiTacMoidel
    {
        public Nullable<DateTime> ThoiGian { get; set; }
        public int LoaiHinh { get; set; }
        public string NguoiTao { get; set; }
        public double Thu { get; set; }
        public double Chi { get; set; }
        public double ChenhLenh { get; set; }
        public string NoiDung { get; set; }
        public string MaDeNghi { get; set; }

    }
    public class DeNghiTaiChinhDoiTac
    {
        public string MaDeNghi { get; set;  }
        public string NoiDung { get; set; }
        public DateTime? ThoiGian { get; set; }
        public string DoiTac { get; set; }
        public string NguoiTao { get; set; }
        public int? LoaiHinh { get; set; }
        public string CongTrinh { get; set; }
        public string DoiThiCong { get; set; }
        public int? LoaiHinhThanhToan { get; set; }
        public double? SoTien { get; set; }
        public double? SoTienDuyet { get; set; }
        public string NguoiDuyet { get; set; }
        public string DanhSachHinhAnhs { get; set; }
    }
    public class DeNghiVanChuyen
    {
        public string MaDeNghi { get; set; }
        public string NoiDung { get; set; }
        public DateTime? ThoiGian { get; set; }
        public string CongTrinh { get; set; }
        public string DoiThiCong { get; set; }
        public string NguoiTao { get; set; }
        public string DanhSachHinhAnhs { get; set; }
        public List<ChiTietDeNghiVanChuyen> ChiTiet { get; set; }
        public int LoaiHinh { get; set; }
    }
    public class ChiTietDeNghiVanChuyen
    {
        public string Ten { get; set; }
        public string Ma { get; set; }
        public string BienSoXe { get; set; }
        public double? SoLuong { get; set; }
        public double? DonGia { get; set; }
    }
    public class DeNghiSuaChua
    {
        public string MaDeNghi { get; set; }
        public string NoiDung { get; set; }
        public DateTime? ThoiGian { get; set; }
        public string CongTrinh { get; set; }
        public string DoiThiCong { get; set; }
        public string NguoiTao { get; set; }
        public string DanhSachHinhAnhs { get; set; }
        public string ThietBi { get; set; }
        public List<ChiTietSuaChua> ChiTiet { get; set; }
    }
    public class ChiTietSuaChua
    {
        public string Ten { get; set; }
        public string Ma { get; set; }
    }
}
