﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.NghiepVu.QuanLyDuAn
{
    public class CongTrinhResults
    {
        public Nullable<Guid> CongTrinhID { get; set; }
        public string TenCongTrinh { get; set; }
        public string ChuDauTu { get; set; }
        public string DiaDiem { get; set; }
        public Nullable<DateTime> NgayTao { get; set; }
        public Nullable<DateTime> NgayKhoiCong { get; set; }
        public Nullable<DateTime> NgayKetThuc { get; set; }
        public Nullable<int> TinhTrang { get; set; }
        public Nullable<int> TrangThai { get; set; }
        public string GhiChu { get; set; }
        public string strNgayBatDau { get; set; }
        public string strNgayKetThuc { get; set; }
        public string strNgayTao { get; set; }
        public string strDaKyHopDong { get; set; }
        public string strTinhTrang { get; set; }
        public string strNgayKyHopDong { get; set; }
        public Nullable<bool> DaKyHopDong { get; set; }
        public Nullable<int> LoaiBaoCaoSanLuong { get; set; }
        public Nullable<Guid> HopDongID { get; set; }
    }

    public class CongTrinhRequest
    {
        public Nullable<Guid> CongTrinhID { get; set; }
        public string TenCongTrinh { get; set; }
        public string ChuDauTu { get; set; }
        public string DiaDiem { get; set; }
        public bool DaKyHopDong { get; set; }
        public string NgayBatDau { get; set; }
        public string NgayKetThuc { get; set; }
        public string TinhTrang { get; set; }
        public string GhiChu { get; set; }
        public string NgayKhoiTao { get; set; }
        public Nullable<Guid> HopDongID { get; set; }
        public Nullable<int> LoaiBaoCaoSanLuong { get; set; }
    }

    public class DanhSachHangMucThiCongResult
    {
        public Nullable<System.Guid> HangMucThiCongID { get; set; }
        public Nullable<System.Guid> DMHangMucThiCongID { get; set; }
        public Nullable<double> SoCoc { get; set; }
        public Nullable<double> MetDat { get; set; }
        public Nullable<double> MetDa { get; set; }
        public Nullable<double> DonGiaThucTeMetDat { get; set; }
        public Nullable<double> DonGiaKhauTruMetDat { get; set; }
        public Nullable<double> DonGiaThucTeMetDa { get; set; }
        public Nullable<double> DonGiaKhauTruMetDa { get; set; }
        public Nullable<double> KGOngSieuAm { get; set; }
        public Nullable<double> KGThep { get; set; }
        public Nullable<double> PhiVCChatThai { get; set; }
        public Nullable<int> HeSo { get; set; }
        public Nullable<System.Guid> CongTrinhID { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string ThoiGianBatDau { get; set; }
        public string ThoiGianKetThuc { get; set; }
        public string TenHangMuc { get; set; }
    }

    public class PhanBoGiaTriCongTrinhResults
    {
        public Guid CongTrinhID { get; set; }
        public List<ThongTinHangMucPhanBo> ThongTinHangMucPhanBo { get; set; }
        public List<ChiTietPhanBoHangMuc> DanhSachCacDoi { get; set; }
    }

    public class ThongTinHangMucPhanBo // bảng ở trên
    {
        public string TenHangMuc { get; set; }
        public Guid HangMucThiCongID { get; set; }
        public Guid DMHangMucThiCongID { get; set; }
        public Nullable<double> SoCoc { get; set; }
        public Nullable<double> MetDa { get; set; }
        public Nullable<double> MetDat { get; set; }
        public Nullable<double> SoCocConLai { get; set; }
        public Nullable<double> MetDaConLai { get; set; }
        public Nullable<double> MetDatConLai { get; set; }
    }

    public class ChiTietPhanBoHangMuc
    {
        public Nullable<Guid> CongTrinhDoiID { get; set; }
        public Nullable<Guid> DoiThiCongID { get; set; }
        public string TenDoi { get; set; }
        public List<HangMucPhanBo> ChiTiet { get; set; }
    }

    public class HangMucPhanBo
    {
        public string TenHangMuc { get; set; }
        public Nullable<double> SoCoc { get; set; }
        public Nullable<double> MetDa { get; set; }
        public Nullable<double> MetDat { get; set; }
        public Nullable<double> DonGiaMetDat { get; set; }
        public Nullable<double> DonGiaMetDa { get; set; }
        public Nullable<Guid> HangMucThiCongID { get; set; }
    }

    public class DSDoiThiCongs
    {
        public Guid DoiThiCongID { get; set; }
        public string TenDoi { get; set; }
    }

    public class PhanBoGiaTriRequest
    {
        public Guid CongTrinhID { get; set; }
        public List<CongTrinhDoiPhanBoRequest> CongTrinhDoi { get; set; }
    }
    public class CongTrinhDoiPhanBoRequest
    {
        public Guid CongTrinhDoiID { get; set; }
        public List<HangMucPhanBo> ChiTiet { get; set; }
    }

    public class UpdateHangMucRequest
    {
        public Nullable<Guid> CongTrinhDoiHangMucThiCongID { get; set; }
        public Guid HangMucThiCongID { get; set; }
        public Nullable<double> PhiVCChatThai { get; set; }
        public Nullable<double> KGOngSieuAm { get; set; }
        public Nullable<double> KGThep { get; set; }
        public Nullable<double> SoCoc { get; set; }
        public Nullable<double> MetDat { get; set; }
        public Nullable<double> MetDa { get; set; }
        public string ThoiGianBatDau { get; set; }
        public string ThoiGianKetThuc { get; set; }
        public Nullable<int> HeSo { get; set; }
    }

    public class DanhSachHopDongs
    {
        public System.Guid HopDongID { get; set; }
        public string TenHopDong { get; set; }
        public int LoaiHopDong { get; set; }
        public string DinhKem { get; set; }
        public System.Guid DonViTao { get; set; }
        public System.DateTime ThoiGianTao { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> NgayBatDau { get; set; }
        public Nullable<System.DateTime> NgayKetThuc { get; set; }
        public Nullable<System.Guid> DonViLienQuan { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
        public Nullable<System.Guid> DoiTacId { get; set; }
        public string TenDonViTao { get; set; }
        public string TenCongTrinh { get; set; }
        public string TenDoiTac { get; set; }
        public string TenLoaiHopDong { get; set; }
        public Nullable<int> CapDoBaoMat { get; set; }
    }

    public class CreateOrUpdateHopDongRequest
    {
        public Nullable<Guid> HopDongID { get; set; }
        public string TenHopDong { get; set; }
        public Nullable<int> LoaiHopDong { get; set; }
        public Nullable<Guid> DonViTao { get; set; }
        public string ThoiGianTao { get; set; }
        public Nullable<Guid> DoiTacID { get; set; }
        public List<string> DSHinhAnh { get; set; }
        public Nullable<Guid> CongTrinhID { get; set; }
        public Nullable<int> CapDoBaoMat { get; set; }
    }

    public class DanhSachThiCongResults
    {
        public Guid CongTrinhID { get; set; }
        public string TenCongTrinh { get; set; }
        public int TinhTrang { get; set; }
        public string NgayKetThuc { get; set; }
        public string NgayBatDau { get; set; }
        public string TenDoi { get; set; }
        public Guid DoiThiCongID { get; set; }
        public Guid CongTrinhDoiID { get; set; }
        public string strTinhTrang { get; set; }

    }

    public class ThiCong_ThongTinChungResults
    {
        public Guid CongTrinhID { get; set; }
        public Guid DoiThiCongID { get; set; }
        public Guid CongTrinhDoiID { get; set; }
        public Nullable<double> GTHopDong { get; set; }
        public Nullable<double> GTDoiThiCong { get; set; }
        public Nullable<double> SanLuong_GT { get; set; }
        public Nullable<double> HienTai_ThietBi { get; set; }
        public Nullable<double> HienTai_CCDC { get; set; }
        public Nullable<double> HienTai_VatTu { get; set; }
        public Nullable<double> HienTai_NhanCong { get; set; }
        public Nullable<double> HienTai_VanChuyen { get; set; }
        public Nullable<double> HienTai_Khac { get; set; }
        public Nullable<double> HienTai_PhanBo { get; set; }
        public Nullable<double> HienTai_CPQLDA { get; set; }
        public Nullable<double> ChenhLech_ThietBi { get; set; }
        public Nullable<double> ChenhLech_CCDC { get; set; }
        public Nullable<double> ChenhLech_VatTu { get; set; }
        public Nullable<double> ChenhLech_NhanCong { get; set; }
        public Nullable<double> ChenhLech_VanChuyen { get; set; }
        public Nullable<double> ChenhLech_Khac { get; set; }
        public Nullable<double> ChenhLech_PhanBo { get; set; }
        public Nullable<double> ChenhLech_CPQLDA { get; set; }
        public Nullable<double> LoiNhuanTamThoi { get; set; }
        public Nullable<double> DuToan_ThietBi { get; set; }
        public Nullable<double> DuToan_CCDC { get; set; }
        public Nullable<double> DuToan_VatTu { get; set; }
        public Nullable<double> DuToan_NhanCong { get; set; }
        public Nullable<double> DuToan_VanChuyen { get; set; }
        public Nullable<double> DuToan_Khac { get; set; }
        public Nullable<double> DuToan_CPQLDA { get; set; }
        public Nullable<double> DuToan_PhanBo { get; set; }
    }

    public class ThiCong_NhanCongResults
    {
        public string TenNhanVien { get; set; }
        public string TenChucVu { get; set; }
        public string ThoiGianDC { get; set; }
        public string ThoiGianDi { get; set; }
    }
    public class ThiCong_SanLuongResults
    {
        public string TenHangMuc { get; set; }
        public Nullable<double> MetDa { get; set; }
        public Nullable<double> MetDat { get; set; }
        public Nullable<double> MetRong { get; set; }
        public Nullable<double> SoCoc { get; set; }
        public Nullable<double> DGMetDa { get; set; }
        public Nullable<double> DGMetDat { get; set; }
        public Nullable<double> DGMetRong { get; set; }
        public Nullable<double> TTMetDa { get; set; }
        public Nullable<double> TTMetDat { get; set; }
        public Nullable<double> TTMetRong { get; set; }
        public Nullable<double> TT { get; set; }
        public Nullable<DateTime> ThoiGian { get; set; }
        public Nullable<Guid> NhanVienID { get; set; }
        public Nullable<Guid> CocThiCongID { get; set; }
        public Nullable<Guid> HangMucID { get; set; }
        public Nullable<Guid> CongTrinhDoiHangMucID { get; set; }
        public string TenNhanVien { get; set; }
        public string strThoiGian { get; set; }
        public string TenCoc { get; set; }
    }

    public class ThiCong_VatTuResults
    {
        public string MaDeNghi { get; set; }
        public string strThoiGian { get; set; }
        public Nullable<DateTime> ThoiGian { get; set; }
        public string TenNhanVien { get; set; }
        public string TenVatTu { get; set; }
        public string TenVietTat { get; set; }
        public string TenDonVi { get; set; }
        public int? LoaiHinh { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<double> TongTien { get; set; }
    }
    public class ThiCong_CCDCV2Results
    {
        public string MaDeNghi { get; set; }
        public string strThoiGian { get; set; }
        public Nullable<DateTime> ThoiGian { get; set; }
        public string TenNhanVien { get; set; }
        public string TenCCDC { get; set; }
        public string TenVietTat { get; set; }
        public string TenDonVi { get; set; }
        public int? LoaiHinh { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<double> DonGia { get; set; }
        public Nullable<double> TongTien { get; set; }
    }
    public class ThiCong_VanChuyenResults
    {
        public string BienSoXe { get; set; }
        public string LaiXe { get; set; }
        public string NoiDi { get; set; }
        public string NoiDen { get; set; }
        public Nullable<double> ChiPhi { get; set; }
        public string NoiDung { get; set; }
        public string strThoiGian { get; set; }
        public Nullable<DateTime> ThoiGian { get; set; }

    }
    public class CongTrinh_KyHopDongResult
    {
        public Guid CongTrinhID { get; set; }
        public Nullable<double> TongGiaTriHopDong { get; set; }
        public Nullable<double> GiaTriKhauTru { get; set; }
        public Nullable<double> ChiPhiQuanLyDoanhNghiep { get; set; }//%
        public Nullable<double> ChiPhiKhac { get; set; }//%
        public List<CongTrinh_KyHopDong_Items> ThongTinGiaTriKhauTru { get; set; }
        public List<CongTrinh_KyHopDong_ChiPhiThiCong_Items> ChiPhiThiCong { get; set; }
        public bool DaKyHopDong { get; set; }
        public Nullable<double> TongChiPhiKeHoach { get; set; }
        public string NgayKyHopDong { get; set; }
        ///
        public Nullable<double> Thep_KL { get; set; }
        public Nullable<double> Thep_DonGia { get; set; }
        public Nullable<double> BeTong_KL { get; set; }
        public Nullable<double> BeTong_DonGia { get; set; }
        public Nullable<double> OSA_D60_KL { get; set; }
        public Nullable<double> OSA_D60_DonGia { get; set; }
        public Nullable<double> OSA_D113_KL { get; set; }
        public Nullable<double> OSA_D113_DonGia { get; set; }
        public Nullable<double> CUTNOI_D60_KL { get; set; }
        public Nullable<double> CUTNOI_D60_DonGia { get; set; }
        public Nullable<double> CUTNOI_D113_KL { get; set; }
        public Nullable<double> CUTNOI_D113_DonGia { get; set; }
        public Nullable<double> NAPBIT_D60_KL { get; set; }
        public Nullable<double> NAPBIT_D60_DonGia { get; set; }
        public Nullable<double> NAPBIT_D113_KL { get; set; }
        public Nullable<double> NAPBIT_D113_DonGia { get; set; }
        public string strNgayKyHopDong { get; set; }
    }
    public class CongTrinh_KyHopDong_Items
    {
        public string TenHangMuc { get; set; }
        public Nullable<double> GiaTri { get; set; }
    }

    public class CongTrinh_KyHopDong_ChiPhiThiCong_Items
    {
        public Nullable<Guid> CongTrinhID { get; set; }
        public Nullable<Guid> DoiThiCongID { get; set; }
        public string TenDoi { get; set; }
        public Nullable<double> ThietBi { get; set; }
        public Nullable<double> CCDC { get; set; }
        public Nullable<double> VatTu { get; set; }
        public Nullable<double> NhanCong { get; set; }
        public Nullable<double> CPPhanBo { get; set; }
        public Nullable<double> CPQLDA { get; set; }
        public Nullable<double> CPVanChuyen { get; set; }
        public Nullable<double> CPKhac { get; set; }
    }
    public partial class DuAnModel
    {
        public Nullable<Guid> DuAnID { get; set; }
        public string TenDuAn { get; set; }
        public string NgayTao { get; set; }
        public int TinhTrang { get; set; }
        public string GhiChu { get; set; }
        public Nullable<Guid> DoiTacID { get; set; }
        public List<lstImageHopDong> ListHinhAnh { get; set; }
        public List<DuAnItemModel> items { get; set; }
    }
    public partial class DuAnItemModel
    {
        public Guid VatTuID { get; set; }
        public string TenDuAn { get; set; }
        public string NgayTao { get; set; }
        public double SoLuong { get; set; }
        public double DonGiia { get; set; }
    }
    
}
