﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.NghiepVu.QuanLyDuAn
{
    public class LichSuDeNghiModel
    {
        public int TrangThai { get; set; }
        public string NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public string ThoiGian { get; set; }
    }
}
