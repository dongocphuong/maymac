﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.NghiepVu
{
    public class DanhSachNguyenLieuDinhNghiaModel
    {
         public int LoaiHinh { get; set; }
         public string MaNguyenLieu { get; set; }
         public string TenNguyenLieu { get; set; }
         public Guid NguyenLieuID { get; set; }
         public string TenDonVi { get; set; }
         public string AnhDaiDien { get; set; }
         public Nullable<double> SoLuong { get; set; }
         public Nullable<int> LoaiVai { get; set; }
         public string TenLoaiVai { get; set; }
    }
    public class DanhSachNguyenLieuModel
    {
        public string SanPhamID { get; set; }
        public string Mau { get; set; }
        public string VaiChinh { get; set; }
        public List<ThongTinNguyenLieuModel> VaiChinhID { get; set; }
        public string VaiPhoiLot { get; set; }
        public List<ThongTinNguyenLieuModel> VaiPhoiLotID { get; set; }
        public string VaiPhoiP1 { get; set; }
        public List<ThongTinNguyenLieuModel> VaiPhoiP1ID { get; set; }
        public string VaiPhoiP2 { get; set; }
        public List<ThongTinNguyenLieuModel> VaiPhoiP2ID { get; set; }
        public string ChiMay { get; set; }
        public List<ThongTinNguyenLieuModel> ChiMayID { get; set; }
        public string Mac { get; set; }
        public List<ThongTinNguyenLieuModel> MacID { get; set; }
        public string Cuc { get; set; }
        public List<ThongTinNguyenLieuModel> CucID { get; set; }
        public string PKKhac { get; set; }
        public List<ThongTinNguyenLieuModel> PKKhacID { get; set; }
        public string NguyenLieu { get; set; }
    }
    public class DanhSachSizeModel
    {
        public string Size { get; set; }
    }
    public class ThongTinNguyenLieuModel
    {
        public string TenNguyenLieu { get; set; }
        public string NguyenLieuID { get; set; }
    }
}
