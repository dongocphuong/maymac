﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.Excel
{
    public class FilterModel
    {
        public string field { get; set; }
        public string Operator { get; set; }
        public string value { get; set; }
    }
}
