﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.Excel.BaoCao
{
    public class ExcelModelsBaoCaoDonDatHang
    {
        public int STT { get; set; }
        public string TenDoiTac { get; set; }
        public string MaSoThue { get; set; }
        public string SDT { get; set; }
        public string SoTaiKhoan { get; set; }
        public string DiaChi { get; set; }
        public string TenSanPham { get; set; }
        public string Email { get; set; }
        public string MaDonHang { get; set; }
        public string MaSanPham { get; set; }
        public string DacDiem { get; set; }
        public string Size { get; set; }
        public string Mau { get; set; }
        public double SoLuong { get; set; }
        public double? SoLuongThucTe { get; set; }
        public double? DonGiaThucTe { get; set; }
        public double? ThanhTien { get; set; }
        public string YeuCauKhac { get; set; }
    }
}
