﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.Excel
{
    public class ExcelLichSuNhapVatTu
    {
        public int STT { get; set; }
        public string TenDonVi { get; set; }
        public string TenNguyenLieu { get; set; }
        public string MaNguyenLieu { get; set; }
        public string TenDoiTac { get; set; }
        public string DanhSachHinhAnh { get; set; }
        public string AnhDaiDien { get; set; }
        public DateTime? ThoiGian { get; set; }
        public string MaPhieuNhap { get; set; }
        public double GiaNhap { get; set; }
        public double SoLuong { get; set; }
        public double ThanhTien { get; set; }
        public string TenNhanVien { get; set; }
        public string NoiDung { get; set; }
    }
    public class ExcelLichSuXuatVatTu
    {
        public int STT { get; set; }
        public string TenDonVi { get; set; }
        public string TenNguyenLieu { get; set; }
        public string MaNguyenLieu { get; set; }
        public string TenDoiTac { get; set; }
        public string DanhSachHinhAnh { get; set; }
        public string AnhDaiDien { get; set; }
        public DateTime? ThoiGian { get; set; }
        public string MaPhieuNhap { get; set; }
        public double GiaNhap { get; set; }
        public double? SoLuong { get; set; }
        public double ThanhTien { get; set; }
        public string TenNhanVien { get; set; }
        public string NoiDung { get; set; }
    }
    public class ExcelLichSuNhapKhoSanPham
    {
        public int STT { get; set; }
        public string TenDonVi { get; set; }
        public string TenSanPham { get; set; }
        public string MaSanPham { get; set; }
        public string TenDoiTac { get; set; }
        public string DanhSachHinhAnh { get; set; }
        public string AnhDaiDien { get; set; }
        public DateTime? ThoiGian { get; set; }
        public string MaPhieuNhap { get; set; }
        public double? GiaNhap { get; set; }
        public double? SoLuong { get; set; }
        public double? ThanhTien { get; set; }
        public string TenNhanVien { get; set; }
        public string NoiDung { get; set; }
    }
    public class ExcelLichSuXuatKhoSanPham
    {
        public int STT { get; set; }
        public string TenDonVi { get; set; }
        public string TenSanPham { get; set; }
        public string MaSanPham { get; set; }
        public string TenDoiTac { get; set; }
        public string DanhSachHinhAnh { get; set; }
        public string AnhDaiDien { get; set; }
        public DateTime? ThoiGian { get; set; }
        public string MaPhieuNhap { get; set; }
        public double GiaNhap { get; set; }
        public double? SoLuong { get; set; }
        public double ThanhTien { get; set; }
        public string TenNhanVien { get; set; }
        public string NoiDung { get; set; }
    }

    public class ExcelDMNguyenLieu
    {
        public int STT { get; set; }
        public string TenNguyenLieu { get; set; }
        public string MaNguyenLieu { get; set; }
        public string TenDonVi { get; set; }
        public double? DonGia { get; set; }
    }

    public class ExcelDMThanhPham
    {
        public int STT { get; set; }
        public string TenThanhPham { get; set; }
        public string MaThanhPham { get; set; }
        public string TenDonVi { get; set; }
        public double? DonGia { get; set; }
    }

    public class ExcelDMSanPham
    {
        public int STT { get; set; }
        public string TenSanPham { get; set; }
        public string MaSanPham { get; set; }
        public string TenDonVi { get; set; }
        public double? DonGia { get; set; }
    }

    public class ExcelDanhSachDonHang
    {
        public int STT { get; set; }
        public string TenDonHang { get; set; }
        public string MaDonHang { get; set; }
        public int? TrangThai { get; set; }
        public string TenDoiTac { get; set; }
        public double? GiaTriDonHang { get; set; }
        public string DiaDiemGiaoHang { get; set; }
        public DateTime? ThoiGian { get; set; }
    }
}
