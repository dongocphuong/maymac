﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt
{
    public class VanBanModels
    {
        public System.Guid VanBanID { get; set; }
        public string TieuDe { get; set; }
        public string NoiDung { get; set; }
        public string FileDinhKem { get; set; }
        public System.Guid NguoiTaoID { get; set; }
        public System.DateTime ThoiGianTao { get; set; }
        public bool IsActive { get; set; }
        public int? LoaiVanBan { get; set; }
        public List<lstImageVanBan> ListHinhAnh { get; set; }
    }
    public class lstImageVanBan
    {
        public string img { get; set; }
        public string name { get; set; }
    }
}
