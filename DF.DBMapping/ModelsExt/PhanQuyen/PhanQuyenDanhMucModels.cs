﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.PhanQuyen
{
    public class PhanQuyenDanhMucModels
    {
        public Nullable<System.Guid> GroupGuid { get; set; }
        public string GroupName { get; set; }
        public Nullable<int> RuleType { get; set; }
        public Nullable<long> Code { get; set; }
        public string UrlLink { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
        public Nullable<int> Nhom { get; set; }
        public Nullable<Guid> GroupParent { get; set; }
        public string KeyLanguage { get; set; }
        public string MoTa { get; set; }
        public string Icon { get; set; }
        public bool IsHeader { get; set; }
        public string STT { get; set; }
        public Nullable<int> STT2 { get; set; }

    }

    public class PhanQuyenByUserModels
    {
        public Nullable<System.Guid> GroupGuid { get; set; }
        public string GroupName { get; set; }
        public Nullable<int> Nhom { get; set; }
        public Nullable<Guid> GroupParent { get; set; }
        public string KeyLang { get; set; }
        public bool IsHeader { get; set; }
        public bool IsCheck { get; set; }
        public string STT { get; set; }
        public Nullable<int> STT2 { get; set; }
    }
    public class PhanQuyenByChucVuModels
    {
        public Nullable<System.Guid> ChucVuID { get; set; }
        public Nullable<System.Guid> GroupID { get; set; }
        public string GroupName { get; set; }
        public Nullable<int> Nhom { get; set; }
        public string KeyLang { get; set; }
        public bool IsCheck { get; set; }
        public  Nullable<int> STT { get; set; }
    }

    public class PhanQuyenByNhanVienPhongChatModels
    {
        public Nullable<System.Guid> NhanVienPhongChatID { get; set; }
        public Nullable<System.Guid> GroupID { get; set; }
        public string GroupName { get; set; }
        public Nullable<int> Nhom { get; set; }
        public string KeyLang { get; set; }
        public bool IsCheck { get; set; }
        public Nullable<int> STT { get; set; }
    }
    public class UserPhanQuyenModels
    {
        public List<PhanQuyenByUserModels> Quyen { get; set; }
        public Guid NhanVienID { get; set; }
        public Guid UserID { get; set; }
    }

    public class AddPhanQuyen_User
    {
        public Nullable<Guid> NhanVienID { get; set; }
        public Nullable<Guid> UserID { get; set; }
        public Nullable<int> LoaiHinh { get; set; }
        public List<ItemQuyen> items { get; set; }
    }
    public class AddPhanQuyen_ChucVu
    {
        public Nullable<Guid> ChucVuID { get; set; }
        public List<ItemQuyen> items { get; set; }
    }
    public class PhanQuyenNhanVienPhongChat
    {
        public Nullable<Guid> NhanVienPhongChatID { get; set; }
        public List<ItemQuyen> items { get; set; }
    }
    public class ItemQuyen
    {
        public Nullable<Guid> GroupGuid { get; set; }
    }
}
