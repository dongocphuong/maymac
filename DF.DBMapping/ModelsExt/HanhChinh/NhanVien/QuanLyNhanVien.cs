﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace DF.DBMapping.ModelsExt
{
    public class QuanLyNhanVienModel
    {
        public System.Guid NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public System.Guid ChucVu { get; set; }
        public string TenChucVu { get; set; }
        public string NgaySinh { get; set; }
        public int GioiTinh { get; set; }
        public string SDT { get; set; }
        public string Cmt { get; set; }
        public int TinhTrang { get; set; }
        public string BangCap { get; set; }
        public int LoaiHopDong { get; set; }
        public bool IsActive { get; set; }
        public string NgayKetThucHd { get; set; }
        public Nullable<bool> DaKyHd { get; set; }
        public string GhiChu { get; set; }
        public Nullable<bool> DaNopBang { get; set; }
        public Nullable<bool> DaNopCmt { get; set; }
        public Guid? DoiThiCongID { get; set; }
        public string CongTrinhID { get; set; }
        public string TenDoi { get; set; }
        public string TenCongTrinhThiCong { get; set; }
        public string DiaChi { get; set; }
        public string NgayDiLam { get; set; }
        public string NgayThoiViec { get; set; }
        public string DsHinhAnh { get; set; }
        public string TenNganHang { get; set; }
        public string SoTaiKhoanNganHang { get; set; }
        public string NgayCapCMT { get; set; }
        public string NoiCapCMT { get; set; }
        public string NganhNghe { get; set; }
        public string Avatar { get; set; }
        public string ThoiGianDieuChuyen { get; set; }
        public string MaSoThue { get; set; }
        public string TruongCapBang { get; set; }
        public string UserName { get; set; }
        public int? SoNgayKhaiCong { get; set; }
        public int? SoNgayGioiHan { get; set; }
        public List<CongTrinhModelMap> DSCongTrinh { get; set; }


    }
    public class CongTrinhModelMap
    {
        public Guid? value { get; set; }
        public string text { get; set; }
    }

    public class UpdateThongTinNhanVienModel
    {
        public string TenNhanVien { get; set; }
        public string Avatar { get; set; }
        public string NgaySinh { get; set; }
        public int GioiTinh { get; set; }
        public string SDT { get; set; }
        public string TenNganHang { get; set; }
        public string SoTaiKhoanNganHang { get; set; }
        public string MaSoThue { get; set; }
        public string CMND { get; set; }
        public string NgayCapCMND { get; set; }
        public string NoiCapCMND { get; set; }
        public string NganhNghe { get; set; }
        public string DiaChi { get; set; }
        public Guid? NhanVienID { get; set; }
        public Nullable<Guid> ChucVuID { get; set; }
        public Nullable<int> LoaiHopDong { get; set; }
        public int TinhTrang { get; set; }
        public Nullable<DateTime> NgayDiLam { get; set; }
        public Nullable<DateTime> NgayHetHanHopDong { get; set; }
        public Nullable<DateTime> NgayThoiViec { get; set; }
        public Nullable<int> SoNgayKhaiCong { get; set; }
        public Nullable<int> SoNgayGioiHan { get; set; }
        public string TruongCapBang { get; set; }
        public int? Lever { get; set; }
        public Nullable<Guid> ChucVu { get; set; }
    }

    public class PhepNhanVienAddRequets
    {
        public string Nam { get; set; }
        public int IsCongDon { get; set; }
        public List<itemsPhepNhanVien> items { get; set; }
    }
    public class itemsPhepNhanVien
    {
        public int SoPhep { get; set; }
        public Nullable<Guid> NhanVienID { get; set; }
    }


    public class QuanLyNhanVienModel_New
    {
        public System.Guid NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public Nullable<System.Guid> ChucVu { get; set; }
        public string TenChucVu { get; set; }
        public string NgaySinh { get; set; }
        public string strGioiTinh { get; set; }
        public string SDT { get; set; }
        public string Cmt { get; set; }
        public string strTinhTrang { get; set; }
        public string BangCap { get; set; }
        public string strLoaiHopDong { get; set; }
        public string NgayKetThucHD { get; set; }
        public string GhiChu { get; set; }
        public Nullable<System.Guid> DoiThiCongID { get; set; }
        public Nullable<System.Guid> CongTrinhID { get; set; }
        public string TenDoi { get; set; }
        public string TenCongDoi { get; set; }
        public string DiaChi { get; set; }
        public string NgayDiLam { get; set; }
        public string NgayThoiViec { get; set; }
        public string DsHinhAnh { get; set; }
        public string TenNganHang { get; set; }
        public string SoTaiKhoanNganHang { get; set; }
        public string NgayCapCMT { get; set; }
        public string NoiCapCMT { get; set; }
        public string NganhNghe { get; set; }
        public string Avatar { get; set; }
        public string ThoiGianDieuChuyen { get; set; }
        public string MaSoThue { get; set; }
        public string TruongCapBang { get; set; }
        public string UserName { get; set; }
        public int? SoNgayKhaiCong { get; set; }
        public int? SoNgayGioiHan { get; set; }
        public int? Lever { get; set; }
        public string Email { get; set; }

    }
}