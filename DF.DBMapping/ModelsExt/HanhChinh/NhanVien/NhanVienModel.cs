﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DF.DBMapping.ModelsExt
{
    public class NhanVienModel
    {
        public int STT { get; set; }
        public string NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public DateTime NgaySinh { get; set; }
        public int GioiTinh { get; set; }
        public string SDT { get; set; }
        public string Email { get; set; }
        public string Cmt { get; set; }
        public string DiaChi { get; set; }
        public string ChucVu { get; set; }
        public string PhongBanId { get; set; }
        public int TinhTrang { get; set; }
        public string BangCap { get; set; }
        public int LoaiHopDong { get; set; }
        public DateTime NgayDiLam { get; set; }
        public DateTime NgayThoiViec { get; set; }
        public string LyDoThoiViec { get; set; }
        public bool IsActive { get; set; }
        public Guid? CongtrinhId { get; set; }
        public Guid? CaId { get; set; }
    }
    public class NhanVienModels
    {
        public System.Guid NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public System.DateTime NgaySinh { get; set; }
        public int GioiTinh { get; set; }
        public string SDT { get; set; }
        public string Email { get; set; }
        public string Cmt { get; set; }
        public string DiaChi { get; set; }
        public int TinhTrang { get; set; }
        public string BangCap { get; set; }
        public Nullable<int> LoaiHopDong { get; set; }
        public Nullable<System.DateTime> NgayDiLam { get; set; }
        public Nullable<System.DateTime> NgayThoiViec { get; set; }
        public string LyDoThoiViec { get; set; }
        public Nullable<System.DateTime> NgayKetThucHd { get; set; }
        public Nullable<bool> DaKyHd { get; set; }
        public string GhiChu { get; set; }
        public Nullable<bool> DaNopBang { get; set; }
        public Nullable<bool> DaNopCmt { get; set; }
        public string DsHinhAnh { get; set; }
        public bool IsActive { get; set; }
        public Nullable<System.DateTime> NgayCapCMT { get; set; }
        public string NoiCapCMT { get; set; }
        public string NamCapBang { get; set; }
        public string NganhNghe { get; set; }
        public string TenNganHang { get; set; }
        public string SoTaiKhoanNganHang { get; set; }
        public Nullable<System.DateTime> NgayHetHanCongChungBang { get; set; }
        public Nullable<System.DateTime> NgayHHGiayKhamSK { get; set; }
        public Nullable<System.DateTime> NgayHHATLD { get; set; }
        public string TruongCapBang { get; set; }
        public string MaSoThue { get; set; }
        public string HinhAnhKemTheo { get; set; }
        public string Avatar { get; set; }
        public Nullable<int> SoNgayGioiHan { get; set; }
        public Nullable<int> IsNhanVienDangKienLeDong { get; set; }
        public Nullable<int> LoaiNhanVienDangKien { get; set; }
    }
    public class NhanVienDoiDieuChuyenModel
    {
        public Guid? NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public string NgaySinh { get; set; }
        public string DiaChi { get; set; }
        public string CMT { get; set; }
        public string GioiTinh { get; set;}
        public string SDT { get; set; }
        public Guid NhanVienDoiThiCongId { get; set; }
        public Guid DoiThiCongID { get; set; }
        public string ChucVuTen { get; set; }
        public Nullable<Guid> ChucVuID { get; set; }
    }
    public class NhanVienDoiDieuChuyenModel2
    {
        public Guid? NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public string NgaySinh { get; set; }
        public string DiaChi { get; set; }
        public string CMT { get; set; }
    }
    public class NhanVienDieuChuyen
    {
        public string TenDoi { get; set; }
        public string ThoiGianDieuChuyen { get; set; }
        public string ChucVu { get; set; }
        public string TenCongTrinh { get; set; }
    }

    public class DieuChinhLuongNhanVienModels
    {
        public Nullable<System.Guid> LuongNVID { get; set; }
        public Nullable<System.Guid> NhanVienID { get; set; }
        public Nullable<System.DateTime> ThoiGianTinh { get; set; }
        public Nullable<double> LuongCoBan { get; set; }
        public Nullable<double> LuongNgoaiGio { get; set; }
        public Nullable<double> LuongBHXH { get; set; }
        public Nullable<System.DateTime> ThoiGianHetHieuLuc { get; set; }
        public int HinhThucTinhLuong { get; set; }
        public Nullable<int> SoNguoiGiamTruGC { get; set; }
        public Nullable<int> VanPhongCongTrinh { get; set; }
        public Nullable<int> LoaiHopDong { get; set; }
        public Nullable<System.Guid> NguoiNhapID { get; set; }
        public Nullable<System.Guid> NguoiXoaID { get; set; }
        public string strHinhThucTinhLuong { get; set; }
        public string strLoaiHopDong { get; set; }
        public string strThoiGianBD { get; set; }
        public string strThoiGianKT { get; set; }
        public string strLoaiVanPhong { get; set; }
    }

    public class BangCap_BienBanModel
    {
        public string ItemID { get; set; }
        public string Ten { get; set; }
        public string NhanVienID { get; set; }
        public List<lstImageHopDong> ListHinhAnh { get; set; }

    }
    public class ChamCongModels
    {
        public Nullable<System.Guid> ChamCongID { get; set; }
        public Nullable<System.Guid> NhanVienID { get; set; }
        public Nullable<double> SoCong { get; set; }
        public string ThoiGian { get; set; }
    }
}
