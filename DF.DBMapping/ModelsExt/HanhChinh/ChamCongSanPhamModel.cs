﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DBMapping.ModelsExt.HanhChinh
{
    public class ChamCongSanPhamModel
    {
        public Nullable<System.Guid> ChamCongSanPhamID { get; set; }
        public string ThoiGian { get; set; }
        public Nullable<System.Guid> NhanVienID { get; set; }
        public string MaSanPham { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public Nullable<double> KhoangThoiGian { get; set; }
        public Nullable<double> DonGia { get; set; }
    }
}
