﻿using DF.DataAccess;
using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;
using DFWeb.Common;
using DFWeb.Security;
using System.Data.SqlClient;
using DFWeb.Models.ReportModels;
using FlexCel.Core;
using System.IO;
using FlexCel.XlsAdapter;
using FlexCel.Report;
using System.Transactions;
using DFWeb.Models.CongTrinhPhongBan;
using DF.Utilities;
using DFWeb.Utils;
using DFSCommon.Models.Enums;
using DFSCommon;
using Webservice.FCMModules;
using System.Threading.Tasks;
using DFWeb.Models;
using DF.DataAccess.DBML;
using Excel;

namespace DFWeb.Controllers.QLCongTrinhPhongBan
{
    [CustomAuthorize(Roles = "2014")]
    public class QuanLyPhongBanController : Controller
    {
        //
        // GET: /QuanLyPhongBan/
        AccountInfo uerx = CookiePersister.getAcountInfo();
        protected UnitOfWork unitofwork;
        public ActionResult Index()
        {
            ViewBag.Title_Function = Models.Language.GetText("quanlyphongban");
            return View();
        }
        public JsonResult LayDanhSachCongTrinh()
        {

            DBMLDFDataContext db = new DBMLDFDataContext();
            var data = db.Web_HanhChinh_LayDanhSachPhongBan().ToList();
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult AddOrUpdateCongTrinh(string data)
        {
            string message = "";
            PhongBan phongban = new PhongBan();
            Guid PhongBanID = Guid.NewGuid();
            unitofwork = new UnitOfWork();
            try
            {
                PhongBan model = new JavaScriptSerializer().Deserialize<PhongBan>(data);
                var temp = unitofwork.Context.PhongBans.Find(model.PhongBanID); //tim ra đối tượng đó từ cái id của  obj
                using (TransactionScope tran = new TransactionScope())
                {
                    if (temp == null)//Thêm
                    {
                        if ((from a in unitofwork.Context.PhongBans where a.TenPhong == model.TenPhong && a.IsActive == true select a.PhongBanID).ToList().Count > 0)
                        {
                            return Json(new { code = "error", message = Models.Language.GetText("mgs_tenphongbandatontai") });
                        }

                        //Insert Thiết bị
                        phongban.PhongBanID = PhongBanID;
                        phongban.IsActive = true;
                        phongban.TenPhong = model.TenPhong;

                        unitofwork.PhongBans.Add(phongban);

                        message = Models.Language.GetText("mgs_themthanhcong");
                    }
                    else//Cập nhật
                    {
                        if ((from a in unitofwork.Context.PhongBans where a.TenPhong == model.TenPhong && a.IsActive == true && a.PhongBanID != model.PhongBanID select a.PhongBanID).ToList().Count > 0)
                        {
                            return Json(new { code = "error", message = Models.Language.GetText("mgs_tenphongbandatontai") });
                        }

                        var TenCu = temp.TenPhong;
                        temp.TenPhong = model.TenPhong;
                        unitofwork.PhongBans.Update(temp);
                        message = Models.Language.GetText("mgs_capnhatthanhcong");
                    }

                    unitofwork.Save();
                    tran.Complete();
                    return Json(new { code = "success", message = message });
                }
            }
            catch (Exception ex) { return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhxulydulieu") }); }
        }
        public JsonResult delCongTrong(string data)
        {
            unitofwork = new UnitOfWork();
            PhongBan duan = new PhongBan();
            try
            {
                PhongBan model = new JavaScriptSerializer().Deserialize<PhongBan>(data);
                var temp = unitofwork.Context.PhongBans.Find(model.PhongBanID); //tim ra đối tượng đó từ cái id của  obj

                using (TransactionScope tran = new TransactionScope())
                {
                    temp.IsActive = false;
                    unitofwork.PhongBans.Update(temp);
                    //unitofwork.CongTrinhs.XoaCongTrinh(model.CongTrinhId.Value);
                    unitofwork.Save();
                    tran.Complete();
                    return Json(new { code = "success", message = Models.Language.GetText("mgs_xoathanhcongphongban") }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhxoabanghi") }, JsonRequestBehavior.AllowGet);
            }
        }
        //public clsExcelResult XuatTemplate()
        //{
        //    string fileName = "";
        //    fileName = "Template.xlsx";
        //    string urlTemp = "";
        //    if (Language.GetKeyLang() == "cn")
        //    {
        //        urlTemp = Server.MapPath("~/TempExcel/Import/PhongBanTemplateCN.xlsx");
        //    }
        //    else
        //    {
        //        urlTemp = Server.MapPath("~/TempExcel/Import/PhongBanTemplate.xlsx");
        //    }
        //    clsExcelResult clsResult = new clsExcelResult();
        //    var temp = new XlsFile(true);
        //    temp.Open(urlTemp);
        //    ExcelFile xls = temp;
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        xls.Save(ms);
        //        ms.Position = 0;
        //        clsResult.ms = ms;
        //        clsResult.FileName = fileName;
        //        clsResult.type = "xlsx";
        //        return clsResult;
        //    }
        //}
        //public JsonResult CheckKetQua()
        //{
        //    string iUploadedCnt = "";
        //    string sPath = "";
        //    sPath = Server.MapPath("~/TempExcel/Import/TempFile/");
        //    unitofwork = new UnitOfWork();
        //    System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
        //    string autoky = ConvertDate.autokey();
        //    // CHECK THE FILE COUNT.
        //    for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
        //    {
        //        System.Web.HttpPostedFile hpf = hfc[iCnt];
        //        if (hpf.ContentLength > 0)
        //        {
        //            if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
        //            {
        //                string ID = Guid.NewGuid().ToString();
        //                iUploadedCnt = (sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
        //                if (hpf != null && hpf.ContentLength > 0)
        //                {
        //                    Stream stream = hpf.InputStream;
        //                    IExcelDataReader reader = null;
        //                    if (hpf.FileName.EndsWith(".xls"))
        //                    {
        //                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
        //                    }
        //                    else if (hpf.FileName.EndsWith(".xlsx"))
        //                    {
        //                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
        //                    }
        //                    else
        //                    {

        //                    }
        //                    reader.IsFirstRowAsColumnNames = true;
        //                    DataSet result = reader.AsDataSet();
        //                    var listct = result.Tables[0].AsEnumerable().Select(p => new
        //                    {
        //                        TenDuAn = p[0].ToString(),
        //                    });
        //                    reader.Close();
        //                    return Json(new { data = listct }, JsonRequestBehavior.AllowGet);
        //                }
        //                // hpf.SaveAs(iUploadedCnt);

        //            }
        //        }
        //    }
        //    var message = "";
        //    return Json(new { url = iUploadedCnt, message = message }, JsonRequestBehavior.AllowGet);
        //}
        //public JsonResult ImportPhongBan(string data)
        //{
        //    //Loaihinh=1: Toàn bộ;Loaihinh=2:Bỏ qua trùng
        //    unitofwork = new UnitOfWork();
        //    dynamic model = new JavaScriptSerializer().Deserialize<dynamic>(data);
        //    for (int i = 0; i < model.Length; i++)
        //    {

        //        DuAn duan = new DuAn();
        //        Guid DuAnID = Guid.NewGuid();
        //        duan.DuAnID = DuAnID;
        //        duan.Dept = 1;
        //        duan.IsActive = true;
        //        duan.TenDuAn = model[i]["TenDuAn"];
        //        unitofwork.DuAns.Add(duan);
        //        unitofwork.Save();
        //    }
        //    return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        //}

        //private string getvalue(System.Array row, int i)
        //{
        //    try
        //    {
        //        return row.GetValue(1, i).ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}
        //public JsonResult DeleteFile(string url)
        //{
        //    if (System.IO.File.Exists(url))
        //    {
        //        System.IO.File.Delete(url);
        //    }
        //    return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult DSDoiThiCong(string CongTrinhID)
        //{
        //    unitofwork = new UnitOfWork();
        //    PhongBan_DoiThiCongModel md = new PhongBan_DoiThiCongModel();
        //    md.CongTrinhID = CongTrinhID;
        //    md.DSDoiThiCong = unitofwork.CongTrinhs.DSPhongBan_DoiThiCong(Guid.Parse(CongTrinhID));
        //    return PartialView("_DSDoiThiCong", md);

        //}

        //public JsonResult ThemDoiThiCong(string data)
        //{
        //    try
        //    {
        //        unitofwork = new UnitOfWork();
        //        AddDoi_PhongBan model = new JavaScriptSerializer().Deserialize<AddDoi_PhongBan>(data);
        //        using (TransactionScope tran = new TransactionScope())
        //        {
        //            //Update hết thành trạng thái isactive = 0 chỉ những đội có type = 0
        //            unitofwork.CongTrinhs.UpdatePhongBanDoiThiCong(model.CongTrinhID.Value);
        //            var CongTrinhDoi = (from a in unitofwork.Context.CongTrinhDois where a.CongTrinhID == model.CongTrinhID select a).ToList();
        //            foreach (var item in model.items)
        //            {
        //                bool add = true;
        //                foreach (var items in CongTrinhDoi)
        //                {
        //                    if (item.DoiThiCongID == items.DoiThiCongID)
        //                    {
        //                        CongTrinhDoi ctd = unitofwork.Context.CongTrinhDois.Find(items.CongTrinhDoiID);
        //                        ctd.IsActive = true;
        //                        unitofwork.CongTrinhDois.Update(ctd);
        //                        add = false;
        //                        break;
        //                    }
        //                }
        //                if (add)
        //                {
        //                    CongTrinhDoi ctds = new CongTrinhDoi();
        //                    ctds.CongTrinhDoiID = Guid.NewGuid();
        //                    ctds.CongTrinhID = model.CongTrinhID.Value;
        //                    ctds.DoiThiCongID = item.DoiThiCongID.Value;
        //                    ctds.IsActive = true;
        //                    unitofwork.CongTrinhDois.Add(ctds);
        //                }
        //            }

        //            unitofwork.Save();
        //            tran.Complete();
        //        }
        //        return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);

        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhxulydulieu") }, JsonRequestBehavior.AllowGet);
        //    }
        //}

    }
}
