﻿using DF.DataAccess;
using DF.DBMapping.Models;
using DF.DBMapping.ModelsExt;
using DFSCommon;
using DFSCommon.Models.Enums;
using DFWeb.Common;
using DFWeb.Models;
using DFWeb.Security;
using DFWeb.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Webservice.FCMModules;
using System.Configuration;
using System.Transactions;
using DF.DataAccess.DBML;
using FlexCel.Core;
using DFWeb.Models.ReportModels;
using FlexCel.XlsAdapter;
using DF.Utilities;
using Excel;
using DF.DBMapping.ModelsExt.NghiepVu;
using DF.DBMapping.ModelsExt.Excel;
using FlexCel.Report;
using Newtonsoft.Json;

namespace DFWeb.Controllers.DanhMuc
{
    [CustomAuthorize(Roles = "2013")]
    public class DMThanhPhamController : Controller
    {
        //
        // GET: /DMThanhPham/
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        string status;
        string code;
        public ActionResult Index()
        {
            ViewBag.Title_Function = "Danh mục thành phẩm";
            return View();
        }
        public JsonResult GetAllDataThanhPham()
        {
            unitofwork = new UnitOfWork();
            try
            {
                List<Web_DanhMucThanhPham_GetAllDataResult> lst = unitofwork.ThanhPhams.GetAllDataDMThanhPham();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayDanhSachNguyenLieu(string ThanhPhamID = "")
        {
            db = new DBMLDFDataContext();
            try
            {
                var tp = Guid.Empty;
                if (ThanhPhamID != "")
                {
                    tp = Guid.Parse(ThanhPhamID);
                }
                var lst = db.Web_ThanhPham_DinhNghiaThanhPham(tp).ToList();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayDanhSachNguyenLieuDeDinhNghia(string SanPhamID="")
        {
            db = new DBMLDFDataContext();
            try
            {

                var lst = db.Web_NguyenLieu_LayDanhSach().Select(p => new DanhSachNguyenLieuDinhNghiaModel
                {
                    AnhDaiDien = p.AnhDaiDien,
                    LoaiHinh = 1,
                    MaNguyenLieu = p.MaNguyenLieu,
                    NguyenLieuID = p.NguyenLieuID,
                    TenDonVi = p.TenDonVi,
                    TenNguyenLieu = p.TenNguyenLieu
                }).ToList();
                lst.AddRange(db.Web_DanhMucThanhPham_TheoSanPham(SanPhamID!=""?Guid.Parse(SanPhamID):Guid.Empty).Select(p => new DanhSachNguyenLieuDinhNghiaModel
                {
                    AnhDaiDien = p.AnhDaiDien,
                    LoaiHinh = 2,
                    MaNguyenLieu = p.MaThanhPham,
                    NguyenLieuID = p.ThanhPhamID,
                    TenDonVi = p.TenDonVi,
                    TenNguyenLieu = p.TenThanhPham
                }));
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDataThanhPhamID(string data)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var dt = db.Web_DanhMucThanhPham_GetAllDataThanhPhamID(Guid.Parse(data));
            return Json(new { data = dt, message = "Thành công" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddOrUpdateDMThanhPham(string data, string listdinhnghia,string SanPhamID="")
        {
            unitofwork = new UnitOfWork();
            var result = unitofwork.ThanhPhams.AddOrUpdateDMThanhPham(data, listdinhnghia, SanPhamID);
            if (result == null)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            //else if (result == 1)
            //{
            //    code = "warning";
            //    status = "Tên thành phẩm hoặc mã thành phẩm đã bị trùng!";
            //}
            else
            {
                code = "success";
                status = "Thành công!";
            }
            return Json(new { code = code, message = status, thanhpham = result }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddOrUpdateDMThanhPhamToanBoMau(string data, string listdinhnghia, string SanPhamID = "")
        {
            unitofwork = new UnitOfWork();
            var result = unitofwork.ThanhPhams.AddOrUpdateDMThanhPhamToanBoMau(data, listdinhnghia, SanPhamID);
            if (result == null)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            //else if (result == 1)
            //{
            //    code = "warning";
            //    status = "Tên thành phẩm hoặc mã thành phẩm đã bị trùng!";
            //}
            else
            {
                code = "success";
                status = "Thành công!";
            }
            return Json(new { code = code, message = status, thanhpham = result }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult XoaDMThanhPham(string data)
        {
            unitofwork = new UnitOfWork();
            int result = unitofwork.ThanhPhams.DeleteDMThanhPham(data);
            if (result == 0)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            else if (result == 1)
            {
                code = "warning";
                status = "Tên thành phẩm đã bị trùng!";
            }
            else
            {
                code = "success";
                status = "Thêm mới thành công!";
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ThemNhanhDonVi(string data)
        {
            unitofwork = new UnitOfWork();
            int rs = unitofwork.DonVis.createdata(data);
            if (rs == 0)
            {
                return Json(new { code = "error", message = "Có lỗi không thể thêm được" }, JsonRequestBehavior.AllowGet);
            }
            else if (rs == 1)
            {
                return Json(new { code = "warning", message = "Đã tồn tại đơn vị" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "success", message = "Đã thêm thành công" }, JsonRequestBehavior.AllowGet);
            }
        }


        //------------------------------------------------------
        public clsExcelResult XuatTemplate()
        {
            string fileName = "";
            fileName = "Template.xlsx";
            string urlTemp = "";
            if (Language.GetKeyLang() == "cn")
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/DanhMucThanhPhamTemplate.xls");
            }
            else
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/DanhMucThanhPhamTemplate.xls");
            }
            clsExcelResult clsResult = new clsExcelResult();
            var temp = new XlsFile(true);
            temp.Open(urlTemp);
            ExcelFile xls = temp;
            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xls";
                return clsResult;
            }
        }
        public JsonResult CheckKetQua()
        {
            string iUploadedCnt = "";
            string sPath = "";
            sPath = Server.MapPath("~/TempExcel/Import/TempFile/");
            unitofwork = new UnitOfWork();
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                if (hpf.ContentLength > 0)
                {
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        iUploadedCnt = (sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        if (hpf != null && hpf.ContentLength > 0)
                        {
                            Stream stream = hpf.InputStream;
                            IExcelDataReader reader = null;
                            if (hpf.FileName.EndsWith(".xls"))
                            {
                                reader = ExcelReaderFactory.CreateBinaryReader(stream);
                            }
                            else if (hpf.FileName.EndsWith(".xlsx"))
                            {
                                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                            }
                            else
                            {

                            }
                            reader.IsFirstRowAsColumnNames = true;
                            DataSet result = reader.AsDataSet();
                            var listct = result.Tables[0].AsEnumerable().Select(p => new
                            {
                                TenThanhPham = p[0].ToString(),
                                MaThanhPham = p[1].ToString(),
                                TenDonVi = p[2].ToString(),
                                DonGia = p[3].ToString(),
                            });
                            reader.Close();
                            return Json(new { data = listct }, JsonRequestBehavior.AllowGet);
                        }
                        // hpf.SaveAs(iUploadedCnt);

                    }
                }
            }
            var message = "";
            return Json(new { url = iUploadedCnt, message = message }, JsonRequestBehavior.AllowGet);


        }
        public JsonResult ImportDMThanhPham(string data)
        {
            //Loaihinh=1: Toàn bộ;Loaihinh=2:Bỏ qua trùng
            unitofwork = new UnitOfWork();
            dynamic model = new JavaScriptSerializer().Deserialize<dynamic>(data);
            for (int i = 0; i < model.Length; i++)
            {
                Guid donviid = Guid.Empty;
                string tendonvi = model[i]["TenDonVi"].Trim().Replace(" ", "").ToLower();
                string mathanhpham = model[i]["MaThanhPham"].Trim().Replace(" ", "").ToLower();
                var mtp = unitofwork.Context.ThanhPhams.FirstOrDefault(p => p.IsActive == true && p.MaThanhPham.Trim().Replace(" ", "").ToLower() == mathanhpham);
                var dv = unitofwork.Context.DonVis.FirstOrDefault(p => p.IsActive == true && p.TenDonVi.Trim().Replace(" ", "").ToLower() == tendonvi);
                if(mtp != null)
                {
                    continue;
                }
                if (dv != null)
                {
                    donviid = dv.DonViId;
                }
                else
                {
                    DonVi dv2 = new DonVi()
                    {
                        IsActive = true,
                        DonViId = Guid.NewGuid(),
                        TenDonVi = model[i]["TenDonVi"]
                    };
                    donviid = dv2.DonViId;
                    unitofwork.Context.DonVis.Add(dv2);
                    unitofwork.Context.SaveChanges();
                    unitofwork.Save();
                }

                ThanhPham ccv2 = new ThanhPham();
                Guid VatTuID = Guid.NewGuid();
                ccv2.ThanhPhamID = VatTuID;
                ccv2.DonViID = donviid;
                ccv2.IsActive = true;
                ccv2.TenThanhPham = model[i]["TenThanhPham"];
                ccv2.MaThanhPham = model[i]["MaThanhPham"];
                ccv2.DonGia = double.Parse(model[i]["DonGia"]);
                unitofwork.ThanhPhams.Add(ccv2);
                unitofwork.Save();
            }
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }
        private string getvalue(System.Array row, int i)
        {
            try
            {
                return row.GetValue(1, i).ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public JsonResult DeleteFile(string url)
        {
            if (System.IO.File.Exists(url))
            {
                System.IO.File.Delete(url);
            }
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }
        //Xuất excel
        public clsExcelResult XuatExcelDMThanhPham(string filters)
        {
            string fileName = "";
            fileName = "DMThanhPham.xlsx";
            string urlTemp = "";
            urlTemp = "~\\TempExcel\\DanhMuc\\DMThanhPham.xlsx";
            clsExcelResult clsResult = new clsExcelResult();
            ExcelFile xls = CreateExcel(Server.MapPath(urlTemp), filters);

            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public ExcelFile CreateExcel(String path, string filters)
        {
            XlsFile Result = new XlsFile(true);
            try
            {

                Result.Open(path);
                FlexCelReport fr = new FlexCelReport();
                unitofwork = new UnitOfWork();
                List<FilterModel> filter = JsonConvert.DeserializeObject<List<FilterModel>>(filters);

                List<Web_DanhMucThanhPham_GetAllDataResult> lst = unitofwork.ThanhPhams.GetAllDataDMThanhPham();
                var stt = 1;
                var data2 = lst.Where(p => GetDateFilter(p, filter)).Select(p => new ExcelDMThanhPham()
                {
                    STT = stt++,
                    TenThanhPham = p.TenThanhPham,
                    MaThanhPham = p.MaThanhPham,
                    TenDonVi = p.TenDonVi,
                    DonGia = p.DonGia,
                }).ToList();
                DataTable dtQuanLyVatTu = Utils.Utilities.ToDataTable(data2);
                dtQuanLyVatTu.TableName = "ChiTiet";
                fr.AddTable("ChiTiet", dtQuanLyVatTu);
                fr.Run(Result);
                fr.Dispose();
                dtQuanLyVatTu.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
        //end xuất excel 
        public string removeUnicode(string input)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
            "đ",
            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
            "í","ì","ỉ","ĩ","ị",
            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
            "ý","ỳ","ỷ","ỹ","ỵ",};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "d",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                input = input.Replace(arr1[i], arr2[i]);
                input = input.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return input;
        }

        public bool GetDateFilter(Web_DanhMucThanhPham_GetAllDataResult ls, List<FilterModel> filters)
        {
            var istrue = filters.Count() <= 0;
            foreach (var filter in filters)
            {
                var value = ls.GetType().GetProperty(filter.field).GetValue(ls);
                if (value != null)
                {
                    if (filter.Operator == "contains" || filter.Operator == "eq")
                    {
                        if (removeUnicode(value.ToString().ToLower()).Contains(removeUnicode(filter.value.ToLower())))
                        {
                            istrue = true;
                            break;
                        }
                    }
                    else
                    {
                        if (value.ToString() == filter.value)
                        {
                            istrue = true;
                            break;
                        }
                    }
                }
            }
            return istrue;
        }
    }
}
