﻿using DF.DataAccess;
using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;
using DFWeb.Common;
using DFWeb.Security;
using System.Data.SqlClient;
using DFWeb.Models.ReportModels;
using FlexCel.Core;
using System.IO;
using FlexCel.XlsAdapter;
using FlexCel.Report;
using System.Transactions;
using DFWeb.Models.CongTrinhPhongBan;
using DF.Utilities;
using DFWeb.Utils;
using DFSCommon.Models.Enums;
using DFSCommon;
using Webservice.FCMModules;
using System.Threading.Tasks;
using DFWeb.Models;
using DF.DataAccess.DBML;

namespace DFWeb.Controllers.DanhMuc
{
    [CustomAuthorize(Roles = "2016")]
    public class DMChucVuController : Controller
    {
        //
        // GET: /DMChucVu/
        protected UnitOfWork unitofwork;
        public ActionResult Index()
        {
            ViewBag.Title_Function = Models.Language.GetText("chucvu");
            return View();
        }

        public JsonResult GetDSChucVu()
        {
            UnitOfWork unitofwork = new UnitOfWork();
            var data = unitofwork.ChucVus.GetAll().Where(z => z.IsActive == true).ToList();
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddOrUpdateChucVu(string ChucVuID, string TenChucVu)
        {
            unitofwork = new UnitOfWork();
            try
            {
                if (ChucVuID == "" || ChucVuID == null)
                {
                    if (unitofwork.Context.ChucVus.Where(t => t.IsActive == true && t.TenChucVu.ToUpper() == TenChucVu.ToUpper()).Count() > 0)
                    {
                        return Json(new { code = "warning", message = Language.GetText("msg_tenchucvutontai") }, JsonRequestBehavior.AllowGet);
                    }
                    ChucVu cv = new ChucVu();
                    cv.ChucVuId = Guid.NewGuid();
                    cv.TenChucVu = TenChucVu;
                    cv.MaChucVu = 0;
                    cv.CapDo = 1;
                    cv.IsActive = true;
                    unitofwork.ChucVus.Add(cv);

                }
                else
                {
                    ChucVu cv = unitofwork.Context.ChucVus.Find(Guid.Parse(ChucVuID));
                    if (unitofwork.Context.ChucVus.Where(t => t.IsActive == true && t.TenChucVu.ToUpper() == TenChucVu.ToUpper() && t.ChucVuId != cv.ChucVuId).Count() > 0)
                    {
                        return Json(new { code = "warning", message = Language.GetText("msg_tenchucvutontai") }, JsonRequestBehavior.AllowGet);
                    }

                    cv.TenChucVu = TenChucVu;
                    cv.CapDo = 1;
                    unitofwork.ChucVus.Update(cv);
                }
                unitofwork.Save();
                return Json(new { code = "success", message = Models.Language.GetText("thanhcong") });
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhxulydulieu") });
            }
        }

        public JsonResult XoaChucVu(string ChucVuID)
        {
            unitofwork = new UnitOfWork();
            Guid CVID = Guid.Empty;
            try
            {
                CVID = Guid.Parse(ChucVuID);
                //if (unitofwork.Context.NhanVienDoiThiCongs.Where(t => t.ChucVu == CVID).Count() > 0)
                //{
                //    return Json(new { code = "warning", message = Language.GetText("msg_chucvudaduocsudung") }, JsonRequestBehavior.AllowGet);
                //}
                ChucVu cv = unitofwork.Context.ChucVus.Find(CVID);
                cv.IsActive = false;
                unitofwork.ChucVus.Update(cv);
                unitofwork.Save();
                return Json(new { code = "success", message = Models.Language.GetText("thanhcong") });
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhxulydulieu") });
            }
        }

        public clsExcelResult XuatTemplate()
        {
            string fileName = "";
            fileName = "Template.xlsx";
            string urlTemp = "";
            if (Language.GetKeyLang() == "cn")
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/DMChucVu.xlsx");
            }
            else
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/DMChucVu.xlsx");
            }
            clsExcelResult clsResult = new clsExcelResult();
            var temp = new XlsFile(true);
            temp.Open(urlTemp);
            ExcelFile xls = temp;
            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }

        public JsonResult CheckKetQua()
        {
            string iUploadedCnt = "";
            string sPath = "";
            sPath = Server.MapPath("~/TempExcel/Import/TempFile/");
            unitofwork = new UnitOfWork();
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                if (hpf.ContentLength > 0)
                {
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        iUploadedCnt = (sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        hpf.SaveAs(iUploadedCnt);

                    }
                }
            }
            var message = "";
            return Json(new { url = iUploadedCnt, message = message }, JsonRequestBehavior.AllowGet);


        }

        public JsonResult ImportChucVu(string url, int LoaiHinh = 1)
        {
            //Loaihinh=1: Toàn bộ;Loaihinh=2:Bỏ qua trùng
            unitofwork = new UnitOfWork();
            if (System.IO.File.Exists(url))
            {
                var MyApp = new Microsoft.Office.Interop.Excel.Application();
                MyApp.Visible = false;
                var MyBook = MyApp.Workbooks.Open(url);
                var MySheet = (Microsoft.Office.Interop.Excel.Worksheet)MyBook.Sheets[1]; // Explicit cast is not required here
                int lastRow = 10000;
                for (int index = 2; index <= lastRow; index++)
                {
                    System.Array v = (System.Array)MySheet.get_Range("A" +
                        index.ToString(), "B" + index.ToString()).Cells.Value;
                    var isbreak = false;
                    for (var j = 1; j <= 2; j++)
                    {
                        if (v.GetValue(1, j) != null)
                        {
                            isbreak = true;
                        };
                    }
                    if (!isbreak)
                    {
                        break;
                    }
                    var chucvu = new ChucVu()
                    {
                        TenChucVu = getvalue(v, 1),
                        IsActive = true,
                        ChucVuId = Guid.NewGuid(),
                        MaChucVu = 0

                    };
                    if (!unitofwork.Context.ChucVus.Any(p => p.TenChucVu.Trim().Replace(" ", "").ToLower() == chucvu.TenChucVu.Trim().Replace(" ", "").ToLower()))
                    {
                        unitofwork.Context.ChucVus.Add(chucvu);
                        unitofwork.Save();
                    }
                }
                MyBook.Close();
                MyApp.Workbooks.Close();
                MyApp.Quit();
                System.IO.File.Delete(url);
            }
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);


        }
        private string getvalue(System.Array row, int i)
        {
            try
            {
                return row.GetValue(1, i).ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public JsonResult DeleteFile(string url)
        {
            if (System.IO.File.Exists(url))
            {
                System.IO.File.Delete(url);
            }
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }

    }
}
