﻿using DF.DataAccess;
using DF.DBMapping.Models;
using DF.DBMapping.ModelsExt;
using DFSCommon;
using DFSCommon.Models.Enums;
using DFWeb.Common;
using DFWeb.Models;
using DFWeb.Security;
using DFWeb.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Webservice.FCMModules;
using System.Configuration;
using System.Transactions;
using DF.DataAccess.DBML;
using FlexCel.Core;
using DFWeb.Models.ReportModels;
using FlexCel.XlsAdapter;
using DF.Utilities;
using Excel;
using DF.DBMapping.ModelsExt.NghiepVu;
using DF.DBMapping.ModelsExt.Excel;
using FlexCel.Report;
using Newtonsoft.Json;

namespace DFWeb.Controllers.DanhMuc
{
    [CustomAuthorize(Roles = "1060")]
    public class NhomNguyenLieuController : Controller
    {
        //
        // GET: /NhomNguyenLieu/
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAllData()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var dt = db.Web_NhomNguyenLieu_GetAllData().OrderBy(t => t.TenNhomNguyenLieu).ToList();
            return Json(new { data = dt, message = "Thành công" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllDatabyNhomNguyenLieuID(string NhomNguyenLieuID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            Guid nnlid;
            try
            {
                nnlid = Guid.Parse(NhomNguyenLieuID);
            }
            catch
            {
                nnlid = Guid.Empty;
            }
            var dt = unitofwork.Context.NhomNguyenLieux.FirstOrDefault(p => p.NhomNguyenLieuID == nnlid);
            return Json(new { data = dt, message = "Thành công" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddOrUpdate(string data)
        {
            unitofwork = new UnitOfWork();
            var banghi = new JavaScriptSerializer().Deserialize<NhomNguyenLieu>(data);
            if(banghi.NhomNguyenLieuID == Guid.Empty)
            {
                var i = new NhomNguyenLieu();
                i.NhomNguyenLieuID = Guid.NewGuid();
                var ten = unitofwork.Context.NhomNguyenLieux.FirstOrDefault(p => p.TenNhomNguyenLieu.ToLower() == banghi.TenNhomNguyenLieu.ToLower() && p.IsActive == 1);
                if(ten == null)
                {
                    i.TenNhomNguyenLieu = banghi.TenNhomNguyenLieu;
                }
                else
                {
                    return Json(new { code = "error", message = "Tên nhóm đã tồn tại! Hãy kiểm tra lại" }, JsonRequestBehavior.AllowGet);
                }
                i.IsActive = 1;
                unitofwork.Context.NhomNguyenLieux.Add(i);
                unitofwork.Context.SaveChanges();
                unitofwork.Save();
                return Json(new { code = "success", message = "Thêm mới thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var temp = unitofwork.Context.NhomNguyenLieux.Find(banghi.NhomNguyenLieuID);
                var ten = unitofwork.Context.NhomNguyenLieux.FirstOrDefault(p => p.TenNhomNguyenLieu.ToLower() == banghi.TenNhomNguyenLieu.ToLower() && p.IsActive == 1 && p.NhomNguyenLieuID != banghi.NhomNguyenLieuID);
                if (ten == null)
                {
                    temp.TenNhomNguyenLieu = banghi.TenNhomNguyenLieu;
                }
                else
                {
                    return Json(new { code = "error", message = "Tên nhóm đã tồn tại! Hãy kiểm tra lại" }, JsonRequestBehavior.AllowGet);
                }
                unitofwork.Context.SaveChanges();
                unitofwork.Save();
                return Json(new { code = "success", message = "Thêm mới thành công" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeleteNhomNguyenLieu(string NhomNguyenLieuID)
        {
            unitofwork = new UnitOfWork();
            Guid id = Guid.Parse(NhomNguyenLieuID);
            var temp = unitofwork.Context.NhomNguyenLieux.Find(id);
            temp.IsActive = 0;
            unitofwork.Context.SaveChanges();
            unitofwork.Save();
            return Json(new { message = "success" }, JsonRequestBehavior.AllowGet);
        }
    }
}
