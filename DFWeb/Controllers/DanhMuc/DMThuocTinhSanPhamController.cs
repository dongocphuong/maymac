﻿using DF.DataAccess;
using DFWeb.Security;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using DF.DataAccess.DBML;


namespace DFWeb.Controllers.QLVatTu
{
    [CustomAuthorize(Roles = "2015")]
    public class DMThuocTinhSanPhamController : Controller
    {
        //
        // GET: /VatTu/
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        string status;
        string code;
        public ActionResult Index()
        {
            ViewBag.Title_Function = "Danh mục thuộc tính sản phẩm";
            return View();
        }
        public JsonResult GetAllDataDMThuocTinhSanPham()
        {
            unitofwork = new UnitOfWork();
            try
            {
                List<Web_DanhMucThuocTinhSanPham_GetAllDataResult> lst = unitofwork.ThuocTinhSanPhams.GetAllDataDMThuocTinhSanPham();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDataThuocTinhSanPhamID(string data)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var dt = db.Web_DanhMucThuocTinhSanPham_GetAllDataThuocTinhSanPhamID(Guid.Parse(data));
            return Json(new { data = dt, message = "Thành công" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddOrUpdateDMThuocTinhSanPham(string data)
        {
            unitofwork = new UnitOfWork();
            int result = unitofwork.ThuocTinhSanPhams.AddOrUpdateDMThuocTinhSanPham(data);
            if (result == 0)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            else if (result == 1)
            {
                code = "warning";
                status = "Tên thuộc tính đã bị trùng!";
            }
            else
            {
                code = "success";
                status = "Thành công!";
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult XoaDMThuocTinhSanPham(string data)
        {
            unitofwork = new UnitOfWork();
            int result = unitofwork.ThuocTinhSanPhams.DeleteDMThuocTinhSanPham(data);
            if (result == 0)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            else if (result == 1)
            {
                code = "warning";
                status = "Tên thuộc tính đã bị trùng!";
            }
            else
            {
                code = "success";
                status = "Thêm mới thành công!";
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
    }
}