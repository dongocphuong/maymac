﻿using DF.DataAccess;
using DF.DataAccess.DBML;
using DF.DBMapping.Models;
using DF.DBMapping.ModelsExt.NghiepVu;
using DFWeb.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace DFWeb.Controllers.DanhMuc
{
    public class DmNhomSanPhamController : Controller
    {
        //
        // GET: /DmNhomSanPham/
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        string status;
        string code;
        [CustomAuthorize(Roles = "2114")]
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult LayDanhSachNhomSanPham()
        {
            db = new DBMLDFDataContext();
            try
            {
               var lst = db.Web_NhomSanPham_LayDanhSach().ToList();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayDanhSachSanPham(Guid MauMayID)
        {
            db = new DBMLDFDataContext();
            try
            {
                var lst = db.Web_NhomSanPham_LayDanhSachSanPham(MauMayID).ToList();
                var lstsize = lst.Select(p => new {p.Size,p.SanPhamID }).Distinct().OrderBy(p => p.Size).ToList();
                var lstthuoctinh = lst.Select(p => new { p.TenThuocTinh }).Distinct().OrderBy(p => p.TenThuocTinh).ToList();
                var tenthuoctinh = lst.Select(p => p.TenThuocTinh).Distinct().ToList();
                return Json(new { data = lst, sizes = lstsize,tenthuoctinh=tenthuoctinh,lstthuoctinh=lstthuoctinh,total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayDanhNguyenLieu(Guid MauMayID)
        {
            db = new DBMLDFDataContext();
            try
            {
                var lst = db.Web_ThanhPhamNhom_DinhNghiaThanhPham(MauMayID).ToList();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CapNhatNguyenLieu(string data)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var ts = new JavaScriptSerializer().Deserialize<List<Web_ThanhPhamNhom_DinhNghiaThanhPhamResult>>(data);
                var lstDinhNghiaTp = unitofwork.Context.DinhNghiaThanhPhams.ToList();
                foreach (var spid in ts)
                {
                    var dn = lstDinhNghiaTp.FirstOrDefault(p => p.DinhNghiaThanhPhamID == spid.DinhNghiaThanhPhamID);
                    if (dn != null)
                    {
                        dn.SoLuong = spid.SoLuong;
                        dn.LoaiVai = spid.LoaiVai;
                    }
                }
                unitofwork.Save();
                code = "success";
                status = "Thành công!";

            }
            catch (Exception ex)
            {
                code = "error";
                status = "Lưu không thành công! Có lỗi xảy ra";
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CapNhatThuocTinh(string tts)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var ts = new JavaScriptSerializer().Deserialize<List<ThuocTinhSanPhamModel>>(tts);
                var lstsanpham = ts.Select(p => p.SanPhamID).Distinct().ToList();
                var lstsp = unitofwork.Context.SanPhams.ToList();
                var ttsps = unitofwork.Context.GiaTriThuocTinhSanPhams.ToList();
                foreach (var spid in lstsanpham)
                {
                    var sp2 = lstsp.FirstOrDefault(p => p.IsActive == true && p.SanPhamID == spid);
                    foreach (var sp in lstsp.Where(p => p.Size == sp2.Size && sp2.MauMayID==p.MauMayID))
                    {
                        var lstttchung = unitofwork.Context.GiaTriThuocTinhSanPhams.Where(p => p.SanPhamID == sp.SanPhamID).ToList();
                        var lstkd = ttsps.Where(p => p.SanPhamID == sp.SanPhamID);
                        unitofwork.Context.GiaTriThuocTinhSanPhams.RemoveRange(lstkd);
                        foreach (var item in ts.Where(p => p.SanPhamID == spid))
                        {
                            var gt = new GiaTriThuocTinhSanPham()
                            {
                                GiaTri = item.GiaTri,
                                GiaTriThuocTinhID = Guid.NewGuid(),
                                SanPhamID = sp.SanPhamID,
                                ThuocTinhSanPhamID = item.ThuocTinhSanPhamID
                            };
                            unitofwork.Context.GiaTriThuocTinhSanPhams.Add(gt);
                        }
                        unitofwork.Save();
                    }
                }
               
                code = "success";
                status = "Thành công!";
               
            }
            catch (Exception ex)
            {
                code = "error";
                status = "Lưu không thành công! Có lỗi xảy ra";
            }
            return Json(new { code = code, message = status}, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InBangNguyenPhuLieu(Guid MauMayID)
        {
            db = new DBMLDFDataContext();
            unitofwork = new UnitOfWork();
            var tp = Guid.Empty;
            var lst = db.Web_ThanhPhamNhom_DinhNghiaThanhPham(MauMayID).ToList();
            var sanpham = unitofwork.Context.SanPhams.FirstOrDefault(p => p.MauMayID == MauMayID);
            ViewBag.MaSanPham = sanpham.MaSanPham;
            return View(lst);
        }
        public JsonResult GetAllDataSanPham(string MauMayID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            Guid mmid;
            try
            {
                mmid = Guid.Parse(MauMayID);
            }
            catch
            {
                mmid = Guid.Empty;
            }
            try
            {      
                List<Web_DanhMucSanPham_GetAllDataResult> list = db.Web_DanhMucSanPham_GetAllData().Where(t => t.MauMayID == mmid).OrderByDescending(t => t.ThoiGian).ToList();
                return Json(new { data = list, total = list.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
