﻿using DF.DataAccess;
using DF.DBMapping.Models;
using DF.DBMapping.ModelsExt;
using DFSCommon;
using DFSCommon.Models.Enums;
using DFWeb.Common;
using DFWeb.Models;
using DFWeb.Security;
using DFWeb.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Webservice.FCMModules;
using System.Configuration;
using System.Transactions;
using DF.DataAccess.DBML;
using FlexCel.Core;
using DFWeb.Models.ReportModels;
using FlexCel.XlsAdapter;
using DF.Utilities;
using Excel;
using DF.DBMapping.ModelsExt.NghiepVu;
using DF.DBMapping.ModelsExt.Excel;
using FlexCel.Report;
using Newtonsoft.Json;

namespace DFWeb.Controllers.DanhMuc
{
    [CustomAuthorize(Roles = "2012")]
    public class DMSanPhamController : Controller
    {
        //
        // GET: /DMSanPham/
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        string status;
        string code;
        public ActionResult Index()
        {
            ViewBag.Title_Function = "Danh mục sản phẩm";
            return View();
        }
        //Xuất excel ---------------------------------------------------------------------------------------
        public clsExcelResult XuatExcelBangXacNhanMau(Guid mmid, string msp, Guid yckhid)
        {
            string fileName = "";
            fileName = "Bảng xác nhận mẫu.xlsx";
            string urlTemp = "";
            urlTemp = "~\\TempExcel\\TemplateBangXacNhanMau.xlsx";
            clsExcelResult clsResult = new clsExcelResult();
            ExcelFile xls = CreateExcel(Server.MapPath(urlTemp), mmid, msp, yckhid);

            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public ExcelFile CreateExcel(String path, Guid mmid, string msp, Guid yckhid)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            XlsFile Result = new XlsFile(true);
            try
            {
                Result.Open(path);
                FlexCelReport fr = new FlexCelReport();
                unitofwork = new UnitOfWork();
                //
                var infor = unitofwork.Context.MauMays.FirstOrDefault(x => x.MauMayID == mmid);
                var dt = db.Web_SanPham_LayThongTinChiTietYeuCaubyMauMayID(mmid).FirstOrDefault();
                var listsize = db.Web_ChiTietYeuCau_getAllData(yckhid).OrderBy(x => x.Size).ToList();
                var str = "";
                foreach (var item in listsize)
                {
                    str += (item.Size + ", ");
                }
                var data = unitofwork.Context.BangXacNhanMaus.Select(x => new {
                    Ngay = DateTime.Now.Day,
                    Thang = DateTime.Now.Month,
                    Nam = DateTime.Now.Year,
                    MaMauMay = infor.MauMayID,
                    CapMay = x.CapMay,
                    NhanVienThietKe = x.NhanVienThietKe,
                    MaSanPham = msp,
                    ChatLieu = dt.ChatLieu,
                    ThietKeLan = x.ThietKeLan,
                    Size = str,
                    DacDiem = dt.DacDiem,
                    LenNen = x.LenNen,
                    LenNenF1 = x.LenNenF1,
                    LenNenF2 = x.LenNenF2,
                    LenNenF3 = x.LenNenF3,
                    LyNen = x.LyNen,
                    LyNenF1 = x.LyNenF1,
                    LyNenF2 = x.LyNenF2,
                    LyNenF3 = x.LyNenF3,
                    LyNenNep = x.LyNenNep,
                    LyNenTong = x.LyNenTong,
                    TT = x.TT,
                    TS = x.TS,
                    Tay = x.Tay,
                    Co = x.Co,
                    Nep = x.Nep,
                    Tong = x.Tong,
                    MatDo1 = x.MatDo1,
                    MatDo2 = x.MatDo2,
                    MatDo3 = x.MatDo3,
                    TocDoTK = x.TocDoTK,
                    TocDoMay = x.TocDoMay,
                    Mo1 = x.Mo1,
                    Mo2 = x.Mo2,
                    Mo3 = x.Mo3, 
                    Mo4 = x.Mo4,
                    Mo5 = x.Mo5,
                    GhiChu1 = x.GhiChu1,
                    LK = x.LK,
                    VS = x.VS,
                    GM = x.GM,
                    LingkingNep = x.LingkingNep,
                    LingkingTong = x.LingkingTong,
                    GhiChu2 = x.GhiChu2,
                    GhiChu3 = x.GhiChu3,
                    GhiChu4 = x.GhiChu4,
                    GhiChu5 = x.GhiChu5,
                    May1 = x.May1,
                    May2 = x.May2, 
                    May3 = x.May3,
                    GiatSay1 = x.GiatSay1,
                    GiatSay2 = x.GiatSay2,
                    GiatSay3 = x.GiatSay3,
                    DuyetMau = x.DuyetMau,
                    NhanXet1 = x.NhanXet1,
                    NhanXet2 = x.NhanXet2,
                    NhanXet3 = x.NhanXet3,
                    TrongLuongPhoiThanhPham = x.TrongLuongPhoiThanhPham,
                    MauMayID = x.MauMayID,
                }).Where(x => x.MauMayID == mmid).ToList();
                DataTable dtThongTinChung = Utils.Utilities.ToDataTable(data);
                dtThongTinChung.TableName = "ChiTiet";
                fr.AddTable("ChiTiet", dtThongTinChung);
                fr.Run(Result);
                fr.Dispose();
                dtThongTinChung.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
        //-------------------------------------------------------------------------------------------------------------------
        public JsonResult AddOrUpdateBXNM(string data)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var records = new JavaScriptSerializer().Deserialize<BangXacNhanMau>(data);
            if (records.BangXacNhanMauID == Guid.Empty) //add
            {
                records.BangXacNhanMauID = Guid.NewGuid();
                unitofwork.Context.BangXacNhanMaus.Add(records);
                unitofwork.Context.SaveChanges();
                unitofwork.Save();
            }
            else //update
            {
                db.Web_BangXacNhanMau_DeleteOldData(records.BangXacNhanMauID);
                records.BangXacNhanMauID = Guid.NewGuid();
                unitofwork.Context.BangXacNhanMaus.Add(records);
                unitofwork.Context.SaveChanges();
                unitofwork.Save();
            }
            return Json(new { code = "success", message = "Lưu thành công!" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDataBXNM(Guid mmid)
        {
            unitofwork = new UnitOfWork();
            var data = unitofwork.Context.BangXacNhanMaus.FirstOrDefault(x => x.MauMayID == mmid);
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllDataSanPham()
        {
            unitofwork = new UnitOfWork();
            try
            {
                List<Web_DanhMucSanPham_GetAllDataResult> lst = unitofwork.SanPhams.GetAllDataDMSanPham();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LuuDanhSachNguyenLieuChoYeuCau(string YeuCauKhachHangID, string SanPhamID, string data, string size)
        {
            unitofwork = new UnitOfWork();
            int result = unitofwork.SanPhams.LuuDanhSachNguyenLieuChoYeuCau(YeuCauKhachHangID, SanPhamID, data, size);
            if (result == 0)
            {
                return Json(new { code = "error", message = "Đã xảy ra lỗi! Vui lòng thử lại!" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "success", message = "Lưu dữ liệu thành công!" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayThongTinChiTietYeuCauKhachHang(Guid yckhid)
        {
            unitofwork = new UnitOfWork();
            var dt = unitofwork.Context.ChiTietYeuCauKhachHangs.Where(x => x.YeuCauKhachHangID == yckhid).ToList();
            return Json(new { data = dt }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayThongTinThietKe(Guid tkid)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var dt = db.Web_SanPham_LayThongTinChiTietYeuCaubyMauMayID(tkid).FirstOrDefault();
            return Json(new { data = dt }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult TakeSize(string YeuCauKhachHangID)
        {
            db = new DBMLDFDataContext();
            Guid yckhid = Guid.Empty;
            if (YeuCauKhachHangID != null)
            {
                yckhid = Guid.Parse(YeuCauKhachHangID);
            }
            var list = db.Web_ChiTietYeuCau_getAllData(yckhid).OrderBy(x => x.Size).ToList();
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DanhSachTatCaNguyenLieu()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var dt = db.Web_NguyenLieu_LayDanhSach().OrderBy(t => t.TenNguyenLieu).ToList();
            return Json(new { data = dt }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTenNguyenLieu(string NguyenLieuID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            Guid nlid;
            try
            {
                nlid = Guid.Parse(NguyenLieuID);
            }
            catch
            {
                nlid = Guid.Empty;
            }
            var dt = db.Web_NguyenLieu_LayDanhSach().Where(x => x.NguyenLieuID == nlid).FirstOrDefault();
            return Json(new { data = dt }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DanhSachNguyenLieu(string YeuCauKhachHangID = "")
        {
            unitofwork = new UnitOfWork();
            if (YeuCauKhachHangID == "")
            {
                var dt = (from a in unitofwork.Context.NguyenLieux
                          join c in unitofwork.Context.DonVis on a.DonViID equals c.DonViId
                          where a.IsActive == true
                          select new
                          {
                              a.AnhDaiDien,
                              a.MaNguyenLieu,
                              a.TenNguyenLieu,
                              c.TenDonVi,
                              a.Type,
                              a.NguyenLieuID
                          }).ToList();
                return Json(new { data = dt, message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var yckhid = Guid.Parse(YeuCauKhachHangID);
                var dt = (from a in unitofwork.Context.NguyenLieux
                          join b in unitofwork.Context.NguyenLieuYeuCauKhachHangs on a.NguyenLieuID equals b.NguyenLieuID
                          join c in unitofwork.Context.DonVis on a.DonViID equals c.DonViId
                          where a.IsActive == true && b.YeuCauKhachHangID == yckhid
                          select new
                          {
                              a.AnhDaiDien,
                              a.MaNguyenLieu,
                              a.TenNguyenLieu,
                              c.TenDonVi,
                              a.Type,
                              a.NguyenLieuID
                          }).ToList();
                return Json(new { data = dt, message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CheckNguyenLieuTheoYeuCauKhachHang(string SanPhamID)
        {
            db = new DBMLDFDataContext();
            var dt = db.Web_SanPham_CheckNguyenLieuTheoYeuCau(Guid.Parse(SanPhamID)).ToList();
            return Json(new { data = dt, code = "success" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachThanhPham(string SanPhamID)
        {
            var db = new DBMLDFDataContext();
            try
            {
                var lst = db.Web_DanhMucThanhPham_TheoSanPham(SanPhamID != "" ? Guid.Parse(SanPhamID) : Guid.Empty).ToList();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDataSanPhamID(string data)
        {
           
            db = new DBMLDFDataContext();
            var dt = db.Web_DanhMucSanPham_GetAllDataSanPhamID(Guid.Parse(data)).ToList();
            var tts = db.Web_DanhMucSanPham_LaySanhSachThuocTinh(Guid.Parse(data), dt.FirstOrDefault().LoaiHinhSanXuat).ToList();
            return Json(new { data = dt,tts=tts, message = "Thành công" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachThuocTinh(string data, int LoaiHinhSanXuat)
        {

            db = new DBMLDFDataContext();
            var tts = db.Web_DanhMucSanPham_LaySanhSachThuocTinh(Guid.Parse(data),LoaiHinhSanXuat).ToList();
            return Json(new { tts = tts, message = "Thành công" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddOrUpdateDMSanPham(string data,string tts)
        {
            unitofwork = new UnitOfWork();
            Guid result = unitofwork.SanPhams.AddOrUpdateDMSanPham(data,tts);
            if (result == Guid.Empty)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            //else if (result == 1)
            //{
            //    code = "warning";
            //    status = "Tên nguyên liệu hoặc mã nguyên liệu đã bị trùng!";
            //}
            else
            {
                code = "success";
                status = "Thành công!";
            }
            return Json(new { code = code, message = status, SanPhamID = result }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CapNhatCongDoan(Guid SanPhamID, string lstcongdoan, string congdoancuoi)
        {
            unitofwork = new UnitOfWork();
            Guid result = unitofwork.SanPhams.CapNhatCongDoan(SanPhamID, lstcongdoan, congdoancuoi);
            if (result == Guid.Empty)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            //else if (result == 1)
            //{
            //    code = "warning";
            //    status = "Tên nguyên liệu hoặc mã nguyên liệu đã bị trùng!";
            //}
            else
            {
                code = "success";
                status = "Thành công!";
            }
            return Json(new { code = code, message = status, SanPhamID = result }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult XoaDMSanPham(string data)
        {
            unitofwork = new UnitOfWork();
            int result = unitofwork.SanPhams.DeleteDMSanPham(data);
            if (result == 0)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            else if (result == 1)
            {
                code = "warning";
                status = "Tên nguyên liệu đã bị trùng!";
            }
            else
            {
                code = "success";
                status = "Thêm mới thành công!";
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ThemNhanhDonVi(string data)
        {
            unitofwork = new UnitOfWork();
            int rs = unitofwork.DonVis.createdata(data);
            if (rs == 0)
            {
                return Json(new { code = "error", message = "Có lỗi không thể thêm được" }, JsonRequestBehavior.AllowGet);
            }
            else if (rs == 1)
            {
                return Json(new { code = "warning", message = "Đã tồn tại đơn vị" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "success", message = "Đã thêm thành công" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayDanhSachCongDoan(Guid SanPhamID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var lstthanhpham = db.Web_DinhNghiaSanPham_LayDanhSachThanhPham(SanPhamID).ToList();;
            var lstCongDoan = (from a in unitofwork.Context.DMHangMucSanXuats
                               join b in unitofwork.Context.DMCongDoanHangMucs on a.DMHangMucSanXuatID equals b.DMHangMucSanXuatID
                               where a.IsActive == true && b.IsActive == true && a.SanPhamID == SanPhamID
                               select new 
                               {
                                   DMHangMucSanXuatID = a.DMHangMucSanXuatID,
                                   HaoHut = a.HaoHut,
                                   TenCongDoan = b.TenCongDoan,
                                   ThoiGianHoanThanh = a.ThoiGianHoanThanh,
                                   ViTri = a.ViTri,
                                   MoTa = a.MoTa,
                                   DsHinhAnh = a.DsHinhAnh,
                                   GiaTri = a.GiaTri
                               }).ToList().Select(p => new CongDoanSanPhamModel {
                                   DMHangMucSanXuatID = p.DMHangMucSanXuatID,
                                   HaoHut = p.HaoHut,
                                   TenCongDoan = p.TenCongDoan,
                                   ViTri = p.ViTri,
                                   MoTa = p.MoTa,
                                   DsHinhAnh = p.DsHinhAnh,
                                   GiaTri = p.GiaTri,
                                   ThoiGianHoanThanh = p.ThoiGianHoanThanh,
                                   item = lstthanhpham.Where(p2 => p2.DMHangMucSanXuatID == p.DMHangMucSanXuatID).Select(p2 => new CongDoanSanPhamItemModel
                                   {
                                       TenDonVi = p2.TenDonVi,
                                       MaThanhPham = p2.MaThanhPham,
                                       TenThanhPham = p2.TenThanhPham,
                                       ThanhPhamID = p2.ThanhPhamID,
                                       SoLuong = p2.SoLuong,
                                       AnhDaiDien=p2.AnhDaiDien
                                   }).ToList(),
                                   
                               }).OrderBy(p=>p.ViTri);

            return Json(new { data = lstCongDoan, message = "Thành công" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachCongDoanCuoi(Guid SanPhamID)
        {
            db = new DBMLDFDataContext();
            unitofwork = new UnitOfWork();
            var thoigianhoanthanh = unitofwork.Context.DinhNghiaSanPhams.FirstOrDefault(p => p.SanPhamID == SanPhamID);
            var lstCongDoan = db.Web_DinhNghiaSanPham_LayDanhSachDinhNghia(SanPhamID).Select(p2 => new CongDoanSanPhamItemModel
                                   {
                                       TenDonVi = p2.TenDonVi,
                                       MaThanhPham = p2.MaThanhPham,
                                       TenThanhPham = p2.TenThanhPham,
                                       ThanhPhamID = p2.ThanhPhamID,
                                       SoLuong = p2.SoLuongThanhPham,
                                       AnhDaiDien = p2.AnhDaiDien,
                                   }).ToList();
            return Json(new { thoigianhoanthanh = (thoigianhoanthanh != null ? thoigianhoanthanh.ThoiGianHoanThanh : 0), MoTa = (thoigianhoanthanh != null ? thoigianhoanthanh.MoTa : ""), GiaTri = (thoigianhoanthanh != null ? thoigianhoanthanh.GiaTri : 0), DsHinhAnh = (thoigianhoanthanh != null ? thoigianhoanthanh.DsHinhAnh : ""), data = lstCongDoan, message = "Thành công" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachNguyenLieu(Guid SanPhamID)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            unitofwork = new UnitOfWork();
            var list = db.LayDanhSachNguyenLieuDeDinhNghia(SanPhamID).Select(x => new
            {
                x.AnhDaiDien,
                x.TenNguyenLieu,
                x.NguyenLieuID,
                x.LoaiVai,
                x.MaNguyenLieu,
                x.TenDonVi
            }).ToList();
            return Json(new { data = list, total = list.Count() }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult TaoCongDoanTuDong(Guid YeuCauKhachHangID, string data)
        {
            unitofwork = new UnitOfWork();
            int result = unitofwork.SanPhams.TaoCongDoanTuDong(YeuCauKhachHangID, data);
            if (result == 0)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            if (result == 2)
            {
                code = "error";
                status = "Sản phẩm đang trong quá trình sản xuất không thể đổi công đoạn";
            }
            //else if (result == 1)
            //{
            //    code = "warning";
            //    status = "Tên nguyên liệu hoặc mã nguyên liệu đã bị trùng!";
            //}
            else
            {
                code = "success";
                status = "Thành công!";
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        //
        #region Sản xuất
        public ActionResult ViewSanXuatSanPham(string SanPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            var spid = Guid.Parse(SanPhamID);
            var dhid = Guid.Parse(DonHangID);
            var dasanxuat = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.DonHangID == dhid && spid == p.SanPhamID);
            if (dasanxuat == null)
            {
                return Redirect("/DMSanPham/ViewSanXuatSanPhamChuaBatDau?SanPhamID=" + SanPhamID + "&DonHangID=" + DonHangID);
            }
            else
            {
                return Redirect("/DMSanPham/ViewSanXuatSanPhamDaBatDau?SanPhamID=" + SanPhamID + "&DonHangID=" + DonHangID);
            }
        }

        public ActionResult ViewSanXuatSanPhamChuaBatDau(string SanPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var lst = unitofwork.SanPhams.SanXuatSanPhamChuaBatDau(SanPhamID, DonHangID);
                return View(lst);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/DMSanPham/ChuaDinhNghiaCongThuc.cshtml");
            }
        }
        public ActionResult ViewSanXuatSanPhamDaBatDau(string SanPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var lst = unitofwork.SanPhams.SanXuatSanPhamDangSanXuat(SanPhamID, DonHangID);
                return View(lst);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/DMSanPham/ChuaDinhNghiaCongThuc.cshtml");
            }

        }
        public JsonResult LayDanhSachNguyenLieuTheoYCKHDaLuu(string YeuCauKhachHangID)
        {
            db = new DBMLDFDataContext(); Guid yckh;
            try
            {
                yckh = Guid.Parse(YeuCauKhachHangID);
            }
            catch
            {
                yckh = Guid.Empty;
            }
            var lst = db.Web_LayDanhSachNguyenLieuDaLuu(yckh).GroupBy(x => x.Mau).Select(gr => gr.ToList()).ToList();
            return Json(new { data = lst, total = lst.Count, code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult BatDauSanXuat(string SanPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.SanPhams.BatDauSanXuat(SanPhamID, DonHangID);
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }

        }

        public JsonResult SanXuatThanhPham(string DonHangID, string SanPhamID, string HangMucSanXuatID, string datanhap, string dataxuat)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.SanPhams.SanXuatThanhPham(DonHangID, SanPhamID, HangMucSanXuatID, datanhap, dataxuat, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SanXuatSanPham(string DonHangID, string SanPhamID, int SoLuong, string dataxuat)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.SanPhams.SanXuatThanhPhamCongDoan(DonHangID, SanPhamID, SoLuong, dataxuat, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayBanThietKe()
        {
            unitofwork = new UnitOfWork(); db = new DBMLDFDataContext();
            var lst = db.Web_SanPham_LayDanhSachThietKe().OrderBy(t => t.TenMauMay).ToList();
            return Json(new { data = lst, total = lst.Count() }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachMauDaLuu(string YeuCauKhachHangID)
        {
            db = new DBMLDFDataContext();
            unitofwork = new UnitOfWork();
            Guid yckhid = Guid.Empty;
            if (YeuCauKhachHangID != null)
            {
                yckhid = Guid.Parse(YeuCauKhachHangID);
            }
            var list = db.Web_SanPham_LayDanhSachMauDaLuu(yckhid).ToList();
            return Json(new { data = list, total = list.Count() }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InThongSo(Guid SanPhamID)
        {
            unitofwork = new UnitOfWork();
            var sanpham = unitofwork.Context.SanPhams.FirstOrDefault(p => p.SanPhamID == SanPhamID);
            var lstsanpham = unitofwork.Context.SanPhams.Where(p => p.MauMayID == sanpham.MauMayID).ToList();
            var lstsize = lstsanpham.Select(p => p.Size).Distinct().ToList();
            var lstMaSanPham = String.Join(",", lstsanpham.Select(p => p.MaSanPham).Distinct());
            var sizethongso = unitofwork.Context.ThongSoSizes.ToList();
            var lst = sizethongso.Where(p => lstsize.Any(p2 => p.Size.ToLower() == p2.ToLower())).ToList();
            if (sanpham.LoaiHinhSanXuat == 1)
            {
                lst = new List<ThongSoSize>();
            }
            var lsthuoctinh = (from a in unitofwork.Context.ThuocTinhSanPhams
                               join b in unitofwork.Context.GiaTriThuocTinhSanPhams on a.ThuocTinhSanPhamID equals b.ThuocTinhSanPhamID
                               join c in unitofwork.Context.SanPhams on b.SanPhamID equals c.SanPhamID
                               where c.SanPhamID == SanPhamID && c.LoaiHinhSanXuat==sanpham.LoaiHinhSanXuat
                               select new
                               {
                                   a.TenThuocTinh,
                                   b.GiaTri,
                                   c.Size
                               }).ToList();
            lst.AddRange(lsthuoctinh.Select(p => new ThongSoSize
            {
                Size = p.Size,
                GiaTri = p.GiaTri,
                TenThongSo = p.TenThuocTinh
            }));
            var model = new ThongSoSanPhamModel()
            {
                LoaiHinhSanXuat = sanpham.LoaiHinhSanXuat,
                lstMaSanPham = lstMaSanPham,
                Sizes = lstsize,
                thongsos = lst
            };
            return View(model);
        }
        #endregion
        //
        public clsExcelResult XuatTemplate()
        {
            string fileName = "";
            fileName = "Template.xlsx";
            string urlTemp = "";
            if (Language.GetKeyLang() == "cn")
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/DanhMucSanPhamTemplate.xlsx");
            }
            else
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/DanhMucSanPhamTemplate.xlsx");
            }
            clsExcelResult clsResult = new clsExcelResult();
            var temp = new XlsFile(true);
            temp.Open(urlTemp);
            ExcelFile xls = temp;
            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public JsonResult CheckKetQua()
        {
            string iUploadedCnt = "";
            string sPath = "";
            sPath = Server.MapPath("~/TempExcel/Import/TempFile/");
            unitofwork = new UnitOfWork();
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                if (hpf.ContentLength > 0)
                {
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        iUploadedCnt = (sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        if (hpf != null && hpf.ContentLength > 0)
                        {
                            Stream stream = hpf.InputStream;
                            IExcelDataReader reader = null;
                            if (hpf.FileName.EndsWith(".xls"))
                            {
                                reader = ExcelReaderFactory.CreateBinaryReader(stream);
                            }
                            else if (hpf.FileName.EndsWith(".xlsx"))
                            {
                                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                            }
                            else
                            {

                            }
                            reader.IsFirstRowAsColumnNames = true;
                            DataSet result = reader.AsDataSet();
                            var listct = result.Tables[0].AsEnumerable().Select(p => new
                            {
                                TenSanPham = p[0].ToString(),
                                MaSanPham = p[1].ToString(),
                                TenDonVi = p[2].ToString(),
                                DonGia = p[3].ToString(),
                            });
                            reader.Close();
                            return Json(new { data = listct }, JsonRequestBehavior.AllowGet);
                        }
                        // hpf.SaveAs(iUploadedCnt);

                    }
                }
            }
            var message = "";
            return Json(new { url = iUploadedCnt, message = message }, JsonRequestBehavior.AllowGet);


        }
        public ActionResult InBangNguyenPhuLieu(Guid SanPhamID,string ThanhPhamID = "")
        {
                db = new DBMLDFDataContext();
                unitofwork = new UnitOfWork();
                var tp = Guid.Empty;
                if (ThanhPhamID != "")
                {
                    tp = Guid.Parse(ThanhPhamID);
                }
                var lst = db.Web_ThanhPham_DinhNghiaThanhPham(tp).ToList();
                var sanpham = unitofwork.Context.SanPhams.FirstOrDefault(p => p.SanPhamID == SanPhamID);
                ViewBag.MaSanPham = sanpham.MaSanPham;
                return View(lst);
        }
        public JsonResult ImportDMSanPham(string data)
        {
            //Loaihinh=1: Toàn bộ;Loaihinh=2:Bỏ qua trùng
            unitofwork = new UnitOfWork();
            dynamic model = new JavaScriptSerializer().Deserialize<dynamic>(data);
            for (int i = 0; i < model.Length; i++)
            {
                Guid donviid = Guid.Empty;
                string tendonvi = model[i]["TenDonVi"].Trim().Replace(" ", "").ToLower();
                string masanpham = model[i]["MaSanPham"].Trim().Replace(" ", "").ToLower();
                var msp = unitofwork.Context.SanPhams.FirstOrDefault(p => p.IsActive == true && p.MaSanPham.Trim().Replace(" ", "").ToLower() == masanpham);
                var dv = unitofwork.Context.DonVis.FirstOrDefault(p => p.IsActive == true && p.TenDonVi.Trim().Replace(" ", "").ToLower() == tendonvi);
                if (msp != null)
                {
                    continue;
                }
                if (dv != null)
                {
                    donviid = dv.DonViId;
                }
                else
                {
                    DonVi dv2 = new DonVi()
                    {
                        IsActive = true,
                        DonViId = Guid.NewGuid(),
                        TenDonVi = model[i]["TenDonVi"]
                    };
                    donviid = dv2.DonViId;
                    unitofwork.Context.DonVis.Add(dv2);
                    unitofwork.Context.SaveChanges();
                    unitofwork.Save();
                }

                SanPham ccv2 = new SanPham();
                Guid VatTuID = Guid.NewGuid();
                ccv2.SanPhamID = VatTuID;
                ccv2.DonViID = donviid;
                ccv2.IsActive = true;
                ccv2.TenSanPham = model[i]["TenSanPham"];
                ccv2.MaSanPham = model[i]["MaSanPham"];
                ccv2.DonGia = double.Parse(model[i]["DonGia"]);
                unitofwork.SanPhams.Add(ccv2);
                unitofwork.Save();
            }
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }
        private string getvalue(System.Array row, int i)
        {
            try
            {
                return row.GetValue(1, i).ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public JsonResult DeleteFile(string url)
        {
            if (System.IO.File.Exists(url))
            {
                System.IO.File.Delete(url);
            }
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }
        //Xuất excel
        public clsExcelResult XuatExcelDMSanPham(string filters)
        {
            string fileName = "";
            fileName = "DMSanPham.xlsx";
            string urlTemp = "";
            urlTemp = "~\\TempExcel\\DanhMuc\\DMSanPham.xlsx";
            clsExcelResult clsResult = new clsExcelResult();
            ExcelFile xls = CreateExcel(Server.MapPath(urlTemp), filters);

            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public ExcelFile CreateExcel(String path, string filters)
        {
            XlsFile Result = new XlsFile(true);
            try
            {

                Result.Open(path);
                FlexCelReport fr = new FlexCelReport();
                unitofwork = new UnitOfWork();
                List<FilterModel> filter = JsonConvert.DeserializeObject<List<FilterModel>>(filters);
                List<Web_DanhMucSanPham_GetAllDataResult> lst = unitofwork.SanPhams.GetAllDataDMSanPham();
                var stt = 1;
                var data2 = lst.Where(p => GetDateFilter(p, filter)).Select(p => new ExcelDMSanPham()
                {
                    STT = stt++,
                    TenSanPham = p.TenSanPham,
                    MaSanPham = p.MaSanPham,
                    TenDonVi = p.TenDonVi,
                    DonGia = p.DonGia,
                }).ToList();
                DataTable dtQuanLyVatTu = Utils.Utilities.ToDataTable(data2);
                dtQuanLyVatTu.TableName = "ChiTiet";
                fr.AddTable("ChiTiet", dtQuanLyVatTu);
                fr.Run(Result);
                fr.Dispose();
                dtQuanLyVatTu.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
        //end xuất excel 
        public string removeUnicode(string input)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
            "đ",
            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
            "í","ì","ỉ","ĩ","ị",
            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
            "ý","ỳ","ỷ","ỹ","ỵ",};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "d",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                input = input.Replace(arr1[i], arr2[i]);
                input = input.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return input;
        }

        public bool GetDateFilter(Web_DanhMucSanPham_GetAllDataResult ls, List<FilterModel> filters)
        {
            var istrue = filters.Count() <= 0;
            foreach (var filter in filters)
            {
                var value = ls.GetType().GetProperty(filter.field).GetValue(ls);
                if (value != null)
                {
                    if (filter.Operator == "contains" || filter.Operator == "eq")
                    {
                        if (removeUnicode(value.ToString().ToLower()).Contains(removeUnicode(filter.value.ToLower())))
                        {
                            istrue = true;
                            break;
                        }
                    }
                    else
                    {
                        if (value.ToString() == filter.value)
                        {
                            istrue = true;
                            break;
                        }
                    }
                }
            }
            return istrue;
        }
    }
}
