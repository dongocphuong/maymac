﻿using DF.DataAccess;
using DF.DBMapping.Models;
using DFWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;
using DFWeb.Common;
using DFWeb.Security;
using System.Data.SqlClient;
using FlexCel.Core;
using System.IO;
using FlexCel.XlsAdapter;
using FlexCel.Report;
using System.Transactions;
using DF.Utilities;
using DF.DataAccess.Repository;
using DFWeb.Models.ReportModels;
using Excel;

namespace DFWeb.Controllers.QuanLyDanhMuc
{
    [CustomAuthorize(Roles = "2019")]
    public class DMDonViController : Controller
    {
        //
        // GET: /DMDonViTinh/
        protected UnitOfWork unitofwork;
        string status;
        string code;
        public ActionResult Index()
        {
            ViewBag.Title_Function = Language.GetText("danhmucdonvi");
            return View();
        }

        public ActionResult read()
        {
            unitofwork = new UnitOfWork();
            var List = unitofwork.DonVis.getdata();
            return Json(new { data = List, total = List.Count }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult create(string data)
        {
            unitofwork = new UnitOfWork();
            int rs = unitofwork.DonVis.createdata(data);
            if (rs == 0)
            {
                code = "error";
                status = Models.Language.GetText("msg_themkhongthanhcong");
            }
            else if (rs == 1)
            {
                code = "warning";
                status = Models.Language.GetText("msg_donvitinhdatontai");
            }
            else
            {
                code = "success";
                status = Models.Language.GetText("mgs_themthanhcong");
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult update(string data)
        {
            unitofwork = new UnitOfWork();
            int rs = unitofwork.DonVis.updatedata(data);
            if (rs == 0)
            {
                code = "error";
                status = Models.Language.GetText("msg_suakhongthanhcong");
            }
            else if (rs == 1)
            {
                code = "warning";
                status = Models.Language.GetText("msg_donvitinhdatontai");
            }
            else
            {
                code = "success";
                status = Models.Language.GetText("msg_suathanhcong");
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult destroy(string data)
        {
            unitofwork = new UnitOfWork();
            int rs = unitofwork.DonVis.deletedata(data);
            if (rs == 0)
            {
                code = "error";
                status = Models.Language.GetText("mgs_xoakhongthanhcong");
            }
            else if(rs==1)
            {
                code = "warning";
                status = Models.Language.GetText("msg_bangvattudangsudungbanghinay");
            }
            else
            {
                code = "success";
                status = Models.Language.GetText("mgs_xoathanhcong");
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }

        //import
        public clsExcelResult XuatTemplate()
        {
            string fileName = "";
            fileName = "Template.xlsx";
            string urlTemp = "";
            if (Language.GetKeyLang() == "cn")
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/DMDonViCN.xlsx");
            }
            else
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/DMDonVi.xlsx");
            }
            clsExcelResult clsResult = new clsExcelResult();
            var temp = new XlsFile(true);
            temp.Open(urlTemp);
            ExcelFile xls = temp;
            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public JsonResult CheckKetQua()
        {
            string iUploadedCnt = "";
            string sPath = "";
            sPath = Server.MapPath("~/TempExcel/Import/TempFile/");
            unitofwork = new UnitOfWork();
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                if (hpf.ContentLength > 0)
                {
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        iUploadedCnt = (sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        if (hpf != null && hpf.ContentLength > 0)
                        {
                            Stream stream = hpf.InputStream;
                            IExcelDataReader reader = null;
                            if (hpf.FileName.EndsWith(".xls"))
                            {
                                reader = ExcelReaderFactory.CreateBinaryReader(stream);
                            }
                            else if (hpf.FileName.EndsWith(".xlsx"))
                            {
                                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                            }
                            else
                            {

                            }
                            reader.IsFirstRowAsColumnNames = true;
                            DataSet result = reader.AsDataSet();
                            var listct = result.Tables[0].AsEnumerable().Select(p => new
                            {
                                TenDonVi = p[0].ToString(),
                            });
                            reader.Close();
                            return Json(new { data = listct }, JsonRequestBehavior.AllowGet);
                        }
                        // hpf.SaveAs(iUploadedCnt);

                    }
                }
            }
            var message = "";
            return Json(new { url = iUploadedCnt, message = message }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ImportDonVi(string data, int LoaiHinh = 1)
        {
            //Loaihinh=1: Toàn bộ;Loaihinh=2:Bỏ qua trùng
            unitofwork = new UnitOfWork();
            dynamic model = new JavaScriptSerializer().Deserialize<dynamic>(data);
            for (int i = 0; i < model.Length; i++)
            {
                var donvi = new DonVi()
                {
                    DonViId = Guid.NewGuid(),
                    TenDonVi = model[i]["TenDonVi"],
                    IsActive = true,
                };
                unitofwork.Context.DonVis.Add(donvi);
                unitofwork.Context.SaveChanges();
                unitofwork.Save();
            }
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }
        private string getvalue(System.Array row, int i)
        {
            try
            {
                return row.GetValue(1, i).ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public JsonResult DeleteFile(string url)
        {
            if (System.IO.File.Exists(url))
            {
                System.IO.File.Delete(url);
            }
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }
    }
}   