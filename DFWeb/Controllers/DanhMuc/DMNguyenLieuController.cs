﻿using DF.DataAccess;
using DF.DBMapping.Models;
using DF.DBMapping.ModelsExt;
using DFSCommon;
using DFSCommon.Models.Enums;
using DFWeb.Common;
using DFWeb.Models;
using DFWeb.Security;
using DFWeb.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Webservice.FCMModules;
using System.Configuration;
using System.Transactions;
using DF.DataAccess.DBML;
using FlexCel.Core;
using DFWeb.Models.ReportModels;
using FlexCel.XlsAdapter;
using DF.Utilities;
using Excel;
using DF.DBMapping.ModelsExt.Excel;
using FlexCel.Core;
using FlexCel.Report;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;


namespace DFWeb.Controllers.QLVatTu
{
    [CustomAuthorize(Roles = "2011")]
    public class DMNguyenLieuController : Controller
    {
        //
        // GET: /VatTu/
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        string status;
        string code;
        public ActionResult Index()
        {
            ViewBag.Title_Function = "Danh mục nguyên liệu";
            return View();
        }
        public JsonResult GetAllDataNguyenLieu()
        {
            unitofwork = new UnitOfWork();
            try
            {
                List<Web_DanhMucNguyenLieu_GetAllDataResult> lst = unitofwork.NguyenLieus.GetAllDataDMNguyenLieu();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDataNguyenLieuID(string data)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var dt = db.Web_DanhMucNguyenLieu_GetAllDataNguyenLieuID(Guid.Parse(data));
            return Json(new { data = dt, message = "Thành công" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddOrUpdate(string data)
        {
            unitofwork = new UnitOfWork();
            var banghi = new JavaScriptSerializer().Deserialize<NhomNguyenLieu>(data);
            if (banghi.NhomNguyenLieuID == Guid.Empty)
            {
                var i = new NhomNguyenLieu();
                i.NhomNguyenLieuID = Guid.NewGuid();
                var ten = unitofwork.Context.NhomNguyenLieux.FirstOrDefault(p => p.TenNhomNguyenLieu.ToLower() == banghi.TenNhomNguyenLieu.ToLower() && p.IsActive == 1);
                if (ten == null)
                {
                    i.TenNhomNguyenLieu = banghi.TenNhomNguyenLieu;
                }
                else
                {
                    return Json(new { code = "error", message = "Tên nhóm đã tồn tại! Hãy kiểm tra lại" }, JsonRequestBehavior.AllowGet);
                }
                i.IsActive = 1;
                unitofwork.Context.NhomNguyenLieux.Add(i);
                unitofwork.Context.SaveChanges();
                unitofwork.Save();
                return Json(new { code = "success", message = "Thêm mới thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var temp = unitofwork.Context.NhomNguyenLieux.Find(banghi.NhomNguyenLieuID);
                var ten = unitofwork.Context.NhomNguyenLieux.FirstOrDefault(p => p.TenNhomNguyenLieu.ToLower() == banghi.TenNhomNguyenLieu.ToLower() && p.IsActive == 1);
                if (ten == null)
                {
                    temp.TenNhomNguyenLieu = banghi.TenNhomNguyenLieu;
                }
                else
                {
                    return Json(new { code = "error", message = "Tên nhóm đã tồn tại! Hãy kiểm tra lại" }, JsonRequestBehavior.AllowGet);
                }
                unitofwork.Context.SaveChanges();
                unitofwork.Save();
                return Json(new { code = "success", message = "Thêm mới thành công" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AddOrUpdateDMNguyenLieu(string data)
        {
            unitofwork = new UnitOfWork();
            var banghi = new JavaScriptSerializer().Deserialize<NguyenLieu>(data);
            //
            var tennguyenlieu = Functions.convertToUnSign2(banghi.TenNguyenLieu + " " + banghi.MaMau);
            string res = "";
            string[] tu = tennguyenlieu.ToUpper().Split(' ').Take(10).ToArray();
            int len = tu.Length;
            for (int i = 0; i <= len - 1; i++)
            {
                res += tu[i].Substring(0, 1);
            }
            var count = unitofwork.Context.NguyenLieux.Count(p => p.MaNguyenLieu.StartsWith(res));
            res += count > 0 ? (count + 1).ToString() : "1";
            //
            int result = unitofwork.NguyenLieus.AddOrUpdateDMNguyenLieu(data, res);
            if (result == 0)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            else if (result == 1)
            {
                code = "warning";
                status = "Tên nguyên liệu hoặc mã nguyên liệu đã bị trùng!";
            }
            else
            {
                code = "success";
                status = "Thành công!";
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult XoaDMNguyenLieu(string data)
        {
            unitofwork = new UnitOfWork();
            int result = unitofwork.NguyenLieus.DeleteDMNguyenLieu(data);
            if (result == 0)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            else if (result == 1)
            {
                code = "warning";
                status = "Tên nguyên liệu đã bị trùng!";
            }
            else
            {
                code = "success";
                status = "Thêm mới thành công!";
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ThemNhanhDonVi(string data)
        {
            unitofwork = new UnitOfWork();
            int rs = unitofwork.DonVis.createdata(data);
            if (rs == 0)
            {
                return Json(new { code = "error", message = "Có lỗi không thể thêm được" }, JsonRequestBehavior.AllowGet);
            }
            else if (rs == 1)
            {
                return Json(new { code = "warning", message = "Đã tồn tại đơn vị" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "success", message = "Đã thêm thành công" }, JsonRequestBehavior.AllowGet);
            }
        }
        //----------------------------------------------------------------------------------
        public clsExcelResult XuatTemplate()
        {
            string fileName = "";
            fileName = "Template.xlsx";
            string urlTemp = "";
            if (Language.GetKeyLang() == "cn")
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/DanhMucNguyenLieuTemplate.xlsx");
            }
            else
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/DanhMucNguyenLieuTemplate.xlsx");
            }
            clsExcelResult clsResult = new clsExcelResult();
            var temp = new XlsFile(true);
            temp.Open(urlTemp);
            ExcelFile xls = temp;
            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public JsonResult CheckKetQua()
        {
            string iUploadedCnt = "";
            string sPath = "";
            sPath = Server.MapPath("~/TempExcel/Import/TempFile/");
            unitofwork = new UnitOfWork();
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                if (hpf.ContentLength > 0)
                {
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        iUploadedCnt = (sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        if (hpf != null && hpf.ContentLength > 0)
                        {
                            Stream stream = hpf.InputStream;
                            IExcelDataReader reader = null;
                            if (hpf.FileName.EndsWith(".xls"))
                            {
                                reader = ExcelReaderFactory.CreateBinaryReader(stream);
                            }
                            else if (hpf.FileName.EndsWith(".xlsx"))
                            {
                                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                            }
                            else
                            {

                            }
                            reader.IsFirstRowAsColumnNames = true;
                            DataSet result = reader.AsDataSet();
                            var listct = result.Tables[0].AsEnumerable().Select(p => new
                            {
                                TenNguyenLieu = p[0].ToString(),
                                MaNguyenLieu = p[1].ToString(),
                                TenDonVi = p[2].ToString(),
                            });
                            reader.Close();
                            return Json(new { data = listct }, JsonRequestBehavior.AllowGet);
                        }
                        // hpf.SaveAs(iUploadedCnt);

                    }
                }
            }
            var message = "";
            return Json(new { url = iUploadedCnt, message = message }, JsonRequestBehavior.AllowGet);


        }
        public JsonResult ImportDMNguyenLieu(string data)
        {
            //Loaihinh=1: Toàn bộ;Loaihinh=2:Bỏ qua trùng
            unitofwork = new UnitOfWork();
            dynamic model = new JavaScriptSerializer().Deserialize<dynamic>(data);
            for (int i = 0; i < model.Length; i++)
            {
                Guid donviid = Guid.Empty;
                string tendonvi = model[i]["TenDonVi"].Trim().Replace(" ", "").ToLower();
                var dv = unitofwork.Context.DonVis.FirstOrDefault(p => p.IsActive == true && p.TenDonVi.Trim().Replace(" ", "").ToLower() == tendonvi);
                string manguyenlieu = model[i]["MaNguyenLieu"].Trim().Replace(" ", "").ToLower();
                var mnl = unitofwork.Context.NguyenLieux.FirstOrDefault(p => p.IsActive == true && p.MaNguyenLieu.Trim().Replace(" ", "").ToLower() == manguyenlieu);
                if (mnl != null)
                {
                    continue;
                }
                if (dv != null)
                {
                    donviid = dv.DonViId;
                }
                else
                {
                    DonVi dv2 = new DonVi()
                    {
                        IsActive = true,
                        DonViId = Guid.NewGuid(),
                        TenDonVi = model[i]["TenDonVi"]
                    };
                    donviid = dv2.DonViId;
                    unitofwork.Context.DonVis.Add(dv2);
                    unitofwork.Context.SaveChanges();
                    unitofwork.Save();
                }

                NguyenLieu nl = new NguyenLieu();
                Guid NguyenLieuID = Guid.NewGuid();
                nl.NguyenLieuID = NguyenLieuID;
                nl.DonViID = donviid;
                nl.IsActive = true;
                nl.TenNguyenLieu = model[i]["TenNguyenLieu"];
                nl.MaNguyenLieu = model[i]["MaNguyenLieu"];
                nl.DonGia = 0;
                unitofwork.NguyenLieus.Add(nl);
                unitofwork.Save();
            }
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }
        private string getvalue(System.Array row, int i)
        {
            try
            {
                return row.GetValue(1, i).ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public JsonResult DeleteFile(string url)
        {
            if (System.IO.File.Exists(url))
            {
                System.IO.File.Delete(url);
            }
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }
        //Xuất excel
        public clsExcelResult XuatExcelDMNguyenLieu(string filters)
        {
            string fileName = "";
            fileName = "DMNguyenLieu.xlsx";
            string urlTemp = "";
            urlTemp = "~\\TempExcel\\DanhMuc\\DMNguyenLieu.xlsx";
            clsExcelResult clsResult = new clsExcelResult();
            ExcelFile xls = CreateExcel(Server.MapPath(urlTemp), filters);

            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public ExcelFile CreateExcel(String path, string filters)
        {
            XlsFile Result = new XlsFile(true);
            try
            {

                Result.Open(path);
                FlexCelReport fr = new FlexCelReport();
                unitofwork = new UnitOfWork();
                List<FilterModel> filter = JsonConvert.DeserializeObject<List<FilterModel>>(filters);

                List<Web_DanhMucNguyenLieu_GetAllDataResult> lst = unitofwork.NguyenLieus.GetAllDataDMNguyenLieu();
                var stt = 1;
                var data2 = lst.Where(p => GetDateFilter(p, filter)).Select(p => new ExcelDMNguyenLieu()
                {
                    STT = stt++,
                    TenNguyenLieu = p.TenNguyenLieu,
                    MaNguyenLieu = p.MaNguyenLieu,
                    TenDonVi = p.TenDonVi,
                    DonGia = p.DonGia,
                }).ToList();
                DataTable dtQuanLyVatTu = Utils.Utilities.ToDataTable(data2);
                dtQuanLyVatTu.TableName = "ChiTiet";
                fr.AddTable("ChiTiet", dtQuanLyVatTu);
                fr.Run(Result);
                fr.Dispose();
                dtQuanLyVatTu.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
        //end xuất excel 
        public string removeUnicode(string input)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
            "đ",
            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
            "í","ì","ỉ","ĩ","ị",
            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
            "ý","ỳ","ỷ","ỹ","ỵ",};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "d",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                input = input.Replace(arr1[i], arr2[i]);
                input = input.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return input;
        }

        public bool GetDateFilter(Web_DanhMucNguyenLieu_GetAllDataResult ls, List<FilterModel> filters)
        {
            var istrue = filters.Count() <= 0;
            foreach (var filter in filters)
            {
                var value = ls.GetType().GetProperty(filter.field).GetValue(ls);
                if (value != null)
                {
                    if (filter.Operator == "contains" || filter.Operator == "eq")
                    {
                        if (removeUnicode(value.ToString().ToLower()).Contains(removeUnicode(filter.value.ToLower())))
                        {
                            istrue = true;
                            break;
                        }
                    }
                    else
                    {
                        if (value.ToString() == filter.value)
                        {
                            istrue = true;
                            break;
                        }
                    }
                }
            }
            return istrue;
        }
    }
}

