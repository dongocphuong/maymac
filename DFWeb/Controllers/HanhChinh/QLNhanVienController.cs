﻿using DF.DataAccess;
using DF.DBMapping.Models;
using DF.DBMapping.ModelsExt;
using DFSCommon;
using DFSCommon.Models.Enums;
using DFWeb.Common;
using DFWeb.Models;
using DFWeb.Security;
using DFWeb.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Webservice.FCMModules;
using System.Configuration;
using System.Transactions;
using DF.DataAccess.DBML;
using FlexCel.Core;
using DFWeb.Models.ReportModels;
using FlexCel.XlsAdapter;
using DF.Utilities;
using Excel;

namespace DFWeb.Controllers.HanhChinh
{

    public class QLNhanVienController : Controller
    {
        //
        // GET: /QLNhanVien/
        UnitOfWork unitofwork;
        DBMLDFDataContext db;

        [CustomAuthorize(Roles = "1048")]
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult LayDanhSachNhanVien_New()
        {
            // db = new DBMLDFDataContext();
            unitofwork = new UnitOfWork();
            var lst = unitofwork.NhanViens.LayDSNhanVienNew();

            return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddNhanVien_Partial()
        {
            unitofwork = new UnitOfWork();
            Guid IDTong = Guid.Parse("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF");
            Add_Update_ModelNhanVien item = new Add_Update_ModelNhanVien();
            //List<itemCongTrinh_NhanVien> lstCongTrinh = unitofwork.Context.DuAns.Where(p => p.Dept == 1 && p.IsActive == true).Select(z => new itemCongTrinh_NhanVien
            //{
            //    value = z.DuAnID,
            //    label = z.TenDuAn
            //}).Where(z => z.value != IDTong).ToList();

            //item.CT = lstCongTrinh;
            return PartialView("_AddorUpdateNhanVien", item);
        }

        public JsonResult NhanVienAdd_Request(string data)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            string IDNV = "";
            try
            {
                NhanVienAdd_Request model = new JavaScriptSerializer().Deserialize<NhanVienAdd_Request>(data);

                if (model.NhanVienID == null)
                {
                    NhanVien nv = new NhanVien();
                    nv.NhanVienID = Guid.NewGuid();
                    nv.TenNhanVien = model.TenNhanVien;
                    try
                    {
                        nv.NgaySinh = DateTime.ParseExact(model.NgaySinh, "dd/MM/yyyy", null);
                    }
                    catch (Exception ex)
                    {
                        nv.NgaySinh = DateTime.Now;
                    }
                    nv.GioiTinh = model.GioiTinh;
                    nv.SDT = model.SDT;
                    nv.CMT = model.Cmt;
                    nv.DiaChi = model.DiaChi;
                    nv.IsActive = true;
                    nv.TinhTrang = model.TinhTrang;
                    nv.AnhDaiDien = model.Avatar == "" || model.Avatar == null ? "no-avatar.png" : model.Avatar;
                    unitofwork.NhanViens.Add(nv);

                    NhanVienPhongBan nvpb = new NhanVienPhongBan();
                    nvpb.NhanVienPhongBanID = Guid.NewGuid();
                    nvpb.NhanVienID = nv.NhanVienID;
                    nvpb.IsActive = true;
                    nvpb.ThoiGian = DateTime.UtcNow;
                    nvpb.PhongBanID = model.PhongBan.GetValueOrDefault();
                    nvpb.ChucVuID = model.ChucVu.GetValueOrDefault();
                    unitofwork.NhanVienPhongBans.Add(nvpb);

                    IDNV = nv.NhanVienID.ToString();
                }
                else
                {
                    var nvpb = unitofwork.Context.NhanVienPhongBans.FirstOrDefault(p=>p.NhanVienID==model.NhanVienID);
                    NhanVien nv = unitofwork.Context.NhanViens.Find(model.NhanVienID);
                    nv.TenNhanVien = model.TenNhanVien;
                    try
                    {
                        nv.NgaySinh = DateTime.ParseExact(model.NgaySinh, "dd/MM/yyyy", null);
                    }
                    catch (Exception ex)
                    {
                        nv.NgaySinh = DateTime.Now;
                    }
                    nv.GioiTinh = model.GioiTinh;
                    nv.SDT = model.SDT;
                    nv.CMT = model.Cmt;
                    nv.DiaChi = model.DiaChi;
                    nv.IsActive = true;
                    nv.TinhTrang = model.TinhTrang;
                    nv.AnhDaiDien = model.Avatar == "" || model.Avatar == null ? "no-avatar.png" : model.Avatar;
                    unitofwork.NhanViens.Update(nv);
                    if (nvpb != null)
                    {
                        nvpb.PhongBanID = model.PhongBan.GetValueOrDefault();
                        nvpb.ChucVuID = model.ChucVu.GetValueOrDefault();
                        nvpb.ThoiGian = DateTime.UtcNow;
                        nvpb.IsActive = true;
                    }
                    else
                    {

                        nvpb = new NhanVienPhongBan();
                        nvpb.NhanVienPhongBanID = Guid.NewGuid();
                        nvpb.NhanVienID = nv.NhanVienID;
                        nvpb.IsActive = true;
                        nvpb.ThoiGian = DateTime.UtcNow;
                        nvpb.PhongBanID = model.PhongBan.GetValueOrDefault();
                        nvpb.ChucVuID = model.ChucVu.GetValueOrDefault();
                        unitofwork.NhanVienPhongBans.Add(nvpb);
                    }

                    IDNV = nv.NhanVienID.ToString();
                }
                unitofwork.Save();

                return Json(new { code = "success", idnhanvien = IDNV }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Language.GetText("coloiphatsinhtrongquatrinhtaomoi") }, JsonRequestBehavior.AllowGet);
            }

        }

        private static string convertToUnSign3(string s)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = s.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public ActionResult ViewChung(string NhanVienID)
        {
            unitofwork = new UnitOfWork();
            Add_Update_ModelNhanVien item = new Add_Update_ModelNhanVien();
            Guid IDTong = Guid.Parse("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF");
            //List<itemCongTrinh_NhanVien> lstCongTrinh = unitofwork.Context.DuAns.Where(p => p.Dept == 1 && p.IsActive == true).Select(z => new itemCongTrinh_NhanVien
            //{
            //    value = z.DuAnID,
            //    label = z.TenDuAn
            //}).Where(z => z.value != IDTong).ToList();
            //item.CT = lstCongTrinh;
            item.NhanVienID = Guid.Parse(NhanVienID);
            return PartialView("_ViewChung", item);
        }
        public ActionResult ThongTinChung(string NhanVienID)
        {
            unitofwork = new UnitOfWork();
            Add_Update_ModelNhanVien item = new Add_Update_ModelNhanVien();
            Guid IDTong = Guid.Parse("FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF");
            //List<itemCongTrinh_NhanVien> lstCongTrinh = unitofwork.Context.DuAns.Where(p => p.Dept == 1 && p.IsActive == true).Select(z => new itemCongTrinh_NhanVien
            //{
            //    value = z.DuAnID,
            //    label = z.TenDuAn
            //}).Where(z => z.value != IDTong).ToList();
            //item.CT = lstCongTrinh;
            item.NhanVienID = Guid.Parse(NhanVienID);
            return PartialView("_AddorUpdateNhanVien", item);
        }
        public JsonResult DanhSachPhongBan()
        {
            unitofwork = new UnitOfWork();
            var list = unitofwork.Context.PhongBans.Where(p => p.IsActive == true);
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayChiTietNhanVien(string NhanVienID)
        {
            unitofwork = new UnitOfWork();
            Guid IDNhanVien = Guid.Parse(NhanVienID);
            Add_Update_ModelNhanVien item = new Add_Update_ModelNhanVien();
            List<string> lst = new List<string>();

            item.CongTrinh = lst;
            item.NhanVienID = Guid.Parse(NhanVienID);
            item.ItemNhanVien = unitofwork.NhanViens.LayChiTietNhanVienNew(IDNhanVien);
            //lst.Add(item.ItemNhanVien.DuAnID.ToString());
            //if (item.ItemNhanVien.Dept == 0)
            //{
            //    List<itemCongTrinh_NhanVien> lstCongTrinh = unitofwork.Context.DuAns.Where(p => p.Dept == 1 && p.IsActive == true).Select(z => new itemCongTrinh_NhanVien
            //    {
            //        value = z.DuAnID,
            //        label = z.TenDuAn
            //    }).ToList();
            //    item.CT = lstCongTrinh;
            //}
            return Json(new { data = item }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult XoaNhanVien(string NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                unitofwork.NhanViens.XoaNhanVien(Guid.Parse(NhanVienID));
                return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Language.GetText("coloitrongquatrinhxulydulieu") }, JsonRequestBehavior.AllowGet);
            }
        }
        public clsExcelResult XuatTemplate()
        {
            string fileName = "";
            fileName = "/NhanVienTemplate.xlsx";
            string urlTemp = "";
            if (Language.GetKeyLang() == "cn")
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/NhanVienTempImportCN.xlsx");
            }
            else
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/NhanVienTempImport.xlsx");
            }
            clsExcelResult clsResult = new clsExcelResult();
            var temp = new XlsFile(true);
            temp.Open(urlTemp);
            ExcelFile xls = temp;
            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }

        public JsonResult CheckKetQua()
        {
            string iUploadedCnt = "";
            string sPath = "";
            sPath = Server.MapPath("~/TempExcel/Import/TempFile/");
            unitofwork = new UnitOfWork();
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                if (hpf.ContentLength > 0)
                {
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        iUploadedCnt = (sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        if (hpf != null && hpf.ContentLength > 0)
                        {
                            Stream stream = hpf.InputStream;
                            IExcelDataReader reader = null;
                            if (hpf.FileName.EndsWith(".xls"))
                            {
                                reader = ExcelReaderFactory.CreateBinaryReader(stream);
                            }
                            else if (hpf.FileName.EndsWith(".xlsx"))
                            {
                                reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                            }
                            else
                            {

                            }
                            reader.IsFirstRowAsColumnNames = true;
                            DataSet result = reader.AsDataSet();
                            var listct = result.Tables[0].AsEnumerable().Select(p => new
                            {
                                TenNhanVien = p[0].ToString(),
                                GioiTinh = p[1].ToString(),
                                NgaySinh = p[2].ToString(),
                                SDT = p[3].ToString(),
                                CMT = p[4].ToString(),
                                DiaChi = p[5].ToString(),
                                TinhTrang = p[6].ToString(),
                            });
                            reader.Close();
                            return Json(new { data = listct }, JsonRequestBehavior.AllowGet);
                        }
                        // hpf.SaveAs(iUploadedCnt);

                    }
                }
            }
            var message = "";
            return Json(new { url = iUploadedCnt, message = message }, JsonRequestBehavior.AllowGet);


        }

        public JsonResult ImportNhanVien(string data)
        {
            //Loaihinh=1: Toàn bộ;Loaihinh=2:Bỏ qua trùng
            unitofwork = new UnitOfWork();
            List<NhanVienAdd_Request> EmpList = new List<NhanVienAdd_Request>();
            dynamic model = new JavaScriptSerializer().Deserialize<dynamic>(data);
            for (int i = 0; i < model.Length; i++)
            {
                try
                {
                    int GioiTinh = 0;
                    int TinhTrang = 1;
                    string strGioiTinh = model[i]["GioiTinh"].Trim().ToLower().Trim().Replace(" ", "");
                    switch (strGioiTinh)
                    {
                        case "nam": GioiTinh = 0; break;
                        case "nữ": GioiTinh = 1; break;
                        case "男": GioiTinh = 0; break;
                        case "女": GioiTinh = 1; break;
                        default: GioiTinh = 0; break;
                    }

                    string strTinhTrang = model[i]["TinhTrang"].Trim().ToLower().Trim().Replace(" ", "");
                    switch (strTinhTrang)
                    {
                        case "đanglàmviệc": TinhTrang = 1; break;
                        case "đangnghỉchờviệc": TinhTrang = 2; break;
                        case "Thôiviệc": TinhTrang = 3; break;
                        case "在工作": TinhTrang = 1; break;
                        case "待工作": TinhTrang = 2; break;
                        case "辞职": TinhTrang = 3; break;
                        default: TinhTrang = 0; break;
                    }


                    NhanVien nv = new NhanVien();
                    NhanVienPhongBan nvpb = new NhanVienPhongBan();
                    nv.NhanVienID = Guid.NewGuid();
                    nv.TenNhanVien = model[i]["TenNhanVien"].Trim();
                    try
                    {
                        nv.NgaySinh = DateTime.ParseExact(model[i]["NgaySinh"].Trim(), "dd/MM/yyyy", null);
                    }
                    catch (Exception ex)
                    {
                        nv.NgaySinh = DateTime.Now;
                    }
                    nv.GioiTinh = GioiTinh;
                    nv.SDT = model[i]["SDT"].Trim();
                    nv.CMT = model[i]["CMT"].Trim();
                    nv.DiaChi = model[i]["DiaChi"].Trim();
                    nv.TinhTrang = TinhTrang;


                    nv.IsActive = true;

                    nvpb.NhanVienID = nv.NhanVienID;
                    nvpb.IsActive = true;
                    nvpb.NhanVienPhongBanID = Guid.NewGuid();
                    nvpb.ThoiGian = DateTime.Now;
                    nvpb.PhongBanID = Guid.Parse("ffffffff-ffff-ffff-ffff-ffffffffffff");
                    nvpb.ChucVuID = Guid.Parse("7D032D17-C8EA-4416-A321-3B3115CB687A");
                    unitofwork.NhanVienPhongBans.Add(nvpb);
                    unitofwork.NhanViens.Add(nv);
                    unitofwork.Save();
                }
                catch (Exception ex)
                {

                }
            }

            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }

        private string getvalue(System.Array row, int i)
        {
            try
            {
                return row.GetValue(1, i).ToString();
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        public JsonResult DeleteFile(string url)
        {
            if (System.IO.File.Exists(url))
            {
                System.IO.File.Delete(url);
            }
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }

    }
}
