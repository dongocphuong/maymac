﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DFWeb.Models;
using DFWeb.Security;
using DF.DataAccess;
using DF.DataAccess.DBML;
using System.Configuration;
using DF.DBMapping.ModelsExt;
using DFWeb.Models.DataResult.HoSoCaNhan;
using System.Web.Script.Serialization;
using DFWeb.Common;

namespace DFWeb.Controllers
{
    public class ThongTinCaNhanController : Controller
    {
        //
        // GET: /ThongTinCaNhan/

        AccountInfo uerx = CookiePersister.getAcountInfo();
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        string nameCookie = ConfigurationManager.AppSettings["name_Cookie_Language"];
        public ActionResult Index()
        {
            ViewBag.Title_Function = Models.Language.GetText("thongtincanhan");
            unitofwork = new UnitOfWork();
            Add_Update_ModelNhanVien item = new Add_Update_ModelNhanVien();
            //List<itemCongTrinh_NhanVien> lstCongTrinh = unitofwork.Context.DuAns.Where(p=>p.Dept==1 && p.IsActive==true).Select(z => new itemCongTrinh_NhanVien
            //{
            //    value = z.DuAnID,
            //    label = z.TenDuAn
            //}).ToList();
            //item.CT = lstCongTrinh;
            item.NhanVienID = Guid.Parse(uerx.NhanVienID);
            return View(item);
        }
       
        //public JsonResult getDataCongTrinh()
        //{
        //    var listCongTrinh = unitofwork.Context.DuAns.Where(t => t.IsActive == true && t.Dept==0);
        //    return Json(new { data = listCongTrinh }, JsonRequestBehavior.AllowGet);
        //}
        //public ActionResult GetDataThongTinCaNhan(string NhanVienID)
        //{

        //    string lang = "vn";
        //    if (Request.Cookies[nameCookie] != null)
        //    {
        //        lang = Request.Cookies[nameCookie].Value;
        //    }
        //    db = new DBMLDFDataContext();
        //    unitofwork = new UnitOfWork();
        //    Guid IDNhanVien = Guid.Parse(NhanVienID);
        //    ThongTinCaNhanResult DataThongTinCaNhan = new ThongTinCaNhanResult();
        //    var obj = db.sp_GetInfoNhanVien_ThongTin(IDNhanVien).FirstOrDefault();

            
        //    DataThongTinCaNhan.NhanVienID = obj.NhanVienID;
        //    DataThongTinCaNhan.TenNhanVien = obj.TenNhanVien;
        //    DataThongTinCaNhan.Avatar = obj.Avatar == "" || obj.Avatar == null ? "no-avatar.png" : obj.Avatar;
        //    DataThongTinCaNhan.NgaySinh = obj.NgaySinh;
        //    DataThongTinCaNhan.GioiTinh = obj.GioiTinh;
        //    DataThongTinCaNhan.SDT = obj.SDT;
        //    DataThongTinCaNhan.TenNganHang = obj.TenNganHang;
        //    DataThongTinCaNhan.SoTaiKhoanNganHang = obj.SoTaiKhoanNganHang;
        //    DataThongTinCaNhan.CMND = obj.Cmt;
        //    DataThongTinCaNhan.NgayCapCMND = obj.NgayCapCMT;
        //    DataThongTinCaNhan.NoiCapCMND = obj.NoiCapCMT;
        //    DataThongTinCaNhan.DiaChi = obj.DiaChi;
        //    DataThongTinCaNhan.TenCongTrinh = obj.TenDuAn;
        //    DataThongTinCaNhan.ChucVuId = obj.ChucVuID;
        //    DataThongTinCaNhan.TinhTrang = obj.TinhTrang.ToString();
        //    DataThongTinCaNhan.LoaiHopDong = obj.LoaiHopDong.ToString();
        //    DataThongTinCaNhan.NgayDiLam =obj.NgayDiLam.HasValue?obj.NgayDiLam.GetValueOrDefault().ToString("dd/MM/yyyy"):"";
        //    DataThongTinCaNhan.NgayHetHanHopDong =obj.NgayKetThucHd.HasValue?obj.NgayKetThucHd.GetValueOrDefault().ToString("dd/MM/yyyy"):"";
        //    DataThongTinCaNhan.NgayThoiViec = obj.NgayThoiViec.HasValue?obj.NgayThoiViec.GetValueOrDefault().ToString("dd/MM/yyyy"):"";
        //    DataThongTinCaNhan.CongTrinhID = obj.DuAnID;
        //    return PartialView("_ThongTinCaNhan", DataThongTinCaNhan);
        //}
        //public JsonResult getDataChucVu()
        //{
        //    unitofwork = new UnitOfWork();
        //    var list = unitofwork.ChucVus.GetAll().OrderBy(t => t.TenChucVu).ToList();
        //    return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        //}
        //public JsonResult UpdateThongTinNhanVien(string models)
        //{
        //    string lang = "vn";
        //    if (Request.Cookies[nameCookie] != null)
        //    {
        //        lang = Request.Cookies[nameCookie].Value;
        //    }
        //    unitofwork = new UnitOfWork();

        //    UpdateThongTinNhanVienModel model = new JavaScriptSerializer().Deserialize<UpdateThongTinNhanVienModel>(models);
        //    string code = "success";
        //    //string message = "Thành Công";
        //    var obj = unitofwork.Context.NhanViens.Find(model.NhanVienID);
        //    obj.TenNhanVien = model.TenNhanVien;
        //    obj.Avatar = model.Avatar == "" || model.Avatar == null ? "no-avatar.png" : model.Avatar;
        //    if (model.NgaySinh == "" || model.NgaySinh == null)
        //        obj.NgaySinh = DateTime.Now;
        //    else
        //        obj.NgaySinh = DateTime.ParseExact(model.NgaySinh, "dd/MM/yyyy", null);
        //    obj.GioiTinh = model.GioiTinh;
        //    obj.SDT = model.SDT;
        //    obj.TenNganHang = model.TenNganHang;
        //    obj.SoTaiKhoanNganHang = model.SoTaiKhoanNganHang;
        //    obj.Cmt = model.CMND;
        //    if (model.NgayCapCMND == "" || model.NgayCapCMND == null) obj.NgayCapCMT = null;
        //    else obj.NgayCapCMT = DateTime.ParseExact(model.NgayCapCMND, "dd/MM/yyyy", null);
        //    obj.NoiCapCMT = model.NoiCapCMND;
        //    obj.DiaChi = model.DiaChi;
        //    obj.TinhTrang = model.TinhTrang;
        //    obj.LoaiHopDong = model.LoaiHopDong;
        //    obj.NgayDiLam = model.NgayDiLam;
        //    obj.NgayKetThucHd = model.NgayHetHanHopDong;
        //    obj.NgayThoiViec = model.NgayThoiViec;
        //    obj.ChucVuID = model.ChucVuID;
        //    unitofwork.NhanViens.Update(obj);
        //    unitofwork.Save();
        //    if (Guid.Parse(uerx.NhanVienID) == model.NhanVienID)
        //    {
        //        uerx.Avatar = ConfigurationManager.AppSettings["urlShowNhanVien"] + model.Avatar == "" || model.Avatar == null ? "no-avatar.png" : model.Avatar;
        //    }
        //    return Json(new { code = code, message = Models.Language.Get_MessageLanguage("success", lang) }, JsonRequestBehavior.AllowGet);
        //}
    }
}
