﻿using DF.DataAccess;
using DF.DataAccess.DBML;
using DF.DBMapping.Models;
using DFWeb.Models;
using DFWeb.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace DFWeb.Controllers.HanhChinh
{
    [CustomAuthorize(Roles = "1056")]
    public class QuanLyTaiKhoanController : Controller
    {
        //
        // GET: /QuanLyTaiKhoan/
        protected IUnitOfWork unitofwork;
        public ActionResult Index()
        {
            unitofwork = new UnitOfWork();
            List<DF.DataAccess.DBML.Web_HanhChinh_QuanLyTaiKhoan_LayDanhSachTaiKhoanNVResult> model = new List<DF.DataAccess.DBML.Web_HanhChinh_QuanLyTaiKhoan_LayDanhSachTaiKhoanNVResult>();
            model = unitofwork.Users.LayDSTaiKhoanNV();
            return View(model);
        }
        public JsonResult AddUserName(string UserName, string NhanVienID, string Password)
        {
            unitofwork = new UnitOfWork();
            List<User> soluongnvcungten = unitofwork.Users.GetAll().Where(p => p.UserName.Trim().ToLower() == UserName.Trim().ToLower()).ToList();
            string tentk = UserName.Trim().ToLower();
            if (soluongnvcungten != null && soluongnvcungten.Count > 0)
            {
                return Json(new { code = "warning", message = Language.GetText("taikhoandatontai") }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                Guid IDNhanVien = Guid.Parse(NhanVienID);
                List<User> lstuser = unitofwork.Users.GetAll().Where(p => p.NhanVienId ==IDNhanVien).ToList();
                if(lstuser.Count > 0)
                {
                    return Json(new { code = "warning", message = Language.GetText("nhanviendatontaiuser") }, JsonRequestBehavior.AllowGet);
                }
            }
            try
            {
                if (unitofwork.Users.ThemUserNameNV(UserName, Password, Guid.Parse(NhanVienID)))
                {
                    return Json(new { code = "success", message = Models.Language.GetText("mgs_capnhatthanhcong") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("coloitrongquatrinhxulydulieu") }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("coloitrongquatrinhxulydulieu") }, JsonRequestBehavior.AllowGet);
            }
           
        }
    }
}
