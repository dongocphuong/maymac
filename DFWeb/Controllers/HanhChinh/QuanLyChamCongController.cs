﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DF.DataAccess.DBML;
using DF.DBMapping.Models;
using DF.DataAccess;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using System.Globalization;
using DFWeb.Security;

namespace DFWeb.Controllers.HanhChinh
{
    [CustomAuthorize(Roles = "1049")]
    public class QuanLyChamCongController : Controller
    {
        //
        // GET: /QuanLyChamCong/
        protected DBMLDFDataContext db;
        protected UnitOfWork unitofwork;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAllDataNhanVien()
        {
            db = new DBMLDFDataContext();
            var dt = db.Web_ChamCong_LayDanhSachDuLieuNhanVien().ToList();
            return Json(new { data = dt, code = "success", message = "Lấy dữ liệu thành công" });
        }
        public JsonResult Submit(string data, string TuNgay, string DenNgay)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            DateTime tn, dn;
            tn = DateTime.ParseExact(TuNgay, "d/M/yyyy", CultureInfo.InvariantCulture);
            dn = DateTime.ParseExact(DenNgay, "d/M/yyyy", CultureInfo.InvariantCulture);
            db.Web_ChamCong_XoaBangCongTheoThang(tn, dn);
            var model = new JavaScriptSerializer().Deserialize<List<ChamCongModels>>(data);
            foreach (var i in model)
            {
                ChamCong cc = new ChamCong();
                cc.ChamCongID = Guid.NewGuid();
                cc.NhanVienID = i.NhanVienID;
                cc.SoCong = i.SoCong;
                cc.ThoiGian = DateTime.ParseExact(i.ThoiGian, "d/M/yyyy", CultureInfo.InvariantCulture);
                unitofwork.Context.ChamCongs.Add(cc);
            }
            unitofwork.Save();
            return Json(new { code = "success", message = "Lưu thành công" });
        }
        public JsonResult GetAllDataBangChamCong(string TuNgay, string DenNgay)
        {
            db = new DBMLDFDataContext();
            DateTime tn, dn;
            tn = DateTime.ParseExact(TuNgay, "d/M/yyyy", CultureInfo.InvariantCulture);
            dn = DateTime.ParseExact(DenNgay, "d/M/yyyy", CultureInfo.InvariantCulture);
            var dt = db.Web_ChamCong_LayToanBoDuLieuBangChamCongTheoThang(tn, dn).ToList();
            return Json(new { data = dt, code = "success" });
        }
        public JsonResult GetTongSoCongTheoThang(string TuNgay, string DenNgay)
        {
            db = new DBMLDFDataContext();
            DateTime tn, dn;
            tn = DateTime.ParseExact(TuNgay, "d/M/yyyy", CultureInfo.InvariantCulture);
            dn = DateTime.ParseExact(DenNgay, "d/M/yyyy", CultureInfo.InvariantCulture);
            var dt = db.Web_ChamCong_TongCongCuaNhanVienTheoThang(tn, dn).ToList();
            return Json(new { data = dt, code = "success" });
        }
    }
}
