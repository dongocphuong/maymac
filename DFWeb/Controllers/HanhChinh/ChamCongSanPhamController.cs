﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DF.DataAccess.DBML;
using DF.DataAccess;
using DF.DBMapping.Models;
using DF.DBMapping.ModelsExt;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt.HanhChinh;

namespace DFWeb.Controllers.HanhChinh
{
    public class ChamCongSanPhamController : Controller
    {
        //
        // GET: /ChamCongSanPham/
        protected DBMLDFDataContext db;
        protected UnitOfWork uow;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAllData(string ThoiGian = "")
        {
            db = new DBMLDFDataContext();
            var tg = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy", null);
            var dt = db.Web_ChamCongSanPham_GetAllDataIfHaveValue().Where(x => x.ThoiGian == tg).ToList();
            if(dt.Count == 0)
            {
                var data = db.Web_ChamCongSanPham_GetAllData().ToList();
                return Json(new { data = data, code = "success", message = "Lấy dữ liệu thành công" });
            }
            else
            {
                return Json(new { data = dt, code = "success", message = "Lấy dữ liệu thành công" });
            }
        }
        public JsonResult AddOrUpdate(string data, string ThoiGian)
        {
            uow = new UnitOfWork();
            db = new DBMLDFDataContext();
            var banghi = new JavaScriptSerializer().Deserialize<List<ChamCongSanPhamModel>>(data);
            db.Web_ChamCongSanPham_DeleteOldData(DateTime.ParseExact(ThoiGian, "dd/MM/yyyy", null));
            foreach (var item in banghi)
            {
                var cc = new ChamCongSanPham();
                cc.ChamCongSanPhamID = Guid.NewGuid();
                cc.NhanVienID = item.NhanVienID;
                cc.DonGia = item.DonGia;
                cc.KhoangThoiGian = item.KhoangThoiGian;
                if(item.MaSanPham == null)
                {
                    cc.MaSanPham = "";
                }
                else
                {
                    cc.MaSanPham = item.MaSanPham;
                }
                cc.SoLuong = item.SoLuong;
                cc.ThoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy", null);
                uow.Context.ChamCongSanPhams.Add(cc);
            }
            uow.Save();
            return Json(new { code = "success", message = "Lấy dữ liệu thành công" });
        }
    }
}
