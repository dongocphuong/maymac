﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DFWeb.Security;
using Resources;
using DFWeb.Common;
using DFWeb.Models;
using DF.DataAccess;
using System.Data;
using DF.DBMapping.Models;
using System.IO;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using DF.DataAccess.DBML;
using DF.DBMapping.ModelsExt;


namespace DFWeb.Controllers
{
    public class HomeController : Controller
    {
        protected IUnitOfWork unitOfWork;
        //
        // GET: /Home/


        public ActionResult Index()
        {
            return View();
        }
        public JsonResult LayDanhSachDoiTac()
        {
            UnitOfWork unitofwork = new UnitOfWork();
            var data = unitofwork.Context.DoiTacs.Where(z => z.IsActive == true).ToList();
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
        }
      
        public JsonResult DSChucVu()
        {
            UnitOfWork unitofwork = new UnitOfWork();
            var data = unitofwork.Context.ChucVus.Where(z => z.IsActive == true).Select(x => new
            {
                value = x.ChucVuId,
                text = x.TenChucVu
            }).ToList();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DoiMK(string mkcu, string mkmoi)
        {
            unitOfWork = new UnitOfWork();
            AccountInfo uerx = CookiePersister.getAcountInfo();
            Guid uid = Guid.Parse(uerx.UserID);
            User u = unitOfWork.Users.GetAll().Where(us => us.UserId == uid).SingleOrDefault();
            if (u.Password == mkcu)
            {
                bool check = unitOfWork.Users.UpdatePassWord(uid, mkmoi);
                return Json(check);
            }
            return Json(false);
        }

        public JsonResult Language()
        {
            var language = DFWeb.Models.Language.GetLanguage();
            return Json(new { language }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetLanguage(string language)
        {
            DFWeb.Models.Language.SetLanguage(language);
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }


        public bool Takejsonlanguagefile()
        {
            var a = DFWeb.Models.Language.ResetLanguage();
           var str =System.IO.File.ReadAllText(Server.MapPath("~/Content/lib/language2.js"));
           using (StreamWriter file = System.IO.File.CreateText(Server.MapPath("~/Content/lib/Language.js")))
            {
                var language = new {language=DFWeb.Models.Language.GetLanguage()};
                var txt = new JavaScriptSerializer().Serialize(language);
                str = str.Replace("['aaaaa']", txt);
                file.Write(str);
            }
            return true;
        }

        public JsonResult GetAllDataDonHang(string TuNgay, string DenNgay)
        {
            DateTime tn, dn;
            var db = new DBMLDFDataContext();
            try
            {
                tn = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            }
            catch
            {
                tn = DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
            }
            try
            {
                dn = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null);
            }
            catch
            {
                dn = DateTime.Now;
            }
            var data = db.Web_DonHang_GetAllDataDonHang_Filter(tn, dn).OrderByDescending(t => t.ThoiGian).ToList();
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
        }
    }
}
