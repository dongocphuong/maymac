﻿using DF.DataAccess;
using DF.Utilities.Cache;
using DF.Utilities.Ftp;
using DFWeb.Common;
using DFWeb.Models;
using DFWeb.Security;
using DFWeb.ViewModels;
using DKKhamModule.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Script.Serialization;

namespace DFWeb.Controllers
{
    public class AccountController : Controller
    {
        protected IUnitOfWork unitOfWork;
        //
        // GET: /Account/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View("Login");
        }
        public ActionResult Logout()
        {
            CookiePersister.removeAccountInfo();
            return View("Login");
        }

        [HttpPost]
        public ActionResult Login(AccountViewModel avm)
        {
            unitOfWork = new UnitOfWork();
            AccountModel am = new AccountModel();
            Account acc = new Account();
            if (string.IsNullOrEmpty(avm.Username) || string.IsNullOrEmpty(avm.Password))
            {
                ViewBag.Error = "Account's Invalid";
                return View("Login");
            }
            var user = unitOfWork.Users.GetByUserName(avm.Username);
            if (user != null)
            {
                if (user.Password == avm.Password)
                {
                    var nv = unitOfWork.NhanViens.GetByNhanVienID(user.NhanVienId);
                    var roles = unitOfWork.GroupUsers.GetByUserID(user.UserId);
                    List<string> rolesArr = new List<string>();
                    for (int i = 0; i < roles.Count; i++)
                    {
                        var groups = unitOfWork.Groups.GetByGroupID(roles[i].GroupGuid).FirstOrDefault();
                        if (groups != null)
                        {
                            rolesArr.Add(groups.RuleType.ToString());
                        }

                    }
                    AccountInfo waccinfo = new AccountInfo();
                    waccinfo.UserID = user.UserId.ToString();
                    waccinfo.Username = user.UserName;
                    waccinfo.HoTen = nv.TenNhanVien;
                    waccinfo.NhanVienID = nv.NhanVienID.ToString();
                    waccinfo.Avatar = ConfigurationManager.AppSettings["ShowImgNV"] + nv.AnhDaiDien;
                    waccinfo.Roles = rolesArr.ToArray();
                    CookiePersister.setAccoutInfo(waccinfo);
                    return RedirectToAction("Index", "Home");
                }
            }
            ViewBag.ErrorLog = Models.Language.GetText("tendangnhaphoacmatkhaukhongdung");
            return View("Login");
        }

    }
}
