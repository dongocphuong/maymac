﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using DF.DataAccess;
using DF.DBMapping.Models;
using DF.Utilities.Cache;
using DF.Utilities.Enums;
using System.Linq;

namespace DF.Controllers
{
    public abstract class BaseController : Controller
    {
        protected IUnitOfWork unitOfWork;
        protected ICacheProvider cacheProvider;
        protected List<string> PermistionList;
        protected BaseController(IUnitOfWork uow, ICacheProvider cache)
        {
            this.cacheProvider = cache;
            this.unitOfWork = uow;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="callback"></param>
        ///// <param name="state"></param>
        ///// <returns></returns>
        //protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        //{
        //    User user = unitOfWork.Users.GetByUserName(userName: User.Identity.Name);
        //    if (user != null)
        //    {
        //        if (string.IsNullOrEmpty(user.Session_Token))
        //        {
        //            Session.Clear();
        //            System.Web.Security.FormsAuthentication.SignOut();
        //            //ExecuteResultError();
        //        }
        //        else
        //        {
        //            string cultureName = @"vi-VN";//null;
        //            if (Request.Cookies["_culture"] != null) {
        //                cultureName = Request.Cookies["_culture"].Value.ToString();
        //            };
        //            //var cultureName2 = Request.Cookies["_culture"] ?? "vi-VN";
        //            // Modify current thread's cultures            
        //            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
        //            Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        //            return base.BeginExecuteCore(callback, state);
        //        }
        //    }
        //    else
        //    {
        //        string cultureName = @"vi-VN";//null;
        //        if (Request.Cookies["_culture"] != null)
        //        {
        //            cultureName = Request.Cookies["_culture"].Value.ToString();
        //        };
        //        // Modify current thread's cultures            
        //        Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cultureName);
        //        Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
        //        return base.BeginExecuteCore(callback, state);

        //    }
        //    return null;

        //}

        //protected override void EndExecute(IAsyncResult asyncResult)
        //{
        //    string controllerName = HttpContext.Request.RequestContext.RouteData.Values[@"controller"].ToString().ToLower();
        //    if (controllerName.Contains(@"category") || controllerName.Contains(@"docversion"))
        //    {
        //        // Xóa cache
        //        cacheProvider.RemoveByTerm("FORM-");
        //    }
        //    if (controllerName.Contains(@"user"))
        //    {
        //        // Xóa cache
        //        cacheProvider.RemoveByTerm("OFFICELIST-");
        //    }
        //    base.EndExecute(asyncResult);
        //}

        /// <summary>
        /// Hàm lấy thông tin người dùng đăng nhập
        /// </summary>
        public User UserX
        {
            get
            {
                return unitOfWork.Users.GetByUserName(this.User.Identity.Name);
            }
        }

    }
}
