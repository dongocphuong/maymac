﻿using DF.DBMapping.ModelsExt;
using DF.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DFWeb.Controllers
{
    public class UploadController : Controller
    {
        string ImageQLNV = System.Configuration.ConfigurationManager.AppSettings["UrlUploadNV"]; //1
        string ImageQLTB = System.Configuration.ConfigurationManager.AppSettings["UrlUploadTB"];//2
        string ImageQLVT = System.Configuration.ConfigurationManager.AppSettings["UrlUploadVT"];//3
        string ImageQLDN = System.Configuration.ConfigurationManager.AppSettings["UrlUploadDN"];//4
        string ImageChat = System.Configuration.ConfigurationManager.AppSettings["UrlUploadChat"];//5
        string ImageQLCCDC = System.Configuration.ConfigurationManager.AppSettings["UrlUpLoadCCDC"];//6
        string ImageHopDong = System.Configuration.ConfigurationManager.AppSettings["UrlUploadHopDong"];//7
        string ImageBL = System.Configuration.ConfigurationManager.AppSettings["UrlUploadBL"];//8
        string ImageTS = System.Configuration.ConfigurationManager.AppSettings["UrlUploadTaiSan"];//9
        string ImageQLTC = System.Configuration.ConfigurationManager.AppSettings["UrlUploadTC"];
        string ImageThongBao = System.Configuration.ConfigurationManager.AppSettings["UrlUploadImageThongBao"];

        public string UploadImage(int LoaiHinh)
        {
            string sPath = "";
            switch (LoaiHinh)
            {
                case 1: sPath = ImageQLNV; break;
                case 2: sPath = ImageQLTB; break;
                case 3: sPath = ImageQLVT; break;
                case 4: sPath = ImageQLDN; break;
                case 5: sPath = ImageChat; break;
                case 6: sPath = ImageQLCCDC; break;
                case 7: sPath = ImageHopDong; break;
                case 8: sPath = ImageBL; break;
                case 9: sPath = ImageTS; break;
                default: sPath = ImageQLNV; break;
            }
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.


            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }
        public ActionResult SaveAvatar(IEnumerable<HttpPostedFileBase> Avatar)
        {
            // The Name of the Upload component is "files"
            if (Avatar != null)
            {
                foreach (var file in Avatar)
                {
                    // Some browsers send file names with full path. This needs to be stripped.
                    var fileName = Path.GetFileName(file.FileName);
                    var physicalPath = Path.Combine(ImageQLNV, fileName);

                    //The files are not actually saved in this demo
                    file.SaveAs(physicalPath);
                }
            }
            // Return an empty string to signify success
            return Content("");
        }
        private IEnumerable<string> GetFileInfo(IEnumerable<HttpPostedFileBase> files)
        {
            return
                from a in files
                where a != null
                select string.Format("{0} ({1} bytes)", Path.GetFileName(a.FileName), a.ContentLength);
        }

        [HttpPost()]
        public string Uploadfiles()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageQLVT;

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }

        [HttpPost()]
        public string UploadAvatarNhanVien()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageQLNV;

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }
        [HttpPost()]
        public string UploadfilesSuaChua()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageQLTB;

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }

        [HttpPost()]
        public string UploadfilesBinhLuan()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageBL;
            bool exists = System.IO.Directory.Exists(sPath);
            if (!exists)
            {
                System.IO.Directory.CreateDirectory(sPath);
            }
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }

        [HttpPost()]
        public string UploadfilesDeNghi()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageQLDN;
            bool exists = System.IO.Directory.Exists(sPath);
            if (!exists)
            {
                System.IO.Directory.CreateDirectory(sPath);
            }
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }

        [HttpPost()]
        public string UploadfilesDeNghiTC()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageQLTC;
            bool exists = System.IO.Directory.Exists(sPath);
            if (!exists)
            {
                System.IO.Directory.CreateDirectory(sPath);
            }
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }
        [HttpPost()]
        public string UploadfilesAvatar()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageQLTB;

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }

        [HttpPost()]
        public string UploadfilesAvatarCCDC()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageQLCCDC;

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }
        [HttpPost()]
        public string UploadfilesAvatarThietBi()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageQLCCDC;

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }
        [HttpPost()]
        public string UploadVanBan()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageHopDong;

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }

        [HttpPost()]
        public string UploadTaiSan()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageTS;
            bool exists = System.IO.Directory.Exists(sPath);
            if (!exists)
            {
                System.IO.Directory.CreateDirectory(sPath);
            }
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }
        [HttpPost()]
        public string UploadHopDong()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageHopDong;

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }

        [HttpPost()]
        public JsonResult UploadHopDong2()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = "";
            sPath = ImageHopDong;

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            var list = new List<lstImageHopDong>();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                {
                    string ID = Guid.NewGuid().ToString();
                    // SAVE THE FILES IN THE FOLDER.
                    hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                    list.Add(new lstImageHopDong()
                    {
                        img = autoky + "_" + ID + Path.GetExtension(hpf.FileName),
                        name = hpf.FileName
                    });
                    iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");

                    // iUploadedCnt = iUploadedCnt + 1;
                }
            }

            return Json(list.FirstOrDefault(), JsonRequestBehavior.AllowGet);

        }

        [HttpPost()]
        public string UploadThongBao()
        {
            string iUploadedCnt = "/center/upload/ImageThongBao/";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = ImageThongBao;

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }

        [HttpPost()]
        public string UploadThongBao2()
        {
            string iUploadedCnt = "";

            // DEFINE THE PATH WHERE WE WANT TO SAVE THE FILES.
            string sPath = ImageThongBao;

            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];

                if (hpf.ContentLength > 0)
                {
                    // CHECK IF THE SELECTED FILE(S) ALREADY EXISTS IN FOLDER. (AVOID DUPLICATE)
                    if (!System.IO.File.Exists(sPath + autoky + "_" + Guid.NewGuid().ToString() + Path.GetExtension(hpf.FileName)))
                    {
                        string ID = Guid.NewGuid().ToString();
                        // SAVE THE FILES IN THE FOLDER.
                        hpf.SaveAs(sPath + autoky + "_" + ID + Path.GetExtension(hpf.FileName));
                        iUploadedCnt += (autoky + "_" + ID + Path.GetExtension(hpf.FileName) + ",");
                        // iUploadedCnt = iUploadedCnt + 1;
                    }
                }
            }

            if (iUploadedCnt != "") return iUploadedCnt.Substring(0, iUploadedCnt.Length - 1);
            else return iUploadedCnt;

        }
    }
}
