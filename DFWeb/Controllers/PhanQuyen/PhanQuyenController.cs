﻿using DF.DataAccess;
using DF.DBMapping.Models;
using DF.DBMapping.ModelsExt.PhanQuyen;
using DFWeb.Common;
using DFWeb.Models;
using DFWeb.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace DFWeb.Controllers
{
    public class PhanQuyenController : Controller
    {
        //
        // GET: /PhanQuyen/
        protected UnitOfWork unitofwork;

        [CustomAuthorize(Roles = "2022")]
        public ActionResult Index()
        {

            ViewBag.Title_Function = Models.Language.GetText("phanquyenchucnang");
            return View();
        }

        public JsonResult getDanhSachNhanVien()
        {
            unitofwork = new UnitOfWork();
            try
            {
                var objlist = (from a in unitofwork.Context.NhanViens
                               join b in unitofwork.Context.Users on a.NhanVienID equals b.NhanVienId
                               where a.IsActive == true
                               select new
                               {
                                   UserId = b.UserId,
                                   NhanVienID = a.NhanVienID,
                                   TenNhanVien = a.TenNhanVien,
                                   UserName = b.UserName,
                                   //CMT = a.Cmt,
                                   DiaChi = a.DiaChi,
                                   MatKhau = b.Password
                               }).ToList();
                return Json(new { code = "success", data = objlist, total = objlist.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhlaydulieu") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult genPhanQuyen(string userid)
        {
            List<PhanQuyenModel> lst = new List<PhanQuyenModel>();
            unitofwork = new UnitOfWork();
            try
            {
                Guid IDUser = Guid.Parse(userid);
                var objgroup = (from a in unitofwork.Context.tblGroups
                                select new
                                {
                                    TenQuyen = a.GroupName,
                                    GroupGuid = a.GroupGuid
                                }).OrderBy(z => z.TenQuyen).ToList();
                foreach (var item in objgroup)
                {
                    if ((from a in unitofwork.Context.tblGroupUsers where a.UserGuid == IDUser && a.GroupGuid == item.GroupGuid select a.GroupGuid).ToList().Count > 0)
                    {
                        lst.Add(new PhanQuyenModel
                        {
                            GroupGuid = item.GroupGuid,
                            IsActive = true,
                            TenGroup = item.TenQuyen,
                            UserID = IDUser
                        });
                        continue;
                    }

                    lst.Add(new PhanQuyenModel
                    {
                        GroupGuid = item.GroupGuid,
                        IsActive = false,
                        TenGroup = item.TenQuyen,
                        UserID = IDUser
                    });

                }
                return Json(new { code = "success", message = Models.Language.GetText("mgs_laydulieuthanhcong"), data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhlaydulieu") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult savePhanQuyen(string data, string userid)
        {
            unitofwork = new UnitOfWork();
            List<savePhanQuyenModel> lst = JsonConvert.DeserializeObject<List<savePhanQuyenModel>>(data);
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    unitofwork.tblGroupUsers.delPhanQuyen(userid);
                    foreach (var item in lst)
                    {
                        tblGroupUser gr = new tblGroupUser();
                        gr.GroupGuid = Guid.Parse(item.id);
                        gr.UserGuid = Guid.Parse(userid);
                        unitofwork.tblGroupUsers.Add(gr);
                    }
                    unitofwork.Save();
                    tran.Complete();
                }
                return Json(new { code = "success", message = Models.Language.GetText("mgs_capnhatdulieuthanhcong") }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("thatbai") }, JsonRequestBehavior.AllowGet);
            }
            
        }

        //[CustomAuthorize(Roles = "99999")]
        public ActionResult DanhMucPhanQuyen()
        {
            List<PhanQuyenDanhMucModels> grlist = new List<PhanQuyenDanhMucModels>();
            grlist = lstDM(2);
            return View(grlist);
        }

        public ActionResult SearchDM(int LoaiHinh)
        {
            List<PhanQuyenDanhMucModels> grlist = new List<PhanQuyenDanhMucModels>();
            grlist = lstDM(LoaiHinh);
            return PartialView("~/Views/QLLuongKhoanNhanVien/_DanhMucPhanQuyen.cshtml", grlist);
        }

        public JsonResult GETDMPhanQuyen(int LoaiHinh)
        {

            try
            {
                List<PhanQuyenDanhMucModels> grlist = new List<PhanQuyenDanhMucModels>();
                grlist = lstDM(LoaiHinh);
                return Json(new { code = "success", data = grlist, total = grlist.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhlaydulieu") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DSQuyenParent()
        {
            unitofwork = new UnitOfWork();
            try
            {
                var grlist = unitofwork.tblGroupUsers.GetDMPhanQuyenParent();
                return Json(new { code = "success", data = grlist, total = grlist.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhlaydulieu") }, JsonRequestBehavior.AllowGet);
            }
        }

        public List<PhanQuyenDanhMucModels> lstDM(int LoaiHinh)
        {
            unitofwork = new UnitOfWork();
            List<DF.DataAccess.DBML.Web_PhanQuyen_GetDanhMucPhanQuyenResult> lst = new List<DF.DataAccess.DBML.Web_PhanQuyen_GetDanhMucPhanQuyenResult>();
            List<DF.DataAccess.DBML.Web_PhanQuyen_GetDanhMucPhanQuyenResult> lst2 = new List<DF.DataAccess.DBML.Web_PhanQuyen_GetDanhMucPhanQuyenResult>();
            List<PhanQuyenDanhMucModels> grlist = new List<PhanQuyenDanhMucModels>();
            List<PhanQuyenDanhMucModels> grlistHeader = new List<PhanQuyenDanhMucModels>();
            lst = unitofwork.tblGroupUsers.GetDanhMucPhanQuyen(LoaiHinh);
            lst2 = lst.Where(z => z.IsHeader == true).OrderBy(z => z.Nhom).ToList();
            int icountHeader = 1;
            if (LoaiHinh == 2) // Nếu là web thì load theo phân cấp như thế này
            {
                foreach (var item in lst2)
                {
                    grlist.Add(new PhanQuyenDanhMucModels
                    {
                        Code = item.Code,
                        GroupGuid = item.GroupGuid,
                        GroupName = item.GroupName,
                        GroupParent = item.GroupParent,
                        Icon = item.Icon,
                        IsHeader = item.IsHeader,
                        KeyLanguage = item.KeyLanguage,
                        LoaiHinh = item.LoaiHinh,
                        MoTa = item.MoTa,
                        Nhom = item.Nhom,
                        RuleType = item.RuleType,
                        UrlLink = item.UrlLink,
                        STT = Utils.Utils.GetSoLaMa(icountHeader.ToString(), true),
                        STT2 = item.STT
                    });
                    lst2 = lst.Where(z => z.Nhom == item.Nhom && z.GroupParent == null && z.GroupGuid != item.GroupGuid && z.IsHeader == false).OrderBy(z => z.STT).ToList();
                    int icount = 1;
                    foreach (var items in lst2)
                    {
                        grlist.Add(new PhanQuyenDanhMucModels
                        {
                            Code = items.Code,
                            GroupGuid = items.GroupGuid,
                            GroupName = items.GroupName,
                            GroupParent = items.GroupParent,
                            Icon = items.Icon,
                            IsHeader = items.IsHeader,
                            KeyLanguage = items.KeyLanguage,
                            LoaiHinh = items.LoaiHinh,
                            MoTa = items.MoTa,
                            Nhom = items.Nhom,
                            RuleType = items.RuleType,
                            UrlLink = items.UrlLink,
                            STT = icount.ToString(),
                            STT2 = items.STT
                        });
                        lst2 = lst.Where(z => z.GroupParent == items.GroupGuid && z.IsHeader == false).OrderBy(z => z.STT).ToList();
                        foreach (var itemss in lst2)
                        {
                            grlist.Add(new PhanQuyenDanhMucModels
                            {
                                Code = itemss.Code,
                                GroupGuid = itemss.GroupGuid,
                                GroupName = itemss.GroupName,
                                GroupParent = itemss.GroupParent,
                                Icon = itemss.Icon,
                                IsHeader = itemss.IsHeader,
                                KeyLanguage = itemss.KeyLanguage,
                                LoaiHinh = itemss.LoaiHinh,
                                MoTa = itemss.MoTa,
                                Nhom = itemss.Nhom,
                                RuleType = itemss.RuleType,
                                UrlLink = itemss.UrlLink,
                                STT = "",
                                STT2 = itemss.STT
                            });
                        }
                        icount++;
                    }
                    icountHeader++;
                }
            }
            else
            {
                int icount = 1;
                foreach (var item in lst)
                {
                    grlist.Add(new PhanQuyenDanhMucModels
                    {
                        Code = item.Code,
                        GroupGuid = item.GroupGuid,
                        GroupName = item.GroupName,
                        GroupParent = item.GroupParent,
                        Icon = item.Icon,
                        IsHeader = item.IsHeader,
                        KeyLanguage = item.KeyLanguage,
                        LoaiHinh = item.LoaiHinh,
                        MoTa = item.MoTa,
                        Nhom = item.Nhom,
                        RuleType = item.RuleType,
                        UrlLink = item.UrlLink,
                        STT = icount.ToString(),
                        STT2 = item.STT
                    });
                    icount++;
                }

            }
            return grlist;
        }

        public JsonResult AddorUpdatePhanQuyen(string data)
        {
            unitofwork = new UnitOfWork();
            try
            {
                PhanQuyenDanhMucModels banghi = new JavaScriptSerializer().Deserialize<PhanQuyenDanhMucModels>(data);

                if (banghi.GroupGuid != null)
                {
                    bool kt = unitofwork.Context.tblGroups.Where(t => t.RuleType == banghi.RuleType && t.GroupGuid != banghi.GroupGuid).Count() > 0 ? true : false;
                    if (kt == false)
                    {
                        tblGroup gr = unitofwork.Context.tblGroups.Find(banghi.GroupGuid);
                        gr.Code = banghi.Code;
                        gr.GroupName = banghi.GroupName;
                        gr.GroupParent = banghi.GroupParent;
                        gr.Icon = banghi.Icon;
                        gr.KeyLanguage = banghi.KeyLanguage;
                        gr.LoaiHinh = banghi.LoaiHinh;
                        gr.MoTa = banghi.MoTa;
                        gr.Nhom = banghi.Nhom;
                        gr.RuleType = banghi.RuleType;
                        gr.UrlLink = banghi.UrlLink;
                        gr.STT = banghi.STT2;
                        unitofwork.Groups.Update(gr);
                        unitofwork.Save();
                        return Json(new { code = "success", message = "" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { code = "warning", message = "RuleType" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    bool kt = unitofwork.Context.tblGroups.Where(t => t.RuleType == banghi.RuleType).Count() > 0 ? true : false;
                    if (kt == false)
                    {
                        tblGroup gr = new tblGroup();
                        gr.Code = banghi.Code;
                        gr.GroupGuid = Guid.NewGuid();
                        gr.GroupName = banghi.GroupName;
                        gr.GroupParent = banghi.GroupParent;
                        gr.Icon = banghi.Icon;
                        gr.IsHeader = false;
                        gr.KeyLanguage = banghi.KeyLanguage;
                        gr.LoaiHinh = banghi.LoaiHinh;
                        gr.MoTa = banghi.MoTa;
                        gr.Nhom = banghi.Nhom;
                        gr.RuleType = banghi.RuleType;
                        gr.UrlLink = banghi.UrlLink;
                        gr.STT = banghi.STT2;
                        unitofwork.Groups.Add(gr);
                        unitofwork.Save();
                        return Json(new { code = "success", message = "" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { code = "warning", message = "RuleType" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Language.GetText("mgs_coloitrongquatrinhxulydulieu") }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult GenPhanQuyenByNhanVien(string NhanVienID, string UserID)
        {
            Guid ConfigMenu = Guid.Parse("97437315-3F68-4CAE-B3C5-6F93C31C0E65");
            unitofwork = new UnitOfWork();
            List<DF.DataAccess.DBML.Web_PhanQuyen_GetPhanQuyenByUserResult> lst = new List<DF.DataAccess.DBML.Web_PhanQuyen_GetPhanQuyenByUserResult>();
            List<DF.DataAccess.DBML.Web_PhanQuyen_GetPhanQuyenByUserResult> lst2 = new List<DF.DataAccess.DBML.Web_PhanQuyen_GetPhanQuyenByUserResult>();
            List<PhanQuyenByUserModels> lstreturn = new List<PhanQuyenByUserModels>();
            UserPhanQuyenModels objReturn = new UserPhanQuyenModels();
            try
            {
                lst = unitofwork.tblGroupUsers.GetPhanQuyenByUser(Guid.Parse(NhanVienID), 2);
                lst2 = lst.Where(z => z.IsHeader == true).OrderBy(z => z.Nhom).ToList();
                int icountHeader = 1;
                foreach (var item in lst2)
                {
                    lstreturn.Add(new PhanQuyenByUserModels
                    {
                        GroupGuid = item.GroupGuid,
                        GroupName = item.GroupName,
                        GroupParent = item.GroupParent,
                        IsHeader = item.IsHeader.Value,
                        KeyLang = item.KeyLang,
                        Nhom = item.Nhom,
                        IsCheck = item.IsCheck.Value,
                        STT = Utils.Utils.GetSoLaMa(icountHeader.ToString(), true)
                    });
                    var lst2s = lst.Where(z => z.Nhom == item.Nhom && z.GroupParent == null && z.GroupGuid != item.GroupGuid && z.IsHeader == false && z.GroupGuid != ConfigMenu).OrderBy(z => z.KeyLang).ToList();
                    int icount = 1;
                    foreach (var items in lst2s)
                    {
                        lstreturn.Add(new PhanQuyenByUserModels
                        {
                            GroupGuid = items.GroupGuid,
                            GroupName = items.GroupName,
                            GroupParent = items.GroupParent,
                            IsHeader = items.IsHeader.Value,
                            KeyLang = items.KeyLang,
                            Nhom = items.Nhom,
                            IsCheck = items.IsCheck.Value,
                            STT = icount.ToString()
                        });

                        var lst2ss = lst.Where(z => z.GroupParent == items.GroupGuid && z.IsHeader == false && z.GroupGuid != ConfigMenu).OrderBy(z => z.KeyLang).ToList();
                        foreach (var itemss in lst2ss)
                        {
                            lstreturn.Add(new PhanQuyenByUserModels
                            {
                                GroupGuid = itemss.GroupGuid,
                                GroupName = itemss.GroupName,
                                GroupParent = itemss.GroupParent,
                                IsHeader = itemss.IsHeader.Value,
                                KeyLang = itemss.KeyLang,
                                Nhom = itemss.Nhom,
                                IsCheck = itemss.IsCheck.Value,
                                STT = ""
                            });
                        }
                        icount++;
                    }
                    icountHeader++;
                }
            }
            catch (Exception ex)
            {

            }

            objReturn.Quyen = lstreturn;
            objReturn.NhanVienID = Guid.Parse(NhanVienID);
            objReturn.UserID = Guid.Parse(UserID);
            return PartialView("_ChiTietPhanQuyenUser", objReturn);
        }

        public ActionResult SearchGenPhanQuyenByNhanVien(string NhanVienID, string UserID, int LoaiHinh)
        {
            Guid ConfigMenu = Guid.Parse("97437315-3F68-4CAE-B3C5-6F93C31C0E65");
            unitofwork = new UnitOfWork();
            List<DF.DataAccess.DBML.Web_PhanQuyen_GetPhanQuyenByUserResult> lst = new List<DF.DataAccess.DBML.Web_PhanQuyen_GetPhanQuyenByUserResult>();
            List<DF.DataAccess.DBML.Web_PhanQuyen_GetPhanQuyenByUserResult> lst2 = new List<DF.DataAccess.DBML.Web_PhanQuyen_GetPhanQuyenByUserResult>();
            List<PhanQuyenByUserModels> lstreturn = new List<PhanQuyenByUserModels>();
            UserPhanQuyenModels objReturn = new UserPhanQuyenModels();
            try
            {
                lst = unitofwork.tblGroupUsers.GetPhanQuyenByUser(Guid.Parse(NhanVienID), LoaiHinh);
                lst2 = lst.Where(z => z.IsHeader == true).OrderBy(z => z.Nhom).ToList();
                int icountHeader = 1;
                if (LoaiHinh == 2)
                {
                    foreach (var item in lst2)
                    {
                        lstreturn.Add(new PhanQuyenByUserModels
                        {
                            GroupGuid = item.GroupGuid,
                            GroupName = item.GroupName,
                            GroupParent = item.GroupParent,
                            IsHeader = item.IsHeader.Value,
                            KeyLang = item.KeyLang,
                            Nhom = item.Nhom,
                            IsCheck = item.IsCheck.Value,
                            STT = Utils.Utils.GetSoLaMa(icountHeader.ToString(), true)
                        });
                        var lst2s = lst.Where(z => z.Nhom == item.Nhom && z.GroupParent == null && z.GroupGuid != item.GroupGuid && z.IsHeader == false && z.GroupGuid != ConfigMenu).OrderBy(z => z.KeyLang).ToList();
                        int icount = 1;
                        foreach (var items in lst2s)
                        {
                            lstreturn.Add(new PhanQuyenByUserModels
                            {
                                GroupGuid = items.GroupGuid,
                                GroupName = items.GroupName,
                                GroupParent = items.GroupParent,
                                IsHeader = items.IsHeader.Value,
                                KeyLang = items.KeyLang,
                                Nhom = items.Nhom,
                                IsCheck = items.IsCheck.Value,
                                STT = icount.ToString()
                            });

                            var lst2ss = lst.Where(z => z.GroupParent == items.GroupGuid && z.IsHeader == false && z.GroupGuid != ConfigMenu).OrderBy(z => z.KeyLang).ToList();
                            foreach (var itemss in lst2ss)
                            {
                                lstreturn.Add(new PhanQuyenByUserModels
                                {
                                    GroupGuid = itemss.GroupGuid,
                                    GroupName = itemss.GroupName,
                                    GroupParent = itemss.GroupParent,
                                    IsHeader = itemss.IsHeader.Value,
                                    KeyLang = itemss.KeyLang,
                                    Nhom = itemss.Nhom,
                                    IsCheck = itemss.IsCheck.Value,
                                    STT = ""
                                });
                            }
                            icount++;
                        }
                        icountHeader++;
                    }
                }
                else
                {
                    int icount = 1;
                    lst = lst.OrderBy(z => z.KeyLang).ToList();
                    foreach (var item in lst)
                    {
                        lstreturn.Add(new PhanQuyenByUserModels
                        {
                            GroupGuid = item.GroupGuid,
                            GroupName = item.GroupName,
                            GroupParent = item.GroupParent,
                            IsHeader = item.IsHeader.Value,
                            KeyLang = item.KeyLang,
                            Nhom = item.Nhom,
                            IsCheck = item.IsCheck.Value,
                            STT = icount.ToString()
                        });
                        icount++;
                    }
                }
            }
            catch (Exception ex)
            {

            }

            objReturn.Quyen = lstreturn;
            objReturn.NhanVienID = Guid.Parse(NhanVienID);
            objReturn.UserID = Guid.Parse(UserID);
            return PartialView("/Views/PhanQuyen/_BangChiTietPhanQyen.cshtml", objReturn);
        }

        public JsonResult PhanQuyenUser(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                AddPhanQuyen_User model = new JavaScriptSerializer().Deserialize<AddPhanQuyen_User>(data);
                using (TransactionScope tran = new TransactionScope())
                {
                    //Update hết thành trạng thái isactive = 0 chỉ những đội có type = 0
                    unitofwork.tblGroupUsers.delPhanQuyenNew(model.UserID.Value, model.LoaiHinh.Value);
                    foreach (var item in model.items)
                    {
                        tblGroupUser tb = new tblGroupUser();
                        tb.GroupGuid = item.GroupGuid.Value;
                        tb.UserGuid = model.UserID.Value;
                        unitofwork.tblGroupUsers.Add(tb);
                    }

                    unitofwork.Save();
                    tran.Complete();
                }
                return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhxulydulieu") }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Destroy_Quyen(string GroupGuid)
        {
            unitofwork = new UnitOfWork();
            try
            {
                unitofwork.tblGroupUsers.delQuyen(Guid.Parse(GroupGuid));
                return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhxulydulieu") }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
