﻿using DF.DataAccess;
using DF.DBMapping.Models;
using DF.DBMapping.ModelsExt;
using DFSCommon;
using DFSCommon.Models.Enums;
using DFWeb.Common;
using DFWeb.Models;
using DFWeb.Security;
using DFWeb.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Webservice.FCMModules;
using System.Configuration;
using System.Transactions;
using DF.DataAccess.DBML;
using FlexCel.Core;
using DFWeb.Models.ReportModels;
using FlexCel.XlsAdapter;
using DF.Utilities;
using Excel;
using DF.DBMapping.ModelsExt.Excel;
using FlexCel.Core;
using FlexCel.Report;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;

namespace DFWeb.Controllers.NghiepVu
{
    [CustomAuthorize(Roles = "400")]
    public class QuanLyVanBanDinhKemController : Controller
    {
        //
        // GET: /QuanLyVanBanDinhKem/
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        string status;
        string code;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAllData()
        {
            unitofwork = new UnitOfWork();
            try
            {
                var lst = unitofwork.VanBanDinhKems.GetAllData();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDatabyVanBanID(string VanBanID)
        {
            db = new DBMLDFDataContext();
            unitofwork = new UnitOfWork();
            Guid vbid = Guid.Empty;
            if(VanBanID != null || VanBanID != "")
            {
                vbid = Guid.Parse(VanBanID);
            }
            try
            {
                var dt = db.Web_VanBan_GetAllData().Where(t => t.VanBanID == vbid).FirstOrDefault();
                return Json(new { data = dt }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AddOrUpdate(string data)
        {
            db = new DBMLDFDataContext();
            unitofwork = new UnitOfWork();
            int result = unitofwork.VanBanDinhKems.AddOrUpdate(data, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (result == 0)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            else if (result == 1)
            {
                code = "warning";
                status = "Tên nguyên liệu hoặc mã nguyên liệu đã bị trùng!";
            }
            else
            {
                code = "success";
                status = "Thành công!";
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult XoaVanBan(string VanBanID)
        {
            unitofwork = new UnitOfWork();
            int result = unitofwork.VanBanDinhKems.XoaVanBan(VanBanID);
            if (result == 0)
            {
                code = "error";
                status = "Thêm mới không thành công! Có lỗi xảy ra";
            }
            else
            {
                code = "success";
                status = "Thành công!";
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachHinhAnhbyVanBanID(string VanBanID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var tp = Guid.Empty;
                if (VanBanID != "")
                {
                    tp = Guid.Parse(VanBanID);
                }
                var lst = unitofwork.Context.HinhAnhVanBans.Where(p => p.VanBanID == tp).Select(p => new lstImageVanBan
                {
                    img = p.FileMaHoa,
                    name = p.TenFile
                }).ToList();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
