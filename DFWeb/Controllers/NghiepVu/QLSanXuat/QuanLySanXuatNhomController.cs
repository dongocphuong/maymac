﻿using DF.DataAccess;
using DF.DBMapping.Models;
using DF.DBMapping.ModelsExt;
using DFSCommon;
using DFSCommon.Models.Enums;
using DFWeb.Common;
using DFWeb.Models;
using DFWeb.Security;
using DFWeb.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Webservice.FCMModules;
using System.Configuration;
using System.Transactions;
using DF.DataAccess.DBML;
using FlexCel.Core;
using DFWeb.Models.ReportModels;
using FlexCel.XlsAdapter;
using DF.Utilities;
using Excel;
using DF.DBMapping.ModelsExt.NghiepVu;
using DF.DBMapping.ModelsExt.Excel;
using FlexCel.Report;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu.VatTu;

namespace DFWeb.Controllers.NghiepVu.QLSanXuat
{
    public class QuanLySanXuatNhomController : Controller
    {
        //
        // GET: /QuanLySanXuatNhom/
        UnitOfWork unitofwork;
        DBMLDFDataContext db;
       [CustomAuthorize(Roles = "1009")]

        public ActionResult Index()
        {
            return View();
        }
       //Xuất excel ---------------------------------------------------------------------------------------
       public clsExcelResult XuatExcelTongNguyenLieu(Guid DonHangID)
       {
           string fileName = "";
           fileName = "Phiếu xuất kho nguyên liệu.xlsx";
           string urlTemp = "";
           urlTemp = "~\\TempExcel\\TemplateTongNguyenLieu.xlsx";
           clsExcelResult clsResult = new clsExcelResult();
           ExcelFile xls = CreateExcel(Server.MapPath(urlTemp), DonHangID);

           using (MemoryStream ms = new MemoryStream())
           {
               xls.Save(ms);
               ms.Position = 0;
               clsResult.ms = ms;
               clsResult.FileName = fileName;
               clsResult.type = "xlsx";
               return clsResult;
           }
       }
       public ExcelFile CreateExcel(String path, Guid DonHangID)
       {
           unitofwork = new UnitOfWork();
           db = new DBMLDFDataContext();
           XlsFile Result = new XlsFile(true);
           try
           {
               Result.Open(path);
               FlexCelReport fr = new FlexCelReport();
               unitofwork = new UnitOfWork();
               //
               var list = db.Web_NguyenLieu_DanhSachNguyenLieuDonHangCan(DonHangID).OrderBy(x => x.TenNguyenLieu).Select(x => new { 
                    TenNguyenLieu = x.TenNguyenLieu,
                    MaNguyenLieu = x.MaNguyenLieu,
                    TenDonVi = x.TenDonVi,
                    TongSoLuong = x.SoLuongCan,
                    SoLuongXuat = x.SoLuongXuat,
                    SoLuongConLai = x.SoLuongCan - x.SoLuongXuat
               }).ToList();
               //
               DataTable dtThongTinChung = Utils.Utilities.ToDataTable(list);
               dtThongTinChung.TableName = "ChiTiet";
               fr.AddTable("ChiTiet", dtThongTinChung);
               fr.Run(Result);
               fr.Dispose();
               dtThongTinChung.Dispose();
               return Result;
           }
           catch (Exception ex)
           {
               return Result;
           }
       }
       //-------------------------------------------------------------------------------------------------------------------
        public JsonResult LayDanhSach(string tungay="",string denngay="")
        {
            db = new DBMLDFDataContext();
            var TuNgay = DateTime.Now.AddMonths(-5).Date;
            var DenNgay = DateTime.Now.AddDays(1).Date;
            if (tungay != "")
            {
                TuNgay = DateTime.ParseExact(tungay, "dd/MM/yyyy", null);
            }
            if (denngay != "")
            {
                DenNgay = DateTime.ParseExact(denngay, "dd/MM/yyyy", null);
            }
            var lst = db.Web_SanXuatNhom_LayDanhSach(TuNgay, DenNgay).ToList();
            return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayLichSuSua(Guid DonHangID, Guid MauMayID)
        {
            db = new DBMLDFDataContext();
            var lst = db.Web_LichSuSua(DonHangID, MauMayID).ToList();
            return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
        }
        //Sản phẩm
        public ActionResult ViewSanXuatSanPham(string MauMayID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            var spid = Guid.Parse(MauMayID);
            var dhid = Guid.Parse(DonHangID);
            var dasanxuat = (from a in unitofwork.Context.QuanLySanXuats
                             join b in unitofwork.Context.ChiTietDonHangs on a.SanPhamID equals b.SanPhamID
                             where a.DonHangID == b.DonHangID && b.DonHangID == dhid
                             select a
                                 ).FirstOrDefault();
            if (dasanxuat == null)
            {
                return Redirect("/QuanLySanXuatNhom/ViewSanXuatSanPhamChuaBatDau?MauMayID=" + MauMayID + "&DonHangID=" + DonHangID);
            }
            else
            {
                return Redirect("/QuanLySanXuatNhom/ViewSanXuatSanPhamDaBatDau?MauMayID=" + MauMayID + "&DonHangID=" + DonHangID);
            }
        }

        public ActionResult ViewSanXuatSanPhamChuaBatDau(string MauMayID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var lst = unitofwork.QuanLySanXuatNhoms.SanXuatSanPhamChuaBatDau(MauMayID, DonHangID);
                return View(lst);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/QuanLySanXuatNhom/ChuaDinhNghiaCongThuc.cshtml");
            }
        }
        public ActionResult ViewSanXuatSanPhamDaBatDau(string MauMayID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var lst = unitofwork.QuanLySanXuatNhoms.SanXuatSanPhamDangSanXuat(MauMayID, DonHangID);
                return View(lst);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/QuanLySanXuatNhom/ChuaDinhNghiaCongThuc.cshtml");
            }
           
        }

        public JsonResult BatDauSanXuat(string MauMayID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuatNhoms.BatDauSanXuat(MauMayID, DonHangID, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
           
        }
        public JsonResult DoiTrangThaiCongDoan(Guid DonHangID,Guid MauMayID,int ViTri, int TrangThai, string LyDoTamDung="")
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuatNhoms.DoiTrangThaiCongDoan(DonHangID, MauMayID,ViTri, TrangThai, LyDoTamDung);
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult DoiTrangThaiCongDoanCuoi(Guid DonHangID,Guid MauMayID, int TrangThai, string LyDoTamDung="")
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuatNhoms.DoiTrangThaiCongDoanCuoi(DonHangID, MauMayID, TrangThai, LyDoTamDung);
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult SanXuatThanhPham(string DonHangID, string MauMayID, int ViTri, string datanhap, string dataxuat)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuatNhoms.SanXuatThanhPham(DonHangID, MauMayID, ViTri, datanhap, dataxuat, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SanXuatSanPham(string DonHangID, string MauMayID, string datanhap, string dataxuat)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuatNhoms.SanXuatThanhPhamCongDoan(DonHangID, MauMayID, datanhap, dataxuat, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SuaThanhPhamLoi(Guid DonHangID, Guid ThanhPhamID, double SoLuongSua)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuatNhoms.SuaThanhPhamLoi(DonHangID, ThanhPhamID, SoLuongSua, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SuaSanPhamLoi(Guid DonHangID, Guid SanPhamID, double SoLuongSua)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuatNhoms.SuaSanPhamLoi(DonHangID, SanPhamID, SoLuongSua, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult KiemTraTonNguyenLieu(string DonHangID, string MauMayID)
        {
            db = new DBMLDFDataContext();
            var lst = db.Web_NguyenLieu_TonKho_DonHangCanTheoSanPhamNhom(Guid.Parse(DonHangID), Guid.Parse(MauMayID)).Where(p => p.SoLuongCan.GetValueOrDefault() > p.SoLuongTon.GetValueOrDefault()).ToList();
            return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
        }
        //Thành phẩm
        public ActionResult ViewSanXuatThanhPham(string ThanhPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            var spid = Guid.Parse(ThanhPhamID);
            var dhid = Guid.Parse(DonHangID);
            var dasanxuat = unitofwork.Context.CongDoanThanhPhamDinhNghias.FirstOrDefault(p => p.DonHangID == dhid && spid == p.ThanhPhamID);
            if (dasanxuat == null)
            {
                return Redirect("/QuanLySanXuatNhom/ViewSanXuatThanhPhamChuaBatDau?ThanhPhamID=" + ThanhPhamID + "&DonHangID=" + DonHangID);
            }
            else
            {
                return Redirect("/QuanLySanXuatNhom/ViewSanXuatThanhPhamDaBatDau?ThanhPhamID=" + ThanhPhamID + "&DonHangID=" + DonHangID);
            }
        }
        public ActionResult ViewSanXuatThanhPhamChuaBatDau(string ThanhPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var lst = unitofwork.QuanLySanXuatNhoms.SanXuatThanhPhamChuaBatDau(ThanhPhamID, DonHangID);
                return View(lst);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/QuanLySanXuatNhom/ChuaDinhNghiaCongThuc.cshtml");
            }
        }
        public ActionResult ViewSanXuatThanhPhamDaBatDau(string ThanhPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var lst = unitofwork.QuanLySanXuatNhoms.SanXuatThanhPhamDangSanXuat(ThanhPhamID, DonHangID);
                return View(lst);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/QuanLySanXuatNhom/ChuaDinhNghiaCongThuc.cshtml");
            }
        }
        public JsonResult SanXuatThanhPham2(string DonHangID, string ThanhPhamID, int SoLuong, string dataxuat)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuatNhoms.SanXuatThanhPham2(DonHangID, ThanhPhamID, SoLuong, dataxuat, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult BatDauSanXuatThanhPham(string ThanhPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuatNhoms.BatDauSanXuatThanhPham(ThanhPhamID, DonHangID);
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult LayDanhSachSanPham(Guid MauMayID)
        {
            db = new DBMLDFDataContext();
            try
            {
                var lst = db.Web_NhomSanPham_LayDanhSachSanPham(MauMayID).ToList();
                var lstsize = lst.Select(p => new { p.Size, p.SanPhamID }).Distinct().OrderBy(p => p.Size).ToList();
                var lstthuoctinh = lst.Select(p => new { p.TenThuocTinh }).Distinct().OrderBy(p => p.TenThuocTinh).ToList();
                var tenthuoctinh = lst.Select(p => p.TenThuocTinh).Distinct().ToList();
                return Json(new { data = lst, sizes = lstsize, tenthuoctinh = tenthuoctinh, lstthuoctinh = lstthuoctinh, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult TaoDeNghi(string data)
        {
            unitofwork = new UnitOfWork();
            var model = new JavaScriptSerializer().Deserialize<DeNghiNguyenLieuModel>(data);
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            var rs = unitofwork.DeNghiNguyenLieus.TaoDeNghi(model, nhanvienid);
            return Json(new { code = rs.Split('_')[0], message = rs.Split('_')[1] });
        }
        public PartialViewResult TaoDeNghi_View()
        {
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            unitofwork = new UnitOfWork();
            return PartialView();
        }
        public JsonResult GetAllDataNguyenLieu(string DoiTacID)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var list = db.Web_NguyenLieu_LayDanhSach().ToList();
            return Json(new { data = list, total = list.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDataKhoTongBoTonKhongTheoDonHang(string DonHangID,string SanPhamID)
        {
            try
            {
                var db = new DBMLDFDataContext();
                var dhid = Guid.Empty;
                if (DonHangID != "")
                {
                    dhid = Guid.Parse(DonHangID);
                }
                var spid = Guid.Empty;
                if (SanPhamID != "")
                {
                    spid = Guid.Parse(SanPhamID);
                }
                var list = db.Web_NguyenLieu_TonKho_DonHangCanTheoSanPham(dhid, spid).ToList();
                return Json(new { data = list, total = list.Count, code = "succes" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InLenhSanXuat(string MauMayID, string DonHangID, int loai)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuatNhoms.InLenh(MauMayID, DonHangID);
            return View(lst);
        }
        public ActionResult InLenhSanXuat2(string SanPhamID, string DonHangID, int loai)
        {
            unitofwork = new UnitOfWork();
            if (loai == 1)
            {
                var lst = unitofwork.QuanLySanXuatNhoms.SanXuatSanPhamChuaBatDau(SanPhamID, DonHangID);
                return View(lst);
            }
            else
            {
                var lst = unitofwork.QuanLySanXuatNhoms.SanXuatSanPhamDangSanXuat2(SanPhamID, DonHangID);
                return View(lst);
            }

        }
    }
}
