﻿using DF.DataAccess;
using DF.DataAccess.DBML;
using DF.DBMapping.ModelsExt.NghiepVu;
using DF.DBMapping.ModelsExt.NghiepVu.VatTu;
using DFWeb.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace DFWeb.Controllers.NghiepVu.QLSanXuat
{
    public class QuanLySanXuatController : Controller
    {
        //
        // GET: /QuanLySanXuat/
        UnitOfWork unitofwork;
        DBMLDFDataContext db;
        [CustomAuthorize(Roles = "1009")]

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult LayDanhSach(string tungay="",string denngay="")
        {
            db = new DBMLDFDataContext();
            var TuNgay = DateTime.Now.AddMonths(-5).Date;
            var DenNgay = DateTime.Now.AddDays(1).Date;
            if (tungay != "")
            {
                TuNgay = DateTime.ParseExact(tungay, "dd/MM/yyyy", null);
            }
            if (denngay != "")
            {
                DenNgay = DateTime.ParseExact(denngay, "dd/MM/yyyy", null);
            }
            var lst = db.Web_SanXuat_LayDanhSach(TuNgay, DenNgay).ToList();
            return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
        }

        //Sản phẩm
        public ActionResult ViewSanXuatSanPham(string SanPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            var spid = Guid.Parse(SanPhamID);
            var dhid = Guid.Parse(DonHangID);
            var dasanxuat = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.DonHangID == dhid && spid == p.SanPhamID);
            if (dasanxuat == null)
            {
                return Redirect("/QuanLySanXuat/ViewSanXuatSanPhamChuaBatDau?SanPhamID=" + SanPhamID + "&DonHangID="+DonHangID);
            }
            else
            {
                return Redirect("/QuanLySanXuat/ViewSanXuatSanPhamDaBatDau?SanPhamID=" + SanPhamID + "&DonHangID=" + DonHangID);
            }
        }

        public ActionResult ViewSanXuatSanPhamChuaBatDau(string SanPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var lst = unitofwork.QuanLySanXuats.SanXuatSanPhamChuaBatDau(SanPhamID, DonHangID);
                return View(lst);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/QuanLySanXuat/ChuaDinhNghiaCongThuc.cshtml");
            }
        }
        public ActionResult ViewSanXuatSanPhamDaBatDau(string SanPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var lst = unitofwork.QuanLySanXuats.SanXuatSanPhamDangSanXuat(SanPhamID, DonHangID);
                return View(lst);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/QuanLySanXuat/ChuaDinhNghiaCongThuc.cshtml");
            }
           
        }
       
        public JsonResult BatDauSanXuat(string SanPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuats.BatDauSanXuat(SanPhamID, DonHangID, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
           
        }
        public JsonResult DoiTrangThaiCongDoan(string HangMucSanXuatID, int TrangThai, string LyDoTamDung="")
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuats.DoiTrangThaiCongDoan(HangMucSanXuatID, TrangThai, LyDoTamDung);
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult DoiTrangThaiCongDoanCuoi(string SanPhamID, string DonHangID, int TrangThai, string LyDoTamDung="")
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuats.DoiTrangThaiCongDoanCuoi(SanPhamID, DonHangID, TrangThai, LyDoTamDung);
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult SanXuatThanhPham(string DonHangID, string SanPhamID, string HangMucSanXuatID, string datanhap, string dataxuat)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuats.SanXuatThanhPham(DonHangID, SanPhamID, HangMucSanXuatID, datanhap, dataxuat, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult SanXuatSanPham(string DonHangID, string SanPhamID, int SoLuong, string dataxuat)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuats.SanXuatThanhPhamCongDoan(DonHangID, SanPhamID, SoLuong, dataxuat, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult KiemTraTonNguyenLieu(string DonHangID, string SanPhamID)
        {
            db = new DBMLDFDataContext();
            var lst = db.Web_NguyenLieu_TonKho_DonHangCanTheoSanPham(Guid.Parse(DonHangID), Guid.Parse(SanPhamID)).Where(p=>p.SoLuongCan>p.SoLuongTon).ToList();
            return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
        }
        //Thành phẩm
        public ActionResult ViewSanXuatThanhPham(string ThanhPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            var spid = Guid.Parse(ThanhPhamID);
            var dhid = Guid.Parse(DonHangID);
            var dasanxuat = unitofwork.Context.CongDoanThanhPhamDinhNghias.FirstOrDefault(p => p.DonHangID == dhid && spid == p.ThanhPhamID);
            if (dasanxuat == null)
            {
                return Redirect("/QuanLySanXuat/ViewSanXuatThanhPhamChuaBatDau?ThanhPhamID=" + ThanhPhamID + "&DonHangID=" + DonHangID);
            }
            else
            {
                return Redirect("/QuanLySanXuat/ViewSanXuatThanhPhamDaBatDau?ThanhPhamID=" + ThanhPhamID + "&DonHangID=" + DonHangID);
            }
        }
        public ActionResult ViewSanXuatThanhPhamChuaBatDau(string ThanhPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var lst = unitofwork.QuanLySanXuats.SanXuatThanhPhamChuaBatDau(ThanhPhamID, DonHangID);
                return View(lst);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/QuanLySanXuat/ChuaDinhNghiaCongThuc.cshtml");
            }
        }
        public ActionResult ViewSanXuatThanhPhamDaBatDau(string ThanhPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var lst = unitofwork.QuanLySanXuats.SanXuatThanhPhamDangSanXuat(ThanhPhamID, DonHangID);
                return View(lst);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/QuanLySanXuat/ChuaDinhNghiaCongThuc.cshtml");
            }
        }
        public JsonResult SanXuatThanhPham2(string DonHangID, string ThanhPhamID, int SoLuong, string dataxuat)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuats.SanXuatThanhPham2(DonHangID, ThanhPhamID, SoLuong, dataxuat, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult BatDauSanXuatThanhPham(string ThanhPhamID, string DonHangID)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.QuanLySanXuats.BatDauSanXuatThanhPham(ThanhPhamID, DonHangID);
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }

        }
        public JsonResult TaoDeNghi(string data)
        {
            unitofwork = new UnitOfWork();
            var model = new JavaScriptSerializer().Deserialize<DeNghiNguyenLieuModel>(data);
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            var rs = unitofwork.DeNghiNguyenLieus.TaoDeNghi(model, nhanvienid);
            return Json(new { code = rs.Split('_')[0], message = rs.Split('_')[1] });
        }
        public PartialViewResult TaoDeNghi_View()
        {
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            unitofwork = new UnitOfWork();
            return PartialView();
        }
        public JsonResult GetAllDataNguyenLieu(string DoiTacID)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var list = db.Web_NguyenLieu_LayDanhSach().ToList();
            return Json(new { data = list, total = list.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDataKhoTongBoTonKhongTheoDonHang(string DonHangID,string SanPhamID)
        {
            try
            {
                var db = new DBMLDFDataContext();
                var dhid = Guid.Empty;
                if (DonHangID != "")
                {
                    dhid = Guid.Parse(DonHangID);
                }
                var spid = Guid.Empty;
                if (SanPhamID != "")
                {
                    spid = Guid.Parse(SanPhamID);
                }
                var list = db.Web_NguyenLieu_TonKho_DonHangCanTheoSanPham(dhid, spid).ToList();
                return Json(new { data = list, total = list.Count, code = "succes" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult InLenhSanXuat(string SanPhamID, string DonHangID, int loai)
        {
            unitofwork = new UnitOfWork();
            if (loai == 1)
            {
                var lst = unitofwork.QuanLySanXuats.SanXuatSanPhamChuaBatDau(SanPhamID, DonHangID);
                return View(lst);
            }
            else
            {
                var lst = unitofwork.QuanLySanXuats.SanXuatSanPhamDangSanXuat2(SanPhamID, DonHangID);
                return View(lst);
            }
           
        }
        public ActionResult InLenhSanXuat2(string SanPhamID, string DonHangID, int loai)
        {
            unitofwork = new UnitOfWork();
            if (loai == 1)
            {
                var lst = unitofwork.QuanLySanXuats.SanXuatSanPhamChuaBatDau(SanPhamID, DonHangID);
                return View(lst);
            }
            else
            {
                var lst = unitofwork.QuanLySanXuats.SanXuatSanPhamDangSanXuat2(SanPhamID, DonHangID);
                return View(lst);
            }

        }
    }
}
