﻿using DF.DataAccess;
using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;
using DFWeb.Common;
using DFWeb.Security;
using System.Data.SqlClient;
using DFWeb.Models.ReportModels;
using FlexCel.Core;
using System.IO;
using FlexCel.XlsAdapter;
using FlexCel.Report;
using System.Transactions;
using DF.Utilities;
using DFWeb.Models;
using DF.DataAccess.DBML;
using DFWeb.Models.CongTrinhPhongBan;
using DF.DBMapping.ModelsExt.NghiepVu.QuanLyDoiTac;
using Excel;

namespace DFWeb.Controllers.QuanLyDanhMuc
{

    [CustomAuthorize(Roles = "1059")]
    public class QuanLyDoiTacController : Controller
    {
        DBMLDFDataContext db;
        protected UnitOfWork unitofwork;
        public ActionResult Index()
        {
            ViewBag.Title_Function = Models.Language.GetText("quanlydoitac");
            return View();
        }
        public JsonResult read(string TuNgay, string DenNgay)
        {
            DateTime tn = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            DateTime dn;
            try
            {
               dn = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null);
            }
            catch
            {
                dn = DateTime.Now;
            }
            db = new DBMLDFDataContext();
            var lst = new List<DoiTacModel>();
            foreach (var p in db.Web_DoiTac_LayDanhSachDoiTacTaiChinh(tn, dn).OrderBy(t => t.TenDoiTac).ToList()) 
            {
                var dtm = new DoiTacModel();
                dtm.DoiTacID = p.DoiTacID;
                dtm.KieuDoiTac = p.KieuDoiTac.GetValueOrDefault();
                dtm.TenDoiTac = p.TenDoiTac;
                dtm.MaDoiTac = p.MaDoiTac;
                dtm.TongThu = p.BanSanPham.GetValueOrDefault() + p.BanThanhPham.GetValueOrDefault();
                dtm.TongChi = p.MuaNguyenLieu.GetValueOrDefault() + p.MuaThanhPham.GetValueOrDefault() + p.MuaSanPham.GetValueOrDefault();
                dtm.ThanhToanCongNo = p.ThanhToanCongNo.GetValueOrDefault();
                if(dtm.TongThu == 0)
                {
                    dtm.ChenhLenh = dtm.TongChi;
                    if (p.LoaiHinhThanhToan == 1)
                    {
                        dtm.ChenhLenh = dtm.TongChi + dtm.ThanhToanCongNo;
                    }
                    else
                    {
                        dtm.ChenhLenh = dtm.TongChi - dtm.ThanhToanCongNo;
                    }
                }
                else if(dtm.TongChi == 0)
                {
                    dtm.ChenhLenh = dtm.TongThu;
                    if (p.LoaiHinhThanhToan == 1)
                    {
                        dtm.ChenhLenh = dtm.TongThu + dtm.ThanhToanCongNo;
                    }
                    else
                    {
                        dtm.ChenhLenh = dtm.TongThu - dtm.ThanhToanCongNo;
                    }
                }
                else
                {
                    if (p.LoaiHinhThanhToan == 1)
                    {
                        dtm.ChenhLenh = (dtm.TongThu - dtm.TongChi) + dtm.ThanhToanCongNo;
                    }
                    else
                    {
                        dtm.ChenhLenh = (dtm.TongThu - dtm.TongChi) - dtm.ThanhToanCongNo;
                    }
                }
                lst.Add(dtm);
            }
            return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult create(string data)
        {
            string message = "";
            DoiTac doithicong = new DoiTac();
            Guid DoiThiCongID = Guid.NewGuid();
            unitofwork = new UnitOfWork();
            try
            {
                DoiThiCongNewsModel model = new JavaScriptSerializer().Deserialize<DoiThiCongNewsModel>(data);
                var temp = unitofwork.Context.DoiTacs.Find(model.DoiTacID); //tim ra đối tượng đó từ cái id của  obj
                using (TransactionScope tran = new TransactionScope())
                {
                    if (temp == null)//Thêm
                    {
                        doithicong.DoiTacID = DoiThiCongID;
                        doithicong.IsActive = true;
                        doithicong.TenDoiTac = model.TenDoiTac;
                        var tendoitac = Functions.convertToUnSign2(model.TenDoiTac);



                        string res = "";
                        string[] tu = tendoitac.Split(' ').Take(3).ToArray();
                        int len = tu.Length;
                        for (int i = 0; i <= len - 1; i++)
                        {
                            res += tu[i].Substring(0, 1);
                        }
                        var count = unitofwork.Context.DoiTacs.Count(p => p.MaDoiTac.Substring(0, 3).ToUpper() == res);
                        res += count > 0 ? (count + 1).ToString() : "1";
                        doithicong.MaDoiTac = res;


                        doithicong.SDT = model.SDT;
                        doithicong.DiaChi = model.DiaChi;
                        doithicong.KieuDoiTac = model.KieuDoiTac;
                        doithicong.Email = model.Email;
                        doithicong.MaSoThue = model.MaSoThue;
                        unitofwork.Context.DoiTacs.Add(doithicong);
                        //
                        //NhanVien nv = new NhanVien();
                        //nv.NhanVienID = Guid.NewGuid();
                        //nv.TenNhanVien = model.TenDoiTac;
                        //nv.NgaySinh = DateTime.Now;
                        //nv.GioiTinh = 1;
                        //nv.SDT = model.SDT;
                        //nv.CMT = "";
                        //nv.DiaChi = model.DiaChi;
                        //nv.IsActive = true;
                        //nv.TinhTrang = 1;
                        //nv.AnhDaiDien = "";
                        //nv.DoiTacID = doithicong.DoiTacID;
                        //unitofwork.NhanViens.Add(nv);
                        //
                        message = Models.Language.GetText("msg_themdoitacthanhcong");
                    }
                    else//Cập nhật
                    {
                        temp.TenDoiTac = model.TenDoiTac;
                        //temp.Email = model.Email;
                        temp.SDT = model.SDT;
                        temp.DiaChi = model.DiaChi;
                        temp.Email = model.Email;
                        temp.MaSoThue = model.MaSoThue;
                        temp.KieuDoiTac = model.KieuDoiTac;
                        message = Models.Language.GetText("mgs_capnhatdoitacthanhcong");
                    }

                    unitofwork.Save();
                    tran.Complete();
                    return Json(new { code = "success", message = message });
                }
            }
            catch (Exception ex) { return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhxulydulieu") }); }
        }
        public JsonResult delDoiThiCong(string data)
        {
            unitofwork = new UnitOfWork();
            DoiTac doithicong = new DoiTac();
            try
            {

                DoiThiCongNewsModel model = new JavaScriptSerializer().Deserialize<DoiThiCongNewsModel>(data);
                var temp = unitofwork.Context.DoiTacs.Find(model.DoiTacID); //tim ra đối tượng đó từ cái id của  obj
                using (TransactionScope tran = new TransactionScope())
                {

                    temp.IsActive = false;
                    unitofwork.Save();
                    tran.Complete();
                    return Json(new { code = "success", message = Models.Language.GetText("mgs_xoathanhcongdoitac") }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("mgs_coloitrongquatrinhxoabanghi") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayChiTietTaiChinh(Guid DoiThiCongID, int? Loai = 1, string TuNgay = "", string DenNgay = "")
        {
            db = new DBMLDFDataContext();
            var lst = new List<TaiChinhDoiTacMoidel>();

            lst.AddRange(db.Web_DoiTac_LayDanhSachTaiChinhMuaNguyenLieu(DoiThiCongID).GroupBy(l => l.MaDeNghi).Select(p => new TaiChinhDoiTacMoidel
            {
                LoaiHinh = p.FirstOrDefault().LoaiHinh,
                Chi = p.Sum(c => c.SoTien.GetValueOrDefault()),
                NguoiTao = p.FirstOrDefault().TenNhanVien,
                ThoiGian = p.FirstOrDefault().ThoiGian,
                Thu = 0,
                NoiDung = p.FirstOrDefault().MaDeNghi,
                ChenhLenh = 0 - p.Sum(c => c.SoTien.GetValueOrDefault()),
            }));
            lst.AddRange(db.Web_DoiTac_LayDanhSachTaiChinhTraNguyenLieu(DoiThiCongID).GroupBy(l => l.MaDeNghi).Select(p => new TaiChinhDoiTacMoidel
            {
                LoaiHinh = p.FirstOrDefault().LoaiHinh,
                Chi = 0,
                NguoiTao = p.FirstOrDefault().TenNhanVien,
                ThoiGian = p.FirstOrDefault().ThoiGian,
                Thu = p.Sum(c => c.SoTien.GetValueOrDefault()),
                NoiDung = p.FirstOrDefault().MaDeNghi,
                ChenhLenh = p.Sum(c => c.SoTien.GetValueOrDefault()),
            }));
            lst.AddRange(db.Web_DoiTac_LayDanhSachTaiChinhMuaSanPham(DoiThiCongID).GroupBy(l => l.MaDeNghi).Select(p => new TaiChinhDoiTacMoidel
            {
                LoaiHinh = p.FirstOrDefault().LoaiHinh,
                Chi = p.Sum(c => c.SoTien.GetValueOrDefault()),
                ChenhLenh = 0 - p.Sum(c => c.SoTien.GetValueOrDefault()),
                NguoiTao = p.FirstOrDefault().TenNhanVien,
                ThoiGian = p.FirstOrDefault().ThoiGian,
                Thu = 0,
                NoiDung = p.FirstOrDefault().MaDeNghi
            }));
            lst.AddRange(db.Web_DoiTac_LayDanhSachTaiChinhMuaThanhPham(DoiThiCongID).GroupBy(l => l.MaDeNghi).Select(p => new TaiChinhDoiTacMoidel
            {
                LoaiHinh = p.FirstOrDefault().LoaiHinh,
                Chi = p.Sum(c => c.SoTien),
                ChenhLenh = 0 - p.Sum(c => c.SoTien),
                NguoiTao = p.FirstOrDefault().TenNhanVien,
                ThoiGian = p.FirstOrDefault().ThoiGian,
                Thu = 0,
                NoiDung = p.FirstOrDefault().MaDeNghi
            }));
            lst.AddRange(db.Web_DoiTac_LayDanhSachTaiChinhBanSanPham(DoiThiCongID).GroupBy(l => l.MaDeNghi).Select(p => new TaiChinhDoiTacMoidel
            {
                LoaiHinh = p.FirstOrDefault().LoaiHinh,
                Chi = 0,
                ChenhLenh = p.Sum(c => c.SoTien),
                ThoiGian = p.FirstOrDefault().ThoiGian,
                Thu = p.Sum(c => c.SoTien),
                NoiDung = p.FirstOrDefault().MaDeNghi
            }));
            lst.AddRange(db.Web_DoiTac_LayDanhSachTaiChinhBanThanhPham(DoiThiCongID).GroupBy(l => l.MaDeNghi).Select(p => new TaiChinhDoiTacMoidel
            {
                LoaiHinh = p.FirstOrDefault().LoaiHinh,
                Chi = 0,
                ChenhLenh = p.Sum(c => c.SoTien),
                ThoiGian = p.FirstOrDefault().ThoiGian,
                Thu = p.Sum(c => c.SoTien),
                NoiDung = p.FirstOrDefault().MaDeNghi
            }));
            if (Loai == 2)
            {
                var tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
                var denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
                lst = lst.Where(p => p.ThoiGian >= tuNgay && p.ThoiGian < denNgay).ToList();
            }
            return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayChiTietDoiTac(Guid DoiThiCongID)
        {
            unitofwork = new UnitOfWork();
            DoiThiCongNewsModel md = new DoiThiCongNewsModel();
            try
            {
                var doiTac = unitofwork.Context.DoiTacs.Find(DoiThiCongID);
                md.DiaChi = doiTac.DiaChi;
                md.DoiTacID = doiTac.DoiTacID;
                md.Email = doiTac.Email;
                md.MaSoThue = doiTac.MaSoThue;
                md.MaDoiTac = doiTac.MaDoiTac;
                md.SDT = doiTac.SDT;
                md.TenDoiTac = doiTac.TenDoiTac;
                md.KieuDoiTac = doiTac.KieuDoiTac.GetValueOrDefault();
                return Json(new { data = md }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { data = md }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult XoaDoiTac(string DoiThiCongID)
        {
            unitofwork = new UnitOfWork();
            Guid dtcid = Guid.Parse(DoiThiCongID);
            try
            {
                var temp = unitofwork.Context.DoiTacs.Find(dtcid);
                temp.IsActive = false;
                unitofwork.Context.SaveChanges();
                return Json(new { code = "success", message = Models.Language.GetText("mgs_thanhcong") });
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("mgs_coloi") });
            }
        }
        public JsonResult LayDanhSachCongNoTheoDoiTac(string DoiTacID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            Guid dtid;
            if (DoiTacID == null || DoiTacID == "")
            {
                dtid = Guid.Empty;
            }
            else
            {
                dtid = Guid.Parse(DoiTacID);
            }
            var dt = db.Web_ThanhToan_LayDanhSachCongNoTheoDoiTac(dtid).OrderBy(t => t.ThoiGian).ToList();
            return Json(new { data = dt, total = dt.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddOrUpdateTTCN(string data)
        {
            unitofwork = new UnitOfWork();
            ThanhToanTienModels model = new JavaScriptSerializer().Deserialize<ThanhToanTienModels>(data);
            string message = "";
            try
            {
                if (model.ThanhToanTienID == null || model.ThanhToanTienID == Guid.Empty)//Thêm
                {
                    ThanhToanTien tt = new ThanhToanTien();
                    tt.ThanhToanTienID = Guid.NewGuid();
                    tt.ThoiGian = DateTime.ParseExact(model.ThoiGian, "dd/MM/yyyy", null);
                    tt.NoiDung = model.NoiDung;
                    tt.NhanVienID = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
                    tt.SoTien = model.SoTien;
                    tt.DoiTacID = model.DoiTacID;
                    tt.LoaiHinh = model.LoaiHinh;
                    unitofwork.ThanhToanTiens.Add(tt);
                    //
                    message = "Thêm mới thành công";
                }
                else//Cập nhật
                {
                    var temp = unitofwork.Context.ThanhToanTiens.Find(model.ThanhToanTienID);
                    temp.ThoiGian = DateTime.ParseExact(model.ThoiGian, "dd/MM/yyyy", null);
                    temp.NoiDung = model.NoiDung;
                    temp.SoTien = model.SoTien;
                    temp.LoaiHinh = model.LoaiHinh;
                    message = "Cập nhật thành công";
                }
                unitofwork.Save();
                return Json(new { code = "success", message = message });
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = "Không thành công có lỗi xảy ra" });
            }
        }
        public JsonResult LayThongTinChiTietCongNo(string ThanhToanTienID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            Guid tttid;
            if (ThanhToanTienID == null || ThanhToanTienID == "")
            {
                tttid = Guid.Empty;
            }
            else
            {
                tttid = Guid.Parse(ThanhToanTienID);
            }
            var dt = db.Web_ThanhToan_LayThongTinChiTiet(tttid);
            return Json(new { data = dt }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult XoaChiTietCongNo(string ThanhToanTienID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            Guid tttid = Guid.Parse(ThanhToanTienID);
            try
            {
                db.Web_ThanhToan_XoaChiTietThanhToan(tttid);
                return Json(new { code = "success", message = "Xóa thành công" });
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = "Có lỗi chưa xóa được" });
            }
        }
        //import
        public clsExcelResult XuatTemplate()
        {
            string fileName = "";
            fileName = "Template.xlsx";
            string urlTemp = "";
            if (Language.GetKeyLang() == "cn")
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/DoiTacCN.xlsx");
            }
            else
            {
                urlTemp = Server.MapPath("~/TempExcel/Import/DoiTac.xlsx");
            }
            clsExcelResult clsResult = new clsExcelResult();
            var temp = new XlsFile(true);
            temp.Open(urlTemp);
            ExcelFile xls = temp;
            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public JsonResult CheckKetQua()
        {
            string iUploadedCnt = "";
            string sPath = "";
            sPath = Server.MapPath("~/TempExcel/Import/TempFile/");
            unitofwork = new UnitOfWork();
            System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;
            string autoky = ConvertDate.autokey();
            // CHECK THE FILE COUNT.
            for (int iCnt = 0; iCnt <= hfc.Count - 1; iCnt++)
            {
                System.Web.HttpPostedFile hpf = hfc[iCnt];
                if (hpf.ContentLength > 0)
                {
                    Stream stream = hpf.InputStream;
                    IExcelDataReader reader = null;
                    if (hpf.FileName.EndsWith(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                    }
                    else if (hpf.FileName.EndsWith(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    }
                    else
                    {

                    }
                    reader.IsFirstRowAsColumnNames = true;
                    DataSet result = reader.AsDataSet();
                    var listct = result.Tables[0].AsEnumerable().Select(p => new
                    {
                        TenDoiTac = p[0].ToString(),
                        Sdt = p[1].ToString(),
                        Email = p[2].ToString(),
                        DiaChi = p[3].ToString(),
                    });
                    reader.Close();
                    return Json(new { data = listct }, JsonRequestBehavior.AllowGet);
                }
            }
            var message = "";
            return Json(new { url = iUploadedCnt, message = message }, JsonRequestBehavior.AllowGet);


        }
        public JsonResult ImportDoiTac(string data)
        {
            unitofwork = new UnitOfWork();
            dynamic model = new JavaScriptSerializer().Deserialize<dynamic>(data);
            for (int i = 0; i < model.Length; i++)
            {
                DoiTac vt = new DoiTac();
                vt.DoiTacID = Guid.NewGuid();
                vt.TenDoiTac = model[i]["TenDoiTac"];
                //vt.Email = model[i]["Email"];
                vt.DiaChi = model[i]["DiaChi"];
                vt.SDT = model[i]["Sdt"];
                vt.IsActive = true;
                unitofwork.Context.DoiTacs.Add(vt);
                unitofwork.Save();
            }
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }
    }
}
