﻿using DF.DataAccess;
using DF.DataAccess.DBML;
using DF.DBMapping.ModelsExt;
using DFWeb.Models.ReportModels;
using DFWeb.Security;
using FlexCel.Core;
using FlexCel.Report;
using FlexCel.XlsAdapter;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DFWeb.Controllers.NghiepVu.NguyenLieu
{
    [CustomAuthorize(Roles = "2000")]
    public class KhoNguyenLieuDangSanXuatController : Controller
    {
        //
        // GET: /KhoNguyenLieuDangSanXuat/
 
        UnitOfWork unitofwork;
        DBMLDFDataContext db;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetDataTonKho(string DonHangID="")
        {
            try
            {
                unitofwork = new UnitOfWork();
                db = new DBMLDFDataContext();
                var donHangID = Guid.Empty.ToString();
                if (DonHangID != "")
                {
                    donHangID = DonHangID;
                }
                var list = db.Web_NguyenLieu_DanhSachNguyenLieuDonHangCan(Guid.Parse(donHangID)).ToList();
                return Json(new { data = list, total = list.Count, code = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAllHoaDon()
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var list = db.Web_DonHang_LayDanhSachDonHangChuaHoanThien(Guid.Parse(Define.CONGTRINHTONG));
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }
        //Xuat Excel 
        public clsExcelResult ExportToExcel(string DonHangID = "")
        {
            string fileName = "";
            fileName = "KhoNguyenLieuDangSanXuat.xlsx";
            string urlTemp = "";
            urlTemp = "~\\TempExcel\\VatTu\\KhoNguyenLieuDangSanXuat.xlsx";
            clsExcelResult clsResult = new clsExcelResult();
            ExcelFile xls = CreateReport(Server.MapPath(urlTemp), DonHangID);
            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public ExcelFile CreateReport(String path, string DonHangID = "")
        {

            XlsFile Result = new XlsFile(true);
            try
            {
                Result.Open(path);
                FlexCelReport fr = new FlexCelReport();
                unitofwork = new UnitOfWork();
                var donHangID = Guid.Empty.ToString();
                if (DonHangID != "")
                {
                    donHangID = DonHangID;
                }
                var list = unitofwork.KhoNguyenLieus.LayTon(donHangID).ToList();
                DataTable dtCCDC = Utils.Utilities.ToDataTable(list);
                dtCCDC.TableName = "ChiTiet";
                fr.AddTable("ChiTiet", dtCCDC);
                fr.Run(Result);
                fr.Dispose();
                dtCCDC.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
        public JsonResult GetDataLichSuNhap(string DonHangID, string TuNgay = "", string DenNgay = "")
        {
            try
            {
                unitofwork = new UnitOfWork();
                Guid dhid;
                try
                {
                    dhid = Guid.Parse(DonHangID);
                }
                catch
                {
                    dhid = Guid.Empty;
                }
                var data = unitofwork.KhoNguyenLieus.LichSuNhapKhoTheoDonHang(dhid, TuNgay, DenNgay);
                return Json(new { data = data, total = data.Count, code = "success", message = Models.Language.GetText("mgs_laydulieuthanhcong") }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("laydulieukhongthanhcong") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDataLichSuXuat(string DonHangID, string TuNgay = "", string DenNgay = "")
        {
            try
            {
                unitofwork = new UnitOfWork();
                var data = unitofwork.KhoNguyenLieus.LichSuXuatKhoTheoDonHang(Guid.Parse(Define.CONGTRINHTONG), DonHangID, TuNgay, DenNgay);
                return Json(new { data = data, total = data.Count, code = "success", message = Models.Language.GetText("mgs_laydulieuthanhcong") }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("laydulieukhongthanhcong") }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
