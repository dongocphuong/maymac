﻿using DF.DataAccess;
using DF.DataAccess.DBML;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.Excel;
using DF.DBMapping.ModelsExt.NghiepVu.VatTu;
using DFWeb.Models.ReportModels;
using DFWeb.Security;
using FlexCel.Core;
using FlexCel.Report;
using FlexCel.XlsAdapter;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace DFWeb.Controllers.NghiepVu.QLNguyenLieu
{
    [CustomAuthorize(Roles = "8501")]
    public class DeNghiNguyenLieuController : Controller
    {
        //
        // GET: /DeNghiNguyenLieu/
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        public ActionResult Index()
        {
            return View();
        }
        #region Json
        //Lấy danh sách đề nghị vật tư
        public JsonResult LayDanhSachDeNghiNguyenLieu(string DonHangID = "", string SanPhamID = "", string TuNgay = "", string DenNgay = "", int TrangThai = 1000)
        {
            unitofwork = new UnitOfWork();
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            Guid ctid = Guid.Empty;
            Guid dtcid = Guid.Empty;
            DateTime tungay = DateTime.Now.AddYears(-5);
            DateTime denngay = DateTime.Now;
            try { ctid = Guid.Parse(DonHangID); } catch { }
            try { dtcid = Guid.Parse(SanPhamID); }
            catch { }
            try { tungay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null); }
            catch { }
            try { denngay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).Date.AddHours(23).AddMinutes(59).AddSeconds(59); }
            catch { }
            var list = unitofwork.DeNghiNguyenLieus.LayDanhSachDeNghiNguyenLieu(nhanvienid, ctid, dtcid, tungay, denngay, TrangThai);
            return Json(new { data = list, total = list.Count }, JsonRequestBehavior.AllowGet);
        }
        //
        //Lấy danh sách đề nghị vật tư
        public JsonResult GetAllDataNguyenLieu(string DoiTacID)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var list = db.Web_NguyenLieu_LayDanhSach().ToList();
            return Json(new { data = list, total = list.Count }, JsonRequestBehavior.AllowGet);
        }
        //
        //lấy danh sách Dự Án by đội
       
        public JsonResult LayDanhSachDonHangByNhanVien()
        {
            var tong = Guid.Parse(Define.CONGTRINHTONG);
            DBMLDFDataContext db = new DBMLDFDataContext();
            var list = db.Web_DonHang_LayDanhSachDonHangChuaHoanThien(Guid.Parse(Define.CONGTRINHTONG));
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LayDanhSachSanPhamByNhanVien(string DonHangID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                var ctid = Guid.Parse(DonHangID);
                var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
                var lst = (from a in unitofwork.Context.ChiTietDonHangs
                           join b in unitofwork.Context.SanPhams on a.SanPhamID equals b.SanPhamID
                           where a.DonHangID == ctid
                           select b).Distinct().Select(p => new {
                               TextHienThi = p.MaSanPham + p.TenSanPham,
                               p.MaSanPham,
                               p.TenSanPham,
                               p.AnhDaiDien,
                               p.SanPhamID
                           }).ToList();
                return Json(new { data = lst }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

       
        // Lấy danh danh sách vật tư được đề nghị
        public JsonResult LayDanhSachNguyenLieuDuocDeNghi(Guid DeNghiNguyenLieuID)
        {
            unitofwork = new UnitOfWork();
            var list = unitofwork.DeNghiNguyenLieus.LayDanhSachNguyenLieuDuocDeNghi(DeNghiNguyenLieuID);
            return Json(new { data = list, total = list.Count }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LayDanhSachDuyetDeNghiNguyenLieu(Guid DeNghiNguyenLieuID)
        {
            unitofwork = new UnitOfWork();
            var list = unitofwork.DeNghiNguyenLieus.LayDanhSachDuyetDeNghiNguyenLieu(DeNghiNguyenLieuID);
            return Json(new { data = list, total = list.Count }, JsonRequestBehavior.AllowGet);
        }
       
        public JsonResult LayDanhSachNguyenLieuDaXuat(Guid DeNghiNguyenLieuID)
        {
            unitofwork = new UnitOfWork();
            var list = unitofwork.DeNghiNguyenLieus.LayDanhSachNguyenLieuDaXuat(DeNghiNguyenLieuID);
            return Json(new { data = list, total = list.Count }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LayDanhSachNguyenLieuConDeXuat(Guid DeNghiNguyenLieuID)
        {
            unitofwork = new UnitOfWork();
            var list = unitofwork.DeNghiNguyenLieus.LayDanhSachDuyetDeNghiNguyenLieu(DeNghiNguyenLieuID);
            var list2 = unitofwork.DeNghiNguyenLieus.LayDanhSachNguyenLieuDaXuat(DeNghiNguyenLieuID);
            var lst2 = list.Where(p => (p.SoLuongDuyet.GetValueOrDefault() - list2.Where(p2 => p2.NguyenLieuID == p.NguyenLieuID).Sum(p2 => p2.SoLuong)).GetValueOrDefault() > 0).Select(p => new { 
            p.HeSoQuyDoi,
            p.IsActive,
            p.MaNguyenLieu,
            p.NguyenLieuID,
            SoLuongDuyet=(p.SoLuongDuyet.GetValueOrDefault() - list2.Where(p2 => p2.NguyenLieuID == p.NguyenLieuID).Sum(p2 => p2.SoLuong)).GetValueOrDefault(),
            p.SoLuongTon,
            p.SoLuongXuat,
            p.TenDonVi,
            p.TenNguyenLieu,
            p.AnhDaiDien,
            p.DanhSachHinhAnhs,
            p.DonGia,
            p.DonHangID,
            p.DonViID,
            }).ToList();
            return Json(new { data = lst2, total = lst2.Count }, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region View

        public ActionResult XemDeNghi_View(Guid DeNghiNguyenLieuID)
        {
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            unitofwork = new UnitOfWork();
            var denghiDonHang = unitofwork.Context.DeNghiNguyenLieux.FirstOrDefault(p => p.DeNghiNguyenLieuID == DeNghiNguyenLieuID);
            if (denghiDonHang.TrangThai == 1)
            {
                return Redirect("/DeNghiNguyenLieu/DuyetDeNghi_View?DeNghiNguyenLieuID=" + denghiDonHang.DeNghiNguyenLieuID);
            }
            else if (denghiDonHang.TrangThai == 2)
            {
                return Redirect("/DeNghiNguyenLieu/XuatNguyenLieu_View?DeNghiNguyenLieuID=" + denghiDonHang.DeNghiNguyenLieuID);
            }
            else if (denghiDonHang.TrangThai == 3 || denghiDonHang.TrangThai == 5)
            {
                return Redirect("/DeNghiNguyenLieu/XuatNguyenLieu_View2?DeNghiNguyenLieuID=" + denghiDonHang.DeNghiNguyenLieuID);
            }
            else
            {
                return Redirect("/DeNghiNguyenLieu/TuChoi_View?DeNghiNguyenLieuID=" + denghiDonHang.DeNghiNguyenLieuID);
            }

        }

        public PartialViewResult TaoDeNghi_View()
        {
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            unitofwork = new UnitOfWork();
            return PartialView();
        }

        public PartialViewResult DuyetDeNghi_View(Guid DeNghiNguyenLieuID)
        {
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            unitofwork = new UnitOfWork();
            var model = (from a in unitofwork.Context.DeNghiNguyenLieux
                         join b in unitofwork.Context.DonHangs on a.DonHangID equals b.DonHangID
                         join c in unitofwork.Context.SanPhams on a.SanPhamID equals c.SanPhamID
                         join d in unitofwork.Context.NhanViens on a.NhanVienID equals d.NhanVienID
                         join e in unitofwork.Context.DeNghiNguyenLieux on a.DeNghiNguyenLieuID equals e.DeNghiNguyenLieuID
                         where a.DeNghiNguyenLieuID == DeNghiNguyenLieuID
                         select new ThongTinPhieuDeNghi
                         {
                             SanPhamID = a.SanPhamID,
                             DonHangID = a.DonHangID,
                             DeNghiNguyenLieuID = a.DeNghiNguyenLieuID,
                             MaDeNghi = a.MaDeNghi,
                             NoiDung = e.NoiDung,
                             TenDonHang = b.TenDonHang,
                             TenSanPham = c.TenSanPham,
                             MaDonHang = b.MaDonHang,
                             MaSanPham = c.MaSanPham,
                             TenNhanVien = d.TenNhanVien,
                             ThoiGian = a.ThoiGian,
                             LichSuDeNghi = a.LichSuDeNghis
                         }).FirstOrDefault();
            return PartialView(model);
        }

        public PartialViewResult XuatNguyenLieu_View(Guid DeNghiNguyenLieuID)
        {
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            unitofwork = new UnitOfWork();
            var model = (from a in unitofwork.Context.DeNghiNguyenLieux
                         join b in unitofwork.Context.DonHangs on a.DonHangID equals b.DonHangID
                         join c in unitofwork.Context.SanPhams on a.SanPhamID equals c.SanPhamID
                         join d in unitofwork.Context.NhanViens on a.NhanVienID equals d.NhanVienID
                         join e in unitofwork.Context.DeNghiNguyenLieux on a.DeNghiNguyenLieuID equals e.DeNghiNguyenLieuID
                         where a.DeNghiNguyenLieuID == DeNghiNguyenLieuID
                         select new ThongTinPhieuDeNghi
                         {
                             SanPhamID = a.SanPhamID,
                             DonHangID = a.DonHangID,
                             DeNghiNguyenLieuID = a.DeNghiNguyenLieuID,
                             MaDeNghi = a.MaDeNghi,
                             NoiDung = e.NoiDung,
                             TenDonHang = b.TenDonHang,
                             TenSanPham = c.TenSanPham,
                             MaDonHang = b.MaDonHang,
                             MaSanPham = c.MaSanPham,
                             TenNhanVien = d.TenNhanVien,
                             ThoiGian = a.ThoiGian,
                             LichSuDeNghi = a.LichSuDeNghis
                         }).FirstOrDefault();
            return PartialView(model);
        }
        public PartialViewResult XuatNguyenLieu_View2(Guid DeNghiNguyenLieuID)
        {
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            unitofwork = new UnitOfWork();
            var model = (from a in unitofwork.Context.DeNghiNguyenLieux
                         join b in unitofwork.Context.DonHangs on a.DonHangID equals b.DonHangID
                         join c in unitofwork.Context.SanPhams on a.SanPhamID equals c.SanPhamID
                         join d in unitofwork.Context.NhanViens on a.NhanVienID equals d.NhanVienID
                         join e in unitofwork.Context.DeNghiNguyenLieux on a.DeNghiNguyenLieuID equals e.DeNghiNguyenLieuID
                         where a.DeNghiNguyenLieuID == DeNghiNguyenLieuID
                         select new ThongTinPhieuDeNghi
                         {
                             SanPhamID = a.SanPhamID,
                             DonHangID = a.DonHangID,
                             DeNghiNguyenLieuID = a.DeNghiNguyenLieuID,
                             MaDeNghi = a.MaDeNghi,
                             NoiDung = e.NoiDung,
                             TenDonHang = b.TenDonHang,
                             TenSanPham = c.TenSanPham,
                             MaDonHang = b.MaDonHang,
                             MaSanPham = c.MaSanPham,
                             TenNhanVien = d.TenNhanVien,
                             ThoiGian = a.ThoiGian,
                             LichSuDeNghi = a.LichSuDeNghis
                         }).FirstOrDefault();
            return PartialView(model);
        }
        public PartialViewResult TuChoi_View(Guid DeNghiNguyenLieuID)
        {
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            unitofwork = new UnitOfWork();
            var model = (from a in unitofwork.Context.DeNghiNguyenLieux
                         join b in unitofwork.Context.DonHangs on a.DonHangID equals b.DonHangID
                         join c in unitofwork.Context.SanPhams on a.SanPhamID equals c.SanPhamID
                         join d in unitofwork.Context.NhanViens on a.NhanVienID equals d.NhanVienID
                         join e in unitofwork.Context.DeNghiNguyenLieux on a.DeNghiNguyenLieuID equals e.DeNghiNguyenLieuID
                         where a.DeNghiNguyenLieuID == DeNghiNguyenLieuID
                         select new ThongTinPhieuDeNghi
                         {
                             SanPhamID = a.SanPhamID,
                             DonHangID = a.DonHangID,
                             DeNghiNguyenLieuID = a.DeNghiNguyenLieuID,
                             MaDeNghi = a.MaDeNghi,
                             NoiDung = e.NoiDung,
                             TenDonHang = b.TenDonHang,
                             TenSanPham = c.TenSanPham,
                             MaDonHang = b.MaDonHang,
                             MaSanPham = c.MaSanPham,
                             TenNhanVien = d.TenNhanVien,
                             ThoiGian = a.ThoiGian,
                             LichSuDeNghi = a.LichSuDeNghis
                         }).FirstOrDefault();
            return PartialView(model);
        }

        #endregion




        #region Submit

        public JsonResult TaoDeNghi(string data)
        {
            unitofwork = new UnitOfWork();
            var model = new JavaScriptSerializer().Deserialize<DeNghiNguyenLieuModel>(data);
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            var rs = unitofwork.DeNghiNguyenLieus.TaoDeNghi(model, nhanvienid);
            return Json(new { code = rs.Split('_')[0], message = rs.Split('_')[1] });
        }
        public JsonResult DuyetDeNghi(string data)
        {
            unitofwork = new UnitOfWork();
            var model = new JavaScriptSerializer().Deserialize<DeNghiNguyenLieuModel>(data);
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            var rs = unitofwork.DeNghiNguyenLieus.DuyetDeNghi(model, nhanvienid);
            return Json(new { code = rs.Split('_')[0], message = rs.Split('_')[1] });
        }
        public JsonResult XuatNguyenLieu(string data)
        {
            unitofwork = new UnitOfWork();
            var model = new JavaScriptSerializer().Deserialize<DeNghiNguyenLieuModel>(data);
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            var rs = unitofwork.DeNghiNguyenLieus.XuatNguyenLieu(model, nhanvienid);
            var denghicongtrinh = unitofwork.Context.DeNghiNguyenLieux.FirstOrDefault(p => p.DeNghiNguyenLieuID == model.DeNghiNguyenLieuID);
            var list = unitofwork.DeNghiNguyenLieus.LayDanhSachDuyetDeNghiNguyenLieu(denghicongtrinh.DeNghiNguyenLieuID);
            var list2 = unitofwork.DeNghiNguyenLieus.LayDanhSachNguyenLieuDaXuat(denghicongtrinh.DeNghiNguyenLieuID);
            var lst2 = list.Where(p => (p.SoLuongDuyet.GetValueOrDefault() - list2.Where(p2 => p2.NguyenLieuID == p.NguyenLieuID).Sum(p2 => p2.SoLuong)).GetValueOrDefault() > 0).Count();
            if (lst2 == 0)
            {
                denghicongtrinh.TrangThai = 5;
            }
            unitofwork.Save();
            return Json(new { code = rs.Split('_')[0], message = rs.Split('_')[1] });
        }
        public JsonResult Tuchoi(string data)
        {
            unitofwork = new UnitOfWork();
            var model = new JavaScriptSerializer().Deserialize<DeNghiNguyenLieuModel>(data);
            var nhanvienid = Guid.Parse(CookiePersister.getAcountInfo().NhanVienID);
            var rs = unitofwork.DeNghiNguyenLieus.TuChoi(model, nhanvienid);
            return Json(new { code = rs.Split('_')[0], message = rs.Split('_')[1] });
        }
        #endregion

        //#region Excel
        //public clsExcelResult XuatExcelDeNghiDieuChuyeNguyenLieu(Guid DeNghiNguyenLieuID)
        //{
        //    string fileName = "";
        //    fileName = "DeNghiDieuChuyenNguyenLieu.xlsx";
        //    string urlTemp = "";
        //    urlTemp = "~\\TempExcel\\NguyenLieu\\DeNghiDieuChuyenNguyenLieu.xlsx";
        //    clsExcelResult clsResult = new clsExcelResult();
        //    ExcelFile xls = CreateExcelDeNghiDieuChuyenNguyenLieu(Server.MapPath(urlTemp), DeNghiNguyenLieuID);

        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        xls.Save(ms);
        //        ms.Position = 0;
        //        clsResult.ms = ms;
        //        clsResult.FileName = fileName;
        //        clsResult.type = "xlsx";
        //        return clsResult;
        //    }
        //}
        //public ExcelFile CreateExcelDeNghiDieuChuyenNguyenLieu(String path, Guid DeNghiNguyenLieuID)
        //{
        //    XlsFile Result = new XlsFile(true);
        //    try
        //    {

        //        Result.Open(path);
        //        FlexCelReport fr = new FlexCelReport();
        //        unitofwork = new UnitOfWork();
        //        var list = unitofwork.DeNghiNguyenLieus.LayDanhSachNguyenLieuDuocDeNghiDieuChuyen(DeNghiNguyenLieuID);

        //        DataTable dtDeNghiDieuChuyenNguyenLieu = Utils.Utilities.ToDataTable(list);
        //        //DataTable dtGroup = ReportModels.SelectDistinct("ChiTietCha", dtDieuChuyen, "strNgay", "strNgay", "", "");

        //        dtDeNghiDieuChuyenNguyenLieu.TableName = "ChiTiet";
        //        //dtGroup.TableName = "ChiTietCha";

        //        fr.AddTable("ChiTiet", dtDeNghiDieuChuyenNguyenLieu);
        //        //fr.AddTable("ChiTietCha", dtGroup);

        //        fr.Run(Result);
        //        fr.Dispose();
        //        dtDeNghiDieuChuyenNguyenLieu.Dispose();
        //        //dtGroup.Dispose();
        //        return Result;
        //    }
        //    catch (Exception ex)
        //    {
        //        return Result;
        //    }
        //}

        //public clsExcelResult XuatExcelDeNghiMuaNguyenLieu(Guid DeNghiNguyenLieuID)
        //{
        //    string fileName = "";
        //    fileName = "DeNghiMuaNguyenLieu.xlsx";
        //    string urlTemp = "";
        //    urlTemp = "~\\TempExcel\\NguyenLieu\\DeNghiMuaNguyenLieu.xlsx";
        //    clsExcelResult clsResult = new clsExcelResult();
        //    ExcelFile xls = CreateExcelDeNghiMuaNguyenLieu(Server.MapPath(urlTemp), DeNghiNguyenLieuID);

        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        xls.Save(ms);
        //        ms.Position = 0;
        //        clsResult.ms = ms;
        //        clsResult.FileName = fileName;
        //        clsResult.type = "xlsx";
        //        return clsResult;
        //    }
        //}
        //public ExcelFile CreateExcelDeNghiMuaNguyenLieu(String path, Guid DeNghiNguyenLieuID)
        //{
        //    XlsFile Result = new XlsFile(true);
        //    try
        //    {

        //        Result.Open(path);
        //        FlexCelReport fr = new FlexCelReport();
        //        unitofwork = new UnitOfWork();
        //        var list = unitofwork.DeNghiNguyenLieus.LayDanhSachNguyenLieuDuocDeNghiMua(DeNghiNguyenLieuID);

        //        DataTable dtDeNghiMuaNguyenLieu = Utils.Utilities.ToDataTable(list);
        //        //DataTable dtGroup = ReportModels.SelectDistinct("ChiTietCha", dtDieuChuyen, "strNgay", "strNgay", "", "");

        //        dtDeNghiMuaNguyenLieu.TableName = "ChiTiet";
        //        //dtGroup.TableName = "ChiTietCha";

        //        fr.AddTable("ChiTiet", dtDeNghiMuaNguyenLieu);
        //        //fr.AddTable("ChiTietCha", dtGroup);

        //        fr.Run(Result);
        //        fr.Dispose();
        //        dtDeNghiMuaNguyenLieu.Dispose();
        //        //dtGroup.Dispose();
        //        return Result;
        //    }
        //    catch (Exception ex)
        //    {
        //        return Result;
        //    }
        //}

        //public clsExcelResult XuatExcelDuyetDeNghiNguyenLieuXuat(Guid DeNghiNguyenLieuID)
        //{
        //    string fileName = "";
        //    fileName = "DuyetDeNghiNguyenLieuXuat.xlsx";
        //    string urlTemp = "";
        //    urlTemp = "~\\TempExcel\\NguyenLieu\\DuyetDeNghiNguyenLieuXuat.xlsx";
        //    clsExcelResult clsResult = new clsExcelResult();
        //    ExcelFile xls = CreateExcelDuyetDeNghiNguyenLieuXuat(Server.MapPath(urlTemp), DeNghiNguyenLieuID);

        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        xls.Save(ms);
        //        ms.Position = 0;
        //        clsResult.ms = ms;
        //        clsResult.FileName = fileName;
        //        clsResult.type = "xlsx";
        //        return clsResult;
        //    }
        //}
        //public ExcelFile CreateExcelDuyetDeNghiNguyenLieuXuat(String path, Guid DeNghiNguyenLieuID)
        //{
        //    XlsFile Result = new XlsFile(true);
        //    try
        //    {

        //        Result.Open(path);
        //        FlexCelReport fr = new FlexCelReport();
        //        unitofwork = new UnitOfWork();
        //        var list = unitofwork.DeNghiNguyenLieus.LayDanhSachDuyetDeNghiNguyenLieu(DeNghiNguyenLieuID);

        //        DataTable dtDuyetDeNghiNguyenLieuXuat = Utils.Utilities.ToDataTable(list);
        //        //DataTable dtGroup = ReportModels.SelectDistinct("ChiTietCha", dtDieuChuyen, "strNgay", "strNgay", "", "");

        //        dtDuyetDeNghiNguyenLieuXuat.TableName = "ChiTiet";
        //        //dtGroup.TableName = "ChiTietCha";

        //        fr.AddTable("ChiTiet", dtDuyetDeNghiNguyenLieuXuat);
        //        //fr.AddTable("ChiTietCha", dtGroup);

        //        fr.Run(Result);
        //        fr.Dispose();
        //        dtDuyetDeNghiNguyenLieuXuat.Dispose();
        //        //dtGroup.Dispose();
        //        return Result;
        //    }
        //    catch (Exception ex)
        //    {
        //        return Result;
        //    }
        //}
        //#endregion
    }
}
