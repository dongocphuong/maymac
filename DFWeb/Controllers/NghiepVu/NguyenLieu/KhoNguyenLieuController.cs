﻿using DF.DataAccess;
using DF.DataAccess.DBML;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.Excel;
using DF.DBMapping.ModelsExt.VatTu;
using DFWeb.Models.ReportModels;
using DFWeb.Security;
using FlexCel.Core;
using FlexCel.Report;
using FlexCel.XlsAdapter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DFWeb.Controllers.NghiepVu.NguyenLieu
{
    [CustomAuthorize(Roles = "2001")]
    public class KhoNguyenLieuController : Controller
    {
        //
        // GET: /KhoNguyenLieu/
        UnitOfWork unitofwork;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetDataKhoTong()
        {
            try
            {
                unitofwork = new UnitOfWork();
                var list = unitofwork.KhoNguyenLieus.LayTon(Define.CONGTRINHTONG).ToList();
                return Json(new { data = list, total = list.Count, code = "succes" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDataKhoTongBoTonKhong()
        {
            try
            {
                unitofwork = new UnitOfWork();
                var list = unitofwork.KhoNguyenLieus.LayTon(Define.CONGTRINHTONG).Where(p => p.SoLuongTon > 0).OrderBy(t => t.TenNguyenLieu).ToList();
                return Json(new { data = list, total = list.Count, code = "succes" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDataKhoTongBoTonKhongTheoDonHang(string DonHangID)
        {
            try
            {
                var db = new DBMLDFDataContext();
                var dhid = Guid.Empty;
                if (DonHangID != "")
                {
                    dhid = Guid.Parse(DonHangID);
                }
                var list = db.Web_NguyenLieu_TonKho_DonHangCan(dhid).OrderBy(t => t.TenNguyenLieu).ToList();
                return Json(new { data = list, total = list.Count, code = "succes" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult PopupNhapKhoTong()
        {
            return PartialView("_PopupNhapKhoTong");
        }
        public ActionResult PopupXuatKhoTong(string PhieuXuatVatTuID, string LoaiXuat)
        {
            unitofwork = new UnitOfWork();
            ChiTietPhieuXuatVatTuResult model = new ChiTietPhieuXuatVatTuResult();
            if (PhieuXuatVatTuID != null && PhieuXuatVatTuID != "")
            {
                var phieuxuatid = Guid.Parse(PhieuXuatVatTuID);
                model = unitofwork.Context.PhieuXuatNguyenLieux.Where(p => p.PhieuXuatNguyenLieuID == phieuxuatid).ToList().Select(t => new ChiTietPhieuXuatVatTuResult()
                {
                    CongTrinhNhanID = t.DonHangNhanID,
                    PhieuXuatVatTuID = t.PhieuXuatNguyenLieuID,
                    LoaiHinhXuat = t.LoaiHinh.GetValueOrDefault(),
                    NhanVienXuatID = t.NguoiTaoID,
                    ThoiGian = t.ThoiGian.GetValueOrDefault(),
                    DanhSachHinhAnh = t.DanhSachHinhAnhs
                }).SingleOrDefault();
            }
            return PartialView("_PopupXuatKhoTong", model);
        }
        public JsonResult GetAllDataDoiTac()
        {
            unitofwork = new UnitOfWork();
            var list = unitofwork.Context.DoiTacs.Where(p => p.IsActive == true).Select(t => new
            {
                DoiTacID = t.DoiTacID,
                TenDoiTac = t.TenDoiTac
            }).ToList();
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllHoaDon()
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var list = db.Web_DonHang_LayDanhSachDonHangChuaHoanThien(Guid.Parse(Define.CONGTRINHTONG));
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachNguyenLieu(string DoiTacID)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var list = db.Web_NguyenLieu_LayDanhSach().OrderBy(t => t.TenNguyenLieu).ToList();
            return Json(new { data = list, total = list.Count() }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Mua(string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var kq = unitofwork.KhoNguyenLieus.Mua(ThoiGian, Guid.Parse(DoiTacID), DataChiTietNhap, DSHinhAnh, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                if (kq == 1)
                {
                    return Json(new { code = "success", message = Models.Language.GetText("muathanhcong") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CapNhatMua(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh)
        {
            try
            {
                unitofwork = new UnitOfWork();
                int kq = unitofwork.KhoNguyenLieus.CapNhatPhieuMua(PhieuNhapVatTuID, ThoiGian, DoiTacID, DataChiTietNhap, DSHinhAnh, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                if (kq == 1)
                {
                    return Json(new { code = "success", message = Models.Language.GetText("mgs_capnhatthanhcong") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("mgs_capnhatthatbai") }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("coloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Xuat(string PhieuXuatVatTuID, string LoaiHinh, string CongTrinhNhanID, string DoiThiCongNhanID, string DataChiTietPhieuXuat, string NoiDung, string DSHinhanh, string ThoiGian)
        {
            var NhanVienXuatID = CookiePersister.getAcountInfo().NhanVienID;
            if (int.Parse(LoaiHinh) == 1)
            {
                NoiDung = Models.Language.GetText("xuatsudung");
            }
            else
            {
                NoiDung = Models.Language.GetText("xuatdieuchuyen");
            }
            try
            {
                unitofwork = new UnitOfWork();
                int kq = 1;
                if (PhieuXuatVatTuID == null || PhieuXuatVatTuID == "")
                {
                    kq = unitofwork.KhoNguyenLieus.Xuat(LoaiHinh, Define.CONGTRINHTONG, CongTrinhNhanID, NhanVienXuatID, DataChiTietPhieuXuat, DSHinhanh, ThoiGian, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                }
                else
                {
                    kq = unitofwork.KhoNguyenLieus.CapNhatXuat(PhieuXuatVatTuID, LoaiHinh, Define.CONGTRINHTONG, CongTrinhNhanID, NhanVienXuatID, DataChiTietPhieuXuat, DSHinhanh, NoiDung, ThoiGian, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                }
                if (kq == 1)
                {
                    return Json(new { code = "success", message = Models.Language.GetText("xuatvattuthanhcong") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("coloixayra") }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("coloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDataLichSuNhapKhoTong(int LoaiHinhNhap, string TuNgay = "", string DenNgay = "")
        {
            try
            {
                unitofwork = new UnitOfWork();
                var data = unitofwork.KhoNguyenLieus.LichSuNhapKho(Guid.Parse(Define.CONGTRINHTONG), LoaiHinhNhap, TuNgay, DenNgay);
                return Json(new { data = data, total = data.Count, code = "success", message = Models.Language.GetText("mgs_laydulieuthanhcong") }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("laydulieukhongthanhcong") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult PopupChiTietPhieuNhapVatTu(string PhieuNhapVatTuID, string LoaiNhap, string PhieuXuatVatTuID)
        {
            unitofwork = new UnitOfWork();
            var model = new ChiTietPhieuNhapVatTuResult();
            var pnvtid = Guid.Parse(PhieuNhapVatTuID);
            model = unitofwork.Context.PhieuNhapNguyenLieux.Where(p => p.PhieuNhapNguyenLieuID == pnvtid).ToList().Select(t => new ChiTietPhieuNhapVatTuResult()
            {
                PhieuNhapVatTuID = t.PhieuNhapNguyenLieuID,
                DoiTacID = t.DoiTacID,
                DSHinhAnh = t.DanhSachHinhAnhs,
                ThoiGianNhap = t.ThoiGian.GetValueOrDefault(),
                PhieuXuatNguyenLieuID = t.PhieuXuatNguyenLieuID
            }).SingleOrDefault();
            if (model.DoiTacID == null)
            {
                var phieuxuatnl = unitofwork.Context.PhieuXuatNguyenLieux.FirstOrDefault(p => p.PhieuXuatNguyenLieuID == model.PhieuXuatNguyenLieuID);
                model.HoaDonDieuChuyenID = phieuxuatnl.DonHangID;
            }
            return PartialView("_PopupChiTietPhieuNhap", model);
        }
        public JsonResult GetDataChiTietPhieuNhap(string PhieuNhapVatTuID)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var data = db.Web_NguyenLieu_LayChiTietPhieuNhapNguyenLieu(Guid.Parse(PhieuNhapVatTuID)).ToList();
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult TraNguyenLieu(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh)
        {
            try
            {
                unitofwork = new UnitOfWork();
                int kq = unitofwork.KhoNguyenLieus.TraNguyenLieu(PhieuNhapVatTuID, ThoiGian, DoiTacID, DataChiTietNhap, DSHinhAnh, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                if (kq == 1)
                {
                    return Json(new { code = "success", message = Models.Language.GetText("mgs_capnhatthanhcong") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("mgs_capnhatthatbai") }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("coloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult XoaPhieuNhapNguyenLieu(string PhieuNhapNguyenLieuID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var kq = unitofwork.Context.PhieuNhapNguyenLieux.Find(Guid.Parse(PhieuNhapNguyenLieuID));
                kq.IsActive = false;
                unitofwork.Context.SaveChanges();
                unitofwork.Save();
                return Json(new { code = "success", message = Models.Language.GetText("mgs_capnhatthanhcong") }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("coloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult XoaPhieuXuatNguyenLieu(string PhieuXuatNguyenLieuID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var kq = unitofwork.Context.PhieuXuatNguyenLieux.Find(Guid.Parse(PhieuXuatNguyenLieuID));
                kq.IsActive = false;
                unitofwork.Context.SaveChanges();
                unitofwork.Save();
                return Json(new { code = "success", message = Models.Language.GetText("mgs_capnhatthanhcong") }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("coloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CapNhatTraNguyenLieu(string PhieuXuatVatTuID, string ThoiGian, string DoiTacID, string DataChiTietPhieuXuat, string DSHinhAnh)
        {
            try
            {
                unitofwork = new UnitOfWork();
                int kq = unitofwork.KhoNguyenLieus.CapNhatTraNguyenLieu(PhieuXuatVatTuID, ThoiGian, DataChiTietPhieuXuat, DSHinhAnh, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                if (kq == 1)
                {
                    return Json(new { code = "success", message = Models.Language.GetText("mgs_capnhatthanhcong") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("mgs_capnhatthatbai") }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("coloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult LayDanhSachDonVi()
        {
            unitofwork = new UnitOfWork();
            var List = unitofwork.DonVis.getdata().Select(p => new { DonViID = p.DonViId, p.TenDonVi }).ToList();
            return Json(new { data = List, total = List.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetLichSuXuatKhoTong(int LoaiHinhXuat, string CongTrinhDenID, string DoiThiCongDenID, string TuNgay = "", string DenNgay = "")
        {
            unitofwork = new UnitOfWork();
            var data = unitofwork.KhoNguyenLieus.LichSuXuatKho(Guid.Parse(Define.CONGTRINHTONG), LoaiHinhXuat, CongTrinhDenID, TuNgay, DenNgay);
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDataChiTietPhieuXuat(string PhieuXuatVatTuID)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var data = db.Web_NguyenLieu_LayChiTietPhieuXuatNguyenLieu(Guid.Parse(PhieuXuatVatTuID)).ToList();
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
        }
        //Xuất excel lịch sử nhập kho nguyên liệu
        public clsExcelResult XuatExcelLichSuNhapVatTuKhoTong(string filters, int LoaiHinhNhap, string TuNgay = "", string DenNgay = "")
        {
            string fileName = "";
            fileName = "LichSuNhapKhoNguyenLieu.xlsx";
            string urlTemp = "";
            urlTemp = "~\\TempExcel\\VatTu\\LichSuNhapKhoNguyenLieu.xlsx";
            clsExcelResult clsResult = new clsExcelResult();
            ExcelFile xls = CreateExcelLichSuNhapVatTuKhoTong(Server.MapPath(urlTemp), filters, LoaiHinhNhap, TuNgay, DenNgay);

            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public ExcelFile CreateExcelLichSuNhapVatTuKhoTong(String path, string filters, int LoaiHinhNhap, string TuNgay = "", string DenNgay = "")
        {
            XlsFile Result = new XlsFile(true);
            try
            {

                Result.Open(path);
                FlexCelReport fr = new FlexCelReport();
                unitofwork = new UnitOfWork();
                List<FilterModel> filter = JsonConvert.DeserializeObject<List<FilterModel>>(filters);
                var data = unitofwork.KhoNguyenLieus.LichSuNhapKho(Guid.Parse(Define.CONGTRINHTONG), LoaiHinhNhap, TuNgay, DenNgay);
                var stt = 1;
                var data2 = data.Where(p => GetDateFilterNhap(p, filter)).OrderByDescending(p => p.ThoiGian).Select(p => new ExcelLichSuNhapVatTu()
                {
                    STT = stt++,
                    TenDonVi = p.TenDonVi,
                    TenNguyenLieu = p.TenNguyenLieu,
                    MaNguyenLieu = p.MaNguyenLieu,
                    TenDoiTac = p.TenDoiTac,
                    NoiDung = p.NoiDung,
                    MaPhieuNhap = p.MaPhieuNhap,
                    GiaNhap = p.GiaNhap,
                    SoLuong = p.SoLuong,
                    ThanhTien = p.ThanhTien,
                    TenNhanVien = p.TenNhanVien,
                    ThoiGian = p.ThoiGian,
                }).ToList();



                DataTable dtQuanLyVatTu = Utils.Utilities.ToDataTable(data2);
                //DataTable dtGroup = ReportModels.SelectDistinct("ChiTietCha", dtDieuChuyen, "strNgay", "strNgay", "", "");

                dtQuanLyVatTu.TableName = "ChiTiet";
                //dtGroup.TableName = "ChiTietCha";

                fr.AddTable("ChiTiet", dtQuanLyVatTu);
                //fr.AddTable("ChiTietCha", dtGroup);

                fr.Run(Result);
                fr.Dispose();
                dtQuanLyVatTu.Dispose();
                //dtGroup.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
        //end xuất excel lịch sử nhập kho nguyên liệu
        //Xuất excel lịch sử xuất kho nguyên liệu
        public clsExcelResult XuatExcelLichSuXuatVatTuKhoTong(string filters, int LoaiHinhXuat, string CongTrinhDenID, string TuNgay = "", string DenNgay = "")
        {
            string fileName = "";
            fileName = "LichSuXuatKhoNguyenLieu.xlsx";
            string urlTemp = "";
            urlTemp = "~\\TempExcel\\VatTu\\LichSuXuatKhoNguyenLieu.xlsx";
            clsExcelResult clsResult = new clsExcelResult();
            ExcelFile xls = CreateExcelLichSuXuatVatTuKhoTong(Server.MapPath(urlTemp), filters, LoaiHinhXuat, CongTrinhDenID, TuNgay, DenNgay);

            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public ExcelFile CreateExcelLichSuXuatVatTuKhoTong(String path, string filters, int LoaiHinhXuat, string CongTrinhDenID, string TuNgay = "", string DenNgay = "")
        {
            XlsFile Result = new XlsFile(true);
            try
            {

                Result.Open(path);
                FlexCelReport fr = new FlexCelReport();
                unitofwork = new UnitOfWork();
                List<FilterModel> filter = JsonConvert.DeserializeObject<List<FilterModel>>(filters);
                var data = unitofwork.KhoNguyenLieus.LichSuXuatKho(Guid.Parse(Define.CONGTRINHTONG), LoaiHinhXuat, CongTrinhDenID, TuNgay, DenNgay);
                var stt = 1;
                var data2 = data.Where(p => GetDateFilterXuat(p, filter)).OrderByDescending(p => p.ThoiGian).Select(p => new ExcelLichSuXuatVatTu()
                {
                    STT = stt++,
                    TenDonVi = p.TenDonVi,
                    TenNguyenLieu = p.TenNguyenLieu,
                    MaNguyenLieu = p.MaNguyenLieu,
                    TenDoiTac = p.TenDoiTac,
                    NoiDung = p.NoiDung,
                    SoLuong = p.SoLuong,
                    TenNhanVien = p.TenNhanVien,
                    ThoiGian = p.ThoiGian,
                }).ToList();



                DataTable dtQuanLyVatTu = Utils.Utilities.ToDataTable(data2);
                //DataTable dtGroup = ReportModels.SelectDistinct("ChiTietCha", dtDieuChuyen, "strNgay", "strNgay", "", "");

                dtQuanLyVatTu.TableName = "ChiTiet";
                //dtGroup.TableName = "ChiTietCha";

                fr.AddTable("ChiTiet", dtQuanLyVatTu);
                //fr.AddTable("ChiTietCha", dtGroup);

                fr.Run(Result);
                fr.Dispose();
                dtQuanLyVatTu.Dispose();
                //dtGroup.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
        //end xuất excel lịch sử xuất vật tư kho tổng
        public bool GetDateFilterNhap(Web_NguyenLieu_LichSuNhapKhoResult ls, List<FilterModel> filters)
        {
            var istrue = filters.Count() <= 0;
            foreach (var filter in filters)
            {
                var value = ls.GetType().GetProperty(filter.field).GetValue(ls);
                if (value != null)
                {
                    if (filter.Operator == "contains" || filter.Operator == "eq")
                    {
                        if (removeUnicode(value.ToString().ToLower()).Contains(removeUnicode(filter.value.ToLower())))
                        {
                            istrue = true;
                            break;
                        }
                    }
                    else
                    {
                        if (value.ToString() == filter.value)
                        {
                            istrue = true;
                            break;
                        }
                    }
                }
            }

            return istrue;
        }
        public bool GetDateFilterXuat(Web_NguyenLieu_LichSuXuatKhoResult ls, List<FilterModel> filters)
        {
            var istrue = filters.Count() <= 0;
            foreach (var filter in filters)
            {
                var value = ls.GetType().GetProperty(filter.field).GetValue(ls);
                if (value != null)
                {
                    if (filter.Operator == "contains" || filter.Operator == "eq")
                    {
                        if (removeUnicode(value.ToString().ToLower()).Contains(removeUnicode(filter.value.ToLower())))
                        {
                            istrue = true;
                            break;
                        }
                    }
                    else
                    {
                        if (value.ToString() == filter.value)
                        {
                            istrue = true;
                            break;
                        }
                    }
                }
            }

            return istrue;
        }
        public string removeUnicode(string input)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
            "đ",
            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
            "í","ì","ỉ","ĩ","ị",
            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
            "ý","ỳ","ỷ","ỹ","ỵ",};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "d",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                input = input.Replace(arr1[i], arr2[i]);
                input = input.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return input;
        }
    }
}