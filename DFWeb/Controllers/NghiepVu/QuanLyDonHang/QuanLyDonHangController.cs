﻿using DF.DataAccess;
using DF.DBMapping.Models;
using DF.DBMapping.ModelsExt;
using DFSCommon;
using DFSCommon.Models.Enums;
using DFWeb.Common;
using DFWeb.Models;
using DFWeb.Security;
using DFWeb.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Webservice.FCMModules;
using System.Configuration;
using System.Transactions;
using DF.DataAccess.DBML;
using FlexCel.Core;
using DFWeb.Models.ReportModels;
using FlexCel.XlsAdapter;
using DF.Utilities;
using Excel;
using DF.DBMapping.ModelsExt.NghiepVu;
using DF.DBMapping.ModelsExt.Excel;
using FlexCel.Report;
using Newtonsoft.Json;

namespace DFWeb.Controllers.NghiepVu.QuanLyDonHang
{
    [CustomAuthorize(Roles = "1010")]
    public class QuanLyDonHangController : Controller
    {
        //
        // GET: /QuanLyDonHang/
        protected DBMLDFDataContext db;
        protected UnitOfWork unitofwork;
        string status;
        string code;
        public ActionResult Index()
        {
            return View();
        }
        //Xuất excel ---------------------------------------------------------------------------------------
        public clsExcelResult XuatExcelTungDonHang(Guid DonHangID, Guid DoiTacID)
        {
            string fileName = "";
            fileName = "Đơn đặt hàng.xlsx";
            string urlTemp = "";
            urlTemp = "~\\TempExcel\\TemplateDonHang.xlsx";
            clsExcelResult clsResult = new clsExcelResult();
            ExcelFile xls = CreateExcel(Server.MapPath(urlTemp), DonHangID, DoiTacID);

            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public ExcelFile CreateExcel(String path, Guid DonHangID, Guid DoiTacID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            XlsFile Result = new XlsFile(true);
            try
            {
                Result.Open(path);
                FlexCelReport fr = new FlexCelReport();
                unitofwork = new UnitOfWork();
                //
                List<Web_BaoCao_DonDatHangTheoNamByDoiTacIDResult> lst = db.Web_BaoCao_DonDatHangTheoNamByDoiTacID(DoiTacID).Where(x => x.DonHangID == DonHangID).OrderBy(x => x.Size).ToList();
                List<Web_BaoCao_DanhSachDoiTacDaDatHangResult> lstt = db.Web_BaoCao_DanhSachDoiTacDaDatHang().Where(p => p.DoiTacID == DoiTacID).ToList();
                var stt = 1;
                var data2 = lst.Select(p => new
                {
                    STT = stt++,
                    TenSanPham = p.TenSanPham,
                    DacDiem = p.DacDiem,
                    ChatLieu = p.ChatLieu,
                    Size = p.Size,
                    Mau = p.Mau,
                    MauPhoi = p.MauPhoi,
                    SoLuong = p.SoLuong,
                    GiaBan = p.DonGia,
                    ThanhTien = p.ThanhTien,
                    YeuCauKhac = p.YeuCauKhac
                }).ToList();
                DataTable dtThongTinChung = Utils.Utilities.ToDataTable(lstt);
                DataTable dtQuanLyVatTu = Utils.Utilities.ToDataTable(data2);
                dtQuanLyVatTu.TableName = "ChiTiet";
                dtThongTinChung.TableName = "ChiTietChung";
                fr.AddTable("ChiTietChung", dtThongTinChung);
                fr.AddTable("ChiTiet", dtQuanLyVatTu);
                fr.Run(Result);
                fr.Dispose();
                dtThongTinChung.Dispose();
                dtQuanLyVatTu.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
        //-------------------------------------------------------------------------------------------------------------------
        public JsonResult GetAllDataDonHang(string TuNgay, string DenNgay)
        {
            DateTime tn, dn;
            db = new DBMLDFDataContext();
            try
            {
                tn = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            }
            catch
            {
                tn = DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
            }
            try
            {
                dn = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null);
            }
            catch
            {
                dn = DateTime.Now;
            }
            var data = db.Web_DonHang_GetAllDataDonHang(tn, dn).OrderBy(t => t.ThoiGian).ToList();
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayTongSoLuongHang(string MauMayID)
        {
            db = new DBMLDFDataContext();
            Guid mmid = Guid.Empty;
            if (MauMayID != null)
            {
                mmid = Guid.Parse(MauMayID);
            }
            var data = db.Web_SanPham_LayTongSoLuongHang(mmid).FirstOrDefault();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachThietKeCoSanPham()
        {
            db = new DBMLDFDataContext();
            var data = db.Web_DonHang_LayDanhSachThietKeCoSanPham().OrderBy(p => p.TenMauMay).ToList();
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachSanPhamTheoThietKe(string MauMayID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            Guid mmid;
            try
            {
                mmid = Guid.Parse(MauMayID);
            }
            catch
            {
                mmid = Guid.Empty;
            }
            var data = db.Web_DonHang_LayDanhSachSanPhamTheoThietKe(mmid).OrderBy(t => t.Size).ThenBy(t => t.Mau).ToList();
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllDataByDonHangID(string data)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var dt = db.Web_DonHang_GetAllDataByDonHangID(Guid.Parse(data));
            return Json(new { data = dt, message = "Thành công" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult XoaDonHang(string DonHangID)
        {
            unitofwork = new UnitOfWork();
            Guid dhid;
            try
            {
                dhid = Guid.Parse(DonHangID);
            }
            catch
            {
                dhid = Guid.Empty;
            }
            var temp = unitofwork.Context.DonHangs.Find(dhid);
            temp.IsActive = false;
            unitofwork.Context.SaveChanges();
            unitofwork.Save();
            return Json(new { code = "success" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMaDonHang()
        {
            db = new DBMLDFDataContext();
            var SoHoaDon = "DH" + DateTime.Now.Date.ToString("yyyyMMdd");
            int counthoadon = db.Web_DonHang_CountSoDonHangTrongNgay().ToList().Count;
            var sodem = 3 - (counthoadon.ToString().Length);
            for (int i = 0; i < sodem; i++)
            {
                SoHoaDon += "0";
            }

            SoHoaDon += counthoadon > 0 ? (counthoadon + 1).ToString() : "1";
            return Json(new { data = SoHoaDon }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachThuocTinhCuaSanPham(string SanPhamID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            Guid spid = Guid.Empty;
            if (SanPhamID != null || SanPhamID != "")
            {
                spid = Guid.Parse(SanPhamID);
            }
            var dt = db.Web_DonHang_LayDanhSachThuocTinhCuaSanPham(spid).ToList();
            return Json(new { data = dt }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhThuocTinhCuaNhomNguyenLieu(string NguyenLieuID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            Guid nlid = Guid.Empty;
            if (NguyenLieuID != null || NguyenLieuID != "")
            {
                nlid = Guid.Parse(NguyenLieuID);
            }
            var dt = db.Web_DonHang_LayDanhThuocTinhCuaNhomNguyenLieu(nlid).ToList();
            return Json(new { data = dt }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CopySanPham(string SanPhamID, string size)
        {
            unitofwork = new UnitOfWork();
            var lst = unitofwork.SanPhams.CopySanPham(Guid.Parse(SanPhamID), size);
            if (lst)
            {
                return Json(new { code = "success", message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { code = "error", message = "Có lỗi trong quá trình sử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayDanhSachYeuCauTheoDonHang(Guid DonHangID)
        {
            unitofwork = new UnitOfWork();
            var dt = (from a in unitofwork.Context.YeuCauKhachHangs
                      join b in unitofwork.Context.DoiTacs on a.DoiTacID equals b.DoiTacID
                      join c in unitofwork.Context.DonHangs on a.YeuCauKhachHangID equals c.YeuCauKhachHangID
                      join d in unitofwork.Context.ChiTietYeuCauKhachHangs on a.YeuCauKhachHangID equals d.YeuCauKhachHangID
                      where a.IsActive == true && c.DonHangID == DonHangID
                      select new
                      {
                          a.HinhAnh,
                          a.ThoiGian,
                          a.YeuCauKhachHangID,
                          b.TenDoiTac,
                          d.SoLuongDat,
                          d.GiaBan,
                          d.ThoiGianGiaoHang
                      }).ToList().FirstOrDefault();

            return Json(new { data = dt, message = "Thành công" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult AddOrUpdateDonHang(string data, string sanphams, string chitiet)
        {
            unitofwork = new UnitOfWork();
            int result = unitofwork.DonHangs.AddOrUpdateDonHang(data, sanphams, chitiet, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
            if (result == 0)
            {
                code = "error";
                status = "Cập nhật không thành công! Có lỗi xảy ra";
            }
            if (result == 3)
            {
                code = "error";
                status = "Không thể sửa đơn hàng vì số lượng sản phẩm đã sản xuất lớn hơn số lượng đặt";
            }
            else
            {
                code = "success";
                status = "Thành công!";
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachDoiTac()
        {
            unitofwork = new UnitOfWork();
            var dt = unitofwork.Context.DoiTacs.OrderBy(t => t.TenDoiTac).Where(p => p.IsActive == true);
            return Json(new { data = dt, total = dt.Count() }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachSanPham(string HoaDonID = "")
        {
            db = new DBMLDFDataContext();
            try
            {
                var tp = Guid.Empty;
                if (HoaDonID != "")
                {
                    tp = Guid.Parse(HoaDonID);
                }
                var lst = db.Web_DonHang_LayDanhSachChiTietSanPham(tp).ToList();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayDanhSachNguyenLieuDeDinhNghia()
        {
            db = new DBMLDFDataContext();
            try
            {

                var lst = db.Web_SanPham_LayDanhSach().Select(p => new HoaDonSanPhamModel
                {
                    AnhDaiDien = p.AnhDaiDien,
                    LoaiHinh = 1,
                    MaSanPham = p.MaSanPham,
                    SanPhamID = p.SanPhamID,
                    TenDonVi = p.TenDonVi,
                    TenSanPham = p.TenSanPham,
                    DonGia = p.DonGia,
                    Size = p.Size,
                    Mau = p.Mau
                }).ToList();
                //lst.AddRange(db.Web_ThanhPham_LayDanhSach().Select(p => new HoaDonSanPhamModel
                //{
                //    AnhDaiDien = p.AnhDaiDien,
                //    LoaiHinh = 2,
                //    MaSanPham = p.MaThanhPham,
                //    SanPhamID = p.ThanhPhamID,
                //    TenDonVi = p.TenDonVi,
                //    TenSanPham = p.TenThanhPham,
                //    DonGia = p.DonGia
                //}));
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayDanhSachHinhAnhLayDonHang(string DonHangID = "")
        {
            unitofwork = new UnitOfWork();
            try
            {
                var tp = Guid.Empty;
                if (DonHangID != "")
                {
                    tp = Guid.Parse(DonHangID);
                }
                var lst = unitofwork.Context.HinhAnhDonHangs.Where(p => p.DonHangID == tp).Select(p => new lstImageHopDong
                {
                    img = p.FileMaHoa,
                    name = p.TenFile
                }).ToList();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayDanhSachYeuCau()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var dt = db.Web_YeuCauKhachHang_GetAllData().Where(x => x.TinhTrang == 3).ToList();
            return Json(new { data = dt, total = dt.Count }, JsonRequestBehavior.AllowGet);
        }

        //Xuất excel
        public clsExcelResult XuatExcelQuanLyDonHang(string filters, string TuNgay, string DenNgay)
        {
            string fileName = "";
            fileName = "DanhSachDonHang.xlsx";
            string urlTemp = "";
            urlTemp = "~\\TempExcel\\VatTu\\DanhSachDonHang.xlsx";
            clsExcelResult clsResult = new clsExcelResult();
            ExcelFile xls = CreateExcel(Server.MapPath(urlTemp), filters, TuNgay, DenNgay);

            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public ExcelFile CreateExcel(String path, string filters, string TuNgay, string DenNgay)
        {
            XlsFile Result = new XlsFile(true);
            try
            {

                Result.Open(path);
                FlexCelReport fr = new FlexCelReport();
                unitofwork = new UnitOfWork();
                List<FilterModel> filter = JsonConvert.DeserializeObject<List<FilterModel>>(filters);
                DateTime tn, dn;
                db = new DBMLDFDataContext();
                try
                {
                    tn = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
                }
                catch
                {
                    tn = DateTime.ParseExact("01/01/1900", "dd/MM/yyyy", null);
                }
                try
                {
                    dn = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null);
                }
                catch
                {
                    dn = DateTime.Now;
                }
                var data = db.Web_DonHang_GetAllDataDonHang(tn, dn).OrderBy(t => t.ThoiGian).ToList();
                var stt = 1;
                var data2 = data.Where(p => GetDateFilter(p, filter)).Select(p => new ExcelDanhSachDonHang()
                {
                    STT = stt++,
                    TenDonHang = p.TenDonHang,
                    MaDonHang = p.MaDonHang,
                    ThoiGian = p.ThoiGian,
                    TrangThai = p.TrangThai,
                    TenDoiTac = p.TenDoiTac,
                    GiaTriDonHang = p.GiaTriDonHang,
                    DiaDiemGiaoHang = p.DiaDiemGiaoHang,

                }).ToList();
                DataTable dtQuanLyVatTu = Utils.Utilities.ToDataTable(data2);
                dtQuanLyVatTu.TableName = "ChiTiet";
                fr.AddTable("ChiTiet", dtQuanLyVatTu);
                fr.Run(Result);
                fr.Dispose();
                dtQuanLyVatTu.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
        //end xuất excel 
        public string removeUnicode(string input)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
            "đ",
            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
            "í","ì","ỉ","ĩ","ị",
            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
            "ý","ỳ","ỷ","ỹ","ỵ",};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "d",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                input = input.Replace(arr1[i], arr2[i]);
                input = input.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return input;
        }

        public bool GetDateFilter(Web_DonHang_GetAllDataDonHangResult ls, List<FilterModel> filters)
        {
            var istrue = filters.Count() <= 0;
            foreach (var filter in filters)
            {
                var value = ls.GetType().GetProperty(filter.field).GetValue(ls);
                if (value != null)
                {
                    if (filter.Operator == "contains" || filter.Operator == "eq")
                    {
                        if (removeUnicode(value.ToString().ToLower()).Contains(removeUnicode(filter.value.ToLower())))
                        {
                            istrue = true;
                            break;
                        }
                    }
                    else
                    {
                        if (value.ToString() == filter.value)
                        {
                            istrue = true;
                            break;
                        }
                    }
                }
            }
            return istrue;
        }
    }
}
