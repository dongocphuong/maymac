﻿using DF.DataAccess;
using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;
using DFWeb.Common;
using DFWeb.Security;
using System.Data.SqlClient;
using DFWeb.Models.ReportModels;
using FlexCel.Core;
using System.IO;
using FlexCel.XlsAdapter;
using FlexCel.Report;
using System.Transactions;
using DF.Utilities;
using DFWeb.Models;
using DF.DataAccess.DBML;
using DFWeb.Models.CongTrinhPhongBan;
using DF.DBMapping.ModelsExt;
using Excel;
using DF.DBMapping.ModelsExt.NghiepVu.VatTu;

namespace DFWeb.Controllers.NghiepVu
{
    [CustomAuthorize(Roles = "1008")]
    public class QuanLyMauMayController : Controller
    {
        //
        // GET: /QuanLyMauMay/
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        string status;
        string code;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAllDataMauMay()
        {
            unitofwork = new UnitOfWork();
            try
            {
                List<Web_MauMay_GetAllDataResult> lst = unitofwork.MauMays.GetAllData().OrderByDescending(t => t.ThoiGian).ToList();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAllDataChiTietMauMay(string ChiTietMauMayID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            Guid MMID;
            try
            {
                MMID = Guid.Parse(ChiTietMauMayID);
            }
            catch
            {
                MMID = Guid.Empty;
            }
            try
            {
                List<Web_MauMay_GetAllDateChiTietMauMayResult> lst = db.Web_MauMay_GetAllDateChiTietMauMay(MMID).OrderBy(t => t.STT).ToList();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAllDatabyMauMayID(string MauMayID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            Guid MMID;
            try
            {
                MMID = Guid.Parse(MauMayID);
            }
            catch
            {
                MMID = Guid.Empty;
            }
            try
            {
                List<Web_MauMay_GetAllDatabyMauMayIDResult> lst = db.Web_MauMay_GetAllDatabyMauMayID(MMID).ToList();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult XoaMauMay(string MauMayID)
        {
            unitofwork = new UnitOfWork();
            Guid mm = Guid.Parse(MauMayID);
            try
            {
                var maumay = new MauMay();
                unitofwork.Context.MauMays.Find(mm).IsActive = 0;
                unitofwork.Context.SaveChanges();
                unitofwork.Save();
                return Json(new { message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = "Có lỗi phát sinh trong quá trình xử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult AddOrUpdate(string data, string chitiet)
        {
            unitofwork = new UnitOfWork();
            int result = unitofwork.MauMays.AddOrUpdate(data, chitiet);
            if (result == 0)
            {
                code = "error";
                status = "Không thành công! Có lỗi xảy ra";
            }
            else
            {
                code = "success";
                status = "Thành công!";
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult XoaMotChiTietMauMay(string ChiTietMauMayID)
        {
            unitofwork = new UnitOfWork();
            Guid ctmm = Guid.Parse(ChiTietMauMayID);
            try
            {
                var chitiet = new ChiTietMauMay();
                unitofwork.Context.ChiTietMauMays.Find(ctmm).IsActive = 0;
                unitofwork.Context.SaveChanges();
                unitofwork.Save();
                return Json(new { message = "Thành công" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = "Có lỗi phát sinh trong quá trình xử lý dữ liệu" }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDatabyChiTietMauMayID(string ChiTietMauMayID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            try
            {
                var data = db.Web_MauMay_GetDatabyChiTietMauMayID(Guid.Parse(ChiTietMauMayID));
                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDataYeuCauKhachHang()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            try
            {
                var data = db.Web_MauMay_GetDataYeuCauKhachHang().Where(x => x.TinhTrang == 3).ToList();
                return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDatabyYeuCauKhachHangID(string YeuCauKhachHangID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            Guid YCKHID;
            try
            {
                YCKHID = Guid.Parse(YeuCauKhachHangID);
            }
            catch
            {
                YCKHID = Guid.Empty;
            }
            try
            {
                List<Web_MauMay_GetDatabyYeuCauKhachHangIDResult> lst = db.Web_MauMay_GetDatabyYeuCauKhachHangID(YCKHID).ToList();
                return Json(new { data = lst, total = lst.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDataPhongBan()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            try
            {
                var data = db.Web_MauMay_GetDataPhongBan();
                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getDataNhanVienThucHien(string PhongBanID)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            if(PhongBanID == "" || PhongBanID == null)
            {
                PhongBanID = Guid.Empty.ToString();
            }
            try
            {
                var data = db.Web_MauMay_GetDataNhanVienbyPhongBanID(Guid.Parse(PhongBanID));
                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult getDataNhanVienPhuTrach()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            try
            {
                var data = db.Web_MauMay_GetAllDataNhanVien();
                return Json(new { data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
