﻿using DF.DataAccess;
using DF.DataAccess.DBML;
using DF.DBMapping.ModelsExt;
using DFWeb.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DFWeb.Controllers.NghiepVu.ThanhPham
{
    [CustomAuthorize(Roles = "7001")]
    public class KhoThanhPhamDangSanXuatController : Controller
    {
        //
        // GET: /KhoThanhPhamDangSanXuat/
 
        UnitOfWork unitofwork;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetDataTonKho(string DonHangID="")
        {
            try
            {
                unitofwork = new UnitOfWork();
                var donHangID = Guid.Empty.ToString();
                if (DonHangID != "")
                {
                    donHangID = DonHangID;
                }
                var list = unitofwork.KhoThanhPhams.LayTon(donHangID).ToList();
                return Json(new { data = list, total = list.Count, code = "succes" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAllHoaDon()
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var list = db.Web_DonHang_LayDanhSachDonHangChuaHoanThien(Guid.Parse(Define.CONGTRINHTONG));
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }
    }
}
