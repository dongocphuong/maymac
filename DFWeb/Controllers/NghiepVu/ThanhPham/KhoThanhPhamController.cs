﻿using DF.DataAccess;
using DF.DataAccess.DBML;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DFWeb.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DFWeb.Controllers.NghiepVu.ThanhPham
{
    [CustomAuthorize(Roles = "7000")]
    public class KhoThanhPhamController : Controller
    {
        //
        // GET: /KhoThanhPham/
        UnitOfWork unitofwork;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetDataKhoTong()
        {
            try
            {
                unitofwork = new UnitOfWork();
                var list = unitofwork.KhoThanhPhams.LayTon(Define.CONGTRINHTONG).ToList();
                return Json(new { data = list, total = list.Count, code = "succes" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult PopupNhapKhoTong()
        {
            return PartialView("_PopupNhapKhoTong");
        }
        public ActionResult PopupXuatKhoTong(string PhieuXuatVatTuID, string LoaiXuat)
        {
            unitofwork = new UnitOfWork();
            ChiTietPhieuXuatVatTuResult model = new ChiTietPhieuXuatVatTuResult();
            if (PhieuXuatVatTuID != null && PhieuXuatVatTuID != "")
            {
                var phieuxuatid = Guid.Parse(PhieuXuatVatTuID);
                model = unitofwork.Context.PhieuXuatThanhPhams.Where(p => p.PhieuXuatThanhPhamID == phieuxuatid).ToList().Select(t => new ChiTietPhieuXuatVatTuResult()
                {
                    CongTrinhNhanID = t.DonHangNhanID,
                    PhieuXuatVatTuID = t.PhieuXuatThanhPhamID,
                    LoaiHinhXuat = t.LoaiHinh.GetValueOrDefault(),
                    NhanVienXuatID = t.NguoiTaoID,
                    ThoiGian = t.ThoiGian.GetValueOrDefault(),
                    DanhSachHinhAnh = t.DanhSachHinhAnhs
                }).SingleOrDefault();
            }
            return PartialView("_PopupXuatKhoTong", model);
        }
        public JsonResult GetAllDataDoiTac()
        {
            unitofwork = new UnitOfWork();
            var list = unitofwork.Context.DoiTacs.Select(t => new
            {
                DoiTacID = t.DoiTacID,
                TenDoiTac = t.TenDoiTac
            }).ToList();
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllHoaDon()
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var list = db.Web_DonHang_LayDanhSachDonHangChuaHoanThien(Guid.Parse(Define.CONGTRINHTONG));
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachThanhPham(string DoiTacID)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var list = db.Web_ThanhPham_LayDanhSach().ToList();
            return Json(new { data = list, total = list.Count() }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Mua(string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var kq = unitofwork.KhoThanhPhams.Mua(ThoiGian, Guid.Parse(DoiTacID), DataChiTietNhap, DSHinhAnh, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                if (kq == 1)
                {
                    return Json(new { code = "success", message = Models.Language.GetText("muathanhcong") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CapNhatMua(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh)
        {
            try
            {
                unitofwork = new UnitOfWork();
                int kq = unitofwork.KhoThanhPhams.CapNhatPhieuMua(PhieuNhapVatTuID, ThoiGian, DoiTacID, DataChiTietNhap, DSHinhAnh, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                if (kq == 1)
                {
                    return Json(new { code = "success", message = Models.Language.GetText("mgs_capnhatthanhcong") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("mgs_capnhatthatbai") }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("coloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult Xuat(string PhieuXuatVatTuID, string LoaiHinh, string CongTrinhNhanID, string DoiThiCongNhanID, string DataChiTietPhieuXuat, string NoiDung, string DSHinhanh, string ThoiGian)
        {
            var NhanVienXuatID = CookiePersister.getAcountInfo().NhanVienID;
            if (int.Parse(LoaiHinh) == 1)
            {
                NoiDung = Models.Language.GetText("xuatsudung");
            }
            else
            {
                NoiDung = Models.Language.GetText("xuatdieuchuyen");
            }
            try
            {
                unitofwork = new UnitOfWork();
                int kq = 1;
                if (PhieuXuatVatTuID == null || PhieuXuatVatTuID == "")
                {
                    kq = unitofwork.KhoThanhPhams.Xuat(LoaiHinh, Define.CONGTRINHTONG, CongTrinhNhanID, NhanVienXuatID, DataChiTietPhieuXuat, DSHinhanh, ThoiGian, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                }
                else
                {
                    kq = unitofwork.KhoThanhPhams.CapNhatXuat(PhieuXuatVatTuID, LoaiHinh, Define.CONGTRINHTONG, CongTrinhNhanID, NhanVienXuatID, DataChiTietPhieuXuat, DSHinhanh, NoiDung, ThoiGian, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                }
                if (kq == 1)
                {
                    return Json(new { code = "success", message = Models.Language.GetText("xuatvattuthanhcong") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("coloixayra") }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("coloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDataLichSuNhapKhoTong(int LoaiHinhNhap, string TuNgay = "", string DenNgay = "")
        {
            try
            {
                unitofwork = new UnitOfWork();
                var data = unitofwork.KhoThanhPhams.LichSuNhapKho(Guid.Parse(Define.CONGTRINHTONG), LoaiHinhNhap, TuNgay, DenNgay);
                return Json(new { data = data, total = data.Count, code = "success", message = Models.Language.GetText("mgs_laydulieuthanhcong") }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("laydulieukhongthanhcong") }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult PopupChiTietPhieuNhapVatTu(string PhieuNhapVatTuID, string LoaiNhap, string PhieuXuatVatTuID)
        {
            unitofwork = new UnitOfWork();
            var model = new ChiTietPhieuNhapThanhPhamResult();
            var pnvtid = Guid.Parse(PhieuNhapVatTuID);
            model = unitofwork.Context.PhieuNhapThanhPhams.Where(p => p.PhieuNhapThanhPhamID == pnvtid).ToList().Select(t => new ChiTietPhieuNhapThanhPhamResult()
            {
                PhieuNhapVatTuID = t.PhieuNhapThanhPhamID,
                DoiTacID = t.DoiTacID,
                DSHinhAnh = t.DanhSachHinhAnhs,
                ThoiGianNhap = t.ThoiGian.GetValueOrDefault(),
                PhieuXuatThanhPhamID = t.PhieuXuatThanhPhamID
            }).SingleOrDefault();
            if (model.DoiTacID == null)
            {
                var phieuxuatnl = unitofwork.Context.PhieuXuatThanhPhams.FirstOrDefault(p => p.PhieuXuatThanhPhamID == model.PhieuXuatThanhPhamID);
                model.HoaDonDieuChuyenID = phieuxuatnl.DonHangID;
            }
            return PartialView("_PopupChiTietPhieuNhap", model);
        }
        public JsonResult GetDataChiTietPhieuNhap(string PhieuNhapVatTuID)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var data = db.Web_ThanhPham_LayChiTietPhieuNhapThanhPham(Guid.Parse(PhieuNhapVatTuID)).ToList();
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult TraThanhPham(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh)
        {
            try
            {
                unitofwork = new UnitOfWork();
                int kq = unitofwork.KhoThanhPhams.TraThanhPham(PhieuNhapVatTuID, ThoiGian, DoiTacID, DataChiTietNhap, DSHinhAnh, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                if (kq == 1)
                {
                    return Json(new { code = "success", message = Models.Language.GetText("mgs_capnhatthanhcong") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("mgs_capnhatthatbai") }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("coloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CapNhatTraThanhPham(string PhieuXuatVatTuID, string ThoiGian, string DoiTacID, string DataChiTietPhieuXuat, string DSHinhAnh)
        {
            try
            {
                unitofwork = new UnitOfWork();
                int kq = unitofwork.KhoThanhPhams.CapNhatTraThanhPham(PhieuXuatVatTuID, ThoiGian, DataChiTietPhieuXuat, DSHinhAnh, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                if (kq == 1)
                {
                    return Json(new { code = "success", message = Models.Language.GetText("mgs_capnhatthanhcong") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("mgs_capnhatthatbai") }, JsonRequestBehavior.AllowGet);
                }
            }

            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("coloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetLichSuXuatKhoTong(int LoaiHinhXuat, string CongTrinhDenID, string DoiThiCongDenID, string TuNgay = "", string DenNgay = "")
        {
            unitofwork = new UnitOfWork();
            var data = unitofwork.KhoThanhPhams.LichSuXuatKho(Guid.Parse(Define.CONGTRINHTONG), LoaiHinhXuat, CongTrinhDenID, TuNgay, DenNgay);
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDataChiTietPhieuXuat(string PhieuXuatVatTuID)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var data = db.Web_ThanhPham_LayChiTietPhieuXuatThanhPham(Guid.Parse(PhieuXuatVatTuID)).ToList();
            return Json(new { data = data, total = data.Count }, JsonRequestBehavior.AllowGet);
        }

    }
}