﻿using DF.DataAccess;
using DF.DataAccess.DBML;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.NghiepVu;
using DFWeb.Common;
using DFWeb.Models.ReportModels;
using DFWeb.Security;
using FlexCel.Core;
using FlexCel.Report;
using FlexCel.XlsAdapter;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DFWeb.Controllers.NghiepVu.SanPham
{
    [CustomAuthorize(Roles = "7002")]
    public class KhoSanPhamDangSanXuatController : Controller
    {
        //
        // GET: /KhoSanPhamDangSanXuat/
 
        UnitOfWork unitofwork;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetDataTonKho(string DonHangID="")
        {
            try
            {
                unitofwork = new UnitOfWork();
                var donHangID = Guid.Empty.ToString();
                if (DonHangID != "")
                {
                    donHangID = DonHangID;
                }
                var list = unitofwork.KhoSanPhams.LayTon(donHangID).ToList();
                return Json(new { data = list, total = list.Count, code = "succes" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetAllHoaDon()
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            var list = db.Web_DonHang_LayDanhSachDonHangChuaHoanThien(Guid.Parse(Define.CONGTRINHTONG));
            return Json(new { data = list }, JsonRequestBehavior.AllowGet);
        }

        //Xuat Excel 
        public clsExcelResult ExportToExcel(string DonHangID = "")
        {
            string fileName = "";
            fileName = "KhoSanPhamDangSanXuat.xlsx";
            string urlTemp = "";
            urlTemp = "~\\TempExcel\\VatTu\\KhoSanPhamDangSanXuat.xlsx";
            clsExcelResult clsResult = new clsExcelResult();
            ExcelFile xls = CreateReport(Server.MapPath(urlTemp), DonHangID);
            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public ExcelFile CreateReport(String path, string DonHangID = "")
        {

            XlsFile Result = new XlsFile(true);
            try
            {
                Result.Open(path);
                FlexCelReport fr = new FlexCelReport();
                unitofwork = new UnitOfWork();
                var donHangID = Guid.Empty.ToString();
                if (DonHangID != "")
                {
                    donHangID = DonHangID;
                }
                var list = unitofwork.KhoSanPhams.LayTon(donHangID).ToList();
                DataTable dtCCDC = Utils.Utilities.ToDataTable(list);
                dtCCDC.TableName = "ChiTiet";
                fr.AddTable("ChiTiet", dtCCDC);
                fr.Run(Result);
                fr.Dispose();
                dtCCDC.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }

        public JsonResult GetDataLichSuNhap(string DonHangID, string TuNgay = "", string DenNgay = "")
        {
            try
            {
                unitofwork = new UnitOfWork();
                Guid dhid;
                try
                {
                    dhid = Guid.Parse(DonHangID);
                }
                catch
                {
                    dhid = Guid.Empty;
                }
                var data = unitofwork.KhoSanPhams.LichSuNhapKhoTheoDonHang(dhid, TuNgay, DenNgay);
                return Json(new { data = data, total = data.Count, code = "success", message = Models.Language.GetText("mgs_laydulieuthanhcong") }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("laydulieukhongthanhcong") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDataLichSuXuat(string DonHangID, string TuNgay = "", string DenNgay = "")
        {
            try
            {
                unitofwork = new UnitOfWork();
                Guid dhid;
                try
                {
                    dhid = Guid.Parse(DonHangID);
                }
                catch
                {
                    dhid = Guid.Empty;
                }
                var data = unitofwork.KhoSanPhams.LichSuXuatKhoTheoDonHang(dhid, TuNgay, DenNgay);
                return Json(new { data = data, total = data.Count, code = "success", message = Models.Language.GetText("mgs_laydulieuthanhcong") }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("laydulieukhongthanhcong") }, JsonRequestBehavior.AllowGet);
            }
            
        }
        public JsonResult GiaoHang(string lstSanPham,Guid DonHangXuatID, Guid DonHangID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var kq = unitofwork.KhoSanPhams.GiaoHang(lstSanPham, DonHangXuatID,DonHangID, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                if (kq !=Guid.Empty)
                {
                    return Json(new { code = "success", message = Models.Language.GetText("muathanhcong"), kq = kq }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult CapNhatGiaoHang(string lstSanPham, Guid PhieuNhanDonHangID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var kq = unitofwork.KhoSanPhams.CapNhatGiaoHang(lstSanPham, PhieuNhanDonHangID, Guid.Parse(CookiePersister.getAcountInfo().NhanVienID));
                if (kq == 1)
                {
                    return Json(new { code = "success", message = Models.Language.GetText("muathanhcong") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LayChiTietLichSuXuat(Guid PhieuNhanDonHangID)
        {
            try
            {
                var db = new DBMLDFDataContext();
                var data = db.Web_SanPham_LichSuChiTietXuat(PhieuNhanDonHangID).ToList();
                return Json(new { data = data, total = data.Count, code = "success", message = Models.Language.GetText("mgs_laydulieuthanhcong") }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { code = "error", message = Models.Language.GetText("laydulieukhongthanhcong") }, JsonRequestBehavior.AllowGet);
            }
        }
        
        
        public ActionResult InHoaDon(Guid PhieuNhanDonHangID)
        {
            unitofwork = new UnitOfWork();
            var db = new DBMLDFDataContext();
            var phieunhandonhang = unitofwork.Context.PhieuNhanDonHangs.FirstOrDefault(p => p.PhieuNhanDonHangID == PhieuNhanDonHangID);
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == phieunhandonhang.DonHangID);
            var doitac = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == donhang.DoiTacID);
            var data = db.Web_SanPham_LichSuChiTietXuat(PhieuNhanDonHangID).ToList().Select(p=> new GiaoHangItemModel{
            DonGia = p.DonGia,
            DonVi = p.TenDonVi,
            MaSanPham=p.MaSanPham,
            SoLuong=p.SoLuong,
            TenSanPham=p.TenSanPham,
            ThanhTien = p.DonGia * p.SoLuong
            }).ToList();
            var giaohangmodel = new GiaoHangModel(){
                DiaChi = doitac.DiaChi,
                item= data,
                SoTienBangChu = Functions.So_chu(data.Sum(p=>p.ThanhTien)),
                TenDoiTac =doitac.TenDoiTac
            };

            return View(giaohangmodel);
        }
    }
}
