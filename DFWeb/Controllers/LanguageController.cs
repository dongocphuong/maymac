﻿using DF.DataAccess;
using DFWeb.Models;
using DFWeb.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DFWeb.Controllers
{
    //[CustomAuthorize(Roles = "3211")]
    public class LanguageController : Controller
    {
        protected UnitOfWork unitofwork;
        string status;
        string code;
        public ActionResult Index()
        {
            ViewBag.Title_Function = "Language";
            return View();
        }

        public ActionResult read()
        {
            unitofwork = new UnitOfWork();
            var List = unitofwork.tblLanguages.getdata();
            return Json(new { data = List, total = List.Count }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult create(string data)
        {
            unitofwork = new UnitOfWork();
            int rs = unitofwork.tblLanguages.createdata(data);
            if (rs == 0)
            {
                code = "error";
                status = Models.Language.GetText("msg_themkhongthanhcong");
            }
            else
            {
                code = "success";
                status = Models.Language.GetText("mgs_themthanhcong");
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult update(string data)
        {
            unitofwork = new UnitOfWork();
            int rs = unitofwork.tblLanguages.updatedata(data);
            if (rs == 0)
            {
                code = "error";
                status = Models.Language.GetText("msg_suakhongthanhcong");
            }
            else
            {
                code = "success";
                status = Models.Language.GetText("msg_suathanhcong");
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult destroy(string data)
        {
            unitofwork = new UnitOfWork();
            int rs = unitofwork.tblLanguages.deletedata(data);
            if (rs == 0)
            {
                code = "error";
                status = Models.Language.GetText("mgs_xoakhongthanhcong");
            }
            else
            {
                code = "success";
                status = Models.Language.GetText("mgs_xoathanhcong");
            }
            return Json(new { code = code, message = status }, JsonRequestBehavior.AllowGet);
        }
    }
}
