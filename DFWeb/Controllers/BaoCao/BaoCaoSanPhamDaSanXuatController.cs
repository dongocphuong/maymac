﻿using DF.DataAccess;
using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;
using DFWeb.Common;
using DFWeb.Security;
using System.Data.SqlClient;
using FlexCel.Core;
using System.IO;
using FlexCel.XlsAdapter;
using FlexCel.Report;
using System.Transactions;
using DF.Utilities;
using DF.DataAccess.Repository;
using DFWeb.Models;
using DF.DataAccess.DBML;

namespace DFWeb.Controllers.BaoCao
{
    [CustomAuthorize(Roles="302")]
    public class BaoCaoSanPhamDaSanXuatController : Controller
    {
        //
        // GET: /BaoCaoSanPhamDaSanXuat/
        protected DBMLDFDataContext db ;
        protected UnitOfWork unitofwork;
        public ActionResult Index()
        {
            ViewBag.Title_Function = "Bao cáo sản phẩm đã sản xuất";
            return View();
        }
        public JsonResult GetList(string TuNgay, string DenNgay)
        {
            try
            {
                db = new DBMLDFDataContext();
                var tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
                var denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).Date.AddHours(23).AddMinutes(59);
                var list = db.Web_BaoCao_SanPhamDaSanXuatDuoc(tuNgay, denNgay).ToList();
                return Json(new { data = list, total = list.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
  

    }
}
