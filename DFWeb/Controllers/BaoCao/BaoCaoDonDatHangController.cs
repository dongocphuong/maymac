﻿using DF.DataAccess;
using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;
using DFWeb.Common;
using DFWeb.Security;
using System.Data.SqlClient;
using FlexCel.Core;
using System.IO;
using FlexCel.XlsAdapter;
using FlexCel.Report;
using System.Transactions;
using DF.Utilities;
using DF.DataAccess.Repository;
using DFWeb.Models;
using DF.DataAccess.DBML;
using DF.DBMapping.ModelsExt;
using DFWeb.Models.ReportModels;
using DF.DBMapping.ModelsExt.Excel;
using DF.DBMapping.ModelsExt.Excel.BaoCao;
using Newtonsoft.Json;

namespace DFWeb.Controllers.BaoCao
{
    [CustomAuthorize(Roles = "305")]
    public class BaoCaoDonDatHangController : Controller
    {
        protected DBMLDFDataContext db;
        protected UnitOfWork unitofwork;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult LayDanhSachDoiTac()
        {
            db = new DBMLDFDataContext();
            var list = db.Web_BaoCao_DanhSachDoiTacDaDatHang().ToList();
            return Json(new { data = list, total = list.Count, code = "success" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetThongTinDoiTac(Guid DoiTacID)
        {
            db = new DBMLDFDataContext();
            var list = db.Web_BaoCao_DanhSachDoiTacDaDatHang().FirstOrDefault(x => x.DoiTacID == DoiTacID);
            return Json(new { data = list, code = "success" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LayDanhSachDonHang(string DoiTacID)
        {
            unitofwork = new UnitOfWork();
            Guid dtid = Guid.Empty;
            if (DoiTacID != "")
            {
                dtid = Guid.Parse(DoiTacID);
            }
            var list = unitofwork.Context.DonHangs.Where(x => x.DoiTacID == dtid && x.IsActive == true).ToList();
            return Json(new { data = list, total = list.Count, code = "success" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllData(string DoiTacID, string TuNgay = "", string DenNgay = "", int Loai = 1, string DonHangID = "")
        {
            Guid dtid; Guid dhid;
            try
            {
                dtid = Guid.Parse(DoiTacID);
            }
            catch
            {
                dtid = Guid.Empty;
            }
            try
            {
                dhid = Guid.Parse(DonHangID);
            }
            catch
            {
                dhid = Guid.Empty;
            }
            try
            {
                unitofwork = new UnitOfWork();
                db = new DBMLDFDataContext();
                var list = db.Web_BaoCao_DonDatHangTheoNamByDoiTacID(dtid).OrderBy(t => t.Mau).ToList();
                if (Loai == 2)
                {
                    var tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
                    var denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
                    list = list.Where(p => p.ThoiGian >= tuNgay && p.ThoiGian < denNgay).ToList();
                    if (DonHangID != "")
                    {
                        list = list.Where(p => p.DonHangID == dhid).ToList();
                    }
                }
                return Json(new { data = list, total = list.Count, code = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
        //Xuất excel
        public clsExcelResult XuatExcelBaoCao(string filters, string DoiTacID, string DonHangID, string TuNgay, string DenNgay)
        {
            string fileName = "";
            fileName = "Đơn đặt hàng.xlsx";
            string urlTemp = "";
            urlTemp = "~\\TempExcel\\BaoCao\\TemplateDonDatHang.xlsx";
            clsExcelResult clsResult = new clsExcelResult();
            ExcelFile xls = CreateExcel(Server.MapPath(urlTemp), filters, DoiTacID, DonHangID, TuNgay, DenNgay);

            using (MemoryStream ms = new MemoryStream())
            {
                xls.Save(ms);
                ms.Position = 0;
                clsResult.ms = ms;
                clsResult.FileName = fileName;
                clsResult.type = "xlsx";
                return clsResult;
            }
        }
        public ExcelFile CreateExcel(String path, string filters, string DoiTacID, string DonHangID, string TuNgay, string DenNgay)
        {
            db = new DBMLDFDataContext();
            XlsFile Result = new XlsFile(true);
            Guid dtid = Guid.Parse(DoiTacID); Guid dhid;
            try
            {
                dhid = Guid.Parse(DonHangID);
            }
            catch
            {
                dhid = Guid.Empty;
            }
            try
            {

                Result.Open(path);
                FlexCelReport fr = new FlexCelReport();
                unitofwork = new UnitOfWork();
                List<FilterModel> filter = JsonConvert.DeserializeObject<List<FilterModel>>(filters);

                List<Web_BaoCao_DonDatHangTheoNamByDoiTacIDResult> lst = db.Web_BaoCao_DonDatHangTheoNamByDoiTacID(dtid).OrderBy(t => t.Mau).ToList();
                List<Web_BaoCao_DanhSachDoiTacDaDatHangResult> lstt = db.Web_BaoCao_DanhSachDoiTacDaDatHang().Where(p => p.DoiTacID == dtid).ToList();
                var stt = 1;
                var data2 = lst.Where(p => GetDateFilter(p, filter)).Select(p => new
                {
                    STT = stt++,
                    TenSanPham = p.TenSanPham,
                    DacDiem = p.DacDiem,
                    Size = p.Size,
                    Mau = p.Mau,
                    MauPhoi = p.MauPhoi,
                    SoLuong = p.SoLuong,
                    GiaBan = p.DonGia,
                    ThanhTien = p.ThanhTien,
                    YeuCauKhac = p.YeuCauKhac,
                    ThoiGian = p.ThoiGian,
                    ThoiGianGiaoHang = p.ThoiGianGiaoHang,
                    DonHangID = p.DonHangID,
                }).ToList();
                var tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
                var denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
                data2 = data2.Where(p => p.ThoiGian >= tuNgay && p.ThoiGian < denNgay).ToList();
                if (dhid != Guid.Empty)
                {
                    data2 = data2.Where(p => p.DonHangID == dhid).ToList();
                }
                DataTable dtThongTinChung = Utils.Utilities.ToDataTable(lstt);
                DataTable dtQuanLyVatTu = Utils.Utilities.ToDataTable(data2);
                dtQuanLyVatTu.TableName = "ChiTiet";
                dtThongTinChung.TableName = "ChiTietChung";
                fr.AddTable("ChiTietChung", dtThongTinChung);
                fr.AddTable("ChiTiet", dtQuanLyVatTu);
                fr.Run(Result);
                fr.Dispose();
                dtThongTinChung.Dispose();
                dtQuanLyVatTu.Dispose();
                return Result;
            }
            catch (Exception ex)
            {
                return Result;
            }
        }
        public bool GetDateFilter(Web_BaoCao_DonDatHangTheoNamByDoiTacIDResult ls, List<FilterModel> filters)
        {
            var istrue = filters.Count() <= 0;
            foreach (var filter in filters)
            {
                var value = ls.GetType().GetProperty(filter.field).GetValue(ls);
                if (value != null)
                {
                    if (filter.Operator == "contains" || filter.Operator == "eq")
                    {
                        if (removeUnicode(value.ToString().ToLower()).Contains(removeUnicode(filter.value.ToLower())))
                        {
                            istrue = true;
                            break;
                        }
                    }
                    else
                    {
                        if (value.ToString() == filter.value)
                        {
                            istrue = true;
                            break;
                        }
                    }
                }
            }
            return istrue;
        }
        public string removeUnicode(string input)
        {
            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
            "đ",
            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
            "í","ì","ỉ","ĩ","ị",
            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
            "ý","ỳ","ỷ","ỹ","ỵ",};
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "d",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",};
            for (int i = 0; i < arr1.Length; i++)
            {
                input = input.Replace(arr1[i], arr2[i]);
                input = input.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            return input;
        }
        //end xuất excel 

    }
}
