﻿using DF.DataAccess;
using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;
using DFWeb.Common;
using DFWeb.Security;
using System.Data.SqlClient;
using FlexCel.Core;
using System.IO;
using FlexCel.XlsAdapter;
using FlexCel.Report;
using System.Transactions;
using DF.Utilities;
using DF.DataAccess.Repository;
using DFWeb.Models;
using DF.DataAccess.DBML;
using DF.DBMapping.ModelsExt;

namespace DFWeb.Controllers.BaoCao
{
    [CustomAuthorize(Roles = "304")]
    public class BaoCaoTinhTrangNguyenLieuController : Controller
    {
        //
        // GET: /BaoCaoTinhTrangNguyenLieu/
        protected DBMLDFDataContext db;
        protected UnitOfWork unitofwork;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAllData()
        {
            try
            {
                unitofwork = new UnitOfWork();
                var list = unitofwork.KhoNguyenLieus.LayTon(Define.CONGTRINHTONG).ToList();
                return Json(new { data = list, total = list.Count, code = "success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { code = "error", message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
