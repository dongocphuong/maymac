﻿using DF.DataAccess;
using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;
using DFWeb.Common;
using DFWeb.Security;
using System.Data.SqlClient;
using FlexCel.Core;
using System.IO;
using FlexCel.XlsAdapter;
using FlexCel.Report;
using System.Transactions;
using DF.Utilities;
using DF.DataAccess.Repository;
using DFWeb.Models;
using DF.DataAccess.DBML;
using DF.DBMapping.ModelsExt.NghiepVu.QuanLyDoiTac;
namespace DFWeb.Controllers.BaoCao
{
    [CustomAuthorize(Roles = "303")]
    public class BaoCaoCongNoDoiTacTheoThoiGianController : Controller
    {
        //
        // GET: /BaoCaoCongNoDoiTacTheoThoiGian/
        protected DBMLDFDataContext db;
        protected UnitOfWork unitofwork;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetAllData(string TuNgay, string DenNgay)
        {
            try
            {
                db = new DBMLDFDataContext();
                var tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
                var denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).Date.AddHours(23).AddMinutes(59);
                var list = new List<DoiTacModel>();
                foreach (var p in db.Web_BaoCao_DanhSachCongNoDoiTacTheoThoiGian(tuNgay, denNgay).OrderBy(t => t.TenDoiTac).ToList())
                {
                    var dtm = new DoiTacModel();
                    dtm.DoiTacID = p.DoiTacID;
                    dtm.KieuDoiTac = p.KieuDoiTac.GetValueOrDefault();
                    dtm.TenDoiTac = p.TenDoiTac;
                    dtm.MaDoiTac = p.MaDoiTac;
                    dtm.TongThu = p.BanSanPham.GetValueOrDefault() + p.BanThanhPham.GetValueOrDefault();
                    dtm.TongChi = p.MuaNguyenLieu.GetValueOrDefault() + p.MuaThanhPham.GetValueOrDefault() + p.MuaSanPham.GetValueOrDefault();
                    dtm.ThanhToanCongNo = p.ThanhToanCongNo.GetValueOrDefault();
                    if (p.LoaiHinhThanhToan == 1)
                    {
                        dtm.ChenhLenh = (dtm.TongThu - dtm.TongChi) - dtm.ThanhToanCongNo;
                    }
                    else
                    {
                        dtm.ChenhLenh = (dtm.TongThu - dtm.TongChi) + dtm.ThanhToanCongNo;
                    }

                    list.Add(dtm);
                }
                return Json(new { data = list, total = list.Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { message = Models.Language.GetText("dacoloixayra") }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
