﻿using DF.DataAccess;
using DF.DBMapping.Models;
using DF.DBMapping.ModelsExt;
using DFSCommon;
using DFSCommon.Models.Enums;
using DFWeb.Common;
using DFWeb.Models;
using DFWeb.Security;
using DFWeb.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Webservice.FCMModules;
using System.Configuration;
using System.Transactions;
using DF.DataAccess.DBML;
using FlexCel.Core;
using DFWeb.Models.ReportModels;
using FlexCel.XlsAdapter;
using DF.Utilities;
using Excel;
using DF.DBMapping.ModelsExt.NghiepVu;

namespace DFWeb.Controllers.BaoCao
{
    [CustomAuthorize(Roles = "301")]
    public class BaoCaoTieuHaoNguyenLieuController : Controller
    {
        //
        // GET: /BaoCaoTieuHaoNguyenLieu/
        protected DBMLDFDataContext db;
        protected UnitOfWork unitofwork;
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult BaoCaoTieuHaoNguyenLieu()
        {
            db = new DBMLDFDataContext();
            var models = db.Web_BaoCao_TieuHaoNguyenLieu().ToList();
            return Json(new { data = models, total = models.Count }, JsonRequestBehavior.AllowGet);
        }
    }
}
