/* ------------------------------------------------------------------------------
*
*  # Responsive extension for Datatables
*
*  Specific JS code additions for datatable_responsive.html page
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {


    // Table setup
    // ------------------------------

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        responsive: true,
        pageLength: 25,
        dom: '<"datatable-header"fl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>'+GetTextLanguage("timkiem")+' :</span> _INPUT_',
            lengthMenu: '<span>' + GetTextLanguage("hienthi") + ':</span> _MENU_',
            zeroRecords: GetTextLanguage("khocoketqua"),
            info: "_START_ - _END_ " + GetTextLanguage("cua") + " _TOTAL_ " + GetTextLanguage("ketqua"),
            infoEmpty: GetTextLanguage("khocoketqua"),
            paginate: { 'first': GetTextLanguage("vedau"), 'last': GetTextLanguage("vecuoi"), 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });



    // Whole row as a control
    



    // External table additions
    // ------------------------------

    // Add placeholder to the datatable filter option
    
    
});
