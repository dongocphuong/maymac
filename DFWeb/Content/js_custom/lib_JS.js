function WatingLoad(controlID,opacity){
	$("#" + controlID).block({
            message: '<i class="icon-spinner9 spinner"></i>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: opacity,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'none'
            }
    });
}

function LoadingContent(controlID, url){
	$.ajax({
        url: url,
        dataType: 'html',
		beforeSend: function() {
			WatingLoad(controlID,1);
		},
        success: function(data) {
            $("#" + controlID).html(data);
			$("#" + controlID).unblock();
        },
		error: function() { 
			$("#" + controlID).unblock();
			showToast("error", "title", "message")
		},
		complete: function() {
			$("#" + controlID).unblock();
		},
    });
}