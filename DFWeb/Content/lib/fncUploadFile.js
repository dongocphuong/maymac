﻿

function uploadFile(urlUpload, idControl, isCurrent) {
    var filesToBeUploaded = document.getElementById(idControl);
    var dataimg = new FormData();
    dataimg.append("uploads", filesToBeUploaded.files[0]);
    var objXhr = new XMLHttpRequest();

    objXhr.open("POST", urlUpload, false);
    objXhr.send(dataimg);
    return objXhr.responseText
}

function uploadFile2(urlUpload, filesToBeUploaded, isCurrent) {
    var dataimg = new FormData();
    dataimg.append("uploads", filesToBeUploaded.files[0]);
    var objXhr = new XMLHttpRequest();

    objXhr.open("POST", urlUpload, false);
    objXhr.send(dataimg);
    return objXhr.responseText
}
function uploadMultipleFile(urlUpload, idControl, isCurrent) {
    var filesToBeUploaded = document.getElementById(idControl);
    var dataimg = new FormData();
    var ins = document.getElementById(idControl).files.length;
    for (var x = 0; x < ins; x++) {
        dataimg.append("uploads", filesToBeUploaded.files[x]);
    }
    var objXhr = new XMLHttpRequest();
    objXhr.upload.addEventListener("progress", function (evt) {
        if (evt.lengthComputable) {
            var percentComplete = evt.loaded / evt.total;
            //Do something with upload progress
            console.log(percentComplete);
        }
    }, false);
    objXhr.open("POST", urlUpload, false);
    objXhr.send(dataimg);
    return objXhr.responseText
}

function uploadMultipleFileParagam(urlUpload, idControl, paragam) {
    ContentWatingOP("content-p", 0.9);
    var filesToBeUploaded = $("#" + idControl)[0];
    var dataimg = new FormData();
    var ins = $("#" + idControl)[0].files.length;
    for (var x = 0; x < ins; x++) {
        $("img", paragam).each(function (index, elememt) {
            if (filesToBeUploaded.files[x].name.toLowerCase().trim() == $(elememt).data("file").toLowerCase().trim()) {
                dataimg.append("uploads", filesToBeUploaded.files[x]);
            }
        })

    }
    var objXhr = new XMLHttpRequest();
    objXhr.open("POST", urlUpload, false);
    objXhr.send(dataimg);
    return objXhr.responseText

}

function uploadMultipleFileParagamWidthFile(urlUpload, files, paragam) {
    var dataimg = new FormData();
    var ins = files.length;
    for (var x = 0; x < ins; x++) {
        $("img", paragam).each(function (index, elememt) {
            if (files[x].name.toLowerCase().trim() == $(elememt).data("file").toLowerCase().trim()) {
                dataimg.append("uploads", files[x]);
            }
        })

    }
    var objXhr = new XMLHttpRequest();
    objXhr.open("POST", urlUpload, false);
    objXhr.send(dataimg);
    return objXhr.responseText

}
function uploadAvatarFileParagamWidthFile(urlUpload, files, paragam) {
    var dataimg = new FormData();
    var ins = files.length;
    dataimg.append("uploads", files[0]);
    var objXhr = new XMLHttpRequest();
    objXhr.open("POST", urlUpload, false);
    objXhr.send(dataimg);
    return objXhr.responseText

}
function uploadMultipleFileParagamAsync(urlUpload, idControl, paragam) {
    var filesToBeUploaded = $("#" + idControl)[0];
    var dataimg = new FormData();
    var ins = $("#" + idControl)[0].files.length;
    for (var x = 0; x < ins; x++) {
        $("img", paragam).each(function (index, elememt) {
            if (filesToBeUploaded.files[x].name.toLowerCase().trim() == $(elememt).data("file").toLowerCase().trim()) {
                dataimg.append("uploads", filesToBeUploaded.files[x]);
            }
        })

    }
    var objXhr = new XMLHttpRequest();
    objXhr.onreadystatechange = function () {
        if (objXhr.readyState == 4) {
            return objXhr.responseText;
        } else {
            return objXhr.responseText;
        }
    }
    objXhr.open("POST", urlUpload, true);
    objXhr.send(dataimg);
}

function uploadMultipleFileParagam2(urlUpload, idControl, paragam) {
    var filesToBeUploaded = document.getElementById(idControl);
    var dataimg = new FormData();
    var ins = document.getElementById(idControl).files.length;
    for (var x = 0; x < ins; x++) {
        $(".closeGenImgSuaChua", paragam).each(function (index, elememt) {
            if (filesToBeUploaded.files[x].name.toLowerCase().trim() == $(elememt).data("file").toLowerCase().trim()) {
                dataimg.append("uploads", filesToBeUploaded.files[x]);
            }
        })

    }
    var objXhr = new XMLHttpRequest();

    objXhr.open("POST", urlUpload, false);
    objXhr.send(dataimg);
    return objXhr.responseText
}
function uploadMultipleFile2(urlUpload, idControl, isCurrent) {
    var filesToBeUploaded = idControl;
    var dataimg = new FormData();
    var ins = filesToBeUploaded.files.length;
    for (var x = 0; x < ins; x++) {
        dataimg.append("uploads", filesToBeUploaded.files[x]);
    }
    var objXhr = new XMLHttpRequest();

    objXhr.open("POST", urlUpload, false);
    objXhr.send(dataimg);
    return objXhr.responseText
}

function uploadMultipleFileVatTu(urlUpload, idControl, isCurrent) {
    var dataimg = new FormData();
    for (var x = 0; x < idControl.length; x++) {
        dataimg.append("uploads", idControl[x]);
    }
    var objXhr = new XMLHttpRequest();

    objXhr.open("POST", urlUpload, false);
    objXhr.send(dataimg);
    return objXhr.responseText
}
