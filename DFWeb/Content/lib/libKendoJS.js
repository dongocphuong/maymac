﻿
//autoNumeric
const autoNumericOptionsInt = {
    digitGroupSeparator: ',',
    decimalCharacter: '.',
    minimumValue: '0',
    maximumValue: '999999999999999',
};
const autoNumericOptionsSoNgayDonGia = {
    digitGroupSeparator: ',',
    decimalCharacter: '.',
    minimumValue: '1',
    maximumValue: '31',
};
const autoNumericOptionsDouble = {
    digitGroupSeparator: ',',
    decimalCharacter: '.',
    minimumValue: '0.00',
    maximumValue: '999999999999999.00',
};
const autoNumericMoneyKendo = {
    digitGroupSeparator: '.',
    decimalCharacter: ',',
    minimumValue: '0',
    maximumValue: '999999999999999',
};
const autoNumericTileKendo = {
    digitGroupSeparator: '.',
    decimalCharacter: ',',
    minimumValue: '0.00',
    maximumValue: '1.00',
};
const autoNumericOptionsDinhMuc = {
    digitGroupSeparator: ',',
    decimalCharacter: '.',
    minimumValue: '0.00',
    maximumValue: '999999999999999.00',
};
const autoNumericOptionsKhoan = {
    digitGroupSeparator: ',',
    decimalCharacter: '.',
    minimumValue: '0.00',
    maximumValue: '999999999999999.00',
};
const autoNumericOptionsPhep = {
    digitGroupSeparator: ',',
    decimalCharacter: '.',
    minimumValue: '0',
    maximumValue: '20',
};
const autoNumericOptionsKyHopDong = {
    digitGroupSeparator: ',',
    decimalCharacter: '.',
    minimumValue: '-999999999999999.00',
    maximumValue: '999999999999999.00',
};
const autoNumericOptionsMoney = {
    digitGroupSeparator: ',',
    decimalCharacter: '.',
    minimumValue: '0.00',
    maximumValue: '999999999999999.00',
};
const autoNumericOptionsSoLuong = {
    digitGroupSeparator: ',',
    decimalCharacter: '.',
    minimumValue: '0.00',
    maximumValue: '999999999999999.00',
};
const autoNumericOptionsSoNgayGiaoHangDuKien = {
    digitGroupSeparator: ',',
    decimalCharacter: '.',
    minimumValue: '-1',
    maximumValue: '999999999999999',
};
function formatMoney(so) {
    nStr = so + '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    //x2 = "" + (x2 / 1).toFixed(0);
    //x2 = x2.substring(1, 10);
    return x1 + x2;
}
function formatSo(so) { /* dinh dang so */
    nStr = so + '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    x2 = "" + (x2 / 1).toFixed(0);
    x2 = x2.substring(1, 10);

    return x1 + x2;
}
var defaultsongaytinhdongia = 22;

function formatDouble(so) { /* dinh dang so */
    nStr = so + '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? ',' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    x2 = "" + (x2 / 1).toFixed(3).replace(".", ",");;
    x2 = x2.substring(1, 10);

    return x1 + x2;
}
var heightGrid = window.innerHeight - 90;
var pageSize = 30;
var FilterInColumn = {
    cell: {
        showOperators: false,
        //operator: "contains",
    }
};
var FilterInTextColumn = {
    cell: {
        showOperators: false,
        operator: "contains",
        suggestionOperator: "contains"
    }
};
var messagegrid = {
    display: "{0} - {1} " + GetTextLanguage("cua") + " {2}",
    empty: GetTextLanguage("khocoketqua"),
    page: GetTextLanguage("trang"),
    of: GetTextLanguage("cua") + " {0}",
    itemsPerPage: GetTextLanguage("ketqua_trang"),
    first: GetTextLanguage("vedau"),
    previous: GetTextLanguage("trangtruoc"),
    next: GetTextLanguage("tieptheo"),
    last: GetTextLanguage("vecuoi"),
    refresh: GetTextLanguage("lammoi"),
    allPages: GetTextLanguage("tatca")
}
var messagegridNonePaging = {
    display: GetTextLanguage("tong") + " {2} " + GetTextLanguage("ketqua"),
    empty: GetTextLanguage("khocoketqua"),
    page: GetTextLanguage("trang"),
    of: GetTextLanguage("cua") + " {0}",
    itemsPerPage: GetTextLanguage("ketqua_trang"),
    first: GetTextLanguage("vedau"),
    previous: GetTextLanguage("trangtruoc"),
    next: GetTextLanguage("tieptheo"),
    last: GetTextLanguage("vecuoi"),
    refresh: GetTextLanguage("lammoi"),
    allPages: GetTextLanguage("tatca")
}
var pageableAll = {
    refresh: true,
    buttonCount: 5,
    pageSizes: true,
    messages: messagegrid
};
var editablePopup = {
    mode: "popup",
    window: {
        title: GetTextLanguage("thongtin")
    },
    confirmation: GetTextLanguage("xoabangi"),
    confirmDelete: "Yes",
}
var editableInline = {
    mode: "inline",
    confirmation: GetTextLanguage("xoabangi"),
    confirmDelete: "Yes",
}
function formatToMoney(nameColumn) {
    return "#=kendo.toString(" + nameColumn + ", 'n0')#"
}
function formatToDouble(nameColumn) {
    return "#=kendo.toString(" + nameColumn + ", 'n2')#"
}
function formatToNumber(nameColumn) {
    return "#=kendo.toString(" + nameColumn + ", 'n0')#"
}
function formatToDate(nameColumn) {
    return "#=" + nameColumn + " == null ? '' : kendo.toString(kendo.parseDate(" + nameColumn + ", 'dd/MM/yyyy'), 'dd/MM/yyyy')#"
}
function formatToDateHaveClass(nameColumn) {
    return "<span class='dateneed'>#=" + nameColumn + " == null ? '' : kendo.toString(kendo.parseDate(" + nameColumn + ", 'dd/MM/yyyy'), 'dd/MM/yyyy')#</span>"
}
function formatToDateTime(nameColumn) {
    return "#=" + nameColumn + " == null ? '' : kendo.toString(kendo.parseDate(" + nameColumn + ", 'dd/MM/yyyy HH:mm'), 'dd/MM/yyyy HH:mm')#"
}

function formatSo(so) { /* dinh dang so */

    nStr = so + '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    x2 = "" + (x2 / 1).toFixed(2);
    x2 = x2.substring(1, 10);

    return x1 + x2;
}


function CheckNumeric() {

    return event.keyCode >= 46 && event.keyCode <= 57;
}
//function TaoDropDownList(Element, url, dataTextField, dataValueField, dataOptionLabel) {
//    $(Element).kendoDropDownList({
//        dataSource = new kendo.data.DataSource({
//            transport: {
//                read: {
//                    url: url,
//                    dataType: "json",
//                    type: "get"
//                }
//            },
//            schema: {
//                type: "json",
//                data: "data"
//            }
//        }),
//        dataTextField: dataTextField,
//        dataValueField: dataValueField,
//        dataOptionLabel: dataOptionLabel,
//        filter: "contains"
//    });
//}

function getDataForDropdownlist(url) {
    var dataSourceObj;
    dataSourceObj = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json",
                type: "get"
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    });
    return dataSourceObj;
}

function getDataForDropdownlistWithParam(url, data) {
    var dataSourceObj;
    dataSourceObj = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json",
                type: "get",
                data: data
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    });
    return dataSourceObj;
}

var editorDatePicker = function (container, column) {
    var input = $("<input id=" + column + " name=" + column + " class='k-input'>");
    input.appendTo(container);
    input.kendoDatePicker({
        format: "dd/MM/yyyy",
        value: null
    });
}
var editorDatePickerUseDateNow = function (container, column) {
    var input = $("<input id=" + column + " name=" + column + " class='k-input'>");
    input.appendTo(container);
    input.kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date()
    });
}

var editorDateTimePicker = function (container, column) {
    var input = $("<input id=" + column + " name=" + column + " class='k-input'>");
    input.appendTo(container);
    input.kendoDateTimePicker({
        format: "dd/MM/yyyy HH:mm",
        value: null
    });
}

var editorDateTimePickerUseTimeNow = function (container, column) {
    var input = $("<input id='" + column + "' name='" + column + "' class='k-input'>");
    input.appendTo(container);
    input.kendoDateTimePicker({
        format: "dd/MM/yyyy HH:mm",
        value: new Date()
    });
}
var editorDatePickerBatDau = function (container, column) {
    var input = $("<input id='BatDau1' name='BatDau1' class='k-input' data-bind='value:BatDau' />");
    input.appendTo(container);
    input.kendoDatePicker({
        format: "dd/MM/yyyy",
    });
    if (column.model.NgayLonNhat != undefined) {
        $("#BatDau1").data("kendoDatePicker").min(column.model.NgayLonNhat);
    }
    if (column.model.KetThuc != undefined) {
        $("#BatDau1").data("kendoDatePicker").max(column.model.KetThuc);
    }
    container.find(".k-datepicker").removeClass("k-input");
}
var editorDatePickerKetThuc = function (container, column) {
    var input = $("<input id='KetThuc1' name='KetThuc1' class='k-input' data-bind='value:KetThuc' />");
    input.appendTo(container);
    input.kendoDatePicker({
        format: "dd/MM/yyyy",
    });
    if (column.model.BatDau != undefined) {
        $('#KetThuc1').data("kendoDatePicker").min(column.model.BatDau);
    } else if (column.model.KetThuc != undefined) {
        $('#KetThuc1').data("kendoDatePicker").min(column.model.KetThuc);
    }
    container.find(".k-datepicker").removeClass("k-input");
}
function gridReload(grid, param) {
    $('#' + grid).data('kendoGrid').dataSource.read(param);
}
function altReturn(data) {
    if (data.code == "success") {
        showToast("success", GetTextLanguage("thanhcong"), data.message);
    }
    else if (data.code == "warning") {
        showToast("warning", GetTextLanguage("canhbao"), data.message);
    }
    else {
        showToast("error", GetTextLanguage("thatbai"), data.message);
    }
}
function closeModel(idform) {
    $('#' + idform).data("kendoWindow").close();
}

function destroyModel(idform) {
    $('#' + idform).data("kendoWindow").destroy();
}
function CreateModalFullScreen(TenIDModal, TenIDTemplate, TieuDeModal) {
    $("#" + TenIDModal + "").kendoWindow({
        title: TieuDeModal,
        modal: true,
        visible: false,
        resizable: false,
        draggable: false,
        actions: [
            "Close"
        ],
        deactivate: function () {
            $(this.element).empty();
        }
    });
    var kendoWindow = $("#" + TenIDModal + "").data("kendoWindow");
    if (TieuDeModal != null && TieuDeModal != "") {
        kendoWindow.title(TieuDeModal);
    }
    if (TenIDTemplate != null && TenIDTemplate != "") {
        kendoWindow.content(kendo.template($("#" + TenIDTemplate + "").html()));
    }
    kendoWindow.open();
    kendoWindow.center().maximize();
};
function CreateModalWithSize(TenIDModal, Width, Height, TenIDTemplate, TieuDeModal) {
    $("#" + TenIDModal + "").kendoWindow({
        title: TieuDeModal,
        modal: true,
        width: Width,
        height: Height,
        visible: false,
        resizable: false,
        draggable: false,
        actions: [
            "Close"
        ],
        deactivate: function () {
            $(this.element).empty();
        }
    });
    var kendoWindow = $("#" + TenIDModal + "").data("kendoWindow");
    if (TieuDeModal != null && TieuDeModal != "") {
        kendoWindow.title(TieuDeModal);
    }
    if (TenIDTemplate != null && TenIDTemplate != "") {
        kendoWindow.content(kendo.template($("#" + TenIDTemplate + "").html()));
    }
    kendoWindow.open();
    kendoWindow.center();
};
function CreateModalAutoSize(TenIDModal, TenIDTemplate, TieuDeModal) {
    $("#" + TenIDModal + "").kendoWindow({
        modal: true,
        visible: false,
        resizable: false,
        draggable: false,
        actions: [
            "Close"
        ],
        activate: function () {
        },
        deactivate: function () {
            $(this.element).empty();
        }
    });
    var kendoWindow = $("#" + TenIDModal + "").data("kendoWindow");
    if (TieuDeModal != null && TieuDeModal != "") {
        kendoWindow.title(TieuDeModal);
    }
    if (TenIDTemplate != null && TenIDTemplate != "") {
        kendoWindow.content(kendo.template($("#" + TenIDTemplate + "").html()));
    }
    kendoWindow.open();
    kendoWindow.center();
};
function TaoModalAutoSize(TenIDModal, TenIDTemplate, TieuDeModal) {
    $("#" + TenIDModal + "").kendoWindow({
        modal: true,
        visible: false,
        resizable: false,
        actions: [
            "Close"
        ],
        activate: function () {
        },
        deactivate: function () {
            $(this.element).empty();
        }
    });
    var kendoWindow = $("#" + TenIDModal + "").data("kendoWindow");
    if (TieuDeModal != null && TieuDeModal != "") {
        kendoWindow.title(TieuDeModal);
    }
    if (TenIDTemplate != null && TenIDTemplate != "") {
        kendoWindow.content(kendo.template($("#" + TenIDTemplate + "").html()));
    }

    kendoWindow.open();
    kendoWindow.center();
};
function getDataInRowClick(GridID, row) {
    return $('#' + GridID + '').data("kendoGrid").dataItem($(row).closest("tr"));
}
function alertToastrRightBottom(result) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    if (result.code === "success") {
        toastr.success(result.message);
    }
    else if (result.code === "warning") {
        toastr.warning(result.message);
    }
    else if (result.code === "error") {
        toastr.error(result.message);
    }
    else {
        toastr.info(result.message);
    }
}
function alertToastr(result) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    if (result.code === "success") {
        if (result.message != null) toastr.success(result.message);
        else toastr.success("Thành Công!");
    }
    else if (result.code === "warning") {
        toastr.warning(result.message);
    }
    else if (result.code === "error") {
        if (result.message != null) toastr.error(result.message);
        else toastr.error("Thất Bại!");
    }
    else {
        toastr.info(result.message);
    }
}
function alertToastrMessage(code, message) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    if (code === "success") {
        toastr.success(message);
    }
    else if (code === "warning") {
        toastr.warning(message);
    }
    else if (code === "error") {
        toastr.error(message);
    }
    else {
        toastr.info(message);
    }
}
function getDataItemClick(GridElement) {
    $(GridElement).data("kendoGrid").dataItem($(this).closest("tr"));
}
function getDataItemDetailClick(IDGrid) {
    $(this).data("kendoGrid").dataItem($(this).closest("tr"));
}

function GroupDateFilterWithCongTrinhID(IDTuNgay, IDDenNgay, func, IDCongTrinh) {
    $("#" + IDTuNgay).kendoDatePicker({
        format: "dd/MM/yyyy",
        open: function (e) {
            if ($("#" + IDDenNgay).data("kendoDatePicker").value() != null) {
                $("#" + IDTuNgay).data("kendoDatePicker").max($("#" + IDDenNgay).data("kendoDatePicker").value());
            }
        },
        change: function (e) {
            if (func != null && func != "") {
                if ($("#" + IDDenNgay).data("kendoDatePicker").value() != null) {
                    var data = {
                        tungay: $("#" + IDTuNgay).data("kendoDatePicker").value(),
                        denngay: $("#" + IDDenNgay).data("kendoDatePicker").value(),
                        congtrinhid: IDCongTrinh
                    }
                    func(data);
                }
            }
        }
    });
    $("#" + IDDenNgay).kendoDatePicker({
        format: "dd/MM/yyyy",
        open: function (e) {
            if ($("#" + IDTuNgay).data("kendoDatePicker").value() != null) {
                $("#" + IDDenNgay).data("kendoDatePicker").min($("#" + IDTuNgay).data("kendoDatePicker").value());
            }
        },
        change: function (e) {
            if (func != null && func != "") {
                if ($("#" + IDTuNgay).data("kendoDatePicker").value() != null) {
                    var data = {
                        tungay: $("#" + IDTuNgay).data("kendoDatePicker").value(),
                        denngay: $("#" + IDDenNgay).data("kendoDatePicker").value(),
                        congtrinhid: IDCongTrinh
                    }
                    func(data);
                }
            }
        }
    });
}

var alignRight = {
    style: "text-align:right"
};
var alignRightRed = {
    style: "text-align:right;color:red",
};
var alignCenter = {
    style: "text-align:center",
};
var alignLeft = {
    style: "text-align:left;"
}

function initMap(lat, lng, ElementID) {
    var uluru = new google.maps.LatLng(lat, lng);
    var map = new google.maps.Map(document.getElementById(ElementID), {
        zoom: 17,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
}
function showToast(loai, title, message) {
    toastr.options.positionClass = "toast-bottom-right";
    toastr.options.closeButton = true;
    toastr[loai](message, title, { timeOut: 2000 });
}
var datatablelanguage = {
    sEmptyTable: GetTextLanguage("kocodulieu"),
    sSearch: GetTextLanguage("timkiem"),
    sinfo: GetTextLanguage("hienthi") + " _PAGE_ / _PAGES_",
    info: GetTextLanguage("hienthi") + " _PAGE_ / _PAGES_",
    sLengthMenu: GetTextLanguage("hienthi") + " _MENU_ ",
    sInfoEmpty: "",
    oPaginate: {
        sFirst: GetTextLanguage("vedau"),
        sPrevious: GetTextLanguage("trangtruoc"),
        sNext: GetTextLanguage("tieptheo"),
        sLast: GetTextLanguage("vecuoi")
    },
}
var located = {
    "format": "dd/MM/YYYY",
    "monthNames": [
         GetTextLanguage("thang1") + " - ",
                GetTextLanguage("thang2"),
                GetTextLanguage("thang3"),
                GetTextLanguage("thang4"),
                GetTextLanguage("thang5"),
                GetTextLanguage("thang6"),
                GetTextLanguage("thang7"),
                GetTextLanguage("thang8"),
                GetTextLanguage("thang9"),
                GetTextLanguage("thang10"),
                GetTextLanguage("thang11"),
                GetTextLanguage("thang12"),
    ],
    "firstDay": 1
}
function getDataSourceGridSum(crudServiceBaseUrl, url, param, groups, columnsum) {
    var dataSources = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + url,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: param
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 20,
        group: groups,
        sort: [{ field: "TenVatTu", dir: "asc" }],
        schema: {
            data: "data",
            total: "total"
        },
        aggregate: columnsum,
    });

    return dataSources;
}
function getDataDroplitwithParam(crudServiceBaseUrl, url, param) {
    var dataSourceObj;
    dataSourceObj = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + url,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: param
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    });
    return dataSourceObj;
}

function getDataDroplit(crudServiceBaseUrl, url) {
    var dataSourceObj;
    dataSourceObj = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + url,
                dataType: "json",
                type: "get"
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    });
    return dataSourceObj;
}
function getDataSourceDropDownList(url) {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json",
                type: "get"
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    });
    return dataSource;
}
function getDataSourceDropDownListwithParam(url, param) {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: param
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    });
    return dataSource;
}
var footerSum = "<div style='text-align:right;'>#= kendo.toString(sum,'n2')#</div>";

function createColoredPushpin(location, color, callback) {
    var img = new Image();

    img.onload = function () {
        var c = document.createElement('canvas');
        c.width = img.width;
        c.height = img.height;
        var context = c.getContext('2d');
        //Draw a colored circle behind the pin
        context.beginPath();
        //Draw the pushpin icon
        context.drawImage(img, 0, 0);
        var pin = new Microsoft.Maps.Pushpin(location, {
            //Generate a base64 image URL from the canvas.
            icon: c.toDataURL(),
            anchor: new Microsoft.Maps.Point(12, 39)
        });
        if (callback) {
            callback(pin);
        }
    };
    img.setAttribute('crossOrigin', 'anonymous');
    img.src = 'http://sunward.dfsglotech.com/center/Upload/red.png';
}


function loadBingMap(Lat, Long, IdControl) {
    var obj = {
        altitude: 0,
        altitudeReference: -1,
        latitude: Lat,
        longitude: Long
    }
    var map = new Microsoft.Maps.Map(IdControl, {
        credentials: YourBingMapsKey
    });

    createColoredPushpin(obj, 'red', function (pin) {
        map.entities.push(pin);
    });
}

function loadGoogleMap(Lat, Long, IdControl) {
    var myLatlng = new google.maps.LatLng(Lat, Long);
    var mapOptions = {
        zoom: 17,
        center: myLatlng
    }
    var map = new google.maps.Map(IdControl);

    var marker = new google.maps.Marker({
        position: myLatlng
    });

    // To add the marker to the map, call setMap();
    marker.setMap(map);
}

function setTitleWindow(id, title) {
    $('#' + id + '_wnd_title').html(title);
}
function resizeWindow3(content, wrap) {
    $("#" + content).css("height", $("#" + wrap).height() - 5);
    $(window).resize(function () {
        setTimeout(function () {
            $("#" + content).css("height", $("#" + wrap).height());
            $("#" + content + " .k-grid-content").css("height", $("#" + wrap).height() - 158);
        }, 300);
        //setTimeout(function () {  }, 300);
    });
}
function readURLFile(file, display) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var name = event.target.result;
        var fileName = file.name;
        if (fileName.match(/\.(jpg|jpeg|png|gif)$/)) {
            var html = "<div class='img-responsive media-preview imgslider'><a rel='gallery' href='" + name + "'><img src='" + name + "' data-src='" + name + "'  class='img-responsive img-rounded media-preview'  data-file='" + fileName + "' alt='' /></a><a href='javascript:void(0)' data-file='" + fileName + "' class='icross closeImgLoad'><i class='icon-cross3'></i></a><a class='download' href='" + name + "' download>" + GetTextLanguage("taixuong") + "</a></div>";
        } else {
            var html = "<div class='img-responsive media-preview imgslider'><a rel='gallery' href='" + name + "'><img src='/content/assets/images/file.png' data-src='" + name + "'  class='img-responsive img-rounded media-preview'  data-file='" + fileName + "' alt='' /></a><a href='javascript:void(0)' data-file='" + fileName + "' class='icross closeImgLoad'><i class='icon-cross3'></i></a><a class='download' href='" + name + "' download>" + GetTextLanguage("taixuong") + "</a></div>";
        }
        $('#' + display).append(html);
        showslideimg();
    }
    reader.readAsDataURL(file);
}
function readURLFile2(file, display) {
    var reader = new FileReader();
    reader.onload = function (event) {
        var name = event.target.result;
        var fileName = file.name;
        var htmls = "";
        if (fileName.match(/\.(jpg|jpeg|png|gif)$/)) {
            htmls += '<tr class="imgslider">';
            htmls += '<td class="hidden">';
            htmls += '<a rel="gallery" href="' + name + '">';
            htmls += '<img src="' + name + '" style="width:70px;height:70px" data-src="' + name + '" class="img-responsive img-rounded media-preview" alt="" data-file="' + fileName + '" />';
            htmls += '</a>'
            htmls += '<td style="width:150px" class="processbar"></td>'
            htmls += '<td>' + fileName + '</td>'
            htmls += '<td><a href="' + name + '" data-file="' + fileName + '" download>' + GetTextLanguage("taixuong") + '</a></td>';
            htmls += '<td><button class="closeImgLoad btn btn-danger" data-file="' + fileName + '">' + GetTextLanguage("xoa") + '</a></td>'
            htmls += '</tr>';
        } else {

            htmls += '<tr class="imgslider">';
            htmls += '<td class="hidden">';
            htmls += '<a rel="gallery" href="' + name + '">';
            htmls += '<img src="/content/assets/images/file.png" style="width:70px;height:70px" data-src="' + name + '" class="img-responsive img-rounded media-preview" alt="" data-file="' + fileName + '" />';
            htmls += '</a>'
            htmls += '<td style="width:150px" class="processbar"></td>'
            htmls += '<td>' + fileName + '</td>'
            htmls += '<td><a href="' + name + '" data-file="' + fileName + '" download>' + GetTextLanguage("taixuong") + '</a></td>';
            htmls += '<td><button class="closeImgLoad btn btn-danger" data-file="' + fileName + '">' + GetTextLanguage("xoa") + '</a></td>'
            htmls += '</tr>';
        }
        $('#' + display).append(htmls);
        showslideimg5($('#' + display));
    }
    reader.readAsDataURL(file);
}
function loadImage(input, display) {
    if (input.files) {

        var filesAmount = input.files.length;
        $('#' + display).find("img").data("src");
        $('#' + display + " img").each(function (index, element) {
            if ($(element).data("src").toLowerCase().trim().indexOf("data") >= 0) {
                $(element).closest(".imgslider").remove();
            }
        });

        for (var i = 0; i < filesAmount; i++) {
            readURLFile(input.files[i], display);
        }

        $("#lstImageDisplay").on("click", ".closeImgLoad", function () {
            var x = confirm(GetTextLanguage("cochacchanxoa"));
            if (x) {
                $(this).parent().remove();
            }
        });
    }
}
function loadImage_new(input, display) {
    if (input.files) {
        var filesAmount = input.files.length;
        for (var i = 0; i < filesAmount; i++) {
            readURLFile(input.files[i], display);
        }

        //$("#lstImageDisplay").on("click", ".closeImgLoad", function () {
        //    $(this).parent().remove();
        //});
    }
}
function loadImage_new2(input, display) {
    if (input.files) {
        var filesAmount = input.files.length;
        for (var i = 0; i < filesAmount; i++) {
            readURLFile2(input.files[i], display);
        }

        //$("#lstImageDisplay").on("click", ".closeImgLoad", function () {
        //    $(this).parent().remove();
        //});
    }
}
function checkextFile(name, name2) {
    var strReturn = name;
    switch (name2.split('.').pop()) {
        case "txt": strReturn = "/Images/txt-icon.png"; break;
        case "doc": strReturn = "/Images/word-icon.png"; break;
        case "pdf": strReturn = "/Images/pdf-icon.png"; break;
        case "xls": strReturn = "/Images/excel-icon.png"; break;
        case "xlsx": strReturn = "/Images/excel-icon.png"; break;
        default: strReturn = name;
    }
    return strReturn;
}

function checkImages() {
    $("img").each(function () {
        $(this).error(function () {
            $(this).attr("src", "/Images/Error-404Window-icon.png");
        });
    });
};

$(".lst-modal").on("click", function () {
    var strTitle_Modal = "";
    var strUrl = "";
    var param = {};
    var sorting = "";
    var column;
    var value = $(this).data('val');
    var changefor = $(this).data('changefor');
    var input = $(this).data('input');
    switch ($(this).data('type')) {
        case "CongTrinh":
            strTitle_Modal = GetTextLanguage("danhsachcongtrinh");
            strUrl = "/Home/DSCongTrinh";
            if ("all" in $(this).data()) {
                if ($(this).data('all')) {
                    strUrl = "/Home/DSCongTrinhAll"
                }
            }
            sorting = "TenCongTrinh";
            column = [{
                field: "TenCongTrinh", title: GetTextLanguage("tencongtrinh"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            },
            {
                field: "ChuDauTu", title: GetTextLanguage("chudautu"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            },
            {
                field: "DiaDiem", title: GetTextLanguage("diadiem"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            }];
            break;
        case "CongTrinhTong":
            strTitle_Modal = GetTextLanguage("danhsachcongtrinh");
            strUrl = "/Home/DSCongTrinhCoCongTrinhTong";
            if ("all" in $(this).data()) {
                if ($(this).data('all')) {
                    strUrl = "/Home/DSCongTrinhAll"
                }
            }
            sorting = "TenCongTrinh";
            column = [{
                field: "TenCongTrinh", title: GetTextLanguage("tencongtrinh"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            },
            {
                field: "ChuDauTu", title: GetTextLanguage("chudautu"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            },
            {
                field: "DiaDiem", title: GetTextLanguage("diadiem"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            }];
            break;
        case "PhongBan":
            strTitle_Modal = GetTextLanguage("phongban");
            strUrl = "/Home/DSPhongBan";
            param.CongTrinhID = "";
            param.DoiThiCongID = "";
            sorting = "TenCongTrinh";
            column = [{
                field: "TenCongTrinh", title: GetTextLanguage("tenphongban"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            }];
            break;
        case "VatTu":
            strTitle_Modal = GetTextLanguage("danhmucvattu");
            strUrl = "/Home/DSVatTu";
            param.CongTrinhID = $(this).data('refer').CongTrinh == "" ? "" : $("#" + $(this).data('refer').CongTrinh).val();
            param.DoiThiCongID = $(this).data('refer').DoiThiCong == "" ? "" : $("#" + $(this).data('refer').DoiThiCong).val();
            sorting = "TenVatTu";
            column = [{
                field: "TenVatTu", title: GetTextLanguage("tenvattu"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            }, {
                field: "MaVatTu", title: GetTextLanguage("mavatu"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            },
            {
                field: "DonGia", title: GetTextLanguage("dongia"), format: "{0:n2}",
                attributes: { style: "text-align:right" }
            }];
            break;
        case "CCDC":
            strTitle_Modal = GetTextLanguage("ccdc");
            strUrl = "/Home/DSCCDC";
            param.CongTrinhID = $(this).data('refer').CongTrinh == "" ? "" : $("#" + $(this).data('refer').CongTrinh).val();
            param.DoiThiCongID = $(this).data('refer').DoiThiCong == "" ? "" : $("#" + $(this).data('refer').DoiThiCong).val();
            sorting = "TenCCDC";
            column = [{
                field: "TenCCDC", title: GetTextLanguage("tenccdc"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            }, {
                field: "MaCCDC", title: GetTextLanguage("maccdc"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            },
            {
                field: "DonGia", title: GetTextLanguage("dongia"), format: "{0:n2}",
                attributes: { style: "text-align:right" }
            }];
            break;
        case "ThietBi":
            strTitle_Modal = GetTextLanguage("danhsachthietbi");
            strUrl = "/Home/DSThietBi";
            param.CongTrinhID = $(this).data('refer').CongTrinh == "" ? "" : $("#" + $(this).data('refer').CongTrinh).val();
            param.DoiThiCongID = $(this).data('refer').DoiThiCong == "" ? "" : $("#" + $(this).data('refer').DoiThiCong).val();
            sorting = "TenThietBi";
            column = [{
                field: "TenThietBi", title: GetTextLanguage("tenthietbi"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            }, {
                field: "MaThietBi", title: GetTextLanguage("mathietbi"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            },
            {
                field: "DonGia", title: GetTextLanguage("dongia"), format: "{0:n2}",
                attributes: { style: "text-align:right" }
            }];
            break;
        case "DoiThiCong":
            strTitle_Modal = GetTextLanguage("danhmucdoithicong");
            strUrl = "/Home/DSDoiThiCong";
            param.CongTrinhID = $(this).data('refer').CongTrinh == "" ? "" : $("#" + $(this).data('refer').CongTrinh).val();
            sorting = "TenDoi";
            column = [{
                field: "TenDoi", title: GetTextLanguage("tendoi"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            }];
            break;
        case "NhanVien":
            strTitle_Modal = GetTextLanguage("danhsachnhanvien");
            strUrl = "/Home/DSNhanVien";
            param.CongTrinhID = $(this).data('refer').CongTrinh == "" ? "" : $("#" + $(this).data('refer').CongTrinh).val();
            param.DoiThiCongID = $(this).data('refer').DoiThiCong == "" ? "" : $("#" + $(this).data('refer').DoiThiCong).val();
            sorting = "TenNhanVien";
            column = [{
                field: "TenNhanVien", title: GetTextLanguage("tennhanvien"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            },
            {
                field: "TenChucVu", title: GetTextLanguage("tenchucvu"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            }];
            break;
        case "DoiTacThietBi":
            strTitle_Modal = GetTextLanguage("danhsachdoitac");
            strUrl = "/QuanLyDieuChuyenThietBi/LayDanhSachDoiTacThietBi";
            sorting = "TenDoi";
            column = [{
                field: "TenDoi", title: GetTextLanguage("tendoitac"), filterable: FilterInTextColumn,
                attributes: { style: "text-align:left" }
            },
            {
                field: "Email", title: GetTextLanguage("email"), filterable: FilterInTextColumn,
                attributes: { style: "text-align:left" }
            },
            {
                field: "SDT", title: GetTextLanguage("sdt"), filterable: FilterInTextColumn,
                attributes: { style: "text-align:left" }
            },
             {
                 field: "DiaChi", title: GetTextLanguage("diachi"), filterable: FilterInTextColumn,
                 attributes: { style: "text-align:left" }
             }
            ];
            break;
        case "DoiTacCCDC":
            strTitle_Modal = GetTextLanguage("danhsachdoitac");
            strUrl = "/QuanLyDieuChuyenThietBi/LayDanhSachDoiTacCCDC";
            sorting = "TenDoi";
            column = [{
                field: "TenDoi", title: GetTextLanguage("tendoitac"), filterable: FilterInTextColumn,
                attributes: { style: "text-align:left" }
            },
            {
                field: "Email", title: GetTextLanguage("email"), filterable: FilterInTextColumn,
                attributes: { style: "text-align:left" }
            },
            {
                field: "SDT", title: GetTextLanguage("sdt"), filterable: FilterInTextColumn,
                attributes: { style: "text-align:left" }
            },
             {
                 field: "DiaChi", title: GetTextLanguage("diachi"), filterable: FilterInTextColumn,
                 attributes: { style: "text-align:left" }
             }
            ];
            break;
    }

    var mdl = $('#modal-lst').kendoWindow({
        width: "50%",
        height: "60%",
        title: strTitle_Modal,
        modal: true,
        visible: false,
        resizable: false,
        actions: [
            "Close"
        ],
        deactivate: function () {
            $("#modal-lst-grid").empty();
        }
    }).data("kendoWindow").center().open();
    setTitleWindow("modal-lst", strTitle_Modal);
    var datalst = new kendo.data.DataSource({
        transport: {
            read: {
                url: strUrl,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: param
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        pageSize: 50,
        sort: [{ field: sorting, dir: "asc" }],
        schema: {
            data: "data",
            total: "total",
        },
    });

    var gridlst = $("#modal-lst-grid").kendoGrid({
        dataSource: datalst,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            messages: messagegridNonePaging,
            pageSizes: false,
            numeric: false,
            previousNext: false
        },
        //toolbar: kendo.template($("#toolbar_DSCongTrinh").html()),
        width: "100%",
        height: window.innerHeight * 0.8,
        dataBinding: function () {
            record = 0;
        },
        columns: column
    }).data("kendoGrid");

    resizeWindow3("modal-lst-grid", "modal-lst");


    $("#modal-lst-grid .k-grid-content").on("dblclick", "td", function () {
        var rowct = $(this).closest("tr"),
               gridct = $("#modal-lst-grid").data("kendoGrid"),
               dataItemct = gridct.dataItem(rowct);
        $("#" + input).data("kendoDropDownList").value(dataItemct.get(value));
        $("#" + input).data("kendoDropDownList").trigger("change");

        closeModel("modal-lst");
    });
});
function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
        val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }
    return val;
}

(function ($) {
    $.fn.popuptablefilters = function () {
        $(this).on("click", function () {
            var strTitle_Modal = "";
            var strUrl = "";
            var param = {};
            var sorting = "";
            var column;
            var value = $(this).data('val');
            var changefor = $(this).data('changefor');
            var input = $(this).data('input');
            switch ($(this).data('type')) {
                case "CongTrinh":
                    strTitle_Modal = GetTextLanguage("danhsachcongtrinh");
                    strUrl = "/Home/DSCongTrinh";
                    if ("all" in $(this).data()) {
                        if ($(this).data('all')) {
                            strUrl = "/Home/DSCongTrinhAll"
                        }
                    }
                    sorting = "TenCongTrinh";
                    column = [{
                        field: "TenCongTrinh", title: GetTextLanguage("tencongtrinh"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    },
                    {
                        field: "ChuDauTu", title: GetTextLanguage("chudautu"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    },
                    {
                        field: "DiaDiem", title: GetTextLanguage("diadiem"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    }];
                    break;
                case "CongTrinhTong":
                    strTitle_Modal = GetTextLanguage("danhsachcongtrinh");
                    strUrl = "/Home/DSCongTrinhCoCongTrinhTong";
                    if ("all" in $(this).data()) {
                        if ($(this).data('all')) {
                            strUrl = "/Home/DSCongTrinhAll"
                        }
                    }
                    sorting = "TenCongTrinh";
                    column = [{
                        field: "TenCongTrinh", title: GetTextLanguage("tencongtrinh"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    },
                    {
                        field: "ChuDauTu", title: GetTextLanguage("chudautu"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    },
                    {
                        field: "DiaDiem", title: GetTextLanguage("diadiem"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    }];
                    break;
                case "PhongBan":
                    strTitle_Modal = GetTextLanguage("phongban");
                    strUrl = "/Home/DSPhongBan";
                    param.CongTrinhID = "";
                    param.DoiThiCongID = "";
                    sorting = "TenCongTrinh";
                    column = [{
                        field: "TenCongTrinh", title: GetTextLanguage("tenphongban"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    }];
                    break;
                case "VatTu":
                    strTitle_Modal = GetTextLanguage("danhmucvattu");
                    strUrl = "/Home/DSVatTu";
                    param.CongTrinhID = $(this).data('refer').CongTrinh == "" ? "" : $("#" + $(this).data('refer').CongTrinh).val();
                    param.DoiThiCongID = $(this).data('refer').DoiThiCong == "" ? "" : $("#" + $(this).data('refer').DoiThiCong).val();
                    sorting = "TenVatTu";
                    column = [{
                        field: "TenVatTu", title: GetTextLanguage("tenvattu"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    }, {
                        field: "MaVatTu", title: GetTextLanguage("mavatu"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    },
                    {
                        field: "DonGia", title: GetTextLanguage("dongia"), format: "{0:n2}",
                        attributes: { style: "text-align:right" }
                    }];
                    break;
                case "CCDC":
                    strTitle_Modal = GetTextLanguage("ccdc");
                    strUrl = "/Home/DSCCDC";
                    param.CongTrinhID = $(this).data('refer').CongTrinh == "" ? "" : $("#" + $(this).data('refer').CongTrinh).val();
                    param.DoiThiCongID = $(this).data('refer').DoiThiCong == "" ? "" : $("#" + $(this).data('refer').DoiThiCong).val();
                    sorting = "TenCCDC";
                    column = [{
                        field: "TenCCDC", title: GetTextLanguage("tenccdc"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    }, {
                        field: "MaCCDC", title: GetTextLanguage("maccdc"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    },
                    {
                        field: "DonGia", title: GetTextLanguage("dongia"), format: "{0:n2}",
                        attributes: { style: "text-align:right" }
                    }];
                    break;
                case "ThietBi":
                    strTitle_Modal = GetTextLanguage("danhsachthietbi");
                    strUrl = "/Home/DSThietBi";
                    param.CongTrinhID = $(this).data('refer').CongTrinh == "" ? "" : $("#" + $(this).data('refer').CongTrinh).val();
                    param.DoiThiCongID = $(this).data('refer').DoiThiCong == "" ? "" : $("#" + $(this).data('refer').DoiThiCong).val();
                    sorting = "TenThietBi";
                    column = [{
                        field: "TenThietBi", title: GetTextLanguage("tenthietbi"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    }, {
                        field: "MaThietBi", title: GetTextLanguage("mathietbi"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    },
                    {
                        field: "DonGia", title: GetTextLanguage("dongia"), format: "{0:n2}",
                        attributes: { style: "text-align:right" }
                    }];
                    break;
                case "DoiThiCong":
                    strTitle_Modal = GetTextLanguage("danhmucdoithicong");
                    strUrl = "/Home/DSDoiThiCong";
                    param.CongTrinhID = $(this).data('refer').CongTrinh == "" ? "" : $("#" + $(this).data('refer').CongTrinh).val();
                    sorting = "TenDoi";
                    column = [{
                        field: "TenDoi", title: GetTextLanguage("tendoi"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    }];
                    break;
                case "NhanVien":
                    strTitle_Modal = GetTextLanguage("danhsachnhanvien");
                    strUrl = "/Home/DSNhanVien";
                    param.CongTrinhID = $(this).data('refer').CongTrinh == "" ? "" : $("#" + $(this).data('refer').CongTrinh).val();
                    param.DoiThiCongID = $(this).data('refer').DoiThiCong == "" ? "" : $("#" + $(this).data('refer').DoiThiCong).val();
                    sorting = "TenNhanVien";
                    column = [{
                        field: "TenNhanVien", title: GetTextLanguage("tennhanvien"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    },
                    {
                        field: "TenChucVu", title: GetTextLanguage("tenchucvu"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    }];
                    break;
                case "DoiTacThietBi":
                    strTitle_Modal = GetTextLanguage("danhsachdoitac");
                    strUrl = "/QuanLyDieuChuyenThietBi/LayDanhSachDoiTacThietBi";
                    sorting = "TenDoi";
                    column = [{
                        field: "TenDoi", title: GetTextLanguage("tendoitac"), filterable: FilterInTextColumn,
                        attributes: { style: "text-align:left" }
                    },
                    {
                        field: "Email", title: GetTextLanguage("email"), filterable: FilterInTextColumn,
                        attributes: { style: "text-align:left" }
                    },
                    {
                        field: "SDT", title: GetTextLanguage("sdt"), filterable: FilterInTextColumn,
                        attributes: { style: "text-align:left" }
                    },
                     {
                         field: "DiaChi", title: GetTextLanguage("diachi"), filterable: FilterInTextColumn,
                         attributes: { style: "text-align:left" }
                     }
                    ];
                    break;
                case "DoiTacCCDC":
                    strTitle_Modal = GetTextLanguage("danhsachdoitac");
                    strUrl = "/QuanLyDieuChuyenThietBi/LayDanhSachDoiTacCCDC";
                    sorting = "TenDoi";
                    column = [{
                        field: "TenDoi", title: GetTextLanguage("tendoitac"), filterable: FilterInTextColumn,
                        attributes: { style: "text-align:left" }
                    },
                    {
                        field: "Email", title: GetTextLanguage("email"), filterable: FilterInTextColumn,
                        attributes: { style: "text-align:left" }
                    },
                    {
                        field: "SDT", title: GetTextLanguage("sdt"), filterable: FilterInTextColumn,
                        attributes: { style: "text-align:left" }
                    },
                     {
                         field: "DiaChi", title: GetTextLanguage("diachi"), filterable: FilterInTextColumn,
                         attributes: { style: "text-align:left" }
                     }
                    ];
                    break;
            }

            var mdl = $('#modal-lst').kendoWindow({
                width: "50%",
                height: "60%",
                title: strTitle_Modal,
                modal: true,
                visible: false,
                resizable: false,
                actions: [
                    "Close"
                ],
                deactivate: function () {
                    $("#modal-lst-grid").empty();
                }
            }).data("kendoWindow").center().open();
            setTitleWindow("modal-lst", strTitle_Modal);
            var datalst = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: strUrl,
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: param
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                pageSize: 50,
                sort: [{ field: sorting, dir: "asc" }],
                schema: {
                    data: "data",
                    total: "total",
                },
            });

            var gridlst = $("#modal-lst-grid").kendoGrid({
                dataSource: datalst,
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    messages: messagegridNonePaging,
                    pageSizes: false,
                    numeric: false,
                    previousNext: false
                },
                //toolbar: kendo.template($("#toolbar_DSCongTrinh").html()),
                width: "100%",
                height: window.innerHeight * 0.8,
                dataBinding: function () {
                    record = 0;
                },
                columns: column
            }).data("kendoGrid");

            resizeWindow3("modal-lst-grid", "modal-lst");


            $("#modal-lst-grid .k-grid-content").on("dblclick", "td", function () {
                var rowct = $(this).closest("tr"),
                       gridct = $("#modal-lst-grid").data("kendoGrid"),
                       dataItemct = gridct.dataItem(rowct);
                $("#" + input).data("kendoDropDownList").value(dataItemct.get(value));
                $("#" + input).data("kendoDropDownList").trigger("change");

                closeModel("modal-lst");
            });
        });
    }
})(jQuery);

function OpenSeLectFrom(type, url, data, input) {
    var strTitle_Modal = "";
    var column = [];
    var sorting = "";
    var value = "";
    //công trình
    if (type == 0) {
        strTitle_Modal = GetTextLanguage('chonphongban');
        column = [
            {
                field: "TenCongTrinh", title: GetTextLanguage("tenphongban"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            },
        ];
    }
    //công trình
    if (type == 1) {
        strTitle_Modal = GetTextLanguage('choncongtrinh');
        value = "CongTrinhID";
        column = [
            {
                field: "TenCongTrinh", title: GetTextLanguage("tencongtrinh"), filterable: FilterInTextColumn,
                attributes: {
                    style: "text-align:left"
                }
            },
                    {
                        field: "ChuDauTu", title: GetTextLanguage("chudautu"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    },
                    {
                        field: "DiaDiem", title: GetTextLanguage("diadiem"), filterable: FilterInTextColumn,
                        attributes: {
                            style: "text-align:left"
                        }
                    }];
    }
    //Đội thi công
    if (type == 2) {
        strTitle_Modal = GetTextLanguage('choncongtrinh');
        sorting = "TenDoi";
        value = "DoiID";
        column = [{
            field: "TenDoi", title: GetTextLanguage("tendoi"), filterable: FilterInTextColumn,
            attributes: {
                style: "text-align:left"
            }
        }];
    }
    //Đối tác
    if (type == 3) {
        strTitle_Modal = GetTextLanguage('chondoitac');
        sorting = "TenDoi";
        value = "DoiID";
        column = [{
            field: "TenDoi", title: GetTextLanguage("tendoi"),
            filterable: FilterInTextColumn,
            attributes: { style: "text-align:left" }
        },
        {
            field: "DiaChi", title: GetTextLanguage("diachi"),
            filterable: FilterInTextColumn,
            attributes: { style: "text-align:left" }
        },
        {
            field: "Email", title: GetTextLanguage("emai"),
            filterable: FilterInTextColumn,
            attributes: { style: "text-align:left" }
        },
        {
            field: "SDT", title: GetTextLanguage("sdt"),
            filterable: FilterInTextColumn,
            attributes: { style: "text-align:left" }
        }
        ];
    }
    //Nhân viên
    if (type == 4) {
        sorting = "TenNhanVien";
        value = "NhanVienID";
        column = [{
            field: "TenNhanVien", title: GetTextLanguage("tennhanvien"), filterable: FilterInTextColumn,
            attributes: {
                style: "text-align:left"
            }
        },
        {
            field: "TenChucVu", title: GetTextLanguage("chucvu"), filterable: FilterInTextColumn,
            attributes: {
                style: "text-align:left"
            }
        }];
    }
    var mdl = $('#modal-lst').kendoWindow({
        width: "50%",
        height: "60%",
        title: strTitle_Modal,
        modal: true,
        visible: false,
        resizable: false,
        actions: [
            "Close"
        ],
        deactivate: function () {
            $("#modal-lst-grid").empty();
        }
    }).data("kendoWindow").center().open();
    setTitleWindow("modal-lst", strTitle_Modal);
    var datalst = new kendo.data.DataSource({
        transport: {
            read: {
                url: url,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: data
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        pageSize: 50,
        sort: [{ field: sorting, dir: "asc" }],
        schema: {
            data: "data",
            total: "total",
        },
    });

    var gridlst = $("#modal-lst-grid").kendoGrid({
        dataSource: datalst,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            messages: messagegridNonePaging,
            pageSizes: false,
            numeric: false,
            previousNext: false
        },
        //toolbar: kendo.template($("#toolbar_DSCongTrinh").html()),
        width: "100%",
        height: innerHeight * 0.8,
        dataBinding: function () {
            record = 0;
        },
        columns: column
    }).data("kendoGrid");
    $("#modal-lst-grid .k-grid-content").on("dblclick", "td", function () {
        var rowct = $(this).closest("tr"),
               gridct = $("#modal-lst-grid").data("kendoGrid"),
               dataItemct = gridct.dataItem(rowct);
        input.data("kendoDropDownList").value(dataItemct.get(value));
        input.data("kendoDropDownList").trigger("change");

        closeModel("modal-lst");
        if ($("#modal-lst").length == 0) {
            $("body").append("<div id='modal-lst'><div id='modal-lst-grid'></div></div>");
        }
    });
}

function showslideimg() {
    //$('#fullscreenSlideshowContainer').remove();
    $('.imgslider img').fullscreenslides();

    // All events are bound to this container element
    var $container = $('#fullscreenSlideshowContainer');

    $container
        //This is triggered once:
        .bind("init", function () {

            // The slideshow does not provide its own UI, so add your own
            // check the fullscreenstyle.css for corresponding styles
            $container
                .append('<div class="ui" id="fs-close">&times;</div>')
                .append('<div class="ui" id="fs-loader">loading...</div>')
                .append('<div class="ui" id="fs-prev">&lt;</div>')
                .append('<div class="ui" id="fs-next">&gt;</div>')
                .append('<div class="ui" id="fs-caption"><span></span></div>');

            // Bind to the ui elements and trigger slideshow events
            $('#fs-prev').click(function () {
                // You can trigger the transition to the previous slide
                $container.trigger("prevSlide");
            });
            $('#fs-next').click(function () {
                // You can trigger the transition to the next slide
                $container.trigger("nextSlide");
            });
            $('#fs-close').click(function () {
                // You can close the slide show like this:
                $container.trigger("close");
            });

        })
        // When a slide starts to load this is called
        .bind("startLoading", function () {
            // show spinner
            $('#fs-loader').show();
        })
        // When a slide stops to load this is called:
        .bind("stopLoading", function () {
            // hide spinner
            $('#fs-loader').hide();
        })
        // When a slide is shown this is called.
        // The "loading" events are triggered only once per slide.
        // The "start" and "end" events are called every time.
        // Notice the "slide" argument:
        .bind("startOfSlide", function (event, slide) {
            // set and show caption
            $('#fs-caption span').text(slide.title);
            $('#fs-caption').show();
        })
        // before a slide is hidden this is called:
        .bind("endOfSlide", function (event, slide) {
            $('#fs-caption').hide();
        });
}

function KeepPopupError(data, grid) {
    if (data.code != "successes") {
        $("#" + grid).data("kendoGrid").cancelChanges();
    }
}

$(".validreq").on("keyup", function () {
    if ($(this).val() == "") {
        $(this).addClass("errorReq");
    } else {
        $(this).removeClass("errorReq");
    }
});

function sumgridtable(table, inputsum, idreturn, objtable) {
    var rows = objtable.rows({ 'search': 'applied' }).nodes();
    var val = 0;
    var i = 0;
    $('.' + inputsum, rows).each(function () {
        val += $(this).autoNumeric('get') * 1;
        i++;
    });
    $("." + idreturn).autoNumeric('set', val);

}
function showslideimg5(div) {
    //$('#fullscreenSlideshowContainer').remove();
    $('.imgslider img', div).fullscreenslides();
    // All events are bound to this container element
    var $container = $('#fullscreenSlideshowContainer');

    $container
        //This is triggered once:
        .bind("init", function () {
            // The slideshow does not provide its own UI, so add your own
            // check the fullscreenstyle.css for corresponding styles
            $container.html("");
            if ($('#fullscreenSlideshowContainer #fs-xoaanh').length == 0) {
                $container
                    .append('<div class="ui" id="fs-close">&times;</div>')
                    .append('<div class="ui" id="fs-loader">' + GetTextLanguage("dangtai") + '</div > ')
                    .append('<div class="ui" id="fs-prev">&lt;</div>')
                    .append('<div class="ui" id="fs-next">&gt;</div>')
                    .append('<div class="ui" id="fs-caption"><span></span></div>')
                    .append('<div  id="fs-xoaanh" class="btn btn-danger" data-vitri="0" style="position: absolute;z-index: 999999;top: 10px;left: 10%;" ><span>Xóa Ảnh</span></div>');
                $('#fs-prev').click(function () {
                    // You can trigger the transition to the previous slide
                    $container.trigger("prevSlide");
                });
                $('#fs-next').click(function () {
                    // You can trigger the transition to the next slide
                    $container.trigger("nextSlide");
                });
                $('#fs-close').click(function () {
                    // You can close the slide show like this:
                    $container.trigger("close");
                });
                $('#fs-xoaanh').click(function () {
                    $("#fullscreenSlideshowContainer .slide").each(function (index, element) {
                        $('.dshinhanhadd .imgslider').each(function (index2, element2) {
                            if ($('#fs-xoaanh').attr("data-image").toLowerCase().indexOf($(element2).find('img').attr('src').toLowerCase()) >= 0) {
                                $(element2).remove();
                            }
                        });
                    })
                    $container.trigger("close");
                });
            }

        })
        // When a slide starts to load this is called
        .bind("startLoading", function () {
            // show spinner
            $('#fs-loader').show();
        })
        // When a slide stops to load this is called:
        .bind("stopLoading", function () {
            // hide spinner
            $('#fs-loader').hide();
        })
        .bind("startOfSlide", function (event, slide) {
            // set and show caption
            $('#fs-caption span').text(slide.title);
            $('#fs-caption').show();
        })
        // before a slide is hidden this is called:
        .bind("endOfSlide", function (event, slide) {
            $('#fs-caption').hide();
        });
}

function loadimgSuaChua5(input, div) {
    //$('.dshinhanhadd').empty();
    if (input.files) {
        var filesAmount = input.files.length;
        var html = "";
        for (var i = 0; i < filesAmount; i++) {
            if (input.files[i].type == "image/png" || input.files[i].type == "image/jpg" || input.files[i].type == "image/jpeg" || input.files[i].type == "image/gif") {
                readURL5(input.files[i], div);
            }
        }

    }
}
function readURL5(file, div) {

    var reader = new FileReader();
    reader.onload = function (event) {
        var html = "";
        html += "<div class='imgslider' style='float:left;position:relative;width:75px;height:90px;margin:3px;'>";
        html += "<div style='margin:0px;' class='img-con ahihi3 img-thumbnail'><a rel='gallery' href='" + event.target.result + "'><img src='" + event.target.result + "'  data-file='" + file.name + "' style='width: 70px;height:70px;margin: 0;opacity: 1;'></a></div>";
        html += "</div>";
        $('.dshinhanhadd', div).append(html);
        showslideimg5($('.dshinhanhadd', div));
    }
    reader.readAsDataURL(file);
}
function loadimgavatar(input, div) {
    if (input.files) {
        var filesAmount = input.files.length;
        var html = "";
        for (var i = 0; i < filesAmount; i++) {
            if (input.files[i].type == "image/png" || input.files[i].type == "image/jpg" || input.files[i].type == "image/jpeg" || input.files[i].type == "image/gif") {
                readURLAvatar(input.files[i], div);
            }
        }
    }
}
function readURLAvatar(file, div) {

    var reader = new FileReader();
    reader.onload = function (event) {
        $(".imgavatar").closest("a").attr("href", event.target.result);
        $(".imgavatar").attr("src", event.target.result);
        $(".imgavatar").attr("data-file", file.name);
        showavatar();
    }
    reader.readAsDataURL(file);
}
function showavatar(div) {
    //$('#fullscreenSlideshowContainer').remove();
    $('.anhdaidien img', div).fullscreenslides();

    // All events are bound to this container element
    var $container = $('#fullscreenSlideshowContainer');

    $container
        //This is triggered once:
        .bind("init", function () {
            // The slideshow does not provide its own UI, so add your own
            // check the fullscreenstyle.css for corresponding styles
            $container.html("");
            if ($('#fullscreenSlideshowContainer #fs-close').length == 0) {
                $container
                    .append('<div class="ui" id="fs-close">&times;</div>')
                    .append('<div class="ui" id="fs-loader">' + GetTextLanguage("dangtai") + '</div > ')
                    .append('<div class="ui" id="fs-prev">&lt;</div>')
                    .append('<div class="ui" id="fs-next">&gt;</div>')
                    .append('<div class="ui" id="fs-caption"><span></span></div>');
                $('#fs-prev').click(function () {
                    // You can trigger the transition to the previous slide
                    $container.trigger("prevSlide");
                });
                $('#fs-next').click(function () {
                    // You can trigger the transition to the next slide
                    $container.trigger("nextSlide");
                });
                $('#fs-close').click(function () {
                    // You can close the slide show like this:
                    $container.trigger("close");
                });
            }

        })
        // When a slide starts to load this is called
        .bind("startLoading", function () {
            // show spinner
            $('#fs-loader').show();
        })
        // When a slide stops to load this is called:
        .bind("stopLoading", function () {
            // hide spinner
            $('#fs-loader').hide();
        })
        .bind("startOfSlide", function (event, slide) {
            // set and show caption
            $('#fs-caption span').text(slide.title);
            $('#fs-caption').show();
        })
        // before a slide is hidden this is called:
        .bind("endOfSlide", function (event, slide) {
            $('#fs-caption').hide();
        });
}
function createpopupdoitac(div) {
    $("button", div).click(function () {
        CreateModalWithSize("mldfilterdoitac", "99%", "90%", "mtplfilterdoitac", "Lọc đối tác");
        var dataKieuDoiTac = [
        { text: "Đối tác mua", val: 1 },
        { text: "Đối tác bán", val: 2 },
        ]
        var dataPhongBan = new kendo.data.DataSource({
            transport: {
                read: {
                    url:  "/Home/LayDanhSachDoiTac",
                    method: "GET",
                },
                parameterMap: function (data, type) {
                    return JSON.stringify(data);
                }
            },
            pageSize: 25,
            batch: true,
            sort: [{ field: "TenDoiTac", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    fields: {
                        DoiThiCongID: { editable: false, nullable: true },
                        TenDoiTac: { type: "string" },
                        TongChi: { type: "number" },
                        TongThu: { type: "number" },
                        ChenhLech: { type: "number" }
                    }
                }
            },
        });

        //Build grid
        var grid = $("#gridfilterdoitac").kendoGrid({
            dataSource: dataPhongBan,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: [25, 50, 100],
                messages: messagegrid
            },
            messages: {
                commands: {
                    update: GetTextLanguage("capnhat"),
                    canceledit: GetTextLanguage("huy")
                }
            },
            height: window.innerHeight * 0.85,
            width: "100%",
            
            dataBinding: function () {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "#= ++record #",
                    width: 60,
                    attributes: alignCenter
                },
                {
                    field: "KieuDoiTac",
                    title: "Kiểu đối tác",
                    filterable: {
                        cell: {
                            template: function (args) {
                                // create a DropDownList of unique values (colors)
                                args.element.kendoDropDownList({
                                    dataSource: [{ id: 1, text: "Đối tác mua" }, { id: 2, text: "Đối tác bán" }],
                                    dataTextField: "text",
                                    dataValueField: "id",
                                    valuePrimitive: true
                                });
                            },
                            showOperators: false
                        }
                    },
                    template: kendo.template($("#tempKieuDoiTacloc").html()),
                    attributes: { style: "text-align:left;" }
                },
                {
                    field: "TenDoiTac",
                    title: "Tên đối tác",
                    filterable: FilterInTextColumn,
                    attributes: { style: "text-align:left;" }
                },
                {
                    field: "DiaChi",
                    title: GetTextLanguage("tendoitac"),
                    filterable: FilterInTextColumn,
                    attributes: { style: "text-align:left;" }
                },
                {
                    field: "SDT",
                    title: "Số điện thoại",
                    filterable: FilterInTextColumn,
                    attributes: { style: "text-align:left;" }
                },
               

            ],
        });
        $("#gridfilterdoitac .k-grid-content").on("dblclick", "td", function () {
            var rowct = $(this).closest("tr");
            if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                var gridct = $("#gridfilterdoitac").data("kendoGrid"),
                dataItemct = gridct.dataItem(rowct)
                $("input", div).data("kendoDropDownList").value(dataItemct.DoiTacID);
                $("input", div).data("kendoDropDownList").trigger("change");
                closeModel("mldfilterdoitac");
            }
        });
    });
}

function createpopupdonhang(div) {
    $("button", div).click(function () {
        CreateModalWithSize("mdlChonDonHangHoaDon", "100%", "90%", "tplChonDonHangHoaDon", "Chọn đơn hàng");
        var dataDonHang = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/Home" + "/GetAllDataDonHang",
                    method: "GET",
                    data: { TuNgay: $("#TuNgay").val(), DenNgay: $("#DenNgay").val() }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            batch: true,
            pageSize: 50,
            schema: {
                type: 'json',
                data: 'data',
                total: "total",
                model: {
                    id: "DonHangID",
                    fields: {
                        DonHangID: { editable: false, nullable: true },
                    }
                }
            },
        });

        //Build grid
        var grid = $("#gridChonDonHangHoaDon").kendoGrid({
            dataSource: dataDonHang,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            dataBinding: function () {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            height: window.innerHeight * 0.8,
            toolbar: [{ template: kendo.template($("#btnThemLocHoaDon").html()) }],
            width: "100%",
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    width: 60,
                    attributes: alignCenter,
                    template: "#= ++record #",
                },
                {
                    field: "ThoiGian",
                    title: "Thời gian đặt",
                    width: 120,
                    attributes: alignCenter,
                    filterable: false,
                    template: formatToDateTime("ThoiGian"),
                },
                {
                    field: "MaDonHang",
                    title: "Mã đơn hàng",
                    width: 100,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn,
                },
                {
                    field: "TenDonHang",
                    title: "Tên đơn hàng",
                    filterable: FilterInTextColumn,
                    attributes: alignLeft,
                    width: 180
                },
                {
                    field: "TrangThai",
                    title: "Trạng thái",
                    filterable: {
                        cell: {
                            template: function (args) {
                                args.element.kendoDropDownList({
                                    dataSource: [
                                    { text: "Chưa bắt đầu", value: 1 },
                                    { text: "Chưa báo giá", value: 7 },
                                    { text: "Đã đã duyệt", value: 5 },
                                    { text: "Đang sản xuất", value: 2 },
                                    { text: "Hoàn thành SX", value: 3 },
                                    { text: "Đã giao hàng", value: 4 },
                                    { text: "Đã thanh toán xong", value: 6 },
                                    { text: "Đã hủy", value: 8 },
                                    ],
                                    dataTextField: "text",
                                    dataValueField: "id",
                                    valuePrimitive: true
                                });
                            },
                            showOperators: false
                        }
                    },
                    attributes: alignLeft,
                    width: 100,
                    template: kendo.template($("#tplTrangThaiHoaDon").html()),
                },
                {
                    field: "TenDoiTac",
                    title: "Tên đối tác",
                    filterable: FilterInTextColumn,
                    attributes: alignLeft,
                    width: 180
                },
                {
                    field: "GiaTriDonHang",
                    title: "Giá trị đơn hàng",
                    filterable: FilterInColumn,
                    attributes: alignRight,
                    format: "{0:n2}",
                    width: 150
                },
                {
                    field: "DiaDiemGiaoHang",
                    title: "Địa điểm giao hàng",
                    filterable: FilterInTextColumn,
                    attributes: alignLeft,
                    width: 200
                },
                
            ],
        });
        var currentTime = new Date();
        $("#TuNgay").kendoDatePicker({
            format: "dd/MM/yyyy",
            value: new Date("01/01/" + currentTime.getFullYear()),
        });
        $("#DenNgay").kendoDatePicker({
            format: "dd/MM/yyyy",
            value: new Date(),
        });
        $("#btnTimKiem").click(function () {
            $("#gridChonDonHangHoaDon").data("kendoGrid").dataSource.read({ TuNgay: $("#TuNgay").val(), DenNgay: $("#DenNgay").val() });
        })
        $("#gridChonDonHangHoaDon .k-grid-content").on("dblclick", "td", function () {
            var rows = $(this).closest("tr"),
                grids = $("#gridChonDonHangHoaDon").data("kendoGrid"),
                dataItems = grids.dataItem(rows);
            $("input", div).data("kendoDropDownList").value(dataItems.DonHangID);
                $("input", div).data("kendoDropDownList").trigger("change");
            closeModel("mdlChonDonHangHoaDon");

        })
       
    });
}