﻿isLoadContent = true;
window.onbeforeunload = function () {
    if (!isLoadContent) {
        return 'Các thay đổi bạn đã thực hiện có thể không được lưu.';
    }
};

function WatingLoad(opacity) {
    $("#content-loader").block({
        message: '<i class="icon-spinner9 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: opacity,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });
}

function ContentWating(IDContent) {
    $("#" + IDContent).block({
        message: '<i class="icon-spinner9 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });
}

function ContentWatingOP(IDContent, opa) {
    $("#" + IDContent).block({
        message: '<i class="icon-spinner9 spinner"></i>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: opa,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'none'
        }
    });
}

function LoadingContent(controlID, url, tenMenu) {
    if (controlID != "") {
        $.ajax({
            url: url,
            dataType: 'html',
            beforeSend: function () {
                WatingLoad(1);
            },
            success: function (data) {
                window.setTimeout(
                    function () {
                        $("#content-loader").unblock();
                        $("#" + controlID).html(data);
                        $(".title-function").html(tenMenu);
                    }, 1000);

            },
            error: function () {
                isLoadContent = true;
                $("#content-loader").unblock();
                showToast("error", "title", "message");
            },
            complete: function () {
                isLoadContent = true;
                window.setTimeout(function () { $("#content-loader").unblock(); }, 1000);
            },
        });
    }
}

function ContentLoading(controlID, url) {
    console.log(isLoadContent);
    if (!isLoadContent) {
        if (confirm(GetTextLanguage("thongtindaduocthaydoi"))) {
            $.ajax({
                url: url,
                dataType: 'html',
                beforeSend: function () {
                    ContentWating(controlID);
                },
                success: function (data) {
                    isLoadContent = true;
                    window.setTimeout(
                        function () {
                            $("#" + controlID).unblock();
                            $("#" + controlID).html(data);
                        }, 1000);

                },
                error: function () {
                    isLoadContent = true;
                    $("#" + controlID).unblock();
                    showToast("error", "title", "message");
                },
                complete: function () {
                    isLoadContent = true;
                    window.setTimeout(function () { $("#" + controlID).unblock(); }, 1000);
                },
            });
        } else {
            activeMenuNav(".hd");
            return false;
        }
    }
    else {
        $.ajax({
            url: url,
            dataType: 'html',
            beforeSend: function () {
                ContentWating(controlID);
            },
            success: function (data) {
                isLoadContent = true;
                window.setTimeout(
                    function () {
                        $("#" + controlID).unblock();
                        $("#" + controlID).html(data);
                    }, 1000);

            },
            error: function () {
                isLoadContent = true;
                $("#" + controlID).unblock();
                showToast("error", "title", "message");
            },
            complete: function () {
                isLoadContent = true;
                window.setTimeout(function () { $("#" + controlID).unblock(); }, 1000);
            },
        });
    }
}


function LoadingSecondSiderbar(url) {
    $.ajax({
        url: url,
        dataType: 'html',
        success: function (data) {
            $("#second-siderbar").html(data);
        },
        error: function () {
            showToast("error", "title", "message");
        },
        complete: function () {
        },
    });
}

function SiderBarXs(url) {

    //$(".sidebar-hidden").show();
    //$("#body").addClass("sidebar-xs");
    //$("#second-siderbar").addClass("siderbar-show");
    LoadingSecondSiderbar(url);
}

function activeMenuNav(e) {
    $(".limenu").removeClass("active");
    $(e).closest('li').addClass("active");
}

function progressFunction(evt) {
    ContentWatingOP("content-loader", 0.9);
}

function AjaxLoading(url, parag, iContent, grid, modal, paragrid) {
    ContentWatingOP(iContent, 0.9);
    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: url,
        data: parag,
        success: function (data) {
            setTimeout(function () {
                if (data.code != "" && data.code != undefined && data.code != null) {
                    altReturn(data);
                    if (data.code == "success") {
                        if (grid != "") {
                            gridReload(grid, paragrid);
                        }
                        if (modal != "") {
                            closeModel(modal);
                        }
                    }
                } else {
                    if (grid != "") {
                        gridReload(grid, paragrid);
                    }
                    if (modal != "") {
                        closeModel(modal);
                    }
                }
                $("#" + iContent).unblock();
            }, 1000);
        },
        error: function (xhr, status, error) {
            setTimeout(function () {
                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                $("#" + iContent).unblock();
            }, 1000);
        },

    });
}
function AjaxLoadingMulti(url, parag, iContent, grid, modal) {
    ContentWatingOP(iContent, 0.9);
    $.ajax({
        cache: false,
        async: true,
        type: "POST",
        url: url,
        data: parag,
        success: function (data) {
            setTimeout(function () {
                if (data.code != "" && data.code != undefined && data.code != null) {
                    altReturn(data);
                    if (data.code == "success") {
                        if (grid.length > 0) {
                            for (var k = 0; k < grid.length; k++) {
                                gridReload(grid[k].grid, grid[k].parag);
                            }
                        }
                        if (modal.length > 0) {
                            for (var k = 0; k < modal.length; k++) {
                                closeModel(modal[k]);
                            }
                            
                        }
                    }
                } else {
                    if (grid.length > 0) {
                        for (var k = 0; k < grid.length; k++) {
                            gridReload(grid[k].grid, grid[k].parag);
                        }
                    }
                    if (modal.length > 0) {
                        for (var k = 0; k < modal.length; k++) {
                            closeModel(modal[k]);
                        }

                    }
                }
                $("#" + iContent).unblock();
            }, 1000);
        },
        error: function (xhr, status, error) {
            setTimeout(function () {
                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                $("#" + iContent).unblock();
            }, 1000);
        },

    });
}

