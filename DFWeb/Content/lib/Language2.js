﻿function Construct() {
    return ['aaaaa'];
}
function GetTextLanguage(key) {
    var languageselect = $.cookie("DFS_Language");
    if (languageselect == undefined || languageselect == null || languageselect == "") {
        languageselect = 'vn';
    }
    if (Construct().language[key] != null && Construct().language[key] != "") {
        return Construct().language[key][languageselect];
    }
    else {
        alertToastr({ code: "warning", message: "Thiếu Ngôn Ngữ Có Key: " + key })
    }
}
function ChangeLanguage(language) {
    $.ajax({
        method: "get",
        url: "/Home/SetLanguage",
        data: { language: language },
        success: function (data) {
            window.location.reload();
        }
    });
}
$(document).ready(function () {
    var languageselect = $.cookie("DFS_Language");
    var texts = "Vietnamese"
    var src = '/Content/assets/images/flags/vn.png';
    if (languageselect == "cn") {
        texts = "Chinese";
        src = '/Content/assets/images/flags/cn.png';
    } else if (languageselect == "en") {
        texts = "English";
        src = '/Content/assets/images/flags/gb.png';
    }
    $(".navbar-nav .dropdown a .language-text").text(texts);
    $(".navbar-nav .dropdown a .logo-language").attr("src", src);
})
