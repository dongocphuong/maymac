﻿if ($.cookie("DFS_Language") == "" || $.cookie("DFS_Language") == "vn") {
    kendo.culture("vi-VN");
    moment.locale('vi');
} else if ($.cookie("DFS_Language") == "cn") {
    kendo.culture("zh-CN");
} else {
    kendo.culture("en-US");
}
function GetImage(DsHinhAnh) {
    if (DsHinhAnh != null && DsHinhAnh != "") {
        var lstimage = DsHinhAnh.split(",");
        var str = "";
        if (lstimage.length > 0) {
            str += '<div class="imgslider2" style="float:left;position:relative;><div style="margin:0px;" class="img-con ahihi3 img-thumbnail">';
            str += '<a rel="gallery" href="' + (localQLVT + lstimage[0]) + '">';
            str += '<img src="' + (localQLVT + lstimage[0]) + '" style="width:100%;height:50px;margin: 0;opacity: 1;"></a></div></div>';
        } else {
            str += '<div class="imgslider2" style="float:left;position:relative;"><div style="margin:0px;" class="img-con ahihi3 img-thumbnail">';
            str += '<a rel="gallery" href="/content/no-image.jpg">';
            str += '<img src="/content/no-image.jpg"  style="width:100%;height:50px;margin: 0;opacity: 1;"></a></div></div>';
        }
        return str;
    } else {
        var str = "";
        str += '<div class="imgslider2" style="float:left;position:relative;"><div style="margin:0px;" class="img-con ahihi3 img-thumbnail">';
        str += '<a rel="gallery" href="/content/no-image.jpg">';
        str += '<img src="/content/no-image.jpg"  style="width:100%;height:50px;margin: 0;opacity: 1;"></a></div></div>';
        return str;
    }
}
function GetImage2(DsHinhAnh) {
    if (DsHinhAnh != null && DsHinhAnh != "") {
        var lstimage = DsHinhAnh.split(",");
        var str = "";
        if (lstimage.length > 0) {
            str += '<div class="imgslider2" style="float:left;position:relative;><div style="margin:0px;" class="img-con ahihi3 img-thumbnail">';
            str += '<a rel="gallery" href="' + (localQLVT + lstimage[0]) + '">';
            str += '<img src="' + (localQLVT + lstimage[0]) + '" style="width:50px;height:50px;margin: 0;opacity: 1;"></a></div></div>';
        } else {
            str += '<div class="imgslider2" style="float:left;position:relative;"><div style="margin:0px;" class="img-con ahihi3 img-thumbnail">';
            str += '<a rel="gallery" href="/content/no-image.jpg">';
            str += '<img src="/content/no-image.jpg"  style="width:50px;height:50px;margin: 0;opacity: 1;"></a></div></div>';
        }
        return str;
    } else {
        var str = "";
        str += '<div class="imgslider2" style="float:left;position:relative;"><div style="margin:0px;" class="img-con ahihi3 img-thumbnail">';
        str += '<a rel="gallery" href="/content/no-image.jpg">';
        str += '<img src="/content/no-image.jpg"  style="width:50px;height:50px;margin: 0;opacity: 1;"></a></div></div>';
        return str;
    }
}
function showslideimg10() {
    $('.imgslider2 a').on("click", function () {
        $("#myModalShowimage").modal();
        $("#myModalShowimage #showimagepopup").attr("src",$(this).attr("href"))
        return false;
      
    })
}
function saveMKMoi() {
    var mkcu = $("#mkcu").val();
    var mkmoi = $("#mkmoi").val();
    var mkmoilai = $("#mkmoilai").val();
    if (mkcu == null || mkcu == "") {
        $(".msg_mkcu").html("Nhập mật khẩu");
        $(".msg_mkcu").show();
        $("#mkcu").focus();
        return false;
    }
    if (mkmoi == null || mkmoi == "") {
        $(".msg_mkmoi").html("Nhập mật khẩu mới");
        $(".msg_mkmoi").show();
        $(".msg_mkcu").hide();
        $(".msg_mkmoilai").hide();
        $("#mkmoi").focus();
        return false;
    }
    if (mkmoilai != mkmoi) {
        $(".msg_mkmoilai").html("Mật khẩu không trùng khớp");
        $(".msg_mkmoilai").show();
        $(".msg_mkmoi").hide();
        $(".msg_mkcu").hide();
        $("#mkmoilai").focus();
        return false;
    }
    $.ajax({
        url: '/Home/DoiMK',
        method: "POST",
        data: {
            mkcu: mkcu,
            mkmoi: mkmoi,
        },
        datatype: 'json',
        success: function (data) {
            if (data == true) {
                $("#doimatkhaumodal").modal("toggle");
                $("#msgThanhCong").html("Đổi mật khẩu thành công!");
                $("#myModalThanhCong").modal('toggle');
            } else {
                $(".msg_mkcu").html("Mật khẩu không đúng!");
                $(".msg_mkcu").show();
                $(".msg_mkmoilai").hide();
                $(".msg_mkmoi").hide();
                $("#mkcu").focus();
            }
        }
    });

}

if ($.cookie("DFS_Language") == "") {
    ChangeLanguage("vn");
    $(".drlanguage").html('<img src="/Content/assets/images/flags/vn.png" alt="" > Vietnamese <span class="caret"></span>');
} else if ($.cookie("DFS_Language") == "vn") {
    $(".drlanguage").html('<img src="/Content/assets/images/flags/vn.png" alt="" > Vietnamese <span class="caret"></span>');
} else if ($.cookie("DFS_Language") == "cn") {
    $(".drlanguage").html('<img src="/Content/assets/images/flags/cn.png" alt="" > Chinese <span class="caret"></span>');
} else if ($.cookie("DFS_Language") == "en") {
    $(".drlanguage").html('<img src="/Content/assets/images/flags/gb.png" alt="" > English <span class="caret"></span>');
}


$("#siderbar-toogle").on("click", function () {
    console.log("asss");
    $(".logo-show").toggle();

    $(".logo-hidden").toggle();



});
$(window).on('hashchange', function (e) {
    var origEvent = e.originalEvent;
    ContentWatingOP("content-loader", 1);
    var urls = origEvent.newURL.split("#");
    var url = origEvent.newURL;
    if (urls.length > 1) {
        url = urls[1];
    }
    $(".navigation-main .active").removeClass("active");
    $(".navigation-main a").each(function (index, element) {
        if (($(element).attr("href")) == '#' + url) {
            $(element).closest("li").addClass("active");
            $(element).closest(".parent").addClass("active");
            $(".title-function").html($(element).text());
            document.title = $(element).text();
        }
    });
    $(".k-overlay").remove();
    $.ajax({
        cache: false,
        async: true,
        type: "get",
        url: url,
        success: function (data) {
            $(".k-list-container").remove();
            $(".k-window").remove();
            setTimeout(function () {
                $("#content-loader").html(data);
                $("#content-loader").unblock();
                $("#title-page").html($(".title-function").text());
            }, 1000);

        },
        error: function (xhr, status, error) {
            setTimeout(function () {
                location.reload();
            }, 1000);
        }
    });
});
$(document).ready(function () {
    $("#title-page").html($(".title-function").text());
    document.title = $(".title-function").text();
    $(".navigation-main li a").click(function () {
        if (!$(this).hasClass('has-ul')) {
            if ($(this).closest("li").hasClass("active")) {
                ContentWatingOP("content-loader", 1);
                $.ajax({
                    cache: false,
                    async: true,
                    type: "get",
                    url: location.hash.split("#")[1],
                    success: function (data) {
                        $(".k-list-container").remove();
                        $(".k-window").remove();
                        setTimeout(function () {
                            //$(".k-list-container").remove();
                            //$(".k-window").remove();
                            $("#content-loader").html(data);
                            $("#content-loader").unblock();

                        }, 1000);

                    },
                    error: function (xhr, status, error) {
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    }
                });
            }
        }
    });
});