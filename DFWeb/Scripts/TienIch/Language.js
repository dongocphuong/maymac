﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/Language";

    var dataPhongBan = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/read",
                type: "get",
                dataType: 'json'
            },
            create: {
                url: crudServiceBaseUrl + "/create",
                type: "post",
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            update: {
                url: crudServiceBaseUrl + "/update",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            destroy: {
                url: crudServiceBaseUrl + "/destroy",
                type: 'post',
                dataType: "json",
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            parameterMap: function (data, type) {
                if (type !== "read") {
                    if (type == "create") {
                        var model = {
                            langkey: data.models[0].langkey,
                            vn: data.models[0].vn,
                            cn: data.models[0].cn,
                            en: data.models[0].en
                        }
                        return { data: kendo.stringify(model) }
                    }
                    if (type == "update") {
                        var model = {
                            langkey: data.models[0].langkey,
                            vn: data.models[0].vn,
                            cn: data.models[0].cn,
                            en: data.models[0].en
                        }
                        return { data: kendo.stringify(model) }
                    }
                    else {
                        return { data: kendo.stringify(data.models[0]) }
                    }
                }
            }
        },
        sort: [
            { field: "langkey", dir: "asc" },
        ],
        batch: true,
        pageSize: 20,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "langkey",
                fields: {
                    langkey: { editable: true, nullable: false},
                    en: {
                        type: "string",
                    },
                    vn: {
                        type: "string",
                    },
                    cn: {
                        type: "string",
                    }
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataPhongBan,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: pageableAll,
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [
            {
                template: kendo.template($("#btnThem").html())
            }],
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "langkey", title: "langkey", filterable: FilterInTextColumn, attributes: { style: "text-align:left;" }
            },
            {
                field: "vn", title: "vn", filterable: FilterInTextColumn, attributes: { style: "text-align:left;" }
            },
            {
                field: "cn", title: "cn", filterable: FilterInTextColumn, attributes: { style: "text-align:left;" }
            },
            {
                field: "en", title: "en", filterable: FilterInTextColumn, attributes: { style: "text-align:left;" }
            },
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                command: [{ name: "edit", text: GetTextLanguage("sua") }, { name: "destroy", text: GetTextLanguage("xoa") }], width: 200
            },
        ],
        editable: {
            mode: "inline",

        }
    });

});