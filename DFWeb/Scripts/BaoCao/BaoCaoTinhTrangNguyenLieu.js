﻿$(document).ready(function () {
    var currentController = "/BaoCaoTinhTrangNguyenLieu";
    var dataSourceKhoTongVatTu = new kendo.data.DataSource({
        transport: {
            read: {
                url: currentController + "/GetAllData",
                dataType: 'json',
                type: 'POST',
                contentType: "application/json; charset=utf-8",
            }
        },
        batch: true,
        pageSize: 100,
        sort: [{ field: "TenNguyenLieu", dir: "asc" }],
        schema: {
            data: 'data',
            total: 'total',
            model: {
                id: "NguyenLieuID",
                fields: {
                    VatTuID: { type: "string" },
                    TenVatTu: { type: "string" },
                    TenDonVi: { type: "string" },
                    TonHienTai: { type: "number" },
                    GhiChu: { type: "string" }
                },
            }
        },
    });
    $("#grid").kendoGrid({
        dataSource: dataSourceKhoTongVatTu,
        height: window.innerHeight * 0.80,
        width: "100%",
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
             {
                 field: "AnhDaiDien", title: "Ảnh",
                 attributes: { style: "text-align:left;" },
                 filterable: FilterInTextColumn,
                 template: kendo.template($("#tplAnh").html()),
                 width: 70
             },
            {
                field: "TenNguyenLieu",
                title: "Tên nguyên liệu",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200
            },
            {
                field: "MaNguyenLieu",
                title: "Mã nguyên liệu",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 150
            },
            {
                field: "Type",
                title: "Loại Nguyên Liệu",
                filterable: {
                    cell: {
                        template: function (args) {
                            args.element.kendoDropDownList({
                                dataSource: [{ text: "Dệt", value: 1 }, { text: "May", value: 2 }, { text: "Phụ kiện", value: 3 }, ],
                                dataTextField: "text",
                                dataValueField: "value",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                template: kendo.template($("#tplLoaiNguyenLieu").html()),
                attributes: { style: "text-align:center;" },
                width: 150,
            },
            {
                field: "TenDonVi",
                title: GetTextLanguage("tendonvi"),
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 100
            },
            {
                field: "TinhTrang",
                title: "Tình trạng nguyên liệu",
                filterable: {
                    cell: {
                        template: function (args) {
                            args.element.kendoDropDownList({
                                dataSource: [{ text: "Cần nhập thêm", value: 1 }, { text: "Vẫn còn dư", value: 2 } ],
                                dataTextField: "text",
                                dataValueField: "value",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                template: kendo.template($("#tplTinhTrangNguyenLieu").html()),
                attributes: { style: "text-align:center;" },
                width: 150,
            },
            {
                field: "SoLuongTon",
                title: GetTextLanguage("tonhientai"),
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongTon"),
                width: 100
            },

        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
})

