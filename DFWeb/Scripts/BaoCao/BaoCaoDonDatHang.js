﻿$(document).ready(function () {
    var currentController = "/BaoCaoDonDatHang";
    var dataSourceKhoTongVatTu = new kendo.data.DataSource({
        transport: {
            read: {
                url: currentController + "/GetAllData",
                dataType: 'json',
                type: 'GET',
                contentType: "application/json; charset=utf-8",
                data: { DoiTacID: $("#inputDoiTac").val() }
            }
        },
        batch: true,
        pageSize: 100,
        sort: [{ field: "Size", dir: "asc" }],
        aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }],
        group: {
            field: "MaDonHang",
            title: "Mã đơn hàng",
            dir: "asc"
        },
        schema: {
            data: 'data',
            total: 'total',
            model: {},
        }
    });
    $("#grid").kendoGrid({
        dataSource: dataSourceKhoTongVatTu,
        height: window.innerHeight * 0.80,
        width: "100%",
        filterable: {
            mode: "row"
        },
        toolbar: kendo.template($("#tplToolbar").html()),
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 40,
                attributes: alignCenter
            },
            {
                field: "TenSanPham",
                title: "Tên Sản Phẩm",
                width: 80,
                attributes: alignCenter,
                filterable: false,
            },
            {
                field: "AnhDaiDien", title: "Ảnh",
                attributes: { style: "text-align:left;" },
                filterable: false,
                template: kendo.template($("#tplAnh").html()),
                width: 80
            },
            {
                field: "MaDonHang",
                title: "Mã đơn hàng",
                width: 200,
                attributes: alignLeft,
                filterable: false,
                hidden: true
            },
            {
                field: "DacDiem",
                title: "Đặc điểm",
                width: 200,
                attributes: alignLeft,
                filterable: false,
            },
            {
                field: "Size",
                title: "Size",
                width: 50,
                attributes: alignCenter,
                filterable: false,
            },
            {
                field: "Mau",
                title: "Màu chính",
                filterable: false,
                attributes: alignCenter,
                width: 80
            },
            {
                field: "MauPhoi",
                title: "Màu phối",
                filterable: false,
                attributes: alignCenter,
                width: 80
            },
            {
                field: "SoLuong",
                title: "Số lượng",
                width: 80,
                filterable: false,
                format: "{0:n0}",
                attributes: alignRight,
                footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n0')#</div>",
            },
            {
                field: "DonGia",
                title: "Giá bán",
                width: 80,
                filterable: false,
                format: "{0:n0}",
                attributes: alignRight,
            },
            {
                field: "ThanhTien",
                title: "Thành tiền",
                width: 80,
                filterable: false,
                format: "{0:n2}",
                attributes: alignRight,
                footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n0')#</div>",
            },
            {
                field: "ThoiGianGiaoHang",
                title: "Lịch giao",
                width: 100,
                filterable: false,
                attributes: alignCenter,
                template: formatToDate("ThoiGianGiaoHang")
            },
            {
                field: "YeuCauKhac",
                title: "Ghi chú",
                width: 100,
                filterable: false,
                attributes: alignCenter,
            },
        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    var dates = new Date(), years = dates.getFullYear(), months = dates.getMonth(), days = dates.getDay();
    $("#inputTuNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(years, 0, 1), change: function () {
            gridReload("grid", { DoiTacID: $("#inputDoiTac").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val(), Loai: 2, DonHangID: $("#inputDonHang").val() });
        }
    });
    $("#inputDenNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(), change: function () {
            gridReload("grid", { DoiTacID: $("#inputDoiTac").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val(), Loai: 2, DonHangID: $("#inputDonHang").val() });
        }
    });
    $("#inputDonHang").kendoDropDownList({
        dataTextField: "TenDonHang",
        dataValueField: "DonHangID",
        filter: "contains",
        optionLabel: "Chọn đơn hàng",
        dataSource: getDataDroplitwithParam(currentController, "/LayDanhSachDonHang", { DoiTacID: $("#inputDoiTac").val() }),
        headerTemplate: '<div class="dropdown-header k-widget k-header"><span>Tên đơn hàng</span></div>',
        height: 400,
        change: function () {
            var value = this.value();
            gridReload("grid", { DoiTacID: $("#inputDoiTac").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val(), Loai: 2, DonHangID: value });
        }
    });
    $("#inputDoiTac").kendoDropDownList({
        dataTextField: "TenDoiTac",
        dataValueField: "DoiTacID",
        filter: "contains",
        optionLabel: "Chọn đối tác",
        dataSource: getDataDroplitwithParam(currentController, "/LayDanhSachDoiTac", {}),
        change: function () {
            var value = this.value();
            $("#inputDonHang").data("kendoDropDownList").dataSource.read({ DoiTacID: value });
            gridReload("grid", { DoiTacID: value, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
            $.ajax({
                cache: false,
                async: false,
                type: "GET",
                url: currentController + "/GetThongTinDoiTac",
                data: { DoiTacID: value },
                success: function (data) {
                    var dt = data.data;
                    console.log(dt);
                    $("#inputMST").html(" " + dt.MaSoThue);
                    $("#inputSTK").html(" " + dt.SoTaiKhoan);
                    $("#inputDC").html(" " + dt.DiaChi);
                    $("#inputDT").html(" " + dt.SDT);
                    $("#inputEmail").html(" " + dt.Email);
                },
                error: function (xhr, status, error) {
                    showToast("error", GetTextLanguage("thatbai"), "Lấy thông tin khách hàng chưa thành công!");
                }
            });
        }
    });
    //---------------------------------------------------------------------------------------------------------------
    $("#grid").on("click", "#btnXuatExcelBaoCao", function () {
        if ($("#inputDoiTac").val() == null || $("#inputDoiTac").val() == "") {
            showToast("warning", "Chưa có thông tin đối tác", "Hãy chọn đối tác!");
        }
        else {
            var filer = [];
            var filers = $("#grid").data("kendoGrid").dataSource.filter();
            console.log($("#grid").data("kendoGrid").dataSource.filter());
            if (filers != null && filers != undefined) {
                for (var i = 0; i < filers.filters.length; i++) {
                    filers.filters[i].Operator = filers.filters[i].operator;
                    filer.push(filers.filters[i]);
                }
            }
            location.href = currentController + '/XuatExcelBaoCao?filters=' + JSON.stringify(filer) + '&DoiTacID=' + $("#inputDoiTac").val() + '&DonHangID=' + $("#inputDonHang").val() + '&TuNgay=' + $("#inputTuNgay").val() + '&DenNgay=' + $("#inputDenNgay").val();
        }
    })
})

