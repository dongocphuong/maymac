﻿$(document).ready(function () {
    var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();

    var currentController = "/BaoCaoSanPhamDaSanXuat";

    var dataSourceKhoTongVatTu = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: currentController + "/GetList",
                    dataType: 'json',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    data: { TuNgay: kendo.toString(new Date(y , m-1, 1), "dd/MM/yyyy"), DenNgay: kendo.toString(new Date(), "dd/MM/yyyy") }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            batch: true,
            pageSize: 20,
            sort: [{ field: "TenSanPham", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "SanPhamID",
                    fields: {
                        TenSanPham: { type: "string" },
                        MaSanPham: { type: "string" },
                        SoLuong: { type: "number" },
                        TenDonVi: { type: "string" }
                    },
                }
            },
        });

    $("#grid").kendoGrid({
        dataSource: dataSourceKhoTongVatTu,
        height: window.innerHeight * 0.80,
        width: "100%",
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        toolbar: [
            { template: kendo.template($("#tlbFilter").html()) }
        ],
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "AnhDaiDien", title: "Ảnh",
                attributes: { style: "text-align:left;" },
                filterable: FilterInTextColumn,
                template: kendo.template($("#tplAnh").html()),
                width: 70
            },
            {
                field: "TenSanPham",
                title: "Tên Sản Phẩm",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200
            },
            {
                field: "MaSanPham",
                title: "Mã Sản Phẩm",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 150
            },
            {
                field: "TenDonVi",
                title: GetTextLanguage("tendonvi"),
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 100
            },

            {
                field: "SoLuong",
                title: GetTextLanguage("soluong"),
                filterable: FilterInColumn,
                attributes: alignRight,
                format: "{0:n0}",
                width: 100
            },

        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    $("#inputTuNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(y - 5, m, 1), change: function () {
            var value = this.value();
            gridReload("grid", { TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay").val() });
        }
    });
    $("#inputDenNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(), change: function () {
            var value = this.value();
            gridReload("grid", { TuNgay: $("#inputTuNgay").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
        }
    });
})

