﻿$(document).ready(function () {
    var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();

    var currentController = "/BaoCaoCongNoDoiTacTheoThoiGian";

    var dataSourceKhoTongVatTu = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: currentController + "/GetAllData",
                    dataType: 'json',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    data: { TuNgay: kendo.toString(new Date(y, m - 1, 1), "dd/MM/yyyy"), DenNgay: kendo.toString(new Date(), "dd/MM/yyyy") }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            batch: true,
            pageSize: 50,
            sort: [{ field: "TenDoiTac", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "DoiTacID",
                    fields: {
                    },
                }
            },
        });

    $("#grid").kendoGrid({
        dataSource: dataSourceKhoTongVatTu,
        height: window.innerHeight * 0.80,
        width: "100%",
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        toolbar: [
            { template: kendo.template($("#tlbFilter").html()) }
        ],
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "KieuDoiTac",
                title: "Kiểu đối tác",
                filterable: {
                    cell: {
                        template: function (args) {
                            // create a DropDownList of unique values (colors)
                            args.element.kendoDropDownList({
                                dataSource: [{ id: 1, text: "Đối tác mua" }, { id: 2, text: "Đối tác bán" }],
                                dataTextField: "text",
                                dataValueField: "id",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                template: kendo.template($("#tempKieuDoiTac").html()),
                attributes: { style: "text-align:left;" }
            },
            {
                field: "TenDoiTac", title: GetTextLanguage("tendoitac"), filterable: FilterInTextColumn, attributes: { style: "text-align:left;" }
            },
            {
                field: "MaDoiTac", title: "Mã đối tác", filterable: FilterInTextColumn, attributes: { style: "text-align:left;" }
            },
            {
                field: "TongThu", title: GetTextLanguage("tongthu"), filterable: FilterInColumn, attributes: { style: "text-align:right;color: green;font-weight: bold;" }, format: "{0:n2}"
            },
            {
                field: "TongChi", title: GetTextLanguage("tongchi"), filterable: FilterInColumn, attributes: { style: "text-align:right;color: red;font-weight: bold;" }, format: "{0:n2}"
            },
            {
                field: "ThanhToanCongNo", title: "Đã thanh toán", filterable: FilterInColumn, attributes: { style: "text-align:right;color: orange;font-weight: bold;" }, format: "{0:n2}"
            },
            {
                field: "ChenhLenh", title: "Còn lại", filterable: FilterInColumn, attributes: { style: "text-align:right;color: blue;font-weight: bold;" }, format: "{0:n2}"
            },
        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    $("#inputTuNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(2000, 0, 1), change: function () {
            var value = this.value();
            gridReload("grid", { TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay").val() });
        }
    });
    $("#inputDenNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(), change: function () {
            var value = this.value();
            gridReload("grid", { TuNgay: $("#inputTuNgay").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
        }
    });
})

