﻿var storedFiles = [];

function loadslbuttonajax(data) {
    var strSlideInner = "<div class='text-center img-con'>";
    for (var i = 0; i < data.length; i++) {
        strSlideInner += "<div  style='background-image:url(" + data[i].Url + ");'  class='imgslider img-con ahihi1'><a rel='gallery' href='" + data[i].Url + "'><img src='" + data[i].Url + "' style='width: 100%;height: 100%;margin: 0;opacity: 0;'></a></div>";
        if (i == data.length - 1) { strSlideInner += "</div><div style='clear:both'></div>" }
    }
    return strSlideInner;
}

//function loadsldinnerajaxvattu(data) {
//    nslider = new NinjaSlider(nsOptions);
//    var strSlideInner = "<div id='ninja-slider'><div class='slider-inner'><ul>";
//    for (var i = 0 ; i < data.length; i++) {

//        strSlideInner += "<li>";
//        strSlideInner += "<a class='ns-img' href='" + data[i] + "'></a>"
//        strSlideInner += "</li>"
//    }
//    strSlideInner += "</ul><div id='fsBtn' class='fs-icon' title='Expand/Close'></div></div></div>";
//    return strSlideInner;
//}
var messagegrid = {
    display: "{0} - {1} " + GetTextLanguage("cua") + " {2}",
    empty: GetTextLanguage("khocoketqua"),
    page: GetTextLanguage("trang"),
    of: GetTextLanguage("cua") + " {0}",
    itemsPerPage: GetTextLanguage("ketqua_trang"),
    first: GetTextLanguage("vedau"),
    previous: GetTextLanguage("trangtruoc"),
    next: GetTextLanguage("tieptheo"),
    last: GetTextLanguage("vecuoi"),
    refresh: GetTextLanguage("lammoi"),
    allPages: GetTextLanguage("tatca")
}

function loadslbuttonvatu(data) {

    var strSlideInner = "";
    for (var i = 0; i < data.length; i++) {
        var name = data[i].split('/');

        strSlideInner += "<div class='imgslider' style='float:left;position:relative;margin:8px;'>";
        strSlideInner += "<div style='background-image:url(\"" + data[i] + "\"); margin:0px;' class='img-con ahihi2'><a rel='gallery' href='" + data[i] + "'><img src='" + data[i] + "' style='width: 100%;height: 100%;margin: 0;opacity: 0;'></a></div>";
        strSlideInner += "<div data-file='" + name[name.length - 1] + "' title='Xóa ảnh'  class='rev removeLoad'><i class='fa fa-times' aria-hidden='true'></i></div></div>";
        if (i == data.length - 1) { strSlideInner += "</div>" }
    }
    return strSlideInner;
}


function closeModel(idform) {
    $('#' + idform).data("kendoWindow").close();
}
//loaddata
function getDataSourceGrid(crudServiceBaseUrl, url, param, groups) {
    var dataSources = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + url,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: param
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 20,
        group: groups,
        schema: {
            data: "data",
            total: "total"
        }
    });

    return dataSources;
}

function getDataSourceGridSum(crudServiceBaseUrl, url, param, groups, columnsum) {
    var dataSources = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + url,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: param
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 20,
        group: groups,
        sort: [{ field: "TenVatTu", dir: "asc" }],
        schema: {
            data: "data",
            total: "total"
        },
        aggregate: columnsum,
    });

    return dataSources;
}
function getDataSourceGridSumNoPagination(crudServiceBaseUrl, url, param, groups, columnsum) {
    var dataSources = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + url,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: param
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        group: groups,
        sort: [{ field: "TenVatTu", dir: "asc" }],
        schema: {
            data: "data",
            total: "total"
        },
        aggregate: columnsum,
    });

    return dataSources;
}


function getDataSourceGridNoneGroup(crudServiceBaseUrl, url, param) {
    var dataSources = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + url,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: param
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 20,
        //group: groups,
        schema: {
            data: "data",
            total: "total"
        }
    });

    return dataSources;
}


function getDataDroplitwithParam(crudServiceBaseUrl, url, param) {
    var dataSourceObj;
    dataSourceObj = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + url,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: param
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    });
    return dataSourceObj;
}

function getDataDroplit(crudServiceBaseUrl, url) {
    var dataSourceObj;
    dataSourceObj = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + url,
                dataType: "json",
                type: "get"
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    });
    return dataSourceObj;
}

function getDataSourceGridOrder(crudServiceBaseUrl, url, param, groups) {
    var dataSources = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + url,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: param
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 20,


        sort: { field: "ThoiGianMota", dir: "desc" },
        group: groups,
        schema: {
            data: "data",
            total: "total",
            model: {
                fields: {
                    ThoiGianMota: { type: "date", format: "{0:dd/MM/yyyy}" },
                }
            }
        }
    });

    return dataSources;
}

function gridReload(grid, parram) {
    $('#' + grid).data('kendoGrid').dataSource.read(parram);
    $('#' + grid).data('kendoGrid').refresh();

}

function showToast(loai, title, message) {
    toastr.options.positionClass = "toast-top-right";
    toastr.options.closeButton = true;
    toastr[loai](message, title, { timeOut: 1000 });
}

function altReturn(data) {
    if (data.code == "success") {
        showToast("success", GetTextLanguage("thanhcong"), data.message);
    }
    else if (data.code == "warning") {
        showToast("warning", GetTextLanguage("canhbao"), data.message)
    }
    else {
        showToast("error", GetTextLanguage("thatbai"), data.message);
    }
}

function setTitleWindow(id, title) {
    $('#' + id + '_wnd_title').html(title);
}

$('.sidebar-toggle').click(function () {
    setTimeout(function () {
        $('#grid').data('kendoGrid').refresh();
    }, 270)
});

function formatSo(so) { /* dinh dang so */

    nStr = so + '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    x2 = "" + (x2 / 1).toFixed(2);
    x2 = x2.substring(1, 10);

    return x1 + x2;
}


function CheckNumeric() {

    return event.keyCode >= 46 && event.keyCode <= 57;
}

function resizeWindow(content, wrap) {
    $("#" + content).css("height", $("#" + wrap).height() - 60);
    $(window).resize(function () {
        setTimeout(function () {
            $("#" + content).css("height", $("#" + wrap).height() - 60);
            $("#" + content + " .k-grid-content").css("height", $("#" + wrap).height() - 213);
        }, 300);
        //setTimeout(function () {  }, 300);
    });
}

function resizeWindow3(content, wrap) {
    $("#" + content).css("height", $("#" + wrap).height() - 5);
    $(window).resize(function () {
        setTimeout(function () {
            $("#" + content).css("height", $("#" + wrap).height());
            $("#" + content + " .k-grid-content").css("height", $("#" + wrap).height() - 158);
        }, 300);
        //setTimeout(function () {  }, 300);
    });
}

function resizeWindow2(content, wrap, height1, height2) {
    $("#" + content).css("height", $("#" + wrap).height() - height1);
    $(window).resize(function () {
        setTimeout(function () {
            $("#" + content).css("height", $("#" + wrap).height() - height1);
            $("#" + content + " .k-grid-content").css("height", $("#" + wrap).height() - height2);
        }, 300);
        //setTimeout(function () {  }, 300);
    });
}

function tooTuChoi() {
    var str = "<a id='btnTuChoi' class='btn btn-warning pull-right' style='margin-left:10px;'><i class='fa fa-ban' aria-hidden='true'></i> " + GetTextLanguage("tuchoi") + "</a>";
    return str;
}
function tooXoa() {
    var str = "<a id='btnXoa' class='btn btn-danger pull-right' style='margin-left:10px;'><i class='fa fa-times' aria-hidden='true'></i> " + GetTextLanguage("xoa") + "</a>";
    return str;
}
function tooDuyet() {
    var str = "<a id='btnDuyet' class='btn btn-primary pull-right' style='margin-left:10px;'><i class='fa fa-check' aria-hidden='true'></i> " + GetTextLanguage("duyet") + "</a>";
    return str;
}
function tooLuu() {
    var str = "<a id='saveas' class='btn btn-success pull-right' style='margin-left:10px;'><i class='fa fa fa-floppy-o' aria-hidden='true'></i> " + GetTextLanguage("luuphieu") + "</a>";
    return str;
}
function toolDaDuyet() {
    var str = "<a  class='btn btn-success pull-right' style='margin-left:10px;'><i class='fa fa fa-check' aria-hidden='true'></i> " + GetTextLanguage("phieudaduocduyet") + "</a>";
    return str;
}

//----------------Upload file phiếu vạt tư

function loadimg(input) {
    //$('.dshinhanhadd').empty();
    if (input.files) {
        var filesAmount = input.files.length;

        for (var i = 0; i < filesAmount; i++) {
            var reader = new FileReader();
            if (input.files[i].type == "image/png" || input.files[i].type == "image/jpg" || input.files[i].type == "image/jpeg" || input.files[i].type == "image/gif") {
                storedFiles.push(input.files[i]);
                var fileName = input.files[i].name;
                reader.onload = function (event) {
                    var html = "<div style='background-image:url(\"" + event.target.result + "\");' class='img-con ahihi2'><div data-file='" + fileName + "'  title='Xóa ảnh'  class='rev removeUpload'><i class='fa fa-times' aria-hidden='true'></i></div></div>";
                    $('.dshinhanhadd').append(html);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }

    }
}

function removeFile(e) {
    var file = $(this).data("file");
    for (var i = 0; i < storedFiles.length; i++) {
        if (storedFiles[i].name === file) {
            storedFiles.splice(i, 1);
            break;
        }
    }
    $(this).parent().remove();
}

$("body").on("click", ".removeUpload", removeFile);

function showslideimg(item) {
    $('.imgslider img').fullscreenslides();

    // All events are bound to this container element
    var $container = $('#fullscreenSlideshowContainer');

    $container
        //This is triggered once:
        .bind("init", function () {

            // The slideshow does not provide its own UI, so add your own
            // check the fullscreenstyle.css for corresponding styles
            $container
                .append('<div class="ui" id="fs-close">&times;</div>')
                .append('<div class="ui" id="fs-loader">Đang tải...</div>')
                .append('<div class="ui" id="fs-prev">&lt;</div>')
                .append('<div class="ui" id="fs-next">&gt;</div>')
                .append('<div class="ui" id="fs-caption"><span></span></div>')
                .append('<div class="ui" id="xoaytrai">&gt;</div>')
                .append('<div class="ui" id="xoayphai"><span></span></div>');

            // Bind to the ui elements and trigger slideshow events
            $('#fs-prev').click(function () {
                // You can trigger the transition to the previous slide
                $container.trigger("prevSlide");
            });
            $('#fs-next').click(function () {
                // You can trigger the transition to the next slide
                $container.trigger("nextSlide");
            });
            $('#fs-close').click(function () {
                // You can close the slide show like this:
                $container.trigger("close");
            });

        })
        // When a slide starts to load this is called
        .bind("startLoading", function () {
            // show spinner
            $('#fs-loader').show();
        })
        // When a slide stops to load this is called:
        .bind("stopLoading", function () {
            // hide spinner
            $('#fs-loader').hide();
        })
        // When a slide is shown this is called.
        // The "loading" events are triggered only once per slide.
        // The "start" and "end" events are called every time.
        // Notice the "slide" argument:
        .bind("startOfSlide", function (event, slide) {
            // set and show caption
            $('#fs-caption span').text(slide.title);
            $('#fs-caption').show();
        })
        // before a slide is hidden this is called:
        .bind("endOfSlide", function (event, slide) {
            $('#fs-caption').hide();
        });
}

