﻿$(document).ready(function () {
    //Init
    var dataimg = new FormData();
    var dataavatar = new FormData();
    var crudServiceBaseUrl = "/DMNguyenLieu";
    var dataLoaiNguyenLieu = [
        { text: "Dệt", value: 1 },
        { text: "May", value: 2 },
        { text: "Phụ kiện", value: 3 },
    ];
    var dataDMNguyenLieu = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetAllDataNguyenLieu",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {}
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        //group: {
        //    field: "TenNhomNguyenLieu",
        //    dir: "asc"
        //},
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "NguyenLieuID",
                fields: {
                    NguyenLieuID: { editable: false, nullable: true },
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataDMNguyenLieu,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [{ template: kendo.template($("#btnThem").html()) }],
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
            {
                field: "AnhDaiDien", title: "Ảnh",
                attributes: { style: "text-align:left;" },
                filterable: FilterInTextColumn,
                template: kendo.template($("#tplAnh").html()),
                width: 70
            },
            {
                field: "MaNguyenLieu",
                title: "Mã nguyên liệu",
                width: 100,
                attributes: alignCenter,
                filterable: FilterInTextColumn,
            },
            {
                field: "TenNhomNguyenLieu",
                title: "Tên nhóm nguyên liệu",
                width: 100,
                attributes: alignCenter,
                filterable: FilterInTextColumn,
            },
            {
                field: "TenNguyenLieu",
                title: "Tên Nguyên Liệu",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
                width: 400
            },
            {
                field: "Type",
                title: "Loại Nguyên Liệu",
                filterable: {
                    cell: {
                        template: function (args) {
                            args.element.kendoDropDownList({
                                dataSource: [{ text: "Dệt", value: 1 }, { text: "May", value: 2 }, { text: "Phụ kiện", value: 3 }, ],
                                dataTextField: "text",
                                dataValueField: "value",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                template: kendo.template($("#tplLoaiNguyenLieu").html()),
                attributes: { style: "text-align:center;" },
                width: 100,
            },
            {
                field: "TenDonVi",
                title: "Đơn vị",
                width: 80,
                attributes: alignCenter,
                filterable: FilterInTextColumn
            },
        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    $("#grid").on("dblclick", "td", function () {
        var rows = $(this).closest("tr"),
            grids = $("#grid").data("kendoGrid"),
            dataItems = grids.dataItem(rows);
        if (!rows.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            var dataobjectload = null;
            var lstHinhAnh = [];
            CreateModalWithSize("mdlThemMoiNguyenLieu", "70%", "80%", "tplThemMoiNguyenLieu", "Chi tiết nguyên liệu");
            dataimg = new FormData();
            dataavatar = new FormData();
            $("#HeSoQuyDoi").autoNumeric(autoNumericOptionsMoney);
            $("#btnXoa").removeClass("hidden");
            $.ajax({
                url: crudServiceBaseUrl + "/GetDataNguyenLieuID",
                method: "GET",
                data: { data: dataItems.NguyenLieuID },
                success: function (data) {
                    dataobjectload = data.data[0];
                    $("#cbbDonVi").kendoDropDownList({
                        autoBind: true,
                        dataTextField: 'TenDonVi',
                        dataValueField: 'DonViId',
                        filter: "contains",
                        optionLabel: GetTextLanguage("chondonvi"),
                        dataSource: getDataDroplit("/DMDonVi", "/read")
                    });
                    $("#NhomNguyenLieuID").kendoDropDownList({
                        autoBind: true,
                        dataTextField: 'TenNhomNguyenLieu',
                        dataValueField: 'NhomNguyenLieuID',
                        filter: "contains",
                        optionLabel: "Chọn nhóm nguyên liệu",
                        dataSource: getDataDroplit("/NhomNguyenLieu", "/GetAllData")
                    });
                    var viewModel = kendo.observable({
                        NguyenLieuID: dataobjectload == null ? "00000000-0000-0000-0000-000000000000" : dataobjectload.NguyenLieuID,
                        MaNguyenLieu: dataobjectload == null ? "" : dataobjectload.MaNguyenLieu,
                        //TenNguyenLieu: dataobjectload == null ? "" : dataobjectload.TenNguyenLieu,
                        DonViID: dataobjectload == null ? "" : dataobjectload.DonViID,
                        DanhSachHinhAnhs: dataobjectload == null ? "" : dataobjectload.DanhSachHinhAnhs,
                        AnhDaiDien: dataobjectload == null ? "" : dataobjectload.AnhDaiDien,
                        HeSoQuyDoi: dataobjectload == null ? "1" : dataobjectload.HeSoQuyDoi,
                        Type: dataobjectload == null ? "" : dataobjectload.Type,
                        MaMau: dataobjectload == null ? "" : dataobjectload.MaMau,
                        NhomNguyenLieuID: dataobjectload == null ? "" : dataobjectload.NhomNguyenLieuID,
                    });
                    kendo.bind($("#frmThemMoiNguyenLieu"), viewModel);
                    if (viewModel.AnhDaiDien != null && viewModel.AnhDaiDien != "") {
                        $(".imgavatar").attr("src", localQLVT + viewModel.AnhDaiDien);
                        $(".imgavatar").attr("data-file", viewModel.AnhDaiDien);
                    }
                    else {
                        $(".imgavatar").attr("src", "/Content/no-image.jpg");
                        $(".imgavatar").attr("data-file", "no-image.jpg");
                    }
                    showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                    //image
                    var lt = dataobjectload.DanhSachHinhAnhs;
                    if (lt == "" || lt == null) {
                        var lst = lt;
                    }
                    else {
                        var lst = lt.split(',');
                        if (lst.length > 0 && lst[0] != "") {
                            for (var i = 0; i < lst.length; i++) {
                                lstHinhAnh.push({ img: lst[i], name: lst[i] });
                            }
                            var html = "";
                            for (var i = 0; i < lstHinhAnh.length; i++) {
                                if (lstHinhAnh[i].img.match(/\.(jpg|jpeg|png|gif)$/)) {
                                    html += "<div class='imgslider' style='float:left; position:relative; width:70px;height:70px; margin:5px;'>";
                                    html += "<div style='margin:0px;' class='img-con ahihi3 img-thumbnail'><a rel='gallery' href='" + (localQLVT + lstHinhAnh[i].img) + "'>";
                                    html += "<img src='" + localQLVT + lstHinhAnh[i].img + "'  data-file='" + (lstHinhAnh[i].img) + "' style='width: 70px;height:70px;margin: 0;opacity: 1;'></a></div>";
                                    html += "</div>";
                                }
                            }
                            $('.dshinhanhadd').append(html);
                            showslideimg5($('#frmThemMoiNguyenLieu'));
                        }
                    }
                    //image
                    ThemNhomNguyenLieuNhanh();
                    ThemDonViNhanh();
                    btnLuuDMNguyenLieu();
                },
                error: function (xhr, status, error) {
                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                }
            });
            $("#btnXoa").click(function () {
                var x = confirm("Bạn có chắc chắn muốn xóa danh mục này không?");
                if (x) {
                    ContentWatingOP("mdlThemMoiNguyenLieu", 0.9);
                    $.ajax({
                        url: crudServiceBaseUrl + "/XoaDMNguyenLieu",
                        method: "POST",
                        data: { data: dataItems.NguyenLieuID },
                        success: function (data) {
                            showToast(data.code, data.code, data.message);
                            if (data.code == "success") {
                                $("#mdlThemMoiNguyenLieu").unblock();
                                $(".windows8").css("display", "none");
                                closeModel("mdlThemMoiNguyenLieu");
                                gridReload("grid", {});
                            }
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $("#mdlThemMoiNguyenLieu").unblock();
                            $(".windows8").css("display", "none");
                            gridReload("grid", {});
                        }
                    });
                }
            })
        }
    });

    $("#grid").on("click", "#btnAdd", function () {
        CreateModalWithSize("mdlThemMoiNguyenLieu", "70%", "80%", "tplThemMoiNguyenLieu", "Thêm mới nguyên liệu");
        dataimg = new FormData();
        dataavatar = new FormData();
        $("#HeSoQuyDoi").autoNumeric(autoNumericOptionsMoney);
        $("#cbbDonVi").kendoDropDownList({
            autoBind: false,
            dataTextField: 'TenDonVi',
            dataValueField: 'DonViId',
            filter: "contains",
            optionLabel: GetTextLanguage("chondonvi"),
            dataSource: getDataDroplit("/DMDonVi", "/read"),
        });
        $("#NhomNguyenLieuID").kendoDropDownList({
            autoBind: true,
            dataTextField: 'TenNhomNguyenLieu',
            dataValueField: 'NhomNguyenLieuID',
            filter: "contains",
            optionLabel: "Chọn nhóm nguyên liệu",
            dataSource: getDataDroplit("/NhomNguyenLieu", "/GetAllData")
        });
        ThemDonViNhanh();
        ThemNhomNguyenLieuNhanh();
        btnLuuDMNguyenLieu();
    })
    
    function btnLuuDMNguyenLieu() {
        $("#Type").kendoDropDownList({
            autoBind: true,
            dataTextField: 'text',
            dataValueField: 'value',
            filter: "contains",
            optionLabel: "Chọn loại nguyên liệu",
            dataSource: dataLoaiNguyenLieu
        });
        //Image---------------<
        $("#frmThemMoiNguyenLieu #files_0").change(function () {
            for (var x = 0; x < this.files.length; x++) {
                dataimg.append("uploads", this.files[x]);
            };
            loadimgSuaChua5(this, $("#frmThemMoiNguyenLieu"));
            $("#frmThemMoiNguyenLieu #files_0").val('').clone(true);
        });
        //avatar
       
        $("#frmThemMoiNguyenLieu #file_1").change(function () {
            dataavatar = new FormData();
            for (var x = 0; x < this.files.length; x++) {
                dataavatar.append("uploads", this.files[x]);
            };
            loadimgavatar(this, $("#frmThemMoiNguyenLieu"));
            $("#frmThemMoiNguyenLieu #file_1").val('').clone(true);
        })
        //Image------------------------>
        $("#btnLuuThemMoi").click(function () {
            var validator = $("#frmThemMoiNguyenLieu").kendoValidator().data("kendoValidator");
            if (validator.validate()) {
                //image
                var hinhanh = "";
                var ins = dataimg.getAll("uploads").length;
                var DSHinhAnh = [];
                $('.dshinhanhadd .imgslider').each(function (index2, element2) {
                    if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                        DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                    }
                });
                if (ins > 0) {
                    var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", dataimg.getAll("uploads"), $(".dshinhanhadd", $('#frmThemMoiNguyenLieu'))).split(',');
                    for (var k = 0; k < strUrl.length; k++) {
                        DSHinhAnh.push({ Url: strUrl[k] });
                    }
                }
                for (var i = 0; i < DSHinhAnh.length; i++) {
                    if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                        hinhanh += DSHinhAnh[i].Url + ",";
                    }
                }
                //avatar
                var inss = dataavatar.getAll("uploads").length;
                var anhdaidien = $('.anhdaidien img').attr("data-file");
                if (inss > 0) {
                    var strUrl = uploadAvatarFileParagamWidthFile("/upload" + "/Uploadfiles", dataavatar.getAll("uploads"), $(".anhdaidien", $('#frmThemMoiNguyenLieu')));
                    anhdaidien = strUrl;
                }
                //image
                var dataNguyenLieu = {
                    NguyenLieuID: $("#NguyenLieuID").val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#NguyenLieuID").val(),
                    MaNguyenLieu: $("#MaNguyenLieu").val(),
                    TenNguyenLieu: $("#NhomNguyenLieuID").data("kendoDropDownList").text(),
                    NhomNguyenLieuID: $("#NhomNguyenLieuID").val(),
                    MaMau: $("#MaMau").val(),
                    DonViId: $("#cbbDonVi").val(),
                    DanhSachHinhAnhs: hinhanh.substring(0, hinhanh.length - 1),
                    AnhDaiDien: anhdaidien,
                    HeSoQuyDoi: $("#HeSoQuyDoi").autoNumeric('get') == null ? "1" : $("#HeSoQuyDoi").autoNumeric('get'),
                    Type: $("#Type").val()
                }
                ContentWatingOP("mdlThemMoiNguyenLieu", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/AddOrUpdateDMNguyenLieu",
                    method: "post",
                    data: { data: JSON.stringify(dataNguyenLieu) },
                    success: function (data) {
                        showToast(data.code, data.code, data.message);
                        if (data.code == "success") {
                            $("#mdlThemMoiNguyenLieu").unblock();
                            $(".windows8").css("display", "none");
                            closeModel("mdlThemMoiNguyenLieu");
                            gridReload("grid", {});
                        } else {
                            $("#mdlThemMoiNguyenLieu").unblock();
                        }
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlThemMoiNguyenLieu").unblock();
                        $(".windows8").css("display", "none");
                        gridReload("grid", {});
                    }
                });
            }
        });
    }
    //---------------------------------------------------------------------------------------------------------------
    $(".btnexcel").on("click", function () {
        var filer = [];
        var filers = $("#grid").data("kendoGrid").dataSource.filter();
        if (filers != null && filers != undefined) {
            for (var i = 0; i < filers.filters.length; i++) {
                filers.filters[i].Operator = filers.filters[i].operator;
                filer.push(filers.filters[i]);
            }
        }
        location.href = crudServiceBaseUrl + '/XuatExcelDMNguyenLieu?filters=' + JSON.stringify(filer);
    });
    $(".btnTemplate").on("click", function () {
        location.href = '/DMNguyenLieu/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();
    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "50%",
                    height: innerHeight * 0.7,
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                var grid = $("#gridthem").kendoGrid({
                    dataSource: data,
                    editable: true,
                    height: innerHeight * 0.5,
                    width: "100%",
                    dataBound: function () {
                        var rows = this.items();
                        dem = 0;
                        $(rows).each(function () {
                            dem++;
                            var rowLabel = $(this).find(".stt");
                            $(rowLabel).html(dem);
                            if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                                dem = 0;
                            }
                        });
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 60,
                        },
                        {
                            field: "TenNguyenLieu",
                            title: "Tên nguyên liệu",
                            filterable: FilterInTextColumn,
                            attributes: alignLeft
                        },
                        {
                            field: "MaNguyenLieu",
                            title: "Mã Nguyên Liệu",
                            filterable: FilterInTextColumn,
                            attributes: alignCenter
                        },
                        {
                            field: "TenDonVi",
                            title: GetTextLanguage("donvitinh"),
                            filterable: FilterInTextColumn,
                            attributes: alignCenter
                        },
                        { command: "destroy", title: "&nbsp;", width: "150px" }
                    ],
                });
                $(".btn-import").click(function () {
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/DMNguyenLieu/ImportDMNguyenLieu",
                        method: "post",
                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()) },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#grid").data("kendoGrid").dataSource.read();
                                $("#grid").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    closeModel("mdlThemFileExcel");
                });
            }
        }
        objXhr3.open("POST", "/DMNguyenLieu/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
    });
    //---------------------------------------------------------------------------------------------------------------
    function ThemNhomNguyenLieuNhanh() {
        $("#btnThemNhomNguyenLieuNhanh").click(function () {
            CreateModalWithSize("mdlThemNhanhNhomNguyenLieu", "25%", "20%", "tplThemNhanhNhomNguyenLieu", "Thêm đơn vị");
            $("#btnLuuNhanhNhomNguyenLieu").click(function () {
                var validate = $("#TenNhomNguyenLieu").kendoValidator().data("kendoValidator");
                if (validate.validate()) {
                    var data = {
                        NhomNguyenLieuID: $("#NhomNguyenLieuID").val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#NhomNguyenLieuID").val(),
                        TenNhomNguyenLieu: $("#TenNhomNguyenLieu").val()
                    }
                    ContentWatingOP("mdlThemNhanhNhomNguyenLieu", 0.9);
                    $.ajax({
                        url: crudServiceBaseUrl + "/AddOrUpdate",
                        method: "post",
                        data: { data: JSON.stringify(data) },
                        success: function (data) {
                            if (data.code == "success") {
                                showToast("success", "Thành công", "Thêm mới thành công");
                                $("#mdlThemNhanhNhomNguyenLieu").unblock();
                                closeModel("mdlThemNhanhNhomNguyenLieu");
                                $("#NhomNguyenLieuID").data("kendoDropDownList").dataSource.read();
                                $("#NhomNguyenLieuID").data("kendoDropDownList").refresh();
                            }
                            if (data.code == "error") {
                                showToast("warning", "Không thành công", data.message);
                                $("#mdlThemNhanhNhomNguyenLieu").unblock();
                            }
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $("#mdlThemNhanhNhomNguyenLieu").unblock();
                            $(".windows8").css("display", "none");
                            gridReload("grid", {});
                        }
                    });
                }
            });
        });
    }
    function ThemDonViNhanh() {
        $("#btnThemDonViNhanh").click(function () {
            CreateModalWithSize("mdlThemNhanhDonVi", "25%", "20%", "tplThemNhanhDonVi", "Thêm đơn vị");
            $("#btnLuuNhanhDonVi").click(function () {
                var validate = $("#TenDonVi").kendoValidator().data("kendoValidator");
                if (validate.validate()) {
                    ContentWatingOP("mdlThemNhanhDonVi", 0.9);
                    $.ajax({
                        url: crudServiceBaseUrl + "/ThemNhanhDonVi",
                        method: "post",
                        data: { data: $("#TenDonVi").val() },
                        success: function (data) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemNhanhDonVi").unblock();
                                $(".windows8").css("display", "none");
                                closeModel("mdlThemNhanhDonVi");
                                $("#cbbDonVi").data("kendoDropDownList").dataSource.read();
                                $("#cbbDonVi").data("kendoDropDownList").refresh();
                            }, 1000);
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $("#mdlThemNhanhDonVi").unblock();
                            $(".windows8").css("display", "none");
                        }
                    });
                }
            });
        });
    }
});
