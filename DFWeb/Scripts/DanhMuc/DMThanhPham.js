﻿$(document).ready(function () {
    //Init
    var dataimg = new FormData();
    var dataavatar = new FormData();
    var crudServiceBaseUrl = "/DMThanhPham";
    var dataDMThanhPham = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetAllDataThanhPham",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {}
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "ThanhPhamID",
                fields: {
                    ThanhPhamID: { editable: false, nullable: true },
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataDMThanhPham,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [{ template: kendo.template($("#btnThem").html()) }],
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
             {
                 field: "AnhDaiDien", title: "Ảnh",
                 attributes: { style: "text-align:left;" },
                 filterable: FilterInTextColumn,
                 template: kendo.template($("#tplAnh").html()),
                 width: 70
             },
            {
                field: "MaThanhPham",
                title: "Mã thành phẩm",
                width: 100,
                attributes: alignCenter,
                filterable: FilterInTextColumn,
            },
            {
                field: "TenThanhPham",
                title: "Tên thành phẩm",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
                width: 200
            },
            {
                field: "MaSanPham",
                title: "Mã sản phẩm",
                width: 100,
                attributes: alignCenter,
                filterable: FilterInTextColumn,
            },
            {
                field: "TenSanPham",
                title: "Tên sản phẩm",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
                width: 200
            },
            {
                field: "TenDonVi",
                title: "Đơn vị",
                width: 80,
                attributes: alignCenter,
                filterable: FilterInTextColumn
            },
             {
                 field: "DonGia",
                 title: "Đơn giá",
                 width: 150,
                 attributes: alignRight,
                 format: "{0:n2}",
                 filterable: FilterInColumn
             },

        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });

    $("#grid").on("dblclick", "td", function () {
        var rows = $(this).closest("tr"),
            grids = $("#grid").data("kendoGrid"),
            dataItems = grids.dataItem(rows);
        if (!rows.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            var dataobjectload = null;
            var lstHinhAnh = [];
            CreateModalWithSize("mdlThemMoiThanhPham", "90%", "90%", "tplThemMoiThanhPham", "Chi tiết thành phẩm");
            dataimg = new FormData();
            dataavatar = new FormData();
            $("#btnXoa").removeClass("hidden");
            $.ajax({
                url: crudServiceBaseUrl + "/GetDataThanhPhamID",
                method: "GET",
                data: { data: dataItems.ThanhPhamID },
                success: function (data) {
                    dataobjectload = data.data[0];
                    $("#DonGia").autoNumeric(autoNumericOptionsMoney);
                    LoadDinhMuc(dataobjectload.ThanhPhamID);
                    $("#cbbDonVi").kendoDropDownList({
                        autoBind: true,
                        dataTextField: 'TenDonVi',
                        dataValueField: 'DonViId',
                        filter: "contains",
                        optionLabel: GetTextLanguage("chondonvi"),
                        dataSource: getDataDroplit("/DMDonVi", "/read")
                    });
                    var viewModel = kendo.observable({
                        ThanhPhamID: dataobjectload == null ? "00000000-0000-0000-0000-000000000000" : dataobjectload.ThanhPhamID,
                        MaThanhPham: dataobjectload == null ? "" : dataobjectload.MaThanhPham,
                        TenThanhPham: dataobjectload == null ? "" : dataobjectload.TenThanhPham,
                        DonViID: dataobjectload == null ? "" : dataobjectload.DonViID,
                        DanhSachHinhAnhs: dataobjectload == null ? "" : dataobjectload.DanhSachHinhAnhs,
                        AnhDaiDien: dataobjectload == null ? "" : dataobjectload.AnhDaiDien,
                        DonGia: dataobjectload == null ? "0" : dataobjectload.DonGia
                    });
                    kendo.bind($("#frmThemMoiThanhPham"), viewModel);
                    if (viewModel.AnhDaiDien != null && viewModel.AnhDaiDien != "") {
                        $(".imgavatar").attr("src", localQLVT + viewModel.AnhDaiDien);
                        $(".imgavatar").attr("data-file", viewModel.AnhDaiDien);
                    }
                    else {
                        $(".imgavatar").attr("src", "/Content/no-image.jpg");
                        $(".imgavatar").attr("data-file", "no-image.jpg");
                    }
                    showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                    //image
                    var lt = dataobjectload.DanhSachHinhAnhs;
                    if (lt == "" || lt == null) {
                        var lst = lt;
                    }
                    else {
                        var lst = lt.split(',');
                        if (lst.length > 0 && lst[0] != "") {
                            for (var i = 0; i < lst.length; i++) {
                                lstHinhAnh.push({ img: lst[i], name: lst[i] });
                            }
                            var html = "";
                            for (var i = 0; i < lstHinhAnh.length; i++) {
                                if (lstHinhAnh[i].img.match(/\.(jpg|jpeg|png|gif)$/)) {
                                    html += "<div class='imgslider' style='float:left; position:relative; width:70px;height:70px; margin:5px;'>";
                                    html += "<div style='margin:0px;' class='img-con ahihi3 img-thumbnail'><a rel='gallery' href='" + (localQLVT + lstHinhAnh[i].img) + "'>";
                                    html += "<img src='" + localQLVT + lstHinhAnh[i].img + "'  data-file='" + (lstHinhAnh[i].img) + "' style='width: 70px;height:70px;margin: 0;opacity: 1;'></a></div>";
                                    html += "</div>";
                                }
                            }
                            $('.dshinhanhadd').append(html);
                            showslideimg5($('#frmThemMoiThanhPham'));
                        }
                    }
                    //image
                    btnLuuDMThanhPham();
                },
                error: function (xhr, status, error) {
                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                }
            });
            $("#btnXoa").click(function () {
                var x = confirm("Bạn có chắc chắn muốn xóa danh mục này không?");
                if (x) {
                    ContentWatingOP("mdlThemMoiThanhPham", 0.9);
                    $.ajax({
                        url: crudServiceBaseUrl + "/XoaDMThanhPham",
                        method: "POST",
                        data: { data: dataItems.ThanhPhamID },
                        success: function (data) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            $("#mdlThemMoiThanhPham").unblock();
                            $(".windows8").css("display", "none");
                            closeModel("mdlThemMoiThanhPham");
                            gridReload("grid", {});
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $("#mdlThemMoiThanhPham").unblock();
                            $(".windows8").css("display", "none");
                            gridReload("grid", {});
                        }
                    });
                }
            })
        }
    });


    $("#grid").on("click", "#btnAdd", function () {
        CreateModalWithSize("mdlThemMoiThanhPham", "90%", "90%", "tplThemMoiThanhPham", "Thêm mới thành phẩm");
        dataimg = new FormData();
        dataavatar = new FormData();
        $("#DonGia").autoNumeric(autoNumericOptionsMoney);
        $("#cbbDonVi").kendoDropDownList({
            autoBind: false,
            dataTextField: 'TenDonVi',
            dataValueField: 'DonViId',
            filter: "contains",
            optionLabel: GetTextLanguage("chondonvi"),
            dataSource: getDataDroplit("/DMDonVi", "/read"),
        });
        $("#btnThemDonViNhanh").click(function () {
            CreateModalWithSize("mdlThemNhanhDonVi", "25%", "20%", "tplThemNhanhDonVi", "Thêm đơn vị");
            $("#btnLuuNhanhDonVi").click(function () {
                var validate = $("#TenDonVi").kendoValidator().data("kendoValidator");
                if (validate.validate()) {
                    ContentWatingOP("mdlThemNhanhDonVi", 0.9);
                    $.ajax({
                        url: crudServiceBaseUrl + "/ThemNhanhDonVi",
                        method: "post",
                        data: { data: $("#TenDonVi").val() },
                        success: function (data) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemNhanhDonVi").unblock();
                                $(".windows8").css("display", "none");
                                closeModel("mdlThemNhanhDonVi");
                                $("#cbbDonVi").data("kendoDropDownList").dataSource.read();
                                $("#cbbDonVi").data("kendoDropDownList").refresh();
                            }, 1000);
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $("#mdlThemNhanhDonVi").unblock();
                            $(".windows8").css("display", "none");
                        }
                    });
                }
            });
        });
        LoadDinhMuc("");
        btnLuuDMThanhPham();
    })
    function btnLuuDMThanhPham() {
        //Image---------------<

        $("#frmThemMoiThanhPham #files_0").change(function () {
            for (var x = 0; x < this.files.length; x++) {
                dataimg.append("uploads", this.files[x]);
            };
            loadimgSuaChua5(this, $("#frmThemMoiThanhPham"));
            $("#frmThemMoiThanhPham #files_0").val('').clone(true);
        });
        //avatar

        $("#frmThemMoiThanhPham #file_1").change(function () {
            dataavatar = new FormData();
            for (var x = 0; x < this.files.length; x++) {
                dataavatar.append("uploads", this.files[x]);
            };
            loadimgavatar(this, $("#frmThemMoiThanhPham"));
            $("#frmThemMoiThanhPham #file_1").val('').clone(true);
        })
        //Image------------------------>
        $("#btnLuuThemMoi").click(function () {
            var validator = $("#frmThemMoiThanhPham").kendoValidator().data("kendoValidator");
            if (validator.validate()) {
                //image
                var hinhanh = "";
                var ins = dataimg.getAll("uploads").length;
                var DSHinhAnh = [];
                $('.dshinhanhadd .imgslider').each(function (index2, element2) {
                    if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                        DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                    }
                });
                if (ins > 0) {
                    var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", dataimg.getAll("uploads"), $(".dshinhanhadd", $('#frmThemMoiThanhPham'))).split(',');
                    for (var k = 0; k < strUrl.length; k++) {
                        DSHinhAnh.push({ Url: strUrl[k] });
                    }
                }
                for (var i = 0; i < DSHinhAnh.length; i++) {
                    if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                        hinhanh += DSHinhAnh[i].Url + ",";
                    }
                }
                //avatar
                var inss = dataavatar.getAll("uploads").length;
                var anhdaidien = $('.anhdaidien img').attr("data-file");
                if (inss > 0) {
                    var strUrl = uploadAvatarFileParagamWidthFile("/upload" + "/Uploadfiles", dataavatar.getAll("uploads"), $(".anhdaidien", $('#frmThemMoiNguyenLieu')));
                    anhdaidien = strUrl;
                }
                //image
                var dataThanhPham = {
                    ThanhPhamID: $("#ThanhPhamID").val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#ThanhPhamID").val(),
                    MaThanhPham: $("#MaThanhPham").val(),
                    TenThanhPham: $("#TenThanhPham").val(),
                    DonViId: $("#cbbDonVi").val(),
                    DanhSachHinhAnhs: hinhanh.substring(0, hinhanh.length - 1),
                    AnhDaiDien: anhdaidien,
                    DonGia: $("#DonGia").autoNumeric('get') == "" ? "0" : $("#DonGia").autoNumeric('get')
                }
                ContentWatingOP("mdlThemMoiThanhPham", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/AddOrUpdateDMThanhPham",
                    method: "post",
                    data: { data: JSON.stringify(dataThanhPham), listdinhnghia: JSON.stringify($("#gridDinhNghia").data("kendoGrid").dataSource.data()) },
                    success: function (data) {
                        showToast(data.code, data.code, data.message);
                        if (data.code == "success") {
                            $("#mdlThemMoiThanhPham").unblock();
                            closeModel("mdlThemMoiThanhPham");
                            gridReload("grid", {});
                        } else {
                            $("#mdlThemMoiThanhPham").unblock();
                        }
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlThemMoiThanhPham").unblock();
                        $(".windows8").css("display", "none");
                        gridReload("grid", {});
                    }
                });
            }
        });
    }

    function LoadDinhMuc(ThanhPhamID) {
        $("#gridDinhNghia").kendoGrid({
            dataSource: new kendo.data.DataSource(
           {
               transport: {
                   read: {
                       url: crudServiceBaseUrl + "/LayDanhSachNguyenLieu",
                       dataType: 'json',
                       type: 'GET',
                       contentType: "application/json; charset=utf-8",
                       data: { ThanhPhamID: ThanhPhamID }
                   }
               },
               batch: true,
               pageSize: 20,
               sort: [{ field: "TenVatTu", dir: "asc" }],
               schema: {
                   data: 'data',
                   total: 'total',
                   model: {
                       id: "NguyenLieuID",
                       fields: {
                           NguyenLieuID: { editable: false },
                           TenDonVi: { type: "string", editable: false },
                           TenNguyenLieu: { type: "string", editable: false },
                           MaNguyenLieu: { type: "string", editable: false },
                           LoaiHinh: { type: "number", editable: false },
                           SoLuong: { type: "number", editable: true },
                           LoaiVai: { type: "number", editable: true },
                       }
                   }
               },
           }),
            height: window.innerHeight * 0.80,
            width: "100%",
            filterable: {
                mode: "row"
            },
            editable: {
                confirmation: GetTextLanguage("xoabangi"),
                confirmDelete: "Yes",
            },
            sortable: true,
            resizable: true,
            dataBinding: function () {
                record = 0;
            },
            pageable: pageableAll,
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "#= ++record #",
                    width: 60,
                    attributes: alignCenter
                },
                {
                    field: "AnhDaiDien", title: "Ảnh",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    template: kendo.template($("#tplAnh").html()),
                    width: 70
                },
                 {
                     field: "LoaiHinh", width: 100,
                     title: "Loại Hình",
                     attributes: { style: "text-align:left;" },
                     filterable: FilterInTextColumn,
                     template: kendo.template($("#tplLoaiHinh").html())
                 },
                {
                    field: "TenNguyenLieu", width: 200,
                    title: "Tên nguyên liệu",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "MaNguyenLieu",
                    width: 100,
                    attributes: { style: "text-align:left;" },
                    title: "Mã nguyên liệu",
                    filterable: FilterInTextColumn
                },
                {
                    field: "TenDonVi", width: 100,
                    title: GetTextLanguage("donvi"),
                    attributes: { "style": "text-align:left !important;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "SoLuong", width: 100,
                    title: GetTextLanguage("soluong"),
                    format: "{0:n2}",
                    attributes: alignRight,
                    filterable: FilterInColumn,
                    editor: function (container, options) {
                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                        var input = $('<input type="text" data-v-min="0" style="height:16px;width:85%;" class="form-control" id="' + options.model.get("NguyenLieuID") + '" ' + rs + ' name="SoLuong" />');
                        input.appendTo(container);
                        input.autoNumeric(autoNumericOptionsSoLuong);
                    },
                },
                 {
                     field: "TenLoaiVai", width: 100,
                     title: "Loại vải",
                     filterable: FilterInColumn,
                     editor: function (container, options) {
                         var input = $('<input data-text-field="TenLoaiVai" data-value-field="LoaiVai"  id="LoaiVai" name="LoaiVai" />');
                         input.appendTo(container);
                         input.kendoDropDownList({
                             dataTextField: "TenLoaiVai",
                             dataValueField: "LoaiVai",
                             filter: "contains",
                             value: options.model.LoaiVai,
                             dataSource: [{ LoaiVai: 1, TenLoaiVai: "Vải chính" },
                                            { LoaiVai: 2, TenLoaiVai: "Vải phối lót" },
                                            { LoaiVai: 3, TenLoaiVai: "Vải phối P1" },
                                            { LoaiVai: 4, TenLoaiVai: "Vải phối P2" },
                                            { LoaiVai: 5, TenLoaiVai: "Chỉ may" },
                                            { LoaiVai: 6, TenLoaiVai: "Mác" },
                                            { LoaiVai: 7, TenLoaiVai: "Cúc" }, { LoaiVai: 8, TenLoaiVai: "PK khác" }],
                             change: function () {
                                 var items = $("#gridDinhNghia").data("kendoGrid").dataSource.data();
                                 for (var i = 0; i < items.length; i++) {
                                     if (items[i].NguyenLieuID === options.model.NguyenLieuID) {
                                         items[i].TenLoaiVai = this.dataItem().TenLoaiVai;
                                         items[i].LoaiVai = this.dataItem().LoaiVai;
                                         $("#gridDinhNghia").data("kendoGrid").dataSource.data(items);
                                         break;
                                     }
                                 }
                             }
                         });
                     },
                 },
                 {
                     title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                     template: '<a class="k-button k-button-icontext k-grid-delete" href="javascript:void(0)"><span class="k-icon k-i-delete"></span>Xóa</a>',
                     width: 100
                 },

            ],
            save: function (e) {
                var items = $("#gridDinhNghia").data("kendoGrid").dataSource.data();
                if (e.values.SoLuong != null) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].NguyenLieuID == e.model.NguyenLieuID) {
                            items[i].ThanhTien = (e.model.DonGia == null ? 1 : e.model.DonGia) * (e.values.SoLuong == null ? 1 : e.values.SoLuong);
                            items[i].SoLuong = e.values.SoLuong;
                            break;
                        }
                    }
                }
                $("#gridDinhNghia").data("kendoGrid").dataSource.data(items);
            },
            dataBound: function (e) {
                showslideimg10();
            },
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $("#btnChonNguyenLieu").click(function () {
            if (true) {
                CreateModalWithSize("mdlDanhSachNguyenLieu", "99%", "90%", "tplDanhSachNguyenLieu", "Dannh sách nguyên liệu");
                $("#gridDanhSachNguyenLieu").kendoGrid({
                    dataSource: new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: crudServiceBaseUrl + "/LayDanhSachNguyenLieuDeDinhNghia",
                                dataType: "json",
                                type: "GET",
                                contentType: "application/json; charset=utf-8",
                            },
                        },
                        schema: {
                            data: "data",
                            total: "total",
                            model: {
                                id: "NguyenLieuID",
                                fields: {
                                    TenNguyenLieu: { type: "string", editable: false },
                                    MaNguyenLieu: { type: "string", editable: false },
                                    TenDonVi: { type: "string", editable: false },
                                    SoLuong: { type: "number", editable: true },
                                    LoaiHinh: { type: "number", editable: false },
                                }
                            }
                        },
                        change: function (e) {
                            var data = this.data();
                        }
                    }),
                    filterable: {
                        mode: "row"
                    },
                    sortable: true,
                    resizable: true,
                    editable: true,
                    height: innerHeight * 0.75,
                    width: "100%",
                    dataBound: function (e) {
                        showslideimg10();
                    },
                    columns: [
                          {
                              title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                              template: "#= ++record #",
                              width: 60,
                              attributes: alignCenter
                          },
                         {
                             field: "LoaiHinh", width: 100,
                             title: "Loại Hình",
                             attributes: { style: "text-align:left;" },
                             filterable: {
                                 cell: {
                                     template: function (args) {
                                         // create a DropDownList of unique values (colors)
                                         args.element.kendoDropDownList({
                                             dataSource: [{ id: 1, text: "Nguyên liệu" }, { id: 2, text: "Thành phẩm" }],
                                             dataTextField: "text",
                                             dataValueField: "id",
                                             valuePrimitive: true
                                         });
                                     },
                                     showOperators: false
                                 }
                             },
                             template: kendo.template($("#tplLoaiHinh").html())
                         },
                          {
                              field: "AnhDaiDien", title: "Ảnh",
                              attributes: { style: "text-align:left;" },
                              filterable: FilterInTextColumn,
                              template: kendo.template($("#tplAnh").html()),
                              width: 70
                          },
                            {
                                field: "TenNguyenLieu",
                                title: "Tên nguyên liệu",
                                filterable: FilterInTextColumn,
                                attributes: alignLeft,
                                width: 200
                            },
                        {
                            field: "MaNguyenLieu",
                            title: "Mã nguyên liệu",
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: 150
                        },
                        {
                            field: "TenDonVi",
                            title: GetTextLanguage("donvi"),
                            width: 200,
                            attributes: { "style": "text-align:left !important;" },
                            filterable: FilterInTextColumn
                        },
                        {
                            field: "SoLuong",
                            width: 100,
                            title: GetTextLanguage("soluong"),
                            format: "{0:n2}",
                            attributes: {
                                "class": "soluongcheck",
                                "style": "font-weight:bold"
                            },
                            filterable: FilterInColumn,
                            editor: function (container, options) {
                                rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                                var input = $('<input type="text" data-v-min="0" style="width:75%;height:15px;" class="form-control" id="' + options.model.get("NguyenLieuID") + '" name="SoLuong" />');
                                input.appendTo(container);
                                input.autoNumeric(autoNumericOptionsSoLuong);
                            },
                        }
                    ],

                });
                $("#btnDongY").click(function () {
                    var arrData = $("#gridDinhNghia").data("kendoGrid").dataSource.data();
                    var dataItem = $("#gridDanhSachNguyenLieu").data("kendoGrid").dataSource.data();
                    for (var i = 0; i < dataItem.length; i++) {
                        if (dataItem[i].SoLuong != null) {
                            var isExsit = false;
                            for (var j = 0; j < arrData.length; j++) {
                                if (dataItem[i].NguyenLieuID == arrData[j].NguyenLieuID) {
                                    isExsit = true;
                                    arrData[j].SoLuong = dataItem[i].SoLuong;
                                }
                            }
                            if (!isExsit) {
                                arrData.push(dataItem[i]);
                            }
                        }
                    }
                    $("#mdlDanhSachNguyenLieu").data("kendoWindow").close();
                    $("#gridDinhNghia").data("kendoGrid").dataSource.data(arrData);
                });
                $("#btnHuyChonVatTu").click(function () {
                    $("#mdlDanhSachNguyenLieu").data("kendoWindow").close();
                });
            }
        });
    }




    //---------------------------------------------------------------------------------------------------------------
    $(".btnexcel").on("click", function () {
        var filer = [];
        var filers = $("#grid").data("kendoGrid").dataSource.filter();
        if (filers != null && filers != undefined) {
            for (var i = 0; i < filers.filters.length; i++) {
                filers.filters[i].Operator = filers.filters[i].operator;
                filer.push(filers.filters[i]);
            }
        }
        location.href = crudServiceBaseUrl + '/XuatExcelDMThanhPham?filters=' + JSON.stringify(filer);
    });
    $(".btnTemplate").on("click", function () {
        location.href = '/DMThanhPham/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();
    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "50%",
                    height: innerHeight * 0.7,
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                var grid = $("#gridthem").kendoGrid({
                    dataSource: data,
                    editable: true,
                    height: innerHeight * 0.5,
                    width: "100%",
                    dataBound: function () {
                        var rows = this.items();
                        dem = 0;
                        $(rows).each(function () {
                            dem++;
                            var rowLabel = $(this).find(".stt");
                            $(rowLabel).html(dem);
                            if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                                dem = 0;
                            }
                        });
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 60,
                        },
                        {
                            field: "TenThanhPham",
                            title: "Tên thành phẩm",
                            filterable: FilterInTextColumn,
                            attributes: alignLeft
                        },
                        {
                            field: "MaThanhPham",
                            title: "Mã thành phẩm",
                            filterable: FilterInTextColumn,
                            attributes: alignCenter
                        },
                        {
                            field: "TenDonVi",
                            title: "Đơn vị",
                            filterable: FilterInTextColumn,
                            attributes: alignCenter
                        },
                        {
                            field: "DonGia",
                            title: "Đơn giá",
                            filterable: false,
                            attributes: alignRight,
                            format: "{0:n2}",
                        },
                        { command: "destroy", title: "&nbsp;", width: "150px" }
                    ],
                });
                $(".btn-import").click(function () {
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/DMThanhPham/ImportDMThanhPham",
                        method: "post",
                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()) },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#grid").data("kendoGrid").dataSource.read();
                                $("#grid").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    closeModel("mdlThemFileExcel");
                });
            }
        }
        objXhr3.open("POST", "/DMThanhPham/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
    });
    //---------------------------------------------------------------------------------------------------------------
});
