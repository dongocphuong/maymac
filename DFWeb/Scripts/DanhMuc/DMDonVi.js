﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/DMDonVi";
    var dataSourceGrid;
    var dataSourceCongTrinh;
    var tlb_drlCongTrinh;
    var tlb_drlLoaiHinh;
    var timeoutkeysearch = null;
    var mdlChooseForm;
    var mdlThietBi;

    var dataPhongBan = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/read",
                type: "get",
                dataType: 'json'
            },
            create: {
                url: crudServiceBaseUrl + "/create",
                type: "post",
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            update: {
                url: crudServiceBaseUrl + "/update",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            destroy: {
                url: crudServiceBaseUrl + "/destroy",
                type: 'post',
                dataType: "json",
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            parameterMap: function (data, type) {
                if (type !== "read") {
                    if (type == "create") {
                        return { data: data.models[0].TenDonVi }
                    }
                    else {
                        return { data: kendo.stringify(data.models[0]) }
                    }
                }
            }
        },
        batch: true,
        pageSize: 20,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "DonViId",
                fields: {
                    DonViId: { editable: false, nullable: true },
                    TenDonVi: {
                        type: "string",
                        validation: {
                            required: true
                        }
                    }
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataPhongBan,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: pageableAll,
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [
            {
                template: kendo.template($("#btnThem").html())
            }],
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "TenDonVi", title: GetTextLanguage("tendonvi"), filterable: FilterInTextColumn,  attributes: { style: "text-align:left;" }
            },
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                command: [{ name: "edit", text: GetTextLanguage("sua") }, { name: "destroy", text: GetTextLanguage("xoa") }], width: 200
            },
        ],
        editable: editableInline
    });
    $(".btnTemplate").on("click", function () {
        location.href = '/DMDonVi/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();

    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "50%",
                    height: innerHeight * 0.7,
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                var grid = $("#gridthem").kendoGrid({
                    dataSource: data,
                    editable: true,
                    height: innerHeight * 0.5,
                    width: "100%",
                    dataBound: function () {
                        var rows = this.items();
                        dem = 0;
                        $(rows).each(function () {
                            dem++;
                            var rowLabel = $(this).find(".stt");
                            $(rowLabel).html(dem);
                            if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                                dem = 0;
                            }
                        });
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 60,
                        },
                        {
                            field: "TenDonVi",
                            title: GetTextLanguage("danhmucthietbi"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft
                        },
                        { command: "destroy", title: "&nbsp;", width: "150px" }
                    ],
                });
                $(".btn-import").click(function () {
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/DMDonVi/ImportDonVi",
                        method: "post",
                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()) },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#grid").data("kendoGrid").dataSource.read();
                                $("#grid").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    closeModel("mdlThemFileExcel");
                });
            }
        }
        objXhr3.open("POST", "/DMDonVi/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
        //////////////////////////////////////////////////////////////////
    });
});