﻿$(document).ready(function () {
    //Init
  
    var crudServiceBaseUrl = "/DMNhomSanPham";
    var dataDMNguyenLieu = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/LayDanhSachNhomSanPham",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {}
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        sort: [{ field: "ThoiGian", dir: "desc" }],
        pageSize: 50,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "MauMayID",
                fields: {
                    ThoiGian: {type:"date"},
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataDMNguyenLieu,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
             {
                 field: "AnhDaiDien", title: "Ảnh",
                 attributes: { style: "text-align:left;" },
                 filterable: FilterInTextColumn,
                 template: kendo.template($("#tplAnh").html()),
                 width: 70
             },
            {
                field: "ThoiGian",
                title: "Thời gian",
                width: 100,
                attributes: alignCenter,
                format:"{0:dd/MM/yyyy}",
                filterable: FilterInTextColumn,
            },
            {
                field: "MaMauMay",
                title: "Mã thiết kế",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
                width: 150
            },
            {
                field: "TenMauMay",
                title: "Tên mẫu may",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
                width: 150
            },
            {
                field: "TenSanPham",
                title: "Tên sản phẩm",
                width: 200,
                attributes: alignCenter,
                filterable: FilterInTextColumn
            },
        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    $("#grid").on("dblclick", "td", function () {
        var rows = $(this).closest("tr"),
            grids = $("#grid").data("kendoGrid"),
            dataItems = grids.dataItem(rows);
        if (!rows.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            CreateModalWithSize("mldchitiet", "99%", "95%", "tplchitiet", "Chi tiết nhóm sản phẩm");
            $("#InNguyenPhuLieu").attr("href", crudServiceBaseUrl + "/InBangNguyenPhuLieu?MauMayID=" + dataItems.MauMayID);
            $("#tabstrip").kendoTabStrip({
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                }
            });
            $('.imgavatar').attr("src", localQLVT + dataItems.AnhDaiDien);
            $("#TenMauMay").val(dataItems.TenMauMay);
            $("#MaMauMay").val(dataItems.MaMauMay);
            $("#TenSanPham").val(dataItems.TenSanPham);
            ContentWatingOP("ThongTinChung", 0.9);
            $('#InThongSo').addClass("hidden");
            $.ajax({
                url: crudServiceBaseUrl + "/LayDanhSachSanPham",
                method: "GET",
                data: { MauMayID: dataItems.MauMayID },
                success: function (data) {
                    setTimeout(function () {
                        var txt = '<tr> <th style="padding:10px 2px;background: #3da2d2;color:white;text-align:center">Thông số</th>';
                        for (var i = 0; i < data.sizes.length; i++) {
                            txt += '<th style="padding:10px 2px;background: #3da2d2;color:white;text-align:center">' + data.sizes[i].Size + '</th>';
                        }
                        txt += '</tr>';
                        $("#ThongTinChung .panel-body thead").append(txt);
                        
                        
                        for (var j = 0; j < data.lstthuoctinh.length; j++) {
                            //var vitri = 0;
                            var txt2 = '<tr><td style="text-align:left;padding:10px">' + data.lstthuoctinh[j].TenThuocTinh + '</td>';
                            for (var i = 0; i < data.sizes.length; i++) {
                                for (var o = 0; o < data.data.length; o++) {
                                    if (data.data[o].Size == data.sizes[i].Size && data.data[o].TenThuocTinh == data.lstthuoctinh[j].TenThuocTinh) {
                                            txt2 += '<td data-sanphamid="' + data.data[o].SanPhamID + '"><input  type="text" class="form-control thuoctinhchung ' + (data.data[o].KieuDuLieu == 1 ? "number" : "text") + '" data-id="' + data.data[o].ThuocTinhSanPhamID + '" value="' + (data.data[o].GiaTri != null ? data.data[o].GiaTri : "") + '" data-text="' + data.data[o].TenThuocTinh + '" style="box-sizing:border-box"></td>';
                                            break;
                                        }
                                    }
                            }
                            txt2 += '</tr>';

                            $("#ThongTinChung .panel-body tbody").append(txt2);
                            $(".number").autoNumeric(autoNumericOptionsMoney);
                            $("#ThongTinChung").unblock();
                        }
                        $("#tblThongTinChung thead").append(txt);
                        for (var j = 0; j < data.lstthuoctinh.length; j++) {
                            //var vitri = 0;
                            var txt4 = '<tr><td style="text-align:left;padding:10px">' + data.lstthuoctinh[j].TenThuocTinh + '</td>';
                            for (var i = 0; i < data.sizes.length; i++) {
                                for (var o = 0; o < data.data.length; o++) {
                                    if (data.data[o].Size == data.sizes[i].Size && data.data[o].TenThuocTinh == data.lstthuoctinh[j].TenThuocTinh) {
                                        txt4 += '<td style="padding:10px 2px;text-align:center;">' + (data.data[o].GiaTri != null ? data.data[o].GiaTri : "") + '</td>';
                                        break;
                                    }
                                }
                            }
                            txt4 += '</tr>';

                            $("#tblThongTinChung  tbody").append(txt4);
                        }
                        $("#tblThongTinChung2 thead").append(txt);
                        for (var j = 0; j < data.lstthuoctinh.length; j++) {
                            //var vitri = 0;
                            var txt4 = '<tr><td style="text-align:left;padding:10px">' + data.lstthuoctinh[j].TenThuocTinh + '</td>';
                            for (var i = 0; i < data.sizes.length; i++) {
                                for (var o = 0; o < data.data.length; o++) {
                                    if (data.data[o].Size == data.sizes[i].Size && data.data[o].TenThuocTinh == data.lstthuoctinh[j].TenThuocTinh) {
                                        txt4 += '<td style="padding:10px 2px;text-align:center;"></td>';
                                        break;
                                    }
                                }
                            }
                            txt4 += '</tr>';

                            $("#tblThongTinChung2  tbody").append(txt4);
                        }
                        $('#InThongSo').removeClass("hidden");
                        $('#InThongSo').on('click', function () {
                            var divToPrint = $("#tblThongTinChung").html();
                            window.frames["print_frame"].document.body.innerHTML = divToPrint;
                            window.frames["print_frame"].window.focus();
                            window.frames["print_frame"].window.print();
                            return false;
                        });
                        $('#InThongSo2').on('click', function () {
                            var divToPrint = $("#tblThongTinChung2").html();
                            window.frames["print_frame"].document.body.innerHTML = divToPrint;
                            window.frames["print_frame"].window.focus();
                            window.frames["print_frame"].window.print();
                            return false;
                        })
                    }, 1000)
                   
                },
                error: function (xhr, status, error) {
                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                }
            });
            DanhSachNguyenLieu(dataItems.MauMayID);
            $("#btnLuuThemMoi").click(function () {
                var tts = [];
                $("#ThongTinChung input").each(function (index, element) {
                    var n = {
                        SanPhamID:$(element).closest("td").attr("data-sanphamid"),
                        ThuocTinhSanPhamID: $(element).attr("data-id"),
                        TenThuocTinh: $(element).attr("data-text"),
                        GiaTri: $(element).hasClass("number") ? $(element).autoNumeric("get") : $(element).val(),
                        LoaiHinh: 1,
                        KieuDuLieu: $(element).hasClass("number") ? 1 : 2
                    };
                    tts.push(n);
                });
                ContentWatingOP("mldchitiet", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/CapNhatThuocTinh",
                    method: "post",
                    data: { tts: JSON.stringify(tts) },
                    success: function (data) {
                        showToast(data.code, data.code, data.message);
                        if (data.code == "success") {
                            $("#mldchitiet").unblock();
                            gridReload("grid", {});
                        } else {
                            $("#mldchitiet").unblock();
                        }
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mldchitiet").unblock();
                        gridReload("grid", {});
                    }
                });
               
            });
          
        }
        var dataTTCT = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + "/GetAllDataSanPham",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: { MauMayID: dataItems.MauMayID }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            batch: true,
            sort: [{ field: "Size", dir: "asc" }],
            pageSize: 100,
            schema: {
                type: 'json',
                data: 'data',
                total: "total",
                model: {
                    id: "SanPhamID",
                    fields: {
                        SanPhamID: { editable: false, nullable: true },
                        ThoiGian: { type: "date" },
                    }
                }
            },
        });
        //Build grid
        var grid = $("#gridThongTinChiTiet").kendoGrid({
            dataSource: dataTTCT,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            dataBinding: function () {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            height: window.innerHeight * 0.8,
            width: "100%",
            columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            width: 60,
                            attributes: alignCenter,
                            template: "#= ++record #",
                        },
                        {
                            field: "ThoiGian",
                            title: "Thời gian tạo",
                            filterable: FilterInTextColumn,
                            attributes: { style: "text-align:center;" },
                            format: "{0:dd/MM/yyyy}",
                            width: 100,
                        },
                        {
                            field: "AnhDaiDien", title: "Ảnh",
                            attributes: { style: "text-align:left;" },
                            filterable: FilterInTextColumn,
                            template: kendo.template($("#tplAnh").html()),
                            width: 70
                        },
                        {
                            field: "MaSanPham",
                            title: "Mã Sản Phẩm",
                            width: 100,
                            attributes: alignCenter,
                            filterable: FilterInTextColumn,
                        },
                        {
                            field: "TenSanPham",
                            title: "Tên Sản Phẩm",
                            filterable: FilterInTextColumn,
                            attributes: { style: "text-align:left;" },
                            width: 200
                        },
                        {
                            field: "Loai",
                            title: "Loại",
                            width: 100,
                            attributes: alignCenter,
                            filterable: {
                                cell: {
                                    template: function (args) {
                                        args.element.kendoDropDownList({
                                            dataSource: [{ id: 1, text: "Người lớn" }, { id: 2, text: "Trẻ con" }],
                                            dataTextField: "text",
                                            dataValueField: "id",
                                            valuePrimitive: true
                                        });
                                    },
                                    showOperators: false
                                }
                            },
                            template: kendo.template($("#tplLoai").html())
                        },
                        {
                            field: "GioiTinh",
                            title: "Giới Tính",
                            width: 100,
                            attributes: alignCenter,
                            filterable: {
                                cell: {
                                    template: function (args) {
                                        args.element.kendoDropDownList({
                                            dataSource: [{ id: 1, text: "Nam" }, { id: 2, text: "Nữ" }],
                                            dataTextField: "text",
                                            dataValueField: "id",
                                            valuePrimitive: true
                                        });
                                    },
                                    showOperators: false
                                }
                            },
                            template: kendo.template($("#tplGioiTinh").html())
                        },
                        {
                            field: "Size",
                            title: "Size",
                            width: 100,
                            attributes: alignCenter,
                            filterable: FilterInTextColumn
                        },
                        {
                            field: "Mau",
                            title: "Màu",
                            filterable: FilterInTextColumn,
                            attributes: { style: "text-align:center;" },
                            width: 70
                        },
                        {
                            field: "DonGia",
                            title: "Đơn giá",
                            width: 150,
                            attributes: alignRight,
                            format: "{0:n2}",
                            filterable: FilterInColumn
                        },
            ],
            dataBound: function (e) {
                showslideimg10();
            },
        });
    });
    
    function DanhSachNguyenLieu(MauMayID) {
        $("#gridNguyenLieu").kendoGrid({
            dataSource: new kendo.data.DataSource(
           {
               transport: {
                   read: {
                       url: crudServiceBaseUrl + "/LayDanhNguyenLieu",
                       dataType: 'json',
                       type: 'GET',
                       contentType: "application/json; charset=utf-8",
                       data: { MauMayID: MauMayID }
                   }
               },
               batch: true,
               pageSize: 20,
               sort: [{ field: "TenNguyenLieu", dir: "asc" }],
               group: [
                { field: "Size", dir: "asc" },
                { field: "Mau", dir: "asc" },
               ],
               schema: {
                   data: 'data',
                   total: 'total',
                   model: {
                       id: "DinhNghiaThanhPhamID",
                       fields: {
                           NguyenLieuID: { editable: false },
                           TenDonVi: { type: "string", editable: false },
                           Size: { type: "string", editable: false },
                           Mau: { type: "string", editable: false },
                           TenNguyenLieu: { type: "string", editable: false },
                           MaNguyenLieu: { type: "string", editable: false },
                           LoaiHinh: { type: "number", editable: false },
                           SoLuong: { type: "number", editable: true },
                           LoaiVai: { type: "number", editable: true },
                       }
                   }
               },
           }),
            height: window.innerHeight * 0.80,
            width: "100%",
            filterable: {
                mode: "row"
            },
            editable: {
                confirmation: GetTextLanguage("xoabangi"),
                confirmDelete: "Yes",
            },
            sortable: true,
            resizable: true,
            dataBinding: function () {
                record = 0;
            },
            pageable: pageableAll,
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "#= ++record #",
                    width: 60,
                    attributes: alignCenter
                },
                {
                    field: "Size", width: 60,
                    title: "Size",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "Mau", width: 100,
                    title: "Màu",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "AnhDaiDien", title: "Ảnh",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    template: kendo.template($("#tplAnh").html()),
                    width: 70
                },
                {
                    field: "TenNguyenLieu", width: 100,
                    title: "Tên nguyên liệu",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "MaNguyenLieu",
                    width: 100,
                    attributes: { style: "text-align:left;" },
                    title: "Mã nguyên liệu",
                    filterable: FilterInTextColumn
                },
                {
                    field: "TenDonVi", width: 100,
                    title: GetTextLanguage("donvi"),
                    attributes: { "style": "text-align:left !important;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "SoLuong", width: 100,
                    title: GetTextLanguage("soluong"),
                    format: "{0:n2}",
                    attributes: alignRight,
                    filterable: FilterInColumn,
                    editor: function (container, options) {
                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                        var input = $('<input type="text" data-v-min="0"  class="form-control" id="' + options.model.get("DinhNghiaThanhPhamID") + '" ' + rs + ' name="SoLuong" />');
                        input.appendTo(container);
                        input.autoNumeric(autoNumericOptionsSoLuong);
                    },
                },
                 {
                     field: "TenLoaiVai", width: 100,
                     title: "Loại vải",
                     filterable: FilterInColumn,
                     editor: function (container, options) {
                         var input = $('<input data-text-field="TenLoaiVai" data-value-field="LoaiVai"  id="LoaiVai" name="LoaiVai" />');
                         input.appendTo(container);
                         input.kendoDropDownList({
                             dataTextField: "TenLoaiVai",
                             dataValueField: "LoaiVai",
                             filter: "contains",
                             value: options.model.LoaiVai,
                             dataSource: [{ LoaiVai: 1, TenLoaiVai: "Vải chính" },
                                            { LoaiVai: 2, TenLoaiVai: "Vải phối lót" },
                                            { LoaiVai: 3, TenLoaiVai: "Vải phối P1" },
                                            { LoaiVai: 4, TenLoaiVai: "Vải phối P2" },
                                            { LoaiVai: 5, TenLoaiVai: "Chỉ may" },
                                            { LoaiVai: 6, TenLoaiVai: "Mác" },
                                            { LoaiVai: 7, TenLoaiVai: "Cúc" }, { LoaiVai: 8, TenLoaiVai: "PK khác" }],
                             change: function () {
                                 var items = $("#gridDinhNghia").data("kendoGrid").dataSource.data();
                                 for (var i = 0; i < items.length; i++) {
                                     if (items[i].NguyenLieuID === options.model.NguyenLieuID) {
                                         items[i].TenLoaiVai = this.dataItem().TenLoaiVai;
                                         items[i].LoaiVai = this.dataItem().LoaiVai;
                                         $("#gridDinhNghia").data("kendoGrid").dataSource.data(items);
                                         break;
                                     }
                                 }
                             }
                         });
                     },
                 },
                

            ],
            save: function (e) {
                var items = $("#gridNguyenLieu").data("kendoGrid").dataSource.data();
                if (e.values.SoLuong != null) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].DinhNghiaThanhPhamID == e.model.DinhNghiaThanhPhamID) {
                            items[i].ThanhTien = (e.model.DonGia == null ? 1 : e.model.DonGia) * (e.values.SoLuong == null ? 1 : e.values.SoLuong);
                            items[i].SoLuong = e.values.SoLuong;
                            break;
                        }
                    }
                }
                $("#gridNguyenLieu").data("kendoGrid").dataSource.data(items);
            },
            dataBound: function (e) {
                showslideimg10();
            },
        });
        $("#btnLuuNguyenLieu").click(function () {
            ContentWatingOP("mldchitiet", 0.9);
            $.ajax({
                url: crudServiceBaseUrl + "/CapNhatNguyenLieu",
                method: "post",
                data: { data: JSON.stringify($("#gridNguyenLieu").data("kendoGrid").dataSource.data()) },
                success: function (data) {
                    showToast(data.code, data.code, data.message);
                    if (data.code == "success") {
                        $("#mldchitiet").unblock();
                        gridReload("grid", {});
                    } else {
                        $("#mldchitiet").unblock();
                    }
                },
                error: function (xhr, status, error) {
                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                    $("#mldchitiet").unblock();
                    gridReload("grid", {});
                }
            });

        });
    }
   
});
