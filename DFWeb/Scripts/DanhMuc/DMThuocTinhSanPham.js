﻿$(document).ready(function () {
    //Init
    var data_KieuDuLieu = [
        { text: "Số", val: 1 },
        { text: "Chữ", val: 2 },
    ]
    var data_LoaiHinhSanXuat = [
        { text: "May", val: 1 },
        { text: "Dệt", val: 2 },
    ]
    var crudServiceBaseUrl = "/DMThuocTinhSanPham";
    var dataDMThuocTinhSanPham = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetAllDataDMThuocTinhSanPham",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {}
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "ThuocTinhSanPhamID",
                fields: {
                    ThuocTinhSanPhamID: { editable: false, nullable: true },
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataDMThuocTinhSanPham,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [{ template: kendo.template($("#btnThem").html()) }],
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
            {
                field: "TenThuocTinh",
                title: "Tên Thuộc Tính",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
                width: 300
            },
            {
                field: "KieuDuLieu",
                title: "Kiểu Dữ Liệu",
                width: 100,
                attributes: alignCenter,
                filterable: FilterInTextColumn,
                template: kendo.template($("#tplKieuDuLieu").html()),
            },
        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    $("#grid").on("dblclick", "td", function () {
        var rows = $(this).closest("tr"),
            grids = $("#grid").data("kendoGrid"),
            dataItems = grids.dataItem(rows);
        if (!rows.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            var dataobjectload = dataItems;
            CreateModalWithSize("mdlThemMoiThuocTinhSanPham", "30%", "30%", "tplThemMoiThuocTinhSanPham", "Chi tiết thuộc tính sản phẩm");
            $("#btnXoa").removeClass("hidden");
            $("#drdKieuDuLieu").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "val",
                optionLabel: "Kiểu Dữ Liệu",
                dataSource: data_KieuDuLieu,
            });
            $("#drdLoaiHinhSanXuat").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "val",
                optionLabel: "Loại sản phẩm",
                dataSource: data_LoaiHinhSanXuat,
            });
            var viewModel = kendo.observable({
                ThuocTinhSanPhamID: dataobjectload == null ? "00000000-0000-0000-0000-000000000000" : dataobjectload.ThuocTinhSanPhamID,
                TenThuocTinh: dataobjectload == null ? "" : dataobjectload.TenThuocTinh,
                KieuDuLieu: dataobjectload == null ? "" : dataobjectload.KieuDuLieu,
                LoaiHinhSanXuat: dataobjectload == null ? "" : dataobjectload.LoaiHinhSanXuat,
            });
            kendo.bind($("#frmThemMoiThuocTinhSanPham"), viewModel);
            $("#btnXoa").click(function () {
                var x = confirm("Bạn có chắc chắn muốn xóa danh mục này không?");
                if (x) {
                    ContentWatingOP("mdlThemMoiThuocTinhSanPham", 0.9);
                    $.ajax({
                        url: crudServiceBaseUrl + "/XoaDMThuocTinhSanPham",
                        method: "POST",
                        data: { data: dataItems.ThuocTinhSanPhamID },
                        success: function (data) {
                            showToast(data.code, data.code, data.message);
                            if (data.code == "success") {
                                $("#mdlThemMoiThuocTinhSanPham").unblock();
                                $(".windows8").css("display", "none");
                                closeModel("mdlThemMoiThuocTinhSanPham");
                                gridReload("grid", {});
                            }
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $("#mdlThemMoiThuocTinhSanPham").unblock();
                            $(".windows8").css("display", "none");
                            gridReload("grid", {});
                        }
                    });
                }
            });
            btnLuuDMThuocTinhSanPham();
        }
    });

    $("#grid").on("click", "#btnAdd", function () {
        CreateModalWithSize("mdlThemMoiThuocTinhSanPham", "30%", "30%", "tplThemMoiThuocTinhSanPham", "Thêm mới thuộc tính sản phẩm");
        $("#drdKieuDuLieu").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "val",
            optionLabel: "Kiểu Dữ Liệu",
            dataSource: data_KieuDuLieu,
        });
        $("#drdLoaiHinhSanXuat").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "val",
            optionLabel: "Loại sản phẩm",
            dataSource: data_LoaiHinhSanXuat,
        });
        btnLuuDMThuocTinhSanPham();
    })
    function btnLuuDMThuocTinhSanPham() {
        $("#btnLuuThemMoi").click(function () {
            var validator = $("#frmThemMoiThuocTinhSanPham").kendoValidator().data("kendoValidator");

            if (validator.validate()) {
                var dataThuocTinhSanPham = {
                    ThuocTinhSanPhamID: $("#ThuocTinhSanPhamID").val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#ThuocTinhSanPhamID").val(),
                    TenThuocTinh: $("#TenThuocTinh").val(),
                    KieuDuLieu: $("#drdKieuDuLieu").val(),
                    LoaiHinhSanXuat: $("#drdLoaiHinhSanXuat").val(),
                }
                ContentWatingOP("mdlThemMoiThuocTinhSanPham", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/AddOrUpdateDMThuocTinhSanPham",
                    method: "post",
                    data: { data: JSON.stringify(dataThuocTinhSanPham) },
                    success: function (data) {
                        showToast(data.code, data.code, data.message);
                        if (data.code == "success") {
                            $("#mdlThemMoiThuocTinhSanPham").unblock();
                            $(".windows8").css("display", "none");
                            closeModel("mdlThemMoiThuocTinhSanPham");
                            gridReload("grid", {});
                        } else {
                            $("#mdlThemMoiThuocTinhSanPham").unblock();
                        }
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlThemMoiThuocTinhSanPham").unblock();
                        $(".windows8").css("display", "none");
                        gridReload("grid", {});
                    }
                });
            }
        });
    }
    //---------------------------------------------------------------------------------------------------------------
    //$(".btnexcel").on("click", function () {
    //    var filer = [];
    //    var filers = $("#grid").data("kendoGrid").dataSource.filter();
    //    if (filers != null && filers != undefined) {
    //        for (var i = 0; i < filers.filters.length; i++) {
    //            filers.filters[i].Operator = filers.filters[i].operator;
    //            filer.push(filers.filters[i]);
    //        }
    //    }
    //    location.href = crudServiceBaseUrl + '/XuatExcelDMThuocTinhSanPham?filters=' + JSON.stringify(filer);
    //});
    //$(".btnTemplate").on("click", function () {
    //    location.href = '/DMThuocTinhSanPham/XuatTemplate';
    //});
    //$(".btnImport").on("click", function () {
    //    $("#inputImport").click();
    //});
    //$("#inputImport").change(function () {
    //    ContentWatingOP("content-loader", 0.9);
    //    var dataimg3 = new FormData();
    //    dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
    //    var objXhr3 = new XMLHttpRequest();
    //    objXhr3.onreadystatechange = function () {
    //        if (objXhr3.readyState == 4) {
    //            $('#content-loader').unblock();
    //            var data = JSON.parse(objXhr3.responseText);
    //            var mdl = $('#mdlThemFileExcel').kendoWindow({
    //                width: "50%",
    //                height: innerHeight * 0.7,
    //                title: "",
    //                modal: true,
    //                visible: false,
    //                resizable: false,
    //                actions: [
    //                    "Close"
    //                ],
    //                deactivate: function () {
    //                    $(this.element).empty();
    //                }
    //            }).data("kendoWindow").center().open();
    //            mdl.content(kendo.template($('#tempImport').html()));
    //            var grid = $("#gridthem").kendoGrid({
    //                dataSource: data,
    //                editable: true,
    //                height: innerHeight * 0.5,
    //                width: "100%",
    //                dataBound: function () {
    //                    var rows = this.items();
    //                    dem = 0;
    //                    $(rows).each(function () {
    //                        dem++;
    //                        var rowLabel = $(this).find(".stt");
    //                        $(rowLabel).html(dem);
    //                        if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
    //                            dem = 0;
    //                        }
    //                    });
    //                },
    //                columns: [
    //                    {
    //                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
    //                        template: "<span class='stt'></span>",
    //                        width: 60,
    //                    },
    //                    {
    //                        field: "TenThuocTinhSanPham",
    //                        title: "Tên nguyên liệu",
    //                        filterable: FilterInTextColumn,
    //                        attributes: alignLeft
    //                    },
    //                    {
    //                        field: "MaThuocTinhSanPham",
    //                        title: "Mã Nguyên Liệu",
    //                        filterable: FilterInTextColumn,
    //                        attributes: alignCenter
    //                    },
    //                    {
    //                        field: "TenDonVi",
    //                        title: GetTextLanguage("donvitinh"),
    //                        filterable: FilterInTextColumn,
    //                        attributes: alignCenter
    //                    },
    //                    { command: "destroy", title: "&nbsp;", width: "150px" }
    //                ],
    //            });
    //            $(".btn-import").click(function () {
    //                ContentWatingOP("mdlThemFileExcel", 0.9);
    //                $.ajax({
    //                    url: "/DMThuocTinhSanPham/ImportDMThuocTinhSanPham",
    //                    method: "post",
    //                    data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()) },
    //                    success: function (data2) {
    //                        showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
    //                        setTimeout(function () {
    //                            $("#mdlThemFileExcel").unblock();
    //                            $("#grid").data("kendoGrid").dataSource.read();
    //                            $("#grid").data("kendoGrid").refresh();
    //                            closeModel("mdlThemFileExcel");
    //                        }, 1000);
    //                    }
    //                });
    //            });
    //            $("#huy").click(function () {
    //                closeModel("mdlThemFileExcel");
    //            });
    //        }
    //    }
    //    objXhr3.open("POST", "/DMThuocTinhSanPham/CheckKetQua", true);
    //    objXhr3.send(dataimg3);
    //    $("#inputImport").val('').clone(true);
    //});
    //---------------------------------------------------------------------------------------------------------------
});
