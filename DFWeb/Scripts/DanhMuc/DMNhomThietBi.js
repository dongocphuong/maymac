﻿$(document).ready(function () {
    var dataSourceLoaiHinh = [
        { text: "Máy phụ", val: 0 },
        { text: "Máy chính", val: 1 }
    ];
    var dataSourceDMNhomThietBi = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: "/DmNhomThietBi/GetData",
                    dataType: 'json',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                },
                update: {
                    url: "/DmNhomThietBi/Update",
                    type: 'post',
                    dataType: 'json',
                    complete: function (e) {
                        altReturn(e.responseJSON);
                        $("#gridDMNhomThietBi").data("kendoGrid").dataSource.read();
                    }
                },
                create: {
                    url: "/DmNhomThietBi/Create",
                    type: 'post',
                    dataType: 'json',
                    complete: function (e) {
                        altReturn(e.responseJSON);
                        $("#gridDMNhomThietBi").data("kendoGrid").dataSource.read();
                    }
                },
                destroy: {
                    url: "/DmNhomThietBi/Delete",
                    type: 'post',
                    dataType: 'json',
                    complete: function (e) {
                        altReturn(e.responseJSON);
                        $("#gridDMNhomThietBi").data("kendoGrid").dataSource.read();
                    }
                },
                parameterMap: function (data, type) {
                    if (type !== "read") {
                        if (type == "create")
                        {
                            var model = {
                                TenNhomThietBi: data.models[0].TenNhomThietBi,
                                MoTa: data.models[0].MoTa,
                                LoaiHinh: data.models[0].LoaiHinh
                            }
                            return { data: kendo.stringify(model) }
                        }
                        else {
                            return { data: kendo.stringify(data.models[0]) }
                        }
                    }
                }
            },
            batch: true,
            sort: [{ field: "TenNhomThietBi", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "NhomThietBiID",
                    fields: {
                        NhomThietBiId: { type: "string" },
                        TenNhomThietBi: {
                            type: "string",
                            validation: {
                                required: {
                                    message: GetTextLanguage("khongtrong")
                                }
                            }
                        },
                        MoTa: { type: "string" },
                        LoaiHinh: { type: "string" }
                    },
                }
            },
            pageSize: pageSize,
        });

    var grid = $("#gridDMNhomThietBi").kendoGrid({
        dataSource: dataSourceDMNhomThietBi,
        height: heightGrid - 90,
        toolbar: [{ template: kendo.template($("#btnThem").html()) }],
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBound: function () {
            var rows = this.items();
            dem = 0;
            $(rows).each(function () {
                dem++;
                var rowLabel = $(this).find(".stt");
                $(rowLabel).html(dem);
                if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                    dem = 0;
                }
            });
        },
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "<span class='stt'></span>",
                width: 60,
                lockable: false,
                filterable: false,
                attributes: alignCenter
            },
            {
                field: "TenNhomThietBi",
                title: GetTextLanguage("tennhomthietbi"),
                width: 200,
                filterable: FilterInTextColumn,
                attributes: alignLeft
            },
            {
                field: "MoTa",
                title: GetTextLanguage("mota"),
                filterable: FilterInTextColumn,
                attributes: alignLeft
            },
            {
                field: "LoaiHinh",
                title: GetTextLanguage("loaihinh"), filterable: false,
                template: "#= LoaiHinh == 0 ? 'Máy phụ' : 'Máy chính' #",
                editor: function (container, options) {
                    var input = $('<input id="LoaiHinh" name="LoaiHinh"/>');
                    input.appendTo(container);
                    input.kendoDropDownList({
                        dataTextField: "text",
                        dataValueField: "val",
                        filter: "contains",
                        value: options.model.val,
                        dataSource: dataSourceLoaiHinh,
                    })
                }
            },
            {
                command: [{ name: "edit", text: GetTextLanguage("sua") }, { name: "destroy", text: GetTextLanguage("xoa") }],
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                width: 200,
                attributes: alignCenter
            }],
        editable: editableInline,
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        pageable: pageableAll
    });
    $(".btnTemplate").on("click", function () {
        location.href = '/DMNhomThietBi/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();

    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "500px",
                    height: "100px",
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                $(".span-text").html(data.message);
                if (data.message != "") {
                    $(".span-text").text(data.message);

                } else {
                    $("#btn-xuatbotrung").addClass("hidden");
                }
                $(".btn-import").click(function () {
                    var loaihinh = $(this).data("data-loai");
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/DMNhomThietBi/ImportNhomThietBi",
                        method: "post",
                        data: { url: data.url, LoaiHinh: loaihinh },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    $.ajax({
                        url: "/DMNhomThietBi/DeleteFile",
                        method: "post",
                        data: { url: data.url },
                        success: function (data2) {
                            setTimeout(function () {
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
            }
        }
        objXhr3.open("POST", "/DMNhomThietBi/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
        //////////////////////////////////////////////////////////////////
    });
});
