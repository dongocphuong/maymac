﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/QuanLyPhongBan";
    var crudServiceBaseUrlDoi = "/DMDoiThiCong";
    var dataSourceGrid;
    var dataSourceCongTrinh;
    var tlb_drlCongTrinh;
    var tlb_drlLoaiHinh;
    var timeoutkeysearch = null;
    var mdlChooseForm;
    var mdlThietBi;
    var LoaiHinhThietBi = 0;//0:của công ty - 1:đi thuê
    var actionThietBi = { add: 10, edit: 15 };

    //datafix
    var datafix_tinhtrang = [
        { text: "Đang thi công", val: 0 },
        { text: "Đã hoàn thiện", val: 1 },
        { text: "Tạm ngưng", val: 2 }
    ]
    var datafix_trangthai = [
        { text: "Đúng tiến độ", val: 0 },
        { text: "Chậm tiến độ", val: 1 },
        { text: "Vượt tiến độ", val: 2 }
    ]
    var dataPhongBan = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/LayDanhSachCongTrinh",
                type: "get",
                dataType: 'json'
            },
            create: {
                url: crudServiceBaseUrl + "/AddOrUpdateCongTrinh",
                type: "post",
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid", {});
                }
            },
            update: {
                url: crudServiceBaseUrl + "/AddOrUpdateCongTrinh",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid", {});
                }
            },
            destroy: {
                url: crudServiceBaseUrl + "/delCongTrong",
                type: 'post',
                dataType: "json",
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid", {});
                }
            },
            parameterMap: function (data, type) {
                if (type !== "read") {
                    if (type == "create") {
                        var model = {
                            TenPhong: data.models[0].TenPhong
                        }
                        return { data: kendo.stringify(model) }
                    }
                    else if (type == "update") {
                        var model = {
                            PhongBanID: data.models[0].PhongBanID,
                            TenPhong: data.models[0].TenPhong
                        }
                        return { data: kendo.stringify(model) }
                    }
                    else {
                        return { data: kendo.stringify(data.models[0]) }
                    }
                }
            }
        },
        batch: true,
        pageSize: 20,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "PhongBanID",
                fields: {
                    PhongBanID: { editable: false, nullable: true },
                    TenPhong: { type: "string" }
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataPhongBan,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: pageableAll,
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [
            {
                template: kendo.template($("#btnThem").html())
            }
        ],
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        edit: function (e) {
            e.container.find(".k-edit-label:first").hide();
            e.container.find(".k-edit-field:first").hide();
        },
        columns: [
            {
                title: "<a href='#' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "TenPhong",
                title: GetTextLanguage("tenphongban"),
                filterable: FilterInTextColumn,
                attributes: alignLeft
            },
            {
                title: "<a href='#' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                command: [
                    { name: "destroy", text: GetTextLanguage("xoa") },
                ],
                width: 200
            }
        ],
        editable: {
            mode: "popup",
            window: {
                title: GetTextLanguage("thongtinchitiet"),
                width: "25%",
            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes",
        },
        dataBound: function (e) {
            e.sender.tbody.find(".k-button.fa-exchange").each(function (idx, element) {
                $(element).removeClass("fa fa-exchange").find("span").addClass("fa fa-exchange");
            });
            e.sender.tbody.find(".k-button.fa-th-large").each(function (idx, element) {
                $(element).removeClass("fa fa-th-large").find("span").addClass("fa fa-th-large");
            });
        }
    });

    $("#grid .k-grid-content").on("dblclick", "td", function () {
        var rowct = $(this).closest("tr");
        if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            $("#grid").data("kendoGrid").editRow(rowct);
        }
    });

    function quanlydoithicong(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        $(".dsphongban").hide();
        LoadingContent("DSDoiThiCong", "/QuanLyPhongBan/DSDoiThiCong?CongTrinhID=" + dataItem.get("CongTrinhID"), dataItem.get("TenPhongBan"));
    }
    function dieuchuyennhanvien(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        var mdl = $('#mdlNhanVienDoi').kendoWindow({
            width: "80%",
            height: "80%",
            title: GetTextLanguage("danhsachnhanvien"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#tempNhanVienDoi').html()));

        var gridNhanVienDoi = $("#gridNhanVienDoi").kendoGrid({
            dataSource: getDataSourceGrid(crudServiceBaseUrlDoi, "/LayDanhSachNhanVienTheoDoi", { DoiThiCongID: dataItem.get("DoiThiCongID") }, ""),
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            height: "99%",
            columns: [
                {
                    field: "DoiThiCongID", hidden: true
                },
                {
                    field: "NhanVienDoiThiCongId", hidden: true
                },
                {
                    field: "NhanVienID", hidden: true
                },
                {
                    field: "TenNhanVien", title: GetTextLanguage("tennhanvien"), filterable: FilterInTextColumn, attributes: alignLeft
                },
                {
                    field: "SDT", title: GetTextLanguage("sdt"), filterable: FilterInTextColumn
                },
                {
                    field: "NgaySinh", title: GetTextLanguage("ngaysinh"), filterable: {
                        cell: {
                            showOperators: false,
                            operator: "contains"

                        }
                    }
                },
                {
                    field: "CMT", title: GetTextLanguage("socmt"), filterable: FilterInTextColumn
                },
                {
                    field: "DiaChi", title: GetTextLanguage("diachi"), width: 300, filterable: FilterInTextColumn, attributes: alignLeft
                },
                {
                    field: "GioiTinh", title: GetTextLanguage("gioitinh"), filterable: {
                        cell: {
                            showOperators: false,
                            operator: "contains"

                        }
                    },
                    attributes: alignLeft
                },
                {
                    field: "ChucVuTen", title: GetTextLanguage("chucvu"), filterable: FilterInTextColumn, attributes: alignLeft
                },
            ]
        });

        $("#btnAddNhanVienDoi").click(function () {
            openDieuChuyenNhanVien(dataItem.get("DoiThiCongID"));
        });

        $("#gridNhanVienDoi").on("click", ".roidoi", function () {
            var x = confirm(GetTextLanguage("xoabangi"));
            if (x) {
                rowct = $(this).closest("tr"),
                    gridct = $("#gridNhanVienDoi").data("kendoGrid"),
                    dataItemct = gridct.dataItem(rowct);

                $.ajax({
                    cache: false,
                    async: false,
                    type: "POST",
                    url: crudServiceBaseUrlDoi + "/RoiDoi",
                    data: { NhanVienDoiThiCongId: dataItemct.get("NhanVienDoiThiCongId"), NhanVienID: dataItemct.get("NhanVienID") },
                    success: function (data) {
                        altReturn(data);
                        gridReload("gridNhanVienDoi", { DoiThiCongID: dataItemct.get("DoiThiCongID") });
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                    }
                });
            } else {
                return false;
            }
        });

    }

    function openDieuChuyenNhanVien(DoiThiCongId) {
        var mdl = $('#mdladdNhanVien').kendoWindow({
            width: 500,
            height: 250,
            title: GetTextLanguage("chonnhanvien"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#tmpladdNhanVien').html()));

        var html = $("#template").html();

        $("#customers").kendoDropDownList({
            dataTextField: "TenNhanVien",
            dataValueField: "NhanVienID",
            filter: "contains",
            autoBind: false,
            optionLabel: GetTextLanguage("chonnhanvien"),
            template: "<span class='k-state-default' style='background-image: url(\'../content/web/Customers/ac.jpg\')'></span><span class='k-state-default'><h3>#: TenNhanVien #</h3><p>#: NgaySinh # - #: DiaChi #</p></span>",
            dataSource: getDataDroplitwithParam(crudServiceBaseUrlDoi, "/LayDanhSachNhanVienTheoDoi2", { DoiThiCongID: DoiThiCongId })
        });

        $("#chucvus").kendoDropDownList({
            filter: "contains",
            dataTextField: "TenChucVu",
            dataValueField: "ChucVuId",
            optionLabel: GetTextLanguage("chonchucvu"),
            dataSource: getDataDroplit(crudServiceBaseUrlDoi, "/LayDanhSachChucVu")
        });

        var dropdownlist = $("#customers").data("kendoDropDownList");

        var viewModelDoi = kendo.observable({
            save: function (e) {
                if (validator.validate()) {

                    $.ajax({
                        cache: false,
                        async: false,
                        type: "POST",
                        url: crudServiceBaseUrlDoi + "/DieuChuyenDoiVeCongTrinh",
                        data: { DoiThiCongID: DoiThiCongId, NhanVienID: $("#customers").val(), ChucVuId: $("#chucvus").val() },
                        success: function (data) {
                            altReturn(data);
                            gridReload("gridNhanVienDoi", { DoiThiCongID: DoiThiCongId });
                            if (data.code == "success") {
                                closeModel("mdladdNhanVien");
                            }
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        }
                    });
                }
            }
        });
        kendo.bind($("#frmaddDoi"), viewModelDoi);
        var validator = $("#frmaddDoi").kendoValidator().data("kendoValidator");
    }


    //loaddata
    function getDataSourceGrid(crud, url, param, groups) {
        var dataSources = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crud + url,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: param
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            batch: true,
            pageSize: 20,
            schema: {
                data: "data",
                total: "total"
            }
        });

        return dataSources;
    }

    function getDataDroplit(crudServiceBaseUrl, url) {
        var dataSourceObj;
        dataSourceObj = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + url,
                    dataType: "json",
                    type: "get"
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        });
        return dataSourceObj;
    }

    function getDataDroplitwithParam(crudServiceBaseUrl, url, param) {
        var dataSourceObj;
        dataSourceObj = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + url,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: param
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        });
        return dataSourceObj;
    }

    function getHeigtContent(grid) {
        if (grid == 1) { return $("#mdlChiTiet").height() - 80; } // tab thông tin
        else if (grid == 2) { //tab điều chuyển
            return $("#mdlChiTiet").height() - 101 - 37 - 96 - 60;

        }
        else {
            return $("#mdlChiTiet").height() - 101 - 37 - 96; //tab còn lại

        }
    }

    $(window).resize(function () {
        resizeContent();
    });

    function resizeContent() {
        $("#mdlfrmThietBi").css("height", getHeigtContent(1));
        $("#mdlLichSu .k-grid-content").css("height", getHeigtContent(3));
        $("#mdlDieuChuyen .k-grid-content").css("height", getHeigtContent(2));
        $("#mdlBaoCaoSuaChua .k-grid-content").css("height", getHeigtContent(3));
    }
    function showToast(loai, title, message) {
        toastr.options.positionClass = "toast-bottom-right";
        toastr[loai](message, title, { timeOut: 3000 });
    }

    function altReturn(data) {
        if (data.code == "success") {
            showToast("success", GetTextLanguage("thanhcong"), data.message);
        }
        else {
            showToast("error", GetTextLanguage("thatbai"), data.message);
        }
    }

    function gridReload(grid, parram) {
        $('#' + grid).data('kendoGrid').dataSource.read(parram);
        $('#' + grid).data('kendoGrid').refresh();

        //$('#grid').data('kendoGrid').dataSource.read({
        //    CongTrinhID: vCongTrinhID,
        //    LoaiHinh: vLoaiHinh
        //});
        //$('#grid').data('kendoGrid').refresh();
    }

    function closeModel(idform) {
        $('#' + idform).data("kendoWindow").close();
    }

    $(".btnTemplate").on("click", function () {
        location.href = '/QuanLyPhongBan/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();

    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "50%",
                    height: innerHeight * 0.7,
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                var grid = $("#gridthem").kendoGrid({
                    dataSource: data,
                    editable: true,
                    height: innerHeight * 0.5,
                    width: "100%",
                    dataBound: function () {
                        var rows = this.items();
                        dem = 0;
                        $(rows).each(function () {
                            dem++;
                            var rowLabel = $(this).find(".stt");
                            $(rowLabel).html(dem);
                            if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                                dem = 0;
                            }
                        });
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 60,
                        },
                        {
                            field: "TenDuAn",
                            title: GetTextLanguage("tenphongban"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft
                        },
                        { command: "destroy", title: "&nbsp;", width: "150px" }
                    ],

                });
                $(".btn-import").click(function () {
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/QuanLyPhongBan/ImportPhongBan",
                        method: "post",
                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()) },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#grid").data("kendoGrid").dataSource.read();
                                $("#grid").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    closeModel("mdlThemFileExcel");
                });
            }
        }
        objXhr3.open("POST", "/QuanLyPhongBan/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
        //////////////////////////////////////////////////////////////////
    });

});