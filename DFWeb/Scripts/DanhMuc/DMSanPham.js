﻿$(document).ready(function () {
    //Init
    var dataavatar = new FormData();
    var dataimg = new FormData();
    var dataimg2 = new FormData();
    var dataavatar2 = new FormData();
    var crudServiceBaseUrl = "/DMSanPham";
    var sizes = [
        { text: '01', val: '01' },
        { text: '02', val: '02' },
        { text: '03', val: '03' },
        { text: '04', val: '04' },
        { text: '05', val: '05' },
        { text: '06', val: '06' },
        { text: '07', val: '07' },
        { text: '08', val: '08' },
        { text: '09', val: '09' },
        { text: '10', val: '10' },
        { text: '11', val: '11' },
        { text: '12', val: '12' },
        { text: '13', val: '13' },
        { text: '14', val: '14' },
        { text: '15', val: '15' },
        { text: '16', val: '16' },
        { text: '17', val: '17' },
        { text: '18', val: '18' },
        { text: '19', val: '19' },
        { text: '20', val: '20' },
        { text: '21', val: '21' },
        { text: '22', val: '22' },
        { text: '23', val: '23' },
        { text: '24', val: '24' },
        { text: '9T', val: '9T' },
        { text: '12T', val: '12T' },
        { text: '18T', val: '18T' },
        { text: '24T', val: '24T' },
        { text: 'XS', val: 'XS' },
        { text: 'S', val: 'S' },
        { text: 'M', val: 'M' },
        { text: 'L', val: 'L' },
        { text: 'XL', val: 'XL' },
        { text: 'XXL', val: 'XXL' }
    ];
    var loai = [
        { text: "Người lớn", value: 1 },
        { text: "Trẻ con", value: 2 },

    ];
    var gioitinh = [
        { text: "Nam", value: 1 },
        { text: "Nữ", value: 2 },

    ];
    var dataTrangThaiSanPham = [
        { text: "Đã định nghĩa công thức", value: 1 },
        { text: "Chưa định nghĩa công thức", value: 2 },
    ];
    var data_LoaiHinhSanXuat = [
        { text: "May", val: 1 },
        { text: "Dệt", val: 2 },
    ]
    var dataDMSanPham = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetAllDataSanPham",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {}
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        sort: [{ field: "Size", dir: "asc" }],
        pageSize: 100,
        group: {
            field: "TenSanPham",
            dir: "asc"
        },
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "SanPhamID",
                fields: {
                    SanPhamID: { editable: false, nullable: true },
                    ThoiGian: { type: "date" },
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataDMSanPham,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        height: window.innerHeight * 0.9,
        width: "100%",
        toolbar: [{ template: kendo.template($("#btnThem").html()) }],
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
            {
                field: "TrangThaiSanPham",
                title: "Trạng thái",
                filterable: FilterInTextColumn,
                filterable: {
                    cell: {
                        template: function (args) {
                            args.element.kendoDropDownList({
                                dataSource: dataTrangThaiSanPham,
                                dataTextField: "text",
                                dataValueField: "value",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                width: 200,
                template: kendo.template($("#tplTrangThaiSanPham").html())
            },
            {
                field: "ThoiGian",
                title: "Thời gian tạo",
                filterable: false,
                attributes: { style: "text-align:center;" },
                format: "{0:dd/MM/yyyy}",
                width: 100,
                hidden: true
            },
            {
                field: "AnhDaiDien", title: "Ảnh",
                attributes: { style: "text-align:left;" },
                filterable: false,
                template: kendo.template($("#tplAnh").html()),
                width: 70
            },
            {
                field: "MaSanPham",
                title: "Mã Sản Phẩm",
                width: 100,
                attributes: alignCenter,
                filterable: FilterInTextColumn,
            },
            {
                field: "TenSanPham",
                title: "Tên Sản Phẩm",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
                width: 150
            },
            {
                field: "Loai",
                title: "Loại",
                width: 100,
                attributes: alignCenter,
                filterable: {
                    cell: {
                        template: function (args) {
                            args.element.kendoDropDownList({
                                dataSource: [{ id: 1, text: "Người lớn" }, { id: 2, text: "Trẻ con" }],
                                dataTextField: "text",
                                dataValueField: "id",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                template: kendo.template($("#tplLoai").html())
            },
            {
                field: "GioiTinh",
                title: "Giới Tính",
                width: 100,
                attributes: alignCenter,
                filterable: {
                    cell: {
                        template: function (args) {
                            args.element.kendoDropDownList({
                                dataSource: [{ id: 1, text: "Nam" }, { id: 2, text: "Nữ" }],
                                dataTextField: "text",
                                dataValueField: "id",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                template: kendo.template($("#tplGioiTinh").html())
            },
            {
                field: "Size",
                title: "Size",
                width: 100,
                attributes: alignCenter,
                filterable: FilterInTextColumn
            },
            {
                field: "Mau",
                title: "Màu",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:center;" },
                width: 70
            },
            {
                field: "DonGia",
                title: "Đơn giá",
                width: 150,
                attributes: alignRight,
                format: "{0:n2}",
                filterable: false
            },
        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    $(".btnexcel").on("click", function () {
        var filer = [];
        var filers = $("#grid").data("kendoGrid").dataSource.filter();
        if (filers != null && filers != undefined) {
            for (var i = 0; i < filers.filters.length; i++) {
                filers.filters[i].Operator = filers.filters[i].operator;
                filer.push(filers.filters[i]);
            }
        }
        location.href = crudServiceBaseUrl + '/XuatExcelDMSanPham?filters=' + JSON.stringify(filer);
    });
    $("#grid").on("dblclick", "td", function () {
        var rows = $(this).closest("tr"),
            grids = $("#grid").data("kendoGrid"),
            dataItems = grids.dataItem(rows);
        if (!rows.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            funcloadsp(dataItems);
        }
    });
    funcloadsp = function (dataItems) {
        var dataobjectload = null;
        var lstHinhAnh = [];
        CreateModalWithSize("mdlThemMoiSanPham", "100%", "95%", "tplThemMoiSanPham", "Chi tiết sản phẩm");
        $("#MauMayID").attr("disabled", "true"); $("#btnChonThietKe").attr("disabled", "true");
        ThemThuocTinh();
        LoadThietKe();
        $("#tabstrip").kendoTabStrip({
            animation: {
                open: {
                    effects: "fadeIn"
                }
            }
        });
        if (dataItems.LoaiHinhSanXuat == 1) {
            $("#btnBangXacNhanMau").addClass("hidden");
        }
        dataimg = new FormData();
        dataavatar = new FormData();
        $("#btnXoa").removeClass("hidden");
        $.ajax({
            url: crudServiceBaseUrl + "/GetDataSanPhamID",
            method: "GET",
            data: { data: dataItems.SanPhamID },
            success: function (data) {
                dataobjectload = data.data[0];
                $("#Loai").kendoDropDownList({
                    dataTextField: "text",
                    dataValueField: "value",
                    filter: "contains",
                    dataSource: loai,

                });
                $("#GioiTinh").kendoDropDownList({
                    dataTextField: "text",
                    dataValueField: "value",
                    filter: "contains",
                    dataSource: gioitinh,

                });
                $("#LoaiHinhSanXuat").kendoDropDownList({
                    dataTextField: "text",
                    dataValueField: "val",
                    dataSource: data_LoaiHinhSanXuat,
                    change: function () {
                        loadThuocTinh(dataItems.SanPhamID, this.value())
                    }
                });
                $("#Size").kendoDropDownList({
                    dataTextField: "text",
                    dataValueField: "val",
                    dataSource: sizes,
                });
                $("#SizeMauGoc").kendoDropDownList({
                    dataTextField: "text",
                    dataValueField: "val",
                    dataSource: sizes,
                });
                $("#LoaiHinhSanXuat").data("kendoDropDownList").select(0);
                $("#cbbDonVi").kendoDropDownList({
                    autoBind: true,
                    dataTextField: 'TenDonVi',
                    dataValueField: 'DonViId',
                    filter: "contains",
                    optionLabel: GetTextLanguage("chondonvi"),
                    dataSource: getDataDroplit("/DMDonVi", "/read")
                });

                var viewModel = kendo.observable({
                    SanPhamID: dataobjectload == null ? "00000000-0000-0000-0000-000000000000" : dataobjectload.SanPhamID,
                    MaSanPham: dataobjectload == null ? "" : dataobjectload.MaSanPham,
                    TenSanPham: dataobjectload == null ? "" : dataobjectload.TenSanPham,
                    DonViID: dataobjectload == null ? "" : dataobjectload.DonViID,
                    DanhSachHinhAnhs: dataobjectload == null ? "" : dataobjectload.DanhSachHinhAnhs,
                    AnhDaiDien: dataobjectload == null ? "" : dataobjectload.AnhDaiDien,
                    Loai: dataobjectload == null ? 1 : dataobjectload.Loai,
                    Mau: dataobjectload == null ? '' : dataobjectload.Mau,
                    Size: dataobjectload == null ? '' : dataobjectload.Size,
                    DonGia: dataobjectload == null ? '' : dataobjectload.DonGia,
                    GioiTinh: dataobjectload == null ? '' : dataobjectload.GioiTinh,
                    SizeMauGoc: dataobjectload == null ? '' : dataobjectload.SizeMauGoc,
                    TiLeSize: dataobjectload == null ? '' : dataobjectload.TiLeSize,
                    LoaiHinhSanXuat: dataobjectload == null ? 1 : dataobjectload.LoaiHinhSanXuat == null ? 1 : dataobjectload.LoaiHinhSanXuat,
                    MauMayID: dataobjectload == null ? 1 : dataobjectload.MauMayID == null ? 1 : dataobjectload.MauMayID,
                    YeuCauKhachHangID: dataobjectload = null ? '' : dataobjectload.YeuCauKhachHangID
                });
                kendo.bind($("#frmThemMoiSanPham"), viewModel);
                $("#DonGia").autoNumeric(autoNumericOptionsMoney);
                //$("#TiLeSize").autoNumeric(autoNumericOptionsMoney);
                if (viewModel.AnhDaiDien != null && viewModel.AnhDaiDien != "") {
                    $(".imgavatar").attr("src", localQLVT + viewModel.AnhDaiDien);
                    $(".imgavatar").attr("data-file", viewModel.AnhDaiDien);
                }
                else {
                    $(".imgavatar").attr("src", "/Content/no-image.jpg");
                    $(".imgavatar").attr("data-file", "no-image.jpg");
                }

                //image
                var lt = dataobjectload.DanhSachHinhAnhs;
                if (lt == "" || lt == null) {
                    var lst = lt;
                }
                else {
                    var lst = lt.split(',');
                    if (lst.length > 0 && lst[0] != "") {
                        for (var i = 0; i < lst.length; i++) {
                            lstHinhAnh.push({ img: lst[i], name: lst[i] });
                        }
                        var html = "";
                        for (var i = 0; i < lstHinhAnh.length; i++) {
                            if (lstHinhAnh[i].img.match(/\.(jpg|jpeg|png|gif)$/)) {
                                html += "<div class='imgslider' style='float:left; position:relative; width:70px;height:70px; margin:5px;'>";
                                html += "<div style='margin:0px;' class='img-con ahihi3 img-thumbnail'><a rel='gallery' href='" + localQLVT + lstHinhAnh[i].img + "'><img src='" + localQLVT + lstHinhAnh[i].img + "'  data-file='" + lstHinhAnh[i].img + "' style='width: 70px;height:70px;margin: 0;opacity: 1;'></a></div>";
                                html += "</div>";
                            }
                        }
                        $('.dshinhanhadd').append(html);
                        showslideimg5($('#mdlThemMoiSanPham'));
                    }
                }
                //image
                ThemCongDoan(dataItems.SanPhamID);
                ThemCongDoanCuoi(dataItems.SanPhamID);
                LoadBangXacNhanMau(dataItems.MauMayID, dataItems.YeuCauKhachHangID, dataItems.MaSanPham);
                btnLuuDMSanPham();
                $(".tab_sanxuatmau").click(function () {
                    ContentWatingOP("SanXuatThu", 0.9);
                    $("#SanXuatThu").load("/DMSanPham/ViewSanXuatSanPham?SanPhamID=" + dataItems.SanPhamID + "&DonHangID=FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF");
                });
                loadThuocTinh(viewModel.SanPhamID, viewModel.LoaiHinhSanXuat)
                editthuoctinhfunction();
                TaoTuDong($("#SanPhamID").val(), $("#YeuCauKhachHangID").val());
                InThongSo(dataItems.SanPhamID);
                LoadNguyenLieu($("#YeuCauKhachHangID").val(), $("#SanPhamID").val());
            },
            error: function (xhr, status, error) {
                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
            }
        });
        $(".boxscroll").css("height", innerHeight * 0.8);
        LoadCongDoan(dataItems.SanPhamID);
        LoadCongDoanCuoi(dataItems.SanPhamID);
        btnLuuCongDoan();
        //LoadNguyenLieu(dataItems.YeuCauKhachHangID, dataItems.SanPhamID);
        $("#btnXoa").click(function () {
            var x = confirm("Bạn có chắc chắn muốn xóa danh mục này không?");
            if (x) {
                ContentWatingOP("mdlThemMoiSanPham", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/XoaDMSanPham",
                    method: "POST",
                    data: { data: dataItems.SanPhamID },
                    success: function (data) {
                        showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                        $("#mdlThemMoiSanPham").unblock();
                        $(".windows8").css("display", "none");
                        closeModel("mdlThemMoiSanPham");
                        gridReload("grid", {});
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlThemMoiSanPham").unblock();
                        $(".windows8").css("display", "none");
                        gridReload("grid", {});
                    }
                });
            }
        });

    }
    $("#grid").on("click", "#btnAdd", function () {
        CreateModalWithSize("mdlThemMoiSanPham", "100%", "95%", "tplThemMoiSanPham", "Thêm mới sản phẩm");
        $("#tabstrip").kendoTabStrip({
            animation: {
                open: {
                    effects: "fadeIn"
                }
            }
        });
        $(".tab_dinhghiacongdoan").addClass("hidden");
        $(".tab_luachonnguyenlieu").addClass("hidden");
        $("#btnBangXacNhanMau").addClass("hidden");
        dataimg = new FormData();
        dataavatar = new FormData();
        $("#DonGia").autoNumeric(autoNumericOptionsMoney);
        //$("#TiLeSize").autoNumeric(autoNumericOptionsMoney);

        $("#Loai").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            filter: "contains",
            dataSource: loai,

        });
        $("#Size").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "val",
            dataSource: sizes,
        });
        $("#SizeMauGoc").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "val",
            dataSource: sizes,
        });
        $("#GioiTinh").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            filter: "contains",
            dataSource: gioitinh,

        });
        $("#LoaiHinhSanXuat").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "val",
            value: 1,
            dataSource: data_LoaiHinhSanXuat,
            change: function () {
                loadThuocTinh('00000000-0000-0000-0000-000000000000', this.value())
            }
        });
        $("#cbbDonVi").kendoDropDownList({
            autoBind: false,
            dataTextField: 'TenDonVi',
            dataValueField: 'DonViId',
            filter: "contains",
            optionLabel: GetTextLanguage("chondonvi"),
            dataSource: getDataDroplit("/DMDonVi", "/read"),
        });
        LoadThietKe();
        $("#btnThemDonViNhanh").click(function () {
            CreateModalWithSize("mdlThemNhanhDonVi", "25%", "20%", "tplThemNhanhDonVi", "Thêm đơn vị");
            $("#btnLuuNhanhDonVi").click(function () {
                var validate = $("#TenDonVi").kendoValidator().data("kendoValidator");
                if (validate.validate()) {
                    ContentWatingOP("mdlThemNhanhDonVi", 0.9);
                    $.ajax({
                        url: crudServiceBaseUrl + "/ThemNhanhDonVi",
                        method: "post",
                        data: { data: $("#TenDonVi").val() },
                        success: function (data) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemNhanhDonVi").unblock();
                                $(".windows8").css("display", "none");
                                closeModel("mdlThemNhanhDonVi");
                                $("#cbbDonVi").data("kendoDropDownList").dataSource.read();
                                $("#cbbDonVi").data("kendoDropDownList").refresh();
                            }, 1000);
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $("#mdlThemNhanhDonVi").unblock();
                            $(".windows8").css("display", "none");
                        }
                    });
                }
            });
        });
        ThemCongDoan();
        ThemCongDoanCuoi();
        btnLuuDMSanPham();
        ThemThuocTinh();
        loadThuocTinh('00000000-0000-0000-0000-000000000000', 1)
    });
    function takeinforThietKe(mmid) {
        var result;
        $.ajax({
            url: crudServiceBaseUrl + "/LayThongTinThietKe",
            method: "post",
            data: { tkid: mmid },
            async: false,
            success: function (data) {
                var dt = data.data;
                //console.log(dt);
                result = dt;
            }
        });
        return result;
    }
    function takeSize(yckhid) {
        var result = "";
        $.ajax({
            url: crudServiceBaseUrl + "/TakeSize",
            method: "POST",
            data: { YeuCauKhachHangID: yckhid },
            async: false,
            success: function (data) {
                var dt = data.data;
                var st = "";
                for (var i = 0; i < dt.length; i++) {
                    st += dt[i].Size + ", ";
                }
                result = st;
            },
            error: function (xhr, status, error) {
            }
        });
        return result;
    }
    function LoadBangXacNhanMau(maumayid, yckhid, msp) {
        $("#btnBangXacNhanMau").click(function () {
            CreateModalWithSize("mdlBangXacNhanMau", "100%", "95%", "tplBangXacNhanMau", "Bảng xác nhận mẫu");
            var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
            var size = takeSize(yckhid);
            var cttk = takeinforThietKe(maumayid);
            var spaceTab = "&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;";
            var htmlth = "";
            htmlth += "<tr>";
            htmlth += "<th class='text-center text-bold' style='font-size:18px;' colspan='5'>BẢNG XÁC NHẬN MẪU</th>";
            htmlth += "</tr>";
            $("#thBXNM").html(htmlth);
            var htmltd = "";
            htmltd += "<tr class='hidden'><td class='text-left' style='font-size:12px;'><input type='text' id='BangXacNhanMauID' name='BangXacNhanMauID' style='border: 1px solid #5b5e63; width: 60%; padding: 5px;'></td></tr>";
            htmltd += "<tr><td class='text-center text-bold' style='font-size:18px;' colspan='5'>Ngày:   " + d + "   Tháng:   " + m + "   Năm:   " + y + "</td></tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left text-bold' style='font-size:14px;' colspan='2'>Mã thiết kế: " + cttk.MaMauMay + "</td>";
            htmltd += "<td class='text-left text-bold' style='font-size:14px;' colspan='1'>Cấp máy: <input type='text' id='CapMay' name='CapMay' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'></td>";
            htmltd += "<td class='text-left text-bold' style='font-size:14px;' colspan='2'>Nhân viên thiết kế: <input type='text' id='NhanVienThietKe' name='NhanVienThietKe' style='border: 1px solid #5b5e63; width: 150px;padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left text-bold' style='font-size:14px;' colspan='2'>Mã hàng sx: " + msp + "</td>";
            htmltd += "<td class='text-left text-bold' style='font-size:14px;' colspan='1'>Chất liệu: " + cttk.ChatLieu + "</td>";
            htmltd += "<td class='text-left text-bold' style='font-size:14px;' colspan='1'>Thiết kế lần: <input type='text' id='ThietKeLan' name='ThietKeLan' style='border: 1px solid #5b5e63; width: 100px;padding: 5px;'></td>";
            htmltd += "<td class='text-left text-bold' style='font-size:14px;' colspan='1'>Size: " + size + "</td>";
            htmltd += "</tr>";
            htmltd += "<tr><td class='text-left text-bold' style='font-size:14px;' colspan='5'>Đặc điểm: " + cttk.DacDiem + "</td></tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-center text-bold' style='font-size:18px;' colspan='1'>Công đoạn</td>";
            htmltd += "<td class='text-center text-bold' style='font-size:18px;' colspan='3'>Diễn giải chi tiết</td>";
            htmltd += "<td class='text-center text-bold' style='font-size:18px;' colspan='1'>Ký xác nhận <br> (TG hoàn thành)</td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-center text-bold' style='font-size:14px;' colspan='1'>Dệt</td>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'>Len nền: <input type='text' id='LenNen' name='LenNen' style='border: 1px solid #5b5e63; width: 100px;padding: 5px;'> /F1: <input type='text' id='LenNenF1' name='LenNenF1' style='border: 1px solid #5b5e63; width: 100px;padding: 5px;'> /F2: <input type='text' id='LenNenF2' name='LenNenF2' style='border: 1px solid #5b5e63; width: 100px;padding: 5px;'> /F3: <input type='text' id='LenNenF3' name='LenNenF3' style='border: 1px solid #5b5e63; width: 100px;padding: 5px;'></td>";
            htmltd += "<td class='text-left' style='font-size:14px;' colspan='1' rowspan='3'>Nhận:</td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-center text-bold' style='font-size:14px;' colspan='1' rowspan='14'>Tài liệu, mật độ, <br> tốc độ, trục cuốn.</td>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'>Ly nền: <input type='text' id='LyNen' name='LyNen' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> /F1: <input type='text' id='LyNenF1' name='LyNenF1' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> /F2: <input type='text' id='LyNenF2' name='LyNenF2' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> /F3: <input type='text' id='LyNenF3' name='LyNenF3' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'>Nẹp: <input type='text' id='LyNenNep' name='LyNenNep' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> Tổng: <input type='text' id='LyNenTong' name='LyNenTong' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'>TT: <input type='text' id='TT' name='TT' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> TS: <input type='text' id='TS' name='TS' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> Tay: <input type='text' id='Tay' name='Tay' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> Cổ: <input type='text' id='Co' name='Co' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> Nẹp: <input type='text' id='Nep' name='Nep' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> Tổng: <input type='text' id='Tong' name='Tong' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'></td>";
            htmltd += "<td class='text-left' style='font-size:14px;' colspan='1' rowspan='4'>HT:</td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'>Mật độ: <input type='text' id='MatDo1' name='MatDo1' style='border: 1px solid #5b5e63; width: 60%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='MatDo2' name='MatDo2' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='MatDo3' name='MatDo3' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'>Tốc độ TK: <input type='text' id='TocDoTK' name='TocDoTK' style='border: 1px solid #5b5e63; width: 60%; padding: 5px;'></td>";
            htmltd += "<td class='text-left' style='font-size:14px;' colspan='1' rowspan='8'>Ký tên:</td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'>Tốc độ máy: <input type='text' id='TocDoMay' name='TocDoMay' style='border: 1px solid #5b5e63; width: 60%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'>Mỏ: <input type='text' id='Mo1' name='Mo1' style='border: 1px solid #5b5e63; width: 60%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='Mo2' name='Mo2' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='Mo3' name='Mo3' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='Mo4' name='Mo4' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='Mo5' name='Mo5' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'>Ghi chú: <input type='text' id='GhiChu1' name='GhiChu1' style='border: 1px solid #5b5e63; width: 60%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-center text-bold' style='font-size:14px;' colspan='1' rowspan='5'>Lingking</td>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'>Thời gian: LK <input type='text' id='LK' name='LK' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> VS <input type='text' id='VS' name='VS' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> GM <input type='text' id='GM' name='GM' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> Nẹp <input type='text' id='LingkingNep' name='LingkingNep' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'> Tổng: <input type='text' id='LingkingTong' name='LingkingTong' style='border: 1px solid #5b5e63; width: 100px; padding: 5px;'></td>";
            htmltd += "<td class='text-left' style='font-size:14px;' colspan='1'>Thời gian:</td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'>Ghi chú: <input type='text' id='GhiChu2' name='GhiChu2' style='border: 1px solid #5b5e63; width: 60%; padding: 5px;'></td>";
            htmltd += "<td class='text-left' style='font-size:14px;' colspan='1' rowspan='4'>Ký tên:</td>"
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='GhiChu3' name='GhiChu3' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='GhiChu4' name='GhiChu4' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='GhiChu5' name='GhiChu5' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-center text-bold' style='font-size:14px;' colspan='1' rowspan='3'>MAY</td>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='May1' name='May1' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "<td class='text-left' style='font-size:14px;' colspan='1'>Thời gian:</td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='May2' name='May2' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "<td class='text-left' style='font-size:14px;' colspan='1' rowspan='2'>Ký tên:</td>"
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='May3' name='May3' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-center text-bold' style='font-size:14px;' colspan='1' rowspan='3'>Giặt, sấy, là ủi</td>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='GiatSay1' name='GiatSay1' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "<td class='text-left' style='font-size:14px;' colspan='1'>Thời gian:</td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='GiatSay2' name='GiatSay2' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "<td class='text-left' style='font-size:14px;' colspan='1' rowspan='2'>Ký tên:</td>"
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='GiatSay3' name='GiatSay3' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-center text-bold' style='font-size:14px;' colspan='1'>Duyệt mẫu</td>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='DuyetMau' name='DuyetMau' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "<td class='text-left' style='font-size:14px;' colspan='1'>Ký xác nhận:</td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-center text-bold' style='font-size:14px;' colspan='1' rowspan='3'>(from áo, mặt vải, <br> hoa văn ... nhận xét)</td>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='NhanXet1' name='NhanXet1' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "<td class='text-left' style='font-size:14px;' colspan='1' rowspan='3'>Chữ ký:</td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='NhanXet2' name='NhanXet2' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<td class='text-left' style='font-size:12px;' colspan='3'><input type='text' id='NhanXet3' name='NhanXet3' style='border: 1px solid #5b5e63; width: 100%; padding: 5px;'></td>";
            htmltd += "</tr>";
            htmltd += "<tr>";
            htmltd += "<tr><td class='text-left text-bold' style='font-size:14px;' colspan='5'>Trọng lượng phối/thành phẩm: <input type='text' id='TrongLuongPhoiThanhPham' name='TrongLuongPhoiThanhPham' style='border: 1px solid #5b5e63; width: 60%; padding: 5px;'></td></tr>";
            htmltd += "</tr>";
            $("#tdBXNM").html(htmltd);
            //
            $.ajax({
                url: crudServiceBaseUrl + "/GetDataBXNM",
                method: "post",
                data: { mmid: maumayid },
                async: false,
                success: function (data) {
                    var dt = data.data;
                    if (dt != null) {
                        $("#BangXacNhanMauID").val(dt.BangXacNhanMauID); $("#CapMay").val(dt.CapMay); $("#NhanVienThietKe").val(dt.NhanVienThietKe); $("#ThietKeLan").val(dt.ThietKeLan); $("#LenNen").val(dt.LenNen);
                        $("#LenNenF1").val(dt.LenNenF1); $("#LenNenF2").val(dt.LenNenF2); $("#LenNenF3").val(dt.LenNenF3); $("#LyNen").val(dt.LyNen); $("#LyNenF1").val(dt.LyNenF1); $("#LyNenF2").val(dt.LyNenF2);
                        $("#LyNenF3").val(dt.LyNenF3); $("#LyNenNep").val(dt.LyNenNep); $("#LyNenTong").val(dt.LyNenTong); $("#TT").val(dt.TT); $("#TS").val(dt.TS); $("#Tay").val(dt.Tay); $("#Co").val(dt.Co); $("#Nep").val(dt.Nep);
                        $("#Tong").val(dt.Tong); $("#MatDo1").val(dt.MatDo1); $("#MatDo2").val(dt.MatDo2); $("#MatDo3").val(dt.MatDo3); $("#TocDoTK").val(dt.TocDoTK); $("#TocDoMay").val(dt.TocDoMay); $("#Mo1").val(dt.Mo1); $("#Mo2").val(dt.Mo2);
                        $("#Mo3").val(dt.Mo3); $("#Mo4").val(dt.Mo4); $("#Mo5").val(dt.Mo5); $("#GhiChu1").val(dt.GhiChu1); $("#LK").val(dt.LK); $("#VS").val(dt.VS); $("#GM").val(dt.GM); $("#LingkingNep").val(dt.LingkingNep);
                        $("#LingkingTong").val(dt.LingkingTong); $("#GhiChu2").val(dt.GhiChu2); $("#GhiChu3").val(dt.GhiChu3); $("#GhiChu4").val(dt.GhiChu4); $("#GhiChu5").val(dt.GhiChu5); $("#May1").val(dt.May1);
                        $("#May2").val(dt.May2); $("#May3").val(dt.May3); $("#GiatSay1").val(dt.GiatSay1); $("#GiatSay2").val(dt.GiatSay2); $("#GiatSay3").val(dt.GiatSay3); $("#DuyetMau").val(dt.DuyetMau); $("#NhanXet1").val(dt.NhanXet1);
                        $("#NhanXet2").val(dt.NhanXet2); $("#NhanXet3").val(dt.NhanXet3); $("#TrongLuongPhoiThanhPham").val(dt.TrongLuongPhoiThanhPham);
                    }
                    else {
                        $("#XuatExcelBXNM").addClass("hidden");
                    }
                }
            });
            //---------------------------------------------------------------------------------------------------------------
            $("#XuatExcelBXNM").click(function () {
                location.href = crudServiceBaseUrl + '/XuatExcelBangXacNhanMau?mmid=' + maumayid + '&msp=' + msp + '&yckhid=' + yckhid;
            });
            //
            $("#btnLuuBXNM").click(function () {
                var dataBXNM = {
                    BangXacNhanMauID: $("#BangXacNhanMauID").val() == undefined ? "00000000-0000-0000-0000-000000000000" : $("#BangXacNhanMauID").val(),
                    MauMayID: maumayid,
                    CapMay: $("#CapMay").val() == null ? "" : $("#CapMay").val(),
                    NhanVienThietKe: $("#NhanVienThietKe").val() == null ? "" : $("#NhanVienThietKe").val(),
                    ThietKeLan: $("#ThietKeLan").val() == null ? "" : $("#ThietKeLan").val(),
                    LenNen: $("#LenNen").val() == null ? "" : $("#LenNen").val(),
                    LenNenF1: $("#LenNenF1").val() == null ? "" : $("#LenNenF1").val(),
                    LenNenF2: $("#LenNenF2").val() == null ? "" : $("#LenNenF2").val(),
                    LenNenF3: $("#LenNenF3").val() == null ? "" : $("#LenNenF3").val(),
                    LyNen: $("#LyNen").val() == null ? "" : $("#LyNen").val(),
                    LyNenF1: $("#LyNenF1").val() == null ? "" : $("#LyNenF1").val(),
                    LyNenF2: $("#LyNenF2").val() == null ? "" : $("#LyNenF2").val(),
                    LyNenF3: $("#LyNenF3").val() == null ? "" : $("#LyNenF3").val(),
                    LyNenNep: $("#LyNenNep").val() == null ? "" : $("#LyNenNep").val(),
                    LyNenTong: $("#LyNenTong").val() == null ? "" : $("#LyNenTong").val(),
                    TT: $("#TT").val() == null ? "" : $("#TT").val(),
                    TS: $("#TS").val() == null ? "" : $("#TS").val(),
                    Tay: $("#Tay").val() == null ? "" : $("#Tay").val(),
                    Co: $("#Co").val() == null ? "" : $("#Co").val(),
                    Nep: $("#Nep").val() == null ? "" : $("#Nep").val(),
                    Tong: $("#Tong").val() == null ? "" : $("#Tong").val(),
                    MatDo1: $("#MatDo1").val() == null ? "" : $("#MatDo1").val(),
                    MatDo2: $("#MatDo2").val() == null ? "" : $("#MatDo2").val(),
                    MatDo3: $("#MatDo3").val() == null ? "" : $("#MatDo3").val(),
                    TocDoTK: $("#TocDoTK").val() == null ? "" : $("#TocDoTK").val(),
                    TocDoMay: $("#TocDoMay").val() == null ? "" : $("#TocDoMay").val(),
                    Mo1: $("#Mo1").val() == null ? "" : $("#Mo1").val(),
                    Mo2: $("#Mo2").val() == null ? "" : $("#Mo2").val(),
                    Mo3: $("#Mo3").val() == null ? "" : $("#Mo3").val(),
                    Mo4: $("#Mo4").val() == null ? "" : $("#Mo4").val(),
                    Mo5: $("#Mo5").val() == null ? "" : $("#Mo5").val(),
                    GhiChu1: $("#GhiChu1").val() == null ? "" : $("#GhiChu1").val(),
                    LK: $("#LK").val() == null ? "" : $("#LK").val(),
                    VS: $("#VS").val() == null ? "" : $("#VS").val(),
                    GM: $("#GM").val() == null ? "" : $("#GM").val(),
                    LingkingNep: $("#LingkingNep").val() == null ? "" : $("#LingkingNep").val(),
                    LingkingTong: $("#LingkingTong").val() == null ? "" : $("#LingkingTong").val(),
                    GhiChu2: $("#GhiChu2").val() == null ? "" : $("#GhiChu2").val(),
                    GhiChu3: $("#GhiChu3").val() == null ? "" : $("#GhiChu3").val(),
                    GhiChu4: $("#GhiChu4").val() == null ? "" : $("#GhiChu4").val(),
                    GhiChu5: $("#GhiChu5").val() == null ? "" : $("#GhiChu5").val(),
                    May1: $("#May1").val() == null ? "" : $("#May1").val(),
                    May2: $("#May2").val() == null ? "" : $("#May2").val(),
                    May3: $("#May3").val() == null ? "" : $("#May3").val(),
                    GiatSay1: $("#GiatSay1").val() == null ? "" : $("#GiatSay1").val(),
                    GiatSay2: $("#GiatSay2").val() == null ? "" : $("#GiatSay2").val(),
                    GiatSay3: $("#GiatSay3").val() == null ? "" : $("#GiatSay3").val(),
                    DuyetMau: $("#DuyetMau").val() == null ? "" : $("#DuyetMau").val(),
                    NhanXet1: $("#NhanXet1").val() == null ? "" : $("#NhanXet1").val(),
                    NhanXet2: $("#NhanXet2").val() == null ? "" : $("#NhanXet2").val(),
                    NhanXet3: $("#NhanXet3").val() == null ? "" : $("#NhanXet3").val(),
                    TrongLuongPhoiThanhPham: $("#TrongLuongPhoiThanhPham").val() == null ? "" : $("#TrongLuongPhoiThanhPham").val(),
                };
                ContentWatingOP("mdlBangXacNhanMau", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/AddOrUpdateBXNM",
                    method: "post",
                    data: { data: JSON.stringify(dataBXNM) },
                    success: function (data) {
                        $("#mdlBangXacNhanMau").unblock();
                        closeModel("mdlBangXacNhanMau");
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlBangXacNhanMau").unblock();
                    }
                });
            });
        })
    }
    function loadThuocTinh(SanPhamID, LoaiHinhSanXuat) {
        ContentWatingOP("ThongTinChung", 0.9);
        $("#ThongTinChung .panel-body tr").html("");
        $.ajax({
            url: crudServiceBaseUrl + "/LayDanhSachThuocTinh",
            method: "GET",
            data: { data: SanPhamID, LoaiHinhSanXuat: LoaiHinhSanXuat == null ? 1 : LoaiHinhSanXuat },
            success: function (data) {
                setTimeout(function () {
                    for (var i = 0; i < data.tts.length; i++) {
                        if (data.tts[i] != null) {
                            if (data.tts[i].LoaiHinh == 1) {
                                var txt = '<th style="padding:10px 2px;width:50px;background: #3da2d2;color:white;text-align:center">' + data.tts[i].TenThuocTinh + '</th>';
                                $("#ThongTinChung .panel-body thead tr").append(txt);
                                var txt2 = '<td><input  type="text" class="form-control thuoctinhchung ' + (data.tts[i].KieuDuLieu == 1 ? "number" : "text") + '" data-id="' + data.tts[i].ThuocTinhSanPhamID + '" value="' + (data.tts[i].GiaTri != null ? data.tts[i].GiaTri : "") + '" data-text="' + data.tts[i].TenThuocTinh + '" style="box-sizing:border-box"></td>';
                                $("#ThongTinChung .panel-body tbody tr").append(txt2);
                            }
                        }
                    }
                    $(".number").autoNumeric(autoNumericOptionsMoney);
                    $("#ThongTinChung").unblock();
                }, 1000)

            },
            error: function (xhr, status, error) {
                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
            }
        });

    }
    function btnLuuDMSanPham() {
        //Image---------------<

        $("#frmThemMoiSanPham #files_0").change(function () {
            for (var x = 0; x < this.files.length; x++) {
                dataimg.append("uploads", this.files[x]);
            };
            loadimgSuaChua5(this, $("#frmThemMoiSanPham"));
            $("#frmThemMoiSanPham #files_0").val('').clone(true);
        });
        //avatar

        $("#frmThemMoiSanPham #file_1").change(function () {
            for (var x = 0; x < this.files.length; x++) {
                dataavatar.append("uploads", this.files[x]);
            };
            loadimgavatar(this, $("#frmThemMoiSanPham"));
            $("#frmThemMoiSanPham #file_1").val('').clone(true);
        })
        //Image------------------------>
        $("#btnLuuThemMoi").click(function () {
            var validator = $("#frmThemMoiSanPham").kendoValidator().data("kendoValidator");
            if (validator.validate()) {
                //image
                var lstcongdoan = [];
                var vl = true;


                if (vl) {
                    var hinhanh = "";
                    var ins = dataimg.getAll("uploads").length;
                    var DSHinhAnh = [];
                    $('.dshinhanhadd .imgslider').each(function (index2, element2) {
                        if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                            DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                        }
                    });
                    if (ins > 0) {
                        var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", dataimg.getAll("uploads"), $(".dshinhanhadd", $('#frmThemMoiSanPham'))).split(',');
                        for (var k = 0; k < strUrl.length; k++) {
                            DSHinhAnh.push({ Url: strUrl[k] });
                        }
                    }
                    for (var i = 0; i < DSHinhAnh.length; i++) {
                        if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                            hinhanh += DSHinhAnh[i].Url + ",";
                        }
                    }
                    //avatar
                    var inss = dataavatar.getAll("uploads").length;
                    var anhdaidien = $('.anhdaidien img').attr("data-file");
                    if (inss > 0) {
                        var strUrl = uploadAvatarFileParagamWidthFile("/upload" + "/Uploadfiles", dataavatar.getAll("uploads"), $(".anhdaidien", $('#frmThemMoiNguyenLieu')));
                        anhdaidien = strUrl;
                    }
                    //
                    var tts = [];
                    $("#ThongTinChung input").each(function (index, element) {
                        var n = {
                            ThuocTinhSanPhamID: $(element).attr("data-id"),
                            TenThuocTinh: $(element).attr("data-text"),
                            GiaTri: $(element).hasClass("number") ? $(element).autoNumeric("get") : $(element).val(),
                            LoaiHinh: 1,
                            KieuDuLieu: $(element).hasClass("number") ? 1 : 2
                        };
                        tts.push(n);

                    });
                    //$("#ThongTinRieng input").each(function (index, element) {
                    //    var n = {
                    //        ThuocTinhSanPhamID: '',
                    //        TenThuocTinh: $(element).closest(".form-group").find("label").text(),
                    //        GiaTri: $(element).hasClass("number") ? $(element).autoNumeric("get") : $(element).val(),
                    //        LoaiHinh: 2,
                    //        KieuDuLieu: $(element).hasClass("number") ? 1 : 2
                    //    };
                    //    tts.push(n);
                    //});
                    //
                    var dataSanPham = {
                        SanPhamID: $("#SanPhamID").val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#SanPhamID").val(),
                        MaSanPham: $("#MaSanPham").val(),
                        TenSanPham: $("#TenSanPham").val(),
                        DonViId: $("#cbbDonVi").val(),
                        MauMayID: $("#MauMayID").val(),
                        Loai: $("#Loai").val(),
                        GioiTinh: $("#GioiTinh").val(),
                        LoaiHinhSanXuat: $("#LoaiHinhSanXuat").val(),
                        SizeMauGoc: $("#SizeMauGoc").val(),
                        TiLeSize: $("#TiLeSize").val(),
                        Mau: $("#Mau").val(),
                        Size: $("#Size").val(),
                        DanhSachHinhAnhs: hinhanh.substring(0, hinhanh.length - 1),
                        AnhDaiDien: anhdaidien,
                        DonGia: $("#DonGia").autoNumeric('get') == "" ? "0" : $("#DonGia").autoNumeric('get')
                    }
                    ContentWatingOP("mdlThemMoiSanPham", 0.9);
                    $.ajax({
                        url: crudServiceBaseUrl + "/AddOrUpdateDMSanPham",
                        method: "post",
                        data: { data: JSON.stringify(dataSanPham), tts: JSON.stringify(tts) },
                        success: function (data) {
                            showToast(data.code, data.code, data.message);
                            if (data.code == "success") {
                                $("#mdlThemMoiSanPham").unblock();
                                funcloadsp({ SanPhamID: data.SanPhamID });
                                //closeModel("mdlThemMoiSanPham");
                                gridReload("grid", {});
                            } else {
                                $("#mdlThemMoiSanPham").unblock();
                            }
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $("#mdlThemMoiSanPham").unblock();
                            gridReload("grid", {});
                        }
                    });
                }

            }
        });
    }
    function btnLuuCongDoan() {
        $("#btnLuuCongDoan").click(function () {
            var lstcongdoan = [];
            var vl = true;
            $(".congdoanbox").each(function (index, element) {
                if ($(".TenCongDoan", $(element)).val() == "") {
                    showToast("warning", GetTextLanguage("thatbai"), "Nhập tên công đoạn");
                    vl = false;
                } else if ($(".gridDanhSachThanhPham", $(element)).data("kendoGrid").dataSource.data().length == 0) {
                    showToast("warning", GetTextLanguage("thatbai"), "Chọn thành phẩm cho công đoạn");
                    vl = false;
                }
                var DSHinhAnh = [];
                var hinhanh = "";
                $('.imgslider', $(element)).each(function (index2, element2) {
                    if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                        DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                    }
                });
                var ins = $(".congdoanbox .box-anh input")[0].files.length;
                if (ins > 0) {
                    var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", $(".box-anh input", $(element))[0].files, $(".dshinhanh", $(element))).split(',');
                    for (var k = 0; k < strUrl.length; k++) {
                        DSHinhAnh.push({ Url: strUrl[k] });
                    }
                }
                for (var i = 0; i < DSHinhAnh.length; i++) {
                    if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                        hinhanh += DSHinhAnh[i].Url + ",";
                    }
                }
                var cd = {
                    TenCongDoan: $(".TenCongDoan", $(element)).val(),
                    ThoiGianHoanThanh: $(".ThoiGianHoanThanh", $(element)).autoNumeric('get') == "" ? "0" : $(".ThoiGianHoanThanh", $(element)).autoNumeric('get'),
                    HaoHut: $(".HaoHut").autoNumeric('get') == "" ? "0" : $(".HaoHut").autoNumeric('get'),
                    DMHangMucSanXuatID: $(element).data("dmhangmucsanxuat"),
                    MoTa: $(".MoTa", $(element)).val(),
                    GiaTri: $(".GiaTri", $(element)).val(),
                    DsHinhAnh: hinhanh,
                    item: $(".gridDanhSachThanhPham", $(element)).data("kendoGrid").dataSource.data(),
                    vitri: index,

                };
                lstcongdoan.push(cd);
            });
            var congdoancuoi = {};
            if ($(".congdoanbox2").length > 0) {
                var DSHinhAnh = [];
                var hinhanh = "";
                $('.imgslider', $(".congdoanbox2")).each(function (index2, element2) {
                    if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                        DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                    }
                });
                var ins = $(".box-anh input", $(".congdoanbox2"))[0].files.length;
                if (ins > 0) {
                    var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", $(".box-anh input", $(".congdoanbox2"))[0].files, $(".dshinhanh", $(".congdoanbox2"))).split(',');
                    for (var k = 0; k < strUrl.length; k++) {
                        DSHinhAnh.push({ Url: strUrl[k] });
                    }
                }
                for (var i = 0; i < DSHinhAnh.length; i++) {
                    if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                        hinhanh += DSHinhAnh[i].Url + ",";
                    }
                }
                congdoancuoi.ThoiGianHoanThanh = $("#ThoiGianHoanThanh", $(".congdoanbox2")).autoNumeric('get') == "" ? "0" : $("#ThoiGianHoanThanh", $(".congdoanbox2")).autoNumeric('get'),
                    congdoancuoi.HaoHut = 0;
                congdoancuoi.MoTa = $(".MoTa", $(".congdoanbox2")).val(),
                    congdoancuoi.item = $(".gridDanhSachThanhPham", $("#boxCongDoanCuoi")).data("kendoGrid").dataSource.data();
                congdoancuoi.GiaTri = $(".GiaTri", $(".congdoanbox2")).val();
                congdoancuoi.DsHinhAnh = hinhanh;
                if ($(".gridDanhSachThanhPham", $("#boxCongDoanCuoi")).data("kendoGrid").dataSource.data().length == 0) {
                    showToast("warning", GetTextLanguage("thatbai"), "Nhập thành phẩm cho công đoạn cuối");
                    vl = false;
                }
            };

            if (vl) {
                ContentWatingOP("mdlThemMoiSanPham", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/CapNhatCongDoan",
                    method: "post",
                    data: { SanPhamID: $("#SanPhamID").val(), lstcongdoan: JSON.stringify(lstcongdoan), congdoancuoi: JSON.stringify(congdoancuoi) },
                    success: function (data) {
                        showToast(data.code, data.code, data.message);
                        if (data.code == "success") {
                            $("#mdlThemMoiSanPham").unblock();
                            //funcloadsp({ SanPhamID: $("#SanPhamID").val() });
                            //closeModel("mdlThemMoiSanPham");
                            //gridReload("grid", {});
                        } else {
                            $("#mdlThemMoiSanPham").unblock();
                        }
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlThemMoiSanPham").unblock();
                        gridReload("grid", {});
                    }
                });
            }
        });
    }
    function ThemCongDoan(SanPhamID) {
        $("#btnThemCongDoan").click(function () {

            $("#boxCongDoan").append(kendo.template($("#tplThemCongDoan").html()));
            var countcongdoan = $("#boxCongDoan .congdoanbox").last();

            $(".HaoHut", countcongdoan).autoNumeric(autoNumericOptionsMoney);
            $(".ThoiGianHoanThanh", countcongdoan).autoNumeric(autoNumericOptionsMoney);

            $(".gridDanhSachThanhPham", countcongdoan).kendoGrid({
                dataSource: null,
                editable: {
                    confirmation: GetTextLanguage("xoabangi"),
                    confirmDelete: "Yes",
                },
                columns: [
                    {
                        field: "AnhDaiDien", title: "Ảnh",
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn,
                        template: kendo.template($("#tplAnh").html()),
                        width: 70
                    },
                    {
                        field: "TenThanhPham", width: 200,
                        title: "Tên thành phẩm",
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "MaThanhPham",
                        width: 100,
                        attributes: { style: "text-align:left;" },
                        title: "Mã thành phẩm",
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "TenDonVi", width: 100,
                        title: GetTextLanguage("donvi"),
                        attributes: { "style": "text-align:left !important;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "SoLuong", width: 100,
                        title: GetTextLanguage("soluong"),
                        format: "{0:n2}",
                        attributes: alignRight,
                        filterable: FilterInColumn,
                        editor: function (container, options) {
                            rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                            var input = $('<input type="text" data-v-min="0" style="height:16px;width:85%;" class="form-control" id="' + options.model.get("NguyenLieuID") + '" name="SoLuong" />');
                            input.appendTo(container);
                            input.autoNumeric(autoNumericOptionsSoLuong);
                        },
                    },
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                        template: '<a class="k-button k-button-icontext k-grid-delete" href="javascript:void(0)"><span class="k-icon k-i-delete"></span>Xóa</a>',
                        width: 100
                    },
                ],
                save: function (e) {
                    var items = $(".gridDanhSachThanhPham", countcongdoan).data("kendoGrid").dataSource.data();
                    if (e.values.SoLuong != null) {
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].ThanhPhamID == e.model.ThanhPhamID) {
                                items[i].SoLuong = e.values.SoLuong;
                                break;
                            }
                        }
                    }
                    $(".gridDanhSachThanhPham", countcongdoan).data("kendoGrid").dataSource.data(items);
                },
                dataBound: function (e) {
                    showslideimg10();
                },
            });
            XoaCongDoan();
            ThemThanhPham(SanPhamID);
            ThemThanhPhamDaCo(SanPhamID);
            ShowChiTietThanhPham($(".gridDanhSachThanhPham", countcongdoan), SanPhamID);
        })
    }
    function ThemCongDoanCuoi(SanPhamID) {
        $("#btnThemCongDoanCuoi").click(function () {
            if ($("#boxCongDoanCuoi .congdoanbox2").length > 0) {
                showToast("warning", GetTextLanguage("thatbai"), "Đã có công đoạn cuối");
                return false;
            }
            $("#boxCongDoanCuoi").append(kendo.template($("#tplThemCongDoanCuoi").html()));
            var countcongdoan = $("#boxCongDoanCuoi .congdoanbox2");

            $("#HaoHut", countcongdoan).autoNumeric(autoNumericOptionsMoney);
            $("#ThoiGianHoanThanh", countcongdoan).autoNumeric(autoNumericOptionsMoney);

            $(".gridDanhSachThanhPham", countcongdoan).kendoGrid({
                dataSource: null,
                editable: {
                    confirmation: GetTextLanguage("xoabangi"),
                    confirmDelete: "Yes",
                },
                columns: [
                    {
                        field: "AnhDaiDien", title: "Ảnh",
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn,
                        template: kendo.template($("#tplAnh").html()),
                        width: 70
                    },
                    {
                        field: "TenThanhPham", width: 200,
                        title: "Tên thành phẩm",
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "MaThanhPham",
                        width: 100,
                        attributes: { style: "text-align:left;" },
                        title: "Mã thành phẩm",
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "TenDonVi", width: 100,
                        title: GetTextLanguage("donvi"),
                        attributes: { "style": "text-align:left !important;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "SoLuong", width: 100,
                        title: GetTextLanguage("soluong"),
                        format: "{0:n2}",
                        attributes: alignRight,
                        filterable: FilterInColumn,
                        editor: function (container, options) {
                            rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                            var input = $('<input type="text" data-v-min="0" style="height:16px;width:85%;" class="form-control" id="' + options.model.get("NguyenLieuID") + '" name="SoLuong" />');
                            input.appendTo(container);
                            input.autoNumeric(autoNumericOptionsSoLuong);
                        },
                    },
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                        template: '<a class="k-button k-button-icontext k-grid-delete" href="javascript:void(0)"><span class="k-icon k-i-delete"></span>Xóa</a>',
                        width: 100
                    },
                ],
                save: function (e) {
                    var items = $(".gridDanhSachThanhPham", countcongdoan).data("kendoGrid").dataSource.data();
                    if (e.values.SoLuong != null) {
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].NguyenLieuID == e.model.NguyenLieuID) {
                                items[i].ThanhTien = (e.model.DonGia == null ? 1 : e.model.DonGia) * (e.values.SoLuong == null ? 1 : e.values.SoLuong);
                                items[i].SoLuong = e.values.SoLuong;
                                break;
                            }
                        }
                    }
                    $(".gridDanhSachThanhPham", countcongdoan).data("kendoGrid").dataSource.data(items);
                },
                dataBound: function (e) {
                    showslideimg10();
                },
            });
            XoaCongDoan();
            ThemThanhPhamCuoi(SanPhamID);
        })
    }
    function XoaCongDoan() {
        $(".xoacongdoan").click(function () {
            if (confirm("Bạn có chắc chắn muốn xóa công đoạn này?")) {
                $(this).closest(".congdoanbox").remove();
            }

        });
    }
    function ThemThanhPham(SanPhamID) {
        $(".themthanhpam").click(function () {
            CreateModalWithSize("mdlThemMoiThanhPham", "100%", "95%", "tplThemMoiThanhPham", "Thêm thành phẩm cho công đoạn");
            var div = $(this).closest(".congdoanbox");
            dataimg2 = new FormData();
            dataavatar2 = new FormData();
            $("#DonGia", $("#frmThemMoiThanhPham")).autoNumeric(autoNumericOptionsMoney);
            $("#cbbDonVi", $("#frmThemMoiThanhPham")).kendoDropDownList({
                autoBind: false,
                dataTextField: 'TenDonVi',
                dataValueField: 'DonViId',
                filter: "contains",
                optionLabel: GetTextLanguage("chondonvi"),
                dataSource: getDataDroplit("/DMDonVi", "/read"),
            });
            LoadDinhMuc("");
            btnLuuDMThanhPham(div, SanPhamID);



            $("#btnChonNguyenLieu").click(function () {
                if (true) {
                    CreateModalWithSize("mdlDanhSachNguyenLieu", "99%", "90%", "tplDanhSachNguyenLieu", "Dannh sách nguyên liệu");
                    $("#gridDanhSachNguyenLieu").kendoGrid({
                        dataSource: new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: "/DMThanhPham" + "/LayDanhSachNguyenLieuDeDinhNghia?SanPhamID=" + SanPhamID,
                                    dataType: "json",
                                    type: "GET",
                                    contentType: "application/json; charset=utf-8",
                                },
                            },
                            schema: {
                                data: "data",
                                total: "total",
                                model: {
                                    id: "NguyenLieuID",
                                    fields: {
                                        TenNguyenLieu: { type: "string", editable: false },
                                        MaNguyenLieu: { type: "string", editable: false },
                                        TenDonVi: { type: "string", editable: false },
                                        SoLuong: { type: "number", editable: true },
                                        LoaiHinh: { type: "number", editable: false },
                                    }
                                }
                            },
                            change: function (e) {
                                var data = this.data();
                            }
                        }),
                        filterable: {
                            mode: "row"
                        },
                        sortable: true,
                        resizable: true,
                        editable: true,
                        height: innerHeight * 0.75,
                        width: "100%",
                        dataBound: function (e) {
                            showslideimg10();
                        },
                        columns: [
                            {
                                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                                template: "#= ++record #",
                                width: 60,
                                attributes: alignCenter
                            },
                            {
                                field: "LoaiHinh", width: 100,
                                title: "Loại Hình",
                                attributes: { style: "text-align:left;" },
                                filterable: {
                                    cell: {
                                        template: function (args) {
                                            // create a DropDownList of unique values (colors)
                                            args.element.kendoDropDownList({
                                                dataSource: [{ id: 1, text: "Nguyên liệu" }, { id: 2, text: "Thành phẩm" }],
                                                dataTextField: "text",
                                                dataValueField: "id",
                                                valuePrimitive: true
                                            });
                                        },
                                        showOperators: false
                                    }
                                },
                                template: kendo.template($("#tplLoaiHinh").html())
                            },
                            {
                                field: "AnhDaiDien", title: "Ảnh",
                                attributes: { style: "text-align:left;" },
                                filterable: FilterInTextColumn,
                                template: kendo.template($("#tplAnh").html()),
                                width: 70
                            },
                            {
                                field: "TenNguyenLieu",
                                title: "Tên nguyên liệu",
                                filterable: FilterInTextColumn,
                                attributes: alignLeft,
                                width: 200
                            },
                            {
                                field: "MaNguyenLieu",
                                title: "Mã nguyên liệu",
                                filterable: FilterInTextColumn,
                                attributes: alignLeft,
                                width: 150
                            },
                            {
                                field: "TenDonVi",
                                title: GetTextLanguage("donvi"),
                                width: 200,
                                attributes: { "style": "text-align:left !important;" },
                                filterable: FilterInTextColumn
                            },
                            {
                                field: "SoLuong",
                                width: 100,
                                title: GetTextLanguage("soluong"),
                                format: "{0:n2}",
                                attributes: {
                                    "class": "soluongcheck",
                                    "style": "font-weight:bold"
                                },
                                filterable: FilterInColumn,
                                editor: function (container, options) {
                                    rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                                    var input = $('<input type="text" data-v-min="0" style="width:75%;height:15px;" class="form-control" id="' + options.model.get("NguyenLieuID") + '" name="SoLuong" />');
                                    input.appendTo(container);
                                    input.autoNumeric(autoNumericOptionsSoLuong);
                                },
                            },
                            {
                                field: "TenLoaiVai", width: 100,
                                title: "Loại vải",
                                attributes: alignRight,
                                filterable: FilterInColumn,
                                editor: function (container, options) {
                                    var input = $('<input data-text-field="TenLoaiVai" data-value-field="LoaiVai"  id="LoaiVai" name="LoaiVai" />');
                                    input.appendTo(container);
                                    input.kendoDropDownList({
                                        dataTextField: "TenLoaiVai",
                                        dataValueField: "LoaiVai",
                                        filter: "contains",
                                        value: options.model.LoaiVai,
                                        dataSource: [{ LoaiVai: 1, TenLoaiVai: "Vải chính" },
                                        { LoaiVai: 2, TenLoaiVai: "Vải phối lót" },
                                        { LoaiVai: 3, TenLoaiVai: "Vải phối P1" },
                                        { LoaiVai: 4, TenLoaiVai: "Vải phối P2" },
                                        { LoaiVai: 5, TenLoaiVai: "Chỉ may" },
                                        { LoaiVai: 6, TenLoaiVai: "Mác" },
                                        { LoaiVai: 7, TenLoaiVai: "Cúc" },
                                        { LoaiVai: 8, TenLoaiVai: "PK khác" }],
                                        change: function () {
                                            var items = $("#gridDanhSachNguyenLieu").data("kendoGrid").dataSource.data();
                                            for (var i = 0; i < items.length; i++) {
                                                if (items[i].NguyenLieuID === options.model.NguyenLieuID) {
                                                    items[i].TenLoaiVai = this.dataItem().TenLoaiVai;
                                                    items[i].LoaiVai = this.dataItem().LoaiVai;
                                                    $("#gridDanhSachNguyenLieu").data("kendoGrid").dataSource.data(items);
                                                    break;
                                                }
                                            }
                                        }
                                    });
                                },
                            },
                        ],

                    });
                    $("#btnDongY").click(function () {
                        var arrData = $("#gridDinhNghia").data("kendoGrid").dataSource.data();
                        var dataItem = $("#gridDanhSachNguyenLieu").data("kendoGrid").dataSource.data();
                        for (var i = 0; i < dataItem.length; i++) {
                            if (dataItem[i].SoLuong != null) {
                                var isExsit = false;
                                for (var j = 0; j < arrData.length; j++) {
                                    if (dataItem[i].NguyenLieuID == arrData[j].NguyenLieuID && dataItem[i].LoaiVai == arrData[j].LoaiVai) {
                                        isExsit = true;
                                        arrData[j].SoLuong = dataItem[i].SoLuong;
                                        arrData[j].LoaiVai = dataItem[i].LoaiVai;
                                        arrData[j].TenLoaiVai = dataItem[i].TenLoaiVai;
                                    }
                                }
                                if (!isExsit) {
                                    arrData.push(dataItem[i]);
                                }
                            }
                        }
                        $("#mdlDanhSachNguyenLieu").data("kendoWindow").close();
                        $("#gridDinhNghia").data("kendoGrid").dataSource.data(arrData);
                    });
                    $("#btnHuyChonVatTu").click(function () {
                        $("#mdlDanhSachNguyenLieu").data("kendoWindow").close();
                    });
                }
            });

        });
    }
    function ThemThanhPhamDaCo(SanPhamID) {
        $(".themthanhphamdaco").click(function () {
            CreateModalWithSize("mdlThemCongDoan", "100%", "95%", "tplThemCongDoan2", "Chọn thành phẩm cho công đoạn");
            var div = $(this).closest(".congdoanbox");

            $(".gridDanhSachThanhPhamChon").kendoGrid({
                dataSource: new kendo.data.DataSource(
                    {
                        transport: {
                            read: {
                                url: crudServiceBaseUrl + "/LayDanhSachThanhPham?SanPhamID=" + SanPhamID,
                                dataType: 'json',
                                type: 'GET',
                                contentType: "application/json; charset=utf-8",
                            }
                        },
                        batch: true,
                        //pageSize: 20,
                        sort: [{ field: "SoLuong", dir: "desc" }],
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: "ThanhPhamID",
                                fields: {
                                    ThanhPhamID: { editable: false },
                                    TenDonVi: { type: "string", editable: false },
                                    TenThanhPham: { type: "string", editable: false },
                                    MaThanhPham: { type: "string", editable: false },
                                    SoLuong: { type: "number", editable: true },
                                }
                            }
                        },
                    }),
                height: window.innerHeight * 0.80,
                width: "100%",
                filterable: {
                    mode: "row"
                },
                editable: true,
                sortable: true,
                resizable: true,
                dataBinding: function () {
                    record = 0;
                },
                pageable: pageableAll,
                columns: [
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                        template: "#= ++record #",
                        width: 60,
                        attributes: alignCenter
                    },
                    {
                        field: "AnhDaiDien", title: "Ảnh",
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn,
                        template: kendo.template($("#tplAnh").html()),
                        width: 70
                    },
                    {
                        field: "TenThanhPham", width: 200,
                        title: "Tên thành phẩm",
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "MaThanhPham",
                        width: 100,
                        attributes: { style: "text-align:left;" },
                        title: "Mã thành phẩm",
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "TenDonVi", width: 100,
                        title: GetTextLanguage("donvi"),
                        attributes: { "style": "text-align:left !important;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "SoLuong", width: 100,
                        title: GetTextLanguage("soluong"),
                        format: "{0:n2}",
                        attributes: alignRight,
                        filterable: FilterInColumn,
                        editor: function (container, options) {
                            rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                            var input = $('<input type="text" data-v-min="0" style="height:16px;width:85%;" class="form-control" id="' + options.model.get("NguyenLieuID") + '" name="SoLuong" />');
                            input.appendTo(container);
                            input.autoNumeric(autoNumericOptionsSoLuong);
                        },
                    },

                ],
                dataBound: function (e) {
                    showslideimg10();
                },
            });

            $("#btnDongY").click(function () {
                var arrData = $(".gridDanhSachThanhPham", div).data("kendoGrid").dataSource.data();
                var dataItem = $(".gridDanhSachThanhPhamChon").data("kendoGrid").dataSource.data();
                for (var i = 0; i < dataItem.length; i++) {
                    if (dataItem[i].SoLuong != null) {
                        var isExsit = false;
                        for (var j = 0; j < arrData.length; j++) {
                            if (dataItem[i].ThanhPhamID == arrData[j].ThanhPhamID) {
                                isExsit = true;
                                arrData[j].SoLuong = dataItem[i].SoLuong;
                            }
                        }
                        if (!isExsit) {
                            arrData.push(dataItem[i]);
                        }
                    }
                }
                $("#mdlThemCongDoan").data("kendoWindow").close();
                $(".gridDanhSachThanhPham", div).data("kendoGrid").dataSource.data(arrData);
            });
        });
    }
    function ThemThanhPhamCuoi(SanPhamID) {
        $(".themthanhpam2").click(function () {
            CreateModalWithSize("mdlThemCongDoan", "100%", "95%", "tplThemCongDoan2", "Thêm nguyên liệu để tạo thành sản phẩm");
            var div = $(this).closest(".congdoanbox2");

            $(".gridDanhSachThanhPhamChon").kendoGrid({
                dataSource: new kendo.data.DataSource(
                    {
                        transport: {
                            read: {
                                url: crudServiceBaseUrl + "/LayDanhSachThanhPham?SanPhamID=" + SanPhamID,
                                dataType: 'json',
                                type: 'GET',
                                contentType: "application/json; charset=utf-8",
                            }
                        },
                        batch: true,
                        //pageSize: 20,
                        sort: [{ field: "SoLuong", dir: "desc" }],
                        schema: {
                            data: 'data',
                            total: 'total',
                            model: {
                                id: "ThanhPhamID",
                                fields: {
                                    ThanhPhamID: { editable: false },
                                    TenDonVi: { type: "string", editable: false },
                                    TenThanhPham: { type: "string", editable: false },
                                    MaThanhPham: { type: "string", editable: false },
                                    SoLuong: { type: "number", editable: true },
                                }
                            }
                        },
                    }),
                height: window.innerHeight * 0.80,
                width: "100%",
                filterable: {
                    mode: "row"
                },
                editable: true,
                sortable: true,
                resizable: true,
                dataBinding: function () {
                    record = 0;
                },
                pageable: pageableAll,
                columns: [
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                        template: "#= ++record #",
                        width: 60,
                        attributes: alignCenter
                    },
                    {
                        field: "AnhDaiDien", title: "Ảnh",
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn,
                        template: kendo.template($("#tplAnh").html()),
                        width: 70
                    },
                    {
                        field: "TenThanhPham", width: 200,
                        title: "Tên thành phẩm",
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "MaThanhPham",
                        width: 100,
                        attributes: { style: "text-align:left;" },
                        title: "Mã thành phẩm",
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "TenDonVi", width: 100,
                        title: GetTextLanguage("donvi"),
                        attributes: { "style": "text-align:left !important;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "SoLuong", width: 100,
                        title: GetTextLanguage("soluong"),
                        format: "{0:n2}",
                        attributes: alignRight,
                        filterable: FilterInColumn,
                        editor: function (container, options) {
                            rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                            var input = $('<input type="text" data-v-min="0" class="form-control" id="' + options.model.get("NguyenLieuID") + '" name="SoLuong" />');
                            input.appendTo(container);
                            input.autoNumeric(autoNumericOptionsSoLuong);
                        },
                    },

                ],
                dataBound: function (e) {
                    showslideimg10();
                },
            });
            $("#btnDongY").click(function () {
                var arrData = $(".gridDanhSachThanhPham", div).data("kendoGrid").dataSource.data();
                var dataItem = $(".gridDanhSachThanhPhamChon").data("kendoGrid").dataSource.data();
                for (var i = 0; i < dataItem.length; i++) {
                    if (dataItem[i].SoLuong != null) {
                        var isExsit = false;
                        for (var j = 0; j < arrData.length; j++) {
                            if (dataItem[i].ThanhPhamID == arrData[j].ThanhPhamID) {
                                isExsit = true;
                                arrData[j].SoLuong = dataItem[i].SoLuong;
                            }
                        }
                        if (!isExsit) {
                            arrData.push(dataItem[i]);
                        }
                    }
                }
                $("#mdlThemCongDoan").data("kendoWindow").close();
                $(".gridDanhSachThanhPham", div).data("kendoGrid").dataSource.data(arrData);
            });
        });
    }
    function LoadCongDoan(SanPhamID) {
        $.ajax({
            url: crudServiceBaseUrl + "/LayDanhSachCongDoan",
            method: "GET",
            data: { SanPhamID: SanPhamID },
            success: function (data) {
                for (var i = 0; i < data.data.length; i++) {
                    $("#boxCongDoan").append(kendo.template($("#tplThemCongDoan").html()));
                    var countcongdoan = $("#boxCongDoan .congdoanbox").last();
                    $(countcongdoan).attr("data-dmhangmucsanxuat", data.data[i].DMHangMucSanXuatID);
                    $(countcongdoan).attr("id", "cd-" + i);
                    $(".HaoHut", countcongdoan).val(data.data[i].HaoHut);
                    $(".GiaTri", countcongdoan).val(data.data[i].GiaTri);
                    $(".ThoiGianHoanThanh", countcongdoan).val(data.data[i].ThoiGianHoanThanh);
                    $(".TenCongDoan", countcongdoan).val(data.data[i].TenCongDoan);
                    $(".MoTa", countcongdoan).val(data.data[i].MoTa);

                    $(".HaoHut", countcongdoan).autoNumeric(autoNumericOptionsMoney);
                    $(".ThoiGianHoanThanh", countcongdoan).autoNumeric(autoNumericOptionsMoney);
                    $(".GiaTri", countcongdoan).autoNumeric(autoNumericOptionsMoney);
                    if (data.data[i].DsHinhAnh != null && data.data[i].DsHinhAnh != "") {
                        var lst = data.data[i].DsHinhAnh.split(',');
                        if (lst.length > 0 && lst[0] != "") {
                            var html2 = "";
                            for (var j = 0; j < lst.length; j++) {
                                if (lst[j] != "") {
                                    html2 += "<div class='imgslider' style='float:left; position:relative; width:70px;height:70px; margin:5px;'>";
                                    html2 += "<div style='margin:0px;' class='img-con ahihi3 img-thumbnail'><a rel='gallery' href='" + localQLVT + lst[j] + "'><img src='" + localQLVT + lst[j] + "'  data-file='" + lst[j] + "' style='width: 70px;height:70px;margin: 0;opacity: 1;'></a></div>";
                                    html2 += "</div>";
                                }
                            }
                            $('.dshinhanh', countcongdoan).html(html2);

                        }
                    }
                    $(".gridDanhSachThanhPham", countcongdoan).kendoGrid({
                        dataSource: new kendo.data.DataSource(
                            {
                                data: data.data[i].item,
                                sort: [{ field: "TenThanhPham", dir: "asc" }],
                                schema: {
                                    model: {
                                        id: "ThanhPhamID",
                                        fields: {
                                            ThanhPhamID: { editable: false },
                                            TenDonVi: { type: "string", editable: false },
                                            TenThanhPham: { type: "string", editable: false },
                                            MaThanhPham: { type: "string", editable: false },
                                            SoLuong: { type: "number", editable: true },
                                        }
                                    }
                                },
                            }),
                        editable: {
                            confirmation: GetTextLanguage("xoabangi"),
                            confirmDelete: "Yes",
                        },
                        columns: [
                            {
                                field: "AnhDaiDien", title: "Ảnh",
                                attributes: { style: "text-align:left;" },
                                filterable: FilterInTextColumn,
                                template: kendo.template($("#tplAnh").html()),
                                width: 70
                            },
                            {
                                field: "TenThanhPham", width: 200,
                                title: "Tên thành phẩm",
                                attributes: { style: "text-align:left;" },
                                filterable: FilterInTextColumn
                            },
                            {
                                field: "MaThanhPham",
                                width: 100,
                                attributes: { style: "text-align:left;" },
                                title: "Mã thành phẩm",
                                filterable: FilterInTextColumn
                            },
                            {
                                field: "TenDonVi", width: 100,
                                title: GetTextLanguage("donvi"),
                                attributes: { "style": "text-align:left !important;" },
                                filterable: FilterInTextColumn
                            },
                            {
                                field: "SoLuong", width: 100,
                                title: GetTextLanguage("soluong"),
                                format: "{0:n2}",
                                attributes: alignRight,
                                filterable: FilterInColumn,
                                editor: function (container, options) {
                                    rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                                    var input = $('<input type="text" data-v-min="0" class="form-control" id="' + options.model.get("NguyenLieuID") + '" name="SoLuong" />');
                                    input.appendTo(container);
                                    input.autoNumeric(autoNumericOptionsSoLuong);
                                },
                            },
                            {
                                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                                template: '<a class="k-button k-button-icontext k-grid-delete" href="javascript:void(0)"><span class="k-icon k-i-delete"></span>Xóa</a>',
                                width: 100
                            },
                        ],
                        save: function (e) {
                            var items = $(".gridDanhSachThanhPham", countcongdoan).data("kendoGrid").dataSource.data();
                            if (e.values.SoLuong != null) {
                                for (var i = 0; i < items.length; i++) {
                                    if (items[i].ThanhPhamID == e.model.ThanhPhamID) {
                                        items[i].SoLuong = e.values.SoLuong;
                                        break;
                                    }
                                }
                            }
                            $(".gridDanhSachThanhPham", countcongdoan).data("kendoGrid").dataSource.data(items);
                        },
                        dataBound: function (e) {
                            showslideimg10();
                            if (i == 0) {
                                $("#InBangNguyenPhuLieu").removeClass("hidden");
                                $("#InBangNguyenPhuLieu").attr("href", crudServiceBaseUrl + "/InBangNguyenPhuLieu?SanPhamID=" + SanPhamID + "&ThanhPhamID=" + this._data[0].ThanhPhamID);
                            }
                        },
                    });
                    ThemThanhPham(SanPhamID);
                    ThemThanhPhamDaCo(SanPhamID);
                    ShowChiTietThanhPham($(".gridDanhSachThanhPham", countcongdoan), SanPhamID);
                    XoaCongDoan();

                }
                $(".congdoanbox .themhinhanh").on("click", function () {
                    var box = $(this).closest(".congdoanbox");
                    $(".box-anh input", box).click();
                    $(".box-anh input", box).on("change", function () {
                        $(".dshinhanh", box).html("");
                        loadimg7($(this)[0], $(".dshinhanh", box));
                    });

                });

            },
            error: function (xhr, status, error) {
                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
            }
        });
    }
    function LoadCongDoanCuoi(SanPhamID) {
        $.ajax({
            url: crudServiceBaseUrl + "/LayDanhSachCongDoanCuoi",
            method: "GET",
            data: { SanPhamID: SanPhamID },
            success: function (data) {
                if (data.data.length > 0) {
                    $("#boxCongDoanCuoi").append(kendo.template($("#tplThemCongDoanCuoi").html()));
                    var countcongdoan = $("#boxCongDoanCuoi .congdoanbox2").last();

                    $("#ThoiGianHoanThanh", countcongdoan).val(data.thoigianhoanthanh);
                    $(".MoTa", countcongdoan).val(data.MoTa);
                    $(".GiaTri", countcongdoan).val(data.GiaTri);
                    $("#ThoiGianHoanThanh", countcongdoan).autoNumeric(autoNumericOptionsMoney);
                    $(".GiaTri", countcongdoan).autoNumeric(autoNumericOptionsMoney);
                    var lt = data.DsHinhAnh;
                    var lstHinhAnh = [];
                    if (lt != null && lt != "") {
                        var lst = lt.split(',');
                        if (lst.length > 0 && lst[0] != "") {
                            for (var i = 0; i < lst.length; i++) {
                                lstHinhAnh.push({ img: lst[i], name: lst[i] });
                            }
                            var html = "";
                            for (var i = 0; i < lstHinhAnh.length; i++) {
                                if (lstHinhAnh[i].img.match(/\.(jpg|jpeg|png|gif)$/)) {
                                    html += "<div class='imgslider' style='float:left; position:relative; width:70px;height:70px; margin:5px;'>";
                                    html += "<div style='margin:0px;' class='img-con ahihi3 img-thumbnail'><a rel='gallery' href='" + localQLVT + lstHinhAnh[i].img + "'><img src='" + localQLVT + lstHinhAnh[i].img + "'  data-file='" + lstHinhAnh[i].img + "' style='width: 70px;height:70px;margin: 0;opacity: 1;'></a></div>";
                                    html += "</div>";
                                }
                            }
                            $('.dshinhanh', countcongdoan).append(html);

                        }
                    }
                    $(".gridDanhSachThanhPham", countcongdoan).kendoGrid({
                        dataSource: new kendo.data.DataSource(
                            {
                                data: data.data,
                                sort: [{ field: "TenThanhPham", dir: "asc" }],
                                schema: {
                                    model: {
                                        id: "ThanhPhamID",
                                        fields: {
                                            ThanhPhamID: { editable: false },
                                            TenDonVi: { type: "string", editable: false },
                                            TenThanhPham: { type: "string", editable: false },
                                            MaThanhPham: { type: "string", editable: false },
                                            SoLuong: { type: "number", editable: true },
                                        }
                                    }
                                },
                            }),
                        editable: {
                            confirmation: GetTextLanguage("xoabangi"),
                            confirmDelete: "Yes",
                        },
                        columns: [
                            {
                                field: "AnhDaiDien", title: "Ảnh",
                                attributes: { style: "text-align:left;" },
                                filterable: FilterInTextColumn,
                                template: kendo.template($("#tplAnh").html()),
                                width: 70
                            },
                            {
                                field: "TenThanhPham", width: 200,
                                title: "Tên thành phẩm",
                                attributes: { style: "text-align:left;" },
                                filterable: FilterInTextColumn
                            },
                            {
                                field: "MaThanhPham",
                                width: 100,
                                attributes: { style: "text-align:left;" },
                                title: "Mã thành phẩm",
                                filterable: FilterInTextColumn
                            },
                            {
                                field: "TenDonVi", width: 100,
                                title: GetTextLanguage("donvi"),
                                attributes: { "style": "text-align:left !important;" },
                                filterable: FilterInTextColumn
                            },
                            {
                                field: "SoLuong", width: 100,
                                title: GetTextLanguage("soluong"),
                                format: "{0:n2}",
                                attributes: alignRight,
                                filterable: FilterInColumn,
                                editor: function (container, options) {
                                    rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                                    var input = $('<input type="text" data-v-min="0" class="form-control" id="' + options.model.get("NguyenLieuID") + '" name="SoLuong" />');
                                    input.appendTo(container);
                                    input.autoNumeric(autoNumericOptionsSoLuong);
                                },
                            },
                            {
                                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                                template: '<a class="k-button k-button-icontext k-grid-delete" href="javascript:void(0)"><span class="k-icon k-i-delete"></span>Xóa</a>',
                                width: 100
                            },
                        ],
                        save: function (e) {
                            var items = $(".gridDanhSachThanhPham", countcongdoan).data("kendoGrid").dataSource.data();
                            if (e.values.SoLuong != null) {
                                for (var i = 0; i < items.length; i++) {
                                    if (items[i].ThanhPhamID == e.model.ThanhPhamID) {
                                        items[i].SoLuong = e.values.SoLuong;
                                        break;
                                    }
                                }
                            }
                            $(".gridDanhSachThanhPham", countcongdoan).data("kendoGrid").dataSource.data(items);
                        },
                        dataBound: function (e) {
                            showslideimg10();
                        },
                    });
                    ThemThanhPhamCuoi(SanPhamID);
                    $(".tab_sanxuatmau").removeClass("hidden");
                    TaoThemSize(SanPhamID);
                    $(".congdoanbox2 .themhinhanh").click(function () {
                        var box = $(this).closest(".congdoanbox2");
                        $(".box-anh input", box).click();
                        $(".box-anh input", box).change(function () {
                            loadimg7($(this)[0], $(".dshinhanh", box));
                        });

                    });
                    //$(".tab_sanxuatmau").click();
                }
            },
            error: function (xhr, status, error) {
                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
            }
        });
    }
    function TaoThemSize(SanPhamID) {
        $(".btn-taothemsize").click(function () {
            CreateModalWithSize("mdlChonSize", "99%", "90%", "tplChonSize", "Chọn size");
            $(".addmau").click(function () {
                if ($("#ipmau").val() != "") {
                    $("#mdlChonSize ul").append("<li style='margin-bottom: 5px;'><input type='text'  style='width: 70%;height: 14px;float: left;' class='form-control mau' value='" + $("#ipmau").val() + "'><span style='color:red;olor: red; float: left;font-weight: bold;font-size: 25px; margin-left: 10px;'> X</span><div class='clearfix'></div></li>");
                    $("#mdlChonSize ul li span").click(function () {
                        $(this).closest("li").remove();
                    })
                }
            })
            $("#btnTaoSize").click(function () {

                var size = [];
                var comau = false;
                $("#mdlChonSize input").each(function (i, e) {
                    if ($(e).is(':checked')) {

                        var lstmau = [];
                        $(e).closest(".form-check").find(".mau").each(function (o, u) {
                            lstmau.push($(u).val());
                            comau = true;
                        });
                        var s = {
                            size: $(e).val(),
                            Mau: lstmau
                        }
                        size.push(s);
                    }
                });
                if (comau) {
                    ContentWatingOP("mdlChonSize", 0.9);
                    $.ajax({
                        url: crudServiceBaseUrl + "/CopySanPham",
                        method: "post",
                        data: { SanPhamID: SanPhamID, size: JSON.stringify(size) },
                        success: function (data) {
                            showToast(data.code, data.code, data.message);
                            if (data.code == "success") {
                                closeModel("mdlChonSize");
                                closeModel("mdlThemMoiSanPham");
                                gridReload("grid", {});
                            } else {
                                $("#mdlChonSize").unblock();
                            }
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $("#mdlChonSize").unblock();
                            gridReload("grid", {});
                        }
                    });
                } else {
                    showToast("error", GetTextLanguage("thatbai"), "Phải thêm màu cho size");
                }

            });

        });
    }
    function btnLuuDMThanhPham(div, SanPhamID) {
        //Image---------------<

        $("#frmThemMoiThanhPham #files_0").change(function () {
            for (var x = 0; x < this.files.length; x++) {
                dataimg2.append("uploads", this.files[x]);
            };
            loadimgSuaChua5(this, $("#frmThemMoiThanhPham"));
            $("#frmThemMoiThanhPham #files_0").val('').clone(true);
        });
        //avatar

        $("#frmThemMoiThanhPham #file_1").change(function () {
            dataavatar = new FormData();
            for (var x = 0; x < this.files.length; x++) {
                dataavatar.append("uploads", this.files[x]);
            };
            loadimgavatar(this, $("#frmThemMoiThanhPham"));
            $("#frmThemMoiThanhPham #file_1").val('').clone(true);
        })
        //Image------------------------>
        $("#btnLuuThemMoi", $("#frmThemMoiThanhPham")).click(function () {
            var validator = $("#frmThemMoiThanhPham").kendoValidator().data("kendoValidator");
            if (validator.validate()) {
                //image
                var hinhanh = "";
                var ins = dataimg2.getAll("uploads").length;
                var DSHinhAnh = [];
                $('.dshinhanhadd .imgslider', $("#frmThemMoiThanhPham")).each(function (index2, element2) {
                    if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                        DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                    }
                });
                if (ins > 0) {
                    var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", dataimg2.getAll("uploads"), $(".dshinhanhadd", $('#frmThemMoiThanhPham'))).split(',');
                    for (var k = 0; k < strUrl.length; k++) {
                        DSHinhAnh.push({ Url: strUrl[k] });
                    }
                }
                for (var i = 0; i < DSHinhAnh.length; i++) {
                    if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                        hinhanh += DSHinhAnh[i].Url + ",";
                    }
                }
                //avatar
                var inss = dataavatar2.getAll("uploads").length;
                var anhdaidien = $('.anhdaidien img', $("#frmThemMoiThanhPham")).attr("data-file");
                if (inss > 0) {
                    var strUrl = uploadAvatarFileParagamWidthFile("/upload" + "/Uploadfiles", dataavatar2.getAll("uploads"), $(".anhdaidien", $('#frmThemMoiNguyenLieu', $("#frmThemMoiThanhPham"))));
                    anhdaidien = strUrl;
                }
                //image
                var dataThanhPham = {
                    ThanhPhamID: $("#ThanhPhamID", $("#frmThemMoiThanhPham")).val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#ThanhPhamID", $("#frmThemMoiThanhPham")).val(),
                    MaThanhPham: $("#MaThanhPham", $("#frmThemMoiThanhPham")).val(),
                    TenThanhPham: $("#TenThanhPham", $("#frmThemMoiThanhPham")).val(),
                    DonViId: $("#cbbDonVi", $("#frmThemMoiThanhPham")).val(),
                    DanhSachHinhAnhs: hinhanh.substring(0, hinhanh.length - 1),
                    AnhDaiDien: anhdaidien,
                    DonGia: $("#DonGia", $("#frmThemMoiThanhPham")).autoNumeric('get') == "" ? "0" : $("#DonGia").autoNumeric('get'),
                    SanPhamID: SanPhamID
                }
                ContentWatingOP("mdlThemMoiThanhPham", 0.9);
                $.ajax({
                    url: "/DMThanhPham" + "/AddOrUpdateDMThanhPham",
                    method: "post",
                    data: { data: JSON.stringify(dataThanhPham), listdinhnghia: JSON.stringify($("#gridDinhNghia").data("kendoGrid").dataSource.data()) },
                    success: function (data) {
                        showToast(data.code, data.code, data.message);
                        if (data.code == "success") {
                            $("#mdlThemMoiThanhPham").unblock();
                            closeModel("mdlThemMoiThanhPham");
                            var arrData = $(".gridDanhSachThanhPham", div).data("kendoGrid").dataSource.data();
                            for (var o = 0; o < arrData.length; o++) {
                                if (data.thanhpham.ThanhPhamID == arrData[o].ThanhPhamID) {
                                    arrData.splice(o, 1);
                                }
                            }
                            data.thanhpham.SoLuong = 1;
                            arrData.push(data.thanhpham);
                            $(".gridDanhSachThanhPham", div).data("kendoGrid").dataSource.data(arrData);
                        } else {
                            $("#mdlThemMoiThanhPham").unblock();
                        }
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlThemMoiThanhPham").unblock();
                        gridReload("grid", {});
                    }
                });
            }
        });
        $("#btnTheoMau", $("#frmThemMoiThanhPham")).click(function () {
            var validator = $("#frmThemMoiThanhPham").kendoValidator().data("kendoValidator");
            if (validator.validate()) {
                //image
                var hinhanh = "";
                var ins = dataimg2.getAll("uploads").length;
                var DSHinhAnh = [];
                $('.dshinhanhadd .imgslider', $("#frmThemMoiThanhPham")).each(function (index2, element2) {
                    if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                        DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                    }
                });
                if (ins > 0) {
                    var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", dataimg2.getAll("uploads"), $(".dshinhanhadd", $('#frmThemMoiThanhPham'))).split(',');
                    for (var k = 0; k < strUrl.length; k++) {
                        DSHinhAnh.push({ Url: strUrl[k] });
                    }
                }
                for (var i = 0; i < DSHinhAnh.length; i++) {
                    if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                        hinhanh += DSHinhAnh[i].Url + ",";
                    }
                }
                //avatar
                var inss = dataavatar2.getAll("uploads").length;
                var anhdaidien = $('.anhdaidien img', $("#frmThemMoiThanhPham")).attr("data-file");
                if (inss > 0) {
                    var strUrl = uploadAvatarFileParagamWidthFile("/upload" + "/Uploadfiles", dataavatar2.getAll("uploads"), $(".anhdaidien", $('#frmThemMoiNguyenLieu', $("#frmThemMoiThanhPham"))));
                    anhdaidien = strUrl;
                }
                //image
                var dataThanhPham = {
                    ThanhPhamID: $("#ThanhPhamID", $("#frmThemMoiThanhPham")).val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#ThanhPhamID", $("#frmThemMoiThanhPham")).val(),
                    MaThanhPham: $("#MaThanhPham", $("#frmThemMoiThanhPham")).val(),
                    TenThanhPham: $("#TenThanhPham", $("#frmThemMoiThanhPham")).val(),
                    DonViId: $("#cbbDonVi", $("#frmThemMoiThanhPham")).val(),
                    DanhSachHinhAnhs: hinhanh.substring(0, hinhanh.length - 1),
                    AnhDaiDien: anhdaidien,
                    DonGia: $("#DonGia", $("#frmThemMoiThanhPham")).autoNumeric('get') == "" ? "0" : $("#DonGia").autoNumeric('get'),
                    SanPhamID: SanPhamID
                }
                ContentWatingOP("mdlThemMoiThanhPham", 0.9);
                $.ajax({
                    url: "/DMThanhPham" + "/AddOrUpdateDMThanhPhamToanBoMau",
                    method: "post",
                    data: { data: JSON.stringify(dataThanhPham), listdinhnghia: JSON.stringify($("#gridDinhNghia").data("kendoGrid").dataSource.data()) },
                    success: function (data) {
                        showToast(data.code, data.code, data.message);
                        if (data.code == "success") {
                            $("#mdlThemMoiThanhPham").unblock();
                            closeModel("mdlThemMoiThanhPham");
                            var arrData = $(".gridDanhSachThanhPham", div).data("kendoGrid").dataSource.data();
                            for (var o = 0; o < arrData.length; o++) {
                                if (data.thanhpham.ThanhPhamID == arrData[o].ThanhPhamID) {
                                    arrData.splice(o, 1);
                                }
                            }
                            data.thanhpham.SoLuong = 1;
                            arrData.push(data.thanhpham);
                            $(".gridDanhSachThanhPham", div).data("kendoGrid").dataSource.data(arrData);
                        } else {
                            $("#mdlThemMoiThanhPham").unblock();
                        }
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlThemMoiThanhPham").unblock();
                        gridReload("grid", {});
                    }
                });
            }
        });
    }
    function LoadDinhMuc(ThanhPhamID) {
        $("#gridDinhNghia").kendoGrid({
            dataSource: new kendo.data.DataSource(
                {
                    transport: {
                        read: {
                            url: "/DMThanhPham" + "/LayDanhSachNguyenLieu",
                            dataType: 'json',
                            type: 'GET',
                            contentType: "application/json; charset=utf-8",
                            data: { ThanhPhamID: ThanhPhamID }
                        }
                    },
                    batch: true,
                    pageSize: 20,
                    sort: [{ field: "TenVatTu", dir: "asc" }],
                    schema: {
                        data: 'data',
                        total: 'total',
                        model: {
                            id: "NguyenLieuID",
                            fields: {
                                NguyenLieuID: { editable: false },
                                TenDonVi: { type: "string", editable: false },
                                TenNguyenLieu: { type: "string", editable: false },
                                MaNguyenLieu: { type: "string", editable: false },
                                LoaiHinh: { type: "number", editable: false },
                                SoLuong: { type: "number", editable: true },
                            }
                        }
                    },
                }),
            height: window.innerHeight * 0.80,
            width: "100%",
            filterable: {
                mode: "row"
            },
            editable: {
                confirmation: GetTextLanguage("xoabangi"),
                confirmDelete: "Yes",
            },
            sortable: true,
            resizable: true,
            dataBinding: function () {
                record = 0;
            },
            pageable: pageableAll,
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "#= ++record #",
                    width: 60,
                    attributes: alignCenter
                },
                {
                    field: "AnhDaiDien", title: "Ảnh",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    template: kendo.template($("#tplAnh").html()),
                    width: 70
                },
                {
                    field: "LoaiHinh", width: 100,
                    title: "Loại Hình",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    template: kendo.template($("#tplLoaiHinh").html())
                },
                {
                    field: "TenNguyenLieu", width: 200,
                    title: "Tên nguyên liệu",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "MaNguyenLieu",
                    width: 100,
                    attributes: { style: "text-align:left;" },
                    title: "Mã nguyên liệu",
                    filterable: FilterInTextColumn
                },
                {
                    field: "TenDonVi", width: 100,
                    title: GetTextLanguage("donvi"),
                    attributes: { "style": "text-align:left !important;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "SoLuong", width: 100,
                    title: GetTextLanguage("soluong"),
                    format: "{0:n2}",
                    attributes: alignRight,
                    filterable: FilterInColumn,
                    editor: function (container, options) {
                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                        var input = $('<input type="text" data-v-min="0" style="height:16px;width:85%;" class="form-control" id="' + options.model.get("NguyenLieuID") + '" ' + rs + ' name="SoLuong" />');
                        input.appendTo(container);
                        input.autoNumeric(autoNumericOptionsSoLuong);
                    },
                },
                {
                    field: "TenLoaiVai", width: 100,
                    title: "Loại vải",
                    attributes: alignRight,
                    filterable: FilterInColumn,
                    editor: function (container, options) {
                        var input = $('<input data-text-field="TenLoaiVai" data-value-field="LoaiVai"  id="LoaiVai" name="LoaiVai" />');
                        input.appendTo(container);
                        input.kendoDropDownList({
                            dataTextField: "TenLoaiVai",
                            dataValueField: "LoaiVai",
                            filter: "contains",
                            value: options.model.LoaiVai,
                            dataSource: [{ LoaiVai: 1, TenLoaiVai: "Vải chính" },
                            { LoaiVai: 2, TenLoaiVai: "Vải phối lót" },
                            { LoaiVai: 3, TenLoaiVai: "Vải phối P1" },
                            { LoaiVai: 4, TenLoaiVai: "Vải phối P2" },
                            { LoaiVai: 5, TenLoaiVai: "Chỉ may" },
                            { LoaiVai: 6, TenLoaiVai: "Mác" },
                            { LoaiVai: 7, TenLoaiVai: "Cúc" },
                            { LoaiVai: 8, TenLoaiVai: "PK khác" }],
                            change: function () {
                                var items = $("#gridDinhNghia").data("kendoGrid").dataSource.data();
                                for (var i = 0; i < items.length; i++) {
                                    if (items[i].NguyenLieuID === options.model.NguyenLieuID) {
                                        items[i].TenLoaiVai = this.dataItem().TenLoaiVai;
                                        items[i].LoaiVai = this.dataItem().LoaiVai;
                                        $("#gridDinhNghia").data("kendoGrid").dataSource.data(items);
                                        break;
                                    }
                                }
                            }
                        });
                    },
                },
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                    template: '<a class="k-button k-button-icontext k-grid-delete" href="javascript:void(0)"><span class="k-icon k-i-delete"></span>Xóa</a>',
                    width: 100
                },

            ],
            save: function (e) {
                var items = $("#gridDinhNghia").data("kendoGrid").dataSource.data();
                if (e.values.SoLuong != null) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].NguyenLieuID == e.model.NguyenLieuID && items[i].LoaiVai == e.model.LoaiVai) {
                            items[i].ThanhTien = (e.model.DonGia == null ? 1 : e.model.DonGia) * (e.values.SoLuong == null ? 1 : e.values.SoLuong);
                            items[i].SoLuong = e.values.SoLuong;
                            break;
                        }
                    }
                }
                $("#gridDinhNghia").data("kendoGrid").dataSource.data(items);
            },
            dataBound: function (e) {
                showslideimg10();
            },
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }
    function ShowChiTietThanhPham(grid, SanPhamID) {
        $(grid).on("dblclick", "td", function () {
            var rows = $(this).closest("tr"),
                grids = $(grid).data("kendoGrid"),
                dataItems = grids.dataItem(rows);
            if (!rows.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                var dataobjectload2 = null;
                var lstHinhAnh2 = [];
                CreateModalWithSize("mdlThemMoiThanhPham", "90%", "90%", "tplThemMoiThanhPham", "Chi tiết thành phẩm");
                dataimg2 = new FormData();
                dataavatar2 = new FormData();
                $("#btnXoa").removeClass("hidden");
                $.ajax({
                    url: "/DMThanhPham" + "/GetDataThanhPhamID",
                    method: "GET",
                    data: { data: dataItems.ThanhPhamID },
                    success: function (data) {
                        dataobjectload = data.data[0];
                        $("#DonGia", $("#frmThemMoiThanhPham")).autoNumeric(autoNumericOptionsMoney);
                        LoadDinhMuc(dataobjectload.ThanhPhamID);
                        $("#cbbDonVi", $("#frmThemMoiThanhPham")).kendoDropDownList({
                            autoBind: true,
                            dataTextField: 'TenDonVi',
                            dataValueField: 'DonViId',
                            filter: "contains",
                            optionLabel: GetTextLanguage("chondonvi"),
                            dataSource: getDataDroplit("/DMDonVi", "/read")
                        });
                        var viewModel = kendo.observable({
                            ThanhPhamID: dataobjectload == null ? "00000000-0000-0000-0000-000000000000" : dataobjectload.ThanhPhamID,
                            MaThanhPham: dataobjectload == null ? "" : dataobjectload.MaThanhPham,
                            TenThanhPham: dataobjectload == null ? "" : dataobjectload.TenThanhPham,
                            DonViID: dataobjectload == null ? "" : dataobjectload.DonViID,
                            DanhSachHinhAnhs: dataobjectload == null ? "" : dataobjectload.DanhSachHinhAnhs,
                            AnhDaiDien: dataobjectload == null ? "" : dataobjectload.AnhDaiDien,
                            DonGia: dataobjectload == null ? "0" : dataobjectload.DonGia
                        });
                        kendo.bind($("#frmThemMoiThanhPham"), viewModel);
                        if (viewModel.AnhDaiDien != null && viewModel.AnhDaiDien != "") {
                            $(".imgavatar", $("#frmThemMoiThanhPham")).attr("src", localQLVT + viewModel.AnhDaiDien);
                            $(".imgavatar", $("#frmThemMoiThanhPham")).attr("data-file", viewModel.AnhDaiDien);
                        }
                        else {
                            $(".imgavatar", $("#frmThemMoiThanhPham")).attr("src", "/Content/no-image.jpg");
                            $(".imgavatar", $("#frmThemMoiThanhPham")).attr("data-file", "no-image.jpg");
                        }
                        showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                        //image
                        var lt = dataobjectload.DanhSachHinhAnhs;
                        if (lt == "" || lt == null) {
                            var lst = lt;
                        }
                        else {
                            var lst = lt.split(',');
                            if (lst.length > 0 && lst[0] != "") {
                                for (var i = 0; i < lst.length; i++) {
                                    lstHinhAnh.push({ img: lst[i], name: lst[i] });
                                }
                                var html = "";
                                for (var i = 0; i < lstHinhAnh.length; i++) {
                                    if (lstHinhAnh[i].img.match(/\.(jpg|jpeg|png|gif)$/)) {
                                        html += "<div class='imgslider' style='float:left; position:relative; width:70px;height:70px; margin:5px;'>";
                                        html += "<div style='margin:0px;' class='img-con ahihi3 img-thumbnail'><a rel='gallery' href='" + (localQLVT + lstHinhAnh[i].img) + "'>";
                                        html += "<img src='" + localQLVT + lstHinhAnh[i].img + "'  data-file='" + (lstHinhAnh[i].img) + "' style='width: 70px;height:70px;margin: 0;opacity: 1;'></a></div>";
                                        html += "</div>";
                                    }
                                }
                                $('.dshinhanhadd', $("#frmThemMoiThanhPham")).append(html);
                                showslideimg5($('#frmThemMoiThanhPham'));
                            }
                        }
                        //image
                        btnLuuDMThanhPham(grid.closest(".congdoanbox"));
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                    }
                });
                $("#btnChonNguyenLieu").click(function () {
                    if (true) {
                        CreateModalWithSize("mdlDanhSachNguyenLieu", "99%", "90%", "tplDanhSachNguyenLieu", "Dannh sách nguyên liệu");
                        $("#gridDanhSachNguyenLieu").kendoGrid({
                            dataSource: new kendo.data.DataSource({
                                transport: {
                                    read: {
                                        url: "/DMThanhPham" + "/LayDanhSachNguyenLieuDeDinhNghia?SanPhamID=" + SanPhamID,
                                        dataType: "json",
                                        type: "GET",
                                        contentType: "application/json; charset=utf-8",
                                    },
                                },
                                schema: {
                                    data: "data",
                                    total: "total",
                                    model: {
                                        id: "NguyenLieuID",
                                        fields: {
                                            TenNguyenLieu: { type: "string", editable: false },
                                            MaNguyenLieu: { type: "string", editable: false },
                                            TenDonVi: { type: "string", editable: false },
                                            SoLuong: { type: "number", editable: true },
                                            LoaiHinh: { type: "number", editable: false },
                                        }
                                    }
                                },
                                change: function (e) {
                                    var data = this.data();
                                }
                            }),
                            filterable: {
                                mode: "row"
                            },
                            sortable: true,
                            resizable: true,
                            editable: true,
                            height: innerHeight * 0.75,
                            width: "100%",
                            dataBound: function (e) {
                                showslideimg10();
                            },
                            columns: [
                                {
                                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                                    template: "#= ++record #",
                                    width: 60,
                                    attributes: alignCenter
                                },
                                {
                                    field: "LoaiHinh", width: 100,
                                    title: "Loại Hình",
                                    attributes: { style: "text-align:left;" },
                                    filterable: {
                                        cell: {
                                            template: function (args) {
                                                // create a DropDownList of unique values (colors)
                                                args.element.kendoDropDownList({
                                                    dataSource: [{ id: 1, text: "Nguyên liệu" }, { id: 2, text: "Thành phẩm" }],
                                                    dataTextField: "text",
                                                    dataValueField: "id",
                                                    valuePrimitive: true
                                                });
                                            },
                                            showOperators: false
                                        }
                                    },
                                    template: kendo.template($("#tplLoaiHinh").html())
                                },
                                {
                                    field: "AnhDaiDien", title: "Ảnh",
                                    attributes: { style: "text-align:left;" },
                                    filterable: FilterInTextColumn,
                                    template: kendo.template($("#tplAnh").html()),
                                    width: 70
                                },
                                {
                                    field: "TenNguyenLieu",
                                    title: "Tên nguyên liệu",
                                    filterable: FilterInTextColumn,
                                    attributes: alignLeft,
                                    width: 200
                                },
                                {
                                    field: "MaNguyenLieu",
                                    title: "Mã nguyên liệu",
                                    filterable: FilterInTextColumn,
                                    attributes: alignLeft,
                                    width: 150
                                },
                                {
                                    field: "TenDonVi",
                                    title: GetTextLanguage("donvi"),
                                    width: 200,
                                    attributes: { "style": "text-align:left !important;" },
                                    filterable: FilterInTextColumn
                                },
                                {
                                    field: "SoLuong",
                                    width: 100,
                                    title: GetTextLanguage("soluong"),
                                    format: "{0:n2}",
                                    attributes: {
                                        "class": "soluongcheck",
                                        "style": "font-weight:bold"
                                    },
                                    filterable: FilterInColumn,
                                    editor: function (container, options) {
                                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                                        var input = $('<input type="text" data-v-min="0" style="width:75%;height:15px;" class="form-control" id="' + options.model.get("NguyenLieuID") + '" name="SoLuong" />');
                                        input.appendTo(container);
                                        input.autoNumeric(autoNumericOptionsSoLuong);
                                    },
                                },
                                {
                                    field: "TenLoaiVai", width: 100,
                                    title: "Loại vải",
                                    attributes: alignRight,
                                    filterable: FilterInColumn,
                                    editor: function (container, options) {
                                        var input = $('<input data-text-field="TenLoaiVai" data-value-field="LoaiVai"  id="LoaiVai" name="LoaiVai" />');
                                        input.appendTo(container);
                                        input.kendoDropDownList({
                                            dataTextField: "TenLoaiVai",
                                            dataValueField: "LoaiVai",
                                            filter: "contains",
                                            value: options.model.LoaiVai,
                                            dataSource: [{ LoaiVai: 1, TenLoaiVai: "Vải chính" },
                                            { LoaiVai: 2, TenLoaiVai: "Vải phối lót" },
                                            { LoaiVai: 3, TenLoaiVai: "Vải phối P1" },
                                            { LoaiVai: 4, TenLoaiVai: "Vải phối P2" },
                                            { LoaiVai: 5, TenLoaiVai: "Chỉ may" },
                                            { LoaiVai: 6, TenLoaiVai: "Mác" },
                                            { LoaiVai: 7, TenLoaiVai: "Cúc" },
                                            { LoaiVai: 8, TenLoaiVai: "PK khác" }],
                                            change: function () {
                                                var items = $("#gridDanhSachNguyenLieu").data("kendoGrid").dataSource.data();
                                                for (var i = 0; i < items.length; i++) {
                                                    if (items[i].NguyenLieuID === options.model.NguyenLieuID) {
                                                        items[i].TenLoaiVai = this.dataItem().TenLoaiVai;
                                                        items[i].LoaiVai = this.dataItem().LoaiVai;
                                                        $("#gridDanhSachNguyenLieu").data("kendoGrid").dataSource.data(items);
                                                        break;
                                                    }
                                                }
                                            }
                                        });
                                    },
                                },
                            ],

                        });
                        $("#btnDongY").click(function () {
                            var arrData = $("#gridDinhNghia").data("kendoGrid").dataSource.data();
                            var dataItem = $("#gridDanhSachNguyenLieu").data("kendoGrid").dataSource.data();
                            for (var i = 0; i < dataItem.length; i++) {
                                if (dataItem[i].SoLuong != null) {
                                    var isExsit = false;
                                    for (var j = 0; j < arrData.length; j++) {
                                        if (dataItem[i].NguyenLieuID == arrData[j].NguyenLieuID && dataItem[i].LoaiVai == arrData[j].LoaiVai) {
                                            isExsit = true;
                                            arrData[j].SoLuong = dataItem[i].SoLuong;
                                            arrData[j].LoaiVai = dataItem[i].LoaiVai;
                                            arrData[j].TenLoaiVai = dataItem[i].TenLoaiVai;
                                        }
                                    }
                                    if (!isExsit) {
                                        arrData.push(dataItem[i]);
                                    }
                                }
                            }
                            $("#mdlDanhSachNguyenLieu").data("kendoWindow").close();
                            $("#gridDinhNghia").data("kendoGrid").dataSource.data(arrData);
                        });
                        $("#btnHuyChonVatTu").click(function () {
                            $("#mdlDanhSachNguyenLieu").data("kendoWindow").close();
                        });
                    }
                });
                $("#btnXoa").click(function () {
                    var x = confirm("Bạn có chắc chắn muốn xóa danh mục này không?");
                    if (x) {
                        ContentWatingOP("mdlThemMoiThanhPham", 0.9);
                        $.ajax({
                            url: crudServiceBaseUrl + "/XoaDMThanhPham",
                            method: "POST",
                            data: { data: dataItems.ThanhPhamID },
                            success: function (data) {
                                showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                                $("#mdlThemMoiThanhPham").unblock();
                                $(".windows8").css("display", "none");
                                closeModel("mdlThemMoiThanhPham");
                                $(grid).data('kendoGrid').dataSource.read();
                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                $("#mdlThemMoiThanhPham").unblock();
                                $(".windows8").css("display", "none");
                                $(grid).data('kendoGrid').dataSource.read();
                            }
                        });
                    }
                })
            }
        });
    }
    function ThemThuocTinh() {
        $(".btn-themthuoctinh").click(function () {
            //CreateModalWithSize("mdlThemThuocTinh", 200, 200, "tplthemthuoctinh", "");
            $("#mdlThemThuocTinh").kendoWindow({
                title: "Thêm thuộc tính",
                modal: true,
                width: 600,
                height: 350,
                visible: false,
                resizable: false,
                draggable: false,
                actions: [
                    "Close"
                ],
                deactivate: function () {
                    $(this.element).empty();
                }
            });
            var kendoWindow = $("#mdlThemThuocTinh").data("kendoWindow");
            kendoWindow.content(kendo.template($("#tplthemthuoctinh").html()));
            kendoWindow.open();
            kendoWindow.center();

            $(".btn-luuthuoctinh").click(function () {
                var txt = "";
                txt += '<div class="form-group">';
                txt += '<label><span class="fa fa-remove" style="color:red"></span> ' + $("#mdlThemThuocTinh #TenThuocTinh").val() + '</label>';
                txt += '<input  type="text" class="form-control thuoctinhrieng ' + ($("#mdlThemThuocTinh .lieudulieu").val() == 1 ? "number" : "text") + '" data-id="" value="" style="box-sizing:border-box">';
                txt += '</div>';
                $("#ThongTinRieng .panel-body").append(txt);
                if ($("#mdlThemThuocTinh .lieudulieu").val() == 1) {
                    $(".number").autoNumeric(autoNumericOptionsMoney);
                }
                editthuoctinhfunction();
                kendoWindow.close();
            });
        });

    }
    function editthuoctinhfunction() {
        $(".fa-remove").click(function () {
            $(this).closest(".form-group").remove();

        });
    }
    function TaoTuDong(SanPhamID, YeuCauKhachHangID) {
        $("#TaoTuDong").click(function () {
            var check, noidung;
            $.ajax({
                url: crudServiceBaseUrl + "/CheckNguyenLieuTheoYeuCauKhachHang",
                method: "post",
                data: { SanPhamID: SanPhamID },
                success: function (data) {
                    if (data.data == null || data.data == "") {
                        showToast("warning", "Có lỗi do thiếu dữ liệu", "Nguyên nhân do: Yêu cầu khách hàng hoặc Thiết kế của sản phẩm này đã bị xóa!");
                    }
                    else if (data.code == "success") {
                        var dt = data.data[0];
                        check = dt.TrangThaiYeuCau;
                        noidung = "Yêu cầu khách hàng sau chưa đủ dữ liệu nguyên liệu! Hãy chọn nguyên liệu cho đơn hàng!";
                        if (check == 2) {
                            showToast("warning", "Có lỗi do thiếu dữ liệu", noidung);
                        }
                        if (check == 1) {
                            CreateModalWithSize("mdlDanhSachVatTu", "60%", "60%", "tplDanhSachVatTu", "Chọn nguyên liệu");
                            $("#gridDanhSachVatTu").kendoGrid({
                                dataSource: new kendo.data.DataSource({
                                    transport: {
                                        read: {
                                            url: crudServiceBaseUrl + "/LayDanhSachNguyenLieu",
                                            dataType: "json",
                                            type: "POST",
                                            data: { SanPhamID: SanPhamID },
                                            contentType: "application/json; charset=utf-8",
                                        },
                                        parameterMap: function (options, type) {
                                            return JSON.stringify(options);
                                        }
                                    },
                                    type: "json",
                                    aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
                                    sort: [{ field: "TenVatTu", dir: "asc" }],
                                    schema: {
                                        data: "data",
                                        total: "total",
                                        model: {
                                            id: "NguyenLieuID",
                                            fields: {
                                                NguyenLieuID: { editable: false },
                                                TenDonVi: { type: "string", editable: false },
                                                TenNguyenLieu: { type: "string", editable: false },
                                                MaNguyenLieu: { type: "string", editable: false },
                                                SoLuong: { type: "number", editable: true },
                                                SoLuongTon: { type: "number", editable: false },
                                                LoaiVai: { type: "string", editable: false },
                                                AnhDaiDien: { type: "string", editable: false },
                                            }
                                        }
                                    },
                                }),

                                filterable: {
                                    mode: "row"
                                },
                                editable: true,
                                sortable: true,
                                resizable: true,
                                pageable: pageableAll,
                                height: innerHeight * 0.5,
                                width: "100%",
                                columns: [
                                    {
                                        title: GetTextLanguage("stt"),
                                        template: "<span class='stt'></span>",
                                        width: 60,
                                        align: "center"
                                    },
                                    {
                                        field: "AnhDaiDien", title: "Ảnh",
                                        attributes: { style: "text-align:center;" },
                                        filterable: FilterInTextColumn,
                                        template: kendo.template($("#tplAnh").html()),
                                        width: 70
                                    },
                                    {
                                        field: "TenNguyenLieu", width: 100,
                                        title: "Tên nguyên liệu",
                                        attributes: { style: "text-align:left;" },
                                        filterable: FilterInTextColumn
                                    },
                                    {
                                        field: "MaNguyenLieu",
                                        width: 50,
                                        attributes: { style: "text-align:center;" },
                                        title: "Mã nguyên liệu",
                                        filterable: FilterInTextColumn
                                    },
                                    {
                                        field: "TenDonVi", width: 50,
                                        title: GetTextLanguage("donvi"),
                                        attributes: { "style": "text-align:center !important;" },
                                        filterable: FilterInTextColumn
                                    },
                                    {
                                        field: "SoLuong", width: 150,
                                        title: GetTextLanguage("soluong"),
                                        format: "{0:n2}",
                                        attributes: { class: "soluongcheck" },
                                        filterable: FilterInColumn,
                                        editor: function (container, options) {
                                            rs = 'data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                                            var input = $('<input type="text" data-v-min="0" class="form-control" style="width:83%;height:20px;" id="' + options.model.get("NguyenLieuID") + '" ' + rs + ' name="SoLuong" />');
                                            input.appendTo(container);
                                            input.autoNumeric(autoNumericOptionsSoLuong);
                                        },
                                    },
                                    {
                                        field: "LoaiVai", width: 100,
                                        title: "Loại vải",
                                        filterable: FilterInColumn,
                                        template: kendo.template($("#tplTenLoaiVai").html()),
                                    },
                                ],
                                dataBound: function (e) {
                                    showslideimg10();
                                    var rows = this.items();
                                    var dem = 0;
                                    $(rows).each(function () {
                                        dem++;
                                        var rowLabel = $(this).find(".stt");
                                        $(rowLabel).html(dem);
                                        if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                                            dem = 0;
                                        }
                                    });
                                },

                            });
                            $("#gridDanhSachVatTu").on("click", ".copy", function () {
                                var rows = $(this).closest("tr"),
                                    grids = $("#gridDanhSachVatTu").data("kendoGrid"),
                                    dataItems = grids.dataItem(rows);
                                if (!rows.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                                    var data = $("#gridDanhSachVatTu").data("kendoGrid").dataSource.data();
                                    data.push({
                                        NguyenLieuID: dataItems.NguyenLieuID,
                                        AnhDaiDien: dataItems.AnhDaiDien,
                                        MaNguyenLieu: dataItems.MaNguyenLieu,
                                        TenNguyenLieu: dataItems.TenNguyenLieu,
                                        TenDonVi: dataItems.TenDonVi,
                                        LoaiVai: dataItems.LoaiVai,
                                        TenLoaiVai: dataItems.TenLoaiVai,
                                    });
                                    $("#gridDanhSachVatTu").data("kendoGrid").dataSource.data(data);

                                }
                            });
                            $("#btnHuyChonVatTu").click(function () {
                                $("#mdlDanhSachVatTu").data("kendoWindow").close();
                            });
                            $("#btnTiepTuc").click(function () {
                                var dataItem = $("#gridDanhSachVatTu").data("kendoGrid").dataSource.data();
                                var lst = [];
                                var kiemtraton = true;
                                for (var i = 0; i < dataItem.length; i++) {
                                    if (dataItem[i].SoLuong > 0) {
                                        lst.push(dataItem[i]);
                                    }
                                }
                                if (confirm("Bạn có chắc chắn muốn thêm đoạn tự động? Nếu bạn thêm tự động những định nghĩa cũ sẽ bị xóa")) {
                                    ContentWatingOP("mdlThemMoiSanPham", 0.9);
                                    ContentWatingOP("mdlDanhSachVatTu", 0.9);
                                    $.ajax({
                                        url: crudServiceBaseUrl + "/TaoCongDoanTuDong",
                                        method: "post",
                                        data: { data: JSON.stringify(lst), YeuCauKhachHangID: YeuCauKhachHangID },
                                        success: function (data) {
                                            if (data.code == "success") {
                                                $("#mdlThemMoiSanPham").unblock();
                                                closeModel("mdlDanhSachVatTu");
                                                gridReload("grid", {});
                                                funcloadsp({ SanPhamID: SanPhamID });
                                                setTimeout(function () {
                                                    $(".tab_dinhghiacongdoan").click();
                                                }, 2000)
                                            } else {
                                                $("#mdlThemMoiSanPham").unblock();
                                                $("#mdlDanhSachVatTu").unblock();
                                            }
                                        },
                                        error: function (xhr, status, error) {
                                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                            $("#mdlThemMoiSanPham").unblock();
                                            $("#mdlDanhSachVatTu").unblock();
                                        }
                                    });
                                }

                            });
                        }
                    }
                },
                error: function (xhr, status, error) {
                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                }
            });
        });

    }
    function LoadThietKe() {
        $("#MauMayID").kendoDropDownList({
            autoBind: true,
            dataTextField: 'MaMauMay',
            dataValueField: 'MauMayID',
            filter: "contains",
            optionLabel: "Chọn bản thiết kế",
            dataSource: getDataDroplit(crudServiceBaseUrl, "/LayBanThietKe"),
            change: function () {
                var value = this.value();
                $.ajax({
                    url: crudServiceBaseUrl + "/LayThongTinThietKe",
                    method: "post",
                    data: { tkid: value },
                    success: function (data) {
                        var dt = data.data;
                        $("#TenSanPham").val(dt.TenSanPham);
                        $("#DonGia").val(dt.GiaBan);
                        $("#LoaiHinhSanXuat").data('kendoDropDownList').value(dt.LoaiHinhSanXuat);
                    }
                });
            }
        });
        $("#btnChonThietKe").click(function () {
            CreateModalWithSize("mdlThietKe", "100%", "90%", null, "Thiết kế");
            var dataDonHang = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: crudServiceBaseUrl + "/LayBanThietKe",
                        method: "GET",
                        data: {}
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                batch: true,
                pageSize: 50,
                schema: {
                    type: 'json',
                    data: 'data',
                    total: "total",
                    model: {
                        //id: "DonHangID",
                        fields: {
                            DonHangID: { editable: false, nullable: true },
                        }
                    }
                },
            });

            //Build grid
            var grid = $("#gridThietKe").kendoGrid({
                dataSource: dataDonHang,
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    buttonCount: 5,
                    pageSizes: true,
                    messages: messagegrid
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                height: window.innerHeight * 0.8,
                width: "100%",
                columns: [
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                        width: 60,
                        attributes: alignCenter,
                        template: "#= ++record #",
                    },
                    {
                        field: "ThoiGian",
                        title: "Thời gian",
                        width: 100,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                        template: formatToDate("ThoiGian"),
                    },
                    {
                        field: "MaMauMay",
                        title: "Mã thiết kế",
                        width: 100,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                    },
                    {
                        field: "TenMauMay",
                        title: "Tên thiết kế",
                        filterable: FilterInTextColumn,
                        attributes: { style: "text-align:left;" },
                        width: 300
                    },
                ],
            });
            $("#gridThietKe .k-grid-content").on("dblclick", "td", function () {
                var rows = $(this).closest("tr"),
                    grids = $("#gridThietKe").data("kendoGrid"),
                    dataItems = grids.dataItem(rows);
                $("#MauMayID").data("kendoDropDownList").value(dataItems.MauMayID);
                $("#MauMayID").data("kendoDropDownList").trigger("change");
                closeModel("mdlThietKe");

            })
        });
    }
    function InThongSo(SanPhamID) {
        $("#InThongSo").removeClass("hidden");
        $("#InThongSo").attr("href", crudServiceBaseUrl + "/InThongSo?SanPhamID=" + SanPhamID);
    }
    function loadimg7(input, div) {
        if (input.files) {
            var filesAmount = input.files.length;
            for (var i = 0; i < filesAmount; i++) {
                if (input.files[i].type == "image/png" || input.files[i].type == "image/jpg" || input.files[i].type == "image/jpeg" || input.files[i].type == "image/gif") {
                    readURL7(input.files[i], div);
                }
            }

        }
    }
    function readURL7(file, div) {

        var reader = new FileReader();
        reader.onload = function (event) {
            var html = "";
            html += "<div class='imgslider' style='float:left;position:relative;width:75px;height:90px;margin:3px;'>";
            html += "<div style='margin:0px;' class='img-con ahihi3 img-thumbnail'><a rel='gallery' href='" + event.target.result + "'><img src='" + event.target.result + "'  data-file='" + file.name + "' style='width: 70px;height:70px;margin: 0;opacity: 1;'></a></div>";
            html += "</div>";
            var daco = false;
            div.each(function (i, e) {
                if ($("img", e).attr("src") == event.target.result) {
                    daco = true;
                }

            });
            if (!daco) {
                div.append(html);
            }
        }
        reader.readAsDataURL(file);
    }
    //---------------------------------------------------------------------------------------------------------------
    $(".btnTemplate").on("click", function () {
        location.href = '/DMSanPham/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();
    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "50%",
                    height: innerHeight * 0.7,
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                var grid = $("#gridthem").kendoGrid({
                    dataSource: data,
                    editable: true,
                    height: innerHeight * 0.5,
                    width: "100%",
                    dataBound: function () {
                        var rows = this.items();
                        dem = 0;
                        $(rows).each(function () {
                            dem++;
                            var rowLabel = $(this).find(".stt");
                            $(rowLabel).html(dem);
                            if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                                dem = 0;
                            }
                        });
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 60,
                        },
                        {
                            field: "TenSanPham",
                            title: "Tên sản phẩm",
                            filterable: FilterInTextColumn,
                            attributes: alignCenter
                        },
                        {
                            field: "MaSanPham",
                            title: "Mã Sản Phẩm",
                            filterable: FilterInTextColumn,
                            attributes: alignCenter
                        },
                        {
                            field: "TenDonVi",
                            title: GetTextLanguage("donvitinh"),
                            filterable: FilterInTextColumn,
                            attributes: alignCenter
                        },
                        {
                            field: "DonGia",
                            title: "Đơn giá",
                            filterable: false,
                            attributes: alignRight,
                            format: "{0:n2}",
                        },
                        { command: "destroy", title: "&nbsp;", width: "150px" }
                    ],
                });
                $(".btn-import").click(function () {
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/DMSanPham/ImportDMSanPham",
                        method: "post",
                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()) },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#grid").data("kendoGrid").dataSource.read();
                                $("#grid").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    closeModel("mdlThemFileExcel");
                });
            }
        }
        objXhr3.open("POST", "/DMSanPham/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
    });
    //---------------------------------------------------------------------------------------------------------------
    function LoadNguyenLieu(YeuCauKhachHangID, SanPhamID) {
        var dssize = [];
        $.ajax({
            url: crudServiceBaseUrl + "/LayDanhSachNguyenLieuTheoYCKHDaLuu",
            method: "POST",
            data: { YeuCauKhachHangID: YeuCauKhachHangID },
            success: function (data) {
                if (data.data.length == 0) {
                    $.ajax({
                        url: crudServiceBaseUrl + "/LayDanhSachMauDaLuu",
                        method: "POST",
                        data: { YeuCauKhachHangID: YeuCauKhachHangID },
                        success: function (data) {
                            if (data.total > 0) {
                                var gridDSNL = $("#gridLuaChonNguyenLieu").data("kendoGrid");
                                var dt = data.data;
                                for (var i = 0; i < dt.length; i++) {
                                    gridDSNL.dataSource.add({
                                        SanPhamID: "", Mau: dt[i].Mau,
                                        VaiChinh: "", VaiChinhID: "",
                                        VaiPhoiLot: "", VaiPhoiLotID: "",
                                        VaiPhoiP1: "", VaiPhoiP1ID: "",
                                        VaiPhoiP2: "", VaiPhoiP2ID: "",
                                        ChiMay: "", ChiMayID: "",
                                        Mac: "", MacID: "",
                                        Cuc: "", CucID: "",
                                        PKKhac: "", PKKhacID: "",
                                        NguyenLieu: ""
                                    });
                                }
                            }
                        }
                    });
                }
                if (data.data.length > 0) {
                    var dt = data.data; var arr = [];
                    for (var i = 0; i < dt.length; i++) {
                        var arr1 = []; var arr2 = []; var arr3 = []; var arr4 = []; var arr5 = []; var arr6 = []; var arr7 = []; var arr8 = [];
                        for (var j = 0; j < dt[i].length; j++) {
                            if (dt[i][j].LoaiVai == 1) {
                                arr1.push({ TenNguyenLieu: dt[i][j].TenNguyenLieu, NguyenLieuID: dt[i][j].NguyenLieuID });
                            }
                            if (dt[i][j].LoaiVai == 2) {
                                arr2.push({ TenNguyenLieu: dt[i][j].TenNguyenLieu, NguyenLieuID: dt[i][j].NguyenLieuID });
                            }
                            if (dt[i][j].LoaiVai == 3) {
                                arr3.push({ TenNguyenLieu: dt[i][j].TenNguyenLieu, NguyenLieuID: dt[i][j].NguyenLieuID });
                            }
                            if (dt[i][j].LoaiVai == 4) {
                                arr4.push({ TenNguyenLieu: dt[i][j].TenNguyenLieu, NguyenLieuID: dt[i][j].NguyenLieuID });
                            }
                            if (dt[i][j].LoaiVai == 5) {
                                arr5.push({ TenNguyenLieu: dt[i][j].TenNguyenLieu, NguyenLieuID: dt[i][j].NguyenLieuID });
                            }
                            if (dt[i][j].LoaiVai == 6) {
                                arr6.push({ TenNguyenLieu: dt[i][j].TenNguyenLieu, NguyenLieuID: dt[i][j].NguyenLieuID });
                            }
                            if (dt[i][j].LoaiVai == 7) {
                                arr7.push({ TenNguyenLieu: dt[i][j].TenNguyenLieu, NguyenLieuID: dt[i][j].NguyenLieuID });
                            }
                            if (dt[i][j].LoaiVai == 8) {
                                arr8.push({ TenNguyenLieu: dt[i][j].TenNguyenLieu, NguyenLieuID: dt[i][j].NguyenLieuID });
                            }
                        }
                        arr.push({ Mau: dt[i][0].Mau, VaiChinhID: arr1, VaiPhoiLotID: arr2, VaiPhoiP1ID: arr3, VaiPhoiP2ID: arr4, ChiMayID: arr5, MacID: arr6, CucID: arr7, PKKhacID: arr8 });
                    }
                    for (var i = 0; i < arr.length; i++) {
                        var text1 = ""; text2 = ""; var text3 = ""; text4 = ""; var text5 = ""; text6 = ""; var text7 = ""; text8 = "";
                        for (var j = 0; j < arr[i].VaiChinhID.length; j++) {
                            var a = arr[i].VaiChinhID;
                            text1 += (a[j].TenNguyenLieu + ", ");
                        }
                        for (var j = 0; j < arr[i].VaiPhoiLotID.length; j++) {
                            var a = arr[i].VaiPhoiLotID;
                            text2 += (a[j].TenNguyenLieu + ", ");
                        }
                        for (var j = 0; j < arr[i].VaiPhoiP1ID.length; j++) {
                            var a = arr[i].VaiPhoiP1ID;
                            text3 += (a[j].TenNguyenLieu + ", ");
                        }
                        for (var j = 0; j < arr[i].VaiPhoiP2ID.length; j++) {
                            var a = arr[i].VaiPhoiP2ID;
                            text4 += (a[j].TenNguyenLieu + ", ");
                        }
                        for (var j = 0; j < arr[i].ChiMayID.length; j++) {
                            var a = arr[i].ChiMayID;
                            text5 += (a[j].TenNguyenLieu + ", ");
                        }
                        for (var j = 0; j < arr[i].MacID.length; j++) {
                            var a = arr[i].MacID;
                            text6 += (a[j].TenNguyenLieu + ", ");
                        }
                        for (var j = 0; j < arr[i].CucID.length; j++) {
                            var a = arr[i].CucID;
                            text7 += (a[j].TenNguyenLieu + ", ");
                        }
                        for (var j = 0; j < arr[i].PKKhacID.length; j++) {
                            var a = arr[i].PKKhacID;
                            text8 += (a[j].TenNguyenLieu + ", ");
                        }
                        gridDSNL.dataSource.insert({
                            SanPhamID: "", Mau: arr[i].Mau,
                            VaiChinh: text1, VaiChinhID: arr1,
                            VaiPhoiLot: text2, VaiPhoiLotID: arr2,
                            VaiPhoiP1: text3, VaiPhoiP1ID: arr3,
                            VaiPhoiP2: text4, VaiPhoiP2ID: arr4,
                            ChiMay: text5, ChiMayID: arr5,
                            Mac: text6, MacID: arr6,
                            Cuc: text7, CucID: arr7,
                            PKKhac: text8, PKKhacID: arr8,
                            NguyenLieu: ""
                        });
                    }
                }
            }
        });
        var size = dssize;
        $("#DSSize").click(function () {
            CreateModalWithSize("mdlChonSize", "45%", "20%", "tplChonSize", "Chọn danh sách size");
            $("#btnLuuDanhSachSize").click(function () {
                size = [];
                str = "";
                var flag = false;
                ContentWatingOP("mdlChonSize", 0.9);
                $("#DanhSachSize input").each(function (i, e) {
                    if ($(e).is(':checked')) {
                        flag = true;
                        size.push({ Size: $(e).val() });
                    }
                });
                if (flag && size.length > 0) {
                    for (var i = 0; i < size.length; i++) {
                        str += size[i].Size + ", ";
                    }
                    $("#DSSize").val(str);
                    $("#mdlChonSize").unblock();
                    closeModel("mdlChonSize");
                }
                else {
                    $("#mdlChonSize").unblock();
                    showToast("warning", "Chưa chọn số size", "Hãy chọn size cho tất cả các sản phẩm!");
                }
            });
        });
        $.ajax({
            url: crudServiceBaseUrl + "/TakeSize",
            method: "POST",
            data: { YeuCauKhachHangID: YeuCauKhachHangID },
            success: function (data) {
                var dt = data.data;
                var st = "";
                for (var i = 0; i < dt.length; i++) {
                    st += dt[i].Size + ", ";
                    dssize.push({ Size: dt[i].Size });
                }
                $("#DSSize").val(st);
            },
            error: function (xhr, status, error) {
            }
        });
        $("#gridLuaChonNguyenLieu").kendoGrid({
            dataSource: new kendo.data.DataSource(
                {
                    batch: true,
                    pageSize: 50,
                    sort: [{ field: "Mau", dir: "asc" }],
                    schema: {
                        data: 'data',
                        total: 'total',
                        model: {
                            id: "SanPhamID",
                            fields: {
                                SanPhamID: { editable: false },
                                Mau: { type: "string", editable: true },
                                VaiChinh: { type: "string", editable: true },
                                VaiChinhID: { type: "string", editable: false },
                                VaiPhoiLot: { type: "string", editable: true },
                                VaiPhoiLotID: { type: "string", editable: false },
                                VaiPhoiP1: { type: "string", editable: true },
                                VaiPhoiP1ID: { type: "string", editable: false },
                                VaiPhoiP2: { type: "string", editable: true },
                                VaiPhoiP2ID: { type: "string", editable: false },
                                ChiMay: { type: "string", editable: true },
                                ChiMayID: { type: "string", editable: false },
                                Mac: { type: "string", editable: true },
                                MacID: { type: "string", editable: false },
                                Cuc: { type: "string", editable: true },
                                CucID: { type: "string", editable: false },
                                PKKhac: { type: "string", editable: true },
                                PKKhacID: { type: "string", editable: false },
                                NguyenLieu: { type: "string", editable: false },

                            }
                        }
                    },
                }),
            height: window.innerHeight * 0.80,
            width: "100%",
            filterable: {
                mode: "row"
            },
            autoBind: false,
            editable: true,
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            columns: [
                {
                    field: "Mau", width: 150,
                    title: "Tên màu",
                    attributes: {
                        "class": "table-cell",
                        style: "font-size: 14px; height:53px; text-align:center;"
                    },
                    filterable: false,
                    editor: function (container, options) {
                        rs = 'required data-required-msg="Bắt buộc phải điền màu"'
                        var input = $('<input type="text" style="height:35px;width:85%;" class="form-control" ' + rs + ' name="Mau" onClick="this.select();"/>');
                        input.appendTo(container);
                    }
                },
                {
                    field: "VaiChinh", width: 300,
                    title: "Vải chính",
                    attributes: {
                        "class": "table-cell",
                        style: "font-size: 14px; height:53px; text-align:center;"
                    },
                    filterable: false,
                    editor: multiSelectEditor
                },
                {
                    field: "VaiPhoiLot", width: 300,
                    title: "Vải phối lót",
                    attributes: {
                        "class": "table-cell",
                        style: "font-size: 14px; height:53px; text-align:center;"
                    },
                    filterable: false,
                    editor: multiSelectEditor,
                },
                {
                    field: "VaiPhoiP1", width: 300,
                    title: "Vải phối P1",
                    attributes: {
                        "class": "table-cell",
                        style: "font-size: 14px; height:53px; text-align:center;"
                    },
                    filterable: false,
                    editor: multiSelectEditor,
                },
                {
                    field: "VaiPhoiP2", width: 300,
                    title: "Vải phối P2",
                    attributes: {
                        "class": "table-cell",
                        style: "font-size: 14px; height:53px; text-align:center;"
                    },
                    filterable: false,
                    editor: multiSelectEditor,
                },
                {
                    field: "ChiMay", width: 300,
                    title: "Chỉ may",
                    attributes: {
                        "class": "table-cell",
                        style: "font-size: 14px; height:53px; text-align:center;"
                    },
                    filterable: false,
                    editor: multiSelectEditor,
                },
                {
                    field: "Mac", width: 300,
                    title: "Mác",
                    attributes: {
                        "class": "table-cell",
                        style: "font-size: 14px; height:53px; text-align:center;"
                    },
                    filterable: false,
                    editor: multiSelectEditor,
                },
                {
                    field: "Cuc", width: 300,
                    title: "Cúc",
                    attributes: {
                        "class": "table-cell",
                        style: "font-size: 14px; height:53px; text-align:center;"
                    },
                    filterable: false,
                    editor: multiSelectEditor,
                },
                {
                    field: "PKKhac", width: 300,
                    title: "Phụ kiện khác",
                    attributes: {
                        "class": "table-cell",
                        style: "font-size: 14px; height:53px; text-align:center;"
                    },
                    filterable: false,
                    editor: multiSelectEditor,
                },
                {
                    width: 100,
                    attributes: {
                        "class": "table-cell",
                        style: "font-size: 14px; height:53px; text-align:center;"
                    },
                    filterable: false,
                    template: "<a type='button' class='btn btn-block btn-danger deleteRow'>Xóa</a>"
                },
            ],
        });
        var gridDSNL = $("#gridLuaChonNguyenLieu").data("kendoGrid");
        $("#btnThemDong").click(function () {
            gridDSNL.dataSource.add({});
        });
        $("#gridLuaChonNguyenLieu").on("click", ".deleteRow", function (e) {
            var gridds = $("#gridLuaChonNguyenLieu").data("kendoGrid");
            var tr = $(e.target).closest("tr");
            gridds.removeRow(tr);
        });
        $("#btnLuuDSNL").click(function () {
            var data = $("#gridLuaChonNguyenLieu").data("kendoGrid").dataSource.data();
            for (var i = 0; i < data.length; i++) {
                if (data[i].VaiChinhID == "") {
                    data[i].VaiChinhID = [];
                }
                if (data[i].VaiPhoiLotID == "") {
                    data[i].VaiPhoiLotID = [];
                }
                if (data[i].VaiPhoiP1ID == "") {
                    data[i].VaiPhoiP1ID = [];
                }
                if (data[i].VaiPhoiP2ID == "") {
                    data[i].VaiPhoiP2ID = [];
                }
                if (data[i].ChiMayID == "") {
                    data[i].ChiMayID = [];
                }
                if (data[i].MacID == "") {
                    data[i].MacID = [];
                }
                if (data[i].CucID == "") {
                    data[i].CucID = [];
                }
                if (data[i].PKKhacID == "") {
                    data[i].PKKhacID = [];
                }
            }
            //
            if (data.length > 0) {
                ContentWatingOP("tabstrip-2", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/LuuDanhSachNguyenLieuChoYeuCau",
                    method: "post",
                    data: { YeuCauKhachHangID: YeuCauKhachHangID, SanPhamID: SanPhamID, data: JSON.stringify(data), size: JSON.stringify(size) },
                    success: function (data) {
                        if (data.code == "success") {
                            $("#tabstrip-2").unblock();
                            gridReload("grid", {});
                        }
                        else {
                            $("#tabstrip-2").unblock();
                            gridReload("grid", {});
                        }
                    },
                    error: function (xhr, status, error) {
                        $("#tabstrip-2").unblock();
                    }
                });
            }
            else {
                showToast("warning", "Cảnh báo", "Bạn điền dữ liệu cho danh sách nguyên liệu!");
            }
        })
        //
    }
    var multiSelectEditor = function (container, options) {
        var multi = $('<select />')
            .appendTo(container)
            .kendoMultiSelect({
                dataTextField: "TenNguyenLieu",
                dataValueField: "NguyenLieuID",
                optionLabel: "Lựa chọn nguyên liệu cần thiết",
                filter: "contains",
                headerTemplate: '<div class="dropdown-header k-widget k-header"><span>Tên nguyên liệu</span></div>',
                suggest: true,
                autoClose: false,
                value: options.model[options.field + "ID"],
                dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/DanhSachTatCaNguyenLieu", {}),
                valuePrimitive: true,
                height: 400,
                change: function () {
                    var item = this.dataItems();
                    var name = options.field; var nameid = name + "ID";
                    var arrName = []; var arrID = []; var LoaiVai = 0;
                    if (name == "VaiChinh") { LoaiVai = 1 }
                    else if (name == "VaiPhoiLot") { LoaiVai = 2 }
                    else if (name == "VaiPhoiP1") { LoaiVai = 3 }
                    else if (name == "VaiPhoiP2") { LoaiVai = 4 }
                    else if (name == "ChiMay") { LoaiVai = 5 }
                    else if (name == "Mac") { LoaiVai = 6 }
                    else if (name == "Cuc") { LoaiVai = 7 }
                    else if (name == "PKKhac") { LoaiVai = 8 }
                    for (var i = 0; i < item.length; i++) {
                        arrName.push(item[i].TenNguyenLieu);
                        arrID.push({ TenNguyenLieu: item[i].TenNguyenLieu, NguyenLieuID: item[i].NguyenLieuID });
                    }
                    options.model[name] = arrName.join(",  ")
                    options.model[nameid] = arrID;
                }
            });
    };
});
