﻿$(document).ready(function () {
    var currentUrl = "/DMChucVu"
    $("#gridChucVu").kendoGrid({
        dataSource: {
            batch: true,
            transport: {
                read: {
                    url: currentUrl + "/GetDSChucVu",
                    type: "get",
                    dataType: "json"
                },
                create: {
                    url: currentUrl + "/AddOrUpdateChucVu",
                    type: "post",
                    dataType: "json",
                    complete: function (result) {
                        alertToastr(result.responseJSON);
                        $("#gridChucVu").data('kendoGrid').dataSource.read();
                    }
                },
                update: {
                    url: currentUrl + "/AddOrUpdateChucVu",
                    type: "post",
                    dataType: "json",
                    complete: function (result) {
                        alertToastr(result.responseJSON);
                        $("#gridChucVu").data('kendoGrid').dataSource.read();
                    }
                },
                destroy: {
                    url: currentUrl + "/XoaChucVu",
                    type: "post",
                    dataType: "json",
                    complete: function (result) {
                        alertToastr(result.responseJSON);
                        $("#gridChucVu").data('kendoGrid').dataSource.read();
                    }
                },
                parameterMap: function (data, type) {
                    if (type != "read") {
                        if (type == "create" || type == "update") {
                            var dataRequest = {
                                ChucVuID: data.models[0].ChucVuId,
                                TenChucVu: data.models[0].TenChucVu,
                                CapDo: data.models[0].CapDo
                            };
                            return dataRequest;
                        }
                        else if (type == "destroy") return { ChucVuID: data.models[0].ChucVuId };
                    }
                }
            },
            pageSize: pageSize,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "ChucVuId",
                    fields: {
                        ChucVuId: { editable: false, nullable: true },
                        TenChucVu: {
                            type: "string",
                            validation: {
                                required: true
                            }
                        },
                        CapDo: {
                            type: "int",
                            validation: {
                                required: true
                            }
                        }
                    }
                }
            }
        },
        edit: function (e) {
            e.container.find(".k-edit-label:first").hide();
            e.container.find(".k-edit-field:first").hide();
        },
        height: heightGrid,
        sortable: true,
        pageable: pageableAll,
        filterable: {
            mode: "row"
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        editable: {
            mode: "popup",
            window: {
                title: GetTextLanguage("thongtinchitiet"),
                width: "25%",
            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes",
        },
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        toolbar: [{
            template: kendo.template($("#btnThem").html())
        }],
        columns: [
            {
                field: "STT",
                title: GetTextLanguage("stt"),
                template: "#= ++record #",
                attributes: alignCenter,
                width: 60,
                filterable: false
            },
            {
                field: "TenChucVu",
                title: GetTextLanguage("chucvu"),
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            //{
            //    field: "CapDo",
            //    title: GetTextLanguage("capdo"),
            //    filterable: FilterInTextColumn,
            //    attributes: alignLeft,
            //},
            {
                width: 200,
                attributes: alignCenter,
                title: "<a class='k-link' href='javascript:void(0)'>" + GetTextLanguage("thaotac") + "</a>",
                command: [
                    {
                        name: "edit",
                        text: GetTextLanguage("sua")
                    },
                   {
                       name: "destroy",
                       text: GetTextLanguage("xoa")
                   },
                ]
            }
        ]
    });
    $(".btnTemplate").on("click", function () {
        location.href = '/DMChucVu/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();

    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "200px",
                    height: "100px",
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                $(".btn-import").click(function () {
                    var loaihinh = $(this).data("data-loai");
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/DMChucVu/ImportChucVu",
                        method: "post",
                        data: { url: data.url, LoaiHinh: loaihinh },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#gridChucVu").data("kendoGrid").dataSource.read();
                                $("#gridChucVu").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    $.ajax({
                        url: "/DMNhomTaiSan/DeleteFile",
                        method: "post",
                        data: { url: data.url },
                        success: function (data2) {
                            setTimeout(function () {
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
            }
        }
        objXhr3.open("POST", "/QuanLyTaiSan/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
        //////////////////////////////////////////////////////////////////
    });

});