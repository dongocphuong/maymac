﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/DMNhomVatTu";
    var LoaiHinhThietBi = 0;//0:của công ty - 1:đi thuê
    var actionThietBi = { add: 10, edit: 15 };

    var dataPhongBan = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/read",
                dataType: 'json'
            },
            create: {
                url: crudServiceBaseUrl + "/create",
                type: "post",
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            update: {
                url: crudServiceBaseUrl + "/update",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            destroy: {
                url: crudServiceBaseUrl + "/destroy",
                type: 'post',
                dataType: "json",
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            parameterMap: function (data, type) {
                if (type !== "read") {
                    if (type == "create") {
                        var model = {
                            TenNhom: data.models[0].TenNhom
                        }
                        return { data: kendo.stringify(model) }
                    }
                    else {
                        return { data: kendo.stringify(data.models[0]) }
                    }
                }
            }
        },
        batch: true,
        pageSize: 20,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "NhomVatTuID",
                fields: {
                    NhomVatTuID: { editable: false, nullable: true },
                    TenNhom: {
                        type: "string",
                        validation: {
                            required: GetTextLanguage("khongtrong")
                        }
                    }
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataPhongBan,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [
            {
                template: kendo.template($("#btnThem").html())
            }],
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        edit: function (e) {
            e.container.find(".k-edit-label:first").hide();
            e.container.find(".k-edit-field:first").hide();
        },
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
            {
                field: "TenNhom",
                title: GetTextLanguage("tennhom"),
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
            },
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                command: [
                    {
                        name: "edit",
                        text: GetTextLanguage("sua")
                    },
                    {
                        name: "destroy",
                        text: GetTextLanguage("xoa")
                    },
                ],
                width: 200,
                attributes: alignCenter
            }
        ],
        editable: editableInline
    });

    $(".btnTemplate").on("click", function () {
        location.href = '/DMNhomVatTu/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();

    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "200px",
                    height: "100px",
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                $(".btn-import").click(function () {
                    var loaihinh = $(this).data("data-loai");
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/DMNhomVatTu/ImportNhomVatTu",
                        method: "post",
                        data: { url: data.url, LoaiHinh: loaihinh },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#grid").data("kendoGrid").dataSource.read();
                                $("#grid").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    $.ajax({
                        url: "/DMNhomVatTu/DeleteFile",
                        method: "post",
                        data: { url: data.url },
                        success: function (data2) {
                            setTimeout(function () {
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
            }
        }
        objXhr3.open("POST", "/DMNhomVatTu/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
        //////////////////////////////////////////////////////////////////
    });

});