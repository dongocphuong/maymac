﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/NhomNguyenLieu";
    var dataGrid = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetAllData",
                dataType: "json",
                type: "GET",
                contentType: "application/json; charset=utf-8",
                data: {}
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "NhomNguyenLieuID",
                fields: {
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataGrid,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [{ template: kendo.template($("#btnThem").html()) }],
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
            {
                field: "TenNhomNguyenLieu",
                title: "Tên nhóm nguyên liệu",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
                width: 200
            },
        ],
    });
    //
    $("#grid").on("dblclick", "td", function () {
        var rows = $(this).closest("tr"),
            grids = $("#grid").data("kendoGrid"),
            dataItems = grids.dataItem(rows);
        if (!rows.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            var dataobjectload = null;
            CreateModalWithSize("mdlAddOrUpdate", "30%", "40%", "tplAddOrUpdate", "Chi tiết");
            $("#btnXoa").removeClass("hidden");
            $.ajax({
                url: crudServiceBaseUrl + "/GetAllDatabyNhomNguyenLieuID",
                method: "POST",
                data: { NhomNguyenLieuID: dataItems.NhomNguyenLieuID },
                success: function (data) {
                    dataobjectload = data.data;
                    var viewModel = kendo.observable({
                        NhomNguyenLieuID: dataobjectload == null ? "00000000-0000-0000-0000-000000000000" : dataobjectload.NhomNguyenLieuID,
                        TenNhomNguyenLieu: dataobjectload == null ? "" : dataobjectload.TenNhomNguyenLieu,
                    });
                    kendo.bind($("#frm"), viewModel);
                    btnLuu();
                },
                error: function (xhr, status, error) {
                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                }
            });
            $("#btnXoa").click(function () {
                var x = confirm("Bạn có chắc chắn muốn xóa danh mục này không?");
                if (x) {
                    ContentWatingOP("mdlAddOrUpdate", 0.9);
                    $.ajax({
                        url: crudServiceBaseUrl + "/DeleteNhomNguyenLieu",
                        method: "POST",
                        data: { NhomNguyenLieuID: dataItems.NhomNguyenLieuID },
                        success: function (data) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            $("#mdlAddOrUpdate").unblock();
                            $(".windows8").css("display", "none");
                            closeModel("mdlAddOrUpdate");
                            gridReload("grid", {});
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $("#mdlAddOrUpdate").unblock();
                            $(".windows8").css("display", "none");
                            gridReload("grid", {});
                        }
                    });
                }
            })
        }
    });
    $("#grid").on("click", "#btnAdd", function () {
        CreateModalWithSize("mdlAddOrUpdate", "30%", "40%", "tplAddOrUpdate", "Thêm mới nhóm nguyên liệu");
        btnLuu();
    })
    function btnLuu() {
        $("#btnLuu").click(function () {
            var validator = $("#frm").kendoValidator().data("kendoValidator");
            if (validator.validate()) {
                var ten = $("#TenNhomNguyenLieu").val();
                ten = ltrim(ten); ten = rtrim(ten); trimStr(ten);
                console.log(ten);
                var data = {
                    NhomNguyenLieuID: $("#NhomNguyenLieuID").val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#NhomNguyenLieuID").val(),
                    TenNhomNguyenLieu: ten
                }
                ContentWatingOP("mdlAddOrUpdate", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/AddOrUpdate",
                    method: "post",
                    data: { data: JSON.stringify(data) },
                    success: function (data) {
                        if (data.code == "success") {
                            showToast("success", "Thành công", "Thêm mới thành công");
                            $("#mdlAddOrUpdate").unblock();
                            closeModel("mdlAddOrUpdate");
                            gridReload("grid", {});
                        }
                        if (data.code == "error") {
                            showToast("warning", "Không thành công", data.message);
                            $("#mdlAddOrUpdate").unblock();
                        }
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlAddOrUpdate").unblock();
                        $(".windows8").css("display", "none");
                        gridReload("grid", {});
                    }
                });
            }
        });
    }
    function ltrim(str) {
        if (str == null) return str;
        return str.replace(/^\s+/g, '');
    }
    function trimStr(str) {
        if (str == null) return str;
        return str.replace(/^\s+|\s+$/g, '');
    }
    function rtrim(str) {
        if (str == null) return str;
        return str.replace(/\s+$/g, '');
    }
});
