﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/DMVatTu";
    var dataSourceGrid;
    var dataSourceCongTrinh;
    var tlb_drlCongTrinh;
    var tlb_drlLoaiHinh;
    var timeoutkeysearch = null;
    var mdlChooseForm;
    var mdlThietBi;
    var LoaiHinhThietBi = 0;//0:của công ty - 1:đi thuê
    var actionThietBi = { add: 10, edit: 15 };

    var dataPhongBan = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/read",
                type: "get",
                dataType: 'json'
            },
            create: {
                url: crudServiceBaseUrl + "/create",
                type: "post",
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            update: {
                url: crudServiceBaseUrl + "/update",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            destroy: {
                url: crudServiceBaseUrl + "/destroy",
                type: 'post',
                dataType: "json",
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            parameterMap: function (data, type) {
                if (type !== "read") {
                    if (type == "create") {
                        var model = {
                            DonViId: data.models[0].DonViId,
                            NhomVatTuID: data.models[0].NhomVatTuID,
                            TenVatTu: data.models[0].TenVatTu,
                            TenVietTat: data.models[0].TenVietTat,
                            DonGiaKhoan: data.models[0].DonGiaKhoan
                        }
                        return { data: kendo.stringify(model) }
                    }
                    else {
                        console.log(data.models[0]);
                        return { data: kendo.stringify(data.models[0]) }
                    }
                }
            }
        },
        batch: true,
        pageSize: 20,
        group: "TenNhom",
        sort: [{ field: "TenVatTu", dir: "asc" }],
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "VatTuID",
                fields: {
                    VatTuID: { editable: false, nullable: true },
                    DonGiaKhoan: {
                        type: "number",
                        validation: {
                            min: 0
                        }
                    },
                    DonViId: { type: "string" },
                    NhomVatTuID: { type: "string" },
                    TenVatTu: {
                        type: "string",
                        validation: {
                            required: true
                        }
                    },
                    TenVietTat: { type: "string" }
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataPhongBan,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [{ template: kendo.template($("#btnThem").html()) }],

        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
            {
                field: "TenVatTu",
                title: GetTextLanguage("tenvattu"),
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
            },
            {
                field: "TenVietTat",
                title: GetTextLanguage("tenviettat"),
                width: 200,
                attributes: alignCenter,
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
            },
            {
                field: "TenNhom",
                title: GetTextLanguage("nhom"),
                hidden: true
            },
            {
                field: "TenDonVi",
                title: GetTextLanguage("donvitinh"),
                width: 80,
                attributes: alignCenter,
                filterable: FilterInTextColumn
            },
            {
                field: "DonGiaKhoan",
                title: GetTextLanguage("dongiadutoan"),
                width: 100,
                format: "{0:n2}",
                attributes: alignRight,
                filterable: FilterInColumn
            },
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                command: [
                    {
                        name: "edit",
                        text: GetTextLanguage("sua")
                    },
                    {
                        name: "destroy",
                        text: GetTextLanguage("xoa")
                    },
                ],
                width: 200,
                attributes: alignCenter
            },
        ],
        editable: {
            mode: "popup",
            window: {
                title: GetTextLanguage("thongtinvattu"),
                width: 600
            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes",
            template: kendo.template($("#popup_editor").html())
        },
        edit: function (e) {
            $("#btnThemNhomVatTuNhanh").click(function () {
                CreateModalWithSize("mdlThemNhanhNhomVatTu", "25%", "20%", "tplThemNhanhNhomVatTu", "Thêm nhóm vật tư");
                $("#btnLuuNhanh").click(function () {
                    var validate = $("#TenNhom").kendoValidator().data("kendoValidator");
                    if (validate.validate()) {
                        ContentWatingOP("mdlThemNhanhNhomVatTu", 0.9);
                        $.ajax({
                            url: crudServiceBaseUrl + "/ThemNhanhNhomVatTu",
                            method: "post",
                            data: { data: $("#TenNhom").val() },
                            success: function (data) {
                                showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                                setTimeout(function () {
                                    $("#mdlThemNhanhNhomVatTu").unblock();
                                    $(".windows8").css("display", "none");
                                    closeModel("mdlThemNhanhNhomVatTu");
                                    $("#cbbNhom").data("kendoDropDownList").dataSource.read();
                                    $("#cbbNhom").data("kendoDropDownList").refresh();
                                }, 1000);
                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                $("#mdlThemNhanhNhomVatTu").unblock();
                                $(".windows8").css("display", "none");
                            }
                        });
                    }
                })
            })
            $("#btnThemDonViNhanh").click(function () {
                CreateModalWithSize("mdlThemNhanhDonVi", "25%", "20%", "tplThemNhanhDonVi", "Thêm đơn vị");
                $("#btnLuuNhanhDonVi").click(function () {
                    var validate = $("#TenDonVi").kendoValidator().data("kendoValidator");
                    if (validate.validate()) {
                        ContentWatingOP("mdlThemNhanhDonVi", 0.9);
                        $.ajax({
                            url: crudServiceBaseUrl + "/ThemNhanhDonVi",
                            method: "post",
                            data: { data: $("#TenDonVi").val() },
                            success: function (data) {
                                showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                                setTimeout(function () {
                                    $("#mdlThemNhanhDonVi").unblock();
                                    $(".windows8").css("display", "none");
                                    closeModel("mdlThemNhanhDonVi");
                                    $("#cbbDonVi").data("kendoDropDownList").dataSource.read();
                                    $("#cbbDonVi").data("kendoDropDownList").refresh();
                                }, 1000);
                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                $("#mdlThemNhanhDonVi").unblock();
                                $(".windows8").css("display", "none");
                            }
                        });
                    }
                })
            })
            $("#cbbNhom").kendoDropDownList({
                autoBind: false,
                dataTextField: 'TenNhom',
                dataValueField: 'NhomVatTuID',
                filter: "contains",
                text: e.model.TenNhom,
                value: e.model.NhomVatTuID,
                optionLabel: GetTextLanguage("chonnhomvattu"),
                dataSource: getDataDroplit("/DMNhomVatTu", "/read")

            });
            $("#cbbDonVi").kendoDropDownList({
                autoBind: false,
                dataTextField: 'TenDonVi',
                dataValueField: 'DonViId',
                filter: "contains",
                text: e.model.TenDonVi,
                value: e.model.DonViId,
                optionLabel: GetTextLanguage("chondonvi"),
                dataSource: getDataDroplit("/DMDonVi", "/read")
            });
            $("#DonGiaKhoan").autoNumeric(autoNumericOptionsSoLuong);
        }
    });
    $(".btnTemplate").on("click", function () {
        location.href = '/DMVatTu/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();

    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "50%",
                    height: innerHeight * 0.7,
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                var grid = $("#gridthem").kendoGrid({
                    dataSource: data,
                    editable: true,
                    height: innerHeight * 0.5,
                    width: "100%",
                    dataBound: function () {
                        var rows = this.items();
                        dem = 0;
                        $(rows).each(function () {
                            dem++;
                            var rowLabel = $(this).find(".stt");
                            $(rowLabel).html(dem);
                            if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                                dem = 0;
                            }
                        });
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 60,
                        },
                        {
                            field: "TenVatTu",
                            title: GetTextLanguage("tenvattu"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft
                        },
                        {
                            field: "TenVietTat",
                            title: GetTextLanguage("tenviettat"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft
                        },
                        {
                            field: "TenNhom",
                            title: GetTextLanguage("tennhom"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft
                        },
                        {
                            field: "TenDonVi",
                            title: GetTextLanguage("donvitinh"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft
                        },
                        { command: "destroy", title: "&nbsp;", width: "150px" }
                    ],

                });
                $(".btn-import").click(function () {
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/DMVatTu/ImportDMVatTu",
                        method: "post",
                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()) },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#grid").data("kendoGrid").dataSource.read();
                                $("#grid").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    closeModel("mdlThemFileExcel");
                });
            }
        }
        objXhr3.open("POST", "/DMVatTu/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
        //////////////////////////////////////////////////////////////////
    });
});