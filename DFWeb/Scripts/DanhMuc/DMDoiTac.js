﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/DMDoiTacDiThue";
    var dataSourceGrid;
    var dataSourceCongTrinh;
    var tlb_drlCongTrinh;
    var tlb_drlLoaiHinh;
    var timeoutkeysearch = null;
    var mdlChooseForm;
    var mdlThietBi;

    var LoaiHinhThietBi = 0;//0:của công ty - 1:đi thuê
    var actionThietBi = { add: 10, edit: 15 };


    //datafix
    var datafix_tinhtrang = [
        { text: GetTextLanguage("dangthicong"), val: 0 },
        { text: GetTextLanguage("dahoanthien"), val: 1 },
        { text: GetTextLanguage("tamngung"), val: 2 }
    ]
    var datafix_trangthai = [
            { text: GetTextLanguage("dungtiendo"), val: 0 },
            { text: GetTextLanguage("chamtiendo"), val: 1 },
            { text: GetTextLanguage("vuottiendo"), val: 2 }
    ]


    var dataPhongBan = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/read",
                type: "get",
                dataType: 'json'
            },
            create: {
                url: crudServiceBaseUrl + "/create",
                type: "post",
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid", {});
                }
            },
            update: {
                url: crudServiceBaseUrl + "/create",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid", {});
                }
            },
            destroy: {
                url: crudServiceBaseUrl + "/delDoiThiCong",
                type: 'post',
                dataType: "json",
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid", {});
                }
            },
            parameterMap: function (data, type) {
                if (type !== "read") {
                    if (type == "create") {
                        var model = {
                            TenDoi: data.models[0].TenDoi
                        }
                        return { data: kendo.stringify(model) }
                    }
                    else {
                        return { data: kendo.stringify(data.models[0]) }
                    }
                }
            }
        },
        batch: true,
        pageSize: 20,
        sort: [{ field: "TenDoi", dir: "asc" }],
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "DoiThiCongID",
                fields: {
                    DoiThiCongID: { editable: false, nullable: true },
                    TenDoi: { type: "string" }
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataPhongBan,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [
            { name: "create", text: GetTextLanguage("them") }
        ],
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "TenDoi", title: GetTextLanguage("tendoitac"), filterable: FilterInTextColumn,attributes: { style: "text-align:left;" }
            },
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                command: [{ name: "edit", text: GetTextLanguage("sua") }, { name: "destroy", text: GetTextLanguage("xoa") }], width: 200
            }

            //{ command: "destroy", title: "&nbsp;", width: 150 }
        ], 
        editable: {
            mode: "inline",
            window: {
                title: GetTextLanguage("thongtinchitiet")

            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes"
        }
    });


    //loaddata
    function getDataSourceGrid(url, param, groups) {
        var dataSources = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + url,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: param
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            batch: true,
            pageSize: 20,
            schema: {
                data: "data",
                total: "total"
            }
        });

        return dataSources;
    }
    function getDataDroplit(crudServiceBaseUrl, url) {
        var dataSourceObj;
        dataSourceObj = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + url,
                    dataType: "json",
                    type: "get"
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        });
        return dataSourceObj;
    }

    function getDataDroplitwithParam(crudServiceBaseUrl, url, param) {
        var dataSourceObj;
        dataSourceObj = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + url,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: param
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        });
        return dataSourceObj;
    }

    function getHeigtContent(grid) {
        if (grid == 1) { return $("#mdlChiTiet").height() - 80; } // tab thông tin
        else if (grid == 2) { //tab điều chuyển
            return $("#mdlChiTiet").height() - 101 - 37 - 96 - 60;

        }
        else {
            return $("#mdlChiTiet").height() - 101 - 37 - 96; //tab còn lại

        }
    }

    $(window).resize(function () {
        resizeContent();
    });

    function resizeContent() {
        $("#mdlfrmThietBi").css("height", getHeigtContent(1));
        $("#mdlLichSu .k-grid-content").css("height", getHeigtContent(3));
        $("#mdlDieuChuyen .k-grid-content").css("height", getHeigtContent(2));
        $("#mdlBaoCaoSuaChua .k-grid-content").css("height", getHeigtContent(3));
    }
    function showToast(loai, title, message) {
        toastr.options.positionClass = "toast-bottom-right";
        toastr[loai](message, title, { timeOut: 3000 });
    }

    function altReturn(data) {
        if (data.code == "success") {
            showToast("success", GetTextLanguage("thanhcong"), data.message);
        }
        else {
            showToast("error", GetTextLanguage("thatbai"), data.message);
        }
    }

    function gridReload(grid, parram) {
        $('#' + grid).data('kendoGrid').dataSource.read(parram);
        $('#' + grid).data('kendoGrid').refresh();

        //$('#grid').data('kendoGrid').dataSource.read({
        //    CongTrinhID: vCongTrinhID,
        //    LoaiHinh: vLoaiHinh
        //});
        //$('#grid').data('kendoGrid').refresh();
    }

    function closeModel(idform) {
        $('#' + idform).data("kendoWindow").close();
    }

});