﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/DMDoiThiCong";
    var dataSourceGrid;
    var dataSourceCongTrinh;
    var tlb_drlCongTrinh;
    var tlb_drlLoaiHinh;
    var timeoutkeysearch = null;
    var mdlChooseForm;
    var mdlThietBi;

    var LoaiHinhThietBi = 0;//0:của công ty - 1:đi thuê
    var actionThietBi = { add: 10, edit: 15 };


    //datafix
    var datafix_tinhtrang = [
        { text: GetTextLanguage("dangthicong"), val: 0 },
        { text: GetTextLanguage("dahoanthien"), val: 1 },
        { text: GetTextLanguage("tamngung"), val: 2 }
    ]
    var datafix_loaihinh = [
            { text: GetTextLanguage("doithicong"), val: 0 },
            { text: GetTextLanguage("phongban"), val: 4 }
    ]

    var dataPhongBan = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/read",
                type: "get",
                dataType: 'json'
            },
            create: {
                url: crudServiceBaseUrl + "/create",
                type: "post",
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid", {});
                }
            },
            update: {
                url: crudServiceBaseUrl + "/create",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid", {});
                }
            },
            destroy: {
                url: crudServiceBaseUrl + "/delDoiThiCong",
                type: 'post',
                dataType: "json",
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid", {});
                }
            },
            parameterMap: function (data, type) {
                if (type !== "read") {
                    if (type == "create") {
                        var model = {
                            TenDoi: data.models[0].TenDoi,
                            Type: $("#LoaiHinh").val()
                        }
                        return { data: kendo.stringify(model) }
                    }
                    else if (type == "update") {
                        var model = {
                            DoiThiCongID: data.models[0].DoiThiCongID,
                            TenDoi: data.models[0].TenDoi,
                            Type: $("#LoaiHinh").val()
                        }
                        return { data: kendo.stringify(model) }
                    }
                    else {
                        return { data: kendo.stringify(data.models[0]) }
                    }
                }
            }
        },
        batch: true,
        pageSize: 25,
        sort: [{ field: "TenDoi", dir: "asc" }],
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "DoiThiCongID",
                fields: {
                    DoiThiCongID: { editable: false, nullable: true },
                    TenDoi: { type: "string" },
                    Type: {type: "number"}
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataPhongBan,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [
          { template: kendo.template($("#btnThem").html()) }
        ],
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        edit: function (e) {
            e.container.find(".k-edit-label:first").hide();
            e.container.find(".k-edit-field:first").hide();
            $("#LoaiHinh").kendoDropDownList({
                dataTextField: 'text',
                dataValueField: 'val',
                filter: "contains",
                dataSource: datafix_loaihinh
            });

        },
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
            {
                field: "TenDoi", title: GetTextLanguage("tendoithicong"), filterable: FilterInTextColumn, attributes: { style: "text-align:left;" }
            },
            {
                field: "strLoaiHinh", title: GetTextLanguage("loaihinh"), filterable: FilterInTextColumn, attributes: { style: "text-align:center;" }, width: 120,
            },
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                command: [
                    { name: "edit", text: GetTextLanguage("sua") }, { name: "destroy", text: GetTextLanguage("xoa") },
                  { text: GetTextLanguage("xemnhanvien"), click: dieuchuyennhanvien, className: "fa fa-exchange" }], width: 350
            }

            //{ command: "destroy", title: "&nbsp;", width: 150 }
        ],
        editable: {
            mode: "popup",
            window: {
                title: GetTextLanguage("thongtinchitiet"),
                width: "25%",
            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes",
            template: kendo.template($("#popup_editor").html())
        },
        dataBound: function (e) {
            e.sender.tbody.find(".k-button.fa").each(function (idx, element) {
                $(element).removeClass("fa fa-exchange").find("span").addClass("fa fa-exchange");
            });
        }
    });

    function dieuchuyennhanvien(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        var mdl = $('#mdlNhanVienDoi').kendoWindow({
            width: "80%",
            height: "80%",
            title: GetTextLanguage("danhsachnhanvien"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#tempNhanVienDoi').html()));

        var gridNhanVienDoi = $("#gridNhanVienDoi").kendoGrid({
            dataSource: getDataSourceGrid("/LayDanhSachNhanVienTheoDoi", { DoiThiCongID: dataItem.get("DoiThiCongID") }, ""),
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            height: "99%",
            //toolbar: [
            //    { template: kendo.template($("#btnAddNhanVien").html()) }
            //],
            columns: [
                {
                    field: "DoiThiCongID", hidden: true
                },
                {
                    field: "NhanVienDoiThiCongId", hidden: true
                },
                {
                    field: "NhanVienID", hidden: true
                },
                {
                    field: "TenNhanVien", title: GetTextLanguage("tennhanvien"), width: 200, filterable: FilterInTextColumn, width: 200, attributes: { style: "text-align:left;" }
                },
                {
                    field: "SDT", title: GetTextLanguage("sdt"), filterable: FilterInTextColumn, width: 100, attributes: alignCenter
                },
                {
                    field: "NgaySinh", title: GetTextLanguage("ngaysinh"), filterable: FilterInTextColumn, width: 150, attributes: alignCenter
                },
                {
                    field: "CMT", title: GetTextLanguage("socmt"), filterable: FilterInTextColumn, width: 100, attributes: alignCenter
                },
                {
                    field: "DiaChi", title: GetTextLanguage("diachi"), width: 200, filterable: FilterInTextColumn, attributes: { style: "text-align:left;" }
                },
                {
                    field: "GioiTinh", title: GetTextLanguage("gioitinh"), filterable: FilterInColumn, width: 100, attributes: alignCenter
                },
                {
                    field: "ChucVuTen", title: GetTextLanguage("chucvu"), filterable: FilterInTextColumn, width: 100, attributes: { style: "text-align:left;" }
                },
                {
                   template: kendo.template($("#btnroidoi").html()), width: 100
                }
            ]
        });

        $("#btnAdd").click(function () {
            openDieuChuyenNhanVien(dataItem.get("DoiThiCongID"));
        });

        $("#gridNhanVienDoi").on("click", ".roidoi", function () {
            var x = confirm(GetTextLanguage("xoabangi"));
            if (x) {
                rowct = $(this).closest("tr"),
                gridct = $("#gridNhanVienDoi").data("kendoGrid"),
                dataItemct = gridct.dataItem(rowct);

                $.ajax({
                    cache: false,
                    async: false,
                    type: "POST",
                    url: crudServiceBaseUrl + "/RoiDoi",
                    data: { NhanVienDoiThiCongId: dataItemct.get("NhanVienDoiThiCongId"), NhanVienID: dataItemct.get("NhanVienID") },
                    success: function (data) {
                        altReturn(data);
                        gridReload("gridNhanVienDoi", { DoiThiCongID: dataItemct.get("DoiThiCongID") });
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                    }
                });
            } else {
                return false;
            }
        });

        $("#gridNhanVienDoi").on("click", ".updatechucvu", function () {
            var rowct = $(this).closest("tr"),
                gridct = $("#gridNhanVienDoi").data("kendoGrid"),
                dataItemct = gridct.dataItem(rowct);
            var mdl = $('#mdlUpdateChucVu').kendoWindow({
                width: 400,
                height: 200,
                title: GetTextLanguage("capnhatchucvu"),
                modal: true,
                visible: false,
                resizable: false,
                actions: [
                    "Close"
                ],
                deactivate: function () {
                    $(this.element).empty();
                }
            }).data("kendoWindow").center().open();
            mdl.content(kendo.template($('#tmplUpdateChucVu').html()));

            $("#chucvus").kendoDropDownList({
                filter: "contains",
                dataTextField: "TenChucVu",
                dataValueField: "ChucVuId",
                optionLabel: GetTextLanguage("chonchucvu"),
                dataSource: getDataDroplit(crudServiceBaseUrl, "/LayDanhSachChucVu")
            });

            var viewModelDoi = kendo.observable({
                chucvus: dataItemct.get("ChucVuID"),
                save: function (e) {
                    if (validator.validate()) {
                        $.ajax({
                            cache: false,
                            async: false,
                            type: "POST",
                            url: crudServiceBaseUrl + "/UpdateChucVu",
                            data: { NhanVienDoiThiCongID: dataItemct.get("NhanVienDoiThiCongId"), ChucVuID: $("#chucvus").val() },
                            success: function (data) {
                                altReturn(data);
                                gridReload("gridNhanVienDoi", { DoiThiCongID: dataItemct.get("DoiThiCongID") });
                                if (data.code == "success") {
                                    closeModel("mdlUpdateChucVu");
                                }
                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            }
                        });
                    }
                }
            });
            kendo.bind($("#frmaddDoi"), viewModelDoi);
            var validator = $("#frmaddDoi").kendoValidator().data("kendoValidator");
        });

    }

    function openDieuChuyenNhanVien(DoiThiCongId) {
        var mdl = $('#mdladdNhanVien').kendoWindow({
            width: 500,
            height: 250,
            title: GetTextLanguage("chonnhanvien"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#tmpladdNhanVien').html()));

        var html = $("#template").html();

        $("#customers").kendoDropDownList({
            dataTextField: "TenNhanVien",
            dataValueField: "NhanVienID",
            filter: "contains",
            autoBind: false,
            optionLabel: GetTextLanguage("chonnhanvien"),
            template: "<span class='k-state-default' style='background-image: url(\'../content/web/Customers/ac.jpg\')'></span><span class='k-state-default'><h3>#: TenNhanVien #</h3><p>#: NgaySinh # - #: DiaChi #</p></span>",
            dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/LayDanhSachNhanVienTheoDoi2", { DoiThiCongID: DoiThiCongId })
        });

        $("#chucvus").kendoDropDownList({
            filter: "contains",
            dataTextField: "TenChucVu",
            dataValueField: "ChucVuId",
            optionLabel: GetTextLanguage("chonchucvu"),
            dataSource: getDataDroplit(crudServiceBaseUrl, "/LayDanhSachChucVu")
        });

        var dropdownlist = $("#customers").data("kendoDropDownList");

        var viewModelDoi = kendo.observable({
            save: function (e) {
                if (validator.validate()) {
                    $.ajax({
                        cache: false,
                        async: false,
                        type: "POST",
                        url: crudServiceBaseUrl + "/DieuChuyenDoiVeCongTrinh",
                        data: { DoiThiCongID: DoiThiCongId, NhanVienID: $("#customers").val(), ChucVuId: $("#chucvus").val() },
                        success: function (data) {
                            altReturn(data);
                            gridReload("gridNhanVienDoi", { DoiThiCongID: DoiThiCongId });
                            if (data.code == "success") {
                                closeModel("mdladdNhanVien");
                            }
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        }
                    });
                }
            }
        });
        kendo.bind($("#frmaddDoi"), viewModelDoi);
        var validator = $("#frmaddDoi").kendoValidator().data("kendoValidator");
    }
    


    //loaddata
    function getDataSourceGrid(url, param, groups) {
        var dataSources = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + url,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: param
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            batch: true,
            pageSize: 20,
            schema: {
                data: "data",
                total: "total"
            }
        });

        return dataSources;
    }
    function getDataDroplit(crudServiceBaseUrl, url) {
        var dataSourceObj;
        dataSourceObj = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + url,
                    dataType: "json",
                    type: "get"
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        });
        return dataSourceObj;
    }

    function getDataDroplitwithParam(crudServiceBaseUrl, url, param) {
        var dataSourceObj;
        dataSourceObj = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + url,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: param
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        });
        return dataSourceObj;
    }

    function getHeigtContent(grid) {
        if (grid == 1) { return $("#mdlChiTiet").height() - 80; } // tab thông tin
        else if (grid == 2) { //tab điều chuyển
            return $("#mdlChiTiet").height() - 101 - 37 - 96 - 60;

        }
        else {
            return $("#mdlChiTiet").height() - 101 - 37 - 96; //tab còn lại

        }
    }

    $(window).resize(function () {
        resizeContent();
    });

    function resizeContent() {
        $("#mdlfrmThietBi").css("height", getHeigtContent(1));
        $("#mdlLichSu .k-grid-content").css("height", getHeigtContent(3));
        $("#mdlDieuChuyen .k-grid-content").css("height", getHeigtContent(2));
        $("#mdlBaoCaoSuaChua .k-grid-content").css("height", getHeigtContent(3));
    }
    function showToast(loai, title, message) {
        toastr.options.positionClass = "toast-bottom-right";
        toastr[loai](message, title, { timeOut: 3000 });
    }

    function altReturn(data) {
        if (data.code == "success") {
            showToast("success", GetTextLanguage("thanhcong"), data.message);
        }
        else {
            showToast("error", GetTextLanguage("thatbai"), data.message);
        }
    }

    function gridReload(grid, parram) {
        $('#' + grid).data('kendoGrid').dataSource.read(parram);
        $('#' + grid).data('kendoGrid').refresh();

        //$('#grid').data('kendoGrid').dataSource.read({
        //    CongTrinhID: vCongTrinhID,
        //    LoaiHinh: vLoaiHinh
        //});
        //$('#grid').data('kendoGrid').refresh();
    }

    function closeModel(idform) {
        $('#' + idform).data("kendoWindow").close();
    }
    $(".btnTemplate").on("click", function () {
        location.href = '/DMDoiThiCong/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();

    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "50%",
                    height: innerHeight * 0.7,
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                var grid = $("#gridthem").kendoGrid({
                    dataSource: data,
                    editable: true,
                    height: innerHeight * 0.5,
                    width: "100%",
                    dataBound: function () {
                        var rows = this.items();
                        dem = 0;
                        $(rows).each(function () {
                            dem++;
                            var rowLabel = $(this).find(".stt");
                            $(rowLabel).html(dem);
                            if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                                dem = 0;
                            }
                        });
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 60,
                        },
                        {
                            field: "TenDoi",
                            title: GetTextLanguage("tendoi"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft
                        },
                        { command: "destroy", title: "&nbsp;", width: "150px" }
                    ],

                });
                $(".btn-import").click(function () {
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/DMDoiThiCong/ImportDoiThiCong",
                        method: "post",
                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()) },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#grid").data("kendoGrid").dataSource.read();
                                $("#grid").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    closeModel("mdlThemFileExcel");
                });
            }
        }
        objXhr3.open("POST", "/DMDoiThiCong/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
        //////////////////////////////////////////////////////////////////
    });

});