﻿$(document).ready(function () {
    var datafix_loaihinh = [
        { text: GetTextLanguage("ccdc"), val: 1 },
        { text: GetTextLanguage("thietbi"), val: 2 },
        { text: GetTextLanguage("vattu"), val: 3 },
        { text: GetTextLanguage("vanchuyen"), val: 4 },
        { text: GetTextLanguage("taichinh"), val: 5 },
        { text: GetTextLanguage("chudautu"), val: 6 },
        { text: GetTextLanguage("khac"), val: 7 },
    ]
    var datasourceDMNhomDichVu = new kendo.data.DataSource({
            batch: true,
            transport: {
                read: {
                    url: "/DMNhomDichVu/read",
                    dataType: 'json',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                },
            },
            sort: [{ field: "TenNhomDichVu", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "DMNhomDichVuID",
                    fields: {
                        DMNhomDichVuID: { type: "string" },
                        LoaiHinh: { type: "number" },
                        TenNhomDichVu: { type: "string", validation: { required: true } },
                        IsActive: { type: "bool" },
                    },
                }
            },
            pageSize: 25,
        });
    var grid = $("#grid").kendoGrid({
        dataSource: datasourceDMNhomDichVu,
        height: window.innerHeight * 0.8,
        text: GetTextLanguage("danhmucnhomdichvu"),
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        toolbar: [
            {
                name: "create",
                text: GetTextLanguage("them")
            }],
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: false,
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        columns: [
            {
                title: "<a href='#' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                filterable: false,
                attributes: alignCenter
            },

            {
                field: "TenNhomDichVu",
                title: GetTextLanguage("nhomdichvu"),
                attributes: { style: "text-align:left;" },
                filterable: FilterInTextColumn

            },
            {
                field: "LoaiHinh",
                title: GetTextLanguage("loaihinh"),
                attributes: { style: "text-align:left;" },
                filterable: FilterInTextColumn,
                template: kendo.template($("#tempLoaiHinh").html())

            },
            {
                title: "<a href='#' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                
                width: 200,
                command: [{ name: "edit", text: GetTextLanguage("sua") }, ]
            }],
        editable: true,
        editable: {
            mode: "popup",
            window: {
                title: GetTextLanguage("thongtinvattu"),
                width: 600
            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes",
            template: kendo.template($("#popup_editor").html())
        },
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: [25, 50, 100],
            messages: messagegrid
        },
        edit: function (e) {
            $("#LoaiHinh").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "val",
                optionLabel: GetTextLanguage("chonloaihinh"),
                dataSource: datafix_loaihinh,
            });
        },
        save: function (e) {
            ContentWatingOP("content-edit", 0.9);
            var objectData = {};
            objectData.TenNhomDichVu = $("#TenNhomDichVu").val();
            objectData.DMNhomDichVuID = e.model.DMNhomDichVuID;
            objectData.LoaiHinh = $("#LoaiHinh").val();
            $.ajax({
                cache: false,
                async: false,
                type: "GET",
                url: "/DMNhomDichVu/update",
                data: { data: JSON.stringify(objectData) },
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    altReturn(data);
                    $("#grid").data("kendoGrid").dataSource.read();
                    $("#content-edit").unblock();
                },
                error: function (xhr, status, error) {
                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_laydulieuthatbai"));
                    $("#content-edit").unblock();
                }
            });
        }
    }).data("kendoGrid");
});