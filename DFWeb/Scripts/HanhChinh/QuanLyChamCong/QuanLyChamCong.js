﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/QuanLyChamCong";
    var count;
    //
    Init();
    $("#btnTimKiem").click(function () {
        $("#btnLuuDuLieuChamCong").removeClass("hidden");
        Main(count);
    })
    btnLuu();
    //
    function Init() {
        $("#inputThang").kendoDatePicker({
            value: new Date(),
            start: "year",// defines the start view
            depth: "year",// defines when the calendar should return date  
            format: "MMMM",// display month and year in the input
            dateInput: true,// specifies that DateInput is used for masking the input element
            max: new Date()
        });
        $("#inputNam").kendoDatePicker({
            value: new Date(),
            start: "decade",
            depth: "decade",
            format: "yyyy",
            max: new Date()
        });
    };
    function daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }
    function normalizeDate(input) {
        var parts = input.split('/');
        return (parts[0] < 10 ? '0' : '')
            + parseInt(parts[0]) + '/'
            + (parts[1] < 10 ? '0' : '')
            + parseInt(parts[1]) + '/'
            + parseInt(parts[2]);
    }
    function GetTongSoCong(tungay, denngay) {
        $.ajax({
            url: crudServiceBaseUrl + "/GetTongSoCongTheoThang",
            method: "POST",
            data: { TuNgay: tungay, DenNgay: denngay },
            success: function (data) {
                if (data.code == "success") {
                    var dt = data.data;
                    //
                    for (var i = 0; i < dt.length; i++) {
                        $("#" + dt[i].NhanVienID).val(dt[i].TongSoCong);
                    }
                }
            },
            error: function (xhr, status, error) {
                showToast("error", "Thất bại", "Đã xảy ra lỗi vui lòng thử lại!");
            }
        });
    }
    function btnLuu() {
        $("#btnLuuDuLieuChamCong").click(function () {
            var arrayChamCong = [];
            var datepickerMonth = $("#inputThang").data("kendoDatePicker");
            var datepickerYear = $("#inputNam").data("kendoDatePicker");
            var numberDaysInMonth = daysInMonth(datepickerMonth.value().getMonth() + 1, datepickerYear.value().getFullYear());
            var tungay = "1/" + (datepickerMonth.value().getMonth() + 1) + "/" + datepickerYear.value().getFullYear();
            var denngay = numberDaysInMonth + "/" + (datepickerMonth.value().getMonth() + 1) + "/" + datepickerYear.value().getFullYear();
            var count = $('#tblBangChamCong #tblInputInfo tr td .inputCong').length;
            var x = confirm("Bạn có chắc chắn muốn lưu bảng chấm công này không?");
            if (x) {
                var numberOfRows = document.getElementById("tblBangChamCong").rows.length;
                var numberOfColumns = document.getElementById("tblBangChamCong").rows[0].cells.length;
                var c = count / (numberOfRows - 1);
                var index = 1;
                for (var i = 1; i < numberOfRows; i++) {
                    for (var j = 0; j < c; j++) {

                        var x = document.getElementById("tblBangChamCong").rows[i].cells[1].innerHTML;
                        //
                        var y = $("#input" + index).autoNumeric("get");
                        index++;
                        //
                        var z = (j + 1) + "/" + (datepickerMonth.value().getMonth() + 1) + "/" + datepickerYear.value().getFullYear();
                        //
                        arrayChamCong.push({ NhanVienID: x, SoCong: y, ThoiGian: z });
                    }
                }
                ContentWatingOP("mdlBangChamCong", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/Submit",
                    method: "POST",
                    data: { data: JSON.stringify(arrayChamCong), TuNgay: tungay, DenNgay: denngay },
                    success: function (data) {
                        if (data.code == "success") {
                            $("#mdlBangChamCong").unblock();
                            GetTongSoCong(tungay, denngay);
                            showToast(data.code, "Thành công", data.message);
                        }
                    },
                    error: function (xhr, status, error) {
                        showToast("error", "Thất bại", "Đã xảy ra lỗi vui lòng thử lại!");
                        $("#mdlBangChamCong").unblock();
                    }
                });
            }
        });
    }
    function Main() {
        var datepickerMonth = $("#inputThang").data("kendoDatePicker");
        var datepickerYear = $("#inputNam").data("kendoDatePicker");
        var numberDaysInMonth = daysInMonth(datepickerMonth.value().getMonth() + 1, datepickerYear.value().getFullYear());
        //
        var htmlPrintDayMonth = "";
        htmlPrintDayMonth += "<tr style='background: #4682B4; color: #fff;'>";
        htmlPrintDayMonth += "<th class='text-bold text-center'>#</th>";
        htmlPrintDayMonth += "<th class='text-bold text-center'>Tên nhân viên</th>";
        htmlPrintDayMonth += "<th class='text-bold text-center'>Phòng ban</th>";
        htmlPrintDayMonth += "<th class='text-bold text-center'>Chức vụ</th>";
        htmlPrintDayMonth += "<th nowrap class='text-bold text-center'>Tổng công</th>";
        for (var i = 1; i <= numberDaysInMonth; i++) {
            htmlPrintDayMonth += "<th class='text-bold text-center'>" + i + "/" + (datepickerMonth.value().getMonth() + 1) + "</th>";
        }
        htmlPrintDayMonth += "</tr>";
        $("#tblInputNgayThang").html(htmlPrintDayMonth);
        //
        count = 1;
        var htmlPrintInfo = "";
        $.ajax({
            url: crudServiceBaseUrl + "/GetAllDataNhanVien",
            method: "POST",
            success: function (data) {
                if (data.code == "success") {
                    var dt = data.data;
                    //
                    for (var j = 0; j < dt.length; j++) {
                        htmlPrintInfo += "<tr>";
                        htmlPrintInfo += "<td>" + (j + 1) + "</td>";
                        htmlPrintInfo += "<td class='hidden'>" + dt[j].NhanVienID + "</td>";
                        htmlPrintInfo += "<td nowrap class='text-bold' style='padding: 10px !important;'>" + dt[j].TenNhanVien + "</td>";
                        htmlPrintInfo += "<td nowrap class='text-bold' style='padding: 10px !important;'>" + dt[j].TenPhong + "</td>";
                        htmlPrintInfo += "<td nowrap class='text-bold' style='padding: 10px !important;'>" + dt[j].TenChucVu + "</td>";
                        htmlPrintInfo += "<td><input value='0' type='text' class='form-control text-center' id='" + dt[j].NhanVienID + "' style='color:red !important;' disabled></td>";
                        for (var i = 0; i < numberDaysInMonth; i++) {
                            var today = (new Date()).getDate();
                            var monthoftoday = (new Date()).getMonth() + 1;
                            if (monthoftoday == (datepickerMonth.value().getMonth() + 1)) {
                                if (i <= (today - 1)) {
                                    var time = normalizeDate((i + 1) + "/" + (datepickerMonth.value().getMonth() + 1) + "/" + datepickerYear.value().getFullYear());
                                    htmlPrintInfo += "<td><input onClick='this.select();' value='0' type='text' class='form-control text-center inputCong " + time + "--" + dt[j].NhanVienID + "' id='input" + count + "'></td>";
                                    count++;
                                }
                                else {
                                    htmlPrintInfo += "<td style='background-color: darkgray;'></td>";
                                }
                            }
                            else {
                                var time = normalizeDate((i + 1) + "/" + (datepickerMonth.value().getMonth() + 1) + "/" + datepickerYear.value().getFullYear());
                                htmlPrintInfo += "<td><input onClick='this.select();' value='0' type='text' class='form-control text-center inputCong " + time + "--" + dt[j].NhanVienID + "' id='input" + count + "'></td>";
                                count++;
                            }
                        }
                        htmlPrintInfo += "</tr>"
                    }
                    $("#tblInputInfo").html(htmlPrintInfo);
                    $(".inputCong").autoNumeric({
                        digitGroupSeparator: ',',
                        decimalCharacter: '.',
                        minimumValue: '0', maximumValue: '1', emptyInputBehavior: 'zero'
                    });
                    var tungay = "1/" + (datepickerMonth.value().getMonth() + 1) + "/" + datepickerYear.value().getFullYear();
                    var denngay = numberDaysInMonth + "/" + (datepickerMonth.value().getMonth() + 1) + "/" + datepickerYear.value().getFullYear();
                    GetTongSoCong(tungay, denngay);
                    $.ajax({
                        url: crudServiceBaseUrl + "/GetAllDataBangChamCong",
                        method: "POST",
                        data: { TuNgay: tungay, DenNgay: denngay },
                        success: function (data) {
                            if (data.code == "success") {
                                var dt = data.data;
                                //
                                for (var i = 0; i < dt.length; i++) {
                                    for (var j = 0; j < (count - 1); j++) {
                                        var cl = (dt[i].ThoiGian + "--" + dt[i].NhanVienID).toString();
                                        var id = "#input" + (j + 1);
                                        if (($(id).hasClass(cl))) {
                                            $(id).val(dt[i].SoCong);
                                        }
                                    }
                                }
                            }
                        },
                        error: function (xhr, status, error) {
                            showToast("error", "Thất bại", "Đã xảy ra lỗi vui lòng thử lại!");
                        }
                    });
                }
            },
            error: function (xhr, status, error) {
                showToast("error", "Thất bại", "Đã xảy ra lỗi vui lòng thử lại!");
            }
        });
    }
});