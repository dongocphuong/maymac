﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/ChamCongSanPham";
    //
    $("#inputThoiGian").kendoDatePicker({
        value: new Date(),
        max: new Date(),
        change: function () {
            gridReload("grid", { ThoiGian: $("#inputThoiGian").val() });
        }
    });
    var datagrid = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetAllData",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: { ThoiGian: $("#inputThoiGian").val() }
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        sort: [{ field: "TenPhong", dir: "asc" }],
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "ChamCongSanPhamID",
                fields: {
                    TenNhanVien: { editable: false },
                    MaSanPham: { editable: true },
                    TenPhong: { editable: false },
                    ThanhTien: { type: "number", editable: false },
                    DonGia: { type: "number", editable: true },
                    SoLuong: { type: "number", editable: true },
                    KhoangThoiGian: { type: "number", editable: true },
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: datagrid,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
            {
                field: "TenNhanVien",
                title: "Tên nhân viên [1]",
                width: 100,
                attributes: alignLeft,
                filterable: FilterInTextColumn,
            },
            {
                field: "TenPhong",
                title: "Tên phòng ban",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 150
            },
            {
                field: "MaSanPham",
                title: "Mã sản phẩm [2]",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 100
            },
            {
                field: "SoLuong",
                title: "Số lượng [3]",
                filterable: FilterInTextColumn,
                attributes: alignRight,
                width: 100,
                editor: function (container, options) {
                    var input = $('<input type="text" class="form-control" name="SoLuong" id="' + options.model.get("NhanVienID") + '" style="width:80% !important; height: auto !important;"/>');
                    input.appendTo(container);
                    input.autoNumeric({
                        digitGroupSeparator: ',',
                        decimalCharacter: '.',
                        minimumValue: '0.00',
                        maximumValue: '9999999.00',
                        emptyInputBehavior: 'zero'
                    });
                },
                format: "{0:n2}",
            },
            {
                field: "KhoangThoiGian",
                title: "Thời gian làm (h) [4]",
                filterable: FilterInTextColumn,
                attributes: alignRight,
                width: 100,
                editor: function (container, options) {
                    var input = $('<input type="text" class="form-control" name="KhoangThoiGian" id="' + options.model.get("NhanVienID") + '" style="width:80% !important; height: auto !important;"/>');
                    input.appendTo(container);
                    input.autoNumeric({
                        digitGroupSeparator: ',',
                        decimalCharacter: '.',
                        minimumValue: '0.00',
                        maximumValue: '9999999.00',
                        emptyInputBehavior: 'zero'
                    });
                },
                format: "{0:n2}",
            },
            {
                field: "DonGia",
                title: "Đơn giá [5]",
                filterable: FilterInTextColumn,
                attributes: alignRight,
                width: 100,
                editor: function (container, options) {
                    var input = $('<input type="text" class="form-control" name="DonGia" id="' + options.model.get("NhanVienID") + '" style="width:80% !important; height: auto !important;"/>');
                    input.appendTo(container);
                    input.autoNumeric({
                        digitGroupSeparator: ',',
                        decimalCharacter: '.',
                        minimumValue: '0.00',
                        maximumValue: '9999999.00',
                        emptyInputBehavior: 'zero'
                    });
                },
                format: "{0:n2}",
            },
            {
                field: "ThanhTien",
                title: "Thành tiền [6]",
                filterable: FilterInTextColumn,
                attributes: alignRight,
                width: 100,
                format: "{0:n2}",
            },
        ],
        editable: true,
        save: function (e) {
            var items = $("#grid").data("kendoGrid").dataSource.data();
            if (e.values.DonGia != null) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i].NhanVienID == e.model.NhanVienID) {
                        items[i].ThanhTien = (e.values.DonGia == null ? 0 : e.values.DonGia) * (e.model.SoLuong == null ? 0 : e.model.SoLuong) * (e.model.KhoangThoiGian == null ? 0 : e.model.KhoangThoiGian);
                        items[i].DonGia = e.values.DonGia;
                        break;
                    }
                }
            }
            if (e.values.SoLuong != null) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i].NhanVienID == e.model.NhanVienID) {
                        items[i].ThanhTien = (e.model.DonGia == null ? 0 : e.model.DonGia) * (e.values.SoLuong == null ? 0 : e.values.SoLuong) * (e.model.KhoangThoiGian == null ? 0 : e.model.KhoangThoiGian);
                        items[i].SoLuong = e.values.SoLuong;
                        break;
                    }
                }
            }
            if (e.values.KhoangThoiGian != null) {
                for (var i = 0; i < items.length; i++) {
                    if (items[i].NhanVienID == e.model.NhanVienID) {
                        items[i].ThanhTien = (e.model.DonGia == null ? 0 : e.model.DonGia) * (e.model.SoLuong == null ? 0 : e.model.SoLuong) * (e.values.KhoangThoiGian == null ? 0 : e.values.KhoangThoiGian);
                        items[i].KhoangThoiGian = e.values.KhoangThoiGian;
                        break;
                    }
                }
            }
            $("#grid").data("kendoGrid").dataSource.data(items);
        },
    });
    $("#btnLuuDuLieuChamCong").click(function () {
        var dataGrid = $("#grid").data("kendoGrid").dataSource.data();
        var x = confirm("Bạn có chắc chắn muốn lưu bảng chấm công này không?");
        if (x) {
            ContentWatingOP("grid", 0.9);
            $.ajax({
                url: crudServiceBaseUrl + "/AddOrUpdate",
                method: "POST",
                data: { data: JSON.stringify(dataGrid), ThoiGian: $("#inputThoiGian").val() },
                success: function (data) {
                    $("#grid").unblock();
                    gridReload("grid", { ThoiGian: $("#inputThoiGian").val() });
                    showToast("success", "Thành công", "Đã lưu dữ liệu thành công!");
                },
                error: function (xhr, status, error) {
                    $("#grid").unblock();
                    gridReload("grid", { ThoiGian: $("#inputThoiGian").val() });
                    showToast("error", "Thất bại", "Có lỗi khi lưu dữ liệu!");
                }
            });
        }
    });
});