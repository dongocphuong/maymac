﻿$(document).ready(function () {
    var selectedOrders = [];
    var DSNVSelected = [];
    var idField = "NhanVienID";
    var datasourceCT = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QLNhanVien/LayDanhSachNhanVien_New",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {
                    LoaiHinh: 1,
                    TrangThai: 1// lấy tất cả Dự Án
                }
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        sort: [{ field: "TenDoi", dir: "asc" }, { field: "TenNhanVien", dir: "asc" }],
        schema: {
            model: {
                id: "NhanVienID",
                fields: {
                    NhanVienID: { type: "string" }
                }
            },
            data: "data",
            total: "total",
        }
    });
    $("#grid_DSNhanVien").kendoGrid({
        width: "100%",
        height: heightGrid * 0.85,
        dataSource: datasourceCT,
        selectable: "multiple",
        navigatable: true,
        filterable: {
            mode: "row"
        },
        sortable: true,
        pageable:
            {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
        dataBinding: function () {
            record = 0;
        },
        columns: [
           {
               title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
               template: "#= ++record #",
               width: 60,
               attributes: alignCenter
           },
            {
                field: "TenNhanVien",
                title: GetTextLanguage("tennhanvien"),
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            {
                field: "TenPhong",
                title: GetTextLanguage("bophan"),
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            {
                field: "TenChucVu",
                title: GetTextLanguage("chucvu"),
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
          
            {
                field: "TinhTrang",
                title: GetTextLanguage("tinhtrang"),
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                template: kendo.template($("#tplTinhTrang").html())
            },
           
            {
                field: "GioiTinh",
                title: GetTextLanguage("gioitinh"),
                width: 100,
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                template: kendo.template($("#tplGioiTinh").html())
            },
            {
                field: "NgaySinh",
                title: GetTextLanguage("ngaysinh"),
                width: 150,
                filterable: FilterInTextColumn,
                template: "#= kendo.toString(kendo.parseDate(NgaySinh, 'yyyy-MM-dd'), 'dd/MM/yyyy') #"
            },
            {
                field: "CMT",
                title: GetTextLanguage("socmt"),
                width: 100,
                filterable: FilterInTextColumn
            },
            {
                field: "SDT",
                title: GetTextLanguage("sdt"),
                width: 100,
                filterable: FilterInColumn,
            },
            {
                field: "UserName",
                title: GetTextLanguage("tentaikhoan"),
                width: 100,
                filterable: FilterInTextColumn,
                attributes: alignLeft
            },
        ],
        change: function (e, args) {
            var grid = e.sender;
            var items = grid.items();
            items.each(function (idx, row) {
                var idValue = grid.dataItem(row).get(idField);
                var ChucVu = grid.dataItem(row).get("TenChucVu");
                var TenDoi = grid.dataItem(row).get("TenDoi");
                var TenNhanVien = grid.dataItem(row).get("TenNhanVien");
                if (row.className.indexOf("k-state-selected") >= 0) {
                    var valid = true;
                    for (var i = 0; i < DSNVSelected.length; i++) {
                        if (DSNVSelected[i].NhanVienID == idValue) {
                            valid = false;
                            break;
                        }
                    }
                    if (valid) {
                        console.log("s");
                        DSNVSelected.push({ NhanVienID: idValue, ChucVu: ChucVu, TenDoi: TenDoi, TenNhanVien: TenNhanVien });
                    }
                    selectedOrders[idValue] = true;
                } else if (selectedOrders[idValue]) {
                    for (var i = 0; i < DSNVSelected.length; i++) {
                        if (DSNVSelected[i].NhanVienID == idValue) {
                            DSNVSelected.splice(i, 1);
                            break;
                        }
                    }
                    delete selectedOrders[idValue];
                }
            });
        },
        dataBound: function (e) {
            var grid = this;
            grid.tbody.find("tr").dblclick(function (e) {
                var dataItem = grid.dataItem(this);
                $("#role_update").val(1);
                $(".content").hide();
                LoadingContent("ChiTietNhanVien", "/QLNhanVien/ViewChung?NhanVienID=" + dataItem.get("NhanVienID"), dataItem.get("TenNhanVien"));
            });

            var gridsend = e.sender;
            var items = gridsend.items();
            var itemsToSelect = [];
            items.each(function (idx, row) {
                var dataItem = gridsend.dataItem(row);
                if (selectedOrders[dataItem[idField]]) {
                    itemsToSelect.push(row);
                }
            });
            e.sender.select(itemsToSelect);
        }
    });



    $(".btAdd").on("click", function () {
        $(".content").hide();
        LoadingContent("ChiTietNhanVien", "/QLNhanVien/AddNhanVien_Partial");
    });
    $(".btnTemplate").on("click", function () {
        location.href = '/QLNhanVien/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();

    });
    $(".btnDieuChuyen").on("click", function () {
        var obj = {};
        obj.NhanVien = DSNVSelected;

        if (DSNVSelected.length <= 0) {
            showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("chonnhanvien"));
        } else {
            $.ajax({
                url: "/QLNhanVien/DieuChuyenMultipleNV_View",
                dataType: 'html',
                beforeSend: function () {
                    WatingLoad(1);
                },
                method: "POST",
                data: {
                    data: JSON.stringify(obj)
                },
                success: function (data) {
                    window.setTimeout(
                        function () {
                            $("#content-loader").unblock();
                            $("#ChiTietNhanVien").html(data);
                            $(".title-function").html(GetTextLanguage("dieuchuyennhanvien"));
                        }, 1000);
                    $(".NV_DS").hide();
                },
                error: function () {
                    isLoadContent = true;
                    $("#content-loader").unblock();
                    showToast("error", "title", "message");
                },
                complete: function () {
                    isLoadContent = true;
                    window.setTimeout(function () { $("#content-loader").unblock(); }, 1000);
                },
            });
        }

        console.log(DSNVSelected);
    });

    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "90%",
                    height: innerHeight * 0.8,
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                var grid = $("#gridthem").kendoGrid({
                    dataSource: data,
                    editable: true,
                    height: innerHeight * 0.6,
                    width: "100%",
                    dataBound: function () {
                        var rows = this.items();
                        dem = 0;
                        $(rows).each(function () {
                            dem++;
                            var rowLabel = $(this).find(".stt");
                            $(rowLabel).html(dem);
                            if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                                dem = 0;
                            }
                        });
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 60,
                        },
                        {
                            field: "TenNhanVien",
                            title: GetTextLanguage("tennhanvien"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: "250px"
                        },
                        {
                            field: "GioiTinh",
                            title: GetTextLanguage("gioitinh"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: "150px"
                        },
                        {
                            field: "NgaySinh",
                            title: GetTextLanguage("ngaysinh"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: "150px"
                        },
                        {
                            field: "SDT",
                            title: GetTextLanguage("sdt"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: "150px"
                        },
                        {
                            field: "CMT",
                            title: GetTextLanguage("socmt"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: "150px"
                        },                 
                        {
                            field: "DiaChi",
                            title: GetTextLanguage("diachi"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: "150px"
                        },
                        {
                            field: "TinhTrang",
                            title: GetTextLanguage("tinhtrang"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: "150px"
                        },
                        { command: "destroy", title: "&nbsp;", width: "150px" }
                    ],

                });
                $(".btn-import").click(function () {
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/QLNhanVien/ImportNhanVien",
                        method: "post",
                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()) },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#grid_DSNhanVien").data("kendoGrid").dataSource.read();
                                $("#grid_DSNhanVien").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    closeModel("mdlThemFileExcel");
                });
            }
        }
        objXhr3.open("POST", "/QLNhanVien/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
        //////////////////////////////////////////////////////////////////
    });
});