﻿
var uid;
$(document).ready(function () {
    var height = window.innerHeight * 0.8;
    var dsDoiThiCong = [];
    var dsChucVu = [];

    var checkDaNopCmt = [
        { text: GetTextLanguage("danop"), value: true },
        { text: GetTextLanguage("chuanop"), value: false }
    ]
    var checkDaNopBang = [
        { text: GetTextLanguage("danop"), value: true },
        { text: GetTextLanguage("chuanop"), value: false }
    ]
    var gioitinh = [
        { text: GetTextLanguage("boy"), value: 0 },
        { text: GetTextLanguage("nu"), value: 1 }
    ];
    var tinhtrang = [
        { text: GetTextLanguage("danglamviec"), value: 1 },
        { text: GetTextLanguage("nghichoviec"), value: 2 },
        { text: GetTextLanguage("thoiviec"), value: 3 }
    ];
    var loaihopdong = [
        { text: GetTextLanguage("chuaky"), value: 0 },
        { text: GetTextLanguage("daihan"), value: 1 },
        { text: GetTextLanguage("thang1236"), value: 2 },
        { text: GetTextLanguage("thuviec"), value: 3 },
        { text: GetTextLanguage("muavu"), value: 4 }
    ];
    var dataSourceLoaiHD = new kendo.data.DataSource({
        data: loaihopdong
    });
    var dataSourceGT = new kendo.data.DataSource({
        data: gioitinh
    });
    var dataSourceDoiThiCong = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QuanLyNhanVien/getDataDoiThiCong",
                type: 'get',
                dataType: 'json'
            }
        },
        schema: {
            type: 'json',
            data: function (response) {
                return response.data;
            }
        }
    });
    var dataSourceCongTrinh = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QuanLyNhanVien/getDataCongTrinh",
                type: 'get',
                dataType: 'json'
            }
        },
        schema: {
            type: 'json',
            data: function (response) {
                return response.data;
            }
        }
    });
    var dataSourceChucVu = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QuanLyNhanVien/getDataChucVu",
                type: 'get',
                dataType: 'json'
            }
        },
        schema: {
            type: 'json',
            data: function (response) {
                return response.data;
            }
        }
    });
    var dataSourceTinhTrang = new kendo.data.DataSource({
        data: tinhtrang
    });
    //Notification
    var notification = $("#notification").kendoNotification({
        position: {
            pinned: true,
            top: 30,
            right: 30
        },
        autoHideAfter: 3000,
        stacking: "down",
        templates: [{
            type: "error",
            template: $("#errorTemplate").html()
        }, {
            type: "success",
            template: $("#successTemplate").html()
        }]

    }).data("kendoNotification");

    var imgbase64;
    //dataSourceDoiThiCong.fetch(function () {
    //    var data = this.data();
    //    for (var i = 0; i < data.length; i++) {
    //        dsDoiThiCong.push({
    //            value: data[i].value,
    //            text: data[i].text
    //        })
    //    };
    //dataSourceChucVu.fetch(function () {
    ///    var data = this.data();
    //    for (var i = 0; i < data.length; i++) {
    //        dsChucVu.push({
    //            value: data[i].value,
    //            text: data[i].text
    //        })
    //    };
    var dataNhanVienHetHan = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QuanLyNhanVien/getDataNhanVienHetHan",
                type: "get",
                dataType: 'json'
            },
            parameterMap: function (data, type) {
                if (type !== "read") {
                    if (type == "create") {
                        console.log(data);
                        var model = {
                            TenNhanVien: data.models[0].TenNhanVien,
                            DoiThiCongID: data.models[0].DoiThiCongID,
                            ChucVu: data.models[0].ChucVu,
                            NgaySinh: data.models[0].NgaySinh,
                            CongTrinhID: data.models[0].CongTrinhID,
                            GioiTinh: data.models[0].GioiTinh,
                            Cmt: data.models[0].Cmt,
                            DaNopCmt: data.models[0].DaNopCmt,
                            TinhTrang: data.models[0].TinhTrang,
                            DaNopBang: data.models[0].DaNopBang,
                            LoaiHopDong: data.models[0].LoaiHopDong,
                            DaKyHd: data.models[0].DaKyHd,
                            NgayKetThucHd: $("#NgayKetThucHd").val(),
                            SDT: data.models[0].SDT,
                            DiaChi: data.models[0].DiaChi,
                            SoNgayGioiHan: data.models[0].SoNgayGioiHan,
                            DsHinhAnh: data.models[0].DsHinhAnh,
                            NgayDiLam: $("#NgayDiLam").val(),
                            NgayThoiViec: $("#NgayThoiViec").val(),
                            ThoiGianDieuChuyen: data.models[0].ThoiGianDieuChuyen,
                            NgayCapCMT: data.models[0].NgayCapCMT,
                            NoiCapCMT: data.models[0].NoiCapCMT,
                            NganhNghe: data.models[0].NganhNghe,
                            SoTaiKhoanNganHang: data.models[0].SoTaiKhoanNganHang,
                            TenNganHang: data.models[0].TenNganHang,
                            MaSoThue: data.models[0].MaSoThue,
                            Avatar: data.models[0].Avatar,
                            TruongCapBang: data.models[0].TruongCapBang,
                            DSCongTrinh: data.models[0].DSCongTrinh
                        }
                        return { models: kendo.stringify(model) }
                    }
                    else if (type == "destroy") {
                        return { nhanVienID: data.models[0].NhanVienID }
                    }
                    else
                        return { models: kendo.stringify(data.models[0]) }
                }
            }
        },
        batch: true,
        pageSize: 20,
        schema: {
            type: 'json',
            data: 'data',
            total: "itemCount",
            model: {
                id: "NhanVienID",
                fields: {
                    NhanVienID: { type: "string" },
                    TenNhanVien: { type: "string" },
                    ChucVu: { type: "string" },
                    TenChucVu: { type: "string" },
                    DiaChi: { type: "string" },
                    SoNgayGioiHan: { type: "string" },
                    DoiThiCongID: { type: "string" },
                    CongTrinhID: { type: "string" },
                    NgaySinh: { type: "date" },
                    GioiTinh: { type: "number" },
                    Cmt: { type: "string" },
                    DaNopCmt: { type: "boolean" },
                    TinhTrang: { type: "number", defaultValue: tinhtrang[0].value },
                    DaNopBang: { type: "boolean" },
                    LoaiHopDong: { type: "number", defaultValue: loaihopdong[0].value },
                    DaKyHd: { type: "boolean" },
                    NgayKetThucHd: { type: "date" },
                    SDT: { type: "string" },
                    IsActive: { type: "boolean" },
                    Avatar: { type: "string", defaultValue: "novarta.png" },
                    NgayDiLam: { type: "date" },
                    NgayThoiViec: { type: "date" },
                    ThoiGianDieuChuyen: { type: "date" },
                    NgayCapCMT: { type: "date" },
                    NoiCapCMT: { type: "string" },
                    NganhNghe: { type: "string" },
                    TenNganHang: { type: "string" },
                    SoTaiKhoanNganHang: { type: "string" },
                    MaSoThue: { type: "string" },
                    DsHinhAnh: { type: "string" },
                    TruongCapBang: { type: "string" },
                    DSCongTrinh: { type: "object" }
                }
            }
        },
    });
    var dataQuanLyNhanVien = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QuanLyNhanVien/getDataQuanLyNhanVien",
                type: "get",
                dataType: 'json'
            },
            baohethanhopdong: {
                url: "/QuanLyNhanVien/BaoHetHanHopDong",
                type: "post",
                dataType: 'json',
                complete: function (e) {
                    notification.show({
                        title: e.responseJSON.notiTile,
                        message: e.responseJSON.notiMessage
                    }, e.responseJSON.notiType);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            create: {
                url: "/QuanLyNhanVien/Create",
                type: "post",
                dataType: 'json',
                complete: function (e) {
                    notification.show({
                        title: e.responseJSON.notiTile,
                        message: e.responseJSON.notiMessage
                    }, e.responseJSON.notiType);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            update: {
                url: "/QuanLyNhanVien/Update",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    notification.show({
                        title: e.responseJSON.notiTile,
                        message: e.responseJSON.notiMessage
                    }, e.responseJSON.notiType);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            destroy: {
                url: "/QuanLyNhanVien/Delete",
                type: 'post',
                dataType: "json",
                complete: function (e) {
                    notification.show({
                        title: e.responseJSON.notiTile,
                        message: e.responseJSON.notiMessage
                    }, e.responseJSON.notiType);
                    $("#grid").data("kendoGrid").dataSource.read();
                }
            },
            parameterMap: function (data, type) {
                if (type !== "read") {
                    if (type == "create") {
                        var model = {
                            TenNhanVien: data.models[0].TenNhanVien,
                            DoiThiCongID: data.models[0].DoiThiCongID,
                            ChucVu: data.models[0].ChucVu,
                            NgaySinh: data.models[0].NgaySinh == null ? null : (data.models[0].NgaySinh == "" ? null : kendo.toString(data.models[0].NgaySinh, "dd/MM/yyyy")),
                            CongTrinhID: data.models[0].CongTrinhID,
                            GioiTinh: data.models[0].GioiTinh,
                            Cmt: data.models[0].Cmt,
                            DaNopCmt: data.models[0].DaNopCmt,
                            TinhTrang: data.models[0].TinhTrang,
                            DaNopBang: data.models[0].DaNopBang,
                            LoaiHopDong: data.models[0].LoaiHopDong,
                            DaKyHd: data.models[0].DaKyHd,
                            NgayKetThucHd: $("#NgayKetThucHd").val(),
                            NgayDiLam: $("#NgayDiLam").val(),
                            NgayThoiViec: $("#NgayThoiViec").val(),
                            SDT: data.models[0].SDT,
                            DiaChi: data.models[0].DiaChi,
                            SoNgayGioiHan: data.models[0].SoNgayGioiHan,
                            DsHinhAnh: data.models[0].DsHinhAnh,
                            ThoiGianDieuChuyen: data.models[0].ThoiGianDieuChuyen,
                            NgayCapCMT: data.models[0].NgayCapCMT,
                            NoiCapCMT: data.models[0].NoiCapCMT,
                            NganhNghe: data.models[0].NganhNghe,
                            SoTaiKhoanNganHang: data.models[0].SoTaiKhoanNganHang,
                            TenNganHang: data.models[0].TenNganHang,
                            MaSoThue: data.models[0].MaSoThue,
                            Avatar: $("#avtastring").val(),
                            TruongCapBang: data.models[0].TruongCapBang,
                            DSCongTrinh: data.models[0].DSCongTrinh,
                            SoNgayKhaiCong: data.models[0].SoNgayKhaiCong
                        }
                        return { models: kendo.stringify(model) }
                    }
                    else if (type = "update") {
                        var model = {
                            TenNhanVien: data.models[0].TenNhanVien,
                            DoiThiCongID: data.models[0].DoiThiCongID,
                            ChucVu: data.models[0].ChucVu,
                            NgaySinh: data.models[0].NgaySinh == null ? null : (data.models[0].NgaySinh == "" ? null : kendo.toString(data.models[0].NgaySinh, "dd/MM/yyyy")),
                            CongTrinhID: data.models[0].CongTrinhID,
                            GioiTinh: data.models[0].GioiTinh,
                            Cmt: data.models[0].Cmt,
                            DaNopCmt: data.models[0].DaNopCmt,
                            TinhTrang: data.models[0].TinhTrang,
                            DaNopBang: data.models[0].DaNopBang,
                            LoaiHopDong: data.models[0].LoaiHopDong,
                            DaKyHd: data.models[0].DaKyHd,
                            NgayKetThucHd: $("#NgayKetThucHd").val(),
                            NgayDiLam: $("#NgayDiLam").val(),
                            NgayThoiViec: $("#NgayThoiViec").val(),
                            SDT: data.models[0].SDT,
                            DiaChi: data.models[0].DiaChi,
                            SoNgayGioiHan: data.models[0].SoNgayGioiHan,
                            DsHinhAnh: data.models[0].DsHinhAnh,
                            ThoiGianDieuChuyen: data.models[0].ThoiGianDieuChuyen,
                            NgayCapCMT: data.models[0].NgayCapCMT,
                            NoiCapCMT: data.models[0].NoiCapCMT,
                            NganhNghe: data.models[0].NganhNghe,
                            SoTaiKhoanNganHang: data.models[0].SoTaiKhoanNganHang,
                            TenNganHang: data.models[0].TenNganHang,
                            MaSoThue: data.models[0].MaSoThue,
                            Avatar: $("#avtastring").val(),
                            TruongCapBang: data.models[0].TruongCapBang,
                            DSCongTrinh: data.models[0].DSCongTrinh,
                            NhanVienID: data.models[0].NhanVienID,
                            SoNgayKhaiCong: data.models[0].SoNgayKhaiCong
                        }
                        return { models: kendo.stringify(model) }
                    }
                    else if (type == "destroy") {
                        return { nhanVienID: data.models[0].NhanVienID }
                    }
                    else
                        return { models: kendo.stringify(data.models[0]) }
                }
            }
        },
        batch: true,
        pageSize: 20,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "NhanVienID",
                fields: {
                    NhanVienID: { type: "string" },
                    TenNhanVien: { type: "string" },
                    ChucVu: { type: "string" },
                    TenChucVu: { type: "string" },
                    DiaChi: { type: "string" },
                    SoNgayGioiHan: { type: "string" },
                    DoiThiCongID: { type: "string" },
                    CongTrinhID: { type: "string" },
                    NgaySinh: { type: "date" },
                    GioiTinh: { type: "number" },
                    Cmt: { type: "string" },
                    DaNopCmt: { type: "boolean" },
                    TinhTrang: { type: "number", defaultValue: tinhtrang[0].value },
                    DaNopBang: { type: "boolean" },
                    LoaiHopDong: { type: "number", defaultValue: loaihopdong[0].value },
                    DaKyHd: { type: "boolean" },
                    NgayKetThucHd: { type: "date" },
                    SDT: { type: "string" },
                    Avatar: { type: "string", defaultValue: "novarta.png" },
                    NgayDiLam: { type: "date" },
                    NgayThoiViec: { type: "date" },
                    ThoiGianDieuChuyen: { type: "date" },
                    NgayCapCMT: { type: "date" },
                    NoiCapCMT: { type: "string" },
                    NganhNghe: { type: "string" },
                    TenNganHang: { type: "string" },
                    SoTaiKhoanNganHang: { type: "string" },
                    MaSoThue: { type: "string" },
                    DsHinhAnh: { type: "string" },
                    TruongCapBang: { type: "string" },
                    DSCongTrinh: { type: "object" },
                    SoNgayKhaiCong: { type: "string" }
                }
            }
        },
    });
    var grid = $("#grid").kendoGrid({
        dataSource: dataQuanLyNhanVien,
        pageable: true,
        selectable: "multiple",
        height: window.innerHeight * 0.8,
        filterable: {
            mode: "row"
        },
        resizable: true,
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        toolbar: [
            {
                name: "create", text: GetTextLanguage("them")
            },
            {
                template: "<button class='k-button k-button-icontext k-grid-baohethan k-grid-Báohếthạn'><i class='fa fa-clock-o' aria-hidden='true'></i>" + "  " + GetTextLanguage("baohethan") + "</button>"
            },
            //{
            //    name: "excel", text: GetTextLanguage("xuatexcel")
            //},
            {
                text: GetTextLanguage("dieuchuyen"),
                className: "k-grid-custom",
                icon: "arrowhead-n"

            },
            {
                template: "<button class='k-button k-button-icontext k-grid-kyhopdong k-grid-Kýhợpđồng'><i class='fa fa-file-text' aria-hidden='true'></i>" + "  " + GetTextLanguage("kyhopdong") + "</button>"
            }
        ],
        columns: [
            {
                template: "<button class='btn btn-primary btnChiTiet'>" + GetTextLanguage('chitiet') + "</button>", width: 100, attributes: alignCenter
            },
            {
                field: "TenNhanVien", title: GetTextLanguage("tennhanvien"), width: 200,
                filterable: FilterInTextColumn
            },
            {
                field: "TenChucVu", title: GetTextLanguage("chucvu"), width: 200, filterable: FilterInTextColumn
            },
            {
                field: "TenDoi", title: GetTextLanguage("tendoi"), width: 200, filterable: FilterInTextColumn
            },
            {
                field: "TenCongDoi", title: GetTextLanguage("tencongtrinh"), width: 200, attributes: { style: "text-align:center;" }, filterable: FilterInTextColumn
            },
            {
                field: "TinhTrang", title: GetTextLanguage("tinhtrang"), width: 100, values: tinhtrang, filterable: FilterInColumn
            },
            {
                field: "NgayDiLam", title: GetTextLanguage("ngaydilam"), format: "{0:dd/MM/yyyy}", width: 180, attributes: { style: "text-align:center;" }, filterable: FilterInColumn, hidden: true
            },
            {
                field: "NgayThoiViec", title: GetTextLanguage("ngaythoiviec"), format: "{0:dd/MM/yyyy}", width: 180, attributes: { style: "text-align:center;" }, filterable: FilterInColumn, hidden: true
            },
            {
                field: "NgayKetThucHd", title: GetTextLanguage("ngayketthuchopdong"), format: "{0:dd/MM/yyyy}", width: 180, attributes: { style: "text-align:center;" }, filterable: FilterInColumn, hidden: true
            },
            {
                field: "DiaChi", title: GetTextLanguage("diachi"), width: 200, filterable: FilterInTextColumn
            },
            {
                field: "NgaySinh", title: GetTextLanguage("ngaysinh"), format: "{0:dd/MM/yyyy}", width: 180, attributes: { style: "text-align:center;" }, filterable: FilterInColumn
            },
            {
                field: "GioiTinh", title: GetTextLanguage("gioitinh"), width: 100, values: gioitinh, attributes: { style: "text-align:center;" }, filterable: FilterInColumn
            },
            {
                field: "SDT", title: GetTextLanguage("sdt"), width: 100, attributes: { style: "text-align:center;" }, filterable: FilterInColumn
            },

            {
                field: "Cmt", title: GetTextLanguage("socmt"), width: 150, attributes: { style: "text-align:center;" }, filterable: FilterInColumn
            },
            {
                field: "DaNopCmt", title: GetTextLanguage("danopcmt"), width: 200, values: checkDaNopCmt, filterable: FilterInColumn,
                template: '<input type="checkbox" #= DaNopCmt == true ? \'checked="checked"\' : "" # disabled/>',
                attributes: { style: "text-align:center;" }, hidden: true
            },
            {
                field: "DaNopBang", title: GetTextLanguage("danopbang"), width: 200, values: checkDaNopBang, filterable: FilterInColumn,
                template: '<input type="checkbox" #= DaNopBang == true ? \'checked="checked"\' : "" # disabled/>',
                attributes: { style: "text-align:center;" }, hidden: true
            },
            {
                field: "LoaiHopDong", title: GetTextLanguage("loaihopdong"), width: 100, values: loaihopdong, filterable: FilterInColumn
            },
            {
                field: "TenNganHang", title: GetTextLanguage("tennganhang"), width: 200, filterable: FilterInTextColumn, hidden: true
            },
            {
                field: "SoTaiKhoanNganHang", title: GetTextLanguage("sotaikhoan"), width: 100, attributes: { style: "text-align:center;" }, filterable: FilterInColumn
            },
            {
                field: "NgayCapCMT", title: GetTextLanguage("ngaycapcmt"), format: "{0:dd/MM/yyyy}", width: 180, attributes: { style: "text-align:center;" }, filterable: FilterInColumn, hidden: true
            },
            {
                field: "NoiCapCMT", title: GetTextLanguage("noicapcmt"), width: 150, attributes: { style: "text-align:center;" }, filterable: FilterInColumn, hidden: true
            },
            {
                field: "NganhNghe", title: GetTextLanguage("nganhnghe"), width: 155, attributes: { style: "text-align:center;" }, filterable: FilterInColumn
            },
            {
                field: "UserName", title: GetTextLanguage("tentaikhoan"), width: 155, attributes: { style: "text-align:center;" }, filterable: FilterInColumn
            },
            {
                field: "SoNgayGioiHan", title: GetTextLanguage("songaygioihan"), width: 155, attributes: { style: "text-align:center;" }, filterable: FilterInColumn, hidden: true
            },
        ],
        editable: {
            mode: "popup",
            template: kendo.template($("#popup_editor").html()),
            window: {
                title: GetTextLanguage("thongtinnhanvien"),
                width: window.innerWidth * 0.9,
                height: window.innerHeight * 0.9,

            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes"
        },
        edit: function (e) {
            var element = $('div.k-widget.k-window');
            kendo.ui.progress(element, true);
            $.ajax({
                url: "/QuanLyNhanVien/LayDSCongTrinhTheoNhanVienCongTrinhDoi",
                dataType: 'json',
                data: { NhanVienID: e.model.NhanVienID },
                success: function (e1) {
                    e.model.DSCongTrinh = e1.data;
                    $("#dsCongTrinh").kendoMultiSelect({
                        filter: "contains",
                        dataSource: getDataSourceCT("/getDataCongTrinh", { DoiThiCongID: e.model.DoiThiCongID }),
                        dataTextField: "text",
                        dataValueField: "value",
                        tagMode: "single",
                        autoClose: false,
                        tagTemplate: '<span deselectable="on"> #:values.length#' + GetTextLanguage("congtrinhduocchon") + '</span>',

                    });
                }
            });
            var hinhanhkemtheo = e.model.DsHinhAnh;
            if (hinhanhkemtheo !== "") {
                var mangDsHinhAnh = hinhanhkemtheo.split(";");
                $.each(mangDsHinhAnh, function (i, item) {
                    $(".highslide-gallery").append("<a href='" + localQLNV + item + "' class='highslide' onclick='return hs.expand(this)'><img src='/Images/orderedList" + i + ".png" + "' style='width:20px;height:20px;margin-left:5px;' /></a>");
                });
            }
            $("#files").kendoUpload({
                async: {
                    saveUrl: "/Upload/Save",
                    removeUrl: "/Upload/Remove",
                    autoUpload: true,
                    localization: {
                        select: "File"
                    }
                },
                localization: {
                    select: GetTextLanguage("chonanh")
                },
                showFileList: false,
                success: function (e) {
                    if (e.operation == "upload") {
                        for (var i = 0; i < e.files.length; i++) {
                            var file = e.files[i].rawFile;

                            if (file) {
                                var reader = new FileReader();

                                reader.onloadend = function () {
                                    $(".avatar").remove();
                                    $("<div class='avatar'  style='text-align:center'><img src=" + this.result + " /></div>").appendTo($("#anh"));
                                    imgbase64 = this.result;
                                    var model = $("#grid").data("kendoGrid").dataSource.getByUid(uid);
                                    model.set('ImageAvatarBase64', imgbase64);
                                };
                                reader.readAsDataURL(file);
                            }
                        }
                        var fileName = e.files[0].name;
                        var model = $("#grid").data("kendoGrid").dataSource.getByUid(uid);
                        model.set('Avatar', fileName);
                    }
                }
            });
            var avata = "";
            $("#Avatar").kendoUpload({
                async: {
                    saveUrl: "/Upload/SaveAvatar",
                    removeUrl: "/Upload/Remove",
                    autoUpload: true,
                    localization: {
                        select: "File"
                    }
                },
                showFileList: false,
                multiple: false,
                localization: {
                    select: GetTextLanguage("chonanh")
                },
                success: function (e) {
                    if (e.operation === "upload") {
                        for (var i = 0; i < e.files.length; i++) {
                            var file = e.files[i].rawFile;
                            if (file) {
                                var reader = new FileReader();
                                reader.onloadend = function () {
                                    $(".avatar").remove();
                                    $("<div class='avatar'  style='text-align:center;margin:15px'><img src=" + this.result + "  width='100%' /></div>").appendTo($("#anh"));
                                    imgbase64 = this.result;
                                };
                                reader.readAsDataURL(file);
                            }
                        }
                        var fileName = e.files[0].name;
                        avata = fileName;
                        $("#avtastring").val(avata);
                    }
                }
            });
            $('.k-edit-buttons').prepend('<a class="btn btn-danger" id="btnXoa"><i class="fa fa-trash-o" aria-hidden="true"></i>' + GetTextLanguage("xoa") + '</a>')
            $('#btnXoa').on('click', function () {
                var cf = confirm(GetTextLanguage("xoabangi"));
                if (cf) {
                    $.ajax({
                        url: "/QuanLyNhanVien/Delete",
                        type: 'post',
                        data: {
                            nhanVienID: e.model.NhanVienID
                        },
                        success: function (result) {
                            if (result.notiType == "success") {
                                editWindow.close();

                            }
                            notification.show({
                                title: result.notiTile,
                                message: result.notiMessage
                            }, result.notiType);
                            $("#grid").data("kendoGrid").dataSource.read();
                        }
                    })
                }

            })
            var editWindow = this.editable.element.data("kendoWindow");
            $("input[name='DoiThiCongID']").kendoDropDownList({
                filter: "contains",
                dataSource: getDataForDropdownlist("/ThongTinCaNhan/getDataDoiThiCong"),
                dataTextField: "TenDoi",
                dataValueField: "DoiThiCongID",
                autobind: true,
                change: function () {
                    optionLabel: GetTextLanguage("chondoi"),
                    $("#dsCongTrinh").data("kendoMultiSelect").setDataSource(getDataSourceCT("/getDataCongTrinh", { DoiThiCongID: $("input[name='DoiThiCongID']").val() }));
                }
            });
            $("input[name='CongTrinhID']").kendoMultiSelect({
                filter: "contains",
                dataSource: getDataSourceCT("/getDataCongTrinh", { DoiThiCongID: $("#TenDoi").val() }),
                dataTextField: "text",
                dataValueField: "value",
                template: '<span deselectable="on"> #=text#</span>',
                autoClose: false,

            });
            $("#GioiTinh").kendoDropDownList({
                dataSource: dataSourceGT,
                dataTextField: "text",
                dataValueField: "value"
            });
            $("#TenChucVu").kendoDropDownList({
                filter: "contains",
                dataTextField: "TenChucVu",
                dataValueField: "ChucVuId",
                autoBind: false,
                optionLabel: GetTextLanguage("chonchucvu"),
                dataSource: getDataDroplit("/ThongTinCaNhan", "/getDataChucVu"),
            });
            $("#TinhTrang").kendoDropDownList({
                dataSource: tinhtrang,
                dataTextField: "text",
                dataValueField: "value",
                autobind: true
            });
            $("#LoaiHopDong").kendoDropDownList({
                dataSource: loaihopdong,
                dataTextField: "text",
                dataValueField: "value",
                autobind: true
            })
            $("#NgayThoiViec").kendoDatePicker({
                format: "dd/MM/yyyy", value: null
            });
            $("#NgayKetThucHd").kendoDatePicker({
                format: "dd/MM/yyyy", value: null
            });
            $(".fileUpload").click(function () {
                $("#Avatar").click();
            })
        },

        sortable: true,
        pageable: {
            pageSize: 50,
            pageSizes: true,
            refresh: true,
            messages: messagegrid
        },
    });
    $("#grid").on("click", ".btnChiTiet", function () {
        var dataItem = $("#grid").data("kendoGrid").dataItem($(this).closest("tr"));
        CreateModalWithSize("mdlThongTinNhanVien", "90%", "90%", null, GetTextLanguage("thongtinnhanvien") + ": " + dataItem.TenNhanVien);
        $("#mdlThongTinNhanVien").load("/ThongTinCaNhan/GetDataThongTinCaNhan?NhanVienID=" + dataItem.NhanVienID);
    });
    //Khi click vào nút điều chuyển
    $(".k-grid-custom").click(function () {
        var models = [];//mảng lữu trữ các mã NhanVienID
        var grid = $("#grid").data("kendoGrid");
        var listcheck = $("#grid").find("tr.k-state-selected");//Kiểm tra xem nhữn dòng nào được chọn
        wnd = $("#dieuchuyen")//tạo 1 poup cho việc điều chuyển
            .kendoWindow({
                title: GetTextLanguage("dieuchuyen"),
                modal: true,
                visible: false,
                resizable: false,
                width: 500,
                height: 350
            }).data("kendoWindow");
        wnd.content(kendo.template($("#template").html()));


        //duyệt qua từng dòng được chọn, và lấy ra model tương ứng với dòng đó
        $.each(listcheck, function (i, item) {
            //lấy ra model tương ứng của dòng được chọn thông qua mã uid của dòng đó
            var model = $("#grid").data("kendoGrid").dataSource.getByUid($(item).data("uid"));
            if (typeof (model) !== 'undefined')
                models.push(
                    model.NhanVienID
                );

        });
        //nếu mà mảng lưu trữ các mã NhanVienID mà có dữ liệu thì mới bật popup lên
        //đồng thời sẽ xử lý việc gửi dữ liệu về controller
        if (models.length != 0) {
            var doiThiCongID;
            var congTrinhThiCongID;
            var thoiGian;
            var chucVu
            wnd.center().open();//mở popup
            //lấy dữ liệu cho ô đôi thi công
            $("#doithicong").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                optionLabel: GetTextLanguage("chondoi"),
                dataSource: {
                    transport: {
                        read: {
                            url: "/QuanLyNhanVien/getDataDoiThiCong",
                            type: 'get',
                            dataType: 'json'
                        }
                    },
                    schema: {
                        type: 'json',
                        data: function (response) {
                            return response.data;
                        }
                    }
                },
                select: function (e) {
                    doiThiCongID = e.dataItem.value;

                },
                change: function (e) {
                    $("#congtrinhthicong").data("kendoMultiSelect").dataSource.read({ DoiThiCongID: e.sender._old });
                }
            });
            $("#congtrinhthicong").kendoMultiSelect({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                autoBind: false,
                optionLabel: GetTextLanguage("choncongtrinh"),
                tagMode: "single",
                autoClose: false,
                tagTemplate: '<span deselectable="on"> #:values.length#' + GetTextLanguage("congtrinhduocchon") + '</span>',
                dataSource: {
                    transport: {
                        read: {
                            url: "/QuanLyNhanVien/getDataCongTrinh",
                            type: 'get',
                            dataType: 'json'
                        }
                    },
                    schema: {
                        type: 'json',
                        data: function (response) {
                            return response.data;
                        }
                    }
                },
                select: function (e) {
                    congTrinhThiCongID = e.dataItem.value;
                },
                close: function (e) {
                }
            });
            $("#chucvu").kendoDropDownList({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: dataSourceChucVu,
                optionLabel: GetTextLanguage("chonchucvu"),
                select: function (e) {
                    chucVu = e.dataItem.value;
                },
            });
            //tạo datetimepicker cho ô thời gian
            $("#thoigian").kendoDateTimePicker({
                format: "dd/MM/yyyy",
                change: function () {
                    thoiGian = kendo.toString(this.value(), 'dd/MM/yyyy HH:mm:ss');
                    console.log(thoiGian);
                },
                value: new Date()
            });
            if (models.length > 1) {
                $('#dieuchuyencongtrinh').attr('disabled', 'disabled');
            }
            else {
                var rowduocchon = $("#grid").find("tr.k-state-selected");
                modelhientai = $("#grid").data("kendoGrid").dataSource.getByUid($(rowduocchon[0]).data("uid"));
                $('#dieuchuyencongtrinh').change(function () {
                    if (this.checked) {
                        $("#doithicong").data("kendoDropDownList").setDataSource([{ text: modelhientai.TenDoi, value: modelhientai.DoiThiCongID }]);
                        $("#doithicong").data("kendoDropDownList").select(1);
                        $("#doithicong").data("kendoDropDownList").trigger("change");
                        $("#chucvu").data("kendoDropDownList").value(modelhientai.ChucVu);
                        $("#chucvu").data("kendoDropDownList").trigger("change");
                    } else {
                        $("#doithicong").data("kendoDropDownList").setDataSource(new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: "/QuanLyNhanVien/getDataDoiThiCong",
                                    type: 'get',
                                    dataType: 'json'
                                }
                            },
                            schema: {
                                type: 'json',
                                data: function (response) {
                                    return response.data;
                                }
                            }
                        }))
                    }
                });
            }

            //xử lý sự kiện khi click vào nut điều chuển
            $("#updateDieuChuyen").click(function () {
                //nếu mà chưa chọn đội thi công thì đưa ra thông báo
                if (doiThiCongID == null) {
                    notification.show({
                        title: GetTextLanguage("coloitrongquatrinhlaydulieu"),
                        message: GetTextLanguage("vuilongchondoithicongnhan") + GetTextLanguage("dieuchuyen")
                    }, "error");
                }
                else if (chucVu == null || chucVu == "") {
                    notification.show({
                        title: GetTextLanguage("coloitrongquatrinhlaydulieu"),
                        message: GetTextLanguage("chonchucvu")
                    }, "error");
                }
                else {
                    $.ajax({
                        url: "/QuanLyNhanVien/DieuChuyen",
                        dataType: 'json',
                        type: 'post',
                        data: {
                            models: JSON.stringify(models),
                            doiThiCongID: doiThiCongID,
                            thoiGian: kendo.toString($("#thoigian").data("kendoDateTimePicker").value(), 'dd/MM/yyyy HH:mm:ss'),
                            congTrinhThiCongID: JSON.stringify($("#congtrinhthicong").data("kendoMultiSelect")._old),
                            chucVu: chucVu,
                            isDieuChuyenCongTrinh: $('#dieuchuyencongtrinh').is(":checked")
                        },
                        success: function (result) {
                            if (result.notiType == "success")
                                wnd.close();
                            notification.show({
                                title: result.notiTile,
                                message: result.notiMessage
                            }, result.notiType);
                            $("#grid").data("kendoGrid").dataSource.read();

                        }
                    });
                }

            });
            $("#cancelDieuChuyen").click(function () {
                wnd.close();
            });
        } else {
            toastr.options.positionClass = "toast-bottom-right";
            toastr.options.closeButton = true;
            toastr["warning"](GetTextLanguage("banphaichonnhanvien"), GetTextLanguage("banco1canhbao"), { timeOut: 2000 });
        }
    });
    $(".k-grid-custom span").addClass("k-icon k-i-arrowhead-n");
    $(".k-grid-kyhopdong").addClass("")
    $(".k-grid-kyhopdong").click(function () {
        var models = [];
        var grid = $("#grid").data("kendoGrid");
        var listcheck = $("#grid").find("tr.k-state-selected");
        var popup = $("#kyhopdong")
            .kendoWindow({
                title: GetTextLanguage("kyhopdong"),
                modal: true,
                visible: false,
                resizable: false,
                width: '82%',
                height: '80%'
            }).data("kendoWindow");
        popup.content(kendo.template($("#tplHopDong").html()));
        $.each(listcheck, function (i, item) {

            var model = $("#grid").data("kendoGrid").dataSource.getByUid($(item).data("uid"));
            if (typeof (model) !== 'undefined')
                models.push(
                    model.NhanVienID
                );

        });
        if (models.length == 1) {
            popup.center().open();
            $("#gridKyHopDong").kendoGrid({
                dataSource: {
                    transport: {
                        read: {
                            url: "/QuanLyNhanVien/LichSuKyHopDong",
                            dataType: 'json',
                            type: 'post',
                            data: {
                                nhanVienID: models[0]
                            }
                        },
                        create: {
                            url: "/QuanLyNhanVien/CreateHopDong",
                            type: "post",
                            dataType: 'json',
                            complete: function (e) {
                                showToast(e.responseJSON.notification.type, e.responseJSON.notification.title, e.responseJSON.notification.message);
                                $("#gridKyHopDong").data("kendoGrid").dataSource.read();
                            }
                        },
                        update: {
                            url: "/QuanLyNhanVien/UpdateHopDong",
                            type: "post",
                            dataType: 'json',
                            complete: function (e) {
                                showToast(e.responseJSON.notification.type, e.responseJSON.notification.title, e.responseJSON.notification.message);
                                $("#gridKyHopDong").data("kendoGrid").dataSource.read();
                            }
                        },
                        destroy: {
                            url: "/QuanLyNhanVien/DeleteHopDong",
                            type: "post",
                            dataType: 'json',
                            complete: function (e) {
                                altReturn(e.responseJSON);
                                $("#gridKyHopDong").data("kendoGrid").dataSource.read();
                            }
                        },
                        parameterMap: function (data, type) {
                            if (type !== "read") {
                                if (type == "create") {
                                    var model = {
                                        NhanVienID: models[0],
                                        NgayBatDau: kendo.toString(data.NgayBatDau, 'dd/MM/yyyy'),
                                        NgayKetThuc: kendo.toString(data.NgayKetThuc, 'dd/MM/yyyy'),
                                        LoaiHopDong: data.LoaiHopDong,
                                    }
                                    return { models: kendo.stringify(model) }
                                }
                                else {
                                    if (type == "update") {
                                        var model = {
                                            NhanVienID: models[0],
                                            NgayBatDau: kendo.toString(data.NgayBatDau, 'dd/MM/yyyy'),
                                            NgayKetThuc: kendo.toString(data.NgayKetThuc, 'dd/MM/yyyy'),
                                            LoaiHopDong: data.LoaiHopDong,
                                            HopDongNhanVienID: data.HopDongNhanVienID
                                        }
                                        return { models: kendo.stringify(model) }
                                    }
                                    else {
                                        return { HopDongNhanVienID: data.HopDongNhanVienID }
                                    }
                                }
                            }
                            else
                                return { NhanVienID: models[0] }

                        }
                    },
                    pageSize: 20,
                    schema: {
                        type: "json",
                        data: 'data',
                        model: {
                            id: "HopDongNhanVienID",
                            fields: {
                                LoaiHopDong: { type: 'number', defaultValue: loaihopdong[0].value },
                                NgayBatDau: { type: 'date' },
                                NgayKetThuc: { type: 'date' },
                                NgayThoiViec: { type: 'date' }
                            }
                        }
                    },
                },
                batch: true,
                toolbar: [{ name: "create", text: GetTextLanguage("them") }],
                editable: {
                    mode: "popup",
                    window: {
                        title: GetTextLanguage("thongtinchitiet"),
                        width: 400
                    },
                    confirmation: GetTextLanguage("xoabangi"),
                    confirmDelete: "Yes",
                },
                columns: [
                    { command: [{ name: "edit", text: GetTextLanguage("sua") }, { name: "destroy", text: GetTextLanguage("xoa") }], title: GetTextLanguage("thaotac"), width: 200 },
                    { field: "LoaiHopDong", title: GetTextLanguage("loaihopdong"), width: 150, values: loaihopdong, attributes: { style: "text-align:center;" } },
                    {
                        field: "NgayBatDau", title: GetTextLanguage("ngaybatdau"), format: "{0:dd/MM/yyyy}", width: 255, attributes: { style: "text-align:center;" }
                    },
                    //{
                    //    field: "NgayThoiViec", title: GetTextLanguage("ngaythoiviec"), format: "{0:dd/MM/yyyy}", width: 255, attributes: { style: "text-align:center;" }
                    //},
                    {
                        field: "NgayKetThuc", title: GetTextLanguage("ngayketthuc"), format: "{0:dd/MM/yyyy}", width: 255, attributes: { style: "text-align:center;" }
                    },
                ],
                messages: {
                    commands: {
                        update: GetTextLanguage("capnhat"),
                        canceledit: GetTextLanguage("huy")
                    }
                },
                edit: function (e) {
                    var start = $(e.container).find("input[name='NgayBatDau']").kendoDatePicker({
                        change: startChange
                    }).data("kendoDatePicker");

                    var end = $(e.container).find("input[name='NgayKetThuc']").kendoDatePicker({
                        change: endChange
                    }).data("kendoDatePicker");

                    function startChange() {
                        var startDate = start.value(),
                            endDate = end.value();

                        if (startDate) {
                            startDate = new Date(startDate);
                            startDate.setDate(startDate.getDate());
                            end.min(startDate);
                        } else if (endDate) {
                            start.max(new Date(endDate));
                        } else {
                            endDate = new Date();
                            start.max(endDate);
                            end.min(endDate);
                        }
                    }

                    function endChange() {
                        var endDate = end.value(),
                            startDate = start.value();

                        if (endDate) {
                            endDate = new Date(endDate);
                            endDate.setDate(endDate.getDate());
                            start.max(endDate);
                        } else if (startDate) {
                            end.min(new Date(startDate));
                        } else {
                            endDate = new Date();
                            start.max(endDate);
                            end.min(endDate);
                        }
                    }
                    start.max(end.value());
                    end.min(start.value());
                },
                sortable: true,
                height: $("#kyhopdong")[0].clientHeight * 0.71
            })
            $("#btnLoad").click(function () {
                $("#gridKyHopDong").data("kendoGrid").dataSource.read();
            });
            $('#btnDong').click(function () {
                popup.close();
            })
        } else {
            toastr.options.positionClass = "toast-bottom-right";
            toastr.options.closeButton = true;
            toastr["warning"](GetTextLanguage("banphaichonnhanvien"), GetTextLanguage("banco1canhbao"), { timeOut: 2000 });
        }
    });
    //Báo hết hạn hợp đồng
    //$(".k-grid-baohethan span").addClass("k-i-notification k-i-bell");
    $(".k-grid-baohethan").click(function () {
        var window;
        window = $("#baohethan")//tạo 1 poup cho việc điều chuyển
            .kendoWindow({
                title: GetTextLanguage("baonhanviensaphethan"),
                modal: true,
                visible: false,
                resizable: false,
                width: '80%',
                height: '80%'
            }).data("kendoWindow");
        window.content(kendo.template($("#template-baohethan").html()));
        window.center().open();//mở popup
        $("#closebaohethan").click(function () {
            window.close();
        });
        var grid = $("#gridbaohethan").kendoGrid({
            dataSource: dataNhanVienHetHan,
            sortable: true,
            pageable: {
                refresh: true,
                //pageSizes: true,
                //buttonCount: 5
            },
            height: '100%',
            columns: [
                {
                    field: "TenNhanVien", title: GetTextLanguage("tennhanvien"), width: 175, attributes: { style: "text-align:center;" }
                },
                {
                    field: "TenChucVu", title: GetTextLanguage("chucvu"), width: 175, attributes: { style: "text-align:center;" }
                },
                {
                    field: "TenDoi", title: GetTextLanguage("tendoi"), width: 175, attributes: { style: "text-align:center;" }
                },
                {
                    field: "TenCongTrinhThiCong", title: GetTextLanguage("congtrinh"), width: 270, attributes: { style: "text-align:center;" }
                },
                {
                    field: "DiaChi", title: GetTextLanguage("diachi"), width: 255, attributes: { style: "text-align:center;" }
                },
                {
                    field: "NgaySinh", title: GetTextLanguage("ngaysinh"), format: "{0:dd/MM/yyyy}", width: 155, attributes: { style: "text-align:center;" }
                },
                {
                    field: "GioiTinh", title: GetTextLanguage("gioitinh"), width: 120, values: gioitinh, attributes: { style: "text-align:center;" }
                },
                {
                    field: "SDT", title: GetTextLanguage("sdt"), width: 155, attributes: { style: "text-align:center;" }
                },
                {
                    field: "Cmt", title: GetTextLanguage("socmt"), width: 155, attributes: { style: "text-align:center;" }
                },
                {
                    field: "DaNopCmt", title: GetTextLanguage("danopcmt"), width: 200, values: checkDaNopCmt, filterable: FilterInColumn,
                    template: '<input type="checkbox" #= DaNopCmt == true ? \'checked="checked"\' : "" # disabled/>',
                    attributes: { style: "text-align:center;" }
                },
                {
                    field: "TinhTrang", title: GetTextLanguage("tinhtrang"), width: 200, values: tinhtrang
                },
                {
                    field: "DaNopBang", title: GetTextLanguage("danopbang"), width: 200, values: checkDaNopBang, filterable: FilterInColumn,
                    template: '<input type="checkbox" #= DaNopBang == true ? \'checked="checked"\' : "" # disabled/>',
                    attributes: { style: "text-align:center;" }
                },
                { field: "LoaiHopDong", title: GetTextLanguage("loaihopdong"), width: 150, values: loaihopdong },
                //{
                //    field: "DaKyHd", title: "Đã Ký HĐ", width: 100,
                //    template: '<input type="checkbox" #= DaKyHd == true ? \'checked="checked"\' : "" # disabled/>',
                //    attributes: { style: "text-align:center;" }
                //},
                {
                    field: "NgayDiLam", title: GetTextLanguage("ngaydilam"), format: "{0:dd/MM/yyyy}", width: 155, attributes: { style: "text-align:center;" }
                },
                {
                    field: "NgayThoiViec", title: GetTextLanguage("ngaythoiviec"), format: "{0:dd/MM/yyyy}", width: 155, attributes: { style: "text-align:center;" }
                },
                {
                    field: "NgayKetThucHd", title: GetTextLanguage("ngayketthuchopdong"), format: "{0:dd/MM/yyyy}", width: 155, attributes: { style: "text-align:center;" }
                },
                {
                    field: "TenNganHang", title: GetTextLanguage("tennganhang"), width: 155,
                },
                {
                    field: "SoTaiKhoanNganHang", title: GetTextLanguage("sotaikhoan"), width: 155, attributes: { style: "text-align:center;" }
                },
                {
                    field: "NgayCapCMT", title: GetTextLanguage("ngaycapcmt"), format: "{0:dd/MM/yyyy}", width: 155, attributes: { style: "text-align:center;" }
                },
                {
                    field: "NoiCapCMT", title: GetTextLanguage("noicapcmt"), width: 155, attributes: { style: "text-align:center;" }
                },
                {
                    field: "NganhNghe", title: GetTextLanguage("nganhnghe"), width: 155, attributes: { style: "text-align:center;" }
                },
                {
                    field: "UserName", title: GetTextLanguage("tentaikhoan"), width: 155, attributes: { style: "text-align:center;" }
                },
                {
                    field: "SoNgayGioiHan", title: GetTextLanguage("songaygioihan"), width: 155, attributes: { style: "text-align:center;" }
                },
                {
                    field: "DsHinhAnh", hidden: true
                }
            ]
        });
    });



    //});       
    //});

    function onRequestEnd(e) {
        if (e.type == "update" && !e.response.Errors) {
            $('#grid').data('kendoGrid').dataSource.read();
            $('#grid').data('kendoGrid').refresh();
        }

        if (e.type == "create" && !e.response.Errors) {
            $('#grid').data('kendoGrid').dataSource.read();
            $('#grid').data('kendoGrid').refresh();
        }
    }
    function showToast(loai, title, message) {
        toastr.options.positionClass = "toast-bottom-right";
        toastr[loai](message, title, { timeOut: 3000 });
    }
    function getDataSourceCT(url, param) {
        var dataSources = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/QuanLyNhanVien" + url,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: param,
                    complete: function (e) {
                        var element = $('div.k-widget.k-window');
                        kendo.ui.progress(element, false);
                    }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            batch: true,
            schema: {
                data: "data",
                total: "total"
            }
        });

        return dataSources;
    }

});