﻿$(document).ready(function () {
    var d = new Date();
    var month = d.getMonth() + 1;
    var datafix_Nam = [];
    for (var i = 0; i < 10; i++) {
        datafix_Nam.push({ text: d.getFullYear() - i, value: d.getFullYear() - i });
    }
   
    $("#Nam").kendoDropDownList({
        dataSource: datafix_Nam,
        dataTextField: "text",
        dataValueField: "value",
        autoBind: true,
    });

    $("#NamAdd").kendoDropDownList({
        dataSource: datafix_Nam,
        dataTextField: "text",
        dataValueField: "value",
        autoBind: true,
    });
});