﻿var crudServiceBaseUrl = "/YKienDongGop";
var crudServiceBaseUrlUploadFile = "/Upload";
$(document).ready(function () {
    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        },
        select: function (e) {
            var tab = e.item.id;
            //if (tab == "tablichsunhanluong") dataTabLichSuNhanLuong();
            if (tab == "tablichsulamviec") dataTabLichSuLamViec();
            else if (tab == "tabbangcapchungchi") dataTabBangCapChungChi();
            else if (tab == "tabtaisandangquanly") dataTabTaiSanDangQuanLy();
            else if (tab == "tablichsudanhgia") dataTabLichSuDanhGia();
            else if (tab == "tabhosocanhan") dataTabHopDongCaNhan();
            else if (tab == "tabykienphanhoi") dataYKienPhanHoi();
            else if (tab == "tabbienban") dataTabBienBan();
        }
    });
    if ($(location).attr('pathname') === "/QuanLyNhanVien/Index") {
        $("#tabykienphanhoi").addClass("hidden");
    }
    dataTabThongTinCaNhan();
    //Thông tin cá nhân
    function dataTabThongTinCaNhan() {

        var dataGioiTinh = [
            { text: GetTextLanguage("boy"), value: 0 },
            { text: GetTextLanguage("nu"), value: 1 }
        ];
        var dataTinhTrang = [
            { text: GetTextLanguage("thoiviec"), value: 3 },
            { text: GetTextLanguage("nghichoviec"), value: 2 },
            { text: GetTextLanguage("danglam"), value: 1 }
        ];
        $("#NgaySinh").kendoDatePicker({
            format: "dd/MM/yyyy"
        });
        $("#GioiTinh").kendoDropDownList({
            dataSource: dataGioiTinh,
            dataTextField: "text",
            dataValueField: "value"
        });
        $("#NgayCapCMND").kendoDatePicker({
            format: "dd/MM/yyyy"
        });
        $("#thongtincanhan").height(heightGrid - 150);
        var avata = $(".avtastring").val();
        $("#Avatar").kendoUpload({
            async: {
                saveUrl: "/Upload/SaveAvatar",
                removeUrl: "/Upload/Remove",
                autoUpload: true,
                localization: {
                    select: "File"
                }
            },
            showFileList: false,
            multiple: false,
            localization: {
                select: GetTextLanguage("chonanh")
            },
            success: function (e) {
                if (e.operation === "upload") {
                    for (var i = 0; i < e.files.length; i++) {
                        var file = e.files[i].rawFile;
                        if (file) {
                            var reader = new FileReader();
                            reader.onloadend = function () {
                                $(".avatar").remove();
                                $("<div class='avatar'  style='text-align:center;margin:15px'><a rel='gallery' href='"  +this.result +"'><img src=" + this.result + "  width='100%' /></a></div>").appendTo($("#anh"));
                                imgbase64 = this.result;
                                showAvataImage();
                            };
                            reader.readAsDataURL(file);
                        }
                    }
                    var fileName = e.files[0].name;
                    avata = fileName;
                }
            }
        });
        $("#thongtincanhan").kendoValidator().data("kendoValidator");
       
        $(".delete").click(function () {
            var model = {};
            model.NhanVienID = $('#ThongTinNhanVienID').val();
            if (model) {
                $.ajax({
                    url: "/QuanLyNhanVien/Delete",
                    method: "POST",
                    dataType: 'json',
                    data: { nhanVienID: model.NhanVienID },
                    success: function (data) {
                        altReturn(data);

                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));

                    }
                });
            }
        });
        $(".save").click(function () {
            var model = {};
            model.NhanVienID = $('#ThongTinNhanVienID').val();
            model.TenNhanVien = $('#TenNhanVien').val();
            model.NgaySinh = kendo.toString($('#NgaySinh').data("kendoDatePicker").value(), 'dd/MM/yyyy');
            model.GioiTinh = $('#GioiTinh').val();
            model.SDT = $('#SDT').val();
            model.TenNganHang = $('#TenNganHang').val();
            model.SoTaiKhoanNganHang = $('#SoTaiKhoanNganHang').val();
            model.MaSoThue = $('#MaSoThue').val();
            model.CMND = $('#CMND').val();
            model.NgayCapCMND = kendo.toString($('#NgayCapCMND').data("kendoDatePicker").value(), 'dd/MM/yyyy');
            model.NoiCapCMND = $('#NoiCapCMND').val();
            model.NganhNghe = $('#NganhNghe').val();
            model.DiaChi = $('#DiaChi').val();
            model.Avatar = avata;
            model.ChucVuID = $("#TenChucVu").data("kendoDropDownList").value();
            model.LoaiHopDong = $("#LoaiHopDong").data("kendoDropDownList").value();
            model.TinhTrang = $("#TinhTrang").data("kendoDropDownList").value();
            model.NgayDiLam = $("#NgayDiLam").data("kendoDatePicker").value();
            model.NgayHetHanHopDong = $("#NgayHetHanHopDong").data("kendoDatePicker").value();
            model.NgayThoiViec = $("#NgayThoiViec").data("kendoDatePicker").value();
            model.SoNgayGioiHan = $('#SoNgayGioiHan').val();
            model.SoNgayKhaiCong = $('#SoNgayKhaiCong').val();
            model.TruongCapBang = $('#TruongCapBang').val
            model.Lever = $('#Lever').val();
            model.ChucVu = $("#TenChucVu").val();
            if (model) {
                if ($(location).attr('pathname') === "/QuanLyNhanVien/Index") {
                    AjaxLoading("/ThongTinCaNhan" + "/UpdateThongTinNhanVien", { models: JSON.stringify(model) }, "mdlThongTinNhanVien", "grid", "mdlThongTinNhanVien", {});
                } else {
                    AjaxLoading("/ThongTinCaNhan" + "/UpdateThongTinNhanVien", { models: JSON.stringify(model) }, "ContentHSCN", "", "", {});
                }
            }
        });
        $("#Lever").kendoDropDownList({
            dataSource: getDataForDropdownlist("/home/DSCapDoBaoMat"),
            dataTextField: "TenCapDo",
            dataValueField: "CapDoBaoMatID",
        });
        $("#TenDoi").kendoDropDownList({
            dataSource: getDataForDropdownlist("/ThongTinCaNhan/getDataDoiThiCong"),
            dataTextField: "TenDoi",
            dataValueField: "DoiThiCongID",
            autobind: true
        });

        $("#TenCongTrinh").kendoMultiSelect({
            filter: "contains",
            dataSource: getDataSourceCT("/getDataCongTrinh", { DoiThiCongID: $("#TenDoi").val() }),
            dataTextField: "text",
            dataValueField: "value",
            template: '<span deselectable="on"> #=text#</span>',
            autoClose: false,
            
        });
        $("#NgayDiLam").kendoDatePicker({
            format: "dd/MM/yyyy"
        });
        $("#NgayHetHanHopDong").kendoDatePicker({
            format: "dd/MM/yyyy"
        });
        $("#NgayThoiViec").kendoDatePicker({
            format: "dd/MM/yyyy"
        });

        $("#TinhTrang").kendoDropDownList({
            dataSource: dataTinhTrang,
            dataTextField: "text",
            dataValueField: "value",
            autobind: true
        });
        $("#TenChucVu").kendoDropDownList({
            dataSource: getDataForDropdownlist("/ThongTinCaNhan/getDataChucVu"),
            dataTextField: "TenChucVu",
            dataValueField: "ChucVuId",
            autobind: true
        });
        var dataLoaiHopDong = [
            { text: GetTextLanguage("chuaky"), value: 0 },
            { text: GetTextLanguage("daihan"), value: 1 },
            { text: GetTextLanguage("thang1236"), value: 2 },
            { text: GetTextLanguage("thuviec"), value: 3 },
            { text: GetTextLanguage("muavu"), value: 4 }
        ]
        $("#LoaiHopDong").kendoDropDownList({
            dataSource: dataLoaiHopDong,
            dataTextField: "text",
            dataValueField: "value",
            autobind: true
        })
        showAvataImage();
    }
    //Lịch sử nhận lương
    function dataTabLichSuNhanLuong() {
        $("#lichsunhanluong").height(heightGrid - 150);
        $(".windows8").css("display", "block");
        $.ajax({
            url: "/ThongTinCaNhan/GetLichSuNhanLuong",
            dataType: 'json',
            async: true,
            data: {
                nam: $("#NamNhanLuong").val(),
                NhanVienID: $('#ThongTinNhanVienID').val()
            },
            success: function (result) {
                var viewModel = kendo.observable({
                    TongThuNhap: formatSo(result.data.TongThuNhap),
                    ThuNhapDaThanhToan: formatSo(result.data.ThuNhapDaThanhToan),
                    TongGiamTru: formatSo(result.data.TongGiamTru),
                    TongThueThuNhapCaNhanTheoNam: formatSo(result.data.TongThueThuNhapCaNhanTheoNam),
                    TongThueThuNhapCaNhanTheoThang: formatSo(result.data.TongThueThuNhapCaNhanTheoThang),
                    ChenhLech: formatSo(result.data.ChenhLech),
                    ThuNhapChuaDuocThanhToan: formatSo(result.data.ThuNhapChuaDuocThanhToan)
                });
                kendo.bind($("#ThongTinThuNhap"), viewModel);

                $("#gridLichSuDieuChinhLuong").kendoGrid({
                    dataSource: result.data.ListLichSuDieuChinhLuong,
                    sortable: true,
                    filterable: {
                        mode: "row"
                    },
                    dataBinding: function () {
                        record = 0;
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "#= ++record #",
                            width: 60,
                            attributes: alignCenter
                        },
                        {
                            field: "TuNgay",
                            title: GetTextLanguage("tungay"),
                            filterable: FilterInColumn,
                            attributes: alignCenter,
                            template: formatToDate("TuNgay"),
                            width: 150
                        },
                        {
                            field: "DenNgay",
                            title: GetTextLanguage("denngay"),
                            filterable: FilterInColumn,
                            attributes: alignCenter,
                            template: formatToDate("DenNgay"),
                            width: 150
                        },
                        {
                            field: "LuongCoBan",
                            title: GetTextLanguage("luongcoban"),
                            filterable: FilterInColumn,
                            attributes: {
                                style: "text-align:right;font-weight: bold;"
                            },
                            template: formatToMoney("LuongCoBan"),
                            width: 200
                        },
                        {
                            field: "LuongBHXH",
                            title: GetTextLanguage("luongbhxh"),
                            filterable: FilterInColumn,
                            attributes: {
                                style: "text-align:right;font-weight: bold;"
                            },
                            template: formatToMoney("LuongBHXH"),
                            width: 200
                        },
                        {
                            field: "LuongNgoaiGio",
                            title: GetTextLanguage("luongngoaigio"),
                            filterable: FilterInColumn,
                            attributes: {
                                style: "text-align:right;font-weight: bold;"
                            },
                            template: formatToMoney("LuongNgoaiGio"),
                            width: 200
                        },
                        {
                            field: "SoNguoiGTGC",
                            title: GetTextLanguage("songuoigtgc"),
                            filterable: FilterInColumn,
                            attributes: alignCenter,
                            template: formatToMoney("SoNguoiGTGC"),
                            width: 200
                        },
                        {
                            field: "HinhThucLuong",
                            title: GetTextLanguage("hinhthucluong"),
                            filterable: FilterInColumn,
                            width: 150
                        },
                        {
                            field: "TenNganHang",
                            title: GetTextLanguage("tennganhang"),
                            filterable: FilterInTextColumn,
                            width: 200
                        },
                        {
                            field: "SoTK",
                            title: GetTextLanguage("sotaikhoan"),
                            filterable: FilterInColumn,
                            width: 150
                        }
                    ]
                });
                $("#gridDanhSachThuTheoThang").kendoGrid({
                    dataSource: result.data.ListThuTheoThang,
                    sortable: true,
                    filterable: {
                        mode: "row"
                    },
                    dataBinding: function () {
                        record = 0;
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "#= ++record #",
                            width: 60,
                            attributes: alignCenter
                        },
                        {
                            field: "ThoiGian",
                            width: 150,
                            title: GetTextLanguage("thoigian"),
                            template: formatToDate("ThoiGian"),
                            filterable: FilterInColumn,
                            attributes: alignCenter
                        },
                        {
                            field: "LuongSanLuong",
                            title: GetTextLanguage("luongsanluong"),
                            width: 200,
                            filterable: FilterInColumn,
                            attributes: {
                                style: "text-align:right;font-weight: bold;"
                            },
                            template: formatToMoney("LuongSanLuong")
                        },
                        {
                            width: 100,
                            template: "<button class='btn btn-primary btnChiTiet'>" + GetTextLanguage('chitiet') + "</button>",
                            attributes: alignCenter
                        }
                    ]
                });
                $("#gridDanhSachThuTheoThang").on("click", ".btnChiTiet", function () {
                    var dataItem = $("#gridDanhSachThuTheoThang").data("kendoGrid").dataItem($(this).closest("tr"));
                    CreateModalWithSize("mdlLichSuDieuChinhLuong", "80%", "70%", null, GetTextLanguage("thongtinchitiet"));
                    console.log(dataItem);
                    $("#mdlLichSuDieuChinhLuong").load("/ThongTinCaNhan/ChiTietThuTheoThang?NhanVienID=" + dataItem.NhanVienID + "&NgayBatDauHD=" + dataItem.NgayBatDauHD + "&NgayDauThangDauTien=" + dataItem.NgayDauThangDauTien + "&NgayCuoiThangDauTien=" + dataItem.NgayCuoiThangDauTien + "&CheckTinhBHXH=" + dataItem.CheckTinhBHXH + "&CheckTinhThue=" + dataItem.CheckTinhBHXH + "&SoTienTinhThue=" + dataItem.SoTienTinhThue);
                });
                $("#gridLichSuNhanThanhToan").kendoGrid({
                    dataSource: result.data.ListLichSuNhanThanhToan,
                    sortable: true,
                    filterable: {
                        mode: "row"
                    },
                    dataBinding: function () {
                        record = 0;
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "#= ++record #",
                            width: 60,
                            attributes: alignCenter
                        },
                        {
                            field: "ThoiGian",
                            title: GetTextLanguage("thoigian"),
                            template: formatToDate("ThoiGian"),
                            filterable: FilterInColumn,
                            attributes: alignCenter,
                            width: 150
                        },
                        {
                            field: "NoiDung",
                            title: GetTextLanguage("noidung"),
                            filterable: FilterInTextColumn,
                            width: 200
                        },
                        {
                            field: "SoTienThanhToan",
                            title: GetTextLanguage("sotienthanhtoan"),
                            filterable: FilterInColumn,
                            attributes: {
                                style: "text-align:right;font-weight: bold;"
                            },
                            width: 200,
                            template: formatToMoney("SoTienThanhToan")
                        }
                    ]
                });
                $(".windows8").css("display", "none");
            },
            error: function (xhr, status, error) {
                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                $(".windows8").css("display", "none");
            }
        });
    }
    //Lịch Sử Làm Việc
    function dataTabLichSuLamViec() {
        $("#gridlichsulamviec").kendoGrid({
            dataSource: {
                batch: true,
                transport: {
                    read: {
                        url: "/ThongTinCaNhan/GetDataLichSuLamViec",
                        type: "get",
                        dataType: "json",
                        data: { NhanVienID: $('#ThongTinNhanVienID').val() },
                        contentType: "application/json; charset=utf-8",
                    }
                },
                sort: { field: "ThoiGianBatDau", dir: "desc" },
                pageSize: pageSize,
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        fields: {
                            ThoiGianBatDau: { type: "date" }
                        }
                    }
                }
            },
            height: heightGrid - 150,
            sortable: true,
            pageable: pageableAll,
            filterable: {
                mode: "row"
            },
            dataBinding: function () {
                record = 0;
            },
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "#= ++record #",
                    width: 60,
                    attributes: alignCenter
                },
                {
                    field: "ThoiGianBatDau",
                    title: GetTextLanguage("thoigianbatdau"),
                    filterable: FilterInColumn,
                    attributes: alignCenter,
                    format:"{0:dd/MM/yyyy}"
                },
                {
                    field: "DonVi",
                    title: GetTextLanguage("donvi"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft
                },
                {
                    field: "ChucVu",
                    title: GetTextLanguage("chucvu"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft
                },
                {
                    field: "MucLuong",
                    title: GetTextLanguage("mucluong"),
                    filterable: FilterInColumn,
                    template: "#=kendo.toString(MucLuong,'n0')#",
                    attributes: alignRight
                },
                {
                    field: "ThoiGianKetThuc",
                    title: GetTextLanguage("thoigianketthuc"),
                    filterable: FilterInColumn,
                    template: formatToDate("ThoiGianKetThuc"),
                    attributes: alignCenter
                }
            ]
        });
    }
    //Bằng Cấp Chứng Chỉ
    function dataTabBangCapChungChi() {
        $("#content_bangcapchungchi").html('<div id="gridbangcapchungchi"></div>');
        var dataimg = new FormData();
        var noedit = $(location).attr('pathname') === "/ThongTinCaNhan/Index";
        $("#gridbangcapchungchi").kendoGrid({
            dataSource: {
                batch: true,
                transport: {
                    read: {
                        url: "/ThongTinCaNhan/GetDataBangCapChungChi?NhanVienID=" + $('#ThongTinNhanVienID').val(),
                        type: "get",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                    },
                    destroy: {
                        url: "/ThongTinCaNhan/DeleteBangCapChungChi",
                        type: 'post',
                        dataType: 'json',
                        complete: function (e) {
                            altReturn(e.responseJSON);
                            $("#gridbangcapchungchi").data("kendoGrid").dataSource.read();
                        }
                    },
                    parameterMap: function (data, type) {
                        if (type !== "read") {
                            if (type == "destroy") {
                                return { BangCapChungChiID: data.models[0].BangCapChungChiID }
                            }
                        } else {
                        }
                    }
                },
                pageSize: pageSize,
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        id: "BangCapChungChiID"
                    }
                }
            },
            height: heightGrid - 150,
            sortable: true,
            pageable: pageableAll,
            filterable: {
                mode: "row"
            },
            toolbar: [
                {
                    template: '<div style="text-align:right"><button id="btnthemBangCap" class="k-button k-button-icontext k-grid-add"><span class="k-icon k-i-plus"></span>' + GetTextLanguage("them") + '</button></div>'
                }
            ],
            edit: function (e) {
                e.container.find(".k-edit-label:first").hide();
                e.container.find(".k-edit-field:first").hide();
            },
            messages: {
                commands: {
                    update: GetTextLanguage("capnhat"),
                    canceledit: GetTextLanguage("huy")
                }
            },
            editable: {
                mode: "popup",
                window: {
                    title: GetTextLanguage("thongtinchitiet"),
                    width: 450
                },
                confirmation: GetTextLanguage("xoabangi"),
                confirmDelete: "Yes",
                template: kendo.template($("#tempBangCapChungChi").html())
            },
            dataBinding: function () {
                record = 0;
            },
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "#= ++record #",
                    width: 60,
                    attributes: alignCenter
                },
                {
                    field: "TenBangCapChungChi",

                    title: GetTextLanguage("tenchungchi"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft
                },
                 {
                     command: [{ name: "destroy", text: GetTextLanguage("xoa") }],
                     title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                     width: 100,
                     attributes: alignCenter,
                     hidden: $(location).attr('pathname') === "/ThongTinCaNhan/Index" ? true : false,
                 }
                //{
                //    field: "TepDinhKem",
                //    title: GetTextLanguage("tepdinhkem"),
                //    filterable: FilterInTextColumn,
                //    attributes: alignCenter,
                //    template: "#=TepDinhKem#",
                //},
                //{
                //    title: GetTextLanguage("chitiet"),
                //    template: "<a style='cursor:pointer' class='chitiet'>" + GetTextLanguage("xemchitiet") + "</a>",
                //    width: 100
                //}
            ],
            edit: function (e) {
                e.container.find(".k-edit-label:first").hide();
                e.container.find(".k-edit-field:first").hide();
                if (noedit) {
                    $(".k-grid-update").addClass("hidden");
                }
                $("#frmbangcapchungchi #DinhKem").val(e.model.TepDinhKem);
                var DSHinhAnh = [];
                if (e.model.TepDinhKem != null && e.model.TepDinhKem != "") {
                    var strUrl = e.model.TepDinhKem;
                    for (var k = 0; k < strUrl.split(',').length; k++) {
                        var anh = strUrl.split(',')[k];
                        DSHinhAnh.push({ Url: strUrl.split(',')[k] });
                    }
                    if (DSHinhAnh.length > 0) {
                        a = loadslbuttonhopdong(DSHinhAnh);
                    }
                    $('#frmbangcapchungchi .dshinhanhadd').html(a);

                }
                $("#frmbangcapchungchi .dshinhanhadd .closeGenImgSuaChua").click(function () {
                    var file = $(this).data("file");
                    for (var i = 0; i < DSHinhAnh.length; i++) {
                        if (DSHinhAnh[i].Url === file) {
                            DSHinhAnh.splice(i, 1);
                            $("#frmbangcapchungchi #DinhKem").val(DSHinhAnh.join());
                            showslideimg();
                            break;
                        }
                    }
                    $(this).parent().remove();

                });
                function loadimgSuaChua2(input) {
                    //$('.dshinhanhadd').empty();
                    if (input.files) {
                        var filesAmount = input.files.length;
                        var html = "";
                        for (var i = 0; i < filesAmount; i++) {
                            if (input.files[i].type == "image/png" || input.files[i].type == "image/jpg" || input.files[i].type == "image/jpeg" || input.files[i].type == "image/gif") {
                                readURL2(input.files[i], input);

                            }
                        }

                    }
                }
                function readURL2(file, input) {

                    var reader = new FileReader();
                    reader.onload = function (event) {
                        var html = "";
                        html += "<div class='imgslider' style='float:left;position:relative;margin-right:10px;'>";
                        html += "<div style='background-image:url(\"" + event.target.result + "\"); margin:0px;' class='img-con ahihi3'><a rel='gallery' href='" + event.target.result + "'><img data-file='" + file.name + "' src='" + event.target.result + "' style='width: 100%;height: 100%;margin: 0;opacity: 0;'></a></div>";
                        html += "<div data-file='" + file.name + "' class='closeGenImgSuaChua' title='Xóa'>X</div></div>";
                        html += "</div>";
                        //html = "<div class='imgGenSuaChua' style=\"width:100px;height:100px;\"><img src='" + event.target.result + "' /><div data-file='" + file.name + "' class='closeGenImgSuaChua' title='Xóa'>X</div></div>";
                        $(input).parent().parent().find('.dshinhanhadd').append(html);
                        showslideimg();
                        $(".dshinhanhadd .closeGenImgSuaChua").click(function () {
                            $(this).parent().remove();
                            showslideimg();
                        });

                    }
                    reader.readAsDataURL(file);
                }
                showslideimg();
                $("#files_1").change(function () {
                    for (var x = 0; x < this.files.length; x++) {
                        dataimg.append("uploads", this.files[x]);
                    }
                    dataimg.getAll("uploads");
                    loadimgSuaChua2(this);
                    $("#files_1").val('').clone(true);
                });
            },
            save: function (e) {
                var hinhanh = "";
                var DSHinhAnh = [];
                $('#frmbangcapchungchi .dshinhanhadd .imgslider').each(function (index2, element2) {
                    if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                        DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                    }
                });
                var ins = dataimg.getAll("uploads").length;
                if (ins > 0) {
                    var strUrl = uploadMultipleFileParagamWidthFile("/Upload" + "/UploadVanBan", dataimg.getAll("uploads"), $(".dshinhanhadd")).split(',');
                    for (var k = 0; k < strUrl.length; k++) {
                        DSHinhAnh.push({ Url: strUrl[k] });
                    }
                }
                for (var i = 0; i < DSHinhAnh.length; i++) {
                    if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                        hinhanh += DSHinhAnh[i].Url + ",";
                    }
                }
                if (e.model.BangCapChungChiID == null || e.model.BangCapChungChiID == "") {
                    $.ajax({
                        url: "/ThongTinCaNhan/AddBangCapChungChi",
                        type: "POST",
                        data: {
                            TenBangCapChungChi: $("#TenBangCapChungChi").val(),
                            NhanVienID: $("#ThongTinNhanVienID").val(),
                            TepDinhKem: hinhanh.substring(0, hinhanh.length - 1)
                        },
                        complete: function (e) {
                            alertToastr(e.responseJSON);
                            $("#gridbangcapchungchi").data("kendoGrid").dataSource.read();
                        }
                    });
                } else {
                    $.ajax({
                        url: "/ThongTinCaNhan/UpdateBangCapChungChi",
                        type: "POST",
                        data: {
                            BangCapChungChiID: e.model.BangCapChungChiID,
                            TenBangCapChungChi: $("#TenBangCapChungChi").val(),
                            TepDinhKem: hinhanh.substring(0, hinhanh.length - 1)
                        },
                        complete: function (e) {
                            alertToastr(e.responseJSON);
                            $("#gridbangcapchungchi").data("kendoGrid").dataSource.read();
                        }
                    });
                }
            },
        });
        $("#gridbangcapchungchi .k-grid-content").on("dblclick", "td", function () {
            var rowct = $(this).closest("tr"),
                gridct = $("#gridbangcapchungchi").data("kendoGrid"),
                dataItemct = gridct.dataItem(rowct);
            if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                $("#gridbangcapchungchi").data("kendoGrid").editRow(rowct);
            }
        })
        if (noedit) {
            $("#btnthemBangCap").addClass("hidden");
        }
    }
    //Tài Sản Đang Quản Lý
    function dataTabTaiSanDangQuanLy() {
        $("#gridtaisandangquanly").kendoGrid({
            dataSource: {
                batch: true,
                transport: {
                    read: {
                        url: "/ThongTinCaNhan/GetDataTaiSanQuanLy",
                        type: "get",
                        dataType: "json",
                        data: { NhanVienID: $('#ThongTinNhanVienID').val() },
                        contentType: "application/json; charset=utf-8",
                    }
                },

                pageSize: pageSize,
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        id: "TaiSanID",
                        fields: {
                            TenTaiSan: { type: "string", editable: false },
                            MaTaiSan: { type: "string", editable: false },
                            NgayNhap: { type: "date", editable: false },
                            TrangThaiHienTai: { type: "string", editable: false }
                        }
                    }
                }
            },

            height: heightGrid - 150,
            sortable: true,
            pageable: pageableAll,
            filterable: {
                mode: "row"
            },
            dataBinding: function () {
                record = 0;
            },
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "#= ++record #",
                    width: 80,
                    filterable: false,
                    attributes: alignCenter
                },
                {
                    field: "TenTaiSan",
                    title: GetTextLanguage("tentaisan"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft,
                    width: 200,
                },
                {
                    field: "MaTaiSan",
                    title: GetTextLanguage("mataisan"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft,
                    width: 150,
                },
                {
                    field: "NgayNhap",
                    title: GetTextLanguage("ngaybangiao"),
                    format: "{0:dd/MM/yyyy}",
                    filterable: FilterInColumn,
                    attributes: alignCenter,
                    width: 150,
                },
                {
                    field: "TrangThaiHienTai",
                    title: GetTextLanguage("trangthai"),
                    width: 150,
                    attributes: alignCenter,
                    filterable: FilterInColumn,
                    template: "#= TrangThaiHienTai == '0' ? '" + GetTextLanguage("dangokho") + "' : (TrangThaiHienTai == '1' ? '" + GetTextLanguage("dabangiao") + "' : '" + GetTextLanguage("dathanhly") + "') #",
                },
            ]
        });

        $("#gridtaisandangquanly .k-grid-content").on("dblclick", "tr td", function () {
            rowct = $(this).closest("tr"),
                gridct = $("#gridtaisandangquanly").data("kendoGrid"),
                dataItemct = gridct.dataItem(rowct);
                var mdl = $('#mdlchitiettaisan').kendoWindow({
                    width: "600px",
                    height: "500px",
                    title: GetTextLanguage("chitiettaisan"),
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempchitiettaisan').html()));
                $("#NhomTaiSanID").kendoDropDownList({
                    filter: "contains",
                    dataTextField: "Text",
                    dataValueField: "Id",
                    optionLabel: GetTextLanguage("chonnhomtaisan"),
                    dataSource: getDataDroplit("/QuanLyTaiSan", "/LayNhomTaiSan")
                });
                $("#NgayNhap").kendoDatePicker({ format: "dd/MM/yyyy", value: new Date() });
                $("#DonGia").autoNumeric(autoNumericOptionsMoney);
                $.ajax({
                    cache: false,
                    async: false,
                    type: "POST",
                    url: "/QuanLyTaiSan" + "/LayChiTietTaiSan",
                    data: { TaiSanID: dataItemct.TaiSanID },
                    success: function (data) {
                        if (data.data != null) {
                            var viewModeldc = kendo.observable({
                                NgayNhap: data.data.NgayNhap,
                                NhomTaiSanID: data.data.NhomTaiSanID,
                                TenTaiSan: data.data.TenTaiSan,
                                MaTaiSan: data.data.MaTaiSan,
                                DonGia: data.data.DonGia,
                                NhanVienID: data.data.NhanVienID,
                            });
                            kendo.bind($("#frmTaiSan"), viewModeldc);
                            var str = ',';
                            if (data.data.Avatar != "" && data.data.Avatar != null) {
                                $('.imgavatar').attr('src', localQLTS + data.data.Avatar);
                            }
                        }

                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                    }
                });
        });
    }
    //Lịch Sử Đánh Giá
    function dataTabLichSuDanhGia() {
        $("#content_lichsudanhgia").html('<div id="gridlichsudanhgia"></div>');
        var noedit = $(location).attr('pathname') === "/ThongTinCaNhan/Index";
        $("#gridlichsudanhgia").kendoGrid({
            dataSource: {
                batch: true,
                transport: {
                    read: {
                        url: "/ThongTinCaNhan/GetDataLichSuDanhGia?NhanVienID=" + $('#ThongTinNhanVienID').val(),
                        type: "get",
                        dataType: "json",
                    },
                    destroy: {
                        url: "/ThongTinCaNhan" + "/XoaDanhGia",
                        type: 'post',
                        dataType: "json",
                        complete: function (e) {
                            altReturn(e.responseJSON);
                            $("#gridlichsudanhgia").data("kendoGrid").dataSource.read();
                        }
                    },
                    parameterMap: function (data, type) {
                        if (type !== "read") {
                            if (type == "create") {
                              
                            }
                            else if (type == "update") {
                             
                            }
                            else {
                                return { DanhGiaCongViecID: data.models[0].DanhGiaCongViecID }
                            }
                        }
                       
                    }
                },
                pageSize: pageSize,
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        id: "DanhGiaCongViecID",
                        fields: {
                            ThoiGian: { type: "date", editable: true },
                        }

                    }
                }
            },
            sort: [{ field: "ThoiGian", dir: "desc" }],
            height: innerHeight * 0.7,
            sortable: true,
            pageable: pageableAll,
            filterable: {
                mode: "row"
            },
            //toolbar: [{
            //    template: '<div style="text-align:right"><button id="btnthemDanhGia" class="k-button k-button-icontext k-grid-add"><span class="k-icon k-i-plus"></span>' + GetTextLanguage("them") + '</button></div>'
            //}
            //],
            columns: [
                {
                    field: "STT",
                    title: GetTextLanguage("stt"),
                    width: 60,
                    filterable: false,
                    attributes: alignCenter
                },
                {
                    field: "ThoiGian",
                    title: GetTextLanguage("thoigian"),
                    filterable: FilterInColumn,
                    attributes: alignCenter,
                    format:"{0:MM/yyyy}"
                },
                {
                    field: "DanhGia",
                    title: GetTextLanguage("danhgia"),
                    filterable: FilterInTextColumn,
                    attributes: alignCenter
                },
                {
                    field: "NhanXet",
                    title: GetTextLanguage("nhanxet"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft
                },
                {
                    field: "NguoiDanhGia",
                    title: GetTextLanguage("nguoidanhgia"),
                    filterable: FilterInTextColumn,
                },
                //{
                //    command: [{ name: "destroy", text: GetTextLanguage("xoa") }],
                //    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                //    width: 100,
                //    attributes: alignCenter,
                //    hidden: $(location).attr('pathname') === "/ThongTinCaNhan/Index" ? true : false,
                //}
            ],
            editable: {
            mode: "popup",
            window: {
                title: GetTextLanguage("thongtinchitiet"),
                width: 450
            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes",
            template: kendo.template($("#tempDanhGia").html())
            },
            edit: function (e) {
                $("#ThoiGian").kendoDatePicker({ start: "year", depth: "year", format: "MM/yyyy", value: new Date() });
                $("#HeSoID").kendoDropDownList({
                    filter: "contains",
                    dataTextField: "TenHeSo",
                    dataValueField: "HeSoID",
                    optionLabel: GetTextLanguage("danhgia"),
                    dataSource: getDataDroplit("/ThongTinCaNhan", "/LayDanhSachHeSo")
                });
                if (noedit) {
                    $(".k-grid-update").addClass("hidden");
                }
            },
            save: function (e) {
                var model = {
                        DanhGiaCongViecID: e.model.DanhGiaCongViecID != null && e.model.DanhGiaCongViecID !="" ? e.model.DanhGiaCongViecID : "",
                        ThoiGian: $("#ThoiGian").val(),
                        NhanVienID: $('#ThongTinNhanVienID').val(),
                        NhanXet: $("#NhanXet").val(),
                        HeSoID: $("#HeSoID").val(),
                    }
                    $.ajax({
                        cache: false,
                        async: false,
                        type: "POST",
                        url: "/ThongTinCaNhan" + "/ThemSuaDanhGia",
                        data: model,
                        success: function (data) {
                            altReturn(data);
                            gridReload("gridlichsudanhgia", {});
                        },
                        error: function (xhr, status, error) {
                            gridReload("gridlichsudanhgia", {});
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("coloi"));
                        }
                    });
            },
        });
        $("#gridlichsudanhgia .k-grid-content").on("dblclick", "td", function () {
            var rowct = $(this).closest("tr");
            if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                $("#gridlichsudanhgia").data("kendoGrid").editRow(rowct);
            }
        });
        if (noedit) {
            $("#btnthemDanhGia").addClass("hidden");
        }
    }
    $("#NamNhanLuong").change(function () {
        dataTabLichSuNhanLuong();
    });
    // Hồ sơ cá nhân
    function dataTabHopDongCaNhan() {
        $("#content_hosocanhan").html('<div id="gridhosocanhan"></div>');
        var dataimg = new FormData();
        var noedit = $(location).attr('pathname') === "/ThongTinCaNhan/Index";
        var hopdongsource = {
            batch: true,
            transport: {
                read: {
                    url: "/QLHopDong/LayDanhSachHopDongQuanLyNhanVien?LoaiHopDong=1&DonViLienQuan=" + $('#ThongTinNhanVienID').val(),
                    type: "post",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                },
                create: {
                    url: "/QLHopDong/ThemSuaHopDong",
                    type: 'post',
                    dataType: 'json',
                    complete: function (e) {
                        altReturn(e.responseJSON);
                        $("#gridhosocanhan").data("kendoGrid").dataSource.read();
                    }
                },
                destroy: {
                    url: "/QLHopDong/XoaHopDong",
                    type: 'post',
                    dataType: 'json',
                    complete: function (e) {
                        altReturn(e.responseJSON);
                        $("#gridhosocanhan").data("kendoGrid").dataSource.read();
                    }
                },
                parameterMap: function (data, type) {
                    if (type !== "read") {
                        var DSHinhAnh = [];
                        if (type == "create") {
                            var hinhanh = "";
                            $('.dshinhanhadd .imgslider').each(function (index2, element2) {
                                if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                                    DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                                }
                            });
                            var ins = dataimg.getAll("uploads").length;
                            if (ins > 0) {
                                var strUrl = uploadMultipleFileParagamWidthFile("/Upload" + "/UploadVanBan", dataimg.getAll("uploads"), $(".dshinhanhadd")).split(',');
                                for (var k = 0; k < strUrl.length; k++) {
                                    DSHinhAnh.push({ Url: strUrl[k] });
                                }
                            }
                            for (var i = 0; i < DSHinhAnh.length; i++) {
                                if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                                    hinhanh += DSHinhAnh[i].Url + ",";
                                }
                            }
                            var model = {
                                HopDongID: "",
                                DinhKem: hinhanh.substring(0, hinhanh.length - 1),
                                DonViLienQuan: $('#ThongTinNhanVienID').val(),
                                LoaiHinh: 1,
                                TenHopDong: data.models[0].TenHopDong,
                                LoaiHopDong: 1,
                            }
                            return { data: kendo.stringify(model) }
                        }
                        else if (type == "destroy") {
                            return { HopDongID: data.models[0].HopDongID }
                        }
                    }
                }

            },
            pageSize: pageSize,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "HopDongID",
                    fields: {
                        ThoiGianTao: { type: "date", editable: false },
                        TenHopDong: { type: "string", editable: true },
                        //NgayBatDau: { type: "date", editable: true },
                        //NgayKetThuc:{type:"date",editable:true}
                    }

                }
            }
        };
        $("#gridhosocanhan").kendoGrid({
            dataSource: hopdongsource,
            height: innerHeight * 0.7,
            sortable: true,
            pageable: pageableAll,
            filterable: {
                mode: "row"
            },
            editable: {
                mode: "popup",
                window: {
                    title: GetTextLanguage("thongtinchitiet"),
                    width: 450
                },
                confirmation: GetTextLanguage("xoabangi"),
                confirmDelete: "Yes",
                template: kendo.template($("#tempHopDong").html())
            },
            toolbar: [{
                template: '<div style="text-align:right"><button id="btnthemHopDong" class="k-button k-button-icontext k-grid-add"><span class="k-icon k-i-plus"></span>' + GetTextLanguage("them") + '</button></div>'
            }
            ],
            edit: function (e) {
                e.container.find(".k-edit-label:first").hide();
                e.container.find(".k-edit-field:first").hide();
                if (noedit) {
                    $(".k-grid-update").addClass("hidden");
                }
                $("#DinhKem").val(e.model.DinhKem);
                var DSHinhAnh = [];
                if (e.model.DinhKem != null && e.model.DinhKem != "") {
                    var strUrl = e.model.DinhKem;
                    for (var k = 0; k < strUrl.split(',').length; k++) {
                        var anh = strUrl.split(',')[k];
                        DSHinhAnh.push({ Url: strUrl.split(',')[k] });
                    }
                    a = loadslbuttonhopdong(DSHinhAnh);
                    $('.dshinhanhadd').html(a);

                }
                $(".dshinhanhadd .closeGenImgSuaChua").click(function () {
                    var file = $(this).data("file");
                    for (var i = 0; i < DSHinhAnh.length; i++) {
                        if (DSHinhAnh[i].Url === file) {
                            DSHinhAnh.splice(i, 1);
                            $("#DinhKem").val(DSHinhAnh.join());
                            showslideimg();
                            break;
                        }
                    }
                    $(this).parent().remove();

                });
                function loadimgSuaChua2(input) {
                    //$('.dshinhanhadd').empty();
                    if (input.files) {
                        var filesAmount = input.files.length;
                        var html = "";
                        for (var i = 0; i < filesAmount; i++) {
                            if (input.files[i].type == "image/png" || input.files[i].type == "image/jpg" || input.files[i].type == "image/jpeg" || input.files[i].type == "image/gif") {
                                readURL2(input.files[i], input);

                            }
                        }

                    }
                }
                function readURL2(file, input) {

                    var reader = new FileReader();
                    reader.onload = function (event) {
                        var html = "";
                        html += "<div class='imgslider' style='float:left;position:relative;margin-right:10px;'>";
                        html += "<div style='background-image:url(\"" + event.target.result + "\"); margin:0px;' class='img-con ahihi3'><a rel='gallery' href='" + event.target.result + "'><img data-file='" + file.name + "' src='" + event.target.result + "' style='width: 100%;height: 100%;margin: 0;opacity: 0;'></a></div>";
                        html += "<div data-file='" + file.name + "' class='closeGenImgSuaChua' title='Xóa'>X</div></div>";
                        html += "</div>";
                        //html = "<div class='imgGenSuaChua' style=\"width:100px;height:100px;\"><img src='" + event.target.result + "' /><div data-file='" + file.name + "' class='closeGenImgSuaChua' title='Xóa'>X</div></div>";
                        $(input).parent().parent().find('.dshinhanhadd').append(html);
                        showslideimg();
                        $(".dshinhanhadd .closeGenImgSuaChua").click(function () {
                            $(this).parent().remove();
                            showslideimg();
                        });

                    }
                    reader.readAsDataURL(file);
                }
                showslideimg();
                $("#files_0").change(function () {
                    for (var x = 0; x < this.files.length; x++) {
                        dataimg.append("uploads", this.files[x]);
                    }
                    dataimg.getAll("uploads");
                    loadimgSuaChua2(this);
                    $("#files_0").val('').clone(true);
                });
            },
            save: function (e) {
                if (e.model.HopDongID != null && e.model.HopDongID != "") {
                    var hinhanh = "";
                    var DSHinhAnh = [];
                    $('.dshinhanhadd .imgslider').each(function (index2, element2) {
                        if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                            DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                        }
                    });
                    var ins = dataimg.getAll("uploads").length;
                    if (ins > 0) {
                        var strUrl = uploadMultipleFileParagamWidthFile("/Upload" + "/UploadVanBan", dataimg.getAll("uploads"), $(".dshinhanhadd")).split(',');
                        for (var k = 0; k < strUrl.length; k++) {
                            DSHinhAnh.push({ Url: strUrl[k] });
                        }
                    }
                    for (var i = 0; i < DSHinhAnh.length; i++) {
                        if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                            hinhanh += DSHinhAnh[i].Url + ",";
                        }
                    }
                    var model = {
                        HopDongID: e.model.HopDongID,
                        DinhKem: hinhanh.substring(0, hinhanh.length - 1),
                        DonViLienQuan: $('#ThongTinNhanVienID').val(),
                        LoaiHinh: 1,
                        LoaiHopDong: 1,
                        TenHopDong: e.model.TenHopDong,
                    }
                    $.ajax({
                        cache: false,
                        async: false,
                        type: "POST",
                        url: "/QLHopDong" + "/ThemSuaHopDong",
                        data: {
                            data: JSON.stringify(model)
                        },
                        success: function (data) {
                            altReturn(data);
                            if (data.code == "success") {
                                gridReload("gridhosocanhan", {});
                                $(".windows8").css("display", "none");
                            } else {
                                for (var k = 0; k < ins; k++) {
                                    DSHinhAnh.splice((DSHinhAnh.length - 1 - k), 1);
                                }
                                $(".windows8").css("display", "none");
                            }

                        },
                        error: function (xhr, status, error) {
                            for (var k = 0; k < ins; k++) {
                                DSHinhAnh.splice((DSHinhAnh.length - 1 - k), 1);
                            }
                            gridReload("gridhosocanhan", {});
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("coloi"));
                        }
                    });
                }
            },
            messages: {
                commands: {
                    update: GetTextLanguage("capnhat"),
                    canceledit: GetTextLanguage("huy")
                }
            },
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "<span class='stt'></span>",
                    width: 60,
                    filterable: false
                },
                {
                    field: "TenHopDong",
                    title: GetTextLanguage("tenhopdong"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft
                },
                {
                    field: "TenCongTrinh",
                    title: GetTextLanguage("donvikhoitao"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft
                },
                {
                    field: "ThoiGianTao",
                    title: GetTextLanguage("thoigiantao"),
                    format: "{0:dd/MM/yyyy}",
                    filterable: FilterInColumn,
                },
                {
                    command: [{ name: "destroy", text: GetTextLanguage("xoa") }],
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                    width: 100,
                    attributes: alignCenter,
                    hidden: $(location).attr('pathname') === "/ThongTinCaNhan/Index" ? true : false,
                }
            ],
            dataBound: function () {
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            }
        });
        $("#gridhosocanhan .k-grid-content").on("dblclick", "td", function () {
            var rowct = $(this).closest("tr"),

                gridct = $("#gridhosocanhan").data("kendoGrid"),
                dataItemct = gridct.dataItem(rowct);
            if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                $("#gridhosocanhan").data("kendoGrid").editRow(rowct);
            }
        })
        if (noedit) {
            $("#btnthemHopDong").addClass("hidden");
        }

    }
    function dataYKienPhanHoi() {
        $("#tabstripclone").kendoTabStrip({
            animation: {
                open: {
                    effects: "fadeIn"
                }
            }
        });
        var grid2 = $("#griddagui").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: crudServiceBaseUrl + "/LayDanhSachYkien",
                        dataType: "json",
                        type: "POST",
                        data: { loai: 1 },
                        contentType: "application/json; charset=utf-8",
                    },
                    parameterMap: function (data, type) {
                        if (type !== "read") {
                            return { data: kendo.stringify(data.models[0]) }
                        } else {
                            return JSON.stringify(data);
                        }
                    }
                },
                type: "json",
                batch: true,
                pageSize: 50,
                schema: {
                    type: 'json',
                    data: 'data',
                    total: "total",
                    model: {
                        id: "YKienID",
                        fields: {
                            ThoiGian: { type: "date" },
                        }
                    }
                },
            }),
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: heightGrid - 150,
            toolbar: [
                { template: '<div style="text-align:right"><button class="btn btn-warning" id="btnThem">' + GetTextLanguage("guigopy") + '</button></div></div>' },
            ],
            dataBinding: function () {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "<span class='stt'></span>",
                    width: 60,
                    attribute: alignCenter
                },
                {
                    template: '<a class="xemchitiet" href="javascript:void(0)">(#:TongPhanHoi#) ' + GetTextLanguage("phanhoi") + '</a>',
                    width: 150,
                    attributes: alignCenter
                },
                {
                    field: "ThoiGian",
                    title: GetTextLanguage("thoigian"),
                    type: "date",
                    format: "{0:dd/MM/yyyy}",
                    filterable: FilterInColumn,
                    width: 150
                },
                {
                    field: "TieuDe",
                    title: GetTextLanguage("tieude"),
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    width: 200
                },
                {
                    field: "NoiDung", title: GetTextLanguage("noidung"),
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    width: 250
                },
            ],
            dataBound: function () {
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            }

        });
        var grid = $("#griddanhan").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: crudServiceBaseUrl + "/LayDanhSachYkien",
                        dataType: "json",
                        type: "POST",
                        data: { loai: 2 },
                        contentType: "application/json; charset=utf-8",
                    },
                    parameterMap: function (data, type) {
                        if (type !== "read") {
                            return { data: kendo.stringify(data.models[0]) }
                        } else {
                            return JSON.stringify(data);
                        }
                    }
                },
                type: "json",
                batch: true,
                pageSize: 50,
                schema: {
                    type: 'json',
                    data: 'data',
                    total: "total",
                    model: {
                        id: "YKienID",
                        fields: {
                            ThoiGian: { type: "date" },
                        }
                    }
                },
            }),
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            height: heightGrid - 150,
            dataBinding: function () {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "<span class='stt'></span>",
                    width: 60,
                    attribute: alignCenter
                },
                {
                    width: 150, align: "center", template: '<a class="xemchitiet" href="javascript:void(0)">(#:TongPhanHoi#) ' + GetTextLanguage("phanhoi") + '</a>'
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"), type: "date", format: "{0:dd/MM/yyyy}", filterable: FilterInColumn,
                    width: 150
                },
                {
                    field: "TieuDe", title: GetTextLanguage("tieude"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn,
                    width: 200
                },

                {
                    field: "NoiDung", title: GetTextLanguage("noidung"),
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    width: 250
                },
            ],
            dataBound: function () {
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            }

        });
        $("#griddagui").on("click", "#btnThem", function () {
            var DSNhanVien = [];
            var checkedIds = [];
            var ListNguoiNhan = [];
            var mdl2 = $('#mdlthemgopy').kendoWindow({
                width: "60%",
                height: "90%",
                title: GetTextLanguage("themdonggop"),
                modal: true,
                visible: false,
                resizable: false,
                actions: [
                    "Close"
                ],
                deactivate: function () {
                    $(this.element).empty();
                }
            }).data("kendoWindow").center().open();
            mdl2.content(kendo.template($('#tempthemgopy').html()));
            var grid2 = $("#gridnguoinhan").kendoGrid({
                dataSource: {
                    data: ListNguoiNhan,
                    schema: {
                        model: {
                            id: "NhanVienID",
                            fields: {
                                AnhDaiDienHoTen: { type: "string", editable: false },
                                GioiTinh: { type: "string", editable: false },
                                NgaySinh: { type: "date", editable: false },
                                SDT: { type: "string", editable: false },
                                DiaChi: { type: "string", editable: false },
                            }
                        }
                    },
                },
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    buttonCount: 5,
                    pageSizes: true,
                    messages: messagegrid
                },
                toolbar: [
                    { template: '<div style="text-align:right"><button class="btn btn-warning" id="btnThemNguoiNhan">' + GetTextLanguage("themnguoinhan") + '</button></div></div>' },
                ],
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                columns: [
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                        template: "<span class='stt'></span>",
                        width: 50,
                        attribute: alignCenter
                    },
                    { template: '<div style="text-align:center"><button class="btn btn-xs btn-warning xemnhanvien">' + GetTextLanguage("chitiet") + '</button></div></div>' },
                    {
                        field: "AnhDaiDienHoTen", title: GetTextLanguage("tennguoinhan"), filterable: FilterInTextColumn
                    },
                    {
                        field: "ChucVu", title: GetTextLanguage("chucvu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn
                    },

                    {
                        field: "GioiTinh", title: GetTextLanguage("gioitinh"),
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn,
                    },
                    {
                        field: "NgaySinh", title: GetTextLanguage("ngaysinh"), type: "date", format: "{0:dd/MM/yyyy}",
                        filterable: FilterInColumn,
                    },
                    {
                        field: "SDT", title: GetTextLanguage("sdt"),
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "DiaChi", title: GetTextLanguage("diachi"),
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn
                    },
                ],
                dataBound: function () {
                    var rows = this.items();
                    dem = 0;
                    $(rows).each(function () {
                        dem++;
                        var rowLabel = $(this).find(".stt");
                        $(rowLabel).html(dem);
                        if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                            dem = 0;
                        }
                    });
                }

            });
            $("#gridnguoinhan").on("click", ".xemnhanvien", function () {
                //alert();
                var data = $("#gridnguoinhan").data("kendoGrid").dataItem($(this).closest("tr"));
                $(".mdlThongTinNhanVien").kendoWindow({
                    modal: true,
                    width: '60%',
                    height: '60%',
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                })
                var popup = $(".mdlThongTinNhanVien").data("kendoWindow");
                popup.open();
                popup.center();
                popup.title(GetTextLanguage("thongtin"));
                $(".mdlThongTinNhanVien").load("/TraCuuDanhBa/ThongTinCaNhan?NhanVienID=" + data.NhanVienID);
            })
            $("#gridnguoinhan").on("click", "#btnThemNguoiNhan", function () {
                var mdl2 = $('#mdlthemnguoinhan').kendoWindow({
                    width: "60%",
                    height: "90%",
                    title: GetTextLanguage("danhsachnhanvien"),
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl2.content(kendo.template($('#tempthemnguoinhan').html()));
                var grid3 = $("#griddanhsachnhanvien").kendoGrid({
                    dataSource: new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: crudServiceBaseUrl + "/LayDanhSachNhanVien",
                                dataType: "json",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                            },
                            parameterMap: function (data, type) {
                                if (type !== "read") {
                                    return { data: kendo.stringify(data.models[0]) }
                                } else {
                                    return JSON.stringify(data);
                                }
                            }
                        },
                        type: "json",
                        batch: true,
                        pageSize: 50,
                        schema: {
                            type: 'json',
                            data: 'data',
                            total: "total",
                            model: {
                                id: "NhanVienID",
                                fields: {
                                    NgaySinh: { type: "date" }
                                }
                            }
                        },
                    }),
                    filterable: {
                        mode: "row"
                    },
                    sortable: true,
                    resizable: true,
                    pageable: {
                        refresh: true,
                        buttonCount: 5,
                        pageSizes: true,
                        messages: messagegrid
                    },
                    height: 700,
                    dataBinding: function () {
                        record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 50,
                            attribute: alignCenter
                        },
                        { template: '<div style="text-align:center" class="noclick"><button class="btn btn-xs btn-warning xemnhanvien">' + GetTextLanguage("chitiet") + '</button></div></div>' },
                        {
                            field: "AnhDaiDienHoTen", title: GetTextLanguage("tennguoinhan"), filterable: FilterInTextColumn
                        },
                        {
                            field: "ChucVu", title: GetTextLanguage("chucvu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn
                        },

                        {
                            field: "GioiTinh", title: GetTextLanguage("gioitinh"),
                            attributes: { style: "text-align:left;" },
                            filterable: FilterInTextColumn
                        },
                        {
                            field: "NgaySinh", type: "date", format: "{0:dd/MM/yyyy}", title: GetTextLanguage("ngaysinh"),
                            filterable: FilterInColumn
                        },
                        {
                            field: "SDT", title: GetTextLanguage("sdt"),
                            attributes: { style: "text-align:left;" },
                            filterable: FilterInTextColumn
                        },
                        {
                            field: "DiaChi", title: GetTextLanguage("diachi"),
                            attributes: { style: "text-align:left;" },
                            filterable: FilterInTextColumn
                        },
                    ],
                    dataBound: onDataBound
                });
                $("#griddanhsachnhanvien").on("click", ".xemnhanvien", function () {
                    //alert();
                    var data = $("#griddanhsachnhanvien").data("kendoGrid").dataItem($(this).closest("tr"));
                    $(".mdlThongTinNhanVien").kendoWindow({
                        modal: true,
                        width: '60%',
                        height: '60%',
                        visible: false,
                        resizable: false,
                        actions: [
                            "Close"
                        ],
                        deactivate: function () {
                            $(this.element).empty();
                        }
                    })
                    var popup = $(".mdlThongTinNhanVien").data("kendoWindow");
                    popup.open();
                    popup.center();
                    popup.title(GetTextLanguage("thongtin"));
                    $(".mdlThongTinNhanVien").load("/TraCuuDanhBa/ThongTinCaNhan?NhanVienID=" + data.NhanVienID);
                });
                $("#griddanhsachnhanvien").on("click", "td", selectRowSoLuong);
                function selectRowSoLuong() {
                    if ($(this).find(".noclick").length > 0) {

                    } else {
                        var rows = $(this).closest("tr");
                        grids = $("#griddanhsachnhanvien").data("kendoGrid"),
                            dataItems = grids.dataItem(rows);
                        if (rows.hasClass('k-state-selected')) {
                            rows.removeClass("k-state-selected");
                            checkedIds[dataItems.NhanVienID] = false;
                            DSNhanVien[dataItems.NhanVienID] = null;
                        } else {
                            rows.addClass("k-state-selected");
                            checkedIds[dataItems.NhanVienID] = true;
                            DSNhanVien[dataItems.NhanVienID] = dataItems;
                        }
                    }
                }
                function onDataBound(e) {
                    var rows = this.items();
                    dem = 0;
                    $(rows).each(function () {
                        dem++;
                        var rowLabel = $(this).find(".stt");
                        $(rowLabel).html(dem);
                        if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                            dem = 0;
                        }
                    });
                    var view = this.dataSource.data();
                    for (var i = 0; i < view.length; i++) {
                        if (view[i].Selected || checkedIds[view[i].NhanVienID]) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .addClass("k-state-selected");
                            view[i].Selected = true;
                            DSNhanVien[dataItems.NhanVienID] = view[i];
                        }
                    }
                }
                $("#btnThemDanhSachNguoiNhan").click(function () {
                    for (var i = ListNguoiNhan.length - 1; i >= 0; i--) {
                        ListNguoiNhan.splice(i, 1);
                    }
                    var grid4 = $("#griddanhsachnhanvien").data("kendoGrid");
                    for (var i = 0; i < grid4.dataSource.data().length; i++) {
                        if (DSNhanVien[grid4.dataSource.data()[i].NhanVienID] != null) {
                            var selectedItem = grid4.dataSource.data()[i];
                            var obj = {};
                            obj.NhanVienID = selectedItem.NhanVienID;
                            obj.AnhDaiDienHoTen = selectedItem.AnhDaiDienHoTen;
                            obj.GioiTinh = selectedItem.GioiTinh;
                            obj.NgaySinh = selectedItem.NgaySinh;
                            obj.SDT = selectedItem.SDT;
                            obj.ChucVu = selectedItem.ChucVu;
                            obj.DiaChi = selectedItem.DiaChi;
                            ListNguoiNhan.push(obj);
                        }
                    }
                    $('#gridnguoinhan').data('kendoGrid').dataSource.read();
                    $('#gridnguoinhan').data('kendoGrid').refresh();
                    closeModel("mdlthemnguoinhan");
                });

            })
            var validatordc = $("#frmthemgopy").kendoValidator().data("kendoValidator");
            $("#btnThemYKien").click(function () {
                var DSHinhAnh = [];
                if (ListNguoiNhan.length == 0) {
                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("chonnguoinhantruockhigui"));
                    return false;
                }
                if (validatordc.validate()) {
                    var hinhanh = "";
                    var ins = document.getElementById('files_xs').files.length;
                    if (ins > 0) {
                        var strUrl = uploadMultipleFileParagam(crudServiceBaseUrlUploadFile + "/UploadfilesBinhLuan", "files_xs", $(".dshinhanhadd")).split(',');
                        for (var k = 0; k < strUrl.length; k++) {
                            DSHinhAnh.push({ Url: strUrl[k] });
                        }
                    }
                    for (var i = 0; i < DSHinhAnh.length; i++) {
                        if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                            hinhanh += DSHinhAnh[i].Url + ",";
                        }
                    }
                    var dataobject = {};
                    dataobject.DSHinhAnh = hinhanh.substring(0, hinhanh.length - 1);
                    dataobject.TieuDe = $("#TieuDe").val();
                    dataobject.NoiDung = $("#NoiDung").val();
                    dataobject.ListNguoiNhan = [];
                    for (var i = 0; i < ListNguoiNhan.length; i++) {
                        dataobject.ListNguoiNhan.push(ListNguoiNhan[i].NhanVienID);
                    }
                    $.ajax({
                        cache: false,
                        async: false,
                        type: "POST",
                        url: crudServiceBaseUrl + "/ThemGopY",
                        data: { data: JSON.stringify(dataobject) },
                        success: function (data) {
                            altReturn(data);
                            if (data.code == "success") {
                                gridReload("griddagui", { loai: 1 });
                                closeModel("mdlthemgopy");
                            } else {
                                for (var k = 0; k < ins; k++) {
                                    DSHinhAnh.splice((DSHinhAnh.length - 1 - k), 1);
                                }
                            }

                        },
                        error: function (xhr, status, error) {
                            for (var k = 0; k < ins; k++) {
                                DSHinhAnh.splice((DSHinhAnh.length - 1 - k), 1);
                            }
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        }
                    });
                }
            });
        });
        $("#griddagui").on("click", ".xemchitiet", function () {
            var rowct = $(this).closest("tr"),
                gridct = $("#griddagui").data("kendoGrid"),
                dataItemct = gridct.dataItem(rowct);
            var mdl2 = $('#mdlxemchitiet').kendoWindow({
                width: "60%",
                height: "90%",
                title: GetTextLanguage("chitietykiendonggop"),
                modal: true,
                visible: false,
                resizable: false,
                actions: [
                    "Close"
                ],
                deactivate: function () {
                    $(this.element).empty();
                }
            }).data("kendoWindow").center().open();
            mdl2.content(kendo.template($('#tempxemchitiet').html()));
            $(".txttieude").text(dataItemct.TieuDe);
            $(".txtnoidung").val(dataItemct.NoiDung);
            var grid2 = $("#griddanhsachnhanvien").kendoGrid({
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: crudServiceBaseUrl + "/LayDanhSachNguoiNhan",
                            dataType: "json",
                            type: "POST",
                            data: { YKienID: dataItemct.YKienID, loai: 1 },
                            contentType: "application/json; charset=utf-8",
                        },
                        parameterMap: function (data, type) {
                            if (type !== "read") {
                                return { data: kendo.stringify(data.models[0]) }
                            } else {
                                return JSON.stringify(data);
                            }
                        }
                    },
                    type: "json",
                    batch: true,
                    pageSize: 50,
                    schema: {
                        type: 'json',
                        data: 'data',
                        total: "total",
                        model: {
                            id: "NhanVienID",
                            fields: {
                                NgaySinh: { type: "date" },
                            }
                        }
                    },
                }),
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    buttonCount: 5,
                    pageSizes: true,
                    messages: messagegrid
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                columns: [
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                        template: "<span class='stt'></span>",
                        width: 60,
                        attribute: alignCenter
                    },
                    {
                        template: '<div style="text-align:center"><button class="btn btn-xs btn-warning xemnhanvien">' + GetTextLanguage("chitiet") + '</button></div></div>',
                        width: 100
                    },
                    {
                        field: "AnhDaiDienHoTen", title: GetTextLanguage("tennguoinhan"), filterable: FilterInTextColumn,
                        width: 150
                    },
                    {
                        field: "ChucVu", title: GetTextLanguage("chucvu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn,
                        width: 150
                    },

                    {
                        field: "GioiTinh", title: GetTextLanguage("gioitinh"),
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn,
                        width: 100
                    },
                    {
                        field: "NgaySinh", title: GetTextLanguage("ngaysinh"), type: "date", format: "{0:dd/MM/yyyy}",
                        filterable: FilterInColumn,
                        width: 150
                    },
                    {
                        field: "SDT", title: GetTextLanguage("sdt"),
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn,
                        width: 150
                    },
                    {
                        field: "DiaChi", title: GetTextLanguage("diachi"),
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn,
                        width: 150
                    }
                ],
                dataBound: function () {
                    var rows = this.items();
                    dem = 0;
                    $(rows).each(function () {
                        dem++;
                        var rowLabel = $(this).find(".stt");
                        $(rowLabel).html(dem);
                        if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                            dem = 0;
                        }
                    });
                }
            });
            $("#griddanhsachnhanvien").on("click", ".xemnhanvien", function () {
                //alert();
                var data = $("#griddanhsachnhanvien").data("kendoGrid").dataItem($(this).closest("tr"));
                $(".mdlThongTinNhanVien").kendoWindow({
                    modal: true,
                    width: '60%',
                    height: '60%',
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                })
                var popup = $(".mdlThongTinNhanVien").data("kendoWindow");
                popup.open();
                popup.center();
                popup.title(GetTextLanguage("thongtin"));
                $(".mdlThongTinNhanVien").load("/TraCuuDanhBa/ThongTinCaNhan?NhanVienID=" + data.NhanVienID);
            });
            $.ajax({
                cache: false,
                async: false,
                type: "POST",
                url: "/YKienDongGop" + "/LayChiTiet",
                data: { YKienID: dataItemct.YKienID },
                success: function (data) {
                    var DSHinhAnh = [];
                    var a = '';
                    if (data.data.DSHinhAnh != null && data.data.DSHinhAnh.indexOf(',') !== -1) {
                        var strUrl = data.data.DSHinhAnh.split(',');
                        for (var k = 0; k < strUrl.length; k++) {
                            var anh = strUrl[k];
                            DSHinhAnh.push({ Url: strUrl[k] });
                        }
                        a = loadshinhanh(DSHinhAnh);
                        $('.dshinhanhadd2').html(a);

                    }
                    else if (data.data.DSHinhAnh != null && data.data.DSHinhAnh != "") {
                        DSHinhAnh.push({ Url: data.data.DSHinhAnh });
                        $('.dshinhanhadd2').html(loadshinhanh(DSHinhAnh));
                    }
                    showslideimg();

                },
                error: function (xhr, status, error) {

                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                }
            });
            Laydanhsachcomment(dataItemct.YKienID);
        });
        $("#griddanhan").on("click", ".xemchitiet", function () {
            var rowct = $(this).closest("tr"),
                gridct = $("#griddanhan").data("kendoGrid"),
                dataItemct = gridct.dataItem(rowct);
            var mdl2 = $('#mdlxemchitiet').kendoWindow({
                width: "60%",
                height: "90%",
                title: GetTextLanguage("chitietykiendonggop"),
                modal: true,
                visible: false,
                resizable: false,
                actions: [
                    "Close"
                ],
                deactivate: function () {
                    $(this.element).empty();
                }
            }).data("kendoWindow").center().open();
            mdl2.content(kendo.template($('#tempxemchitiet').html()));
            $(".txttieude").text(dataItemct.TieuDe);
            $(".txtnoidung").val(dataItemct.NoiDung);
            var grid2 = $("#griddanhsachnhanvien").kendoGrid({
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: crudServiceBaseUrl + "/LayDanhSachNguoiNhan",
                            dataType: "json",
                            type: "POST",
                            data: { YKienID: dataItemct.YKienID, loai: 2 },
                            contentType: "application/json; charset=utf-8",
                        },
                        parameterMap: function (data, type) {
                            if (type !== "read") {
                                return { data: kendo.stringify(data.models[0]) }
                            } else {
                                return JSON.stringify(data);
                            }
                        }
                    },
                    type: "json",
                    batch: true,
                    pageSize: 50,
                    schema: {
                        type: 'json',
                        data: 'data',
                        total: "total",
                        model: {
                            id: "NhanVienID",
                            fields: {
                                NgaySinh: { type: "date" },
                            }
                        }
                    },
                }),
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                height: 140,
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                columns: [
                    {
                        title: GetTextLanguage("stt"),
                        template: "<span class='stt'></span>",
                        width: 60,
                        attribute: alignCenter
                    },
                    {
                        template: '<div style="text-align:center"><button class="btn btn-xs btn-warning xemnhanvien">' + GetTextLanguage("chitiet") + '</button></div></div>',
                        width: 100
                    },
                    {
                        field: "AnhDaiDienHoTen", title: GetTextLanguage("tennguoinhan"), filterable: FilterInColumn,
                        width: 150
                    },
                    {
                        field: "ChucVu", title: GetTextLanguage("chucvu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn,
                        width: 150
                    },

                    {
                        field: "GioiTinh", title: GetTextLanguage("gioitinh"),
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                        width: 100
                    },
                    {
                        field: "NgaySinh", title: GetTextLanguage("ngaysinh"), type: "date", format: "{0:dd/MM/yyyy}",
                        filterable: FilterInColumn,
                        width: 150
                    },
                    {
                        field: "SDT", title: GetTextLanguage("sdt"),
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn,
                        width: 150
                    },
                    {
                        field: "DiaChi", title: GetTextLanguage("diachi"),
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn,
                        width: 150
                    },
                ],
                dataBound: function () {
                    var rows = this.items();
                    dem = 0;
                    $(rows).each(function () {
                        dem++;
                        var rowLabel = $(this).find(".stt");
                        $(rowLabel).html(dem);
                        if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                            dem = 0;
                        }
                    });
                }
            });
            $("#griddanhsachnhanvien").on("click", ".xemnhanvien", function () {
                //alert();
                var data = $("#griddanhsachnhanvien").data("kendoGrid").dataItem($(this).closest("tr"));
                $(".mdlThongTinNhanVien").kendoWindow({
                    modal: true,
                    width: '60%',
                    height: '60%',
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                })
                var popup = $(".mdlThongTinNhanVien").data("kendoWindow");
                popup.open();
                popup.center();
                popup.title(GetTextLanguage("thongtin"));
                $(".mdlThongTinNhanVien").load("/TraCuuDanhBa/ThongTinCaNhan?NhanVienID=" + data.NhanVienID);
            });
            $.ajax({
                cache: false,
                async: false,
                type: "POST",
                url: crudServiceBaseUrl + "/LayChiTiet",
                data: { YKienID: dataItemct.YKienID },
                success: function (data) {
                    var DSHinhAnh = [];
                    var a = '';
                    if (data.data.DSHinhAnh != null && data.data.DSHinhAnh.indexOf(',') !== -1) {
                        var strUrl = data.data.DSHinhAnh.split(',');
                        for (var k = 0; k < strUrl.length; k++) {
                            var anh = strUrl[k];
                            DSHinhAnh.push({ Url: strUrl[k] });
                        }
                        a = loadshinhanh(DSHinhAnh);
                        $('.dshinhanhadd2').html(a);

                    }
                    else if (data.data.DSHinhAnh != null && data.data.DSHinhAnh != "") {
                        DSHinhAnh.push({ Url: data.data.DSHinhAnh });
                        $('.dshinhanhadd2').html(loadshinhanh(DSHinhAnh));
                    }
                    showslideimg();

                },
                error: function (xhr, status, error) {

                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                }
            });
            Laydanhsachcomment(dataItemct.YKienID);
            $(".nhanvienguinhan").text("Nhân viên gửi");
        });
    }
    function Laydanhsachcomment(YKienID) {
        $.ajax({
            cache: false,
            async: false,
            type: "POST",
            url: "/YKienDongGop" + "/LayBinhLuanYKien",
            data: { YKienID: YKienID },
            success: function (data) {
                $(".danhsachphanhoi").html("");
                $(".danhsachphanhoi").html(data);
                showslideimg2();
                $(".guiphanhoi").click(function () {
                    var hinhanh = "";
                    var DSHinhAnh = [];
                    var ins = document.getElementById('files_xs').files.length;
                    if (ins > 0) {
                        var strUrl = uploadMultipleFileParagam(crudServiceBaseUrlUploadFile + "/UploadfilesBinhLuan", "files_xs", $(".dshinhanhadd")).split(',');
                        for (var k = 0; k < strUrl.length; k++) {
                            DSHinhAnh.push({ Url: strUrl[k] });
                        }
                    }
                    for (var i = 0; i < DSHinhAnh.length; i++) {
                        if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                            hinhanh += DSHinhAnh[i].Url + ",";
                        }
                    }

                    $.ajax({
                        cache: false,
                        async: false,
                        type: "POST",
                        url: "/YKienDongGop" + "/ThemBinhLuan",
                        data: { hinhanh: hinhanh.substring(0, hinhanh.length - 1), noidung: $("#NoiDung").val(), YKienID: YKienID },
                        success: function (data) {
                            altReturn(data);
                            if (data.code == "success") {
                                Laydanhsachcomment(YKienID);
                            } else {
                                for (var k = 0; k < ins; k++) {
                                    DSHinhAnh.splice((DSHinhAnh.length - 1 - k), 1);
                                }
                            }

                        },
                        error: function (xhr, status, error) {
                            for (var k = 0; k < ins; k++) {
                                DSHinhAnh.splice((DSHinhAnh.length - 1 - k), 1);
                            }
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        }
                    });
                })
            },
            error: function (xhr, status, error) {

                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
            }
        });
    }
    function dataTabBienBan() {
        $("#content_bienban").html('<div id="gridbienban"></div>');
        var noedit = $(location).attr('pathname') === "/ThongTinCaNhan/Index";
        var dataimg = new FormData();
        $("#gridbienban").kendoGrid({
            dataSource: {
                batch: true,
                transport: {
                    read: {
                        url: "/ThongTinCaNhan/GetDataBienBan?NhanVienID=" + $('#ThongTinNhanVienID').val(),
                        type: "get",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                    },
                    destroy: {
                        url: "/ThongTinCaNhan/DeleteBienBan",
                        type: 'get',
                        complete: function (e) {
                            altReturn(e.responseJSON);
                            $("#gridbienban").data("kendoGrid").dataSource.read();
                        }
                    },
                    parameterMap: function (data, type) {
                        if (type !== "read") {
                            if (type == "destroy") {
                                return { BienBanID: data.models[0].BienBanID }
                            }
                        } else {
                        }
                    }
                },
                pageSize: pageSize,
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        id: "BienBanID"
                    }
                }
            },
            height: heightGrid - 150,
            sortable: true,
            pageable: pageableAll,
            filterable: {
                mode: "row"
            },
            toolbar: [
                {
                    template: '<div style="text-align:right"><button id="btnthemHopDong" class="k-button k-button-icontext k-grid-add"><span class="k-icon k-i-plus"></span>' + GetTextLanguage("them") + '</button></div>'
                }
            ],
            edit: function (e) {
                e.container.find(".k-edit-label:first").hide();
                e.container.find(".k-edit-field:first").hide();
            },
            messages: {
                commands: {
                    update: GetTextLanguage("capnhat"),
                    canceledit: GetTextLanguage("huy")
                }
            },
            editable: {
                mode: "popup",
                window: {
                    title: GetTextLanguage("thongtinchitiet"),
                    width: 450
                },
                confirmation: GetTextLanguage("xoabangi"),
                confirmDelete: "Yes",
                template: kendo.template($("#tempBienBan").html())
            },
            dataBinding: function () {
                record = 0;
            },
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "#= ++record #",
                    width: 60,
                    attributes: alignCenter
                },
                {
                    field: "TenBienBan",

                    title: GetTextLanguage("tenbienban"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft
                },
                 {
                     command: [{ name: "destroy", text: GetTextLanguage("xoa") }],
                     title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                     width: 100,
                     attributes: alignCenter,
                     hidden: $(location).attr('pathname') === "/ThongTinCaNhan/Index" ? true : false,
                 }
                
            ],
            edit: function (e) {
                e.container.find(".k-edit-label:first").hide();
                e.container.find(".k-edit-field:first").hide();
                if (noedit) {
                    $(".k-grid-update").addClass("hidden");
                }
                $("#frmbienban #DinhKem").val(e.model.TepDinhKem);
                var DSHinhAnh = [];
                if (e.model.TepDinhKem != null && e.model.TepDinhKem != "") {
                    var strUrl = e.model.TepDinhKem;
                    for (var k = 0; k < strUrl.split(',').length; k++) {
                        var anh = strUrl.split(',')[k];
                        DSHinhAnh.push({ Url: strUrl.split(',')[k] });
                    }
                    a = loadslbuttonhopdong(DSHinhAnh);
                    $('#frmbienban .dshinhanhadd').html(a);

                }
                $("#frmbienban .dshinhanhadd .closeGenImgSuaChua").click(function () {
                    var file = $(this).data("file");
                    for (var i = 0; i < DSHinhAnh.length; i++) {
                        if (DSHinhAnh[i].Url === file) {
                            DSHinhAnh.splice(i, 1);
                            showslideimg();
                            break;
                        }
                    }
                    $(this).parent().remove();
                    var hinhanh2 = "";
                    for (var i = 0; i < DSHinhAnh.length; i++) {
                        hinhanh2 += DSHinhAnh[i].Url + ",";
                    }
                    $("#frmbienban #DinhKem").val(hinhanh2.substring(0, hinhanh2.length - 1));
                });
                function loadimgSuaChua2(input) {
                    //$('.dshinhanhadd').empty();
                    if (input.files) {
                        var filesAmount = input.files.length;
                        var html = "";
                        for (var i = 0; i < filesAmount; i++) {
                            if (input.files[i].type == "image/png" || input.files[i].type == "image/jpg" || input.files[i].type == "image/jpeg" || input.files[i].type == "image/gif") {
                                readURL2(input.files[i], input);

                            }
                        }

                    }
                }
                function readURL2(file, input) {

                    var reader = new FileReader();
                    reader.onload = function (event) {
                        var html = "";
                        html += "<div class='imgslider' style='float:left;position:relative;margin-right:10px;'>";
                        html += "<div style='background-image:url(\"" + event.target.result + "\"); margin:0px;' class='img-con ahihi3'><a rel='gallery' href='" + event.target.result + "'><img data-file='" + file.name + "' src='" + event.target.result + "' style='width: 100%;height: 100%;margin: 0;opacity: 0;'></a></div>";
                        html += "<div data-file='" + file.name + "' class='closeGenImgSuaChua' title='Xóa'>X</div></div>";
                        html += "</div>";
                        //html = "<div class='imgGenSuaChua' style=\"width:100px;height:100px;\"><img src='" + event.target.result + "' /><div data-file='" + file.name + "' class='closeGenImgSuaChua' title='Xóa'>X</div></div>";
                        $(input).parent().parent().find('.dshinhanhadd').append(html);
                        showslideimg();
                        $(".dshinhanhadd .closeGenImgSuaChua").click(function () {
                            $(this).parent().remove();
                            showslideimg();
                        });

                    }
                    reader.readAsDataURL(file);
                }
                showslideimg();
                $("#files_1").change(function () {
                    for (var x = 0; x < this.files.length; x++) {
                        dataimg.append("uploads", this.files[x]);
                    }
                    dataimg.getAll("uploads");
                    loadimgSuaChua2(this);
                    $("#files_1").val('').clone(true);
                });

            },
            save: function (e) {
                var hinhanh = "";
                var DSHinhAnh = [];
                $('.dshinhanhadd .imgslider').each(function (index2, element2) {
                    if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                        DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                    }
                });
                var ins = dataimg.getAll("uploads").length;
                if (ins > 0) {
                    var strUrl = uploadMultipleFileParagamWidthFile("/Upload" + "/UploadVanBan", dataimg.getAll("uploads"), $("#frmbienban .dshinhanhadd")).split(',');
                    for (var k = 0; k < strUrl.length; k++) {
                        DSHinhAnh.push({ Url: strUrl[k] });
                    }
                }
                for (var i = 0; i < DSHinhAnh.length; i++) {
                    if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                        hinhanh += DSHinhAnh[i].Url + ",";
                    }
                }
                
                if (e.model.BienBanID == null || e.model.BienBanID == "") {
                    $.ajax({
                        url: "/ThongTinCaNhan/AddBienBan",
                        type: "POST",
                        data: {
                            TenBienBan: $("#TenBienBan").val(),
                            NhanVienID: $("#ThongTinNhanVienID").val(),
                            TepDinhKem: hinhanh.substring(0, hinhanh.length - 1)
                        },
                        complete: function (e) {
                            alertToastr(e.responseJSON);
                            $("#gridbienban").data("kendoGrid").dataSource.read();
                        }
                    });
                } else {
                    $.ajax({
                        url: "/ThongTinCaNhan/UpdateBienBan",
                        type: "POST",
                        data: {
                            BienBanID: e.model.BienBanID,
                            TenBienBan: $("#TenBienBan").val(),
                            TepDinhKem: hinhanh.substring(0, hinhanh.length - 1)
                        },
                        complete: function (e) {
                            alertToastr(e.responseJSON);
                            $("#gridbienban").data("kendoGrid").dataSource.read();
                        }
                    });
                }
            },
        });
        $("#gridbienban .k-grid-content").on("dblclick", "td", function () {
            var rowct = $(this).closest("tr"),
                gridct = $("#gridbienban").data("kendoGrid"),
                dataItemct = gridct.dataItem(rowct);
            if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                $("#gridbienban").data("kendoGrid").editRow(rowct);
            }
        })
        if (noedit) {
            $("#gridbienban #btnthemHopDong").addClass("hidden");
        }

    }

    if ($(location).attr('pathname') === "/ThongTinCaNhan/Index") {
        $("#TenDoi").prop("disabled", true);
        $("#SoNgayGioiHan").prop("disabled", true);
        $("#SoNgayKhaiCong").prop("disabled", true);
        $("#Lever").data("kendoDropDownList").enable(false);
        $("#TenCongTrinh").prop("disabled", true);
        $("#TenChucVu").prop("disabled", true);
        $("#TinhTrang").data("kendoDropDownList").enable(false);
        $("#LoaiHopDong").data("kendoDropDownList").enable(false);
        $("#NgayDiLam").data("kendoDatePicker").enable(false);
        $("#NgayHetHanHopDong").data("kendoDatePicker").enable(false);
        $("#NgayThoiViec").data("kendoDatePicker").enable(false);
        $("#btnthemHopDong").addClass("hidden");
    }
    //Xem ảnh function
    function ShowImg(obj) {
        var mdl = $('#mdlShowImg').kendoWindow({
            width: 880,
            height: 500,
            title: GetTextLanguage("hinhanhbaocao"),
            modal: true,
            visible: false,
            resizable: false,
            close: function () {
                $(".slider-inner").empty();
                $(".chitietleft").empty();
            },
            actions: [
                "Close"
            ]
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#tempBaoCaoChiTiet').html()));
        $(".hinhanhbaocao").html(loadslbutton(obj));
        showslideimg();
    }
});
function getDataSourceCT(url, param) {
    var dataSources = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QuanLyNhanVien" + url,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: param,
                complete: function (e) {
                    var element = $('div.k-widget.k-window');
                    kendo.ui.progress(element, false);
                }
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        schema: {
            data: "data",
            total: "total"
        }
    });

    return dataSources;
}
function loadslbutton(data) {
    var strSlideInner = "<div class='text-center img-con'>";
    for (var i = 0; i < data.length; i++) {
        strSlideInner += "<div  style='background-image:url(" + localQLTS + data[i] + "); ' class='imgslider img-con ahihi'><a rel='gallery' href='" + localQLTS + data[i] + "'><img src='" + localQLTS + data[i] + "' style='width: 100%;height: 100%;margin: 0;opacity: 0;'></a></div>";
        if (i == data.length - 1) { strSlideInner += "</div><div style='clear:both'></div>" }
    }
    return strSlideInner;
}
function loadslbuttonhopdong(data) {
    var strSlideInner = "";
    for (var i = 0; i < data.length; i++) {
        strSlideInner += "<div class='imgslider' style='float:left;position:relative;margin-right:10px;'>";
        strSlideInner += "<div style='background-image:url(\"" + localQLVanBan + data[i].Url + "\"); margin:0px;' class='img-con ahihi3'><a rel='gallery' href='" + localQLVanBan + data[i].Url + "'><img data-file='" + data[i].Url + "' src='" + localQLVanBan + data[i].Url + "' style='width: 100%;height: 100%;margin: 0;opacity: 0;'></a></div>";
        strSlideInner += "<div data-file='" + data[i].Url + "' class='closeGenImgSuaChua' title='Xóa'>X</div></div>";
        if (i == data.length - 1) { strSlideInner += "</div>" }
    }
    return strSlideInner;
}
function showslideimg() {
    $('.imgslider img').fullscreenslides();

    // All events are bound to this container element
    var $container = $('#fullscreenSlideshowContainer');
    $container
    $container
        //This is triggered once:
        .bind("init", function () {
            // The slideshow does not provide its own UI, so add your own
            // check the fullscreenstyle.css for corresponding styles
            if ($('#fullscreenSlideshowContainer #fs-close').length == 0) {
                $container
                    .append('<div class="ui" id="fs-close">&times;</div>')
                    .append('<div class="ui" id="fs-loader">' + GetTextLanguage("dangtai") + '</div > ')
                    .append('<div class="ui" id="fs-prev">&lt;</div>')
                    .append('<div class="ui" id="fs-next">&gt;</div>')
                    .append('<div class="ui" id="fs-caption"><span></span></div>');
                $('#fs-prev').click(function () {
                    // You can trigger the transition to the previous slide
                    $container.trigger("prevSlide");
                });
                $('#fs-next').click(function () {
                    // You can trigger the transition to the next slide
                    $container.trigger("nextSlide");
                });
                $('#fs-close').click(function () {
                    // You can close the slide show like this:
                    $container.trigger("close");
                });
            }
            // Bind to the ui elements and trigger slideshow events


        })
        // When a slide starts to load this is called
        .bind("startLoading", function () {
            // show spinner
            $('#fs-loader').show();
        })
        // When a slide stops to load this is called:
        .bind("stopLoading", function () {
            // hide spinner
            $('#fs-loader').hide();
        })
        // When a slide is shown this is called.
        // The "loading" events are triggered only once per slide.
        // The "start" and "end" events are called every time.
        // Notice the "slide" argument:
        .bind("startOfSlide", function (event, slide) {
            // set and show caption
            $('#fs-caption span').text(slide.title);
            $('#fs-caption').show();
        })
        // before a slide is hidden this is called:
        .bind("endOfSlide", function (event, slide) {
            $('#fs-caption').hide();
        });
}
function showAvataImage() {
    $('.avatar img').fullscreenslides();
    var $container = $('#fullscreenSlideshowContainer');
    $container
    $container
        .bind("init", function () {
            if ($('#fullscreenSlideshowContainer #fs-close').length == 0) {
                $container
                    .append('<div class="ui" id="fs-close">&times;</div>')
                    .append('<div class="ui" id="fs-caption"><span></span></div>');
                $('#fs-close').click(function () {
                    $container.trigger("close");
                });
            }
        })
        .bind("startLoading", function () {
            $('#fs-loader').show();
        })
        .bind("stopLoading", function () {
            $('#fs-loader').hide();
        })
        .bind("startOfSlide", function (event, slide) {
         
            $('#fs-caption span').text(slide.title);
            $('#fs-caption').show();
        })
        .bind("endOfSlide", function (event, slide) {
            $('#fs-caption').hide();
        });
}
function loadslbuttondstaisan(data) {
    var strSlideInner = "";
    for (var i = 0 ; i < data.length; i++) {
        strSlideInner += "<div class='imgslider' style='float:left;position:relative;margin-right:10px;'>";
        strSlideInner += "<div style='background-image:url(\"" + localQLTS + data[i].Url + "\"); margin:0px;' class='img-con ahihi3'><a rel='gallery' href='" + localQLTS + data[i].Url + "'><img src='" + localQLTS + data[i].Url + "' style='width: 100%;height: 100%;margin: 0;opacity: 0;'></a></div>";
        strSlideInner += "<div data-file='" + data[i].Url + "' class='closeGenImgSuaChua' title='Xóa'>X</div></div>";
        if (i == data.length - 1) { strSlideInner += "</div>" }
    }
    return strSlideInner;
}
function loadshinhanh(data) {
    var strSlideInner = "";
    for (var i = 0; i < data.length; i++) {
        strSlideInner += "<div class='imgslider' style='float:left;position:relative;margin-right:10px;'>";
        strSlideInner += "<div style='background-image:url(\"" + localQLBL + data[i].Url + "\"); margin:0px;' class='img-con ahihi3'><a rel='gallery' href='" + localQLBL + data[i].Url + "'><img src='" + localQLBL + data[i].Url + "' style='width: 100%;height: 100%;margin: 0;opacity: 0;'></a></div>";
        strSlideInner += "</div>";
        if (i == data.length - 1) { strSlideInner += "</div>" }
    }
    return strSlideInner;
}
function showslideimg2() {
    $('.danhsachphanhoi img').fullscreenslides();

    // All events are bound to this container element
    var $container = $('#fullscreenSlideshowContainer');
    $container
    $container
        //This is triggered once:
        .bind("init", function () {
            // The slideshow does not provide its own UI, so add your own
            // check the fullscreenstyle.css for corresponding styles
            if ($('#fullscreenSlideshowContainer #fs-close').length == 0) {
                $container
                    .append('<div class="ui" id="fs-close">&times;</div>')
                    .append('<div class="ui" id="fs-loader">Đang tải...</div>')
                    .append('<div class="ui" id="fs-prev">&lt;</div>')
                    .append('<div class="ui" id="fs-next">&gt;</div>')
                    .append('<div class="ui" id="fs-caption"><span></span></div>');
                $('#fs-prev').click(function () {
                    // You can trigger the transition to the previous slide
                    $container.trigger("prevSlide");
                });
                $('#fs-next').click(function () {
                    // You can trigger the transition to the next slide
                    $container.trigger("nextSlide");
                });
                $('#fs-close').click(function () {
                    // You can close the slide show like this:
                    $container.trigger("close");
                });
            }
            // Bind to the ui elements and trigger slideshow events


        })
        // When a slide starts to load this is called
        .bind("startLoading", function () {
            // show spinner
            $('#fs-loader').show();
        })
        // When a slide stops to load this is called:
        .bind("stopLoading", function () {
            // hide spinner
            $('#fs-loader').hide();
        })
        // When a slide is shown this is called.
        // The "loading" events are triggered only once per slide.
        // The "start" and "end" events are called every time.
        // Notice the "slide" argument:
        .bind("startOfSlide", function (event, slide) {
            // set and show caption
            $('#fs-caption span').text(slide.title);
            $('#fs-caption').show();
        })
        // before a slide is hidden this is called:
        .bind("endOfSlide", function (event, slide) {
            $('#fs-caption').hide();
        });
}