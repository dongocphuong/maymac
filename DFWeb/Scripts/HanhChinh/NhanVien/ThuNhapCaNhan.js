﻿$(document).ready(function () {
    var d = new Date();
    var month = d.getMonth() + 1;
    var datafix_Nam = [];
    for (var i = 0; i < 10; i++) {
        datafix_Nam.push({ text: d.getFullYear() - i, value: d.getFullYear() - i });
    }
    var datafix_loaikhoan = [
            { text: GetTextLanguage("nhanvienvanphong"), value: 1 },
            { text: GetTextLanguage("nhanviencongtrinh"), value: 0 }
    ];

    $("#Nam").kendoDropDownList({
        dataSource: datafix_Nam,
        dataTextField: "text",
        dataValueField: "value",
        autoBind: true,
    });
    $(".btn-timkiem").on("click", function () {
        ContentLoading("BangTPNV", "/ThuNhapCaNhan/TimKiemThuNhapCaNhan?Nam=" + $("#Nam").val() + "&NhanVienID=" + $("#IDNhanVien").val());
    });
});