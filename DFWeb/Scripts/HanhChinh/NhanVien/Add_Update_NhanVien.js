﻿var dataimg = new FormData();
var Dept = 1;
function chageavatar() {
    var preview = document.getElementById('img-avatar-show');
    var file = document.getElementById('upLoadFile').files[0];
    var reader = new FileReader();

    reader.addEventListener("load", function () {
        preview.src = reader.result;
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
}
$(document).ready(function () {
    lstHinhAnh = [];
    $("#SoNgayKhaiBaoBu").autoNumeric(autoNumericOptionsInt);
    $("#SoNgayGioiHan").autoNumeric(autoNumericOptionsInt);
    var gioitinh = [
        { text: GetTextLanguage("boy"), value: 0 },
        { text: GetTextLanguage("nu"), value: 1 }
    ];
    var checkDaNopCMT = [
        { text: GetTextLanguage("danop"), value: true },
        { text: GetTextLanguage("chuanop"), value: false }
    ]
    var checkDaNopBang = [
        { text: GetTextLanguage("danop"), value: true },
        { text: GetTextLanguage("chuanop"), value: false }
    ]
    var gioitinh = [
        { text: GetTextLanguage("boy"), value: 0 },
        { text: GetTextLanguage("nu"), value: 1 }
    ];
    var tinhtrang = [
        { text: GetTextLanguage("danglamviec"), value: 1 },
        { text: GetTextLanguage("nghichoviec"), value: 2 },
        { text: GetTextLanguage("thoiviec"), value: 3 }
    ];
    var loaihopdong = [
        { text: GetTextLanguage("chuaky"), value: 0 },
        { text: GetTextLanguage("daihan"), value: 1 },
        { text: GetTextLanguage("thang1236"), value: 2 },
        { text: GetTextLanguage("thuviec"), value: 3 },
        { text: GetTextLanguage("muavu"), value: 4 }
    ];

    var thamgiaphongchat = [
        { text: GetTextLanguage("macdinhkhongthamgia"), value: 0 },
        { text: GetTextLanguage("macdinhthamgiaphongchat"), value: 1 },
    ]

    $("#NgaySinh").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date(),
    });

    $("#NgayCapCMT").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date(),
    });

    $("#NgayDiLam").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date(),
    });

    $("#NgayThoiViec").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date(),
    });



    $("#NgayDiLam").val("");
    $("#NgayThoiViec").val("");

    var categCheck = $('.multiselect').multiselect({
        enableFiltering: true,
        templates: {
            filter: '<li class="multiselect-item multiselect-filter"><i class="icon-search4"></i> <input class="form-control" type="text"></li>'
        },
        onChange: function () {
            $.uniform.update();
        }
    });
    // Primary
    $(".control-primary").uniform({
        radioClass: 'choice',
        wrapperClass: 'border-primary-600 text-primary-800'
    });
    $(".styled, .multiselect-container input").uniform({ radioClass: 'choice' });

    $("#CapDoID").kendoDropDownList({
        dataTextField: 'TenCapDo',
        dataValueField: 'CapDoBaoMatID',
        filter: "contains",
        autoBind: true,
        optionLabel: GetTextLanguage("capdobaomat"),
        dataSource: null
    });

    $("#DoiThiCongID").kendoDropDownList({
        dataTextField: 'TenDoi',
        dataValueField: 'DoiThiCongID',
        filter: "contains",
        autoBind: true,
        optionLabel: GetTextLanguage("donvi"),
        dataSource: null,
        change: function () {
            loadMultiselect($("#DoiThiCongID").data("kendoDropDownList").value());
        }
    });
    $("#PhongBanID").kendoDropDownList({
        dataTextField: 'TenPhong',
        dataValueField: 'PhongBanID',
        filter: "contains",
        autoBind: true,
        optionLabel: GetTextLanguage("phongban"),
        dataSource: getDataForDropdownlistWithParam("/QLNhanVien/DanhSachPhongBan"),
    });
    $("#GioiTinh").kendoDropDownList({
        dataTextField: 'text',
        dataValueField: 'value',
        filter: "contains",
        autoBind: true,
        dataSource: gioitinh
    });
    $("#TinhTrang").kendoDropDownList({
        dataTextField: 'text',
        dataValueField: 'value',
        filter: "contains",
        autoBind: true,
        dataSource: tinhtrang
    });
    $("#ThamGiaPhongChat").kendoDropDownList({
        dataTextField: 'text',
        dataValueField: 'value',
        filter: "contains",
        autoBind: true,
        dataSource: thamgiaphongchat
    });
    $("#LoaiHopDong").kendoDropDownList({
        dataTextField: 'text',
        dataValueField: 'value',
        filter: "contains",
        autoBind: true,
        dataSource: loaihopdong
    });
    $("#ChucVu").kendoDropDownList({
        dataTextField: 'text',
        dataValueField: 'value',
        filter: "contains",
        //autoBind: true,
        dataSource: getDataForDropdownlistWithParam("/Home/DSChucVu", {})
    });
    
    $('#ChucVu').data("kendoDropDownList").value("7d032d17-c8ea-4416-a321-3b3115cb687a");

    $(".validreq").on("keyup", function () {
        if ($(this).val() == "") {
            $(this).addClass("errorReq");
        } else {
            $(this).removeClass("errorReq");
        }
    });

    $("#VanPhong").on("click", function () {
        Dept = 1;
        $(".inputct").removeClass("shows").addClass("hiddens"); // Chưa Ký
        $("#DoiThiCongID").data("kendoDropDownList").dataSource.read({ Dept: 1 });
        $(".congtrinhtab").addClass("hidden");
        $(".phongbantab").removeClass("hidden");
    });
    $("#CongTrinh").on("click", function () {
        Dept = 0;
        $(".inputct").removeClass("hiddens").addClass("shows"); //Đã Kỹ
        $("#DoiThiCongID").data("kendoDropDownList").dataSource.read({ Dept: 0 });
        $(".congtrinhtab").removeClass("hidden");
        $(".phongbantab").addClass("hidden");

    });
    if ($("#IDNhanVien").val() != null && $("#IDNhanVien").val() != "") {
        $.ajax({
            url: '/QLNhanVien/LayChiTietNhanVien',
            method: "POST",
            data: {
                NhanVienID: $("#IDNhanVien").val()
            },
            datatype: 'json',
            success: function (data) {
                console.log(data.data);
                ContentWatingOP("ctloader", 0.9);
                $(".edits").hide();
                $("#CapDoID").data("kendoDropDownList").value(data.data.ItemNhanVien.Lever);
                $("#TenNhanVien").val(data.data.ItemNhanVien.TenNhanVien);
                //$("#TaiKhoan").val(data.data.ItemNhanVien.UserName);
                $("#NgaySinh").data('kendoDatePicker').value(data.data.ItemNhanVien.strNgaySinh);
                $("#GioiTinh").data("kendoDropDownList").value(data.data.ItemNhanVien.GioiTinh);
                $("#SDT").val(data.data.ItemNhanVien.SDT);
                $("#SoCMT").val(data.data.ItemNhanVien.CMT);
                $("#DiaChi").val(data.data.ItemNhanVien.DiaChi);
                $("#TinhTrang").data("kendoDropDownList").value(data.data.ItemNhanVien.TinhTrang);
                $("#NgayDiLam").data("kendoDatePicker").value(data.data.ItemNhanVien.strNgayDiLam);
                $("#NgayThoiViec").data("kendoDatePicker").value(data.data.ItemNhanVien.strNgayThoiViec);
                $("#DaNopBang").prop('checked', data.data.ItemNhanVien.DaNopBang);
                $("#DaNopCMT").prop('checked', data.data.ItemNhanVien.DaNopCMT);
                $("#ThoatPhongChat").prop('checked', data.data.ItemNhanVien.ThoatKhiNghiViec);
                if (data.data.ItemNhanVien.DaNopCMT) {
                    $('#uniform-DaNopCMT').find('span').addClass('checked');
                }
                if (data.data.ItemNhanVien.DaNopBang) {
                    $('#uniform-DaNopBang').find('span').addClass('checked');
                }
                if (data.data.ItemNhanVien.ThoatKhiNghiViec) {
                    $('#uniform-ThoatPhongChat').find('span').addClass('checked');
                }
                $("#NgayCapCMT").data("kendoDatePicker").value(data.data.ItemNhanVien.strNgayCapCMT);
                $("#NoiCapCMT").val(data.data.ItemNhanVien.NoiCapCMT);
                $("#NganhNghe").val(data.data.ItemNhanVien.NganhNghe);
                $("#TenNganHang").val(data.data.ItemNhanVien.TenNganHang);
                $("#SoTaiKhoan").val(data.data.ItemNhanVien.SoTaiKhoanNganHang);
                $("#NoiCapBang").val(data.data.ItemNhanVien.TruongCapBang);
                $("#MaSoThue").val(data.data.ItemNhanVien.MaSoThue);
                $("#SoNgayGioiHan").val(data.data.ItemNhanVien.SoNgayGioiHan);
                $("#SoNgayKhaiBaoBu").val(data.data.ItemNhanVien.SoNgayKhaiCong);
                $("#ChucVu").data("kendoDropDownList").value(data.data.ItemNhanVien.ChucVuID);
                $("#NV_Avatar").val(data.data.ItemNhanVien.AnhDaiDien);
                $("#NgayKetThucHopDong").val(data.data.ItemNhanVien.strNgayKetThucHd);
                $("#LoaiHopDong").data("kendoDropDownList").value(data.data.ItemNhanVien.LoaiHopDong);
                $("#ThamGiaPhongChat").data("kendoDropDownList").value(data.data.ItemNhanVien.ThamGiaPhongChat);
                document.getElementById('img-avatar-show').src = localQLNV + (data.data.ItemNhanVien.AnhDaiDien == null ? "noavatar.png" : (data.data.ItemNhanVien.AnhDaiDien == "" ? "noavatar.png" : data.data.ItemNhanVien.AnhDaiDien));

                Dept = 1;
                $("#PhongBanID").data("kendoDropDownList").value(data.data.ItemNhanVien.PhongBanID);
                //$(".congtrinhtab").addClass("hidden");
                //$(".phongbantab").removeClass("hidden");

                //$("#DoiThiCongID").data("kendoDropDownList").enable(false);
                categCheck.multiselect('disable');
                $("#TaiKhoan").attr("disabled", "disabled");
                $("#ctloader").unblock();


            },
            error: function () {
                $("#ctloader").unblock();
            }

        });

        if ($("#role_update").val() == 0) {
            $("#btnThemNhanh").attr("disabled", "disabled");
            $("#ChucVu").data("kendoDropDownList").enable(false);
            $("#SoNgayGioiHan").attr("disabled", "disabled");
            $("#SoNgayKhaiBaoBu").attr("disabled", "disabled");
            $("#CapDoID").data("kendoDropDownList").enable(false);
            $("#TinhTrang").data("kendoDropDownList").enable(false);
            $("#NgayDiLam").data("kendoDatePicker").enable(false);
            $(".tb-our").css("display", "none");
        } else {
            $(".btn-xoa").css("display", "inline-block");
        }


    } else {
        $("#ctloader").addClass("addnv");
        
    }

    function loadMultiselect(DoiThiCongID) {
        $.ajax({
            url: "/Home/DsContTrinhTheoDoiMultiple",
            type: "POST",
            data: {
                DoiThiCongID: DoiThiCongID
            },
            complete: function (e) {
                categCheck.multiselect('dataprovider', e.responseJSON.data);
                $(".control-primary").uniform({
                    radioClass: 'choice',
                    wrapperClass: 'border-primary-600 text-primary-800'
                });
                $(".styled, .multiselect-container input").uniform({ radioClass: 'choice' });
            }
        });
    }

    //$("#TenNhanVien").focusout(function () {
    //    if ($("#IDNhanVien").val() == null || $("#IDNhanVien").val() == "") {
    //        $("#TaiKhoan").val(getlink($(this).val()));
    //    }
    //});

});
function valid() {
    if ($("#TenNhanVien").val() == "") {
        showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("tennhanvien"));
        $("#TenNhanVien").focus();
        return false;
    }
    //if ($("#TaiKhoan").val() == "") {
    //    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("tentaikhoan"));
    //    $("#TaiKhoan").focus();
    //    return false;
    //}
    if (Dept == 0) {
        if ($("#DoiThiCongID").val() == "") {
            showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("bophan"));
            $("#DoiThiCongID").data("kendoDropDownList").focus();
            return false;
        }
    } else {
        if ($("#PhongBanID").val() == "") {
            showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("phongban"));
            $("#PhongBanID").data("kendoDropDownList").focus();
            return false;
        }
    }
    
    if ($("#ChucVu").val() == "") {
        showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("chucvu"));
        $("#ChucVu").data("kendoDropDownList").focus();
        return false;
    }
    return true;
}

function saveNhanVien() {
    if (valid()) {
        ContentWatingOP("content-loader", 0.9);

        var ins = document.getElementById("upLoadFile").files.length;;
        if (ins > 0) {
            for (var x = 0; x < ins; x++) {
                dataimg.append("uploads", document.getElementById("upLoadFile").files[x]);
            }

        }
        var objData = {};
        var CTLQ = [];
        CTLQ = $("#CTLienQuan").val();
        objData.NhanVienID = $("#IDNhanVien").val();
        objData.TenNhanVien = $("#TenNhanVien").val();
        objData.NgaySinh = $("#NgaySinh").val();
        objData.GioiTinh = $("#GioiTinh").val();
        objData.SDT = $("#SDT").val();
        objData.CMT = $("#SoCMT").val();
        objData.DiaChi = $("#DiaChi").val();
        objData.TinhTrang = $("#TinhTrang").val();
        objData.NgayDiLam = $("#NgayDiLam").val();
        objData.NgayThoiViec = $("#NgayThoiViec").val();
        objData.DaNopBang = $("#DaNopBang").is(':checked');
        objData.DaNopCMT = $("#DaNopCMT").is(':checked');
        objData.NgayCapCMT = $("#NgayCapCMT").val();
        objData.NoiCapCMT = $("#NoiCapCMT").val();
        objData.NganhNghe = $("#NganhNghe").val();
        objData.TenNganHang = $("#TenNganHang").val();
        objData.SoTaiKhoanNganHang = $("#SoTaiKhoan").val();
        objData.TruongCapBang = $("#NoiCapBang").val();
        objData.MaSoThue = $("#MaSoThue").val();
        objData.SoNgayGioiHan = $("#SoNgayGioiHan").val();
        objData.SoNgayKhaiCong = $("#SoNgayKhaiBaoBu").val();
        objData.Lever = $("#CapDoID").val();
        //objData.CT = [];
        //objData.CT.push($("#PhongBanID").val());

        objData.PhongBan = $("#PhongBanID").val();

        
        objData.ChucVu = $("#ChucVu").val();
        objData.Dept = Dept;
        //objData.TaiKhoan = $("#TaiKhoan").val();
        objData.ThamGiaPhongChat = $("#ThamGiaPhongChat").val();
        objData.ThoatKhiNghiViec = $("#ThoatPhongChat").is(':checked');
        var flag = true;
        if (flag) {
            var objXhr = new XMLHttpRequest();
            objXhr.onreadystatechange = function () {
                if (objXhr.readyState == 4) {
                    if (objXhr.responseText != "") {
                        var strUrl = objXhr.responseText.split(',');
                        for (var k = 0; k < strUrl.length; k++) {
                            objData.Avatar = strUrl[k];
                        }
                    } else {
                        objData.Avatar = $("#NV_Avatar").val();
                    }

                    setTimeout(function () {
                        $.ajax({
                            cache: false,
                            async: false,
                            type: "POST",
                            url: "/QLNhanVien/NhanVienAdd_Request",
                            data: { data: JSON.stringify(objData) },
                            success: function (data) {
                                altReturn(data);
                                lstHinhAnh = [];
                                if (data.code == "warning") {
                                    $("#TaiKhoan").focus();
                                    $("#TaiKhoan").addClass("errorReq");
                                }
                                if (data.code == "success" && ($("#IDNhanVien").val() == "" || $("#IDNhanVien").val() == null)) {
                                    $("#role_update").val(1);
                                    LoadingContent("ChiTietNhanVien", "/QLNhanVien/ViewChung?NhanVienID=" + data.idnhanvien, $("#TenNhanVien").val());
                                }
                                $("#content-loader").unblock();

                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            }
                        });
                    }, 1000);
                }
            }

            objXhr.open("POST", "/Upload/UploadAvatarNhanVien", true);
            objXhr.send(dataimg);
        }
    }
}

function xoaNhanVien() {
    if (confirm(GetTextLanguage("bancomuonxoabanghinaykhong"))) {
        $.ajax({
            cache: false,
            async: false,
            type: "POST",
            url: "/QLNhanVien/XoaNhanVien",
            data: {
                NhanVienID: $("#IDNhanVien").val()
            },
            success: function (data) {
                altReturn(data);
                if (data.code == "success") {
                    reload();
                }
            },
            error: function (xhr, status, error) {
                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
            }
        });
    }
}

function reload() {
    ContentWatingOP("content-loader", 1);
    $("#grid_DSNhanVien").data("kendoGrid").dataSource.read();
    $(".content").show();
    $("#ChiTietNhanVien").html("");
    $("#content-loader").unblock();
}

function getlink(str) {
    //code by Minit - www.canthoit.info - 13-05-2009
    //var str = (document.getElementById("title").value);
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g, "");
    str = str.replace(/-+-/g, ""); //thay thế 2- thành 1-
    str = str.replace(/^\-+|\-+$/g, ""); //cắt bỏ ký tự - ở đầu và cuối chuỗi
    return str;
}