﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/PhanQuyen";

    var actionThietBi = { add: 10, edit: 15 };

    //parameter search
    var vstart = 0;
    var vlimit = 20;
    var vsearchValue = "";
    var vCongTrinhID = "";
    var vLoaiHinh = 10;

    //datafix

    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: getDataSourceGrid("/getDanhSachNhanVien", {}, ""),
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: pageableAll,
        height: window.innerHeight * 0.8,
        width: "100%",
        excel: {
            fileName: "Danh sách thiết bị.xlsx",
            allPages: true
        },
        columns: [
            {
                width: 100,
                align: "center",
                template: kendo.template($("#tmplaction").html())
            },
            {
                field: "TenNhanVien",
                title: GetTextLanguage("tennhanvien"),
                filterable: FilterInTextColumn,
                width: 200,
                attributes: { style: "text-align:left;" }
            },
            {
                field: "UserName",
                title: GetTextLanguage("tentaikhoan"),//, template: kendo.template($("#tempLoaiHinh").html())
                filterable: FilterInTextColumn,
                width: 200,
                attributes: { style: "text-align:left;" }
            },
            //{
            //    field: "CMT", title: "Số CMT", width: 120, filterable: {
            //        cell: {
            //            showOperators: false,
            //            operator: "contains"

            //        }
            //    }
            //},
            //{
            //    field: "DiaChi", title: "Địa Chỉ", width: 150, filterable: {
            //        cell: {
            //            showOperators: false,
            //            operator: "contains"

            //        }
            //    }
            //}
            {
                field: "MatKhau",
                title: GetTextLanguage("matkhau"),//, template: kendo.template($("#tempLoaiHinh").html())
                filterable: FilterInColumn,
                width: 200,
                attributes: { style: "text-align:left;" }
            },
            //{ command: "destroy", title: "&nbsp;", width: 150 }
        ]
    });

    $("#grid").on("click", ".xemchitiet", xemchitietnew);
    function xemchitiet() {
        var checkedIds = {};

        row = $(this).closest("tr"),
        grid = $("#grid").data("kendoGrid"),
        dataItem = grid.dataItem(row);

        var mdl = $('#mdlquyen').kendoWindow({
            width: 700,
            height: "80%",
            title: GetTextLanguage("phanquyen"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#tmplquyen').html()));

        var gridquyen = $("#gridquyen").kendoGrid({
            dataSource: getDataSourceGrid("/genPhanQuyen", { userid: dataItem.get("UserId") }, ""),
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            //pageable: {
            //    refresh: true,
            //    buttonCount: 5,
            //    pageSizes: true,
            //    messages: messagegrid
            //},
            height: window.innerHeight * 0.8,
            width: "100%",
            dataBound: function (e) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (checkedIds[view[i].uid]) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                    }
                }
            },
            columns: [
                {
                    field: "TenGroup",
                    attributes: { "style": "text-align:left !important;" },
                    title: GetTextLanguage("tennhom"),
                    width: 500,//, template: kendo.template($("#tempLoaiHinh").html())
                    filterable: FilterInTextColumn
                },
                {
                    field: "IsActive",
                    title: GetTextLanguage("quyen"),
                    template: "<input name='IsActive' class='checkbox' type='checkbox' data-bind='checked: IsActive' #= IsActive ? checked='checked' : '' #/>",
                    width: 60,
                    filterable: false
                }

                //{ command: "destroy", title: "&nbsp;", width: 150 }
            ]
        }).data("kendoGrid");
        
        $(".excelsuachuain").click(function () {
            var idsToSend = [];


            var grid = $("#gridquyen").data("kendoGrid")
            var ds = grid.dataSource.view();

            for (var i = 0; i < ds.length; i++) {
                var row = grid.table.find("tr[data-uid='" + ds[i].uid + "']");
                var checkbox = $(row).find(".checkbox");

                if (checkbox.is(":checked")) {
                    idsToSend.push({id:ds[i].GroupGuid});
                }
            }

            setTimeout(function () {
                $.ajax({
                    cache: false,
                    async: false,
                    type: "POST",
                    url: crudServiceBaseUrl + "/savePhanQuyen",
                    data: { data: JSON.stringify(idsToSend), userid: dataItem.get("UserId") },
                    success: function (data) {
                        altReturn(data);
                        closeModel("mdlquyen");
                       // dataobjectload = data.data[0];
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                    }
                });
            }, 300);
        });
        resizeContent();

        gridquyen.table.on("click", ".checkbox", function () {
            console.log(this.checked);
            var checked = this.checked,
                rows = $(this).closest("tr"),
                grids = $("#gridquyen").data("kendoGrid"),
                dataItems = grids.dataItem(rows);
            checkedIds[dataItems.uid] = checked;
            if (checked) {
                //-select the row
                rows.addClass("k-state-selected");
            } else {
                //-remove selection
                rows.removeClass("k-state-selected");
            }
        });
    }
    function xemchitietnew() {
        row = $(this).closest("tr"),
        grid = $("#grid").data("kendoGrid"),
        dataItem = grid.dataItem(row);
        console.log(dataItem);
        $(".dsnv").hide();
        LoadingContent("ChiTietNhanVien", "/PhanQuyen/GenPhanQuyenByNhanVien?NhanVienID=" + dataItem.get("NhanVienID") + "&UserID=" + dataItem.get("UserId"), dataItem.get("TenNhanVien"));
    }
    

    //loaddata
    function getDataSourceGrid(url, param, groups) {
        var dataSources = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + url,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: param
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            batch: true,
            //pageSize: 20,
            //group: groups,
            schema: {
                data: "data",
                total: "total"
            }
        });

        return dataSources;
    }





    function getHeigtContent(grid) {
        return ($("#mdlquyen").height() - 102 - 50 - 50);
    }

    $(window).resize(function () {
        resizeContent();
    });

    function resizeContent() {
        $("#gridquyen .k-grid-content").css("height", getHeigtContent());
    }
    function showToast(loai, title, message) {
        toastr.options.positionClass = "toast-bottom-right";
        toastr[loai](message, title, { timeOut: 3000 });
    }

    function altReturn(data) {
        if (data.code == "success") {
            showToast("success", GetTextLanguage("thanhcong"), data.message);
        }
        else {
            showToast("error", GetTextLanguage("thatbai"), data.message);
        }
    }
    $('.sidebar-toggle').click(function () {
        setTimeout(function () {
            $('#grid').data('kendoGrid').refresh();
        }, 270)
    })
});