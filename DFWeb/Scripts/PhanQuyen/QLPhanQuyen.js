﻿$(document).ready(function () {
    var loaihopdong = [
        { text: "Đề nghị", value: 1 },
        { text: "web", value: 2 },
    ];
    $("#TinhTrangCT").kendoDropDownList({
        dataSource: loaihopdong,
        dataTextField: "text",
        dataValueField: "value",
        autoBind: true,
        filter: "contains",
        change: function (e) {
            var value = this.value();
            if (value) {
                $("#grid_DMQuyen").data("kendoGrid").dataSource.read({ LoaiHinh: value });
            }
        }
    });
    var nhom = [
        { text: GetTextLanguage("hosocanhan"), value: 1 },
        { text: GetTextLanguage("quanlydanhmuc"), value: 2 },
        { text: GetTextLanguage("nghiepvu"), value: 3 },
        { text: GetTextLanguage("quantrihethong"), value: 4 },
        { text: GetTextLanguage("dutoan"), value: 5 },
        { text: GetTextLanguage("tienich"), value: 6 },
        { text: GetTextLanguage("denghivattu"), value: 7 },
        { text: GetTextLanguage("denghitaichinh"), value: 8 },
        { text: GetTextLanguage("denghiccdc"), value: 9 },
        { text: GetTextLanguage("denghithietbi"), value: 10 },
        { text: GetTextLanguage("quyenkhac"), value: 11 },
    ];
    $("#TinhTrangCT").data("kendoDropDownList").value(2);
    //Danh Sach cong trinh
    var datasourceCT = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/PhanQuyen/GETDMPhanQuyen",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {
                    LoaiHinh: 2
                }
            },
            create: {
                url: "/PhanQuyen/AddorUpdatePhanQuyen",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid_DMQuyen").data("kendoGrid").dataSource.read({
                        LoaiHinh: $("#TinhTrangCT").val()
                    });
                }
            },
            update: {
                url: "/PhanQuyen/AddorUpdatePhanQuyen",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid_DMQuyen").data("kendoGrid").dataSource.read({ LoaiHinh: $("#TinhTrangCT").val() });
                }
            },
            destroy: {
                url: "/PhanQuyen/Destroy_Quyen",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    $("#grid_DMQuyen").data("kendoGrid").dataSource.read({ LoaiHinh: $("#TinhTrangCT").val() });
                }
            },
            parameterMap: function (options, type) {
                console.log(options);
                var model = {};
                if (type === "read") {
                    return JSON.stringify(options);
                }
                else if (type === "create" || type === "update") {
                    model = {
                        GroupGuid: options.models[0].GroupGuid,
                        Code: $("#Code").val(),
                        GroupName: $("#GroupName").val(),
                        GroupParent: $("#Parent").val(),
                        Icon: $("#Icon").val(),
                        KeyLanguage: $("#Language").val(),
                        LoaiHinh: $("#LoaiHinh").val(),
                        Nhom: $("#Nhom").val(),
                        RuleType: $("#RuleType").val(),
                        STT2: $("#STT").val(),
                        UrlLink: $("#UrlLink").val()
                    }
                    return { data: kendo.stringify(model) };
                }
                else {
                    return { GroupGuid: options.models[0].GroupGuid };
                }
                
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        schema: {
            data: "data",
            total: "total",
            model: {
                id: "GroupGuid",
                fields: {
                    GroupGuid: { type: "string" }
                },
            }
        }
    });
    $("#grid_DMQuyen").kendoGrid({
        width: "100%",
        height: heightGrid * 0.85,
        dataSource: datasourceCT,
        filterable: {
            mode: "row"
        },
        toolbar: [
               { name: "create", text: GetTextLanguage("them") }
        ],
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        sortable: false,
        pageable:
            {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
        columns: [
            {
                field: "STT",
                title: GetTextLanguage("stt"),
                width: 50,
                filterable: FilterInColumn,
                attributes: alignCenter,
            },
            {
                field: "GroupName",
                title: GetTextLanguage("tenquyen"),
                width: 150,
                filterable: FilterInColumn,
                attributes: alignLeft,
            },
            {
                field: "Code",
                title: "Code",
                width: 150,
                filterable: FilterInColumn,
                attributes: alignCenter,
            },
            {
                field: "RuleType",
                title: "RuleType",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignCenter,
            },
            {
                field: "UrlLink",
                title: "UrlLink",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            {
                field: "KeyLanguage",
                title: "KeyLanguage",
                width: 150,
                filterable: FilterInColumn,
                attributes: alignCenter,
            },
            {
                field: "STT2",
                title: "Order",
                width: 50,
                filterable: FilterInColumn,
                attributes: alignCenter,
            }
        ],
        editable: {
            mode: "popup",
            window: {
                title: GetTextLanguage("thongtin"),
                width: 600
            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes",
            template: kendo.template($("#popup_editor").html())
        },
        edit: function (e) {
            if (e.model.get("GroupGuid") != null && e.model.get("GroupGuid") != "") {
                e.container.find(".k-edit-buttons").append('<a class="k-button k-button-icontext k-grid-delete text-red" href="javascript:void(0)"><span class="k-icon k-i-close"></span>' + GetTextLanguage("xoa") + '</a>');
                e.container.find(".k-button.k-grid-delete").on("click", function (e) {
                    var uid = $(e.target).closest(".k-popup-edit-form").data("uid");
                    var dataRow = $('#grid_DMQuyen').data("kendoGrid").dataSource.getByUid(uid);
                    $('#grid_DMQuyen').data("kendoGrid").dataSource.remove(dataRow);
                    $('#grid_DMQuyen').data("kendoGrid").dataSource.sync();
                });
            }
            $("#LoaiHinh").kendoDropDownList({
                dataSource: loaihopdong,
                dataTextField: "text",
                dataValueField: "value",
                autoBind: true,
                filter: "contains"
            });
            $("#Nhom").kendoDropDownList({
                dataSource: nhom,
                dataTextField: "text",
                dataValueField: "value",
                autoBind: true,
                filter: "contains"
            });
            $("#Parent").kendoDropDownList({
                dataTextField: 'GroupName',
                dataValueField: 'GroupGuid',
                filter: "contains",
                optionLabel: "Chọn quyền cha",
                dataSource: getDataDroplitwithParam("/PhanQuyen/", "DSQuyenParent", {}),

            });

        },
        dataBound: function (e) {
            var grid = this;
            var data = grid.dataSource.data();
            $.each(data, function (i, row) {
                if (row.IsHeader) {
                    var element = $('tr[data-uid="' + row.uid + '"] ');
                    $(element).addClass("backgroundisheader");
                } else {
                    if (row.GroupParent == null) {
                        var element = $('tr[data-uid="' + row.uid + '"] ');
                        $(element).addClass("backgroudisparent");
                    }
                }
            });
        },
    });
    
    $("#grid_DMQuyen .k-grid-content").on("dblclick", "td", function () {
        var rowct = $(this).closest("tr");
        if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            $("#grid_DMQuyen").data("kendoGrid").editRow(rowct);
        }
    });

    $(".btnTemplate").on("click", function () {
        location.href = '/QuanLyCongTrinh/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();

    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "80%",
                    height: innerHeight * 0.7,
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                var datasourceImport = new kendo.data.DataSource({
                    data: data,
                    type: "json",
                    batch: true,
                    pageSize: 50,
                    sort: [{ field: "TenCongTrinh", dir: "asc" }],
                    schema: {
                        data: "data",
                        total: "total",
                        model: {
                            fields: {
                                NgayKhoiTao: { type: "date", format: "{0:dd/MM/yyyy}" },
                                NgayKhoiCongDuKien: { type: "date", format: "{0:dd/MM/yyyy}" },
                                NgayHoanThanhDuKien: { type: "date", format: "{0:dd/MM/yyyy}" },
                                TinhTrang: { type: "string", editable: false }
                            }
                        }
                    }
                });
                var grid = $("#gridthem").kendoGrid({
                    dataSource: datasourceImport,
                    editable: true,
                    height: innerHeight * 0.5,
                    width: "100%",
                    dataBound: function () {
                        var rows = this.items();
                        dem = 0;
                        $(rows).each(function () {
                            dem++;
                            var rowLabel = $(this).find(".stt");
                            $(rowLabel).html(dem);
                            if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                                dem = 0;
                            }
                        });
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 60,
                        },
                        {
                            field: "NgayKhoiTao",
                            title: GetTextLanguage("ngaykhoitao"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: 150,
                            format: "{0:dd/MM/yyyy}"

                        },
                         {
                             field: "TenCongTrinh",
                             title: GetTextLanguage("tencongtrinh"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 200

                         },
                         {
                             field: "ChuDauTu",
                             title: GetTextLanguage("kieuphongban"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 200

                         },
                         {
                             field: "NgayKhoiCongDuKien",
                             title: GetTextLanguage("ngaykhoicongdukien"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 150,
                             format: "{0:dd/MM/yyyy}"

                         },
                         {
                             field: "NgayHoanThanhDuKien",
                             title: GetTextLanguage("ngayhoanthanhdukien"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 150,
                             format: "{0:dd/MM/yyyy}"
                         },
                         {
                             field: "TinhTrang",
                             title: GetTextLanguage("tinhtrang"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 150,

                         },
                         {
                             field: "DiaDiem",
                             title: GetTextLanguage("diadiem"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 200

                         },
                         {
                             field: "GhiChu",
                             title: GetTextLanguage("ghichu"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 200

                         },
                        { command: "destroy", title: "&nbsp;", width: "150px" }
                    ],

                });
                $(".btn-import").click(function () {
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/QuanLyCongTrinh/ImportCongTrinh",
                        method: "post",
                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()) },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#grid_DSCongTrinh").data("kendoGrid").dataSource.read({ LoaiHinh: DaKy, TrangThai: $("#TinhTrangCT").val() });
                                $("#grid_DSCongTrinh").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    closeModel("mdlThemFileExcel");
                });
            }
        }
        objXhr3.open("POST", "/QuanLyCongTrinh/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
        //////////////////////////////////////////////////////////////////
    });
});