﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/QuanLyDoiTac";
    var dataSourceGrid;
    var dataSourceCongTrinh;
    var tlb_drlCongTrinh;
    var tlb_drlLoaiHinh;
    var timeoutkeysearch = null;
    var mdlChooseForm;
    var mdlThietBi;
    var LoaiHinhThietBi = 0;//0:của công ty - 1:đi thuê
    var actionThietBi = { add: 10, edit: 15 };

    var dataLoaiHinhTTCN = [
        { text: "Người mua thanh toán tiền hàng", val: 1 },
        { text: "Thanh toán cho người bán", val: 2}
    ]
    var dataKieuDoiTac = [
        { text: "Đối tác mua", val: 1 },
        { text: "Đối tác bán", val: 2 },
    ]
    var dates = new Date(), years = dates.getFullYear(), months = dates.getMonth(), days = dates.getDay();
    var dataPhongBan = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/read",
                type: "post",
                data: {  },
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                data: { TuNgay: '01/01/' + years, DenNgay: dates.toLocaleDateString("en-US") }
            },
            parameterMap: function (data, type) {
                if (type !== "read") {
                    if (type == "create") {
                        var model = {
                            TenDoi: data.models[0].TenDoi
                        }
                        return { data: kendo.stringify(model) }
                    }
                    else {
                        return { data: kendo.stringify(data.models[0]) }
                    }
                } else {
                    return JSON.stringify(data);
                }
            }
        },
        pageSize: 25,
        batch: true,
        sort: [{ field: "TenDoi", dir: "asc" }],
        schema: {
            type: 'json',
            data: 'data',
            total: 'total',
            model: {
                id: "DoiThiCongID",
                fields: {
                    DoiThiCongID: { editable: false, nullable: true },
                    TenDoiTac: { type: "string" },
                    TongChi: { type: "number" },
                    TongThu: { type: "number" },
                    ChenhLech: { type: "number" }
                }
            }
        },
    });

    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataPhongBan,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: [25, 50, 100],
            messages: messagegrid
        },
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        height: window.innerHeight * 0.85,
        width: "100%",
        toolbar: [
            { template: kendo.template($("#templtool").html()) },
        ],
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "KieuDoiTac",
                title: "Kiểu đối tác",
                filterable: {
                    cell: {
                        template: function (args) {
                            // create a DropDownList of unique values (colors)
                            args.element.kendoDropDownList({
                                dataSource: [{ id: 1, text:"Đối tác mua" }, { id: 2, text: "Đối tác bán" }],
                                dataTextField: "text",
                                dataValueField: "id",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                template: kendo.template($("#tempKieuDoiTac").html()),
                attributes: { style: "text-align:center;" }
            },
            {
                field: "TenDoiTac", title: GetTextLanguage("tendoitac"), filterable: FilterInTextColumn, attributes: { style: "text-align:left;" }
            },
            {
                field: "MaDoiTac", title: "Mã đối tác", filterable: FilterInTextColumn, attributes: { style: "text-align:center;" }
            },
            {
                field: "TongThu", title: "Thực thu", filterable: false, attributes: { style: "text-align:right;color: green;font-weight: bold;" }, format: "{0:n2}"
            },
            {
                field: "TongChi", title: "Thực chi", filterable: false, attributes: { style: "text-align:right;color: red;font-weight: bold;" }, format: "{0:n2}"
            },
            {
                field: "ThanhToanCongNo", title: "Đã thanh toán", filterable: false, attributes: { style: "text-align:right;color: violet;font-weight: bold;" }, format: "{0:n2}"
            },
            {
                field: "ChenhLenh", title: "Còn lại", filterable: false, attributes: { style: "text-align:right;color: blue;font-weight: bold;" }, format: "{0:n2}"
            },

        ],
    });
    $("#inputTinhCongNoTuNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(years, 0, 1), change: function () {
            gridReload("grid", { TuNgay: $("#inputTinhCongNoTuNgay").val(), DenNgay: $("#inputTinhCongNoDenNgay").val() });
        }
    });
    $("#inputTinhCongNoDenNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(), change: function () {
            gridReload("grid", { TuNgay: $("#inputTinhCongNoTuNgay").val(), DenNgay: $("#inputTinhCongNoDenNgay").val() });
        }
    });
    $("#grid .k-grid-content").on("dblclick", "td", function () {
        var rowct = $(this).closest("tr"),
            grid = $("#grid").data("kendoGrid"),
            dataItem = grid.dataItem(rowct);
        if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            var mdl = $('#mdlChiTiet').kendoWindow({
                width: "90%",
                height: "85%",
                title: GetTextLanguage("thongtinchitiet"),
                modal: true,
                visible: false,
                resizable: false,
                actions: [
                    "Close"
                ],
                deactivate: function () {
                    $(this.element).empty();
                }
            }).data("kendoWindow").center().open();
            mdl.content(kendo.template($('#tempChiTiet').html()));
            $("#tabstrip").kendoTabStrip({
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                }
            });
            var grid = $("#gridtonghoptaichinh").kendoGrid({
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: crudServiceBaseUrl + "/LayChiTietTaiChinh",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            data: { DoiThiCongID: dataItem.DoiTacID },
                        },
                        parameterMap: function (options, type) {
                            return JSON.stringify(options);
                        }
                    },
                    batch: true,
                    pageSize: 20,
                    aggregate: [{ field: "Thu", aggregate: "sum" }, { field: "Chi", aggregate: "sum" }, { field: "ChenhLenh", aggregate: "sum" }],
                    sort: [{ field: "ThoiGian", dir: "desc" }],
                    schema: {
                        type: 'json',
                        data: 'data',
                        model: {
                            fields: {
                                ThoiGian: { type: "date" },
                                Thu: { type: "number" },
                                Chi: { type: "number" },
                                ChenhLech: { type: "number" }
                            }
                        }
                    },
                }),
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    buttonCount: 5,
                    pageSizes: [25, 50, 100],
                    messages: messagegrid
                },
                height: window.innerHeight * 0.6,
                width: "100%",
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                columns: [
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                        template: "#= ++record #",
                        width: 60,
                        attributes: alignCenter
                    },
                    {
                        field: "ThoiGian", width: 100, title: GetTextLanguage("thoigian"), format: "{0:dd/MM/yyyy}", filterable: FilterInTextColumn
                    },
                    {
                        field: "LoaiHinh", width: 300, title: GetTextLanguage("noidung"), filterable: FilterInTextColumn, attributes: { style: "text-align:left;" },
                        template: kendo.template($("#tempNoiDung").html())
                    },
                    {
                        field: "NguoiTao", width: 100, title: GetTextLanguage("nguoitao"), filterable: FilterInTextColumn, attributes: { style: "text-align:left;" }
                    },
                    {
                        field: "Thu", width: 100, title: "Thực thu", filterable: FilterInColumn, attributes: { style: "text-align:right;color: green;font-weight: bold;" }, format: "{0:n2}",
                        footerTemplate: "<div style='color:green;text-align:right;'>#=kendo.toString(sum, 'n2')#</div>",
                    },
                    {
                        field: "Chi", width: 100, title: "Thực chi", filterable: FilterInColumn, attributes: { style: "text-align:right;color: red;font-weight: bold;" }, format: "{0:n2}",
                        footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n2')#</div>",
                    },
                    {
                        field: "ChenhLenh", width: 100, title: GetTextLanguage("chenhlech"), filterable: FilterInColumn, attributes: { style: "text-align:right;color: blue;font-weight: bold;" }, format: "{0:n2}",
                        footerTemplate: "<div style='color:blue;text-align:right;'>#=kendo.toString(sum, 'n2')#</div>",
                    },

                ],
            });
            var gridTTCN = $("#gridThanhToanCongNo").kendoGrid({
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: crudServiceBaseUrl + "/LayDanhSachCongNoTheoDoiTac",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            data: { DoiTacID: dataItem.DoiTacID },
                        },
                        parameterMap: function (options, type) {
                            return JSON.stringify(options);
                        }
                    },
                    batch: true,
                    pageSize: 20,
                    aggregate: [{ field: "SoTien", aggregate: "sum" }],
                    sort: [{ field: "ThoiGian", dir: "desc" }],
                    schema: {
                        type: 'json',
                        data: 'data',
                        model: {
                            fields: {
                                ThanhToanTienID: { editable: false, nullable: true },
                            }
                        }
                    },
                }),
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    buttonCount: 5,
                    pageSizes: [25, 50, 100],
                    messages: messagegrid
                },
                height: window.innerHeight * 0.7,
                toolbar: [{ template: kendo.template($("#btnThemCongNo").html()) }],
                width: "100%",
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                columns: [
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                        width: 60,
                        attributes: alignCenter,
                        template: "#= ++record #",
                    },
                    {
                        field: "ThoiGian",
                        title: "Thời gian thanh toán",
                        width: 90,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                        template: formatToDate("ThoiGian"),
                    },
                    {
                        field: "LoaiHinh",
                        title: "Loại hình thanh toán",
                        width: 120,
                        filterable: {
                            cell: {
                                template: function (args) {
                                    // create a DropDownList of unique values (colors)
                                    args.element.kendoDropDownList({
                                        dataSource: dataLoaiHinhTTCN,
                                        dataTextField: "text",
                                        dataValueField: "val",
                                        valuePrimitive: true
                                    });
                                },
                                showOperators: false
                            }
                        },
                        template: kendo.template($("#tempLoaiHinhTTCN").html()),
                        attributes: { style: "text-align:center;" }
                    },
                    {
                        field: "NoiDung",
                        title: "Nội dung thanh toán",
                        filterable: FilterInTextColumn,
                        attributes: { style: "text-align:left;" },
                        width: 200
                    },
                    {
                        field: "SoTien",
                        title: "Số tiền thanh toán",
                        filterable: FilterInTextColumn,
                        attributes: { style: "text-align:right;" },
                        format: "{0:n2}",
                        width: 120,
                        footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n2')#</div>",
                    },
                    {
                        field: "TenNhanVien",
                        title: "Người tạo",
                        width: 100,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                    },
                ],
            });
            $("#gridThanhToanCongNo").on("click", "#btnAddTTCN", function () {
                CreateModalWithSize("mdlTTCN", "30%", "60%", "tplTTCN", "Thêm mới chi tiết thanh toán");
                $("#ThoiGian").kendoDatePicker({
                    format: "dd/MM/yyyy",
                    value: new Date()
                });
                $("#SoTien").autoNumeric({
                    digitGroupSeparator: ',',
                    decimalCharacter: '.',
                    minimumValue: '0',
                    maximumValue: '999999999999999',
                });
                $("#LoaiHinh").kendoDropDownList({
                    dataSource: dataLoaiHinhTTCN,
                    dataTextField: "text",
                    dataValueField: "val",
                });
                btnLuuTTCN(dataItem.DoiTacID);
            });
            $("#gridThanhToanCongNo").on("dblclick", "td", function () {
                var rowtt = $(this).closest("tr"),
                    gridtt = $("#gridThanhToanCongNo").data("kendoGrid"),
                    dataItemtt = gridtt.dataItem(rowtt);
                CreateModalWithSize("mdlTTCN", "30%", "60%", "tplTTCN", "Chi tiết thanh toán");
                $("#btnXoaTTCN").removeClass("hidden");
                $.ajax({
                    url: crudServiceBaseUrl + "/LayThongTinChiTietCongNo",
                    method: "GET",
                    data: { ThanhToanTienID: dataItemtt.ThanhToanTienID },
                    success: function (data) {
                        dataobjectload = data.data[0];
                        $("#ThoiGian").kendoDatePicker({
                            format: "dd/MM/yyyy",
                            value: new Date()
                        });
                        $("#SoTien").autoNumeric({
                            digitGroupSeparator: ',',
                            decimalCharacter: '.',
                            minimumValue: '0',
                            maximumValue: '999999999999999',
                        });
                        $("#LoaiHinh").kendoDropDownList({
                            dataSource: dataLoaiHinhTTCN,
                            dataTextField: "text",
                            dataValueField: "val",
                        });
                        var viewModelTTCN = kendo.observable({
                            ThanhToanID: dataobjectload == null ? "00000000-0000-0000-0000-000000000000" : dataobjectload.ThanhToanTienID,
                            NoiDung: dataobjectload == null ? "" : dataobjectload.NoiDung,
                            SoTien: dataobjectload == null ? "" : dataobjectload.SoTien,
                            ThoiGian: dataobjectload == null ? "" : dataobjectload.ThoiGian,
                            LoaiHinh: dataobjectload == null ? "" : dataobjectload.LoaiHinh
                        });
                        kendo.bind($("#frmTTCN"), viewModelTTCN);
                        showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                        btnLuuTTCN(dataItem.DoiTacID);
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                    }
                });
                $("#btnXoaTTCN").click(function () {
                    var x = confirm("Bạn có chắc chắn muốn xóa chi tiết này không?");
                    if (x) {
                        ContentWatingOP("mdlTTCN", 0.9);
                        $.ajax({
                            url: crudServiceBaseUrl + "/XoaChiTietCongNo",
                            method: "POST",
                            data: { ThanhToanTienID: dataItemtt.ThanhToanTienID },
                            success: function (data) {
                                showToast(data.code, data.code, data.message);
                                if (data.code == "success") {
                                    $("#mdlTTCN").unblock();
                                    $(".windows8").css("display", "none");
                                    closeModel("mdlTTCN");
                                    gridReload("gridThanhToanCongNo", { DoiTacID: dataItem.DoiTacID });
                                    gridReload("grid", {});
                                }
                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                $("#mdlTTCN").unblock();
                                $(".windows8").css("display", "none");
                                gridReload("gridThanhToanCongNo", { DoiTacID: dataItem.DoiTacID });
                                gridReload("grid", {});
                            }
                        });
                    }
                })
            });
            $("#chitiet").html($("#templThemSua").html());
            $.ajax({
                type: "POST",
                url: crudServiceBaseUrl + "/LayChiTietDoiTac",
                data: { DoiThiCongID: dataItem.DoiTacID },
                success: function (data) {
                    var viewModeldc = kendo.observable({
                        TenDoi: data.data.TenDoiTac,
                        SDT: data.data.SDT,
                        Email: data.data.Email,
                        DiaChi: data.data.DiaChi,
                        KieuDoiTac: data.data.KieuDoiTac,
                        MaSoThue: data.data.MaSoThue
                    });
                   
                    $("#xoaDoiTac").removeClass("hidden");
                   
                    kendo.bind($("#frmDoiTac"), viewModeldc);
                    var validatordc = $("#frmDoiTac").kendoValidator().data("kendoValidator");

                    $("#KieuDoiTac").kendoDropDownList({
                        dataSource: dataKieuDoiTac,
                        dataTextField: "text",
                        dataValueField: "val",
                    });

                    var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
                    $("#inputTuNgay").kendoDatePicker({
                        format: "dd/MM/yyyy", value: new Date(y, m, 1), change: function () {
                            gridReload("gridtonghoptaichinh", { DoiThiCongID: dataItem.DoiTacID, Loai: 2, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
                        }
                    });
                    $("#inputDenNgay").kendoDatePicker({
                        format: "dd/MM/yyyy", value: new Date(), change: function () {
                            gridReload("gridtonghoptaichinh", { DoiThiCongID: dataItem.DoiTacID, Loai: 2, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
                        }
                    });
                    $("#xoaDoiTac").click(function () {
                        $(".windows8").css("display", "block");
                        var x = confirm("Bạn có chắc chắn muốn xóa không?");
                        if (x) {
                            $.ajax({
                                type: "POST",
                                url: crudServiceBaseUrl + "/XoaDoiTac",
                                data: { DoiThiCongID: dataItem.DoiTacID },
                                success: function (data) {
                                    altReturn(data);
                                    gridReload("grid", {});
                                    closeModel('mdlChiTiet');
                                    $(".windows8").css("display", "none");
                                    $("#mdlChiTiet").unblock();
                                },
                                error: function (xhr, status, error) {
                                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                    $(".windows8").css("display", "none");
                                    $("#mdlChiTiet").unblock();
                                }
                            });
                        }
                    });
                    $(".k-grid-update").click(function () {
                        if (validatordc.validate()) {
                            $(".windows8").css("display", "block");
                            var objectData = {
                                DoiTacID: dataItem.DoiTacID,
                                TenDoiTac: $("#TenDoi").val(),
                                DiaChi: $("#DiaChi").val(),
                                SDT: $("#SDT").val(),
                                Email: $("#Email").val(),
                                NgayBatDau: $("#DiaChi").val(),
                                KieuDoiTac: $("#KieuDoiTac").val(),
                                MaSoThue: $("#MaSoThue").val(),
                            }
                            ContentWatingOP("mdlChiTiet", 0.9);
                            setTimeout(function () {
                                $.ajax({
                                    type: "POST",
                                    url: crudServiceBaseUrl + "/create",
                                    data: { data: JSON.stringify(objectData) },
                                    success: function (data) {
                                        altReturn(data);
                                        gridReload("grid", {});
                                        closeModel('mdlChiTiet');
                                        $(".windows8").css("display", "none");
                                        $("#mdlChiTiet").unblock();
                                    },
                                    error: function (xhr, status, error) {
                                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                        $(".windows8").css("display", "none");
                                        $("#mdlChiTiet").unblock();
                                    }
                                });
                            }, 3000);
                        }
                        else {
                            $("#mdlChiTiet").unblock();
                        }
                    })
                    $(".chontatca").click(function () {
                        $("#inputTuNgay").data("kendoDatePicker").enable(false);
                        $("#inputDenNgay").data("kendoDatePicker").enable(false);
                        gridReload("gridtonghoptaichinh", { DoiThiCongID: dataItem.DoiTacID, Loai: 1, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
                    })
                    $(".chonngay").click(function () {
                        $("#inputTuNgay").data("kendoDatePicker").enable(true);
                        $("#inputDenNgay").data("kendoDatePicker").enable(true);
                        gridReload("gridtonghoptaichinh", { DoiThiCongID: dataItem.DoiTacID, Loai: 2, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
                    })
                },
                error: function (xhr, status, error) {
                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                }
            })
        }
    });
    $("#grid .k-grid-add").click(function () {
        var mdl = $('#mdlThem').kendoWindow({
            width: "30%",
            height: "500px",
            title: GetTextLanguage("thongtinchitiet"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#templThemSua').html()));
        var viewModeldc = kendo.observable({
        });
        kendo.bind($("#frmDoiTac"), viewModeldc);
        var validatordc = $("#frmDoiTac").kendoValidator().data("kendoValidator");
        $("#KieuDoiTac").kendoDropDownList({
            dataSource: dataKieuDoiTac,
            dataTextField: "text",
            dataValueField: "val",
        });
        $(".k-grid-update").click(function () {
            if (validatordc.validate()) {
                $(".windows8").css("display", "block");
                var objectData = {
                    DoiTacID: '',
                    TenDoiTac: $("#TenDoi").val(),
                    DiaChi: $("#DiaChi").val(),
                    SDT: $("#SDT").val(),
                    Email: $("#Email").val(),
                    KieuDoiTac: $("#KieuDoiTac").val(),
                    MaSoThue: $("#MaSoThue").val()
                }
                ContentWatingOP("mdlThem", 0.9);
                setTimeout(function () {
                    $.ajax({
                        type: "POST",
                        url: crudServiceBaseUrl + "/create",
                        data: { data: JSON.stringify(objectData) },
                        success: function (data) {
                            altReturn(data);
                            gridReload("grid", {});
                            closeModel('mdlThem');
                            $(".windows8").css("display", "none");
                            $("#mdlThem").unblock();
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $(".windows8").css("display", "none");
                            $("#mdlThem").unblock();
                        }
                    });
                }, 3000);
            }
            else {
                $("#mdlThem").unblock();
            }
        })
    });

    $(".btnTemplate").on("click", function () {
        location.href = '/QuanLyDoiTac/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();

    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "80%",
                    height: innerHeight * 0.7,
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                var datasourceImport = new kendo.data.DataSource({
                    data: data,
                    type: "json",
                    batch: true,
                    pageSize: 50,
                    sort: [{ field: "TenDoiTac", dir: "asc" }],
                    schema: {
                        data: "data",
                        total: "total",
                        model: {
                            fields: {

                            }
                        }
                    }
                });
                var grid = $("#gridthem").kendoGrid({
                    dataSource: datasourceImport,
                    editable: true,
                    height: innerHeight * 0.5,
                    width: "100%",
                    dataBound: function () {
                        var rows = this.items();
                        dem = 0;
                        $(rows).each(function () {
                            dem++;
                            var rowLabel = $(this).find(".stt");
                            $(rowLabel).html(dem);
                            if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                                dem = 0;
                            }
                        });
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 60,
                        },
                        {
                            field: "TenDoiTac",
                            title: GetTextLanguage("tendoitac"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: 200,

                        },
                        {
                            field: "Sdt",
                            title: GetTextLanguage("sdt"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: 150

                        },
                        {
                            field: "Email",
                            title: GetTextLanguage("email"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: 150

                        },
                        {
                            field: "DiaChi",
                            title: GetTextLanguage("diachi"),
                            filterable: FilterInTextColumn,
                            attributes: alignCenter,
                            width: 150,
                        },

                        { command: "destroy", title: "&nbsp;", width: "150px" }
                    ],

                });
                $(".btn-import").click(function () {
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/QuanLyDoiTac/ImportDoiTac",
                        method: "post",
                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()) },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#grid").data("kendoGrid").dataSource.read();
                                $("#grid").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    closeModel("mdlThemFileExcel");
                });
            }
        }
        objXhr3.open("POST", "/QuanLyDoiTac/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
    });

    //function
    function getDataSourceGrid(url, param, groups) {
        var dataSources = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + url,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: param
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            batch: true,
            pageSize: 20,
            schema: {
                data: 'data',
                total: 'total'
            }
        });

        return dataSources;
    }
    function getDataDroplit(crudServiceBaseUrl, url) {
        var dataSourceObj;
        dataSourceObj = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + url,
                    dataType: "json",
                    type: "get"
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        });
        return dataSourceObj;
    }
    function getDataDroplitwithParam(crudServiceBaseUrl, url, param) {
        var dataSourceObj;
        dataSourceObj = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + url,
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: param
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        });
        return dataSourceObj;
    }
    function getHeigtContent(grid) {
        if (grid == 1) { return $("#mdlChiTiet").height() - 80; } // tab thông tin
        else if (grid == 2) { //tab điều chuyển
            return $("#mdlChiTiet").height() - 101 - 37 - 96 - 60;

        }
        else {
            return $("#mdlChiTiet").height() - 101 - 37 - 96; //tab còn lại

        }
    }
    $(window).resize(function () {
        resizeContent();
    });
    function resizeContent() {
        $("#mdlfrmThietBi").css("height", getHeigtContent(1));
        $("#mdlLichSu .k-grid-content").css("height", getHeigtContent(3));
        $("#mdlDieuChuyen .k-grid-content").css("height", getHeigtContent(2));
        $("#mdlBaoCaoSuaChua .k-grid-content").css("height", getHeigtContent(3));
    }
    function showToast(loai, title, message) {
        toastr.options.positionClass = "toast-bottom-right";
        toastr[loai](message, title, { timeOut: 3000 });
    }
    function altReturn(data) {
        if (data.code == "success") {
            showToast("success", GetTextLanguage("thanhcong"), data.message);
        }
        else {
            showToast("error", GetTextLanguage("thatbai"), data.message);
        }
    }
    function gridReload(grid, parram) {
        $('#' + grid).data('kendoGrid').dataSource.read(parram);
        $('#' + grid).data('kendoGrid').refresh();
    }
    function closeModel(idform) {
        $('#' + idform).data("kendoWindow").close();
    }
    function btnLuuTTCN(Doitacid) {
        $("#btnLuuTTCN").click(function () {
            var validator = $("#frmTTCN").kendoValidator().data("kendoValidator");
            if (validator.validate()) {
                var dataTTCN = {
                    ThanhToanTienID: $("#ThanhToanID").val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#ThanhToanID").val(),
                    ThoiGian: $("#ThoiGian").val(),
                    NoiDung: $("#NoiDung").val(),
                    SoTien: $("#SoTien").autoNumeric("get"),
                    DoiTacID: Doitacid,
                    LoaiHinh: $("#LoaiHinh").val()
                }
                ContentWatingOP("mdlTTCN", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/AddOrUpdateTTCN",
                    method: "post",
                    data: { data: JSON.stringify(dataTTCN) },
                    success: function (data) {
                        showToast(data.code, data.code, data.message);
                        if (data.code == "success") {
                            $("#mdlTTCN").unblock();
                            $(".windows8").css("display", "none");
                            closeModel("mdlTTCN");
                            gridReload("gridThanhToanCongNo", { DoiTacID: Doitacid });
                            gridReload("grid", {});
                        } else {
                            $("#mdlTTCN").unblock();
                        }
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlTTCN").unblock();
                        $(".windows8").css("display", "none");
                        gridReload("gridThanhToanCongNo", { DoiTacID: Doitacid });
                        gridReload("grid", {});
                    }
                });
            }
        });
    }
});