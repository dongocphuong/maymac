﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/QuanLyVanBanDinhKem";
    var dataimg = new FormData();
    var lstHinhAnh = [];
    var dataLoaiVanBan = [
        { text: "Hợp đồng, Phụ lục", val: 1 },
        { text: "Văn bản", val: 2 },
        { text: "Nội quy cơ chế", val: 3 }
    ]
    //Build grid
    var datagrid = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetAllData",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {}
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "VanBanID",
                fields: {
                    VanBanID: { editable: false, nullable: true },
                }
            }
        },
    });
    var grid = $("#grid").kendoGrid({
        dataSource: datagrid,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [{ template: kendo.template($("#btnThem").html()) }],
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
            {
                field: "ThoiGianTao",
                title: "Thời gian tạo",
                width: 120,
                attributes: alignCenter,
                filterable: false,
                template: formatToDate("ThoiGianTao"),
            },
            {
                field: "LoaiVanBan",
                title: "Loại văn bản",
                width: 100,
                filterable: {
                    cell: {
                        template: function (args) {
                            // create a DropDownList of unique values (colors)
                            args.element.kendoDropDownList({
                                dataSource: dataLoaiVanBan,
                                dataTextField: "text",
                                dataValueField: "val",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                template: kendo.template($("#tempLoaiVanBan").html()),
                attributes: { style: "text-align:center;" }
            },
            {
                field: "TieuDe",
                title: "Tiêu đề",
                width: 120,
                attributes: alignLeft,
                filterable: FilterInTextColumn,
            },
            {
                field: "NoiDung",
                title: "Nội dung",
                width: 120,
                attributes: alignLeft,
                filterable: FilterInTextColumn,
            },
            {
                field: "TenNhanVien",
                title: "Tên người tạo",
                width: 120,
                attributes: alignLeft,
                filterable: FilterInTextColumn,
            },
        ],
    });
    //Chi tiết của grid
    $("#grid").on("dblclick", "td", function () {
        var rows = $(this).closest("tr"),
            grids = $("#grid").data("kendoGrid"),
            dataItems = grids.dataItem(rows);
        CreateModalWithSize("mdlChiTietVanBan", "30%", "60%", "tplChiTietVanBan", "Chi tiết văn bản");
        $.ajax({
            url: crudServiceBaseUrl + "/GetDatabyVanBanID",
            method: "GET",
            data: { VanBanID: dataItems.VanBanID },
            success: function (data) {
                $("#VanBanID").val(data.data.VanBanID);
                $("#TieuDe").val(data.data.TieuDe);
                $("#NoiDung").val(data.data.NoiDung);
                $("#LoaiVanBan").val(data.data.LoaiVanBan);
                showToast("success", "Thành công", "Lấy dữ liệu thành công");
                //Show file đình kèm ra màn hình
                $.ajax({
                    url: crudServiceBaseUrl + '/LayDanhSachHinhAnhbyVanBanID?VanBanID=' + dataItems.VanBanID,
                    method: 'GET',
                    success: function (data) {
                        lstHinhAnh = [];
                        var lst = data.data;
                        if (lst.length > 0 && lst[0] != "") {
                            for (var i = 0; i < lst.length; i++) {
                                lstHinhAnh.push({ img: lst[i].img, name: lst[i].name });
                            }
                            var htmls = "";
                            for (var i = 0; i < lstHinhAnh.length; i++) {
                                htmls += '<tr class="imgslider">';
                                htmls += '<td>' + lstHinhAnh[i].name + '</td>'
                                htmls += '<td><a href="' + localQLVanBan + lstHinhAnh[i].img + '" data-file="' + lstHinhAnh[i].img + '" download>' + GetTextLanguage("taixuong") + '</a></td>';
                                htmls += '<td><button class="closeImgLoad btn btn-danger" data-file="' + lstHinhAnh[i].img + '">' + GetTextLanguage("xoa") + '</a></td>'
                                htmls += '</tr>';
                            }
                            $('#lstImageDisplay tbody').append(htmls);
                            $("#lstImageDisplay").on("click", ".closeImgLoad", function () {
                                var file = $(this).data("file");
                                for (var i = 0; i < lstHinhAnh.length; i++) {
                                    if (lstHinhAnh[i].img === file) {
                                        lstHinhAnh.splice(i, 1);
                                        break;
                                    }
                                }
                                var list = dataimg.getAll("uploads");
                                dataimg = new FormData();
                                for (var i = 0; i < list; i++) {
                                    if (list[i].name === file) {

                                    } else {
                                        dataimg.append("uploads", list[i]);
                                    }
                                }
                                $(this).closest("tr").remove();
                            });
                            showslideimg5($("#lstImageDisplay"));
                        }
                    },
                })
            },
            error: function (xhr, status, error) {
                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
            }
        });
        $("#btnXoa").removeClass("hidden");
        //Xóa một chi tiết của grid
        $("#btnXoa").click(function () {
            var x = confirm("Bạn có chắc chắn muốn xóa văn bản này không?");
            if (x) {
                ContentWatingOP("mdlChiTietVanBan", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/XoaVanBan",
                    method: "POST",
                    data: { VanBanID: dataItems.VanBanID },
                    success: function (data) {
                        showToast("success", "Thành công", "Xóa thành công");
                        if (data.code == "success") {
                            $("#mdlChiTietVanBan").unblock();
                            $(".windows8").css("display", "none");
                            closeModel("mdlChiTietVanBan");
                            gridReload("grid", {});
                        }
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlChiTietVanBan").unblock();
                        $(".windows8").css("display", "none");
                        gridReload("grid", {});
                    }
                });
            }
        });
        btnLuu();
    });
    //Thêm mới một chi tiết vào grid
    $("#grid").on("click", "#btnAdd", function () {
        CreateModalWithSize("mdlChiTietVanBan", "30%", "60%", "tplChiTietVanBan", "Thêm mới văn bản");
        btnLuu();
    });
    //function
    function btnLuu() {
        $("#LoaiVanBan").kendoDropDownList({
            dataSource: dataLoaiVanBan,
            dataTextField: "text",
            dataValueField: "val",
        });
        //File đính kèm
        dataimg = new FormData();
        $("#upLoadFile").change(function () {
            loadImage_new2(this, 'tbody');
            for (var x = 0; x < this.files.length; x++) {
                dataimg.append("uploads", this.files[x]);
            }
            $("#upLoadFile").val('').clone(true);
            $("#lstImageDisplay").on("click", ".closeImgLoad", function () {
                var file = $(this).data("file");
                for (var i = 0; i < lstHinhAnh.length; i++) {
                    if (lstHinhAnh[i].img === file) {
                        lstHinhAnh.splice(i, 1);
                        break;
                    }
                }
                var list = dataimg.getAll("uploads");
                dataimg = new FormData();
                for (var i = 0; i < list.length; i++) {
                    if (list[i].name === file) {

                    } else {
                        dataimg.append("uploads", list[i]);
                    }
                }
                $(this).closest("tr").remove();
            });
            showslideimg5($("#lstImageDisplay"));
        });
        //Lưu thay đổi
        $("#btnLuu").click(function () {
            var hinhanh = "";
            var validator = $("#frmChiTietVanBan").kendoValidator().data("kendoValidator");
            if (validator.validate()) {
                //Dữ liệu để lưu
                var objData = {};
                objData.VanBanID = $("#VanBanID").val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#VanBanID").val();
                objData.TieuDe = $("#TieuDe").val();
                objData.NoiDung = $("#NoiDung").val();
                objData.LoaiVanBan = $("#LoaiVanBan").val();
                //Xử lý dữ liệu lưu file đính kèm vào máy
                var files = dataimg.getAll("uploads");
                var count = 0;
                if (files.length > 0) {
                    for (var i = 0; i < files.length; i++) {
                        $(".imgslider img").each(function (index, element) {
                            if (files[i].name == $(element).data("file")) {
                                var dataimg2 = new FormData();
                                dataimg2.append("uploads", files[i]);
                                $(".processbar", $(element).closest(".imgslider")).html('<div style="width: 0%;background: #c66161;height: 20px;"></div>');
                                $.ajax({
                                    url: '/Upload/UploadHopDong2',
                                    data: dataimg2,
                                    processData: false,
                                    contentType: false,
                                    sycn: false,
                                    type: 'POST',
                                    // this part is progress bar
                                    xhr: function () {
                                        var xhr = new window.XMLHttpRequest();
                                        xhr.upload.addEventListener("progress", function (evt) {
                                            if (evt.lengthComputable) {
                                                var percentComplete = evt.loaded / evt.total;
                                                percentComplete = parseInt(percentComplete * 100);
                                                $(".processbar div", $(element).closest(".imgslider")).attr("style", "width: " + percentComplete + "%;background: #40aa24;height: 20px;text-align: center;color: white;font-weight: bold;")
                                                $(".processbar div", $(element).closest(".imgslider")).text(percentComplete + "%");
                                            }
                                        }, false);
                                        return xhr;
                                    },
                                    success: function (data) {
                                        count++;
                                        lstHinhAnh.push({ img: data.img, name: data.name });
                                        if (count == files.length) {
                                            objData.ListHinhAnh = lstHinhAnh;
                                            ContentWatingOP("mdlChiTietVanBan", 0.9);
                                            setTimeout(function () {
                                                $.ajax({
                                                    cache: false,
                                                    async: false,
                                                    type: "POST",
                                                    url: crudServiceBaseUrl + "/AddOrUpdate",
                                                    data: { data: JSON.stringify(objData) },
                                                    success: function (data) {
                                                        altReturn(data);
                                                        $("#mdlChiTietVanBan").unblock();
                                                        if (data.code == "success") {
                                                            closeModel("mdlChiTietVanBan");
                                                            gridReload("grid", {});
                                                            return false;
                                                        }
                                                    },
                                                    error: function (xhr, status, error) {
                                                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                                    }
                                                });
                                            }, 1000);
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
                else {
                    ContentWatingOP("mdlChiTietVanBan", 0.9);
                    objData.ListHinhAnh = lstHinhAnh;
                    setTimeout(function () {
                        $.ajax({
                            cache: false,
                            async: false,
                            type: "POST",
                            url: crudServiceBaseUrl + "/AddOrUpdate",
                            data: { data: JSON.stringify(objData) },
                            success: function (data) {
                                altReturn(data);
                                $("#mdlChiTietVanBan").unblock();
                                if (data.code == "success") {
                                    closeModel("mdlChiTietVanBan");
                                    gridReload("grid", {});
                                    return false;
                                }
                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            }
                        });
                    }, 1000);
                }
            }
        });
    }
});
