﻿$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/QuanLyDonHang";
    var dataimg = new FormData();
    var currentTime = new Date();
    var dataTrangThai = [
        { text: "Chưa bắt đầu", value: 1 },
        { text: "Đang sản xuất", value: 2 },
        { text: "Hoàn thành SX", value: 3 },
        { text: "Đã giao hàng", value: 4 },
        { text: "Đã duyệt", value: 5 },
        { text: "Đã thanh toán xong", value: 6 },
        { text: "Chưa báo giá", value: 7 },
        { text: "Đã hủy", value: 8 },
    ];
    var dataTinhTrang = [
        { text: "Bình thường", value: 1 },
        { text: "Gấp", value: 2 },
        { text: "Yêu cầu đã chốt", value: 3 },
    ];
    var dataTrangThaiDonHang = [
        { text: "Đủ dữ liệu sản phẩm", value: 1 },
        { text: "Thiếu dữ liệu sản phẩm", value: 2 },
    ];
    var dataLoaiHinh = [
        { text: "May", value: 1 },
        { text: "Dệt", value: 2 },
    ];
    var loaihinhthanhtoan = [
        { text: "Tiền mặt", value: 1 },
        { text: "Chuyển khoản", value: 2 },

    ];
    var dataDonHang = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetAllDataDonHang",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: { TuNgay: $("#TuNgay").val(), DenNgay: $("#DenNgay").val() }
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        sort: [{ field: "ThoiGian", dir: "desc" }],
        pageSize: 50,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "DonHangID",
                fields: {
                    DonHangID: { editable: false, nullable: true },
                    ThoiGian: { type: "date" },
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataDonHang,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [{ template: kendo.template($("#btnThem").html()) }],
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
            {
                field: "ThoiGianThanhToanConLai",
                title: "Thời gian thanh toán còn lại",
                width: 120,
                attributes: alignCenter,
                filterable: false,
                template: kendo.template($("#ThoiGianThanhToanConLai").html()),
            },
            {
                field: "TrangThaiDonHang",
                title: "Trạng thái đơn hàng",
                filterable: FilterInTextColumn,
                filterable: {
                    cell: {
                        template: function (args) {
                            args.element.kendoDropDownList({
                                dataSource: dataTrangThaiDonHang,
                                dataTextField: "text",
                                dataValueField: "value",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                width: 150,
                template: kendo.template($("#tplTrangThaiDonHang").html()),
            },
            {
                field: "ThoiGian",
                title: "Thời gian sản xuất",
                width: 120,
                attributes: alignCenter,
                filterable: false,
                template: formatToDateTime("ThoiGian"),
            },
            {
                field: "MaDonHang",
                title: "Mã đơn hàng",
                width: 100,
                attributes: alignCenter,
                filterable: FilterInTextColumn,
            },
            {
                field: "TenDonHang",
                title: "Tên đơn hàng",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 180
            },
            {
                field: "TrangThai",
                title: "Trạng thái",
                filterable: FilterInTextColumn,
                filterable: {
                    cell: {
                        template: function (args) {
                            args.element.kendoDropDownList({
                                dataSource: dataTrangThai,
                                dataTextField: "text",
                                dataValueField: "value",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                width: 150,
                template: kendo.template($("#tplTrangThai").html())
            },
            {
                field: "TenDoiTac",
                title: "Tên đối tác",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 180
            },
            {
                field: "GiaTriDonHang",
                title: "Giá trị đơn hàng",
                filterable: false,
                attributes: alignRight,
                format: "{0:n0}",
                width: 150
            },
            {
                field: "DiaDiemGiaoHang",
                title: "Địa điểm giao hàng",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200
            },

        ],
    });
    $(".btnexcel").on("click", function () {
        var filer = [];
        var filers = $("#grid").data("kendoGrid").dataSource.filter();
        if (filers != null && filers != undefined) {
            for (var i = 0; i < filers.filters.length; i++) {
                filers.filters[i].Operator = filers.filters[i].operator;
                filer.push(filers.filters[i]);
            }
        }
        location.href = crudServiceBaseUrl + '/XuatExcelQuanLyDonHang?filters=' + JSON.stringify(filer) + "&TuNgay=" + $("#TuNgay").val() + "&DenNgay=" + $("#DenNgay").val();

    });
    $("#grid").on("dblclick", "td", function () {
        var rows = $(this).closest("tr"),
            grids = $("#grid").data("kendoGrid"),
            dataItems = grids.dataItem(rows);
        if (!rows.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            ThemSuaDonHang(dataItems);
        }
    });
    $("#grid").on("click", "#btnAdd", function () {
        ThemSuaDonHang(null);
    });
    $("#TuNgay").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date("01/01/" + currentTime.getFullYear()),
    });
    $("#DenNgay").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date()
    });
    $("#btnTimKiem").click(function () {
        $("#grid").data("kendoGrid").dataSource.read({ TuNgay: $("#TuNgay").val(), DenNgay: $("#DenNgay").val() });
    })
    function ThemSuaDonHang(dataitem) {
        CreateModalWithSize("mdlChiTietDonHang", "100%", "95%", "tplChiTietDonHang", "Chi tiết đơn hàng");
        lstHinhAnh = [];
        if (dataitem == null) {
            $("#MaDonHang").closest(".form-group").addClass("hidden");
            $("#TrangThai").closest(".form-group").addClass("hidden");
        }
        if (dataitem != null) {
            $("#XoaDonHang").removeClass("hidden");
            $("#XuatExcel").removeClass("hidden");
            var viewModel = kendo.observable({
                TenDonHang: dataitem == null ? "" : dataitem.TenDonHang,
                MaDonHang: dataitem == null ? "" : dataitem.MaDonHang,
                DoiTacID: dataitem == null ? "" : dataitem.DoiTacID,
                TrangThai: dataitem == null ? "" : dataitem.TrangThai,
                DiaDiemGiaoHang: dataitem == null ? "" : dataitem.DiaDiemGiaoHang,
                SoTienDaThanhToan: dataitem == null ? "" : dataitem.SoTienDaThanhToan,
                LoaiHinhThanhToan: dataitem == null ? "" : dataitem.LoaiHinhThanhToan,
                YeuCauKhac: dataitem == null ? "" : dataitem.YeuCauKhac,
                EmailKeToan: dataitem == null ? "" : dataitem.EmailKeToan,
                GhiChu: dataitem == null ? "" : dataitem.GhiChu,
                YeuCauKhachHangID: dataitem == null ? "" : dataitem.YeuCauKhachHangID

            });
            kendo.bind($("#frmEdit"), viewModel);
            btnXuatExcelDonHang(dataitem.DonHangID, dataitem.DoiTacID);
            $.ajax({
                url: '/QuanLyDonHang/LayDanhSachHinhAnhLayDonHang?DonHangID=' + dataitem.DonHangID,
                method: 'GET',
                success: function (data) {
                    var lst = data.data;
                    if (lst.length > 0 && lst[0] != "") {
                        for (var i = 0; i < lst.length; i++) {
                            lstHinhAnh.push({ img: lst[i].img, name: lst[i].name });
                        }
                        var htmls = "";
                        for (var i = 0; i < lstHinhAnh.length; i++) {
                            if (lstHinhAnh[i].img.match(/\.(jpg|jpeg|png|gif)$/)) {

                                htmls += '<tr class="imgslider">';
                                htmls += '<td class="hidden">';
                                htmls += '<a rel="gallery" href="' + localQLVanBan + lstHinhAnh[i].img + '">';
                                htmls += '<img src="' + localQLVanBan + lstHinhAnh[i].img + '" style="width:70px;height:70px" data-src="' + localQLVanBan + lstHinhAnh[i].img + '" class="img-responsive img-rounded media-preview" alt="" data-file="' + lstHinhAnh[i].img + '" />';
                                htmls += '</a>'
                                htmls += '<td style="width:150px"></td>'
                                htmls += '<td>' + lstHinhAnh[i].name + '</td>'
                                htmls += '<td><a href="' + localQLVanBan + lstHinhAnh[i].img + '" data-file="' + lstHinhAnh[i].img + '" download>' + GetTextLanguage("taixuong") + '</a></td>';
                                htmls += '<td><button class="closeImgLoad btn btn-danger" data-file="' + lstHinhAnh[i].img + '">' + GetTextLanguage("xoa") + '</a></td>'
                                htmls += '</tr>';
                            } else {

                                htmls += '<tr class="imgslider">';
                                htmls += '<td class="hidden">';
                                htmls += '<a rel="gallery" href="' + localQLVanBan + lstHinhAnh[i].img + '">';
                                htmls += '<img src="/content/assets/images/file.png" style="width:70px;height:70px" data-src="' + localQLVanBan + lstHinhAnh[i].img + '" class="img-responsive img-rounded media-preview" alt="" data-file="' + lstHinhAnh[i].img + '" />';
                                htmls += '</a>'
                                htmls += '<td style="width:150px"></td>'
                                htmls += '<td>' + lstHinhAnh[i].name + '</td>'
                                htmls += '<td><a href="' + localQLVanBan + lstHinhAnh[i].img + '" data-file="' + lstHinhAnh[i].img + '" download>' + GetTextLanguage("taixuong") + '</a></td>';
                                htmls += '<td><button class="closeImgLoad btn btn-danger" data-file="' + lstHinhAnh[i].img + '">' + GetTextLanguage("xoa") + '</a></td>'
                                htmls += '</tr>';
                            }

                        }
                        $('#lstImageDisplay tbody').append(htmls);
                        $("#lstImageDisplay").on("click", ".closeImgLoad", function () {
                            var file = $(this).data("file");
                            for (var i = 0; i < lstHinhAnh.length; i++) {
                                if (lstHinhAnh[i].img === file) {
                                    lstHinhAnh.splice(i, 1);
                                    break;
                                }
                            }
                            var list = dataimg.getAll("uploads");
                            dataimg = new FormData();
                            for (var i = 0; i < list; i++) {
                                if (list[i].name === file) {

                                } else {
                                    dataimg.append("uploads", list[i]);
                                }
                            }
                            $(this).closest("tr").remove();
                        });
                        showslideimg5($("#lstImageDisplay"));
                    }
                },
            })
        }
        $("#tabstrip").kendoTabStrip({
            animation: {
                open: {
                    effects: "fadeIn"
                }
            }
        });
        $("#DoiTacID").kendoDropDownList({
            dataTextField: "TenDoiTac",
            dataValueField: "DoiTacID",
            filter: "contains",
            optionLabel: "Chọn đối tác",
            dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/LayDanhSachDoiTac", {}),
        });
        createpopupdoitac($(".filter-doitac"));
        $("#TrangThai").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            filter: "contains",
            index: 1,
            optionLabel: "Chọn trạng thái",
            dataSource: dataTrangThai,

        });
        $("#LoaiHinhThanhToan").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "value",
            filter: "contains",
            index: 1,
            optionLabel: "Chọn loại hình thanh toán",
            dataSource: loaihinhthanhtoan,

        });
        $("#SoTienDaThanhToan").autoNumeric({
            digitGroupSeparator: ',',
            decimalCharacter: '.',
            minimumValue: '0',
            maximumValue: '999999999999999',
        });
        $(".thoigiantao").removeClass("hidden");
        $("#ThoiGianTao").kendoDatePicker({
            format: "dd/MM/yyyy HH:mm",
            value: dataitem == null ? new Date() : dataitem.ThoiGianTao
        });
        $("#NgayKhoiTao").kendoDatePicker({
            format: "dd/MM/yyyy HH:mm",
            value: dataitem == null ? new Date() : dataitem.ThoiGian
        });
        $("#ThoiGianDatCoc").kendoDatePicker({
            format: "dd/MM/yyyy HH:mm",
            //value: dataitem == null ? new Date() : dataitem.ThoiGianDatCoc
            defaultOption: null
        });
        $("#ThoiGianThanhToan").kendoDatePicker({
            format: "dd/MM/yyyy HH:mm",
            //value: dataitem == null ? new Date() : dataitem.ThoiGianThanhToan,
            defaultOption: null
        });
        $("#ThoiGianDuKienSanXuat").kendoDatePicker({
            format: "dd/MM/yyyy HH:mm",
            //value: dataitem == null ? new Date() : dataitem.ThoiGianDuKienSanXuat
            defaultOption: null
        });
        $("#ThoiGianDuKienGiaoHang").kendoDatePicker({
            format: "dd/MM/yyyy HH:mm",
            //value: dataitem == null ? new Date() : dataitem.ThoiGianDuKienGiaoHang
            defaultOption: null
        });
        $("#upLoadFile").change(function () {
            loadImage_new2(this, 'tbody');
            for (var x = 0; x < this.files.length; x++) {
                dataimg.append("uploads", this.files[x]);
            }
            $("#upLoadFile").val('').clone(true);
            $("#lstImageDisplay").on("click", ".closeImgLoad", function () {
                var file = $(this).data("file");
                for (var i = 0; i < lstHinhAnh.length; i++) {
                    if (lstHinhAnh[i].img === file) {
                        lstHinhAnh.splice(i, 1);
                        break;
                    }
                }
                var list = dataimg.getAll("uploads");
                dataimg = new FormData();
                for (var i = 0; i < list.length; i++) {
                    if (list[i].name === file) {

                    } else {
                        dataimg.append("uploads", list[i]);
                    }
                }
                $(this).closest("tr").remove();
            });
            showslideimg5($("#lstImageDisplay"));
        });
        ThemYeuCauKhachHang(dataitem != null ? dataitem.DonHangID : null);
        $("#gridsanpham").kendoGrid({
            width: "100%",
            height: heightGrid * 0.9,
            dataSource: new kendo.data.DataSource(
                {
                    transport: {
                        read: {
                            url: crudServiceBaseUrl + "/LayDanhSachSanPham",
                            dataType: 'json',
                            type: 'GET',
                            contentType: "application/json; charset=utf-8",
                            data: { HoaDonID: dataitem != null ? dataitem.DonHangID : "" }
                        }
                    },
                    batch: true,
                    pageSize: 50,
                    aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }, { field: "SoLuongGiao", aggregate: "sum" }, { field: "SoLuongTon", aggregate: "sum" }, { field: "SoLuongConLai", aggregate: "sum" }],
                    sort: [{ field: "Size", dir: "asc" }],
                    schema: {
                        data: 'data',
                        total: 'total',
                        model: {
                            id: "SanPhamID",
                            fields: {
                                TenDonVi: { type: "string", editable: false },
                                TenSanPham: { type: "string", editable: false },
                                MaSanPham: { type: "string", editable: false },
                                AnhDaiDien: { type: "string", editable: false },
                                CongDoan: { type: "string", editable: false },
                                LoaiHinh: { type: "number", editable: false },
                                SoLuongConLai: { type: "number", editable: false },
                                SoLuongTon: { type: "number", editable: false },
                                SoLuong: { type: "number", editable: true },
                                SoLuongGiao: { type: "number", editable: true },
                                DonGia: { type: "number", editable: true },
                                ThanhTien: { type: "number", editable: false },
                                TrangThai: { type: "number", editable: false },
                                Size: { type: "string", editable: false },
                                SoLuongGiao: { type: "number", editable: false },
                                GhiChu: { type: "string", editable: true },
                                Mau: { type: "string", editable: false }
                            }
                        }
                    },
                }),
            filterable: {
                mode: "row"
            },
            messages: {
                commands: {
                    update: GetTextLanguage("capnhat"),
                    canceledit: GetTextLanguage("huy")
                }
            },
            editable: {
                confirmation: GetTextLanguage("xoabangi"),
                confirmDelete: "Yes",
            },
            sortable: true,
            dataBinding: function () {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            pageable:
            {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    width: 60,
                    attributes: alignCenter,
                    template: "#= ++record #",
                    footerTemplate: "<div style='color:red;text-align:center;'>Tổng</div>",
                },
                {
                    field: "SanPhamID",
                    width: 150, hidden: true,
                    filterable: FilterInColumn
                },
                {
                    field: "AnhDaiDien", title: "Ảnh",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    template: kendo.template($("#tplAnh").html()),
                    width: 70
                },
                {
                    field: "TenSanPham",
                    title: "Tên sản phẩm",
                    width: 150,
                    filterable: FilterInColumn
                },
                {
                    field: "MaSanPham",
                    title: "Mã sản phẩm",
                    width: 100,
                    filterable: FilterInTextColumn,
                    attributes: alignLeft,
                },
                {
                    field: "TrangThai", width: 150,
                    title: "Trạng thái",
                    attributes: { style: "text-align:center;" },
                    filterable: FilterInTextColumn,
                    template: kendo.template($("#tplTrangThai").html()),
                    hidden: dataitem == null ? true : false
                },
                {
                    field: "CongDoan", width: 100,
                    title: "Công Đoạn",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    hidden: dataitem == null ? true : false
                },
                //{
                //    field: "LoaiHinhSanXuat", width: 100,
                //    title: "Loại Hình",
                //    attributes: { style: "text-align:left;" },
                //    filterable: FilterInTextColumn,
                //    template: kendo.template($("#tplLoaiHinh").html()),
                //    hidden:true
                //},
                {
                    field: "Mau",
                    title: "Màu",
                    width: 100,
                    filterable: FilterInColumn
                },
                {
                    field: "Size",
                    title: "Size",
                    width: 100,
                    filterable: false
                },
                {
                    field: "SoLuong",
                    title: "Số lượng đặt",
                    width: 100,
                    filterable: FilterInColumn,
                    format: "{0:n0}",
                    attributes: alignRight,
                    editor: function (container, options) {
                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                        var input = $('<input type="text" data-v-min="0"  class="form-control" id="' + options.model.get("SanPhamID") + '" name="SoLuong" />');
                        input.appendTo(container);
                        input.autoNumeric(autoNumericOptionsSoLuong);
                    },
                    footerTemplate: "<div id='TongSoLuongHang' style='color:red;text-align:right;'>#=kendo.toString(sum, 'n0')#</div>",
                },
                {
                    field: "SoLuongTon",
                    title: "Số lượng đã sản xuất",
                    width: 100,
                    filterable: FilterInColumn,
                    format: "{0:n0}",
                    attributes: alignRight,
                    hidden: dataitem == null ? true : false,
                    footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n0')#</div>",
                },
                {
                    field: "SoLuongGiao",
                    title: "Số lượng đã giao",
                    width: 100,
                    filterable: FilterInColumn,
                    format: "{0:n0}",
                    attributes: alignRight,
                    hidden: dataitem == null ? true : false,
                    editor: function (container, options) {
                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                        var input = $('<input type="text" data-v-min="1"  class="form-control" id="' + options.model.get("SanPhamID") + '" name="SoLuongGiao" />');
                        input.appendTo(container);
                        input.autoNumeric(autoNumericOptionsSoLuong);
                    },
                    footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n0')#</div>",
                },
                {
                    field: "SoLuongConLai",
                    title: "Số lượng còn lại",
                    width: 100,
                    filterable: FilterInColumn,
                    format: "{0:n0}",
                    attributes: alignRight,
                    hidden: dataitem == null ? true : false,
                    footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n0')#</div>",
                },
                {
                    field: "DonGia",
                    title: "Đơn giá",
                    width: 100,
                    filterable: FilterInColumn,
                    format: "{0:n0}",
                    attributes: alignRight,
                    editor: function (container, options) {
                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                        var input = $('<input type="text" data-v-min="0"  class="form-control" id="' + options.model.get("SanPhamID") + '" name="DonGia" />');
                        input.appendTo(container);
                        input.autoNumeric(autoNumericOptionsSoLuong);
                    },
                },
                {
                    field: "ThanhTien",
                    title: "Thành tiền",
                    width: 100,
                    filterable: FilterInColumn,
                    format: "{0:n0}",
                    attributes: alignRight,
                    footerTemplate: "<div id='TongSoLuongTien' style='color:red;text-align:right;'>#=kendo.toString(sum, 'n0')#</div>",
                },
                {
                    field: "GhiChu",
                    title: "Ghi chú",
                    width: 100,
                    filterable: false
                },
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                    template: '<a class="k-button k-button-icontext k-grid-delete" href="javascript:void(0)"><span class="k-icon k-i-delete"></span>Xóa</a>',
                    width: 100
                },
            ],
            save: function (e) {
                var items = $("#gridsanpham").data("kendoGrid").dataSource.data();
                if (e.values.DonGia != null) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].SanPhamID == e.model.SanPhamID && items[i].LoaiHinh == e.model.LoaiHinh) {
                            items[i].ThanhTien = (e.values.DonGia == null ? 1 : e.values.DonGia) * (e.model.SoLuong == null ? 1 : e.model.SoLuong);
                            items[i].DonGia = e.values.DonGia;
                            break;
                        }
                    }
                } else if (e.values.SoLuong != null) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].SanPhamID == e.model.SanPhamID && items[i].LoaiHinh == e.model.LoaiHinh) {
                            items[i].ThanhTien = (e.model.DonGia == null ? 1 : e.model.DonGia) * (e.values.SoLuong == null ? 1 : e.values.SoLuong);
                            items[i].SoLuong = e.values.SoLuong;
                            break;
                        }
                    }
                }
                $("#gridsanpham").data("kendoGrid").dataSource.data(items);
            },
            dataBound: function (e) {
                showslideimg10();
            },
            // editable: editableInline
        });
        //
        $("#btnChonSanPham").click(function () {
            CreateModalWithSize("mdlDanhSachSanPham", "99%", "90%", "tplDanhSachSanPham", "Dannh sách sản phẩm");
            var objMau = "";
            var objSize = ""; var arrData;
            var arrMau = [];
            var arrSize = [];
            $("#inputChonThietKe").kendoDropDownList({
                dataTextField: "TenMauMay",
                dataValueField: "MauMayID",
                filter: "contains",
                optionLabel: "Chọn thiết kế để tính",
                dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/LayDanhSachThietKeCoSanPham", {}),
                change: function () {
                    ContentWatingOP("mdlDanhSachSanPham", 0.9);
                    $("#inputTongSoLuong").removeClass("hidden"); $("#inputTongSoLuong").val('');
                    arrSize = []; objSize = ""; arrMau = []; objMau = "";
                    $.ajax({
                        cache: false,
                        async: false,
                        type: "POST",
                        url: crudServiceBaseUrl + "/LayDanhSachSanPhamTheoThietKe",
                        data: { MauMayID: $("#inputChonThietKe").val() },
                        success: function (data) {
                            $("#mdlDanhSachSanPham").unblock();
                            var dt = data.data; arrData = data.data;
                            $("#inputTenSanPham").val(dt[0].TenSanPham);
                            for (var i = 0; i < dt.length; i++) {
                                var add = true;
                                if (arrMau.length == 0) {
                                    arrMau.push(dt[0].Mau.toLowerCase());
                                } else {
                                    for (var j = 0; j < arrMau.length; j++) {
                                        if (arrMau[j] == dt[i].Mau.toLowerCase()) {
                                            add = false;
                                        }
                                    }
                                    if (add) {
                                        arrMau.push(dt[i].Mau.toLowerCase());
                                    }
                                }
                            }
                            for (var i = 0; i < dt.length; i++) {
                                var add = true;
                                if (arrSize.length == 0) {
                                    arrSize.push(dt[0].Size.toLowerCase());
                                } else {
                                    for (var j = 0; j < arrSize.length; j++) {
                                        if (arrSize[j] == dt[i].Size.toLowerCase()) {
                                            add = false;
                                        }
                                    }
                                    if (add) {
                                        arrSize.push(dt[i].Size.toLowerCase());
                                    }
                                }
                            }
                            objMau += "<th style='text-align: center;'>Tên màu/Size</th>";
                            objMau += "<th style='text-align: center;'>Tỉ lệ size</th>";
                            objMau += "<th style='text-align: center;'>Tổng</th>";
                            for (var i = 0; i < arrMau.length; i++) {
                                objMau += "<th style='text-align: center;'>" + arrMau[i].toUpperCase() + "</th>";
                            }
                            for (var i = 0; i <= arrSize.length; i++) {
                                if (i < arrSize.length) {
                                    objSize += "<tr style='height: 40px;text-align: center;'>";
                                    objSize += "<td>" + arrSize[i].toUpperCase() + "</td>";
                                    objSize += "<td style='padding-right: 25px;'><input id='TiLeSize" + [i] + "' type='text' class='form-control inputSoLuong TiLeSize hidden' style='height:15px;width:95%;text-align: right;' onClick='this.select();'></td>"
                                    objSize += "<td style='padding-right: 25px;'><input id='TongSoLuongDong" + [i] + "' type='text' class='hidden form-control inputSoLuong TongSoLuongDong " + arrSize[i].replace(/\s/g, "") + "tong' style='height:15px;width:95%;text-align: right;' onClick='this.select();'></td>"
                                    for (var j = 0; j <= arrMau.length; j++) {
                                        var flag = false; var spid; var size; var color;
                                        if (j < arrMau.length) {
                                            for (var k = 0; k < dt.length; k++) {
                                                if (dt[k].Size.toLowerCase() == arrSize[i] && dt[k].Mau.toLowerCase() == arrMau[j]) {
                                                    flag = true; spid = dt[k].SanPhamID; size = dt[k].Size.toLowerCase().replace(/\s/g, ""); color = dt[k].Mau.toLowerCase().replace(/\s/g, "");
                                                }
                                            }
                                            if (flag) {
                                                objSize += "<td style='padding-right: 25px;'><input id='" + spid + "' type='text' class='hidden form-control inputSoLuong SoLuongSanPham " + size + " " + color + size + " " + color + "' style='height:15px;width:95%;text-align: right;' onClick='this.select();'></td>";
                                            } else {
                                                objSize += "<td style='padding-right: 25px;'></td>";
                                            }
                                        }
                                    }
                                    objSize += "</tr>";
                                }
                                else if (i == arrSize.length) {
                                    objSize += "<tr style='height: 40px;text-align: center;'>";
                                    objSize += "<td style='padding-right: 25px;'></td>";
                                    objSize += "<td style='padding-right: 25px;'></td>";
                                    objSize += "<td style='padding-right: 25px;'><input id='TongSoLuongSauKhiTinh' type='text' class='hidden form-control inputSoLuong' style='height:15px;width:95%;text-align: right;' disabled='true'></td>";
                                    for (var j = 0; j < arrMau.length; j++) {
                                        objSize += "<td style='padding-right: 25px;'><input id='TongSoLuongCot" + [j] + "' type='text' class='hidden form-control inputSoLuong' style='height:15px;width:95%;text-align: right;' disabled='true'></td>";
                                    }
                                    objSize += "</tr>";
                                }
                            }
                            $("#inputMau").html(objMau);
                            $("#inputSize").html(objSize);
                            $(".inputSoLuong").autoNumeric({
                                digitGroupSeparator: ',',
                                decimalCharacter: '.',
                                minimumValue: '0',
                                maximumValue: '999999999999999',
                                emptyInputBehavior: 'zero',
                            });
                            TaoThemSize(dt[0].SanPhamID);
                        },
                    });
                    $.ajax({
                        url: crudServiceBaseUrl + "/LayTongSoLuongHang",
                        method: "POST",
                        data: { MauMayID: $("#inputChonThietKe").val() },
                        success: function (data) {
                            $("#inputTongSoLuong").val(data.data.SoLuongDat);
                            $("#inputTongSoLuong").trigger("change");
                        },
                        error: function (xhr, status, error) { }
                    });
                }
            });
            $("#inputTongSoLuong").autoNumeric({
                digitGroupSeparator: ',',
                decimalCharacter: '.',
                minimumValue: '0',
                maximumValue: '999999999999999',
                //emptyInputBehavior: 'zero'
            });
            $("#inputTongSoLuong").change(function (e) {
                $(".inputSoLuong").removeClass("hidden");
                var value = $("#inputTongSoLuong").autoNumeric("get");
                var tsl = parseInt(value / arrSize.length);
                $(".TiLeSize").val(1);
                $(".TongSoLuongDong").val(tsl);
                for (var i = 0; i < arrSize.length; i++) {
                    var sz = arrSize[i].replace(/\s/g, "");
                    var numberOfClass = $("." + sz).length;
                    $("." + sz).val(parseInt(tsl / numberOfClass));
                }
                var numberOfClassTSLD = $(".TongSoLuongDong").length;
                var resultTSL = 0;
                for (var i = 0; i < numberOfClassTSLD; i++) {
                    var rs = parseInt($("#TongSoLuongDong" + [i]).val());
                    resultTSL += rs;
                }
                $("#TongSoLuongSauKhiTinh").val(resultTSL);
                for (var i = 0; i < arrMau.length; i++) {
                    var resultTSLColor = 0;
                    for (var j = 0; j < arrSize.length; j++) {
                        var color = arrMau[i].toLowerCase().replace(/\s/g, "");
                        var size = arrSize[j].toLowerCase().replace(/\s/g, "");
                        var numberOfClassColor = $("." + color + size).length;
                        if (numberOfClassColor > 0) {
                            var rs = parseInt($("." + color + size).val());
                            resultTSLColor += rs;
                        }
                    }
                    $("#TongSoLuongCot" + [i]).val(resultTSLColor);
                }
                $(".TiLeSize").change(function (e) {
                    var sumTLS = 0;
                    $("#tblSoLuongSanPham .TiLeSize").each(function () {
                        sumTLS += parseInt($(this).val());
                    });
                    var result = parseInt(value / sumTLS);
                    var count = $(".TiLeSize").length;
                    for (var i = 0; i < count; i++) {
                        var a = $("#TiLeSize" + [i] + "").val();
                        $("#TongSoLuongDong" + [i] + "").val(result * a);
                    }
                    for (var i = 0; i < arrSize.length; i++) {
                        var a = $("#TongSoLuongDong" + [i] + "").val();
                        var sz = arrSize[i].replace(/\s/g, "");
                        var numberOfClass = $("." + sz).length;
                        $("." + sz).val(parseInt(a / numberOfClass));
                    }
                    var numberOfClassTSLD = $(".TongSoLuongDong").length;
                    var resultTSL = 0;
                    for (var i = 0; i < numberOfClassTSLD; i++) {
                        var rs = parseInt($("#TongSoLuongDong" + [i]).val());
                        resultTSL += rs;
                    }
                    $("#TongSoLuongSauKhiTinh").val(resultTSL);
                    for (var i = 0; i < arrMau.length; i++) {
                        var resultTSLColor = 0;
                        for (var j = 0; j < arrSize.length; j++) {
                            var color = arrMau[i].toLowerCase().replace(/\s/g, "");
                            var size = arrSize[j].toLowerCase().replace(/\s/g, "");
                            var numberOfClassColor = $("." + color + size).length;
                            if (numberOfClassColor > 0) {
                                var rs = parseInt($("." + color + size).val());
                                resultTSLColor += rs;
                            }
                        }
                        $("#TongSoLuongCot" + [i]).val(resultTSLColor);
                    }
                });
                $(".TongSoLuongDong").change(function () {
                    for (var i = 0; i < arrSize.length; i++) {
                        var sz = arrSize[i].replace(/\s/g, "");
                        var numberOfClass = $("." + sz).length;
                        var sumTSLD = $("." + sz + "tong").val();
                        $("." + sz).val(parseInt(sumTSLD / numberOfClass));
                    }
                    var numberOfClassTSLD = $(".TongSoLuongDong").length;
                    var resultTSL = 0;
                    for (var i = 0; i < numberOfClassTSLD; i++) {
                        var rs = parseInt($("#TongSoLuongDong" + [i]).val());
                        resultTSL += rs;
                    }
                    $("#TongSoLuongSauKhiTinh").val(resultTSL);
                    for (var i = 0; i < arrMau.length; i++) {
                        var resultTSLColor = 0;
                        for (var j = 0; j < arrSize.length; j++) {
                            var color = arrMau[i].toLowerCase().replace(/\s/g, "");
                            var size = arrSize[j].toLowerCase().replace(/\s/g, "");
                            var numberOfClassColor = $("." + color + size).length;
                            if (numberOfClassColor > 0) {
                                var rs = parseInt($("." + color + size).val());
                                resultTSLColor += rs;
                            }
                        }
                        $("#TongSoLuongCot" + [i]).val(resultTSLColor);
                    }
                });
                $(".SoLuongSanPham").change(function () {
                    for (var i = 0; i < arrSize.length; i++) {
                        var sum = 0;
                        var sz = arrSize[i].replace(/\s/g, "");
                        var numberOfClass = $("." + sz).length;
                        $("#tblSoLuongSanPham ." + sz).each(function () {
                            sum += parseInt($(this).val());
                        });
                        $("." + sz + "tong").val(sum);
                    }
                    var numberOfClassTSLD = $(".TongSoLuongDong").length;
                    var resultTSL = 0;
                    for (var i = 0; i < numberOfClassTSLD; i++) {
                        var rs = parseInt($("#TongSoLuongDong" + [i]).val());
                        resultTSL += rs;
                    }
                    $("#TongSoLuongSauKhiTinh").val(resultTSL);
                    for (var i = 0; i < arrMau.length; i++) {
                        var resultTSLColor = 0;
                        for (var j = 0; j < arrSize.length; j++) {
                            var color = arrMau[i].toLowerCase().replace(/\s/g, "");
                            var size = arrSize[j].toLowerCase().replace(/\s/g, "");
                            var numberOfClassColor = $("." + color + size).length;
                            if (numberOfClassColor > 0) {
                                var rs = parseInt($("." + color + size).val());
                                resultTSLColor += rs;
                            }
                        }
                        $("#TongSoLuongCot" + [i]).val(resultTSLColor);
                    }
                });
            });
            //
            $("#btnDongY").click(function () {
                if ($("#inputChonThietKe").val() == null || $("#inputChonThietKe").val() == "") {
                    showToast("warning", "Chưa chọn thiết kế", "Bạn chưa chọn thiết kế của sản phẩm");
                }
                else {
                    if ($("#inputTongSoLuong").val() == null || $("#inputTongSoLuong").val() == "") {
                        showToast("warning", "Thiếu dữ liệu", "Bạn chưa điền đủ dữ liệu tổng số lượng hàng");
                    }
                    else {
                        for (var i = 0; i < arrData.length; i++) {
                            $("#tblSoLuongSanPham .SoLuongSanPham").each(function () {
                                if (arrData[i].SanPhamID == $(this)[0].id) {
                                    var Thanhtien = parseInt($(this)[0].value) * arrData[i].DonGia;
                                    arrData[i].SoLuong = parseInt($(this)[0].value);
                                    arrData[i].ThanhTien = Thanhtien;
                                }
                            });
                        }
                        $("#mdlDanhSachSanPham").data("kendoWindow").close();
                        $("#gridsanpham").data("kendoGrid").dataSource.data(arrData);
                    }
                }
            });
        });
        //
        $("#XoaDonHang").click(function () {
            var x = confirm("Bạn có chắc chắn muốn xóa đơn hàng này không?");
            if (x) {
                ContentWatingOP("mdlChiTietDonHang", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/XoaDonHang",
                    method: "POST",
                    data: { DonHangID: dataitem.DonHangID },
                    success: function (data) {
                        $("#mdlChiTietDonHang").unblock();
                        closeModel("mdlChiTietDonHang");
                        gridReload("grid", {});
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlChiTietDonHang").unblock();
                        gridReload("grid", {});
                    }
                });
            }
        })
        $("#LuuDonHang").click(function () {
            var validator = $("#frmEdit").kendoValidator().data("kendoValidator");
            if (validator.validate()) {
                $.ajax({
                    cache: false,
                    async: false,
                    type: "POST",
                    url: "/QuanLyDonHang/GetMaDonHang",
                    data: {},
                    success: function (data) {
                        $("#MaDonHang").val(data.data);
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), "Có lỗi lấy mã đơn hàng ");
                    }
                });
                var items = $("#gridsanpham").data("kendoGrid").dataSource.data();
                //if (items.length <= 0) {
                //    showToast("warning", "Cảnh báo", "Vui lòng chọn sản phẩm cho hóa đơn");
                //    return false;
                //}
                var chitiet = [];

                var objData = {};
                objData.DonHangID = dataitem != null ? dataitem.DonHangID : "";
                objData.ThoiGian = $("#NgayKhoiTao").val();
                objData.ThoiGianDatCoc = $("#ThoiGianDatCoc").val();
                objData.MaDonHang = $("#MaDonHang").val();
                objData.TenDonHang = $("#TenDonHang").val();
                objData.TrangThai = $("#TrangThai").val();
                objData.YeuCauKhachHangID = $("#YeuCauKhachHangID").val();
                objData.DiaDiemGiaoHang = $("#DiaDiemGiaoHang").val();
                objData.SoTienDaThanhToan = $("#SoTienDaThanhToan").autoNumeric("get");
                objData.ThoiGianThanhToan = $("#ThoiGianThanhToan").val();
                objData.ThoiGianDuKienSanXuat = $("#ThoiGianDuKienSanXuat").val();
                objData.ThoiGianDuKienGiaoHang = $("#ThoiGianDuKienGiaoHang").val();
                objData.LoaiHinhThanhToan = $("#LoaiHinhThanhToan").val();
                objData.YeuCauKhac = $("#YeuCauKhac").val();
                objData.DoiTacID = $("#DoiTacID").val();
                objData.EmailKeToan = $("#EmailKeToan").val();
                objData.GhiChu = $("#GhiChu").val();
                var files = dataimg.getAll("uploads"); var count = 0;
                if (files.length > 0) {
                    for (var i = 0; i < files.length; i++) {
                        $(".imgslider img").each(function (index, element) {
                            if (files[i].name == $(element).data("file")) {
                                var dataimg2 = new FormData();
                                dataimg2.append("uploads", files[i]);
                                $(".processbar", $(element).closest(".imgslider")).html('<div style="width: 0%;background: #c66161;height: 20px;"></div>');
                                $.ajax({
                                    url: '/Upload/UploadHopDong2',
                                    data: dataimg2,
                                    processData: false,
                                    contentType: false,
                                    sycn: false,
                                    type: 'POST',
                                    // this part is progress bar
                                    xhr: function () {
                                        var xhr = new window.XMLHttpRequest();
                                        xhr.upload.addEventListener("progress", function (evt) {
                                            if (evt.lengthComputable) {
                                                var percentComplete = evt.loaded / evt.total;
                                                percentComplete = parseInt(percentComplete * 100);
                                                $(".processbar div", $(element).closest(".imgslider")).attr("style", "width: " + percentComplete + "%;background: #40aa24;height: 20px;text-align: center;color: white;font-weight: bold;")
                                                $(".processbar div", $(element).closest(".imgslider")).text(percentComplete + "%");
                                            }
                                        }, false);
                                        return xhr;
                                    },
                                    success: function (data) {
                                        count++;
                                        lstHinhAnh.push({ img: data.img, name: data.name });

                                        if (count == files.length) {
                                            objData.ListHinhAnh = lstHinhAnh;
                                            ContentWatingOP("mdlChiTietDonHang", 0.9);
                                            setTimeout(function () {
                                                $.ajax({
                                                    cache: false,
                                                    async: false,
                                                    type: "POST",
                                                    url: "/QuanLyDonHang/AddOrUpdateDonHang",
                                                    data: { data: JSON.stringify(objData), sanphams: JSON.stringify(items), chitiet: JSON.stringify(chitiet) },
                                                    success: function (data) {
                                                        altReturn(data);
                                                        $("#mdlChiTietDonHang").unblock();
                                                        if (data.code == "success") {
                                                            closeModel("mdlChiTietDonHang");
                                                            gridReload("grid", {});
                                                            return false;
                                                        }

                                                    },
                                                    error: function (xhr, status, error) {
                                                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                                    }
                                                });
                                            }, 1000);
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
                else {
                    ContentWatingOP("mdlChiTietDonHang", 0.9);
                    objData.ListHinhAnh = lstHinhAnh;
                    setTimeout(function () {
                        $.ajax({
                            cache: false,
                            async: false,
                            type: "POST",
                            url: "/QuanLyDonHang/AddOrUpdateDonHang",
                            data: { data: JSON.stringify(objData), sanphams: JSON.stringify(items), chitiet: JSON.stringify(chitiet) },
                            success: function (data) {
                                altReturn(data);
                                $("#mdlChiTietDonHang").unblock();
                                if (data.code == "success") {
                                    closeModel("mdlChiTietDonHang");
                                    gridReload("grid", {});
                                    return false;
                                }
                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            }
                        });
                    }, 1000);
                }
            }
        });
        //
        $("#gridDanhSachNguyenLieu").kendoGrid({
            width: "100%",
            height: heightGrid * 0.9,
            dataSource: new kendo.data.DataSource(
                {
                    transport: {
                        read: {
                            url: crudServiceBaseUrl + "/LayDanhSachNguyenLieu",
                            dataType: 'json',
                            type: 'GET',
                            contentType: "application/json; charset=utf-8",
                            data: { HoaDonID: dataitem != null ? dataitem.DonHangID : "" }
                        }
                    },
                    batch: true,
                    pageSize: 50,
                    schema: {
                        data: 'data',
                        total: 'total',
                        model: {
                            id: "NguyenLieuID",
                            fields: {
                            }
                        }
                    },
                }),
            filterable: {
                mode: "row"
            },
            sortable: true,
            dataBinding: function () {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            pageable:
            {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    width: 60,
                    attributes: alignCenter,
                    template: "#= ++record #",
                    footerTemplate: "<div style='color:red;text-align:center;'>Tổng</div>",
                },
                {
                    field: "AnhDaiDien", title: "Ảnh",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    template: kendo.template($("#tplAnh").html()),
                    width: 70
                },
                //{
                //    field: "LoaiHinh",
                //    title: "Loại nguyên liệu",
                //    width: 100,
                //    filterable: FilterInColumn
                //},
                {
                    field: "MaNguyenLieu",
                    title: "Mã nguyên liệu",
                    width: 100,
                    filterable: FilterInTextColumn,
                    attributes: alignLeft,
                },
                {
                    field: "TenNguyenLieu",
                    title: "Tên nguyên liệu",
                    width: 100,
                    filterable: FilterInColumn
                },
                //{
                //    field: "SoLuongCan",
                //    title: "Số lượng cần",
                //    width: 100,
                //    filterable: false
                //},
                //{
                //    field: "SoLuongCo",
                //    title: "Số lượng hiện có",
                //    width: 100,
                //    filterable: false
                //},
                //{
                //    field: "LoaiVai",
                //    title: "Loại vải",
                //    width: 100,
                //    filterable: false
                //},
            ],
            dataBound: function (e) {
                showslideimg10();
            },
        });
    }
    function btnXuatExcelDonHang(donhangid, dtid) {
        //---------------------------------------------------------------------------------------------------------------
        $("#XuatExcel").click(function () {
            location.href = crudServiceBaseUrl + '/XuatExcelTungDonHang?DonHangID=' + donhangid + '&DoiTacID=' + dtid;
        });
    }
    function ThemYeuCauKhachHang(DonHangID) {
        if (DonHangID != null) {
            ContentWatingOP("yeucaukhachhang", 1);
            $.ajax({
                type: "GET",
                url: "/QuanLyDonHang/LayDanhSachYeuCauTheoDonHang",
                data: { DonHangID: DonHangID },
                success: function (data) {
                    setTimeout(function () {
                        if (data !== null && data.data != null) {
                            $(".NoiDungYeuCau").val(data.data.NoiDung);
                            $(".ThoiGianTaoYeuCau").val(kendo.toString(kendo.parseDate(data.data.ThoiGian, 'dd/MM/yyyy'), 'dd/MM/yyyy'));
                            $(".DoiTacYeuCau").val(data.data.TenDoiTac);
                            $("#YeuCauKhachHangID").val(data.data.YeuCauKhachHangID);
                            $(".SoLuongDat").val(kendo.toString(data.data.SoLuongDat, "n0"));
                            $(".GiaBan").val(kendo.toString(data.data.GiaBan, "n0"));
                            $(".ThoiGianGiaoHang").val(kendo.toString(kendo.parseDate(data.data.ThoiGianGiaoHang, 'dd/MM/yyyy'), 'dd/MM/yyyy'));
                            $.ajax({
                                url: "/YeuCauKhachHang" + "/TakeSize",
                                method: "POST",
                                data: { YeuCauKhachHangID: $("#YeuCauKhachHangID").val() },
                                success: function (data) {
                                    var dt = data.data;
                                    var str = "";
                                    for (var i = 0; i < dt.length; i++) {
                                        str += dt[i].Size + ", ";
                                    }
                                    $(".Size").val(str);
                                },
                                error: function (xhr, status, error) {
                                }
                            });
                            var html = "";
                            if (data.data.HinhAnh != null && data.data.HinhAnh != "") {
                                for (var i = 0; i < data.data.HinhAnh.split(',').length; i++) {
                                    html += "<div class='img-responsive media-preview imgslider' style='float: left; margin-right: 10px;'>";
                                    html += "<a rel='gallery' href='" + (localQLVanBan + data.data.HinhAnh.split(',')[i]) + "'>";
                                    html += '<img src="' + (localQLVanBan + data.data.HinhAnh.split(',')[i]) + '" class="img-responsive img-rounded media-preview" style="width: 70px;height: 70px;" /></a></div>';
                                }
                            }
                            $(".dshinhanh").html(html);
                            showslideimg();
                        }
                        $("#yeucaukhachhang").unblock();
                    }, 1000)
                },
                error: function (xhr, status, error) {
                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                }
            });

        }
        $("#btnThemYeuCau").click(function () {
            CreateModalWithSize("mdlchonyeucau", "100%", "95%", "tplchonyeucau", "Yêu cầu khách hàng");
            $("#gridchonyeucau").kendoGrid({
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: crudServiceBaseUrl + "/LayDanhSachYeuCau",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                        },
                        parameterMap: function (options, type) {
                            return JSON.stringify(options);
                        }
                    },
                    type: "json",
                    batch: true,
                    sort: [{ field: "ThoiGian", dir: "desc" }, { field: "TinhTrang", dir: "asc" }],
                    pageSize: 50,
                    schema: {
                        type: 'json',
                        data: 'data',
                        total: "total",
                        model: {
                            fields: {
                                ThoiGian: { type: "date" },
                            }
                        }
                    },
                }),
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                pageable:
                {
                    refresh: true,
                    buttonCount: 5,
                    pageSizes: true,
                    messages: messagegrid
                },
                filterable: {
                    mode: "row"
                },
                height: window.innerHeight * 0.8,
                columns: [
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                        width: 60,
                        attributes: alignCenter,
                        template: "#= ++record #",
                    },
                    {
                        field: "ThoiGian", width: 150,
                        title: "Thời Gian",
                        attributes: { style: "text-align:center;" },
                        filterable: FilterInTextColumn,
                        format: "{0:dd/MM/yyyy}"
                    },
                    {
                        field: "TrangThai",
                        title: "Trạng thái",
                        filterable: FilterInTextColumn,
                        filterable: {
                            cell: {
                                template: function (args) {
                                    args.element.kendoDropDownList({
                                        dataSource: dataTrangThai,
                                        dataTextField: "text",
                                        dataValueField: "value",
                                        valuePrimitive: true
                                    });
                                },
                                showOperators: false
                            }
                        },
                        width: 200,
                        template: kendo.template($("#tplTrangThai").html())
                    },
                    {
                        field: "TinhTrang",
                        title: "Loại yêu cầu",
                        filterable: FilterInTextColumn,
                        filterable: {
                            cell: {
                                template: function (args) {
                                    args.element.kendoDropDownList({
                                        dataSource: dataTinhTrang,
                                        dataTextField: "text",
                                        dataValueField: "value",
                                        valuePrimitive: true
                                    });
                                },
                                showOperators: false
                            }
                        },
                        width: 150,
                        template: kendo.template($("#tplTinhTrang").html())
                    },
                    {
                        field: "LoaiHinhSanXuat",
                        title: "Loại hình",
                        filterable: FilterInTextColumn,
                        filterable: {
                            cell: {
                                template: function (args) {
                                    args.element.kendoDropDownList({
                                        dataSource: dataLoaiHinh,
                                        dataTextField: "text",
                                        dataValueField: "value",
                                        valuePrimitive: true
                                    });
                                },
                                showOperators: false
                            }
                        },
                        width: 150,
                        template: kendo.template($("#tplLoaiHinh").html())
                    },
                    {
                        field: "MaDonHang",
                        title: "Mã Đơn Hàng",
                        width: 100,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                    },
                    {
                        field: "TenDoiTac",
                        title: "Tên Đối Tác",
                        width: 200,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "NoiDung",
                        title: "Nội Dung",
                        width: 400,
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn,
                    },
                ],
            });
            $("#gridchonyeucau").on("dblclick", "td", function () {
                var rows = $(this).closest("tr"),
                    grids = $("#gridchonyeucau").data("kendoGrid"),
                    dataItems = grids.dataItem(rows);
                if (!rows.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                    $(".ThoiGianTaoYeuCau").val(kendo.toString(dataItems.ThoiGian, 'dd/MM/yyyy'));
                    $(".DoiTacYeuCau").val(dataItems.TenDoiTac);
                    $(".SoLuongDat").val(kendo.toString(dataItems.SoLuongDat, "n0"));
                    $(".GiaBan").val(kendo.toString(dataItems.GiaBan, "n0"));
                    var a = kendo.toString(kendo.parseDate(dataItems.ThoiGianGiaoHang, 'dd/MM/yyyy'), 'dd/MM/yyyy');
                    $(".ThoiGianGiaoHang").val(a);
                    $("#YeuCauKhachHangID").val(dataItems.YeuCauKhachHangID);
                    $('#DoiTacID').data("kendoDropDownList").value(dataItems.DoiTacID);
                    $.ajax({
                        url: "/YeuCauKhachHang" + "/TakeSize",
                        method: "POST",
                        data: { YeuCauKhachHangID: $("#YeuCauKhachHangID").val() },
                        success: function (data) {
                            var dt = data.data;
                            var str = "";
                            for (var i = 0; i < dt.length; i++) {
                                str += dt[i].Size + ", ";
                            }
                            $(".Size").val(str);
                        },
                        error: function (xhr, status, error) {
                        }
                    });
                    //
                    $("#gridChiTietYeuCauKhachHang").kendoGrid({
                        dataSource: new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: "/YeuCauKhachHang" + "/GetAllDataChiTietYeuCauKhachHang",
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    data: { YeuCauKhachHangID: $("#YeuCauKhachHangID").val() }

                                },
                                parameterMap: function (options, type) {
                                    return JSON.stringify(options);
                                }
                            },
                            type: "json",
                            batch: true,
                            pageSize: 50,
                            sort: [{ field: "TenSanPham", dir: "asc" }, { field: "Mau", dir: "asc" }],
                            schema: {
                                type: 'json',
                                data: 'data',
                                total: "total",
                                model: {
                                    id: "ChiTietYeuCauID",
                                    fields: {
                                    }
                                }
                            },
                        }),
                        filterable: {
                            mode: "row"
                        },
                        sortable: true,
                        resizable: true,
                        pageable: {
                            refresh: true,
                            buttonCount: 5,
                            pageSizes: true,
                            messages: messagegrid
                        },
                        height: window.innerHeight * 0.5,
                        width: "100%",
                        columns: [
                            {
                                field: "TenSanPham",
                                title: "Tên sản phẩm",
                                width: 100,
                                attributes: alignCenter,
                                filterable: FilterInTextColumn,
                            },
                            {
                                field: "DacDiem",
                                title: "Đặc điểm",
                                width: 200,
                                attributes: alignCenter,
                                filterable: FilterInTextColumn,
                            },
                            {
                                field: "ChatLieu",
                                title: "Chất liệu",
                                width: 100,
                                attributes: alignCenter,
                                filterable: FilterInTextColumn,
                            },
                            {
                                field: "Mau",
                                title: "Màu chính",
                                width: 100,
                                attributes: alignCenter,
                                filterable: FilterInTextColumn,
                            },
                            {
                                field: "MauPhoi",
                                title: "Màu phối",
                                width: 100,
                                attributes: alignCenter,
                                filterable: FilterInTextColumn,
                            },
                            {
                                field: "GiaBan",
                                title: "Giá bán",
                                width: 100,
                                attributes: alignCenter,
                                filterable: FilterInColumn,
                                format: "{0:n0}",
                                hidden: $("#YeuCauKhachHangID").val() == null ? true : false
                            },
                            {
                                field: "YeuCauKhac",
                                title: "Ghi chú",
                                width: 150,
                                attributes: alignCenter,
                                filterable: FilterInTextColumn,
                            },
                        ],
                    });
                    //
                    var html = "";
                    if (dataItems.HinhAnh != null && dataItems.HinhAnh != "") {
                        for (var i = 0; i < dataItems.HinhAnh.split(',').length; i++) {
                            html += "<div class='img-responsive media-preview imgslider' style='float: left; margin-right: 10px;'>";
                            html += "<a rel='gallery' href='" + (localQLVanBan + dataItems.HinhAnh.split(',')[i]) + "'>";
                            html += '<img src="' + (localQLVanBan + dataItems.HinhAnh.split(',')[i]) + '" class="img-responsive img-rounded media-preview" style="width: 70px;height: 70px;" /></a></div>';
                        }
                    }
                    $(".dshinhanh").html(html);
                    showslideimg();
                    closeModel("mdlchonyeucau");
                }
            });
        });
    }
    function inputLoaiVai(ten, id) {
        $(ten).kendoDropDownList({
            dataTextField: "MaMau",
            dataValueField: "NguyenLieuID",
            filter: "contains",
            optionLabel: "Chọn thuộc tính",
            dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/LayDanhThuocTinhCuaNhomNguyenLieu", { NguyenLieuID: id }),
        });
        var ddl = $(ten).data("kendoDropDownList");
        ddl.value(id);
    }
    function TaoThemSize(SanPhamID) {
        $(".btn-taothemsize").click(function () {
            CreateModalWithSize("mdlChonSize", "99%", "90%", "tplChonSize", "Chọn size");
            $.ajax({
                url: crudServiceBaseUrl + "/LayDanhSachThuocTinhCuaSanPham",
                method: "POST",
                data: { SanPhamID: SanPhamID },
                success: function (data) {
                    var dt = data.data;
                    for (var i = 0; i < dt.length; i++) {
                        if (dt[i].LoaiVai == 1) {
                            var ten = "#inputVaiChinh";
                            $(".inputVaiChinh").removeClass("hidden");
                            inputLoaiVai(ten, dt[i].NguyenLieuID);
                        }
                        if (dt[i].LoaiVai == 2) {
                            var ten = "#inputVaiPhoiLot";
                            $(".inputVaiPhoiLot").removeClass("hidden");
                            inputLoaiVai(ten, dt[i].NguyenLieuID);
                        }
                        if (dt[i].LoaiVai == 3) {
                            var ten = "#inputVaiPhoiP1";
                            $(".inputVaiPhoiP1").removeClass("hidden");
                            inputLoaiVai(ten, dt[i].NguyenLieuID);
                        }
                        if (dt[i].LoaiVai == 4) {
                            var ten = "#inputVaiPhoiP2";
                            $(".inputVaiPhoiP2").removeClass("hidden");
                            inputLoaiVai(ten, dt[i].NguyenLieuID);
                        }
                        if (dt[i].LoaiVai == 5) {
                            var ten = "#inputChiMay";
                            $(".inputChiMay").removeClass("hidden");
                            inputLoaiVai(ten, dt[i].NguyenLieuID);
                        }
                        if (dt[i].LoaiVai == 6) {
                            var ten = "#inputMac";
                            $(".inputMac").removeClass("hidden");
                            inputLoaiVai(ten, dt[i].NguyenLieuID);
                        }
                        if (dt[i].LoaiVai == 7) {
                            var ten = "#inputCuc";
                            $(".inputCuc").removeClass("hidden");
                            inputLoaiVai(ten, dt[i].NguyenLieuID);
                        }
                        if (dt[i].LoaiVai == 8) {
                            var ten = "#inputPKKhac";
                            $(".inputPKKhac").removeClass("hidden");
                            inputLoaiVai(ten, dt[i].NguyenLieuID);
                        }
                    }
                    $("#btnTaoSize").click(function () {
                        var size = [];
                        var comau = false;
                        $("#mdlChonSize input").each(function (i, e) {
                            if ($(e).is(':checked')) {
                                var lstmau = []; var nlid;
                                for (var i = 0; i < dt.length; i++) {
                                    if (dt[i].LoaiVai == 1) { nlid = $("#inputVaiChinh").val() }
                                    if (dt[i].LoaiVai == 2) { nlid = $("#inputVaiPhoiLot").val() }
                                    if (dt[i].LoaiVai == 3) { nlid = $("#inputVaiPhoiP1").val() }
                                    if (dt[i].LoaiVai == 4) { nlid = $("#inputVaiPhoiP2").val() }
                                    if (dt[i].LoaiVai == 5) { nlid = $("#inputChiMay").val() }
                                    if (dt[i].LoaiVai == 6) { nlid = $("#inputMac").val() }
                                    if (dt[i].LoaiVai == 7) { nlid = $("#inputCuc").val() }
                                    if (dt[i].LoaiVai == 8) { nlid = $("#inputPKKhac").val() }
                                    lstmau.push({ LoaiVai: dt[i].LoaiVai, NguyenLieuID: nlid })
                                }
                                comau = true;
                                size.push({ Size: $(e).val(), Mau: lstmau })
                            }
                        });
                        if (comau) {
                            ////if ($("#inputMauSanPham").val() == null || $("#inputMauSanPham").val() == "") {
                            //    showToast("warning", "Chưa điền màu sản phẩm", "Bản phải điền màu cho sản phẩm");
                            //}
                            //else {
                            ContentWatingOP("mdlChonSize", 0.9);
                            $.ajax({
                                url: crudServiceBaseUrl + "/CopySanPham",
                                method: "post",
                                data: { SanPhamID: SanPhamID, size: JSON.stringify(size) },
                                success: function (data) {
                                    //showToast(data.code, data.code, data.message);
                                    if (data.code == "success") {
                                        //closeModel("mdlDanhSachSanPham");
                                        closeModel("mdlChonSize");
                                        $("#inputMau").html("");
                                        $("#inputSize").html("");
                                        $("#inputChonThietKe").data("kendoDropDownList").value(null);
                                        $("#inputTenSanPham").val(null);
                                        $(".btn-taothemsize").addClass("hidden");
                                        $("#inputTongSoLuong").addClass("hidden");
                                    } else {
                                        //closeModel("mdlDanhSachSanPham");
                                        $("#mdlChonSize").unblock();
                                    }
                                },
                                error: function (xhr, status, error) {
                                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                    $("#mdlChonSize").unblock();
                                    gridReload("grid", {});
                                }
                            });
                            //}
                        }
                        else {
                            showToast("warning", GetTextLanguage("thatbai"), "Bạn chưa chọn thêm size nào cả!");
                        }
                    });
                },
                error: function (xhr, status, error) {
                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                }
            });
        });
    }
})