﻿$(document).ready(function () {
    var CurrentUrl = "/QLHopDong";
    var date = new Date(), d = date.getDay(), m = date.getMonth(), y = date.getFullYear()
    var datafix_loaihopdong = [
        { text: GetTextLanguage("tatca"), val: 0 },
        { text: GetTextLanguage("hopdonglaodong"), val: 1 },
        { text: GetTextLanguage("hopdongmuaban"), val: 2 },
        { text: GetTextLanguage("hopdongsuachua"), val: 3 },
        { text: GetTextLanguage("hopdongkinhte"), val: 4 },
        { text: GetTextLanguage("hopdongthicong"), val: 5 },
        { text: GetTextLanguage("congvanden"), val: 6 },
        { text: GetTextLanguage("congvandi"), val: 7 },
        { text: GetTextLanguage("tailieu"), val: 8 },
        { text: GetTextLanguage("bieumau"), val: 9 }
    ]
    $("#LoaiHopDong").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "val",
        dataSource: datafix_loaihopdong,
    });
    $("#TuNgay").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date(y, 0, 1),
    });
    $("#DenNgay").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date(),
    });

});
