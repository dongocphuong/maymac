﻿$(document).ready(function () {
    var datasourceSX = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QuanLySanXuatNhom/LayDanhSach",
                dataType: "json",
                method: "GET",
                contentType: "application/json; charset=utf-8",
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        group: {
            field: "TenDonHang",
        },
        aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
        sort: [{ field: "ThoiGian", dir: "desc" }],
        schema: {
            model: {
                fields: {
                    DuAnID: { type: "string" },
                    ThoiGian: { type: "date" },
                    ThanhTien: { type: "number" }
                }
            },
            data: "data",
            total: "total",
        }
    });
    $("#grid").kendoGrid({
        width: "100%",
        dataSource: datasourceSX,
        height: window.innerHeight * 0.8,
        filterable: {
            mode: "row"
        },
        sortable: true,
        pageable:
        {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        dataBinding: function () {
            record = 0;
        },
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "ThoiGian",
                title: "Thời gian tạo",
                filterable: false,
                attributes: { style: "text-align:center;" },
                format: "{0:dd/MM/yyyy}",
                width: 100,
            },
            {
                field: "MaDonHang",
                title: "Mã đơn hàng",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:center;" },
                width:100,
            },
             {
                 field: "TenDonHang",
                 title: "Tên đơn hàng",
                 filterable: FilterInTextColumn,
                 attributes: { style: "text-align:center;" },
                 width: 150,
             },
              {
                  field: "LoaiHinh", width: 100,
                  title: "Loại Hình",
                  attributes: { style: "text-align:left;" },
                  filterable: FilterInTextColumn,
                  template: kendo.template($("#tplLoaiHinh").html()),
                  hidden:true,
              },
             {
                 field: "AnhDaiDien", title: "Ảnh",
                 attributes: { style: "text-align:left;" },
                 filterable: false,
                 template: kendo.template($("#tplAnh").html()),
                 width: 70
             },
            {
                field: "TenSanPham",
                title: "Tên sản phẩm",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:center;" },
                width: 150,
            },
            {
                field: "TenDoiTac",
                title: "Tên khách hàng",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:center;" },
                width: 150,
            },
            {
                field: "TinhTrang",
                title: "Tình trạng",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:center;" },
                width: 150,
                template: kendo.template($("#tplTinhTrang").html()),
            },
            {
                field: "ThoiGianConLai",
                title: "Thời gian còn lại",
                filterable: false,
                attributes: { style: "text-align:center;" },
                template: kendo.template($("#tplThoiGianConLai").html()),
                width: 100, hidden: true
            },
             {
                 field: "SoLuong",
                 title: "Số lượng đặt",
                 filterable: FilterInColumn,
                 attributes: { style: "text-align:right;" },
                 format: "{0:n2}",
                 width: 100,
             },
            {
                field: "ThanhTien",
                title: "Giá trị đơn hàng",
                filterable: FilterInColumn,
                attributes: { style: "text-align:right;" },
                format: "{0:n2}",
                width: 100,
                footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n2')#</div>",
            },
        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    //$("#grid").data("kendoGrid").dataSource.filter({
    //    "field": "TinhTrang",
    //    "operator": "contains",
    //    "value": "Chưa sản xuất"
    //});
    $("#tatca").click(function () {
        $("#grid").data("kendoGrid").dataSource.filter(null);

    });
    $("#chuasanxuat").click(function () {
        $("#grid").data("kendoGrid").dataSource.filter({
            "field": "TinhTrang",
            "operator": "contains",
            "value": "Chưa sản xuất"
        });
    });
    $("#grid .k-grid-content").on("dblclick", "td", function () {
        var rowct = $(this).closest("tr"),
               gridct = $("#grid").data("kendoGrid"),
               dataItem = gridct.dataItem(rowct);
        if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            CreateModalWithSize("mdlsanxuat", "100%", "95%", null, "Quản lý sản xuất");
            ContentWatingOP("mdlsanxuat", 0.9);
            if (dataItem.LoaiHinh == 1) {
                $("#mdlsanxuat").load("/QuanLySanXuatNhom/ViewSanXuatSanPham?MauMayID=" + dataItem.MauMayID + "&DonHangID=" + dataItem.DonHangID);
            } else {
                $("#mdlsanxuat").load("/QuanLySanXuatNhom/ViewSanXuatThanhPham?ThanhPhamID=" + dataItem.SanPhamID + "&DonHangID=" + dataItem.DonHangID);
            }
        }
    })


});