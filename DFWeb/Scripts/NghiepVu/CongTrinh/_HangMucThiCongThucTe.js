﻿$(document).ready(function () {
    //Danh sach hang muc thi coong
    var dataHangMucThiCongTheoCongTrinh = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QuanLyCongTrinh/LayDanhSachHangMuc",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {
                    CongTrinhID: $("#IDCongTrinh").val(), // lấy tất cả Dự Án
                }
            },
            create: {
                url: "/QuanLyCongTrinh/CreateOrUpdateHangMucThiCong",
                type: "post",
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid_DSHangMucThiCong", { CongTrinhID: $("#IDCongTrinh").val() });
                }
            },
            update: {
                url: "/QuanLyCongTrinh/CreateOrUpdateHangMucThiCong",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid_DSHangMucThiCong", { CongTrinhID: $("#IDCongTrinh").val() });
                }
            },
            destroy: {
                url: "/QuanLyCongTrinh/DestroyHangMucThiCongTheoCongTrinh",
                type: 'post',
                dataType: "json",
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid_DSHangMucThiCong", { CongTrinhID: $("#IDCongTrinh").val() });
                }
            },
            parameterMap: function (options, type) {
                if (type !== "read") {
                    if (type == "create") {
                        var model = {
                            DMHangMucThiCongID: $("#DMHangMucThiCongID").val(),
                            SoCoc: $("#SoCoc").autoNumeric('get'),
                            MetDat: $("#MetDat").autoNumeric('get'),
                            DonGiaThucTeMetDat: $("#DonGiaThucTeMetDat").autoNumeric('get'),
                            MetDa: $("#MetDa").autoNumeric('get'),
                            DonGiaThucTeMetDa: $("#DonGiaThucTeMetDa").autoNumeric('get'),
                            CongTrinhID: $("#IDCongTrinh").val(),
                            ThoiGianBatDau: $("#TuNgay").val(),
                            ThoiGianKetThuc: $("#DenNgay").val(),
                        }
                        return { data: kendo.stringify(model) }
                    }
                    else if (type == "update") {
                        var model = {
                            DMHangMucThiCongID: $("#DMHangMucThiCongID").val(),
                            SoCoc: $("#SoCoc").autoNumeric('get'),
                            MetDat: $("#MetDat").autoNumeric('get'),
                            DonGiaThucTeMetDat: $("#DonGiaThucTeMetDat").autoNumeric('get'),
                            MetDa: $("#MetDa").autoNumeric('get'),
                            DonGiaThucTeMetDa: $("#DonGiaThucTeMetDa").autoNumeric('get'),
                            CongTrinhID: $("#IDCongTrinh").val(),
                            ThoiGianBatDau: $("#TuNgay").val(),
                            ThoiGianKetThuc: $("#DenNgay").val(),
                            HangMucThiCongID: options.models[0].HangMucThiCongID,
                        }
                        return { data: kendo.stringify(model) }
                    } else {
                        return { HangMucThiCongID: options.models[0].HangMucThiCongID };
                    }
                } else {
                    return JSON.stringify(options);
                }
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        sort: { field: "TenHangMuc", dir: "asc" },
        schema: {
            data: "data",
            total: "total",
            model: {
                id: "HangMucThiCongID",
                fields: {
                    HangMucThiCongID: { type: "string" },
                    SoCoc: { type: "number", editable: false },
                    MetDat: { type: "number", editable: false },
                    DonGiaThucTeMetDat: { type: "number", editable: false },
                    MetDa: { type: "number", editable: false },
                    DonGiaThucTeMetDa: { type: "number", editable: false }
                }
            }
        },
        aggregate: [{ field: "SoCoc", aggregate: "sum" },
                    { field: "MetDat", aggregate: "sum" },
                    { field: "MetDa", aggregate: "sum" }]
    });

    var gridHangMucThiCong = $("#grid_DSHangMucThiCong").kendoGrid({
        dataSource: dataHangMucThiCongTheoCongTrinh,
        sortable: true,
        toolbar: [{ name: "create", text: GetTextLanguage("them") }, { template: kendo.template($("#titlegridhangmuc").html()) }],
        filterable: {
            extra: false,
        },
        columns: [
            {
                field: "HangMucThiCongID",
                hidden: true
            },
            {
                field: "TenHangMuc",
                title: GetTextLanguage("tenhangmuc"),
                //width: 200,
                filterable: false,
                attributes: { style: "text-align:left;" },
                footerTemplate: "<span style='font-weight: bold; text-align:left'> " + GetTextLanguage("tong") + " </span>",
            },
            {
                field: "ThoiGianBatDau",
                title: GetTextLanguage("tungay"),
                width: 150,
                filterable: false,
                attributes: { style: "text-align:center;" }
            },
            {
                field: "ThoiGianKetThuc",
                title: GetTextLanguage("denngay"),
                width: 150,
                filterable: false,
                attributes: { style: "text-align:center;" }
            },
            {
                field: "SoCoc",
                width: 80,
                title: GetTextLanguage("sococ"),
                filterable: false,
                format: "{0:n0}",
                footerTemplate: "<div style='text-align:right'>#=kendo.toString(sum, \"n0\")#</div>",
                template: "<div style='text-align:right'>#=kendo.toString(SoCoc, \"n0\")#</div>",
            },
            {
                field: "MetDat",
                width: 150,
                title: GetTextLanguage("sometdat"),
                filterable: false,
                footerTemplate: "<div style='text-align:right'>#=kendo.toString(sum, \"n2\")#</div>",
                template: "<span style='font-weight: bold;'> #: kendo.toString(MetDat,'n2') # </span>",
                format: "{0:n2}",
                attributes: { style: "text-align:right;" },
            },
            {
                field: "DonGiaThucTeMetDat",
                width: 150,
                title: GetTextLanguage("dongiametdat"),
                filterable: false,
                //footerTemplate: "<div style='text-align:right'>#=kendo.toString(sum, \"n3\")#</div>",
                template: "<span style='font-weight: bold; color:red;'> #: kendo.toString(DonGiaThucTeMetDat,'n2') # </span>",
                format: "{0:n2}",
                attributes: { style: "text-align:right;" },
            },
            {
                field: "MetDa",
                width: 150,
                title: GetTextLanguage("sometda"),
                filterable: false,
                footerTemplate: "<div style='text-align:right'>#=kendo.toString(sum, \"n2\")#</div>",
                template: "<span style='font-weight: bold;'> #: kendo.toString(MetDa,'n2') # </span>",
                format: "{0:n2}",
                attributes: { style: "text-align:right;" },
            },
            {
                field: "DonGiaThucTeMetDa",
                width: 150,
                title: GetTextLanguage("dongiametda"),
                filterable: false,
                //footerTemplate: "<div style='text-align:right'>#=kendo.toString(sum, \"n3\")#</div>",
                template: "<span style='font-weight: bold; color:red;'> #: kendo.toString(DonGiaThucTeMetDa,'n2') # </span>",
                format: "{0:n2}",
                attributes: { style: "text-align:right;" },
            },
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                width: 100,
                command: [
                    {
                        name: "destroy", text: GetTextLanguage("xoa")
                    }
                ],
            }
        ],
        edit: function (e) {
            if (e.model.get("HangMucThiCongID") == null || e.model.get("HangMucThiCongID") == "") {
                $("#SoCoc").val(0);
                $("#MetDat").val(0);
                $("#MetDa").val(0);
                $("#DonGiaThucTeMetDat").val(0);
                $("#DonGiaThucTeMetDa").val(0);
            }
            $("#LoaiHangMucThiCong").kendoDropDownList({
                dataTextField: 'TenLoaiHangMuc',
                dataValueField: 'LoaiHangMucThiCongID',
                filter: "contains",
                dataSource: getDataDroplit("/QuanLyCongTrinh", "/GetLoaiHangMucThiCong")
            });
            $("#LoaiHangMucThiCong").data("kendoDropDownList").value('ffffffff-ffff-ffff-ffff-ffffffffffff');//Fix
            $("#LoaiHangMucThiCong").data("kendoDropDownList").enable(false);

            $("#DMHangMucThiCongID").kendoDropDownList({
                dataTextField: 'TenHangMuc',
                dataValueField: 'DMHangMucThiCongID',
                filter: "contains",
                optionLabel: GetTextLanguage("chonhangmucthicong"),
                dataSource: getDataDroplitwithParam("/QuanLyCongTrinh/", "GetDMHangMucThiCongID", { LoaiHangMucThiCongID: $("#LoaiHangMucThiCong").val() })

            });
            $("#TuNgay").kendoDatePicker({
                format: "dd/MM/yyyy",
                value: new Date(),
                open: function (e) {
                    if ($("#DenNgay").data("kendoDatePicker").value() !== null) {
                        $("#TuNgay").data("kendoDatePicker").max($("#DenNgay").data("kendoDatePicker").value());
                    }
                }
            });
            $("#DenNgay").kendoDatePicker({
                format: "dd/MM/yyyy",
                value: new Date(),
                open: function (e) {
                    if ($("#TuNgay").data("kendoDatePicker").value() !== null) {
                        $("#DenNgay").data("kendoDatePicker").min($("#TuNgay").data("kendoDatePicker").value());
                    }
                }
            });
            $("#SoCoc").autoNumeric(autoNumericOptionsInt);
            $("#MetDat").autoNumeric(autoNumericOptionsKhoan);
            $("#MetDa").autoNumeric(autoNumericOptionsKhoan);
            $("#DonGiaThucTeMetDat").autoNumeric(autoNumericOptionsKhoan);
            $("#DonGiaThucTeMetDa").autoNumeric(autoNumericOptionsKhoan);
        },
        editable: {
            mode: "popup",
            window: {
                title: GetTextLanguage("thongtinhangmucthicong"),
                width: 600
            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes",
            template: kendo.template($("#popup_editor_hangmucthicong").html())
        },
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        save: function (e) {
            if (!e.model.isNew()) {
                e.model.dirty = true;
            }
        },
    }).data("kendoGrid");

    $("#grid_DSHangMucThiCong .k-grid-content").on("dblclick", "td", function () {
        var rowct = $(this).closest("tr");
        if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            $("#grid_DSHangMucThiCong").data("kendoGrid").editRow(rowct);
        }
    });

});