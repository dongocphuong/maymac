﻿$(document).ready(function () {
    $("#DangThiCong").on("click", function () {
        $("#grid_DSCongTrinh").data("kendoGrid").dataSource.read({ TrangThai: 1 }); 
    });
    $("#TamDung").on("click", function () {
        $("#grid_DSCongTrinh").data("kendoGrid").dataSource.read({ TrangThai: 2 }); 
    });
    $("#DaHoanThien").on("click", function () {
        $("#grid_DSCongTrinh").data("kendoGrid").dataSource.read({ TrangThai: 3 }); 
    });
    $("#DaQuyetToan").on("click", function () {
        $("#grid_DSCongTrinh").data("kendoGrid").dataSource.read({ TrangThai: 4 });
    });
    $("#TatCa").on("click", function () {
        $("#grid_DSCongTrinh").data("kendoGrid").dataSource.read({ TrangThai: 0 }); 
    });
    //Danh Sach cong trinh
    var datasourceCT = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QuanLyThiCong/DanhSachThiCong",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {
                    TrangThai: 1, // lấy tất cả Dự Án
                }
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        group: { field: "TenCongTrinh", dir: "asc" },
        sort: { field: "TenDoi", dir: "asc" },
        schema: {
            data: "data",
            total: "total",
            model: {
                fields: {
                    NgayTao: { type: "date", format: "{0:dd/MM/yyyy}" },
                }
            }
        }
    });
    $("#grid_DSCongTrinh").kendoGrid({
        width: "100%",
        height: heightGrid * 0.85,
        dataSource: datasourceCT,
        filterable: {
            mode: "row"
        },
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        sortable: true,
        pageable:
            {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
        columns: [
            {
                field: "STT",
                title: GetTextLanguage("stt"),
                hidden: true
            },
            {
                field: "TenCongTrinh",
                title: GetTextLanguage("tencongtrinh"),
                width: 200,
                attributes: alignLeft,
                filterable: FilterInTextColumn
            },
            {
                field: "TenDoi",
                title: GetTextLanguage("tendoi"),
                width: 200,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            {
                field: "strTinhTrang",
                title: GetTextLanguage("tinhtrang"),
                width: 150,
                filterable: FilterInColumn,
                attributes: alignCenter,
            },
            {
                field: "NgayBatDau",
                title: GetTextLanguage("ngaybatdau"),
                width: 100,
                filterable: FilterInColumn,
                attributes: alignCenter,
            },
            {
                field: "NgayKetThuc",
                title: GetTextLanguage("ngayketthuc"),
                width: 100,
                filterable: FilterInColumn,
                //template: formatToNumber("SoNgay"),
                attributes: alignCenter,
            },
        ],
        dataBound: function (e) {
            var grid = this;
            grid.tbody.find("tr").dblclick(function (e) {
                var dataItem = grid.dataItem(this);
                $(".dstc").hide();
                LoadingContent("ChiTietThiCong", "/QuanLyThiCong/ChiTietThiCong?TuNgay=&DenNgay=&CongTrinhDoiID=" + dataItem.get("CongTrinhDoiID") + "&CongTrinhID=" + dataItem.get("CongTrinhID") + "&DoiThiCongID=" + dataItem.get("DoiThiCongID"), dataItem.get("TenCongTrinh") + " - " + dataItem.get("TenDoi"));
            });
        },
    });
});