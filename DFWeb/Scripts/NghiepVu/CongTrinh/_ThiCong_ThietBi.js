﻿$(document).ready(function () {
    $(".tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        },
        select: function (e) {
            var tab = e.item.id;
            if (tab == "camay") dataCaMay();
            else if (tab == "suachua") dataSuaChua();
                //else if (tab == "congcudungcu") dataTabCCDC();
            else if (tab == "thanhly") dataThanhLy();
        }
    });

    dataCaMay();

    function dataCaMay() {
        $("#gridThietBi_CaMay").kendoGrid({
            dataSource: {
                batch: true,
                transport: {
                    read: {
                        url: "/QuanLyThiCong/ThiCong_ThietBi_CaMay",
                        type: "POST",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#CongTrinh_ThiCongID").val(),
                            DoiThiCongID: $("#Doi_ThiCongID").val()
                        }
                    },
                    parameterMap: function (data, type) {
                        return JSON.stringify(data);
                    }
                },
                pageSize: pageSize,
                aggregate: [{ field: "ChiPhi", aggregate: "sum" }],
                schema: {
                    type: 'json',
                    data: 'data',
                    total: "total",
                    model: {
                        fields: {
                            ThoiGianDi: { type: "date" },
                            ThoiGianDen: { type: "date" },
                            ChiPhi: { type: "number" }
                        }
                    }
                },
            },
            height: window.innerHeight * 0.75,
            sortable: true,
            pageable: pageableAll,
            filterable: {
                mode: "row"
            },
            columns: [
                {
                    field: "TenThietBi", title: GetTextLanguage("tenthietbi"), filterable: FilterInTextColumn, width: 200, attributes: alignLeft
                },
                {
                    field: "ViTriDi", title: GetTextLanguage("vitridi"), filterable: FilterInTextColumn, width: 200, attributes: alignLeft
                },
                {
                    field: "ViTriDen", title: GetTextLanguage("vitriden"), filterable: FilterInTextColumn, width: 200, attributes: alignLeft
                },
                
                {
                    field: "ThoiGianDen", title: GetTextLanguage("denngay"), filterable: FilterInColumn, width: 120, format: "{0:dd/MM/yyyy}"
                },
                {
                    field: "ThoiGianDi", title: GetTextLanguage("dingay"), filterable: FilterInColumn, width: 120, format: "{0:dd/MM/yyyy}"
                },
                {
                    field: "SoNgayOCongTrinh", title: GetTextLanguage("songayocongtrinh"), filterable: FilterInColumn, width: 150, attributes: alignRight
                },
                {
                    field: "SoNgayTinhChiPhi", title: GetTextLanguage("songaychiphi"), width: 150, filterable: FilterInColumn, attributes: alignRight
                },
                
                {
                    field: "TenNhanVien", title: GetTextLanguage("nhanvienbaocao"), filterable: FilterInTextColumn, width: 100
                },
                {
                    field: "ChiPhi", title: GetTextLanguage("chiphi"), format: "{0:n2}", width: 150, attributes: alignRight, filterable: FilterInColumn,
                    footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n2')#</div>",
                },
                //{ command: "destroy", title: "&nbsp;", width: 150 }
            ],
        });
    }

    function dataSuaChua() {
        $("#gridThietBi_SuaChua").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "/QuanLyThiCong/ThiCong_ThietBi_SuaChua",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#CongTrinh_ThiCongID").val(),
                            DoiThiCongID: $("#Doi_ThiCongID").val()
                        }
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                batch: true,
                pageSize: 50,
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
                schema: {
                    type: 'json',
                    data: 'data',
                    total: "total",
                    model: {
                        fields: {
                            ThoiGian: { type: "date" },
                            DonGia: { type: "number" },
                            SoLuong: { type: "number" },
                            ThanhTien: { type: "number" },
                        }
                    }
                },
            }),
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: window.innerHeight * 0.75,
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center",
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"), filterable: FilterInColumn, width: 200, attributes: alignCenter, format: "{0:dd/MM/yyyy}"
                },
                {
                    field: "TenThietBi", title: GetTextLanguage("tenthietbi"), filterable: FilterInTextColumn, width: 200, attributes: alignLeft
                },
                {
                    field: "HangMucSuaChua", title: GetTextLanguage("noidungsuachua"), filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("donvitinh"), width: 120, filterable: FilterInTextColumn, attributes: alignCenter
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), width: 100, format: "{0:n2}", filterable: FilterInColumn, attributes: alignRight
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), format: "{0:n2}",
                    width: 150,
                    filterable: FilterInColumn,
                    attributes: alignRight,
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("giathanh"), width: 150, format: "{0:n2}",
                    filterable:
                    FilterInColumn,
                    width: 200,
                    attributes: alignRight,
                    footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n2')#</div>",
                },
                
            ],
            dataBound: function () {
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            }
        });
    }

    function dataThanhLy() {
        $("#gridThietBi_ThanhLy").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: "/QuanLyThiCong/ThiCong_ThietBi_ThanhLy",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                batch: true,
                pageSize: 50,
                sort: [{ field: "ThoiGian", dir: "desc" }],
                //serverSorting: true,
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        id: "LichSuThietBiID",
                        fields: {
                            SoTien: { type: "number" },
                            ThoiGian: { type: "date" },
                        }
                    }
                }
            }),
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            height: window.innerHeight * 0.75,
            width: "100%",
            columns: [
                {
                    field: "MaThietBi", title: GetTextLanguage("mathietbi"), attributes: { style: "text-align:left;" }, width: 100, filterable: FilterInColumn
                },
                {
                    field: "TenThietBi", title: GetTextLanguage("tenthietbi"), attributes: { style: "text-align:left;" },  filterable: FilterInTextColumn
                },
                {
                    field: "ThoiGian",
                    title: GetTextLanguage("thoigian"),
                    width: 200, filterable: FilterInColumn,
                    format: "{0:dd/MM/yyyy}"
                },
                {
                    field: "SoTien", title: GetTextLanguage("giathanhly"),
                    attributes: alignRight,
                    width: 120,
                    format: "{0:n2}",
                    filterable: FilterInColumn,
                },
                

            ]

        });
    }
});