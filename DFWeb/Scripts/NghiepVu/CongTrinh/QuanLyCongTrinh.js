﻿$(document).ready(function () {

    var loaihopdong = [
        { text: "Đang thực hiện", value: 1 },
        { text: "Đã xong", value: 3 },
        { text: "Đã giao hàng", value: 4},
        { text: "Đã hủy", value: 5 },
    ];
    $("#TinhTrangCT").kendoDropDownList({
        dataSource: loaihopdong,
        dataTextField: "text",
        dataValueField: "value",
        autoBind: true,
        filter: "contains",
        change: function (e) {
            var value = this.value();
            if (value) {
                $("#grid_DSCongTrinh").data("kendoGrid").dataSource.read({  tinhtrang: value });
            }
        }
    });
    $("#TinhTrangCT").data("kendoDropDownList").value(1);
    //Danh Sach cong trinh
    var datasourceCT = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QuanLyCongTrinh/DanhSachCongTrinh",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {
                    tinhtrang: 1,
                }
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        sort: [{ field: "NgayTao", dir: "desc" }, { field: "TenCongTrinh", dir: "asc" }],
        schema: {
            data: "data",
            total: "total",
            model: {
                fields: {
                    NgayTao: { type: "date", format: "{0:dd/MM/yyyy}" },
                }
            }
        }
    });
    $("#grid_DSCongTrinh").kendoGrid({
        width: "100%",
        height: heightGrid * 0.85,
        dataSource: datasourceCT,
        filterable: {
            mode: "row"
        },
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        sortable: true,
        pageable:
            {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
        columns: [
            {
                field: "STT",
                title: GetTextLanguage("stt"),
                hidden: true
            },
            {
                field: "NgayTao",
                title: GetTextLanguage("ngaykhoitao"),
                width: 150,
                format:"{0:dd/MM/yyyy}",
                filterable: FilterInColumn
            },
            {
                field: "TenDuAn",
                title: "Tên hóa đơn",
                width: 150,
                filterable: FilterInColumn
            },
            {
                field: "MaDuAn",
                title: "Mã hóa đơn",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            {
                field: "TenDoiTac",
                title: "Đối tác",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            {
                field: "TinhTrang",
                title: GetTextLanguage("tinhtrang"),
                width: 150,
                filterable: FilterInColumn,
                attributes: alignCenter,
                template: kendo.template($("#tpltinhtrang").html())
            },
            {
                field: "GiaTriHopDong",
                title: "Thành tiền",
                width: 100,
                filterable: FilterInColumn,
                //template: formatToNumber("SoNgay"),
                attributes: alignCenter,
            },
        ],
        dataBound: function (e) {
            var grid = this;
            grid.tbody.find("tr").dblclick(function (e) {
                var dataItem = grid.dataItem(this);
                $(".dsct").hide();
                //LoadingContent("ChiTietCongTrinh", "/QuanLyCongTrinh/AddHopDongTau_Partial?CongTrinhID=" + dataItem.get("DuAnID"), dataItem.get("TenDuAn"));
            });
        },
    });

    $(".btnTemplate").on("click", function () {
        location.href = '/QuanLyCongTrinh/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();

    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "80%",
                    height: innerHeight * 0.7,
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                var datasourceImport = new kendo.data.DataSource({
                    data:data,
                    type: "json",
                    batch: true,
                    pageSize: 50,
                    sort: [{ field: "TenCongTrinh", dir: "asc" }],
                    schema: {
                        data: "data",
                        total: "total",
                        model: {
                            fields: {
                                NgayKhoiTao: { type: "date", format: "{0:dd/MM/yyyy}" },
                                NgayKhoiCongDuKien: { type: "date", format: "{0:dd/MM/yyyy}" },
                                NgayHoanThanhDuKien: { type: "date", format: "{0:dd/MM/yyyy}" },
                                TinhTrang: { type: "string", editable:false }
                            }
                        }
                    }
                });
                var grid = $("#gridthem").kendoGrid({
                    dataSource: datasourceImport,
                    editable: true,
                    height: innerHeight * 0.5,
                    width: "100%",
                    dataBound: function () {
                        var rows = this.items();
                        dem = 0;
                        $(rows).each(function () {
                            dem++;
                            var rowLabel = $(this).find(".stt");
                            $(rowLabel).html(dem);
                            if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                                dem = 0;
                            }
                        });
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 60,
                        },
                        {
                            field: "NgayKhoiTao",
                            title: GetTextLanguage("ngaykhoitao"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: 150,
                            format:"{0:dd/MM/yyyy}"
                            
                        },
                         {
                             field: "TenCongTrinh",
                             title: GetTextLanguage("tencongtrinh"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 200
                             
                         },
                         {
                             field: "ChuDauTu",
                             title: GetTextLanguage("kieuphongban"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 200
                             
                         },
                         {
                             field: "NgayKhoiCongDuKien",
                             title: GetTextLanguage("ngaykhoicongdukien"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 150,
                             format: "{0:dd/MM/yyyy}"
                             
                         },
                         {
                             field: "NgayHoanThanhDuKien",
                             title: GetTextLanguage("ngayhoanthanhdukien"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 150,
                             format: "{0:dd/MM/yyyy}"
                         },
                         {
                             field: "TinhTrang",
                             title: GetTextLanguage("tinhtrang"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 150,
                             
                         },
                         {
                             field: "DiaDiem",
                             title: GetTextLanguage("diadiem"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 200
                             
                         },
                         {
                             field: "GhiChu",
                             title: GetTextLanguage("ghichu"),
                             filterable: FilterInTextColumn,
                             attributes: alignLeft,
                             width: 200
                             
                         },
                        { command: "destroy", title: "&nbsp;", width: "150px" }
                    ],

                });
                $(".btn-import").click(function () {
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/QuanLyCongTrinh/ImportCongTrinh",
                        method: "post",
                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()) },
                        success: function (data2) {
                            showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                            setTimeout(function () {
                                $("#mdlThemFileExcel").unblock();
                                $("#grid_DSCongTrinh").data("kendoGrid").dataSource.read({ LoaiHinh: DaKy, TrangThai: $("#TinhTrangCT").val() });
                                $("#grid_DSCongTrinh").data("kendoGrid").refresh();
                                closeModel("mdlThemFileExcel");
                            }, 1000);
                        }
                    });
                });
                $("#huy").click(function () {
                    closeModel("mdlThemFileExcel");
                });
            }
        }
        objXhr3.open("POST", "/QuanLyCongTrinh/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
        //////////////////////////////////////////////////////////////////
    });
});