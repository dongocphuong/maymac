﻿﻿$(document).ready(function () {
    var dataSource = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: "/QuanLyCongTrinh/DMVung_DanhSach?CongTrinhID=" + $("#CongTrinhID").val(),
                    dataType: 'json',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                },
                update: {
                    url: "/QuanLyCongTrinh/SuaDMVung",
                    type: 'post',
                    dataType: 'json',
                    complete: function (e) {
                        altReturn(e.responseJSON);
                        $("#grid").data("kendoGrid").dataSource.read();
                    }
                },
                create: {
                    url: "/QuanLyCongTrinh/ThemDMVung",
                    type: 'post',
                    dataType: 'json',
                    complete: function (e) {
                        altReturn(e.responseJSON);
                        $("#grid").data("kendoGrid").dataSource.read();
                    }
                },
                destroy: {
                    url: "/QuanLyCongTrinh/XoaDMVung",
                    type: 'post',
                    dataType: 'json',
                    complete: function (e) {
                        altReturn(e.responseJSON);
                        $("#grid").data("kendoGrid").dataSource.read();
                    }
                },
                parameterMap: function (data, type) {
                    if (type !== "read") {
                        if (type == "create") {
                            var model = {
                                TenVung: data.models[0].TenVung,
                                CongTrinhID: $("#CongTrinhID").val(),
                            }
                            return { data: kendo.stringify(model) }
                        }
                        else if (type == "update") {
                            var model = {
                                VungID: data.models[0].VungID,
                                TenVung: data.models[0].TenVung,
                                CongTrinhID: $("#CongTrinhID").val(),
                            }
                            return { data: kendo.stringify(model) }
                        }
                        else {
                            return { data: kendo.stringify(data.models[0]) }
                        }
                    } else {
                        return JSON.stringify(data);
                    }
                }
            },
            batch: true,
            sort: [{ field: "TenVung", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "VungID",
                    fields: {
                        VungID: { type: "string" },
                        TenVung: {
                            type: "string",
                            validation: {
                                required: {
                                    message: GetTextLanguage("khongtrong")
                                }
                            }
                        },
                    },
                }
            },
            pageSize: pageSize,
        });

    var grid = $("#grid").kendoGrid({
        dataSource: dataSource,
        height: heightGrid - 90,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        toolbar: [{ name: "create", text: GetTextLanguage("them") }],
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='#' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "TenVung",
                title: GetTextLanguage("tenvung"),
                filterable: FilterInTextColumn,
                attributes: alignLeft
            },
                        {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                command: [{ name: "edit", text: GetTextLanguage("sua") },{ name: "destroy", text: GetTextLanguage("xoa") }],
                width: 200, attributes: alignCenter
            }
        ],
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        edit: function (e) {
            e.container.find(".k-edit-label:first").hide();
            e.container.find(".k-edit-field:first").hide();
            setTimeout(function () {
                $("input[name=TenVung]")[0].focus();
            }, 1000);
        },
        editable: {
            mode: "popup",
            window: {
                title: GetTextLanguage("thongtinchitiet"),
                width: 300
            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes",
        },
    });
});
