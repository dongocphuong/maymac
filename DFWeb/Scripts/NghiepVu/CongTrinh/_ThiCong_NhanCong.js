﻿$(document).ready(function () {
    //Danh sach hang muc thi coong
    var dataHangMucThiCongTheoCongTrinh = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QuanLyCongTrinh/DanhSachNhanVien",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {
                    CongTrinhID: $("#CongTrinh_ThiCongID").val(), // lấy tất cả Dự Án
                    DoiThiCongID: $("#Doi_ThiCongID").val()
                }
            },
            create: {
                url: "/QuanLyCongTrinh/CreateOrUpdateHangMucThiCong",
                type: "post",
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid_DSNhanCong", { CongTrinhID: $("#CongTrinh_ThiCongID").val(), DoiThiCongID: $("#Doi_ThiCongID").val() });
                }
            },
            update: {
                url: "/QuanLyCongTrinh/CreateOrUpdateHangMucThiCong",
                type: 'post',
                dataType: 'json',
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid_DSNhanCong", { CongTrinhID: $("#CongTrinh_ThiCongID").val(), DoiThiCongID: $("#Doi_ThiCongID").val() });
                }
            },
            destroy: {
                url: "/QuanLyCongTrinh/DestroyHangMucThiCongTheoCongTrinh",
                type: 'post',
                dataType: "json",
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid_DSNhanCong", { CongTrinhID: $("#CongTrinh_ThiCongID").val(), DoiThiCongID: $("#Doi_ThiCongID").val() });
                }
            },
            parameterMap: function (options, type) {
                if (type !== "read") {
                    if (type == "create") {
                        var model = {
                            DMHangMucThiCongID: $("#DMHangMucThiCongID").val(),
                            SoCoc: $("#SoCoc").autoNumeric('get'),
                            MetDat: $("#MetDat").autoNumeric('get'),
                            DonGiaThucTeMetDat: $("#DonGiaMetDat").autoNumeric('get'),
                            MetDa: $("#MetDa").autoNumeric('get'),
                            DonGiaThucTeMetDa: $("#DonGiaMetDa").autoNumeric('get'),
                            CongTrinhID: $("#IDCongTrinh").val()
                        }
                        return { data: kendo.stringify(model) }
                    }
                    else if (type == "update") {
                        var model = {
                            DMHangMucThiCongID: $("#DMHangMucThiCongID").val(),
                            SoCoc: $("#SoCoc").autoNumeric('get'),
                            MetDat: $("#MetDat").autoNumeric('get'),
                            DonGiaThucTeMetDat: $("#DonGiaMetDat").autoNumeric('get'),
                            MetDa: $("#MetDa").autoNumeric('get'),
                            DonGiaThucTeMetDa: $("#DonGiaMetDa").autoNumeric('get'),
                            CongTrinhID: $("#IDCongTrinh").val(),
                            HangMucThiCongID: options.models[0].HangMucThiCongID
                        }
                        return { data: kendo.stringify(model) }
                    } else {
                        return { HangMucThiCongID: options.models[0].HangMucThiCongID };
                    }
                } else {
                    return JSON.stringify(options);
                }
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        sort: { field: "TenNhanVien", dir: "desc" },
        schema: {
            data: "data",
            total: "total",
            model: {
                id: "HangMucThiCongID",
                fields: {
                    HangMucThiCongID: { type: "string" },
                }
            }
        },
        //aggregate: [{ field: "SoCoc", aggregate: "sum" },
        //    { field: "MetDat", aggregate: "sum" },
        //    { field: "MetDa", aggregate: "sum" }]
    });

    var gridDSNhanCong = $("#grid_DSNhanCong").kendoGrid({
        dataSource: dataHangMucThiCongTheoCongTrinh,
        sortable: true,
        height: innerHeight * 0.8,
        filterable: {
            mode: "row"
        },
        columns: [
            {
                field: "TenNhanVien",
                title: GetTextLanguage("tennhanvien"),
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
            },
            {
                field: "TenChucVu",
                title: GetTextLanguage("chucvu"),
                width: 200,
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
            },
            {
                field: "ThoiGianDC",
                title: GetTextLanguage("thoigiandichuyenden"),
                width: 200,
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
            },
            {
                field: "ThoiGianDi",
                title: GetTextLanguage("thoigiandieuchuyendi"),
                width: 200,
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
            }
        ],
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },

    }).data("kendoGrid");

    $("#grid_DSNhanCong .k-grid-content").on("dblclick", "td", function () {
        var rowct = $(this).closest("tr");
        if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            $("#grid_DSNhanCong").data("kendoGrid").editRow(rowct);
        }
    });

});