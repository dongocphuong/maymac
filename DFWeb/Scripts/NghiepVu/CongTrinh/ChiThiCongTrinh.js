﻿$(document).ready(function () {
    $("#drdCongTrinh").kendoDropDownList({
        dataTextField: 'TenCongTrinh',
        dataValueField: 'CongTrinhID',
        filter: "contains",
        optionLabel: GetTextLanguage("tatca"),
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/Home/DSCongTrinh",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                },
            },
            schema: {
                type: "json",
                data: "data"
            }
        }),
        change: function (e) {
            var value = this.value();
            if (value) {
                $("#drdDoiThiCong").data("kendoDropDownList").dataSource.read({ CongTrinhID: value });
            }
        },
    });
    $("#drdDoiThiCong").kendoDropDownList({
        dataTextField: 'TenDoi',
        dataValueField: 'DoiThiCongID',
        filter: "contains",
        index: 0,
        optionLabel: GetTextLanguage("tatca"),
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/Home/DSDoiThiCong",
                    dataType: "json",
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        CongTrinhID: $("#drdCongTrinh").data("kendoDropDownList").value()
                    },
                    complete: function (e) {
                        if (e.responseJSON.data.length > 0) {
                            $("#drdDoiThiCong").data("kendoDropDownList").select(0);
                        }
                    }
                },
            },
            schema: {
                type: "json",
                data: "data"
            }
        }),
    });


});