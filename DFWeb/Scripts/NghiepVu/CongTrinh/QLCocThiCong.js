﻿$(document).ready(function () {
    var dataSourceQuanLyCocThiCong = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: "/QuanLyCocThiCong/GetData?CongTrinhID=" + $("#CongTrinhID2").val() + "&DoiThiCongID=" + $("#DoiThiCongID2").val(),
                    dataType: 'json',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                },
                update: {
                    url: "/QuanLyCocThiCong/Update",
                    type: 'post',
                    dataType: 'json',
                    complete: function (e) {
                        altReturn(e.responseJSON);
                        $("#gridQuanLyCocThiCong").data("kendoGrid").dataSource.read();
                    }
                },
                create: {
                    url: "/QuanLyCocThiCong/Create",
                    type: 'post',
                    dataType: 'json',
                    complete: function (e) {
                        altReturn(e.responseJSON);
                        $("#gridQuanLyCocThiCong").data("kendoGrid").dataSource.read();
                    }
                },
                destroy: {
                    url: "/QuanLyCocThiCong/Delete",
                    type: 'post',
                    dataType: 'json',
                    complete: function (e) {
                        altReturn(e.responseJSON);
                        $("#gridQuanLyCocThiCong").data("kendoGrid").dataSource.read();
                    }
                },
                parameterMap: function (data, type) {
                    if (type !== "read") {
                        if (type == "create") {
                            var model = {
                                TenCoc: data.models[0].TenCoc,
                                HangMucID: $("#HangMucID").val(),
                                CongTrinhID: $("#CongTrinhID2").val(),
                                DoiThiCongID: $("#DoiThiCongID2").val(),
                                Vung: $(".k-edit-form-container #Vung").val(),
                                ToaDoX: $("#ToaDoX").val(),
                                ToaDoY: $("#ToaDoY").val(),
                            }
                            return { data: kendo.stringify(model) }
                        }
                        else if (type == "update") {
                            var model = {
                                CocThiCongID: data.models[0].CocThiCongID,
                                HangMucID: $("#HangMucID").val(),
                                TenCoc: data.models[0].TenCoc,
                                CongTrinhID: $("#CongTrinhID2").val(),
                                DoiThiCongID: $("#DoiThiCongID2").val(),
                                Vung: $(".k-edit-form-container #Vung").val(),
                                ToaDoX: $("#ToaDoX").val(),
                                ToaDoY: $("#ToaDoY").val(),
                             }
                    return { data: kendo.stringify(model) }
                        }
                        else {
                            return { data: kendo.stringify(data.models[0]) }
                        }
                    } else {
                        return JSON.stringify(data);
                    }
                }
            },
            batch: true,
            sort: [{ field: "TenCoc", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "CocThiCongID",
                    fields: {
                        CocThiCongID: { type: "string" },
                        TenCoc: {
                            type: "string",
                            validation: {
                                required: {
                                    message: GetTextLanguage("khongtrong")
                                }
                            }
                        },
                    },
                }
            },
            pageSize: pageSize,
        });

    var grid = $("#gridQuanLyCocThiCong").kendoGrid({
        dataSource: dataSourceQuanLyCocThiCong,
        height: heightGrid - 90,
        toolbar: [{ template: kendo.template($("#tlbDoiThiCong").html()) }],
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        columns: [
            {
                title: "<a href='#' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "TenCoc",
                title: GetTextLanguage("tencoc"),
                filterable: FilterInTextColumn,
                attributes: alignLeft
            },
            {
                field: "TenHangMuc",
                title: GetTextLanguage("hangmuc"),
                filterable: FilterInTextColumn,
                attributes: alignLeft
            },
              {
                  field: "ToaDoX",
                  title: "X",
                  filterable: FilterInTextColumn,
                  attributes: alignLeft
              },
                {
                    field: "ToaDoY",
                    title: "Y",
                    filterable: FilterInTextColumn,
                    attributes: alignLeft
                },
                  {
                      field: "TenVung",
                      title: GetTextLanguage("vung"),
                      filterable: FilterInTextColumn,
                      attributes: alignLeft
                  },
            {
                command: [{ name: "edit", text: GetTextLanguage("sua") }, { name: "destroy", text: GetTextLanguage("xoa") }],
                title: "<a href='#' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                width: 200,
                attributes: alignCenter
            }],
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        pageable: pageableAll,
        editable: {
            mode: "popup",
            window: {
                title: GetTextLanguage("thongtinchitiet"),
                width: 600
            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes",
            template: kendo.template($("#popup_editor").html())
        },
        edit: function (e) {
            $(".lst-modal").popuptablefilters();
            //$("#CongTrinhID").kendoDropDownList({
            //    dataTextField: "TenCongTrinh",
            //    dataValueField: "CongTrinhID",
            //    optionLabel: GetTextLanguage("tatcacongtrinh"),
            //    dataSource: getDataDroplit("/Home", "/DSCongTrinh"),
            //    change: function () {
            //        var value = this.value();
            //        if (value) {

            //            vCongTrinhID = value;
            //            vDoiThiCongID = "";
            //        } else {
            //            vCongTrinhID = "";// tất cả Dự Án
            //        }
            //        $("#DoiThiCongID").data("kendoDropDownList").dataSource.read({ CongTrinhId: value });
            //        $("#DoiThiCongID").data("kendoDropDownList").refresh();
        
            //    }
            //});
           //$("#DoiThiCongID").kendoDropDownList({
           //     dataTextField: "TenDoi",
           //     dataValueField: "DoiThiCongID",
           //     optionLabel: GetTextLanguage("tatcadoi"),
           //     dataSource: getDataDroplitwithParam("/Home", "/DSDoiThiCong", { CongTrinhId: $("#CongTrinhID").val() }),
           //     change: function () {
           //         $("#CongTrinhDoiHangMucID").data("kendoDropDownList").dataSource.read({ CongTrinhId: $("#CongTrinhID").val(), DoiThiCongID: $("#DoiThiCongID").val() });
           //         $("#CongTrinhDoiHangMucID").data("kendoDropDownList").refresh();
           //     }
            //});
            $("#ToaDoX").autoNumeric({
                digitGroupSeparator: '',
                decimalCharacter: '.',
                minimumValue: '0.000',
                maximumValue: '999999999999999.000',
            });
            $("#ToaDoY").autoNumeric({
                digitGroupSeparator: '',
                decimalCharacter: '.',
                minimumValue: '0.000',
                maximumValue: '999999999999999.000',
            });
            $(".k-edit-form-container #Vung").kendoDropDownList({
                dataTextField: "TenVung",
                dataValueField: "VungID",
                optionLabel: GetTextLanguage("chonvung"),
                dataSource: getDataDroplitwithParam("/QuanLyCocThiCong", "/LayDanhSachVung", { CongTrinhID: $("#CongTrinhID2").val() }),
            });
           $("#HangMucID").kendoDropDownList({
               dataTextField: "TenHangMuc",
               dataValueField: "HangMucID",
               optionLabel: GetTextLanguage("chonhangmuc"),
               dataSource: getDataDroplitwithParam("/QuanLyCocThiCong", "/LayDanhSachHangMucThiCong", { CongTrinhId: $("#CongTrinhID2").val(), DoiThiCongID: $("#DoiThiCongID2").val() }),
               change: function () {

               }
           });
        }

    });
   
    $(".lst-modal").popuptablefilters();


    $(".btnTemplate").on("click", function () {
        location.href = '/QuanLyCocThiCong/XuatTemplate';
    });
    $(".btnImport").on("click", function () {
        $("#inputImport").click();

    });
    $("#inputImport").change(function () {
        ContentWatingOP("content-loader", 0.9);
        var dataimg3 = new FormData();
        dataimg3.append("uploads", document.getElementById("inputImport").files[0]);
        var objXhr3 = new XMLHttpRequest();
        objXhr3.onreadystatechange = function () {
            if (objXhr3.readyState == 4) {
                $('#content-loader').unblock();
                var data = JSON.parse(objXhr3.responseText);
                var mdl = $('#mdlThemFileExcel').kendoWindow({
                    width: "50%",
                    height: innerHeight * 0.7,
                    title: "",
                    modal: true,
                    visible: false,
                    resizable: false,
                    actions: [
                        "Close"
                    ],
                    deactivate: function () {
                        $(this.element).empty();
                    }
                }).data("kendoWindow").center().open();
                mdl.content(kendo.template($('#tempImport').html()));
                $("#mdlThemFileExcel #Vung").kendoDropDownList({
                    dataTextField: "TenVung",
                    dataValueField: "VungID",
                    optionLabel: GetTextLanguage("chonvung"),
                    dataSource: getDataDroplitwithParam("/QuanLyCocThiCong", "/LayDanhSachVung", { CongTrinhID: $("#CongTrinhID2").val()}),
                });
                $(".btn-themvung").click(function () {
                    if ($("#txtThem").val() != "") {
                        $.ajax({
                            url: "/QuanLyCocThiCong/ThemDMVung",
                            method: "post",
                            data: { TenVung: $("#txtThem").val(), CongTrinhID: $("#CongTrinhID2").val() },
                            success: function (data2) {
                                showToast(data2.code, "", data2.message);
                                $("#txtThem").val("");
                                $("#mdlThemFileExcel #Vung").data("kendoDropDownList").dataSource.read({ CongTrinhID: $("#CongTrinhID2").val() });
                                $("#mdlThemFileExcel #Vung").data("kendoDropDownList").refresh();
                                $("input.vungthem").each(function (index, element) {
                                    $(element).data("kendoDropDownList").dataSource.read({ CongTrinhID: $("#CongTrinhID2").val() });
                                    $(element).data("kendoDropDownList").refresh();
                                });
                            }
                        });
                    }
                });
                var datasourceImport = new kendo.data.DataSource({
                    data: data,
                    type: "json",
                    batch: true,
                    schema: {
                        data: "data",
                        total: "total",
                        model: {
                            fields: {
                                ToaDoX: { type: "number", },
                                ToaDoY: { type: "number", },
                                MetDat: { type: "number", },
                                MetDa: { type: "number", },
                                Thep: { type: "number", },
                                BeTong: { type: "number", },
                            }
                        }
                    }
                });
                var grid = $("#gridthem").kendoGrid({
                    dataSource: datasourceImport,
                    editable: true,
                    height: innerHeight * 0.5,
                    width: "100%",
                    dataBound: function () {
                        var rows = this.items();
                        dem = 0;
                        $(rows).each(function () {
                            dem++;
                            var rowLabel = $(this).find(".stt");
                            $(rowLabel).html(dem);
                            if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                                dem = 0;
                            }
                        });
                    },
                    columns: [
                        {
                            title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                            template: "<span class='stt'></span>",
                            width: 60,
                        },
                        {
                            field: "TenCoc",
                            title: GetTextLanguage("tencoc"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                           
                        },
                        {
                            field: "HangMuc",
                            title: GetTextLanguage("hangmuc"),
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                        },
                        {
                            field: "ToaDoX",
                            title: "X",
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                        },
                        {
                            field: "ToaDoY",
                            title: "Y",
                            filterable: FilterInTextColumn,
                            attributes: alignLeft
                        },
                        {
                            field: "MetDat",
                            title: GetTextLanguage("metdat"),
                            filterable: FilterInColumn,
                            attributes: alignRight,
                            format: "{0:n2}"
                        },
                        {
                            field: "MetDa",
                            title: GetTextLanguage("metda"),
                            filterable: FilterInColumn,
                            attributes: alignRight,
                            format: "{0:n2}"
                        },
                         {
                             field: "Thep",
                             title: GetTextLanguage("thep"),
                             filterable: FilterInColumn,
                             attributes: alignRight,
                             format: "{0:n2}"
                         },
                         {
                              field: "BeTong",
                              title: GetTextLanguage("betong"),
                              filterable: FilterInColumn,
                              attributes: alignRight,
                              format: "{0:n2}"
                         },
                        { command: "destroy", title: "&nbsp;", width: "150px" }
                    ],

                });
                $(".btn-import").click(function () {
                    if ($("#mdlThemFileExcel #Vung").val() == "") {
                        showToast("warning", GetTextLanguage("thatbai"), GetTextLanguage("chonvung"));
                        return false;
                    }
                    ContentWatingOP("mdlThemFileExcel", 0.9);
                    $.ajax({
                        url: "/QuanLyCocThiCong/CheckVung",
                        method: "post",
                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()), vung: $("#mdlThemFileExcel #Vung").val(), CongTrinhID: $("#CongTrinhID2").val(), DoiThiCongID: $("#DoiThiCongID2").val() },
                        success: function (data2) {
                            if (data2.code == 'success') {
                                var list = data2.data;
                                console.log(list);
                                if (list.length > 1) {
                                    var mdl = $('#mdlCheckVung').kendoWindow({
                                        width: "300px",
                                        height: "100px",
                                        title: "",
                                        modal: true,
                                        visible: false,
                                        resizable: false,
                                        actions: [
                                            "Close"
                                        ],
                                        deactivate: function () {
                                            $(this.element).empty();
                                        }
                                    }).data("kendoWindow").center().open();
                                    mdl.content(kendo.template($('#tempVung').html()));
                                    $(".ChonVung").click(function () {
                                        closeModel("mdlCheckVung");
                                        for (var i = 1; i < list.length; i++) {
                                            var str = '';
                                            str += '<div class="col-sm-6">';
                                            str += '<div class="form-group">';
                                            str += '<label>' + GetTextLanguage("vung") + ' ' + (i + 1) + '</label>';
                                            str += '<div class="input-group" style="width:100%">';
                                            str += '<input type="text" class="form-control vungthem" name="Vung" id="Vung'+i+'" style="box-sizing: border-box; height: 36px !important;width:100%">';
                                            str += '</div>';
                                            str += '</div>';
                                            str += '</div>';
                                            $(".box-vung").append(str);
                                            $("#Vung"+i).kendoDropDownList({
                                                dataTextField: "TenVung",
                                                dataValueField: "VungID",
                                                optionLabel: GetTextLanguage("chonvung"),
                                                dataSource: getDataDroplitwithParam("/QuanLyCocThiCong", "/LayDanhSachVung", { CongTrinhID: $("#CongTrinhID2").val() }),
                                            });
                                        }
                                        $("#mdlThemFileExcel").unblock();
                                        $(".btn-import").addClass("hidden");
                                        $("#btn-importafterchosevung").removeClass("hidden");
                                        $("#btn-importafterchosevung").click(function () {
                            
                                            var listvung = [];
                                            listvung.push({ VungID: $("#mdlThemFileExcel #Vung").val() })
                                            $("input.vungthem").each(function (index, element) {
                                                listvung.push({ VungID: $(element).val() })
                                                if ($(element).val() == "") {
                                                    showToast("warning", GetTextLanguage("thatbai"), GetTextLanguage("chonvung"));
                                                    return false;
                                                }
                                            });
                                            ContentWatingOP("mdlThemFileExcel", 0.9);
                                            $.ajax({
                                                url: "/QuanLyCocThiCong/Import2",
                                                method: "post",
                                                data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()), vung: JSON.stringify(listvung), CongTrinhID: $("#CongTrinhID2").val(), DoiThiCongID: $("#DoiThiCongID2").val(), LoaiThem: 0 },
                                                success: function (data2) {
                                                    if (data2.code == 'success') {
                                                        showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                                                        setTimeout(function () {
                                                            $("#mdlThemFileExcel").unblock();
                                                            $("#gridQuanLyCocThiCong").data("kendoGrid").dataSource.read();
                                                            $("#gridQuanLyCocThiCong").data("kendoGrid").refresh();
                                                            closeModel("mdlThemFileExcel");
                                                        }, 1000);
                                                    } else {
                                                        showToast("error", GetTextLanguage("thatbai"), data2.message);
                                                        setTimeout(function () {
                                                            $("#mdlThemFileExcel").unblock();
                                                        }, 1000);
                                                    }
                                                }
                                            });
                                        });
                                    });
                                    $(".ThemVungMoi").click(function () {
                                        $.ajax({
                                            url: "/QuanLyCocThiCong/Import",
                                            method: "post",
                                            data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()), vung: $("#mdlThemFileExcel #Vung").val(), CongTrinhID: $("#CongTrinhID2").val(), DoiThiCongID: $("#DoiThiCongID2").val(),LoaiThem:1 },
                                            success: function (data2) {
                                                if (data2.code == 'success') {
                                                    showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                                                    setTimeout(function () {
                                                        $("#mdlThemFileExcel").unblock();
                                                        $("#gridQuanLyCocThiCong").data("kendoGrid").dataSource.read();
                                                        $("#gridQuanLyCocThiCong").data("kendoGrid").refresh();
                                                        closeModel("mdlThemFileExcel");
                                                    }, 1000);
                                                } else {
                                                    showToast("error", GetTextLanguage("thatbai"), data2.message);
                                                    setTimeout(function () {
                                                        $("#mdlThemFileExcel").unblock();
                                                    }, 1000);
                                                }


                                            }
                                        });
                                    });
                                } else {
                                    $.ajax({
                                        url: "/QuanLyCocThiCong/Import",
                                        method: "post",
                                        data: { data: JSON.stringify($("#gridthem").data("kendoGrid").dataSource.data()), vung: $("#mdlThemFileExcel #Vung").val(), CongTrinhID: $("#CongTrinhID2").val(), DoiThiCongID: $("#DoiThiCongID2").val() },
                                        success: function (data2) {
                                            if (data2.code == 'success') {
                                                showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                                                setTimeout(function () {
                                                    $("#mdlThemFileExcel").unblock();
                                                    $("#gridQuanLyCocThiCong").data("kendoGrid").dataSource.read();
                                                    $("#gridQuanLyCocThiCong").data("kendoGrid").refresh();
                                                    closeModel("mdlThemFileExcel");
                                                }, 1000);
                                            } else {
                                                showToast("error", GetTextLanguage("thatbai"), data2.message);
                                                setTimeout(function () {
                                                    $("#mdlThemFileExcel").unblock();
                                                }, 1000);
                                            }


                                        }
                                    });
                                }
                              
                            } else {
                                showToast("error", GetTextLanguage("thatbai"), data2.message);
                                setTimeout(function () {
                                    $("#mdlThemFileExcel").unblock();
                                }, 1000);
                            }
                           
                            
                        }
                    });
                });
                $("#huy").click(function () {
                    closeModel("mdlThemFileExcel");
                });
            }
        }
        objXhr3.open("POST", "/QuanLyCocThiCong/CheckKetQua", true);
        objXhr3.send(dataimg3);
        $("#inputImport").val('').clone(true);
        //////////////////////////////////////////////////////////////////
    });

});
