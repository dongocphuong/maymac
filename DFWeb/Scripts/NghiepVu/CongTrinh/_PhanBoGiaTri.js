﻿$(document).ready(function () {
    $("#addDoiThiCong").on("click", function () {
        var mdl = $('#mdl_DSDoiThiCong').kendoWindow({
            width: "50%",
            height: "60%",
            title: GetTextLanguage("danhsachdoithicong"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#temp_DSDoiThiCong').html()));
        var dataXuatKho = new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/QuanLyCongTrinh/DSDoiThiCongChuaCoTrongCongTrinh",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: { CongTrinhId: $("#IDCongTrinh").val() }

                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 50,
            schema: {
                data: "data",
                total: "total",
            },
        });
        var gridDSDoiThiCongh = $("#grid_DSDoiThiCong").kendoGrid({
            dataSource: dataXuatKho,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                messages: messagegridNonePaging,
                pageSizes: false,
                numeric: false,
                previousNext: false
            },
            //toolbar: kendo.template($("#toolbar_DSCongTrinh").html()),
            width: "100%",
            height: 500,
            dataBinding: function () {
                record = 0;
            },
            dataBound: function () {
                $("table.k-focusable tbody tr").hover(
                  function () {
                      $(this).css("cursor", "pointer");
                  });
            },
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "#= ++record #",
                    width: 60,
                    attributes: alignCenter
                },
                {
                    field: "TenDoi",
                    title: GetTextLanguage("tendoi"),
                    filterable: FilterInTextColumn,
                    attributes: {
                        style: "text-align:left"
                    }
                },
                {
                    field: "ChuDauTu", title: GetTextLanguage("chudautu"), filterable: FilterInTextColumn,
                    attributes: {
                        style: "text-align:left"
                    }
                },
                {
                    field: "DiaDiem", title: GetTextLanguage("diadiem"),
                    filterable: FilterInTextColumn,
                    attributes: {
                        style: "text-align:left"
                    }
                }
            ],
        }).data("kendoGrid");

        //resizeWindow3("grid_DSDoiThiCong", "mdl_DSDoiThiCong");

        $("#grid_DSDoiThiCong .k-grid-content").on("dblclick", "td", function () {
            if (confirm(GetTextLanguage("haychacchanbandaluu"))) {
                var rowct = $(this).closest("tr"),
                       gridct = $("#grid_DSDoiThiCong").data("kendoGrid"),
                       dataItemct = gridct.dataItem(rowct);
                destroyModel("mdl_DSDoiThiCong");

                if (dataItemct) {
                    $.ajax({
                        cache: false,
                        async: true,
                        type: "POST",
                        url: "/QuanLyCongTrinh/DieuChuyenDoiVeCongTrinh",
                        data: { CongTrinhID: $("#IDCongTrinh").val(), DoiThiCongID: dataItemct.get("DoiThiCongID") },
                        success: function (data) {
                            altReturn(data);
                            if (data.code == "success") {
                                
                                ContentLoading("contentCongTrinh", "/QuanLyCongTrinh/PhanBoGiaTri_ParitialNew?CongTrinhID=" + $("#IDCongTrinh").val());
                            };
                            $('#grid_DSDoiThiCong').data('kendoGrid').refresh();

                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("coloi"));
                        }
                    });
                }
            } else {
                return false;
            }
        });
    });

});