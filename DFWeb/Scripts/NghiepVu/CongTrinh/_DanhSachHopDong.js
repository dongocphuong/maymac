﻿$(document).ready(function () {
    //Danh sach hang muc thi coong
    var datafix_loaihopdong = [
        { text: GetTextLanguage("hopdongthicong"), val: 5 },
        { text: GetTextLanguage("congvanden"), val: 6 },
        { text: GetTextLanguage("congvandi"), val: 7 }
    ];

    var datafix_loaihopdongfilter = [
        { text: GetTextLanguage("tatca"), val: 0 },
        { text: GetTextLanguage("hopdongthicong"), val: 5 },
        { text: GetTextLanguage("congvanden"), val: 6 },
        { text: GetTextLanguage("congvandi"), val: 7 }
    ];
    lstHinhAnh = [];
    var LoaiHinhHienTai = 0;

    $("#LoaiHopDongIndex").kendoDropDownList({
        dataSource: datafix_loaihopdongfilter,
        dataTextField: "text",
        dataValueField: "val",
        autoBind: true,
        filter: "contains",
        change: function (e) {
            var value = this.value();
            if (value) {
                $("#grid_DSHopDong").data("kendoGrid").dataSource.read({ CongTrinhID: $("#IDCongTrinh").val(), LoaiHinh: value });
            }
        }
    });

    var dataHopDong = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QuanLyCongTrinh/LayDanhSachHopDong",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {
                    CongTrinhID: $("#IDCongTrinh").val(),
                    LoaiHinh: 0// lấy tất cả Dự Án
                }
            },
            create: {
                url: "/QuanLyCongTrinh/CreateOrUpdateHopDong",
                type: "post",
                dataType: 'json',
                async: true,
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid_DSHopDong", { CongTrinhID: $("#IDCongTrinh").val(), LoaiHinh: LoaiHinhHienTai });
                    $("#content-p").unblock();
                },
                error: function () {
                    $("#content-p").unblock();
                }
            },
            update: {
                url: "/QuanLyCongTrinh/CreateOrUpdateHopDong",
                type: 'post',
                dataType: 'json',
                async: true,
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid_DSHopDong", { CongTrinhID: $("#IDCongTrinh").val(), LoaiHinh: LoaiHinhHienTai });
                    $("#content-p").unblock();
                },
                error: function () {
                    $("#content-p").unblock();
                }
            },
            destroy: {
                url: "/QuanLyCongTrinh/DestroyHopDong",
                type: 'post',
                dataType: "json",
                complete: function (e) {
                    altReturn(e.responseJSON);
                    gridReload("grid_DSHopDong", { CongTrinhID: $("#IDCongTrinh").val(), LoaiHinh: LoaiHinhHienTai });
                    $("#content-p").unblock();
                },
                error: function () {
                    $("#content-p").unblock();
                }
            },
            parameterMap: function (options, type) {
                if (type !== "read") {
                    ContentWatingOP("content-p", 0.9);
                    if (type == "create") {
                        var ins = document.getElementById('upLoadFile').files.length;
                        if (ins > 0) {

                            var strUrl = uploadMultipleFileParagam("/Upload/UploadHopDong", "upLoadFile", $('#lstImageDisplay')).split(',');
                            if (strUrl) {
                                for (var k = 0; k < strUrl.length; k++) {
                                    lstHinhAnh.push(strUrl[k]);
                                }
                            }
                        }

                        var model = {
                            TenHopDong: $("#TenHopDong").val(),
                            LoaiHopDong: $("#LoaiHopDong").val(),
                            ThoiGianTao: $("#ThoiGianTao").val(),
                            DoiTacID: $("#DoiTacId").val(),
                            DSHinhAnh: lstHinhAnh,
                            CongTrinhID: $("#IDCongTrinh").val(),
                            CapDoBaoMat: $("#CapDoID").val()
                        }
                        lstHinhAnh = [];
                        return { data: kendo.stringify(model) }
                    }
                    else if (type == "update") {
                        var ins = document.getElementById('upLoadFile').files.length;
                        if (ins > 0) {
                            ContentWatingOP("popup_editor", 0.9);
                            var strUrl = uploadMultipleFileParagam("/Upload/UploadHopDong", "upLoadFile", $('#lstImageDisplay')).split(',');
                            for (var k = 0; k < strUrl.length; k++) {
                                lstHinhAnh.push(strUrl[k]);
                            }
                        }
                        var model = {
                            HopDongID: options.models[0].HopDongID,
                            TenHopDong: $("#TenHopDong").val(),
                            LoaiHopDong: $("#LoaiHopDong").val(),
                            ThoiGianTao: $("#ThoiGianTao").val(),
                            DoiTacID: $("#DoiTacId").val(),
                            DSHinhAnh: lstHinhAnh,
                            CongTrinhID: $("#IDCongTrinh").val(),
                            CapDoBaoMat: $("#CapDoID").val()
                        }
                        lstHinhAnh = [];
                        return { data: kendo.stringify(model) }
                    } else {
                        return { HopDongID: options.models[0].HopDongID };
                    }
                } else {
                    return JSON.stringify(options);
                }
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        sort: { field: "TenHangMuc", dir: "desc" },
        schema: {
            data: "data",
            total: "total",
            model: {
                id: "HopDongID",
                fields: {
                    HopDongID: { type: "string" },
                    ThoiGianTao: { type: "date" },
                }
            }
        },
        //aggregate: [{ field: "SoCoc", aggregate: "sum" },
        //    { field: "MetDat", aggregate: "sum" },
        //    { field: "MetDa", aggregate: "sum" }]
    });

    var gridDSHopDong = $("#grid_DSHopDong").kendoGrid({
        dataSource: dataHopDong,
        sortable: true,
        height: innerHeight * 0.75,
        filterable: {
            mode: "row"
        },
        columns: [
            {
                field: "ThoiGianTao",
                width: 150,
                title: GetTextLanguage("thoigiantao"),
                filterable: false,
                template: formatToDate("ThoiGianTao"),
            },
            {
                field: "TenHopDong",
                title: GetTextLanguage("noidung"),
                width: 200,
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
            },
            {
                field: "TenDonViTao",
                width: 150,
                title: GetTextLanguage("donvitao"),
                filterable: FilterInTextColumn
            },
            
            {
                field: "TenDoiTac",
                width: 200,
                title: GetTextLanguage("tendoitac"),
                filterable: FilterInTextColumn
            },
            {
                field: "TenLoaiHopDong",
                width: 150,
                title: GetTextLanguage("loaivanban"),
                filterable: FilterInTextColumn,
            }
        ],
        editable: {
            mode: "popup",
            window: {
                title: GetTextLanguage("thongtinhopdong"),
                width: 628
            },
            confirmation: GetTextLanguage("xoabangi"),
            confirmDelete: "Yes",
            template: kendo.template($("#popup_editor").html())
        },
        edit: function (e) {
            lstHinhAnh = [];
            if (e.model.get("HopDongID") != null && e.model.get("HopDongID") != "") {
                e.container.find(".k-edit-buttons").append('<a class="k-button k-button-icontext k-grid-delete text-red" href="javascript:void(0)"><span class="k-icon k-i-close"></span>' + GetTextLanguage("xoa") + '</a>');
                e.container.find(".k-button.k-grid-delete").on("click", function (e) {
                    var uid = $(e.target).closest(".k-popup-edit-form").data("uid");
                    var dataRow = $('#grid_DSHopDong').data("kendoGrid").dataSource.getByUid(uid);
                    $('#grid_DSHopDong').data("kendoGrid").dataSource.remove(dataRow);
                    $('#grid_DSHopDong').data("kendoGrid").dataSource.sync();
                });
                //loadimages
                if (e.model.DinhKem != "" && e.model.DinhKem != null) {
                    lstHinhAnh = e.model.DinhKem.split(',');
                    var htmls = "";
                    for (var i = 0; i < lstHinhAnh.length; i++) {
                        if (lstHinhAnh[i].match(/\.(jpg|jpeg|png|gif)$/)) {
                            htmls += "<div class='img-responsive media-preview imgslider'><a rel='gallery' href='" + localQLVanBan + lstHinhAnh[i] + "'><img src='" + localQLVanBan + lstHinhAnh[i] + "' data-src='" + localQLVanBan + lstHinhAnh[i] + "' class='img-responsive img-rounded media-preview' alt=''  data-file='" + lstHinhAnh[i] + "' /></a><a href='javascript:void(0)' data-file='" + lstHinhAnh[i] + "' class='icross closeImgLoad'><i class='icon-cross3'></i></a><a class='download' href='" + localQLVanBan + lstHinhAnh[i] + "' download>" + GetTextLanguage("taixuong") + "</a></div>";
                        } else {
                            htmls += "<div class='img-responsive media-preview imgslider'><a rel='gallery' href='" + localQLVanBan + lstHinhAnh[i] + "'><img src='/content/assets/images/file.png' data-src='" + localQLVanBan + lstHinhAnh[i] + "' class='img-responsive img-rounded media-preview'  data-file='" + lstHinhAnh[i] + "' alt='' /></a><a href='javascript:void(0)' data-file='" + lstHinhAnh[i] + "' class='icross closeImgLoad'><i class='icon-cross3'></i></a><a class='download' href='" + localQLVanBan + lstHinhAnh[i] + "' download>" + GetTextLanguage("taixuong") + "</a></div>";
                        }

                    }
                    $('#lstImageDisplay').append(htmls);
                    $("#lstImageDisplay").on("click", ".closeImgLoad", function () {
                        var x = confirm(GetTextLanguage("cochacchanxoa"));
                        if (x) {
                            var file = $(this).data("file");
                            for (var i = 0; i < lstHinhAnh.length; i++) {
                                if (lstHinhAnh[i] === file) {
                                    lstHinhAnh.splice(i, 1);
                                    break;
                                }
                            }
                            $(this).parent().remove();
                        }
                    });
                    showslideimg();
                }
            }
            $("#LoaiHopDong").kendoDropDownList({
                dataTextField: 'text',
                dataValueField: 'val',
                filter: "contains",
                optionLabel: GetTextLanguage("chonloaihopdong"),
                dataSource: datafix_loaihopdong,
            });


            $("#DoiTacId").kendoDropDownList({
                dataTextField: 'TenDoi',
                dataValueField: 'DoiThiCongID',
                filter: "contains",
                optionLabel: GetTextLanguage("chondoitac"),
                dataSource: getDataSourceDropDownList("/QuanLyCongTrinh/DanhSachDoiTac")
            });

            $("#CapDoID").kendoDropDownList({
                dataTextField: 'TenCapDo',
                dataValueField: 'TenCapDo',
                filter: "contains",
                optionLabel: GetTextLanguage("capdobaomat"),
                dataSource: getDataSourceDropDownList("/QLHopDong/LayDanhSachCapDoBaoMat")
            });

            $("#ThoiGianTao").kendoDatePicker({ value: new Date(), format: "dd/MM/yyyy" });

            checkImages();
        },
        messages: {
            commands: {
                update: GetTextLanguage("capnhat"),
                canceledit: GetTextLanguage("huy")
            }
        },
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        save: function (e) {
            if (!e.model.isNew()) {
                e.model.dirty = true;
            }
        }
    }).data("kendoGrid");

    $("#grid_DSHopDong .k-grid-content").on("dblclick", "td", function () {
        var rowct = $(this).closest("tr");
        if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            $("#grid_DSHopDong").data("kendoGrid").editRow(rowct);
        }
    });

    $(".add-hopdong").on("click", function () {
        $("#grid_DSHopDong").data("kendoGrid").addRow(null);
    });

});