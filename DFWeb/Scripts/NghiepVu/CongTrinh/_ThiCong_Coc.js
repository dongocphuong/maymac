﻿$(document).ready(function () {
    var dataSourceQuanLyCocThiCong = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: "/QuanLyThiCong/ThiCong_DanhMucCoc_DanhSach?CongTrinhID="+$("#CongTrinhID").val()+"&DoiThiCongID="+$("#DoiThiCongID").val(),
                    dataType: 'json',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                },
                parameterMap: function (data, type) {
                    if (type !== "read") {
                            return { data: kendo.stringify(data.models[0])
                        }
                    } else {
                        return JSON.stringify(data);
                    }
                }
            },
            batch: true,
            sort: [{ field: "TenCoc", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "CocThiCongID",
                    fields: {
                        CocThiCongID: { type: "string" },
                        TenCoc: {
                            type: "string",
                            validation: {
                                required: {
                                    message: GetTextLanguage("khongtrong")
                                }
                            }
                        },
                    },
                }
            },
            pageSize: pageSize,
        });

    var grid = $("#gridQuanLyCocThiCong").kendoGrid({
        dataSource: dataSourceQuanLyCocThiCong,
        height: heightGrid - 90,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='#' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "TenCoc",
                title: GetTextLanguage("tencoc"),
                filterable: FilterInTextColumn,
                attributes: alignLeft
            },
            {
                field: "TenCongTrinh",
                title: GetTextLanguage("congtrinh"),
                filterable: FilterInTextColumn,
                attributes: alignLeft
            },
            {
                field: "TenDoi",
                title: GetTextLanguage("doithicong"),
                filterable: FilterInTextColumn,
                attributes: alignLeft
            },
            {
                field: "TenHangMuc",
                title: GetTextLanguage("tenhangmuc"),
                filterable: FilterInTextColumn,
                attributes: alignLeft
            }]
    });
});