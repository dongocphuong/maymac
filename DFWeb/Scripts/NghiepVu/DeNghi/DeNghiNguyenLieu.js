﻿
$(document).ready(function () {

    
    var gridTonVatTu = $("#grid").kendoGrid({
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/DeNghiNguyenLieu" + "/LayDanhSachDeNghiNguyenLieu",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: { DonHangID: "", SanPhamID: "", TrangThai:1000 }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }

            },
            sort: [{ field: "ThoiGian", dir: "desc" }],
            type: "json",
            batch: true,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "DeNghiNguyenLieuID",
                    fields: {
                        DeNghiNguyenLieuID: { editable: false },
                        MaDeNghi: { editable: false },
                        ThoiGian: { type: "date", editable: false },
                        TenNhanVien: { type: "string", editable: false },
                        TenDonHang: { type: "string", editable: false },
                        TenDoi: { type: "string", editable: false },
          
                    }
                }
            },
        }),
        pageable: pageableAll,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: kendo.template($("#templtool").html()),
        columns: [
            {
                title: GetTextLanguage("stt"),
                template: "<span class='stt'></span>",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "ThoiGian",
                title: GetTextLanguage("thoigian"),
                attributes: { "style": "text-align:center !important;" },
                filterable: FilterInTextColumn,
                width: 150,
                format:"{0:dd/MM/yyyy HH:mm}"
            },
            {
                field: "TrangThai",
                title: GetTextLanguage("trangthai"),
                attributes: { "style": "text-align:left !important;" },
                filterable: FilterInTextColumn, width: 200,
                template: kendo.template($("#tempTrangThai").html())
            },
            {
                field: "MaDeNghi", title: GetTextLanguage("madenghi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
            },
            {
                field: "TenDonHang", title: "Đơn hàng", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
            },
            {
                field: "TenSanPham", title: "Sản Phẩm", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
            },
             {
                  field: "TenNhanVien", title:"Người tạo", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
          },
        ],
        dataBound: function (e) {
            var rows = this.items();
            var dem = 0;
            $(rows).each(function () {
                dem++;
                var rowLabel = $(this).find(".stt");
                $(rowLabel).html(dem);
                if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                    dem = 0;
                }
            });
        },
    });
    $("#grid .k-grid-add").click(function () {
        CreateModalWithSize("mdlChiTiet", "90%", "90%", null, "Đề nghị nguyên liệu");
        $("#mdlChiTiet").load("/DeNghiNguyenLieu" + "/TaoDeNghi_View");
    });
    $("#grid .k-grid-content").on("dblclick", "td", function () {
        var rowct = $(this).closest("tr"),
            grid = $("#grid").data("kendoGrid"),
            dataItem = grid.dataItem(rowct);
        if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            CreateModalWithSize("mdlChiTiet", "90%", "90%", null, "Đề nghị vật tư");
            $("#mdlChiTiet").load("/DeNghiNguyenLieu" + "/XemDeNghi_View?DeNghiNguyenLieuID=" + dataItem.DeNghiNguyenLieuID);
        }
    });


    var datatrangthai = [
      { text: "Chưa duyệt", val: 1 },
      { text: "Đã duyệt", val: 2 },
      { text: "Đã xuất", val: 3 },
      { text: "Đã từ chối", val: 4 },
    ];
    $("#inputTrangThai").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "val",
        autoBind: false,
        optionLabel: "Tất cả trạng thái",
        dataSource: datatrangthai,
        change: function () {
            gridReload("grid", { DonHangID: $("#inputDonHangID").val(), SanPhamID: $("#inputSanPhamID").val(), TrangThai: this.value(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });


    $("#inputDonHangID").kendoDropDownList({
        dataTextField: "TenDonHang",
        dataValueField: "DonHangID",
        autoBind: false,
        optionLabel: "Tất cả đơn hàng",
        dataSource: getDataDroplit("/DeNghiNguyenLieu", "/LayDanhSachDonHangByNhanVien"),
        change: function () {
            $("#inputSanPhamID").data("kendoDropDownList").dataSource.read({ DonHangID: this.value() });
            $("#inputSanPhamID").data("kendoDropDownList").refresh();
            gridReload("grid", { DonHangID: $("#inputDonHangID").val(), SanPhamID: $("#inputSanPhamID").val(), TrangThai: $("#inputTrangThai").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });

    $("#inputSanPhamID").kendoDropDownList({
        dataTextField: "TenSanPham",
        dataValueField: "SanPhamID",
        autoBind: false,
        optionLabel: "Tất cả sản phẩm",
        dataSource: getDataDroplitwithParam("/DeNghiNguyenLieu", "/LayDanhSachSanPhamByNhanVien", { DonHangID: $("#inputDonHangID").val() }),
        change: function () {
            gridReload("grid", { DonHangID: $("#inputDonHangID").val(), SanPhamID: $("#inputSanPhamID").val(), TrangThai: $("#inputTrangThai").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });
    var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
    $("#inputTuNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(y - 5, m, 1), change: function () {
            gridReload("grid", { DonHangID: $("#inputDonHangID").val(), SanPhamID: $("#inputSanPhamID").val(), TrangThai: $("#inputTrangThai").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });
    $("#inputDenNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(), change: function () {
            gridReload("grid", { DonHangID: $("#inputDonHangID").val(), SanPhamID: $("#inputSanPhamID").val(), TrangThai: $("#inputTrangThai").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });
    createpopupdonhang($(".filter-hoadon"));
})