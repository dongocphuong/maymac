﻿$(document).ready(function () {
    //Init
    var dataimg = new FormData();
    //var dataavatar = new FormData();
    var crudServiceBaseUrl = "/YeuCauKhachHang";
    var dataTrangThai = [
       { text: "Chưa bắt đầu", value: 1 },
       { text: "Chưa báo giá", value: 7 },
       { text: "Đã duyệt", value: 5 },
       { text: "Đang sản xuất", value: 2 },
       { text: "Hoàn thành SX", value: 3 },
       { text: "Đã giao hàng", value: 4 },
       { text: "Đã thanh toán xong", value: 6 },
       { text: "Đã hủy", value: 8 },
    ];
    var dataTrangThaiYeuCau = [
       { text: "Đã chọn nguyên liệu", value: 1 },
       { text: "Chưa chọn nguyên liệu", value: 2 },
    ];
    var dataTinhTrang = [
       { text: "Bình thường", value: 1 },
       { text: "Gấp", value: 2 },
       { text: "Yêu cầu đã chốt", value: 3 }
    ];
    var dataLoaiHinh = [
      { text: "May", value: 1 },
      { text: "Dệt", value: 2 },
    ];
    var dataYeuCauKhachHang = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetAllDataYeuCauKhachHang",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {}
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        sort: [{ field: "ThoiGian", dir: "desc" }, { field: "TinhTrang", dir: "asc" }],
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "YeuCauKhachHangID",
                fields: {
                    YeuCauKhachHangID: { editable: false, nullable: true },
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: dataYeuCauKhachHang,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [{ template: kendo.template($("#btnThem").html()) }],
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
            {
                field: "MaYeuCau",
                title: "Mã yêu cầu khách hàng",
                width: 150,
                attributes: alignCenter,
                filterable: FilterInTextColumn,
            },
            {
                field: "TrangThaiYeuCau",
                title: "Trạng thái yêu cầu",
                filterable: FilterInTextColumn,
                filterable: {
                    cell: {
                        template: function (args) {
                            args.element.kendoDropDownList({
                                dataSource: dataTrangThaiYeuCau,
                                dataTextField: "text",
                                dataValueField: "value",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                width: 200, hidden: true,
                template: kendo.template($("#tplTrangThaiYeuCau").html())
            },
            {
                field: "ThoiGian",
                title: "Thời Gian",
                filterable: false,
                attributes: alignCenter,
                width: 100,
                template: formatToDate("ThoiGian"),
            },
            {
                field: "TrangThai",
                title: "Trạng thái",
                filterable: FilterInTextColumn,
                filterable: {
                    cell: {
                        template: function (args) {
                            args.element.kendoDropDownList({
                                dataSource: dataTrangThai,
                                dataTextField: "text",
                                dataValueField: "value",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                width: 200, hidden: true,
                template: kendo.template($("#tplTrangThai").html())
            },
            {
                field: "TinhTrang",
                title: "Loại yêu cầu",
                filterable: FilterInTextColumn,
                filterable: {
                    cell: {
                        template: function (args) {
                            args.element.kendoDropDownList({
                                dataSource: dataTinhTrang,
                                dataTextField: "text",
                                dataValueField: "value",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                width: 150,
                template: kendo.template($("#tplTinhTrang").html())
            },
             {
                 field: "LoaiHinhSanXuat",
                 title: "Loại hinh",
                 filterable: FilterInTextColumn,
                 filterable: {
                     cell: {
                         template: function (args) {
                             args.element.kendoDropDownList({
                                 dataSource: dataLoaiHinh,
                                 dataTextField: "text",
                                 dataValueField: "value",
                                 valuePrimitive: true
                             });
                         },
                         showOperators: false
                     }
                 },
                 width: 150,
                 template: kendo.template($("#tplLoaiHinh").html())
             },
            {
                field: "MaDonHang",
                title: "Mã Đơn Hàng",
                width: 100,
                attributes: alignCenter, hidden: true,
                filterable: FilterInTextColumn,
            },
            {
                field: "TenDoiTac",
                title: "Tên Đối Tác",
                width: 200,
                attributes: alignCenter,
                filterable: FilterInTextColumn
            },
            {
                field: "NoiDung",
                title: "Nội Dung",
                width: 250,
                attributes: { style: "text-align:left;" },
                filterable: FilterInTextColumn,
            },
            {
                field: "TenNhanVien",
                title: "Người tạo",
                width: 200,
                attributes: { style: "text-align:center;" },
                filterable: FilterInTextColumn,
            },
        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    $("#grid").on("dblclick", "td", function () {
        var rows = $(this).closest("tr"),
            grids = $("#grid").data("kendoGrid"),
            dataItems = grids.dataItem(rows);
        if (!rows.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            var dataobjectload = null;
            var lstHinhAnh = [];
            CreateModalWithSize("mdlThemMoiYeuCauKhachHang", "100%", "95%", "tplThemMoiYeuCauKhachHang", "Chi tiết yêu cầu khách hàng");
            gridChiTietYeuCau(dataItems.YeuCauKhachHangID);
            dataimg = new FormData();
            $("#ThoiGianGiaoHang").kendoDatePicker({
                format: "dd/MM/yyyy",
                value: new Date()
            });
            //dataavatar = new FormData();
            $("#btnXoa").removeClass("hidden");
            $("#iptSize").removeClass("hidden");
            $("#btnLichSuSua").removeClass("hidden");
            $("#btnChotYeuCau").removeClass("hidden");
            $("#btnChotYeuCau").click(function () {
                var x = confirm("Bạn có chắc chắn muốn chốt yêu cầu khách hàng này không?");
                if (x) {
                    ContentWatingOP("mdlThemMoiYeuCauKhachHang", 0.9);
                    $.ajax({
                        url: crudServiceBaseUrl + "/ChotYeuCauKhachHang",
                        method: "POST",
                        data: { YeuCauKhachHangID: dataItems.YeuCauKhachHangID },
                        success: function (data) {
                            $("#mdlThemMoiYeuCauKhachHang").unblock();
                            closeModel("mdlThemMoiYeuCauKhachHang");
                            gridReload("grid", {});
                        },
                        error: function (xhr, status, error) {
                            $("#mdlThemMoiYeuCauKhachHang").unblock();
                        }
                    });
                }
            });
            var dssize = [];
            $.ajax({
                url: crudServiceBaseUrl + "/GetDataYeuCauKhachHangID",
                method: "GET",
                data: { data: dataItems.YeuCauKhachHangID },
                success: function (data) {
                    dataobjectload = data.data[0];
                    if (dataobjectload.TinhTrang == 3) {
                        $("#btnLuuThemMoi").addClass("hidden");
                        $("#btnChotYeuCau").addClass("hidden");
                        $("#btnXoa").addClass("hidden");
                        $(".deleteRow").addClass("hidden");
                        $("#btnThemChiTiet").addClass("hidden");
                    }
                    $.ajax({
                        url: crudServiceBaseUrl + "/TakeSize",
                        method: "POST",
                        data: { YeuCauKhachHangID: dataItems.YeuCauKhachHangID },
                        success: function (data) {
                            var dt = data.data;
                            var st = "";
                            for (var i = 0; i < dt.length; i++) {
                                st += dt[i].Size + ", ";
                                dssize.push({ Size: dt[i].Size });
                            }
                            $("#Size").val(st);
                        },
                        error: function (xhr, status, error) {
                        }
                    });
                    $("#cbbDoiTac").kendoDropDownList({

                        dataTextField: 'TenDoiTac',
                        dataValueField: 'DoiTacID',
                        filter: "contains",
                        optionLabel: "Chọn đối tác",
                        dataSource: getDataDroplit("/YeuCauKhachHang", "/read")
                    });
                    $("#TinhTrang").kendoDropDownList({

                        dataTextField: 'text',
                        dataValueField: 'value',
                        filter: "contains",
                        optionLabel: "Chọn loại yêu cầu",
                        dataSource: [{ text: "Bình thường", value: 1 }, { text: "Gấp", value: 2 }]
                    });
                    $("#GiaBan").autoNumeric({
                        digitGroupSeparator: ',',
                        decimalCharacter: '.',
                        minimumValue: '0',
                        maximumValue: '999999999999999',
                        emptyInputBehavior: 'zero'
                        
                    });
                    $("#SoLuongDat").autoNumeric({
                        digitGroupSeparator: ',',
                        decimalCharacter: '.',
                        minimumValue: '0',
                        maximumValue: '999999999999999',
                        emptyInputBehavior: 'zero'
                    });
                    $("#LoaiHinhSanXuat").kendoDropDownList({
                        dataTextField: 'text',
                        dataValueField: 'value',
                        filter: "contains",
                        optionLabel: "Chọn loại hình",
                        dataSource: dataLoaiHinh
                    });
                    var viewModel = kendo.observable({
                        YeuCauKhachHangID: dataobjectload == null ? "00000000-0000-0000-0000-000000000000" : dataobjectload.YeuCauKhachHangID,
                        NoiDung: dataobjectload == null ? "" : dataobjectload.NoiDung,
                        DoiTacID: dataobjectload == null ? "" : dataobjectload.DoiTacID,
                        HinhAnh: dataobjectload == null ? "" : dataobjectload.HinhAnh,
                        ThoiGian: dataobjectload == null ? "" : dataobjectload.ThoiGian,
                        MaDonHang: dataobjectload == null ? "" : dataobjectload.MaDonHang,
                        LoaiHinhSanXuat: dataobjectload == null ? "" : dataobjectload.LoaiHinhSanXuat,
                        TinhTrang: dataobjectload == null ? "" : dataobjectload.TinhTrang,
                        GiaBan: dataobjectload == null ? "" : dataobjectload.GiaBan,
                        SoLuongDat: dataobjectload == null ? "" : dataobjectload.SoLuongDat,
                        ThoiGianGiaoHang: dataobjectload == null ? "" : dataobjectload.ThoiGianGiaoHang,
                    });
                    kendo.bind($("#frmThemMoiYeuCauKhachHang"), viewModel);
                    showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                    var lt = dataobjectload.HinhAnh;
                    if (lt == "" || lt == null) {
                        var lst = lt;
                    }
                    else {
                        var lst = lt.split(',');
                        if (lst.length > 0 && lst[0] != "") {
                            for (var i = 0; i < lst.length; i++) {
                                lstHinhAnh.push({ img: lst[i], name: lst[i] });
                            }
                            var html = "";
                            for (var i = 0; i < lstHinhAnh.length; i++) {
                                if (lstHinhAnh[i].img.match(/\.(jpg|jpeg|png|gif)$/)) {
                                    html += "<div class='imgslider' style='float:left; position:relative; width:70px;height:70px; margin:5px;'>";
                                    html += "<div style='margin:0px;' class='img-con ahihi3 img-thumbnail'><a rel='gallery' href='" + (localQLVanBan + lstHinhAnh[i].img) + "'>";
                                    html += "<img src='" + localQLVanBan + lstHinhAnh[i].img + "'  data-file='" + (lstHinhAnh[i].img) + "' style='width: 70px;height:70px;margin: 0;opacity: 1;'></a></div>";
                                    html += "</div>";
                                }
                            }
                            $('.dshinhanhadd').append(html);
                            showslideimg5($('#frmThemMoiYeuCauKhachHang'));
                        }
                    }
                    //image
                    btnLuuDMYeuCauKhachHang(dssize);
                },
                error: function (xhr, status, error) {
                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                }
            });
            LoadNguyenLieu(dataItems.YeuCauKhachHangID);
            $("#btnXoa").click(function () {
                var x = confirm("Bạn có chắc chắn muốn xóa danh mục này không?");
                if (x) {
                    ContentWatingOP("mdlThemMoiYeuCauKhachHang", 0.9);
                    $.ajax({
                        url: crudServiceBaseUrl + "/XoaYeuCauKhachHang",
                        method: "POST",
                        data: { data: dataItems.YeuCauKhachHangID },
                        success: function (data) {
                            showToast(data.code, data.code, data.message);
                            if (data.code == "success") {
                                $("#mdlThemMoiYeuCauKhachHang").unblock();
                                $(".windows8").css("display", "none");
                                closeModel("mdlThemMoiYeuCauKhachHang");
                                gridReload("grid", {});
                            }
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $("#mdlThemMoiYeuCauKhachHang").unblock();
                            $(".windows8").css("display", "none");
                            gridReload("grid", {});
                        }
                    });
                }
            })
        }
    });

    $("#grid").on("click", "#btnAdd", function () {
        CreateModalWithSize("mdlThemMoiYeuCauKhachHang", "100%", "95%", "tplThemMoiYeuCauKhachHang", "Thêm mới yêu cầu khách hàng");
        dataimg = new FormData();
        $("#ThoiGianGiaoHang").kendoDatePicker({
            format: "dd/MM/yyyy",
            value: new Date()
        });
        $("#cbbDoiTac").kendoDropDownList({

            dataTextField: 'TenDoiTac',
            dataValueField: 'DoiTacID',
            filter: "contains",
            optionLabel: "Chọn đối tác",
            dataSource: getDataDroplit("/YeuCauKhachHang", "/read")
        });
        $("#GiaBan").autoNumeric({
            digitGroupSeparator: ',',
            decimalCharacter: '.',
            minimumValue: '0',
            maximumValue: '999999999999999',
            emptyInputBehavior: 'zero'
        });
        $("#SoLuongDat").autoNumeric({
            digitGroupSeparator: ',',
            decimalCharacter: '.',
            minimumValue: '0',
            maximumValue: '999999999999999',
            emptyInputBehavior: 'zero'
        });
        $("#TinhTrang").kendoDropDownList({

            dataTextField: 'text',
            dataValueField: 'value',
            filter: "contains",
            optionLabel: "Chọn loại yêu cầu",
            dataSource: [{ text: "Bình thường", value: 1 }, { text: "Gấp", value: 2 }]
        });
        $("#LoaiHinhSanXuat").kendoDropDownList({

            dataTextField: 'text',
            dataValueField: 'value',
            filter: "contains",
            optionLabel: "Chọn loại hình",
            dataSource: dataLoaiHinh
        });
        gridChiTietYeuCau();
        LoadNguyenLieu('00000000-0000-0000-0000-000000000000');
        btnLuuDMYeuCauKhachHang();
    })
    function btnLuuDMYeuCauKhachHang(dssize) {
        //Image---------------<
        $("#frmThemMoiYeuCauKhachHang #files_0").change(function () {
            for (var x = 0; x < this.files.length; x++) {
                dataimg.append("uploads", this.files[x]);
            };
            loadimgSuaChua5(this, $("#frmThemMoiYeuCauKhachHang"));
            $("#frmThemMoiYeuCauKhachHang #files_0").val('').clone(true);
        });
        //avatar

        //$("#frmThemMoiYeuCauKhachHang #file_1").change(function () {
        //    dataavatar = new FormData();
        //    for (var x = 0; x < this.files.length; x++) {
        //        dataavatar.append("uploads", this.files[x]);
        //    };
        //    loadimgavatar(this, $("#frmThemMoiYeuCauKhachHang"));
        //    $("#frmThemMoiYeuCauKhachHang #file_1").val('').clone(true);
        //})
        //Image------------------------>
        var size = dssize;
        var str = "";
        $("#Size").click(function () {
            CreateModalWithSize("mdlChonSize", "45%", "20%", "tplChonSize", "Chọn danh sách size");
            $("#btnLuuDanhSachSize").click(function () {
                size = [];
                str = "";
                var flag = false;
                ContentWatingOP("mdlChonSize", 0.9);
                $("#DanhSachSize input").each(function (i, e) {
                    if ($(e).is(':checked')) {
                        flag = true;
                        size.push({ Size: $(e).val() });
                    }
                });
                if (flag && size.length > 0) {
                    for (var i = 0; i < size.length; i++) {
                        str += size[i].Size + ", ";
                    }
                    $("#Size").val(str);
                    $("#mdlChonSize").unblock();
                    closeModel("mdlChonSize");
                }
                else {
                    $("#mdlChonSize").unblock();
                    showToast("warning", "Chưa chọn số size", "Hãy chọn size cho tất cả các sản phẩm!");
                }
            });
        });
        $("#btnLuuThemMoi").click(function () {
            var validator = $("#frmThemMoiYeuCauKhachHang").kendoValidator().data("kendoValidator");
            if (validator.validate()) {
                var data = $("#gridChiTietYeuCau").data("kendoGrid").dataSource.data();
                console.log(size);
                if (size.length > 0) {
                    if (data.length > 0) {
                        //image
                        var hinhanh = "";
                        var ins = dataimg.getAll("uploads").length;
                        var DSHinhAnh = [];
                        $('.dshinhanhadd .imgslider').each(function (index2, element2) {
                            if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                                DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                            }
                        });
                        if (ins > 0) {
                            var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/UploadHopDong", dataimg.getAll("uploads"), $(".dshinhanhadd", $('#frmThemMoiYeuCauKhachHang'))).split(',');
                            for (var k = 0; k < strUrl.length; k++) {
                                DSHinhAnh.push({ Url: strUrl[k] });
                            }
                        }
                        for (var i = 0; i < DSHinhAnh.length; i++) {
                            if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                                hinhanh += DSHinhAnh[i].Url + ",";
                            }
                        }
                        var dataYeuCauKhachHang = {
                            YeuCauKhachHangID: $("#YeuCauKhachHangID").val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#YeuCauKhachHangID").val(),
                            NoiDung: " ",
                            ThoiGian: $("#ThoiGian").val(),
                            DoiTacID: $("#cbbDoiTac").val(),
                            TinhTrang: $("#TinhTrang").val(),
                            LoaiHinhSanXuat: $("#LoaiHinhSanXuat").val(),
                            HinhAnh: hinhanh.substring(0, hinhanh.length - 1),
                        }
                        ContentWatingOP("mdlThemMoiYeuCauKhachHang", 0.9);
                        $.ajax({
                            url: crudServiceBaseUrl + "/AddOrUpdateYeuCauKhachHang",
                            method: "post",
                            data: {
                                data: JSON.stringify(dataYeuCauKhachHang),
                                dataChiTiet: JSON.stringify(data),
                                dataSize: JSON.stringify(size),
                                SoLuongDat: $("#SoLuongDat").autoNumeric("get"),
                                GiaBan: $("#GiaBan").autoNumeric("get"),
                                ThoiGianGiaoHang: $("#ThoiGianGiaoHang").val(),
                            },
                            success: function (data) {
                                showToast(data.code, data.code, data.message);
                                if (data.code == "success") {
                                    $("#mdlThemMoiYeuCauKhachHang").unblock();
                                    $("#mdlChonSize").unblock();
                                    $(".windows8").css("display", "none");
                                    closeModel("mdlThemMoiYeuCauKhachHang");
                                    gridReload("grid", {});
                                } else {
                                    $("#mdlThemMoiYeuCauKhachHang").unblock();

                                }
                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                $("#mdlThemMoiYeuCauKhachHang").unblock();
                                $(".windows8").css("display", "none");
                                gridReload("grid", {});
                            }
                        });
                    }
                    else {
                        showToast("warning", "Cảnh báo", "Bạn cần điền dữ liệu cho chi tiết yêu cầu!");
                    }
                }
                else {
                    showToast("warning", "Chưa chọn số size", "Hãy chọn size cho tất cả các sản phẩm!");
                }
            }
        });
    }
    function LoadNguyenLieu(YeuCauKhachHangID) {
        $("#gridDinhNghia").kendoGrid({
            dataSource: new kendo.data.DataSource(
           {
               transport: {
                   read: {
                       url: crudServiceBaseUrl + "/DanhSachNguyenLieu",
                       dataType: 'json',
                       type: 'GET',
                       contentType: "application/json; charset=utf-8",
                       data: { YeuCauKhachHangID: YeuCauKhachHangID }
                   }
               },
               batch: true,
               pageSize: 20,
               sort: [{ field: "TenVatTu", dir: "asc" }],
               schema: {
                   data: 'data',
                   total: 'total',
                   model: {
                       id: "NguyenLieuID",
                       fields: {
                           NguyenLieuID: { editable: false },
                           TenDonVi: { type: "string", editable: false },
                           TenNguyenLieu: { type: "string", editable: false },
                           MaNguyenLieu: { type: "string", editable: false },
                           LoaiHinh: { type: "number", editable: false },
                           SoLuong: { type: "number", editable: true },
                           LoaiVai: { type: "number", editable: true },
                       }
                   }
               },
           }),
            height: window.innerHeight * 0.80,
            width: "100%",
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            editable: {
                confirmation: GetTextLanguage("xoabangi"),
                confirmDelete: "Yes",
            },
            dataBinding: function () {
                record = 0;
            },
            pageable: pageableAll,
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "#= ++record #",
                    width: 60,
                    attributes: alignCenter
                },
                {
                    field: "AnhDaiDien", title: "Ảnh",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    template: kendo.template($("#tplAnh").html()),
                    width: 70
                },

                {
                    field: "TenNguyenLieu", width: 200,
                    title: "Tên nguyên liệu",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "MaNguyenLieu",
                    width: 100,
                    attributes: { style: "text-align:left;" },
                    title: "Mã nguyên liệu",
                    filterable: FilterInTextColumn
                },
                {
                    field: "TenDonVi", width: 100,
                    title: GetTextLanguage("donvi"),
                    attributes: { "style": "text-align:left !important;" },
                    filterable: FilterInTextColumn
                },
                 {
                     title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                     template: '<a class="k-button k-button-icontext k-grid-delete" href="javascript:void(0)"><span class="k-icon k-i-delete"></span>Xóa</a>',
                     width: 100
                 },

            ],

            dataBound: function (e) {
                showslideimg10();
            },
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $("#btnChonNguyenLieu").click(function () {
            if (true) {
                CreateModalWithSize("mdlDanhSachNguyenLieu", "99%", "90%", "tplDanhSachNguyenLieu", "Dannh sách nguyên liệu");
                $("#gridDanhSachNguyenLieu").kendoGrid({
                    dataSource: new kendo.data.DataSource({
                        transport: {
                            read: {
                                url: crudServiceBaseUrl + "/DanhSachNguyenLieu",
                                dataType: "json",
                                type: "GET",
                                contentType: "application/json; charset=utf-8",
                            },
                        },
                        schema: {
                            data: "data",
                            total: "total",
                            model: {
                                id: "NguyenLieuID",
                                fields: {
                                    TenNguyenLieu: { type: "string", editable: false },
                                    MaNguyenLieu: { type: "string", editable: false },
                                    TenDonVi: { type: "string", editable: false },
                                    SoLuong: { type: "number", editable: true },
                                    LoaiHinh: { type: "number", editable: false },
                                    Selected: { type: "bool", editable: false },
                                }
                            }
                        },
                    }),
                    filterable: {
                        mode: "row"
                    },
                    sortable: true,
                    resizable: true,
                    //selectable: "multiple, row",
                    height: innerHeight * 0.75,
                    width: "100%",
                    dataBound: function (e) {
                        showslideimg10();
                        var rows = this.items();
                        var dt = this._data;
                        $(rows).each(function () {
                            var rowLabel = $(this).data("uid");
                            var id = $(this);
                            for (var i = 0; i < dt.length; i++) {
                                if (dt[i].uid == rowLabel) {
                                    if (dt[i].Selected) {
                                        id.addClass("k-state-selected");
                                    }
                                }
                            }
                        });
                    },
                    columns: [
                          {
                              title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                              template: "#= ++record #",
                              width: 60,
                              attributes: alignCenter
                          },
                           {
                               field: "Type",
                               title: "Loại Nguyên Liệu",
                               filterable: {
                                   cell: {
                                       template: function (args) {
                                           args.element.kendoDropDownList({
                                               dataSource: [{ text: "Dệt", value: 1 }, { text: "May", value: 2 }, { text: "Phụ kiện", value: 3 }, ],
                                               dataTextField: "text",
                                               dataValueField: "value",
                                               valuePrimitive: true
                                           });
                                       },
                                       showOperators: false
                                   }
                               },
                               template: kendo.template($("#tplLoaiNguyenLieu").html()),
                               attributes: { style: "text-align:center;" },
                               width: 100,
                           },
                          {
                              field: "AnhDaiDien", title: "Ảnh",
                              attributes: { style: "text-align:left;" },
                              filterable: FilterInTextColumn,
                              template: kendo.template($("#tplAnh").html()),
                              width: 70
                          },
                          {
                              field: "TenNguyenLieu",
                              title: "Tên nguyên liệu",
                              filterable: FilterInTextColumn,
                              attributes: alignLeft,
                              width: 200
                          },
                        {
                            field: "MaNguyenLieu",
                            title: "Mã nguyên liệu",
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: 150
                        },
                        {
                            field: "TenDonVi",
                            title: GetTextLanguage("donvi"),
                            width: 200,
                            attributes: { "style": "text-align:left !important;" },
                            filterable: FilterInTextColumn
                        },

                    ],


                });
                $("#gridDanhSachNguyenLieu").on("click", "td", function () {
                    var rows = $(this).closest("tr"),
                        grids = $("#gridDanhSachNguyenLieu").data("kendoGrid"),
                        dataItems = grids.dataItem(rows);
                    var a = grids.dataSource.data();
                    if (!rows.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                        for (var i = 0; i < a.length; i++) {
                            if (a[i].NguyenLieuID == dataItems.NguyenLieuID) {
                                if (a[i].Selected) {
                                    a[i].Selected = false;
                                } else {
                                    a[i].Selected = true;
                                }
                            }
                        }
                        $("#gridDanhSachNguyenLieu").data("kendoGrid").dataSource.data(a);
                    }
                });
                $("#btnDongY").click(function () {
                    var arrData = $("#gridDinhNghia").data("kendoGrid").dataSource.data();
                    var dataItem = $("#gridDanhSachNguyenLieu").data("kendoGrid").dataSource.data();
                    for (var i = 0; i < dataItem.length; i++) {
                        if (dataItem[i].Selected) {
                            arrData.push(dataItem[i]);
                        }
                    }
                    $("#mdlDanhSachNguyenLieu").data("kendoWindow").close();
                    $("#gridDinhNghia").data("kendoGrid").dataSource.data(arrData);
                });
                $("#btnHuyChonVatTu").click(function () {
                    $("#mdlDanhSachNguyenLieu").data("kendoWindow").close();
                });
            }
        });

    }
    function gridChiTietYeuCau(yckhid) {
        //Build grid
        $("#gridChiTietYeuCau").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: crudServiceBaseUrl + "/GetAllDataChiTietYeuCauKhachHang",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: { YeuCauKhachHangID: yckhid }

                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                batch: true,
                pageSize: 50,
                sort: [{ field: "TenSanPham", dir: "asc" }, { field: "Mau", dir: "asc" }],
                schema: {
                    type: 'json',
                    data: 'data',
                    total: "total",
                    model: {
                        id: "ChiTietYeuCauID",
                        fields: {
                        }
                    }
                },
            }),
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            height: window.innerHeight * 0.5,
            width: "100%",
            toolbar: [{ template: kendo.template($("#tlbgridChiTietYeuCau").html()) }],
            columns: [
                {
                    field: "TenSanPham",
                    title: "Tên sản phẩm",
                    width: 100,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn,
                },
                {
                    field: "DacDiem",
                    title: "Đặc điểm",
                    width: 200,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn,
                },
                {
                    field: "ChatLieu",
                    title: "Chất liệu",
                    width: 100,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn,
                },
                {
                    field: "Mau",
                    title: "Màu chính",
                    width: 100,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn,
                },
                {
                    field: "MauPhoi",
                    title: "Màu phối",
                    width: 100,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn,
                },
                //{
                //    field: "GiaBan",
                //    title: "Giá bán",
                //    width: 100,
                //    attributes: alignCenter,
                //    filterable: FilterInColumn,
                //    format: "{0:n0}",
                //    hidden: yckhid == null ? true : false
                //},
                {
                    field: "YeuCauKhac",
                    title: "Ghi chú",
                    width: 150,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn,
                },
                {
                    width: 50,
                    attributes: {
                        "class": "table-cell",
                        style: "font-size: 14px; height:53px; text-align:center;"
                    },
                    filterable: false,
                    template: "<a type='button' class='btn btn-block btn-danger deleteRow' style='width:60% !important;'>Xóa</a>"
                },
            ],
            editable: true
        });
        //$("#gridChiTietYeuCau").on("dblclick", "td", function () {
        //    var rows = $(this).closest("tr"),
        //        grids = $("#gridChiTietYeuCau").data("kendoGrid"),
        //        dataItems = grids.dataItem(rows);
        //    CreateModalWithSize("mdlThemMoiChiTietYeuCau", "100%", "95%", "tplThemMoiChiTietYeuCau", "Thông tin chi tiết yêu cầu khách hàng");
        //    $("#TenSanPham").val(dataItems.TenSanPham);
        //    $("#DacDiem").val(dataItems.DacDiem);
        //    $("#ChatLieu").val(dataItems.ChatLieu);
        //    $("#Mau").val(dataItems.Mau);
        //    $("#MauPhoi").val(dataItems.MauPhoi);
        //    $("#YeuCauKhac").val(dataItems.YeuCauKhac);
        //    $("#btnTiepTuc").click(function () {
        //        var validate = $("#frm").kendoValidator().data("kendoValidator");
        //        if (validate.validate()) {
        //            var arrData = $("#gridChiTietYeuCau").data("kendoGrid").dataSource.data();
        //            for (var i = 0; i <= arrData.length; i++) {
        //                if (arrData[i].uid == dataItems.uid) {
        //                    arrData[i].TenSanPham = $("#TenSanPham").val(),
        //                    arrData[i].DacDiem = $("#DacDiem").val(),
        //                    arrData[i].ChatLieu = $("#ChatLieu").val(),
        //                    arrData[i].Mau = $("#Mau").val(),
        //                    arrData[i].MauPhoi = $("#MauPhoi").val(),
        //                    arrData[i].YeuCauKhac = $("#YeuCauKhac").val(),
        //                    $("#gridChiTietYeuCau").data("kendoGrid").dataSource.data(arrData);
        //                    $("#mdlThemMoiChiTietYeuCau").data("kendoWindow").close();
        //                }
        //            }
        //        };
        //    });
        //});
        $("#gridChiTietYeuCau").on("click", "#btnThemChiTiet", function () {
            CreateModalWithSize("mdlThemMoiChiTietYeuCau", "20%", "80%", "tplThemMoiChiTietYeuCau", "Thêm mới chi tiết yêu cầu khách hàng");
            $("#btnTiepTuc").click(function () {
                var validate = $("#frm").kendoValidator().data("kendoValidator");
                if (validate.validate()) {
                    var gridCT = $("#gridChiTietYeuCau").data("kendoGrid");
                    gridCT.dataSource.add({ TenSanPham: $("#TenSanPham").val(), Mau: $("#Mau").val(), YeuCauKhac: $("#YeuCauKhac").val(), DacDiem: $("#DacDiem").val(), ChatLieu: $("#ChatLieu").val(), MauPhoi: $("#MauPhoi").val() })
                    closeModel("mdlThemMoiChiTietYeuCau");
                };
            });
        });
        $("#gridChiTietYeuCau").on("click", "#btnLichSuSua", function () {
            CreateModalWithSize("mdlLichSuSua", "60%", "80%", "tplLichSuSua", "Lịch sử sửa dữ liệu");
            $("#gridLichSuSua").kendoGrid({
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: crudServiceBaseUrl + "/GetAllDataLichSuSua",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            data: { YeuCauKhachHangID: yckhid }
                        },
                        parameterMap: function (options, type) {
                            return JSON.stringify(options);
                        }
                    },
                    type: "json",
                    batch: true,
                    pageSize: 50,
                    sort: [{ field: "TenSanPham", dir: "asc" }, { field: "Mau", dir: "asc" }, { field: "Size", dir: "asc" }],
                    schema: {
                        type: 'json',
                        data: 'data',
                        total: "total",
                        model: {
                            id: "LichSuChiTietYeuCau",
                            fields: {
                            }
                        }
                    },
                    group: [{
                        field: "ThoiGian",
                        dir: "desc",
                    },
                    {
                        field: "Times",
                        dir: "asc",
                    }],
                }),
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    buttonCount: 5,
                    pageSizes: true,
                    messages: messagegrid
                },
                height: window.innerHeight * 0.7,
                width: "100%",
                columns: [
                    {
                        field: "ThoiGian", title: "Thời gian sửa",
                        attributes: alignCenter, filterable: false, width: 150,
                        hidden: true,
                        template: formatToDateTime("ThoiGian"),
                        groupHeaderTemplate: "Thời gian sửa: #= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy') #"
                    },
                    {
                        field: "Times", title: "Lần sửa thứ",
                        attributes: alignCenter, filterable: false, width: 150,
                        hidden: true,
                    },
                    {
                        field: "TenSanPham",
                        title: "Tên sản phẩm",
                        width: 100,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                    },
                    {
                        field: "DacDiem",
                        title: "Đặc điểm",
                        width: 200,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                    },
                    {
                        field: "ChatLieu",
                        title: "Chất liệu",
                        width: 100,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                    },
                    {
                        field: "Mau",
                        title: "Màu chính",
                        width: 100,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                    },
                    {
                        field: "MauPhoi",
                        title: "Màu phối",
                        width: 100,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                    },
                    {
                        field: "GiaBan",
                        title: "Giá bán",
                        width: 100,
                        attributes: alignCenter,
                        filterable: FilterInColumn,
                        format: "{0:n0}",
                        hidden: yckhid == null ? true : false
                    },
                    {
                        field: "YeuCauKhac",
                        title: "Ghi chú",
                        width: 150,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                    },
                ],
            });
        });
        $("#gridChiTietYeuCau").on("click", ".deleteRow", function (e) {
            if (confirm("Bạn có chắc chắn muốn chi tiết này?")) {
                var gridCT = $("#gridChiTietYeuCau").data("kendoGrid");
                var tr = $(e.target).closest("tr");
                gridCT.removeRow(tr);
            }
        });
    }
});
