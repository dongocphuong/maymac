﻿function getNoidungGroupLichSuNhap(value) {
    var datasource = $("#gridLichSuNhap").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[i].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupLichSuXuat(value) {
    var datasource = $("#gridLichSuXuat").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                result = group[i].NoiDung
            }
        }
    });
    return result;
};
$(document).ready(function () {
    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });

    var currentController = "/KhoNguyenLieuDangSanXuat";

    var dataSourceKhoTongVatTu = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: currentController + "/GetDataTonKho",
                    dataType: 'json',
                    type: 'GET',
                    contentType: "application/json; charset=utf-8",
                    data: { DonHangID: "" }
                }
            },
            batch: true,
            pageSize: 100,
            sort: [{ field: "TenNguyenLieu", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "NguyenLieuID",
                    fields: {
                        VatTuID: { type: "string" },
                        TenVatTu: { type: "string" },
                        TenDonVi: { type: "string" },
                        TonHienTai: { type: "number" },
                        GhiChu: { type: "string" },
                        SoLuongCan: { type: "number" },
                        SoLuongTon: { type: "number" },
                        SoLuongXuat: { type: "number" },

                    },
                }
            },
            group: {
                field: "MaDonHang",
                dir: "asc"
            },
        });

    $("#gridKhoNguyenLieuDangSanXuat").kendoGrid({
        dataSource: dataSourceKhoTongVatTu,
        height: window.innerHeight * 0.75,
        width: "100%",
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "MaDonHang",
                title: "Mã đơn hàng",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200,
                hidden: true
            },
            {
                field: "AnhDaiDien", title: "Ảnh",
                attributes: { style: "text-align:left;" },
                filterable: false,
                template: kendo.template($("#tplAnh").html()),
                width: 70
            },
            {
                field: "TenNguyenLieu",
                title: "Tên nguyên liệu",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 150
            },
            {
                field: "MaNguyenLieu",
                title: "Mã nguyên liệu",
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 150
            },
            {
                field: "TenDonVi",
                title: GetTextLanguage("tendonvi"),
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 100
            },
            {
                field: "SoLuongCan",
                title: "Số lượng cần",
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongCan"),
                width: 100
            },
            {
                field: "SoLuongTon",
                title: "Tồn trong kho",
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongTon"),
                width: 100
            },
            {
                field: "SoLuongXuat",
                title: "Số lượng đã sử dụng",
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongXuat"),
                width: 100
            },

        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    var dataSourceKhoTong = new kendo.data.DataSource({
        transport: {
            read: {
                url: currentController + "/GetAllHoaDon",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8"
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    });
    $("#XuatExcel").click(function () {
        var url = "/KhoNguyenLieuDangSanXuat/ExportToExcel?DonHangID=" + $("#HoaDonID").val();
        location.href = url;
    });

    $("#HoaDonID").kendoDropDownList({
        dataSource: dataSourceKhoTong,
        filter: "contains",
        dataTextField: "TextHienThi",
        dataValueField: "DonHangID",
        optionLabel: "Tất cả",
        headerTemplate: kendo.template($("#HoaDonTemplateHeader").html()),
        template: kendo.template($("#HoaDonTemplate").html()),
        valueTemplate: kendo.template($("#HoaDonValueTemplateHeader").html()),
        change: function () {
            var value = this.value();
            $("#gridKhoNguyenLieuDangSanXuat").data("kendoGrid").dataSource.read({ DonHangID: value });
        }
    });
    createpopupdonhang($(".filter-hoadon"));
    //LS nhập xuất
    $("#btnLichSuNhap").click(function () {
        if ($("#HoaDonID").val() == null || $("#HoaDonID").val() == "") {
            showToast("warning", GetTextLanguage("canhbao"), "Bạn phải chọn đơn hàng trước");
        }
        else {
            CreateModalWithSize("mdlLichSuNhap", "95%", "90%", "tplLichSuNhap", "Lịch sử nhập");
            var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
            $("#inputTuNgay").kendoDatePicker({
                format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                    var value = this.value();
                    gridReload("gridLichSuNhap", { DonHangID: $("#HoaDonID").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay").val() });
                }
            });
            $("#inputDenNgay").kendoDatePicker({
                format: "dd/MM/yyyy", value: new Date(), change: function () {
                    var value = this.value();
                    gridReload("gridLichSuNhap", { DonHangID: $("#HoaDonID").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
                }
            });
            var dataLichSuNhapVatTuKhoTong = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/GetDataLichSuNhap",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: { DonHangID: $("#HoaDonID").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() }
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                pageSize: 25,
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        id: "ChiTietPhieuNhapNguyenLieuID",
                        fields: {
                            PhieuNhapNguyenLieuID: { type: "string" },
                            ThoiGian: { type: "date" }
                        }
                    }
                },
                group: {
                    field: "ThoiGian",
                    dir: "desc",
                    aggregates: [{
                        field: "ThanhTien",
                        aggregate: "sum"
                    }]
                },
                sort: [{ field: "TenNguyenLieu", dir: "asc" }],
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
            });
            $("#gridLichSuNhap").kendoGrid({
                dataSource: dataLichSuNhapVatTuKhoTong,
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: pageableAll,
                height: innerHeight * 0.7,
                width: "100%",
                columns: [
                    {
                        title: GetTextLanguage("stt"),
                        template: "<span class='stt'></span>",
                        width: 60,
                        align: "center"
                    },
                    {
                        field: "ThoiGian", title: GetTextLanguage("thoigian"),
                        attributes: alignCenter, filterable: FilterInColumn, width: 150,
                        hidden: true,
                        template: formatToDateTime("ThoiGian"),
                        groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuNhap(value)#</span>",
                    },
                     {
                         field: "AnhDaiDien", title: "Ảnh",
                         attributes: { style: "text-align:left;" },
                         filterable: FilterInTextColumn,
                         template: kendo.template($("#tplAnh").html()),
                         width: 70
                     },
                     {
                         field: "MaPhieuNhap", title: GetTextLanguage("maphieu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                     },
                    {
                        field: "TenNguyenLieu", title: "Tên nguyên liệu", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                    },
                    {
                        field: "MaNguyenLieu", title: "Mã nguyên liệu", attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 100
                    },
                    {
                        field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 100
                    },
                    {
                        field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                        template: "#= kendo.toString(SoLuong,'n2') #",
                        footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                    },
                    {
                        field: "GiaNhap", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                        template: "#= kendo.toString(GiaNhap,'n2') #",
                    },
                    {
                        field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                        attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                        template: "#= kendo.toString(ThanhTien,'n2') #",
                        footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                    },
                    {
                        field: "TenDoiTac", title: GetTextLanguage("doitac"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                    },
                    {
                        field: "TenNhanVien", title: "Nhân viên nhập", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                    },
                ],
                dataBound: function (e) {
                    showslideimg10();
                    var rows = this.items();
                    var dem = 0;
                    $(rows).each(function () {
                        dem++;
                        var rowLabel = $(this).find(".stt");
                        $(rowLabel).html(dem);
                        if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                            dem = 0;
                        }
                    });
                },
            });
        }
    })
    $("#btnLichSuXuat").click(function () {
        if ($("#HoaDonID").val() == null || $("#HoaDonID").val() == "") {
            showToast("warning", GetTextLanguage("canhbao"), "Bạn phải chọn đơn hàng trước");
        }
        else {
            CreateModalWithSize("mdlLichSuXuat", "95%", "90%", "tplLichSuXuat", "Lịch sử xuất");
            var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
            $("#inputTuNgay2").kendoDatePicker({
                format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                    var value = this.value();
                    gridReload("gridLichSuXuat", { DonHangID: $("#HoaDonID").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay2").val() });
                }
            });
            $("#inputDenNgay2").kendoDatePicker({
                format: "dd/MM/yyyy", value: new Date(), change: function () {
                    var value = this.value();
                    gridReload("gridLichSuXuat", { DonHangID: $("#HoaDonID").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
                }
            });
            var dataLichSuXuatVatTuKhoTong = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/GetDataLichSuXuat",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            DonHangID: $("#HoaDonID").val(),
                            TuNgay: $("#inputTuNgay2").val(),
                            DenNgay: $("#inputDenNgay2").val()
                        }
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                pageSize: 25,
                schema: {
                    data: "data",
                    total: "total",
                },
                group: {
                    field: "ThoiGian",
                    dir: "desc",
                    aggregates: [{
                        field: "ThanhTien",
                        aggregate: "sum"
                    }]
                },
                sort: [{ field: "TenNguyenLieu", dir: "asc" }],
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
            });
            $("#gridLichSuXuat").kendoGrid({
                dataSource: dataLichSuXuatVatTuKhoTong,
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: pageableAll,
                height: window.innerHeight * 0.9 - 100,
                width: "100%",
                columns: [
                    {
                        title: GetTextLanguage("stt"),
                        template: "<span class='stt'></span>",
                        width: 60,
                        align: "center"
                    },
                    {
                        field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                        hidden: true,
                        template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                        groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuXuat(value)#</span>",
                    },
                     {
                         field: "MaPhieuXuat", title: "Mã phiếu", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                     },
                      {
                          field: "AnhDaiDien", title: "Ảnh",
                          attributes: { style: "text-align:left;" },
                          filterable: FilterInTextColumn,
                          template: kendo.template($("#tplAnh").html()),
                          width: 70
                      },
                      {
                          field: "TenNguyenLieu", title: "Tên nguyên liệu", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                      },
                    {
                        field: "MaNguyenLieu", title: "Mã nguyên liệu", attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 100
                    },
                    {
                        field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                    },
                    {
                        field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                        template: "#= kendo.toString(SoLuong,'n2') #",
                        footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                    },
                    {
                        field: "TenNhanVien", title: GetTextLanguage("nguoixuat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                    },

                ],
                dataBound: function (e) {
                    showslideimg10();
                    var rows = this.items();
                    var dem = 0;
                    $(rows).each(function () {
                        dem++;
                        var rowLabel = $(this).find(".stt");
                        $(rowLabel).html(dem);
                        if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                            dem = 0;
                        }
                    });
                },
            });
        }
    })
});

