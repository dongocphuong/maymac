﻿function getNoidungGroupLichSuNhap(value) {
    var datasource = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[i].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupLichSuNhap2(value) {
    var datasource = $("#gridtheophieu").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[i].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupLichSuXuat(value) {
    var datasource = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                result = group[i].NoiDung
            }
        }
    });
    return result;
};
$(document).ready(function () {
    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });

    var currentController = "/KhoNguyenLieu";

    var dataSourceKhoTongVatTu = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: currentController + "/GetDataKhoTong",
                    dataType: 'json',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                }
            },
            batch: true,
            pageSize: 20,
            sort: [{ field: "TenNguyenLieu", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "NguyenLieuID",
                    fields: {
                        VatTuID: { type: "string" },
                        TenVatTu: { type: "string" },
                        TenDonVi: { type: "string" },
                        TonHienTai: { type: "number" },
                        GhiChu: { type: "string" },
                        SoLuongNhap: { type: "number" },
                        SoLuongXuat: { type: "number" },
                        SoLuongTon: { type: "number" }
                    },
                }
            },
        });

    $("#gridQuanLyVatTuKhoTong").kendoGrid({
        dataSource: dataSourceKhoTongVatTu,
        height: window.innerHeight * 0.80,
        width: "100%",
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
             {
                 field: "AnhDaiDien", title: "Ảnh",
                 attributes: { style: "text-align:left;" },
                 filterable: false,
                 template: kendo.template($("#tplAnh").html()),
                 width: 70
             },
            {
                field: "TenNguyenLieu",
                title: "Tên nguyên liệu",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200
            },
            {
                field: "MaNguyenLieu",
                title: "Mã nguyên liệu",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 150
            },
            {
                field: "Type",
                title: "Loại Nguyên Liệu",
                filterable: {
                    cell: {
                        template: function (args) {
                            args.element.kendoDropDownList({
                                dataSource: [{ text: "Dệt", value: 1 }, { text: "May", value: 2 }, { text: "Phụ kiện", value: 3 }, ],
                                dataTextField: "text",
                                dataValueField: "value",
                                valuePrimitive: true
                            });
                        },
                        showOperators: false
                    }
                },
                template: kendo.template($("#tplLoaiNguyenLieu").html()),
                attributes: { style: "text-align:center;" },
                width: 150,
            },
            {
                field: "TenDonVi",
                title: GetTextLanguage("tendonvi"),
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 100
            },
            {
                field: "SoLuongNhap",
                title: "Tổng nhập",
                filterable: FilterInColumn,
                attributes: alignRight,
                format:"{0:n0}",
                width: 100
            },

            {
                field: "SoLuongXuat",
                title: "Tổng xuất",
                filterable: FilterInColumn,
                attributes: alignRight,
                format: "{0:n0}",
                width: 100
            },
            {
                field: "SoLuongTon",
                title: GetTextLanguage("tonhientai"),
                filterable: FilterInColumn,
                attributes: alignRight,
                format: "{0:n0}",
                width: 100
            },

        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    $("#gridQuanLyVatTuKhoTongIN").kendoGrid({
        dataSource: new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: currentController + "/GetDataKhoTong",
                    dataType: 'json',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                }
            },
            batch: true,
            sort: [{ field: "TenNguyenLieu", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "NguyenLieuID",
                    fields: {
                        VatTuID: { type: "string" },
                        TenVatTu: { type: "string" },
                        TenDonVi: { type: "string" },
                        TonHienTai: { type: "number" },
                        GhiChu: { type: "string" }
                    },
                }
            },
        }),
        height: window.innerHeight * 0.80,
        width: "100%",
        filterable:false,
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },

        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
             {
                 field: "AnhDaiDien", title: "Ảnh",
                 attributes: { style: "text-align:left;" },
                 filterable: FilterInTextColumn,
                 template: kendo.template($("#tplAnh2").html()),
                 width: 70
             },
            {
                field: "TenNguyenLieu",
                title: "Tên nguyên liệu",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200
            },
            {
                field: "MaNguyenLieu",
                title: "Mã nguyên liệu",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 150
            },
            {
                field: "Type",
                title: "Loại Nguyên Liệu",
                template: kendo.template($("#tplLoaiNguyenLieu").html()),
                attributes: { style: "text-align:center;" },
                width: 150,
            },
            {
                field: "TenDonVi",
                title: GetTextLanguage("tendonvi"),
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 100
            },
            {
                field: "SoLuongNhap",
                title: "Tổng nhập",
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongNhap"),
                width: 100
            },

            {
                field: "SoLuongNhap",
                title: "Tổng xuất",
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongXuat"),
                width: 100
            },
            {
                field: "SoLuongTon",
                title: GetTextLanguage("tonhientai"),
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongTon"),
                width: 100
            },

        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    $('#In').on('click', function () {
        $("#gridQuanLyVatTuKhoTongIN").data("kendoGrid").dataSource.filter($("#gridQuanLyVatTuKhoTong").data("kendoGrid").dataSource.filter());
        $("#tablein thead").html($("#gridQuanLyVatTuKhoTongIN .k-grid-header table thead").html());
        $("#tablein tbody").html($("#gridQuanLyVatTuKhoTongIN .k-grid-content table tbody").html());
        $("#tablein td").attr("style", "padding:5px");
        $("#tablein th").attr("style", "padding:5px");
        var divToPrint = $("#tablein").html();
        window.frames["print_frame"].document.body.innerHTML = divToPrint;
        window.frames["print_frame"].window.focus();
        window.frames["print_frame"].window.print();
        return false;
    })
    $("#btnNhapVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlNhapVatTuKhoTong", "95%", "90%", null, "Mua");
        $("#mdlNhapVatTuKhoTong").load(currentController + "/PopupNhapKhoTong");
    });

    $("#btnXuatVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlXuatVatTuKhoTong", "95%", "90%", null, "Xuất");
        $("#mdlXuatVatTuKhoTong").load(currentController + "/PopupXuatKhoTong");
    });

    $("#btnLichSuNhapVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlLichSuNhapVatTuKhoTong", "99%", "90%", "tplLichSuNhapVatTuKhoTong", "Lịch sử nhập");
        var dataLoaiHinhNhap = [
            { text: GetTextLanguage("tatca"), value: "0" },
            { text: GetTextLanguage("dieuchuyen"), value: "1" },
            { text: GetTextLanguage("mua"), value: "2" }
        ];
        $("#ipLoaiHinhNhap").kendoDropDownList({
            dataSource: dataLoaiHinhNhap,
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var value = this.value();
                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
            }
        })
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y, 0, 1), change: function () {
                var value = this.value();
                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay").val() });
            }
        });
        $("#inputDenNgay").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });
        //Tạo Grid LichSuNhapVatTuKhoTong
        var dataLichSuNhapVatTuKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetDataLichSuNhapKhoTong",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: { LoaiHinhNhap: 0, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            sort: [{ field: "TenNguyenLieu", dir: "asc" }],
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "ChiTietPhieuNhapNguyenLieuID",
                    fields: {
                        PhieuNhapNguyenLieuID: { type: "string" },
                        ThoiGian: { type: "date" }
                    }
                }
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
        });
        $("#gridLichSuNhapVatTuKhoTong").kendoGrid({
            dataSource: dataLichSuNhapVatTuKhoTong,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: innerHeight * 0.7,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"),
                    attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: formatToDateTime("ThoiGian"),
                    groupHeaderTemplate: "<button class='btn btn-info btnTraNguyenLieu'>Trả nguyên liệu</button>     [ #= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # - <span style='color:brown'>#=getNoidungGroupLichSuNhap(value)#</span> - <span style='color:brown'> Tổng giá trị phiếu: #= kendo.toString(aggregates.ThanhTien.sum,'n0') # VNĐ </span> ]     <button class='btn btn-danger btnXoaPhieu pull-right'>Xóa phiếu này</button>",
                },
                 {
                     field: "AnhDaiDien", title: "Ảnh",
                     attributes: { style: "text-align:left;" },
                     filterable: FilterInTextColumn,
                     template: kendo.template($("#tplAnh").html()),
                     width: 70
                 },
                 {
                     field: "MaPhieuNhap", title: GetTextLanguage("maphieu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                 },
                {
                    field: "TenNguyenLieu", title: "Tên nguyên liệu", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "MaNguyenLieu", title: "Mã nguyên liệu", attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 100
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 100
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n0') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n0')# </div>",
                },
                {
                    field: "GiaNhap", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(GiaNhap,'n0') #",
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                    attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(ThanhTien,'n0') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n0')# </div>",
                },
                {
                    field: "TenDoiTac", title: GetTextLanguage("doitac"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenNhanVien", title: "Nhân viên nhập", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                },
            ],
            dataBound: function (e) {
                showslideimg10();
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
                $(".btnTraNguyenLieu").click(function () {
                    var rowct = $(this).closest("tr"),
                         gridct = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid"),
                         dataItem = gridct.dataItem(rowct);
                    console.log(dataItem);
                    CreateModalWithSize("mdlTraNguyenLieu", "99%", "90%", "tpltranguyenlieu", "Trả nguyên liệu");
                    $("#ThoiGianTra").kendoDateTimePicker({
                        format: "dd/MM/yyyy HH:mm",
                        value: new Date(),
                    });
                    $("#gridDanhSachVatTuTra").kendoGrid({
                        dataSource: new kendo.data.DataSource({
                            transport: {
                                read: {
                                    url: currentController + "/GetDataChiTietPhieuNhap",
                                    dataType: "json",
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    data: { PhieuNhapVatTuID: dataItem.PhieuNhapNguyenLieuID }
                                },
                                parameterMap: function (options, type) {
                                    return JSON.stringify(options);
                                }
                            },
                            type: "json",
                            pageSize: 25,
                            sort: [{ field: "TenNguyenLieu", dir: "asc" }],
                            schema: {
                                data: "data",
                                total: "total",
                                model: {
                                    id: "ChiTietNhapXuatVatTuID",
                                    fields: {
                                        PhieuNhapVatTuID: { type: "string", editable: false },
                                        ThoiGian: { type: "date", editable: false },
                                        TenDonVi: { type: "string", editable: false },
                                        TenVatTu: { type: "string", editable: false },
                                        TenVietTat: { type: "string", editable: false },
                                        SoLuong: { type: "number", editable: true },
                                        DonGia: { type: "number", editable: false },
                                        ThanhTien: { type: "number", editable: false },
                                    }
                                }
                            },
                        }),
                        editable: true,
                        selectable: false,
                        sortable: true,
                        filterable: {
                            mode: "row"
                        },
                        batch: true,
                        height: 400,
                        dataBinding: function () {
                            record = 0;
                        },
                        columns:
                        [
                            {
                                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                                template: "#= ++record #",
                                width: 60,
                                attributes: alignCenter
                            },
                            { field: "VatTuID", hidden: true },
                            {
                                field: "DsHinhAnh", title: "Ảnh",
                                attributes: { style: "text-align:left;" },
                                filterable: FilterInTextColumn,
                                template: kendo.template($("#tplAnh").html()),
                                width: 70
                            },
                            {
                                field: "TenNguyenLieu", width: 200,
                                title: "Tên nguyên liệu",
                                attributes: { style: "text-align:left;" },
                                filterable: FilterInTextColumn
                            },
                            {
                                field: "MaNguyenLieu",
                                width: 200,
                                attributes: { style: "text-align:left;" },
                                title: "Mã nguyên liệu",
                                filterable: FilterInTextColumn
                            },
                            {
                                field: "TenDonVi", width: 200,
                                title: GetTextLanguage("donvi"),
                                attributes: { "style": "text-align:left !important;" },
                                filterable: FilterInTextColumn
                            },
                            {
                                field: "DonGia", title: GetTextLanguage("gianhap"),
                                format: "{0:n0}",
                                attributes: { style: "text-align:right;" },
                                filterable: FilterInColumn, width: 150,
                            },
                            {
                                field: "SoLuong", width: 100,
                                title: GetTextLanguage("soluong"),
                                format: "{0:n0}",
                                attributes: { class: "soluongcheck" },
                                filterable: FilterInColumn,
                                editor: function (container, options) {
                                    rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                                    var input = $('<input type="text" style="width:75%;height:15px;" class="form-control" id="' + options.model.get("VatTuID") + '" ' + rs + ' name="SoLuong" />');
                                    input.appendTo(container);
                                    input.autoNumeric(autoNumericOptionsSoLuong);
                                },
                            },
                            {
                                field: "ThanhTien",
                                title: GetTextLanguage("thanhtien"),
                                format: "{0:n0}",
                                attributes: { "style": "text-align:right !important;" },
                                filterable: FilterInColumn,
                                width: 150,
                            },
                            {
                                title: GetTextLanguage("thaotac"),
                                command: [
                                    {
                                        text: GetTextLanguage("xoa"),
                                        name: "destroy"
                                    }
                                ],
                                width: 100,
                            },
                        ],
                        dataBound: function (e) {
                            showslideimg10();
                        },
                        save: function (e) {
                            var items = $("#gridDanhSachVatTuTra").data("kendoGrid").dataSource.data();
                            if (e.values.DonGia != null) {
                                for (var i = 0; i < items.length; i++) {
                                    if (items[i].VatTuID == e.model.VatTuID) {
                                        items[i].ThanhTien = (e.values.DonGia == null ? 1 : e.values.DonGia) * (e.model.SoLuong == null ? 1 : e.model.SoLuong);
                                        items[i].DonGia = e.values.DonGia;
                                        break;
                                    }
                                }
                            } else if (e.values.SoLuong != null) {
                                for (var i = 0; i < items.length; i++) {
                                    if (items[i].VatTuID == e.model.VatTuID) {
                                        items[i].ThanhTien = (e.model.DonGia == null ? 1 : e.model.DonGia) * (e.values.SoLuong == null ? 1 : e.values.SoLuong);
                                        items[i].SoLuong = e.values.SoLuong;
                                        break;
                                    }
                                }
                            }

                            $("#gridDanhSachVatTuTra").data("kendoGrid").dataSource.data(items);
                        }
                    });
                    $("#btnLuuTraNguyenLieu").click(function () {
                        var dataChiTietNhap = $("#gridDanhSachVatTuTra").data("kendoGrid").dataSource.data();
                        var PhieuNhapVatTuID = dataItem.PhieuNhapNguyenLieuID;
                        var thoigianmua = $("#ThoiGianTra").val();
                        //var doitacID = $("#drdDoiTac").data("kendoDropDownList").value();
                        var hinhanh = "";
                        var DSHinhAnh = [];
                        var ins = document.getElementById('files_02').files.length;
                        if (ins > 0) {
                            var strUrl = uploadMultipleFileParagam2("/Upload" + "/Uploadfiles", "files_02", $(".dshinhanhadd2")).split(',');
                            for (var k = 0; k < strUrl.length; k++) {
                                DSHinhAnh.push({ Url: strUrl[k] });
                            }
                        }
                        for (var i = 0; i < DSHinhAnh.length; i++) {
                            if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                                hinhanh += DSHinhAnh[i].Url + ",";
                            }
                        }
                        $.ajax({
                            url: currentController + "/TraNguyenLieu",
                            type: "POST",
                            data: {
                                PhieuNhapVatTuID: PhieuNhapVatTuID,
                                ThoiGian: thoigianmua,
                                DoiTacID: dataItem.DoiTacID,
                                DataChiTietNhap: JSON.stringify(dataChiTietNhap),
                                DSHinhAnh: hinhanh.substring(0, hinhanh.length - 1),
                            },
                            complete: function (e) {
                                alertToastr(e.responseJSON);
                                if (e.responseJSON.code === "success") {
                                    $("#mdlTraNguyenLieu").data("kendoWindow").close();
                                    $("#gridQuanLyVatTuKhoTong").data("kendoGrid").dataSource.read();
                                    $("#gridQuanLyVatTuKhoDonVi").data("kendoGrid").dataSource.read();
                                }
                            }
                        });
                    });
                });
                $(".btnXoaPhieu").click(function () {
                    var rowct = $(this).closest("tr"),
                         gridct = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid"),
                         dataItem = gridct.dataItem(rowct);
                    var x = confirm("Bạn có chắc chắn muốn xóa mục này không?");
                    if (x) {
                        ContentWatingOP("mdlLichSuNhapVatTuKhoTong", 0.9);
                        $.ajax({
                            url: currentController + "/XoaPhieuNhapNguyenLieu",
                            method: "POST",
                            data: { PhieuNhapNguyenLieuID: dataItem.PhieuNhapNguyenLieuID },
                            success: function (data) {
                                showToast(data.code, data.code, data.message);
                                if (data.code == "success") {
                                    $("#mdlLichSuNhapVatTuKhoTong").unblock();
                                    $(".windows8").css("display", "none");
                                    gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: 0, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
                                    gridReload("gridQuanLyVatTuKhoTong", {});
                                }
                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                $("#mdlLichSuNhapVatTuKhoTong").unblock();
                                $(".windows8").css("display", "none");
                                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: 0, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
                                gridReload("gridQuanLyVatTuKhoTong", {});
                            }
                        });
                    }
                });
            },
        });
        $("#gridLichSuNhapVatTuKhoTong .k-grid-content").on("dblclick", "td", function () {
            var rowct = $(this).closest("tr"),
                   gridct = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid"),
                   dataItem = gridct.dataItem(rowct);
            if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {

                CreateModalWithSize("mdlChiTietPhieuNhap", "99%", "90%", null, "Chi Tiết Phiếu Nhập");
                $("#mdlChiTietPhieuNhap").load(currentController + "/PopupChiTietPhieuNhapVatTu?PhieuNhapVatTuID=" + dataItem.PhieuNhapNguyenLieuID);
            }
        })


        $("#mdlLichSuNhapVatTuKhoTong .xuat-excel").click(function () {
            var filer = [];
            var filers = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid").dataSource.filter();
            if (filers != null && filers != undefined) {
                for (var i = 0; i < filers.filters.length; i++) {
                    filers.filters[i].Operator = filers.filters[i].operator;
                    filer.push(filers.filters[i]);
                }
            }
            location.href = currentController + '/XuatExcelLichSuNhapVatTuKhoTong?filters=' + JSON.stringify(filer) + '&LoaiHinhNhap=0&TuNgay=' + $("#inputTuNgay").val() + '&DenNgay=' + $("#inputDenNgay").val();
        });

    });

    $("#btnLichSuXuatVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlLichSuXuatVatTuKhoTong", "99%", "90%", "tplLichSuXuatVatTuKhoTong", "Lịch sử xuất ");
        var dataLoaiHinhXuat = [
            { text: GetTextLanguage("tatca"), value: "0" },
            { text: "Xuất cho đơn hàng", value: "1" },
            { text: "Xuất trả lại", value: "2" }
        ];
        $("#ipLoaiHinhXuat").kendoDropDownList({
            dataSource: dataLoaiHinhXuat,
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var value = this.value();
                $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource.read({ LoaiHinhXuat: value, CongTrinhDenID: $("#ipCongTrinhDen").data("kendoDropDownList").value(), TuNgay: $("#inputTuNgay2").val(), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        createpopupdonhang($(".filter-hoadon"));
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay2").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                var value = this.value();
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhDenID: $("#ipCongTrinhDen").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        $("#inputDenNgay2").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhDenID: $("#ipCongTrinhDen").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });
        var dataSourceKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetAllHoaDon",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8"
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        })
        var iskhodonvi = $("#gridQuanLyVatTuKhoDonVi").length > 0;
        $("#ipCongTrinhDen").kendoDropDownList({
            dataSource: dataSourceKhoTong,
            filter: "contains",
            dataTextField: "TextHienThi",
            dataValueField: "DonHangID",
            optionLabel: "Chọn đơn hàng",
            headerTemplate: kendo.template($("#HoaDonTemplateHeader").html()),
            template: kendo.template($("#HoaDonTemplate").html()),
            valueTemplate: kendo.template($("#HoaDonValueTemplateHeader").html()),
            change: function () {
                var value = this.value();
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhDenID: $("#ipCongTrinhDen").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        //Tạo Grid LichSuXuatVatTuKhoTong
        var dataLichSuXuatVatTuKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetLichSuXuatKhoTong",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        LoaiHinhXuat: 0,
                        CongTrinhDenID: $("#ipCongTrinhDen").val(),
                        TuNgay: $("#inputTuNgay2").val(),
                        DenNgay: $("#inputDenNgay2").val()
                    }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total",
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            sort: [{ field: "TenNguyenLieu", dir: "asc" }],
            aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
        });
        $("#gridLichSuXuatVatTuKhoTong").kendoGrid({
            dataSource: dataLichSuXuatVatTuKhoTong,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: window.innerHeight * 0.9 - 100,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuXuat(value)#</span> ----------- <button class='btn btn-danger btnXoaPhieuXuat pull-right'>Xóa phiếu này</button>",
                },
                 {
                     field: "MaPhieuXuat", title: "Mã phiếu", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                 },
                  {
                      field: "AnhDaiDien", title: "Ảnh",
                      attributes: { style: "text-align:left;" },
                      filterable: FilterInTextColumn,
                      template: kendo.template($("#tplAnh").html()),
                      width: 70
                  },
                  {
                      field: "TenNguyenLieu", title: "Tên nguyên liệu", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                  },
                {
                    field: "MaNguyenLieu", title: "Mã nguyên liệu", attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 100
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n0') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n0') # </div>"
                },
                {
                    field: "TenNhanVien", title: GetTextLanguage("nguoixuat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },

            ],
            dataBound: function (e) {
                showslideimg10();
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
                $(".btnXoaPhieuXuat").click(function () {
                    var rowct = $(this).closest("tr"),
                         gridct = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid"),
                         dataItem = gridct.dataItem(rowct);
                    var x = confirm("Bạn có chắc chắn muốn xóa mục này không?");
                    if (x) {
                        ContentWatingOP("mdlLichSuXuatVatTuKhoTong", 0.9);
                        $.ajax({
                            url: currentController + "/XoaPhieuXuatNguyenLieu",
                            method: "POST",
                            data: { PhieuXuatNguyenLieuID: dataItem.PhieuXuatNguyenLieuID },
                            success: function (data) {
                                showToast(data.code, data.code, data.message);
                                if (data.code == "success") {
                                    $("#mdlLichSuXuatVatTuKhoTong").unblock();
                                    $(".windows8").css("display", "none");
                                    gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhNhap: 0, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
                                    gridReload("gridQuanLyVatTuKhoTong", {});
                                }
                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                $("#mdlLichSuXuatVatTuKhoTong").unblock();
                                $(".windows8").css("display", "none");
                                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhNhap: 0, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
                                gridReload("gridQuanLyVatTuKhoTong", {});
                            }
                        });
                    }
                });
            },
        });
        $("#gridLichSuXuatVatTuKhoTong .k-grid-content").on("dblclick", "td", function () {
            var rowct = $(this).closest("tr"),
                   gridct = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid"),
                   dataItem = gridct.dataItem(rowct);
            if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                CreateModalWithSize("mdlChiTietPhieuXuat", "99%", "90%", null, GetTextLanguage("chitietphieuxuat"));
                $("#mdlChiTietPhieuXuat").load(currentController + "/PopupXuatKhoTong?PhieuXuatVatTuID=" + dataItem.PhieuXuatNguyenLieuID);
            }
        })

        $("#mdlLichSuXuatVatTuKhoTong .xuat-excel").click(function () {
            var filer = [];
            var filers = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource.filter();
            if (filers != null && filers != undefined) {
                for (var i = 0; i < filers.filters.length; i++) {
                    filers.filters[i].Operator = filers.filters[i].operator;
                    filer.push(filers.filters[i]);
                }
            }
            location.href = currentController + '/XuatExcelLichSuXuatVatTuKhoTong?filters=' + JSON.stringify(filer) + '&LoaiHinhXuat=' + $("#ipLoaiHinhXuat").val() + '&TuNgay=' + $("#inputTuNgay2").val() + '&DenNgay=' + $("#inputDenNgay2").val() + '&CongTrinhDenID=' + $("#ipCongTrinhDen").val();
        });
    });

});

