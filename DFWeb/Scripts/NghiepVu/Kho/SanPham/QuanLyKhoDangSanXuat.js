﻿function getNoidungGroupLichSuNhap(value) {
    var datasource = $("#gridLichSuNhap").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[i].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupLichSuXuat(value) {
    var datasource = $("#gridLichSuXuat").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                result = "Giao hàng";
            }
        }
    });
    return result;
};
$(document).ready(function () {
    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });
    var currentTime = new Date();
    var currentController = "/KhoSanPhamDangSanXuat";

    var dataSourceKhoTongVatTu = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: currentController + "/GetDataTonKho",
                    dataType: 'json',
                    type: 'GET',
                    contentType: "application/json; charset=utf-8",
                    data: { DonHangID: "" }
                }
            },
            batch: true,
            pageSize: 20,
            sort: [{ field: "TenSanPham", dir: "asc" }, { field: "Size", dir: "asc" }],
            aggregate: [{ field: "SoLuongNhap", aggregate: "sum" }, { field: "SoLuongXuat", aggregate: "sum" }, { field: "SoLuongTon", aggregate: "sum" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "SanPhamID",
                    fields: {
                        VatTuID: { type: "string" },
                        TenVatTu: { type: "string" },
                        TenDonVi: { type: "string" },
                        TonHienTai: { type: "number" },
                        GhiChu: { type: "string" }
                    },
                }
            },
            group: {
                field: "MaDonHang",
                dir: "asc"
            },
        });

    $("#gridKhoSanPhamDangSanXuat").kendoGrid({
        dataSource: dataSourceKhoTongVatTu,
        height: window.innerHeight * 0.80,
        width: "100%",
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        selectable: "multiple, row",
        change: function (e) {
            var selectedRows = this.select()[0];
            grid = $("#gridKhoSanPhamDangSanXuat").data("kendoGrid"),
             dataItem = grid.dataItem(selectedRows);
            $("#HoaDonID").data("kendoDropDownList").value(dataItem.DonHangID);
        },
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "MaDonHang",
                title: "Mã đơn hàng",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200,
                hidden: true
            },
            {
                field: "AnhDaiDien", title: "Ảnh",
                attributes: { style: "text-align:left;" },
                filterable: FilterInTextColumn,
                template: kendo.template($("#tplAnh").html()),
                width: 70
            },
            {
                field: "TenSanPham",
                title: "Tên Sản Phẩm",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200
            },
            {
                field: "MaSanPham",
                title: "Mã Sản Phẩm",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 150
            },
            {
                field: "Mau",
                title: "Màu",
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 80
            },
            {
                field: "Size",
                title: "Size",
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 60
            },
            {
                field: "TenDonVi",
                title: GetTextLanguage("tendonvi"),
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 100
            },
            {
                field: "TinhTrang",
                title: "Tình Trạng",
                filterable: FilterInColumn,
                attributes: alignCenter,
                template: kendo.template($("#tplTinhTrang").html()),
                width: 100
            },
            {
                field: "LoaiHinh",
                title: "Loại hình",
                filterable: FilterInColumn,
                attributes: alignCenter,
                template: kendo.template($("#tplLoaiHinh").html()),
                width: 100
            },
            {
                field: "SoLuongNhap",
                title: "Số lượng nhập và sản xuất",
                filterable: FilterInColumn,
                attributes: alignRight,
                format: "{0:n0}",
                width: 100,
                footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n0')#</div>",
            },
            {
                field: "SoLuongXuat",
                title: "Số lượng xuất",
                filterable: FilterInColumn,
                attributes: alignRight,
                format: "{0:n0}",
                width: 100,
                footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n0')#</div>",
            },
            {
                field: "SoLuongTon",
                title: GetTextLanguage("tonhientai"),
                filterable: FilterInColumn,
                attributes: alignRight,
                format: "{0:n0}",
                width: 100,
                footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n0')#</div>",
            },

        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    var dataSourceKhoTong = new kendo.data.DataSource({
        transport: {
            read: {
                url: currentController + "/GetAllHoaDon",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8"
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    })
    $("#XuatExcel").click(function () {
        var url = "/KhoSanPhamDangSanXuat/ExportToExcel?DonHangID=" + $("#HoaDonID").val();
        location.href = url;
    });
    $("#HoaDonID").kendoDropDownList({
        dataSource: dataSourceKhoTong,
        filter: "contains",
        dataTextField: "TextHienThi",
        dataValueField: "DonHangID",
        optionLabel: "Tất cả",
        headerTemplate: kendo.template($("#HoaDonTemplateHeader").html()),
        template: kendo.template($("#HoaDonTemplate").html()),
        valueTemplate: kendo.template($("#HoaDonValueTemplateHeader").html()),
        change: function () {
            var value = this.value();
            $("#gridKhoSanPhamDangSanXuat").data("kendoGrid").dataSource.read({ DonHangID: value });
            $("#HoaDonIDDonHangDaChon").val(value);
        }
    });
    createpopupdonhang($(".filter-hoadon"));
    //LS nhập xuất
    $("#btnLichSuNhap").click(function () {
        if ($("#HoaDonID").val() == null || $("#HoaDonID").val() == "") {
            showToast("warning", GetTextLanguage("canhbao"), "Bạn phải chọn đơn hàng trước");
        }
        else {
            CreateModalWithSize("mdlLichSuNhap", "95%", "90%", "tplLichSuNhap", "Lịch sử nhập");
            var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
            $("#inputTuNgay").kendoDatePicker({
                format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                    var value = this.value();
                    gridReload("gridLichSuNhap", { DonHangID: $("#HoaDonID").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay").val() });
                }
            });
            $("#inputDenNgay").kendoDatePicker({
                format: "dd/MM/yyyy", value: new Date(), change: function () {
                    var value = this.value();
                    gridReload("gridLichSuNhap", { DonHangID: $("#HoaDonID").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
                }
            });
            var dataLichSuNhapVatTuKhoTong = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/GetDataLichSuNhap",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: { DonHangID: $("#HoaDonID").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() }
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                pageSize: 25,
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        id: "ChiTietPhieuNhapNguyenLieuID",
                        fields: {
                            PhieuNhapNguyenLieuID: { type: "string" },
                            ThoiGian: { type: "date" }
                        }
                    }
                },
                group: {
                    field: "ThoiGian",
                    dir: "desc",
                    aggregates: [{
                        field: "ThanhTien",
                        aggregate: "sum"
                    }]
                },
                sort: [{ field: "TenSanPham", dir: "asc" }, { field: "Size", dir: "asc" }],
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
            });
            $("#gridLichSuNhap").kendoGrid({
                dataSource: dataLichSuNhapVatTuKhoTong,
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: pageableAll,
                height: innerHeight * 0.7,
                width: "100%",
                columns: [
                    {
                        title: GetTextLanguage("stt"),
                        template: "<span class='stt'></span>",
                        width: 60,
                        align: "center"
                    },
                    {
                        field: "ThoiGian", title: GetTextLanguage("thoigian"),
                        attributes: alignCenter, filterable: FilterInColumn, width: 150,
                        hidden: true,
                        template: formatToDateTime("ThoiGian"),
                        groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuNhap(value)#</span>",
                    },
                     {
                         field: "AnhDaiDien", title: "Ảnh",
                         attributes: { style: "text-align:left;" },
                         filterable: FilterInTextColumn,
                         template: kendo.template($("#tplAnh").html()),
                         width: 70
                     },
                     {
                         field: "MaPhieuNhap", title: GetTextLanguage("maphieu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                     },
                    {
                        field: "TenSanPham", title: "Tên sản phẩm", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                    },
                    {
                        field: "MaSanPham", title: "Mã sản phẩm", attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 100
                    },
                    {
                        field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 100
                    },
                    {
                        field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                        template: "#= kendo.toString(SoLuong,'n2') #",
                        footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                    },
                    {
                        field: "GiaNhap", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                        template: "#= kendo.toString(GiaNhap,'n2') #",
                    },
                    {
                        field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                        attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                        template: "#= kendo.toString(ThanhTien,'n2') #",
                        footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                    },
                    {
                        field: "TenDoiTac", title: GetTextLanguage("doitac"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                    },
                    {
                        field: "TenNhanVien", title: "Nhân viên nhập", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                    },
                ],
                dataBound: function (e) {
                    showslideimg10();
                    var rows = this.items();
                    var dem = 0;
                    $(rows).each(function () {
                        dem++;
                        var rowLabel = $(this).find(".stt");
                        $(rowLabel).html(dem);
                        if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                            dem = 0;
                        }
                    });
                },
            });
        }
    });
    $("#btnLichSuXuat").click(function () {
        if ($("#HoaDonID").val() == null || $("#HoaDonID").val() == "") {
            showToast("warning", GetTextLanguage("canhbao"), "Bạn phải chọn đơn hàng trước");
        }
        else {
            CreateModalWithSize("mdlLichSuXuat", "95%", "90%", "tplLichSuXuat", "Lịch sử xuất");
            var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
            $("#inputTuNgay2").kendoDatePicker({
                format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                    var value = this.value();
                    gridReload("gridLichSuXuat", { DonHangID: $("#HoaDonID").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay2").val() });
                }
            });
            $("#inputDenNgay2").kendoDatePicker({
                format: "dd/MM/yyyy", value: new Date(), change: function () {
                    var value = this.value();
                    gridReload("gridLichSuXuat", { DonHangID: $("#HoaDonID").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
                }
            });
            var dataLichSuXuatVatTuKhoTong = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/GetDataLichSuXuat",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            DonHangID: $("#HoaDonID").val(),
                            TuNgay: $("#inputTuNgay2").val(),
                            DenNgay: $("#inputDenNgay2").val()
                        }
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                pageSize: 25,
                schema: {
                    data: "data",
                    total: "total",
                },
                group: {
                    field: "ThoiGian",
                    dir: "desc",
                    aggregates: [{
                        field: "ThanhTien",
                        aggregate: "sum"
                    }]
                },
                sort: [{ field: "TenSanPham", dir: "asc" }, { field: "Size", dir: "asc" }],
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
            });
            $("#gridLichSuXuat").kendoGrid({
                dataSource: dataLichSuXuatVatTuKhoTong,
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: pageableAll,
                height: window.innerHeight * 0.9 - 100,
                width: "100%",
                columns: [
                    {
                        title: GetTextLanguage("stt"),
                        template: "<span class='stt'></span>",
                        width: 60,
                        align: "center"
                    },
                    {
                        field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                        hidden: true,
                        template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                        groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuXuat(value)#</span>",
                    },
                     {
                         field: "MaPhieu", title: "Mã phiếu", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                     },
                      {
                          field: "AnhDaiDien", title: "Ảnh",
                          attributes: { style: "text-align:left;" },
                          filterable: FilterInTextColumn,
                          template: kendo.template($("#tplAnh").html()),
                          width: 70
                      },
                      {
                          field: "TenSanPham", title: "Tên sản phẩm", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                      },
                    {
                        field: "MaSanPham", title: "Mã sản phẩm", attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 100
                    },
                    {
                        field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                    },
                    {
                        field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                        template: "#= kendo.toString(SoLuong,'n2') #",
                        footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                    },
                    {
                        field: "TenNhanVien", title: GetTextLanguage("nguoixuat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                    },

                ],
                dataBound: function (e) {
                    showslideimg10();
                    var rows = this.items();
                    var dem = 0;
                    $(rows).each(function () {
                        dem++;
                        var rowLabel = $(this).find(".stt");
                        $(rowLabel).html(dem);
                        if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                            dem = 0;
                        }
                    });
                },
            });
            $("#gridLichSuXuat .k-grid-content").on("dblclick", "td", function () {
                var rowct = $(this).closest("tr"),
                       gridct = $("#gridLichSuXuat").data("kendoGrid"),
                       dataItem = gridct.dataItem(rowct);
                if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                    CreateModalWithSize("mdlGiaoHang", "95%", "90%", "tplGiaoHang", "Giao sản phẩm");
                    $("#HoaDonIDDonHangDaChon").kendoDropDownList({
                        dataSource: dataSourceKhoTong,
                        filter: "contains",
                        dataTextField: "TextHienThi",
                        dataValueField: "DonHangID",
                        optionLabel: "Tất cả",
                        headerTemplate: kendo.template($("#HoaDonTemplateHeader").html()),
                        template: kendo.template($("#HoaDonTemplate").html()),
                        valueTemplate: kendo.template($("#HoaDonValueTemplateHeader").html()),
                    });
                    $("#HoaDonIDDonHangDaChon").data("kendoDropDownList").value($("#HoaDonID").val());
                    createpopupdonhang($(".filter-hoadon2"));
                    $("#gridGiaoHang").kendoGrid({
                        dataSource: new kendo.data.DataSource(
                        {
                            transport: {
                                read: {
                                    url: currentController + "/LayChiTietLichSuXuat",
                                    dataType: 'json',
                                    type: 'GET',
                                    contentType: "application/json; charset=utf-8",
                                    data: { PhieuNhanDonHangID: dataItem.PhieuNhanDonHangID }
                                }
                            },
                            batch: true,
                            sort: [{ field: "TenSanPham", dir: "asc" }, { field: "Size", dir: "asc" }],
                            aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }],
                            pageSize: 20,
                            schema: {
                                data: 'data',
                                total: 'total',
                                model: {
                                    id: "SanPhamID",
                                    fields: {
                                        AnhDaiDien: { type: "string", editable: false },
                                        TenSanPham: { type: "string", editable: false },
                                        MaSanPham: { type: "string", editable: false },
                                        TonHienTai: { type: "number", editable: false },
                                        SoLuong: { type: "number", editable: true },
                                        TenDonVi: { type: "string", editable: false },
                                        TinhTrang: { type: "string", editable: false },
                                        LoaiHinh: { type: "number", editable: false },
                                        ThanhTien: { type: "number", editable: false },
                                        DonGia: { type: "number", editable: true },
                                    },
                                }
                            },

                        }),
                        height: window.innerHeight * 0.80,
                        width: "100%",
                        filterable: {
                            mode: "row"
                        },
                        sortable: true,
                        resizable: true,
                        editable: true,
                        dataBinding: function () {
                            record = 0;
                        },
                        pageable: pageableAll,
                        columns: [
                            {
                                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                                template: "#= ++record #",
                                width: 60,
                                attributes: alignCenter
                            },
                             {
                                 field: "AnhDaiDien", title: "Ảnh",
                                 attributes: { style: "text-align:left;" },
                                 filterable: FilterInTextColumn,
                                 template: kendo.template($("#tplAnh").html()),
                                 width: 70
                             },
                            {
                                field: "TenSanPham",
                                title: "Tên Sản Phẩm",
                                filterable: FilterInTextColumn,
                                attributes: alignLeft,
                                width: 200
                            },
                            {
                                field: "MaSanPham",
                                title: "Mã Sản Phẩm",
                                filterable: FilterInTextColumn,
                                attributes: alignLeft,
                                width: 150
                            },
                            {
                                field: "TenDonVi",
                                title: GetTextLanguage("tendonvi"),
                                filterable: FilterInTextColumn,
                                attributes: alignCenter,
                                width: 100
                            },
                            {
                                field: "TinhTrang",
                                title: "Tình Trạng",
                                filterable: FilterInColumn,
                                attributes: alignCenter,
                                template: kendo.template($("#tplTinhTrang").html()),
                                width: 100
                            },
                             {
                                 field: "LoaiHinh",
                                 title: "Loại hình",
                                 filterable: FilterInColumn,
                                 attributes: alignCenter,
                                 template: kendo.template($("#tplLoaiHinh").html()),
                                 width: 100
                             },

                            //{
                            //    field: "SoLuongTon",
                            //    title: GetTextLanguage("tonhientai"),
                            //    filterable: FilterInColumn,
                            //    attributes: alignRight,
                            //    template: formatToDouble("SoLuongTon"),
                            //    width: 100
                            //},
                            {
                                field: "SoLuong",
                                title: "Số lượng giao",
                                filterable: FilterInColumn,
                                attributes: { class: "soluongcheck" },
                                format: "{0:n2}",
                                width: 100,
                                editor: function (container, options) {
                                    rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                                    var input = $('<input type="text" style="width:75%;height:15px;" data-v-min="1" class="form-control" id="' + options.model.get("SanPhamID") + '"  ' + rs + ' name="SoLuong" />');
                                    input.appendTo(container);
                                    input.autoNumeric(autoNumericOptionsSoLuong);
                                },
                            },
                            {
                                field: "DonGia",
                                title: "Đơn giá",
                                filterable: FilterInColumn,
                                attributes: { class: "soluongcheck" },
                                format: "{0:n2}",
                                width: 100,
                                editor: function (container, options) {
                                    rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                                    var input = $('<input type="text" data-v-min="1" class="form-control" id="' + options.model.get("SanPhamID") + '"  ' + rs + ' name="DonGia" />');
                                    input.appendTo(container);
                                    input.autoNumeric(autoNumericOptionsSoLuong);
                                },
                            },
                            {
                                field: "ThanhTien",
                                title: GetTextLanguage("thanhtien"),
                                format: "{0:n2}",
                                attributes: alignRight,
                                filterable: FilterInColumn,
                                width: 150,
                                footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n0')#</div>",
                                editor: function (container, options) {
                                    container.text(kendo.toString(options.model.ThanhTien, "n2"));
                                },
                            },

                        ],
                        save: function (e) {
                            var items = $("#gridGiaoHang").data("kendoGrid").dataSource.data();
                            if (e.values.DonGia != null) {
                                for (var i = 0; i < items.length; i++) {
                                    if (items[i].SanPhamID == e.model.SanPhamID) {
                                        items[i].ThanhTien = (e.values.DonGia == null ? 1 : e.values.DonGia) * (e.model.SoLuong == null ? 1 : e.model.SoLuong);
                                        items[i].DonGia = e.values.DonGia;
                                        break;
                                    }
                                }
                            } else if (e.values.SoLuong != null) {
                                for (var i = 0; i < items.length; i++) {
                                    if (items[i].SanPhamID == e.model.SanPhamID) {
                                        items[i].ThanhTien = (e.model.DonGia == null ? 1 : e.model.DonGia) * (e.values.SoLuong == null ? 1 : e.values.SoLuong);
                                        items[i].SoLuong = e.values.SoLuong;
                                        break;
                                    }
                                }
                            }
                            $("#gridGiaoHang").data("kendoGrid").dataSource.data(items);
                        },
                        dataBound: function (e) {
                            showslideimg10();
                        },
                    });
                    $("#btn-saveGiaoHang").click(function () {
                        ContentWatingOP("mdlGiaoHang", 0.9);
                        var lst = [];
                        var datas = $("#gridGiaoHang").data("kendoGrid").dataSource.data();
                        for (var i = 0; i < datas.length; i++) {
                            if (datas[i].SoLuong > 0 && datas[i].DonGia > 0) {
                                lst.push(datas[i]);
                            }
                        }
                        $.ajax({
                            url: currentController + "/CapNhatGiaoHang",
                            type: "POST",
                            data: {
                                PhieuNhanDonHangID: dataItem.PhieuNhanDonHangID,
                                lstSanPham: JSON.stringify(lst),
                            },
                            success: function (data) {
                                alertToastr(data);
                                setTimeout(function () {
                                    if (data.code === "success") {
                                        $("#mdlGiaoHang").data("kendoWindow").close();
                                        $("#gridKhoSanPhamDangSanXuat").data("kendoGrid").dataSource.read({ DonHangID: $("#HoaDonID").val() });
                                        $("#gridLichSuXuat").data("kendoGrid").dataSource.read({
                                            DonHangID: $("#HoaDonID").val(), TuNgay: $("#inputTuNgay2").val(),
                                            DenNgay: $("#inputDenNgay2").val()
                                        });
                                    }
                                    $("#mdlGiaoHang").unblock();
                                }, 1000)

                            }
                        });

                    });
                    $("#btn-xuathoadon").attr("href", '/KhoSanPhamDangSanXuat/InHoaDon?PhieuNhanDonHangID=' + dataItem.PhieuNhanDonHangID)
                }
            })
        }
    });
    $("#btnGiaoHang").click(function () {
        if ($("#HoaDonID").val() == null || $("#HoaDonID").val() == "") {
            showToast("warning", GetTextLanguage("canhbao"), "Bạn phải chọn đơn hàng trước");
        }
        else {
            CreateModalWithSize("mdlGiaoHang", "95%", "90%", "tplGiaoHang", "Giao sản phẩm");
            $("#HoaDonIDDonHangDaChon").kendoDropDownList({
                dataSource: dataSourceKhoTong,
                filter: "contains",
                dataTextField: "TextHienThi",
                dataValueField: "DonHangID",
                optionLabel: "Tất cả",
                headerTemplate: kendo.template($("#HoaDonTemplateHeader").html()),
                template: kendo.template($("#HoaDonTemplate").html()),
                valueTemplate: kendo.template($("#HoaDonValueTemplateHeader").html()),
            });
            $("#HoaDonIDDonHangDaChon").data("kendoDropDownList").value($("#HoaDonID").val());
            createpopupdonhang($(".filter-hoadon2"));
            $("#btn-xuathoadon").addClass("hidden");
            $("#gridGiaoHang").kendoGrid({
                dataSource: new kendo.data.DataSource(
                {
                    transport: {
                        read: {
                            url: currentController + "/GetDataTonKho",
                            dataType: 'json',
                            type: 'GET',
                            contentType: "application/json; charset=utf-8",
                            data: { DonHangID: $("#HoaDonID").val() }
                        }
                    },
                    batch: true,
                    pageSize: 20,
                    sort: [{ field: "TenSanPham", dir: "asc" }, { field: "Size", dir: "asc" }],
                    aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuongTon", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }],
                    schema: {
                        data: 'data',
                        total: 'total',
                        model: {
                            id: "SanPhamID",
                            fields: {
                                AnhDaiDien: { type: "string", editable: false },
                                TenSanPham: { type: "string", editable: false },
                                MaSanPham: { type: "string", editable: false },
                                TonHienTai: { type: "number", editable: false },
                                SoLuong: { type: "number", editable: true },
                                TenDonVi: { type: "string", editable: false },
                                TinhTrang: { type: "string", editable: false },
                                LoaiHinh: { type: "number", editable: false },
                                DonGia: { type: "number", editable: true },
                                Mau: { type: "string", editable: false },
                                Size: { type: "string", editable: false },

                            },
                        }
                    },

                }),
                height: window.innerHeight * 0.80,
                width: "100%",
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                editable: true,
                dataBinding: function () {
                    record = 0;
                },
                pageable: pageableAll,
                columns: [
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                        template: "#= ++record #",
                        width: 60,
                        attributes: alignCenter
                    },
                     {
                         field: "AnhDaiDien", title: "Ảnh",
                         attributes: { style: "text-align:left;" },
                         filterable: FilterInTextColumn,
                         template: kendo.template($("#tplAnh").html()),
                         width: 70
                     },
                    {
                        field: "TenSanPham",
                        title: "Tên Sản Phẩm",
                        filterable: FilterInTextColumn,
                        attributes: alignLeft,
                        width: 200
                    },
                    {
                        field: "MaSanPham",
                        title: "Mã Sản Phẩm",
                        filterable: FilterInTextColumn,
                        attributes: alignCenter,
                        width: 50
                    },
                    {
                        field: "TenDonVi",
                        title: GetTextLanguage("tendonvi"),
                        filterable: FilterInTextColumn,
                        attributes: alignCenter,
                        width: 50
                    },
                    {
                        field: "TinhTrang",
                        title: "Tình Trạng",
                        filterable: FilterInColumn,
                        attributes: alignCenter,
                        template: kendo.template($("#tplTinhTrang").html()),
                        width: 100
                    },
                    {
                        field: "LoaiHinh",
                        title: "Loại hình",
                        filterable: FilterInColumn,
                        attributes: alignCenter,
                        template: kendo.template($("#tplLoaiHinh").html()),
                        width: 100
                    },
                    {
                        field: "Mau",
                        title: "Màu",
                        filterable: FilterInTextColumn,
                        attributes: alignCenter,
                        width: 100
                    },
                    {
                        field: "Size",
                        title: "Size",
                        filterable: FilterInTextColumn,
                        attributes: alignCenter,
                        width: 50
                    },
                    {
                        field: "SoLuongTon",
                        title: GetTextLanguage("tonhientai"),
                        filterable: FilterInColumn,
                        attributes: alignRight,
                        format: "{0:n0}",
                        width: 100,
                        footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n0')#</div>",
                    },
                    {
                        field: "SoLuong",
                        title: "Số lượng giao",
                        filterable: FilterInColumn,
                        attributes: { class: "soluongcheck" },
                        format: "{0:n0}",
                        width: 100,
                        footerTemplate: "<div style='color:red;text-align:right;'>#= kendo.toString(sum, 'n0') == 'undefined' || kendo.toString(sum, 'n0') == null ? '--' : kendo.toString(sum, 'n0') #</div>",
                        editor: function (container, options) {
                            rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                            var input = $('<input type="text" style="width:75%;height:15px;" class="form-control" id="' + options.model.get("SanPhamID") + '"  ' + rs + ' name="SoLuong"/>');
                            input.appendTo(container);
                            input.autoNumeric({
                                digitGroupSeparator: ',',
                                decimalCharacter: '.',
                                minimumValue: '0',
                                maximumValue: options.model.SoLuongTon,
                                emptyInputBehavior: 'zero'
                            });
                        },
                    },
                    {
                        field: "DonGia",
                        title: "Đơn giá",
                        filterable: FilterInColumn,
                        attributes: alignRight,
                        format: "{0:n0}",
                        width: 100,
                        editor: function (container, options) {
                            rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                            var input = $('<input type="text" style="width:75%;height:15px;" class="form-control" id="' + options.model.get("SanPhamID") + '"  ' + rs + ' name="DonGia" />');
                            input.appendTo(container);
                            input.autoNumeric({
                                digitGroupSeparator: ',',
                                decimalCharacter: '.',
                                minimumValue: '0',
                                emptyInputBehavior: 'zero'
                            });
                        },
                    },
                    {
                        field: "ThanhTien",
                        title: GetTextLanguage("thanhtien"),
                        format: "{0:n0}",
                        attributes: alignRight,
                        filterable: FilterInColumn,
                        width: 150,
                        footerTemplate: "<div style='color:red;text-align:right;'>#= kendo.toString(sum, 'n0') #</div>",
                        editor: function (container, options) {
                            container.text(kendo.toString(options.model.ThanhTien, "n2"));
                        },
                    },
                ],
                save: function (e) {
                    var items = $("#gridGiaoHang").data("kendoGrid").dataSource.data();
                    if (e.values.DonGia != null) {
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].SanPhamID == e.model.SanPhamID) {
                                items[i].ThanhTien = (e.values.DonGia == null ? 1 : e.values.DonGia) * (e.model.SoLuong == null ? 1 : e.model.SoLuong);
                                items[i].DonGia = e.values.DonGia;
                                break;
                            }
                        }
                    } else if (e.values.SoLuong != null) {
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].SanPhamID == e.model.SanPhamID) {
                                items[i].ThanhTien = (e.model.DonGia == null ? 1 : e.model.DonGia) * (e.values.SoLuong == null ? 1 : e.values.SoLuong);
                                items[i].SoLuong = e.values.SoLuong;
                                break;
                            }
                        }
                    }
                    $("#gridGiaoHang").data("kendoGrid").dataSource.data(items);
                },
                dataBound: function (e) {
                    showslideimg10();
                },
            });
            $("#btn-saveGiaoHang").click(function () {
                ContentWatingOP("mdlGiaoHang", 0.9);
                var lst = [];
                var datas = $("#gridGiaoHang").data("kendoGrid").dataSource.data();
                for (var i = 0; i < datas.length; i++) {
                    if (datas[i].SoLuong > 0 && datas[i].DonGia > 0) {
                        lst.push(datas[i]);
                    }
                }
                $.ajax({
                    url: currentController + "/GiaoHang",
                    type: "POST",
                    data: {
                        DonHangID: $("#HoaDonIDDonHangDaChon").val(),
                        DonHangXuatID: $("#HoaDonID").val(),
                        lstSanPham: JSON.stringify(lst),
                    },
                    success: function (data) {
                        alertToastr(data);
                        if (data.code === "success") {
                            $.get('/KhoSanPhamDangSanXuat/InHoaDon?PhieuNhanDonHangID=' + data.kq, function (data) {
                                window.frames["print_frame"].document.body.innerHTML = data;
                                window.frames["print_frame"].window.focus();
                                window.frames["print_frame"].window.print();
                            });

                            //$("#btn-xuathoadon").click();
                            //var form = document.createElement("form");
                            //var x = document.createElement("INPUT");
                            //x.setAttribute("type", "text");
                            //x.setAttribute("name", "PhieuNhanDonHangID");
                            //x.setAttribute("value", data.kq);
                            //form.appendChild(x);
                            //form.method = "GET";
                            //form.action = '/KhoSanPhamDangSanXuat/InHoaDon';
                            //form.target = "_blank";
                            //document.body.appendChild(form);
                            //form.submit();
                        }
                        setTimeout(function () {
                            if (data.code === "success") {
                                $("#mdlGiaoHang").data("kendoWindow").close();
                                $("#gridKhoSanPhamDangSanXuat").data("kendoGrid").dataSource.read({ DonHangID: $("#HoaDonID").val() });

                            }
                            $("#mdlGiaoHang").unblock();
                        }, 1000)

                    }
                });

            });
        }
    })
});

