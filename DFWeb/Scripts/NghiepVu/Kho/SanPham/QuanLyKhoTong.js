﻿function getNoidungGroupLichSuNhap(value) {
    var datasource = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[i].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupLichSuNhap2(value) {
    var datasource = $("#gridtheophieu").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[i].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupLichSuXuat(value) {
    var datasource = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                result = group[i].NoiDung
            }
        }
    });
    return result;
};
$(document).ready(function () {
    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });

    var currentController = "/KhoSanPham";

    var dataSourceKhoTongVatTu = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: currentController + "/GetDataKhoTong",
                    dataType: 'json',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                }
            },
            batch: true,
            pageSize: 20,
            sort: [{ field: "TenSanPham", dir: "asc" }, { field: "Size", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "SanPhamID",
                    fields: {
                        VatTuID: { type: "string" },
                        TenVatTu: { type: "string" },
                        TenDonVi: { type: "string" },
                        TonHienTai: { type: "number" },
                        GhiChu: { type: "string" },
                        SoLuongNhap: { type: "number" },
                        SoLuongXuat: { type: "number" },
                        SoLuongTon: { type: "number" },

                    },
                }
            },
        });

    $("#gridQuanLyVatTuKhoTong").kendoGrid({
        dataSource: dataSourceKhoTongVatTu,
        height: window.innerHeight * 0.80,
        width: "100%",
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
             {
                 field: "AnhDaiDien", title: "Ảnh",
                 attributes: { style: "text-align:left;" },
                 filterable: false,
                 template: kendo.template($("#tplAnh").html()),
                 width: 70
             },
            {
                field: "TenSanPham",
                title: "Tên Sản Phẩm",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200
            },
            {
                field: "MaSanPham",
                title: "Mã Sản Phẩm",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 150
            },
            {
                field: "Mau",
                title: "Màu",
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 80
            },
            {
                field: "Size",
                title: "Size",
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 60
            },
            {
                field: "TenDonVi",
                title: GetTextLanguage("tendonvi"),
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 100
            },
             //{
             //    field: "TinhTrang",
             //    title: "Tình Trạng",
             //    filterable: false,
             //    attributes: alignCenter,
             //    template: kendo.template($("#tplTinhTrang").html()),
             //    width: 100
             //},
             //{
             //    field: "LoaiHinh",
             //    title: "Loại hình",
             //    filterable: false,
             //    attributes: alignCenter,
             //    template: kendo.template($("#tplLoaiHinh").html()),
             //    width: 100
             //},

            {
                field: "SoLuongNhap",
                title: "Tổng nhập",
                filterable: FilterInColumn,
                attributes: alignRight,
                format:"{0:n0}",
                width: 100
            },
            {
                field: "SoLuongXuat",
                title: "Tổng xuất",
                filterable: FilterInColumn,
                attributes: alignRight,
                format:"{0:n0}",
                width: 100
            },
            {
                field: "SoLuongTon",
                title: GetTextLanguage("tonhientai"),
                filterable: FilterInColumn,
                attributes: alignRight,
                format:"{0:n0}",
                width: 100
            },

        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });

    $("#btnNhapVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlNhapVatTuKhoTong", "95%", "90%", null, "Mua");
        $("#mdlNhapVatTuKhoTong").load(currentController + "/PopupNhapKhoTong");
    });

    $("#btnXuatVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlXuatVatTuKhoTong", "95%", "90%", null, "Xuất");
        $("#mdlXuatVatTuKhoTong").load(currentController + "/PopupXuatKhoTong");
    });

    $("#btnLichSuNhapVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlLichSuNhapVatTuKhoTong", "99%", "90%", "tplLichSuNhapVatTuKhoTong", "Lịch sử nhập");
        var dataLoaiHinhNhap = [
            { text: GetTextLanguage("tatca"), value: "0" },
            { text: GetTextLanguage("dieuchuyen"), value: "1" },
            { text: GetTextLanguage("mua"), value: "2" }
        ];
        $("#ipLoaiHinhNhap").kendoDropDownList({
            dataSource: dataLoaiHinhNhap,
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var value = this.value();
                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
            }
        })
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y, 0, 1), change: function () {
                var value = this.value();
                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay").val() });
            }
        });
        $("#inputDenNgay").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });
        //Tạo Grid LichSuNhapVatTuKhoTong
        var dataLichSuNhapVatTuKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetDataLichSuNhapKhoTong",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: { LoaiHinhNhap: 0, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "ChiTietPhieuNhapSanPhamID",
                    fields: {
                        PhieuNhapSanPhamID: { type: "string" },
                        ThoiGian: { type: "date" }
                    }
                }
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            sort: [{ field: "TenSanPham", dir: "asc" }, { field: "Size", dir: "asc" }],
            aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
        });
        $("#gridLichSuNhapVatTuKhoTong").kendoGrid({
            dataSource: dataLichSuNhapVatTuKhoTong,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: innerHeight * 0.7,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"),
                    attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: formatToDateTime("ThoiGian"),
                    groupHeaderTemplate: "[ #= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # - <span style='color:brown'>#=getNoidungGroupLichSuNhap(value)#</span> - <span style='color:brown'> Tổng giá trị phiếu: #= kendo.toString(aggregates.ThanhTien.sum,'n0') # VNĐ </span> ]",
                },
                 {
                     field: "AnhDaiDien", title: "Ảnh",
                     attributes: { style: "text-align:left;" },
                     filterable: FilterInTextColumn,
                     template: kendo.template($("#tplAnh").html()),
                     width: 70
                 },
                 {
                     field: "MaPhieuNhap", title: GetTextLanguage("maphieu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                 },
                {
                    field: "TenSanPham", title: "Tên Sản Phẩm", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "MaSanPham", title: "Mã Sản Phẩm", attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 100
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 100
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n0') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n0')# </div>",
                },
                {
                    field: "GiaNhap", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(GiaNhap,'n0') #",
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                    attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(ThanhTien,'n0') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n0')# </div>",
                },
                {
                    field: "TenDoiTac", title: GetTextLanguage("doitac"), attributes: { "style": "text-align:center !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenNhanVien", title: "Nhân viên nhập", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                },
            ],
            dataBound: function (e) {
                showslideimg10();
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });
        $("#gridLichSuNhapVatTuKhoTong .k-grid-content").on("dblclick", "td", function () {
            var rowct = $(this).closest("tr"),
                   gridct = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid"),
                   dataItem = gridct.dataItem(rowct);
            if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                console.log(dataItem);
                CreateModalWithSize("mdlChiTietPhieuNhap", "99%", "90%", null, "Chi Tiết Phiếu Nhập");
                $("#mdlChiTietPhieuNhap").load(currentController + "/PopupChiTietPhieuNhapVatTu?PhieuNhapVatTuID=" + dataItem.PhieuNhapSanPhamID);
            }
        })


        $("#mdlLichSuNhapVatTuKhoTong .xuat-excel").click(function () {
            var filer = [];
            var filers = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid").dataSource.filter();
            if (filers != null && filers != undefined) {
                for (var i = 0; i < filers.filters.length; i++) {
                    filers.filters[i].Operator = filers.filters[i].operator;
                    filer.push(filers.filters[i]);
                }
            }
            location.href = currentController + '/XuatExcelLichSuNhapVatTuKhoTong?filters=' + JSON.stringify(filer) + '&LoaiHinhNhap=0&TuNgay=' + $("#inputTuNgay").val() + '&DenNgay=' + $("#inputDenNgay").val();
        });
    });

    $("#btnLichSuXuatVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlLichSuXuatVatTuKhoTong", "99%", "90%", "tplLichSuXuatVatTuKhoTong", "Lịch sử xuất");
        var dataLoaiHinhXuat = [
            { text: GetTextLanguage("tatca"), value: "0" },
            { text: "Xuất cho đơn hàng", value: "1" },
            { text: "Xuất trả lại", value: "2" }
        ];
        $("#ipLoaiHinhXuat").kendoDropDownList({
            dataSource: dataLoaiHinhXuat,
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var value = this.value();
                $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource.read({ LoaiHinhXuat: value, CongTrinhDenID: $("#ipCongTrinhDen").data("kendoDropDownList").value(), TuNgay: $("#inputTuNgay2").val(), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        createpopupdonhang($(".filter-hoadon"));
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay2").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                var value = this.value();
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhDenID: $("#ipCongTrinhDen").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        $("#inputDenNgay2").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhDenID: $("#ipCongTrinhDen").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });
        var dataSourceKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetAllHoaDon",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8"
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        })
        var iskhodonvi = $("#gridQuanLyVatTuKhoDonVi").length > 0;
        $("#ipCongTrinhDen").kendoDropDownList({
            dataSource: dataSourceKhoTong,
            filter: "contains",
            dataTextField: "TextHienThi",
            dataValueField: "DonHangID",
            optionLabel: "Tất cả",
            headerTemplate: kendo.template($("#HoaDonTemplateHeader").html()),
            template: kendo.template($("#HoaDonTemplate").html()),
            valueTemplate: kendo.template($("#HoaDonValueTemplateHeader").html()),
            change: function () {
                var value = this.value();
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhDenID: $("#ipCongTrinhDen").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        //Tạo Grid LichSuXuatVatTuKhoTong
        var dataLichSuXuatVatTuKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetLichSuXuatKhoTong",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        LoaiHinhXuat: 0,
                        CongTrinhDenID: $("#ipCongTrinhDen").val(),
                        TuNgay: $("#inputTuNgay2").val(),
                        DenNgay: $("#inputDenNgay2").val()
                    }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "ChiTietPhieuXuatSanPhamID",
                    fields: {
                        PhieuXuatSanPhamID: { type: "string" },
                        ThoiGian: { type: "date" }
                    }
                }
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            sort: [{ field: "TenSanPham", dir: "asc" }, { field: "Size", dir: "asc" }],
            aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
        });
        $("#gridLichSuXuatVatTuKhoTong").kendoGrid({
            dataSource: dataLichSuXuatVatTuKhoTong,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: window.innerHeight * 0.9 - 100,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                    groupHeaderTemplate: "<button class='btn btn-info btnTraSanPham'>Trả sản phẩm</button>     [ #= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # - <span style='color:brown'>#=getNoidungGroupLichSuXuat(value)#</span> - <span style='color:brown'> Tổng giá trị phiếu: #= kendo.toString(aggregates.ThanhTien.sum,'n0') # VNĐ </span> ]",
                },
                {
                    field: "AnhDaiDien", title: "Ảnh",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    template: kendo.template($("#tplAnh").html()),
                    width: 70
                },
                {
                    field: "TenSanPham", 
                    title: "Tên Sản Phẩm", 
                    attributes: { "style": "text-align:left !important;" }, 
                    filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "MaSanPham", title: "Mã Sản Phẩm", attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 100
                },
                {
                    field: "Mau",
                    title: "Màu",
                    filterable: FilterInTextColumn,
                    attributes: alignCenter,
                    width: 100
                },
                {
                    field: "Size",
                    title: "Size",
                    filterable: FilterInTextColumn,
                    attributes: alignCenter,
                    width: 50
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n0') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n0') # </div>"
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(DonGia,'n0') #",
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                    attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(ThanhTien,'n0') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n0')# </div>",
                },
                {
                    field: "TenNhanVien", title: GetTextLanguage("nguoixuat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },

            ],
            dataBound: function (e) {
                showslideimg10();
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });

            },
        });
        $("#gridLichSuXuatVatTuKhoTong").on("click", ".btnTraSanPham", function(){
            var rowct = $(this).closest("tr"),
                gridct = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid"),
                dataItem = gridct.dataItem(rowct);
            CreateModalWithSize("mdlTraSanPham", "99%", "90%", "tpltraSanPham", "Trả Sản Phẩm");
            $("#ThoiGianTra").kendoDateTimePicker({
                format: "dd/MM/yyyy HH:mm",
                value: new Date(),
            });
            $("#gridDanhSachVatTuTra").kendoGrid({
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: currentController + "/GetDataChiTietPhieuNhan",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            data: { PhieuNhanDonHangID: dataItem.PhieuNhanDonHangID }
                        },
                        parameterMap: function (options, type) {
                            return JSON.stringify(options);
                        }
                    },
                    type: "json",
                    sort: [{ field: "TenSanPham", dir: "asc" }, { field: "Size", dir: "asc" }],
                    pageSize: 25,
                    schema: {
                        data: "data",
                        total: "total",
                        model: {
                            id: "ChiTietXuatXuatVatTuID",
                            fields: {
                                PhieuXuatVatTuID: { type: "string", editable: false },
                                ThoiGian: { type: "date", editable: false },
                                TenDonVi: { type: "string", editable: false },
                                TenVatTu: { type: "string", editable: false },
                                TenVietTat: { type: "string", editable: false },
                                SoLuong: { type: "number", editable: true },
                                DonGia: { type: "number", editable: false },
                                ThanhTien: { type: "number", editable: false },
                            }
                        }
                    },
                }),
                height: window.innerHeight * 0.6,
                editable: true,
                selectable: false,
                batch: true,
                dataBinding: function () {
                    record = 0;
                },
                filterable: {
                    mode: "row"
                },
                columns:
                [
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                        template: "#= ++record #",
                        width: 60,
                        attributes: alignCenter
                    },
                    { field: "VatTuID", hidden: true },
                    {
                        field: "DsHinhAnh", title: "Ảnh",
                        attributes: { style: "text-align:left;" },
                        filterable: false,
                        template: kendo.template($("#tplAnh").html()),
                        width: 70
                    },
                    {
                        field: "TenSanPham", width: 200,
                        title: "Tên sản phẩm",
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "MaSanPham",
                        width: 200,
                        attributes: { style: "text-align:left;" },
                        title: "Mã sản phẩm",
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "TenDonVi", width: 200,
                        title: GetTextLanguage("donvi"),
                        attributes: { "style": "text-align:left !important;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "DonGia", title: GetTextLanguage("gianhap"),
                        format: "{0:n0}",
                        attributes: { style: "text-align:right;" },
                        filterable: FilterInColumn, width: 150,
                    },
                    {
                        field: "SoLuong", width: 100,
                        title: GetTextLanguage("soluong"),
                        format: "{0:n0}",
                        attributes: { alignRight, class: "soluongcheck" },
                        filterable: FilterInColumn,
                        editor: function (container, options) {
                            rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                            var input = $('<input type="text" style="width:75%;height:15px;" data-v-min="1" class="form-control" id="' + options.model.get("VatTuID") + '" ' + rs + ' name="SoLuong" />');
                            input.appendTo(container);
                            input.autoNumeric(autoNumericOptionsSoLuong);
                        },
                    },
                    {
                        field: "ThanhTien",
                        title: GetTextLanguage("thanhtien"),
                        format: "{0:n0}",
                        attributes: { "style": "text-align:right !important;" },
                        filterable: FilterInColumn,
                        width: 150,
                    },
                    {
                        title: GetTextLanguage("thaotac"),
                        command: [
                            {
                                text: GetTextLanguage("xoa"),
                                name: "destroy"
                            }
                        ],
                        width: 100,
                    },
                ],
                dataBound: function (e) {
                    showslideimg10();
                },
                save: function (e) {
                    var items = $("#gridDanhSachVatTuTra").data("kendoGrid").dataSource.data();
                    if (e.values.DonGia != null) {
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].VatTuID == e.model.VatTuID) {
                                items[i].ThanhTien = (e.values.DonGia == null ? 1 : e.values.DonGia) * (e.model.SoLuong == null ? 1 : e.model.SoLuong);
                                items[i].DonGia = e.values.DonGia;
                                break;
                            }
                        }
                    } else if (e.values.SoLuong != null) {
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].VatTuID == e.model.VatTuID) {
                                items[i].ThanhTien = (e.model.DonGia == null ? 1 : e.model.DonGia) * (e.values.SoLuong == null ? 1 : e.values.SoLuong);
                                items[i].SoLuong = e.values.SoLuong;
                                break;
                            }
                        }
                    }

                    $("#gridDanhSachVatTuTra").data("kendoGrid").dataSource.data(items);
                }
            });
            $("#btnLuuTraSanPham").click(function () {
                var dataChiTietXuat = $("#gridDanhSachVatTuTra").data("kendoGrid").dataSource.data();
                var PhieuXuatVatTuID = dataItem.PhieuNhanDonHangID;
                var thoigianmua = $("#ThoiGianTra").val();
                //var doitacID = $("#drdDoiTac").data("kendoDropDownList").value();
                var hinhanh = "";
                var DSHinhAnh = [];
                var ins = document.getElementById('files_02').files.length;
                if (ins > 0) {
                    var strUrl = uploadMultipleFileParagam2("/Upload" + "/Uploadfiles", "files_02", $(".dshinhanhadd2")).split(',');
                    for (var k = 0; k < strUrl.length; k++) {
                        DSHinhAnh.push({ Url: strUrl[k] });
                    }
                }
                for (var i = 0; i < DSHinhAnh.length; i++) {
                    if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                        hinhanh += DSHinhAnh[i].Url + ",";
                    }
                }
                $.ajax({
                    url: currentController + "/TraSanPham",
                    type: "POST",
                    data: {
                        PhieuNhanDonHangID: PhieuXuatVatTuID,
                        ThoiGian: thoigianmua,
                        //DoiTacID: doitacID,
                        DataChiTietXuat: JSON.stringify(dataChiTietXuat),
                        DSHinhAnh: hinhanh.substring(0, hinhanh.length - 1),
                    },
                    complete: function (e) {
                        alertToastr(e.responseJSON);
                        if (e.responseJSON.code === "success") {
                            $("#mdlTraSanPham").data("kendoWindow").close();
                            $("#gridQuanLyVatTuKhoTong").data("kendoGrid").dataSource.read();
                            $("#gridQuanLyVatTuKhoDonVi").data("kendoGrid").dataSource.read();
                        }
                    }
                });
            });
        });
        //$("#gridLichSuXuatVatTuKhoTong .k-grid-content").on("dblclick", "td", function () {
        //    var rowct = $(this).closest("tr"),
        //           gridct = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid"),
        //           dataItem = gridct.dataItem(rowct);
        //    if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
        //        CreateModalWithSize("mdlChiTietPhieuXuat", "99%", "90%", null, GetTextLanguage("chitietphieuxuat"));
        //        $("#mdlChiTietPhieuXuat").load(currentController + "/PopupXuatKhoTong?PhieuXuatVatTuID=" + dataItem.PhieuXuatSanPhamID);
        //    }
        //})

        $("#mdlLichSuXuatVatTuKhoTong .xuat-excel").click(function () {
            var filer = [];
            var filers = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource.filter();
            if (filers != null && filers != undefined) {
                for (var i = 0; i < filers.filters.length; i++) {
                    filers.filters[i].Operator = filers.filters[i].operator;
                    filer.push(filers.filters[i]);
                }
            }
            location.href = currentController + '/XuatExcelLichSuXuatVatTuKhoTong?filters=' + JSON.stringify(filer) + '&LoaiHinhXuat=' + $("#ipLoaiHinhXuat").val() + '&TuNgay=' + $("#inputTuNgay2").val() + '&DenNgay=' + $("#inputDenNgay2").val() + '&CongTrinhDenID=' + $("#ipCongTrinhDen").val();
        });
    });

});

