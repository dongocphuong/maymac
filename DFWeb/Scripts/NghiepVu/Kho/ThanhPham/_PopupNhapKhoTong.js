﻿$(document).ready(function () {
    var currentController = "/KhoThanhPham";
    if ($("#ThoiGianNhap").data("kendoDateTimePicker") === undefined) {
        $("#ThoiGianNhap").kendoDateTimePicker({
            format: "dd/MM/yyyy hh:mm",
            value: new Date()
        });
    }
    if ($("#drdDoiTac").data("kendoDropDownList") === undefined) {
        $("#drdDoiTac").kendoDropDownList({
            autoBind: false,
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/GetAllDataDoiTac",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8"
                    }
                },
                schema: {
                    type: "json",
                    data: "data"
                }
            }),
            dataTextField: "TenDoiTac",
            dataValueField: "DoiTacID",
            optionLabel: GetTextLanguage("chondoitac"),
            filter: "contains",
            change: function () {
                if ($("#gridChiTietNhap").data("kendoGrid").dataSource.data().length > 0) {
                    alert(GetTextLanguage("canlamlaylaigianhaptheodoitacmoi"))
                }
            }
        });
    }
    var FileDinhKem = [];
    if ($("#gridChiTietNhap").data("kendoGrid") === undefined) {
        $("#gridChiTietNhap").kendoGrid({
            dataSource: {
                batch: true,
                schema: {
                    model: {
                        id: "ThanhPhamID",
                        fields: {
                            ThanhPhamID: { editable: false },
                            TenDonVi: { type: "string", editable: false },
                            TenThanhPham: { type: "string", editable: false },
                            MaThanhPham: { type: "string", editable: false },
                            ThanhTien: { type: "number", editable: false },
                            SoLuong: { type: "number", editable: true },
                            DonGia: { type: "number", editable: true },
                        }
                    }
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
            },
            dataBound: function (e) {
                showslideimg10();
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
            editable: true,
            selectable: false,
            height: window.innerHeight * 0.5,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            columns:
            [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "<span class='stt'></span>",
                    width: 60,
                    attributes: alignCenter
                },
                {
                    field: "AnhDaiDien", title: "Ảnh",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    template: kendo.template($("#tplAnh").html()),
                    width: 70
                },
                {
                    field: "TenThanhPham", width: 200,
                    title: "Tên Thành phẩm",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "MaThanhPham",
                    width: 200,
                    attributes: { style: "text-align:left;" },
                    title: "Mã Thành phẩm",
                    filterable: FilterInTextColumn
                },
                {
                    field: "TenDonVi", width: 200,
                    title: GetTextLanguage("donvi"),
                    attributes: { "style": "text-align:left !important;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "DonGia", title: GetTextLanguage("gianhap"),
                    format: "{0:n2}",
                    attributes: alignRight,
                    filterable: FilterInColumn, width: 150,
                    editor: function (container, options) {
                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                        var input = $('<input type="text" data-v-min="1" class="form-control" id="' + options.model.get("ThanhPhamID") + '"  ' + rs + ' name="DonGia" />');
                        input.appendTo(container);
                        input.autoNumeric(autoNumericOptionsSoLuong);
                    },
                },
                {
                    field: "SoLuong", width: 100,
                    title: GetTextLanguage("soluong"),
                    format: "{0:n2}",
                    attributes: alignRight,
                    filterable: FilterInColumn,
                    editor: function (container, options) {
                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                        var input = $('<input type="text" data-v-min="1" class="form-control" id="' + options.model.get("ThanhPhamID") + '" ' + rs + ' name="SoLuong" />');
                        input.appendTo(container);
                        input.autoNumeric(autoNumericOptionsSoLuong);
                    },
                },
                {
                    field: "ThanhTien",
                    title: GetTextLanguage("thanhtien"),
                    format: "{0:n2}",
                    attributes: alignRight,
                    filterable: FilterInColumn,
                    width: 150,
                    footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n2')#</div>",
                    editor: function (container, options) {
                        container.text(kendo.toString(options.model.ThanhTien, "n2"));
                    },
                },
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                    command: [
                        {
                            text: GetTextLanguage("xoa"),
                            name: "destroy"
                        }
                    ],
                    width: 100
                },
            ],
            save: function (e) {
                var items = $("#gridChiTietNhap").data("kendoGrid").dataSource.data();
                if (e.values.DonGia != null) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].ThanhPhamID == e.model.ThanhPhamID) {
                            items[i].ThanhTien = (e.values.DonGia == null ? 1 : e.values.DonGia) * (e.model.SoLuong == null ? 1 : e.model.SoLuong);
                            items[i].DonGia = e.values.DonGia;
                            break;
                        }
                    }
                } else if (e.values.SoLuong != null) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].ThanhPhamID == e.model.ThanhPhamID) {
                            items[i].ThanhTien = (e.model.DonGia == null ? 1 : e.model.DonGia) * (e.values.SoLuong == null ? 1 : e.values.SoLuong);
                            items[i].SoLuong = e.values.SoLuong;
                            break;
                        }
                    }
                }
                $("#gridChiTietNhap").data("kendoGrid").dataSource.data(items);
            }
        });
    }
    $("#mdlNhapVatTuKhoTong").on("click", ".xoaItemFile", function () {
        var file = $(this).data("file");
        for (var i = 0; i < FileDinhKem.length; i++) {
            if (FileDinhKem[i].name === file) {
                FileDinhKem.splice(i, 1);
                break;
            }
        }
        $(this).parent().remove();
    });
    $("#btnChonVatTu").click(function () {
        var validateDoiTac = $("#drdDoiTac").kendoValidator().data("kendoValidator");
        if (validateDoiTac.validate()) {
            CreateModalWithSize("mdlDanhSachVatTu", "99%", "90%", "tplDanhSachVatTu", GetTextLanguage("danhsachvattu"));
            var dataChon = [
                { text: GetTextLanguage("dachon"), value: "true" },
                { text: GetTextLanguage("chuachon"), value: "false" }
            ];
            $("#gridDanhSachVatTu").kendoGrid({
                dataSource: new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: currentController + "/LayDanhSachThanhPham",
                            dataType: "json",
                            type: "GET",
                            contentType: "application/json; charset=utf-8",
                            data: {
                                DoiTacID: $("#drdDoiTac").data("kendoDropDownList").value()
                            }
                        },
                    },
                    schema: {
                        data: "data",
                        total: "total",
                        model: {
                            id: "ThanhPhamID",
                            fields: {
                                Chon: {
                                    type: "boolean",
                                    editable: false
                                },
                                TenThanhPham: {
                                    type: "string",
                                    editable: false
                                },
                                MaThanhPham: {
                                    type: "string",
                                    editable: false
                                },
                                TenDonVi: {
                                    type: "string",
                                    editable: false
                                },
                                DonGia: {
                                    type: "number"
                                },
                                SoLuong: {
                                    type: "number"
                                },

                            }
                        }
                    },
                    change: function (e) {
                        var data = this.data();
                    }
                }),
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                editable: true,
                height: innerHeight * 0.75,
                width: "100%",
                dataBound: function (e) {
                    var dataSource = this.dataSource.view();
                    var dataItem = $("#gridChiTietNhap").data("kendoGrid").dataSource.data();
                    for (var i = 0; i < dataSource.length; i++) {
                        if (dataSource[i].Chon === undefined) dataSource[i].Chon = false;
                        for (var j = 0; j < dataItem.length; j++) {
                            if (dataSource[i].ThanhPhamID == dataItem[j].ThanhPhamID) {
                                $("#gridDanhSachVatTu").find("tr[data-uid='" + dataSource[i].uid + "'] td:eq(3)").text(dataItem[j].DonGia).autoNumeric(autoNumericOptionsSoLuong);
                                dataSource[i].DonGia = dataItem[j].DonGia;
                                $("#gridDanhSachVatTu").find("tr[data-uid='" + dataSource[i].uid + "'] td:eq(4)").text(dataItem[j].SoLuong).autoNumeric(autoNumericOptionsSoLuong);
                                dataSource[i].SoLuong = dataItem[j].SoLuong;
                            }
                        }
                    }
                },
                columns: [
                   
                      {
                          field: "AnhDaiDien", title: "Ảnh",
                          attributes: { style: "text-align:left;" },
                          filterable: FilterInTextColumn,
                          template: kendo.template($("#tplAnh").html()),
                          width: 70
                      },
                        {
                            field: "TenThanhPham",
                            title: "Tên Thành phẩm",
                            filterable: FilterInTextColumn,
                            attributes: alignLeft,
                            width: 200
                        },
                    {
                        field: "MaThanhPham",
                        title: "Mã Thành phẩm",
                        filterable: FilterInTextColumn,
                        attributes: alignLeft,
                        width: 150
                    },
                    {
                        field: "TenDonVi",
                        title: GetTextLanguage("donvi"),
                        width: 200,
                        attributes: { "style": "text-align:left !important;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "DonGia", width: 150,
                        title: GetTextLanguage("gianhap"),
                        format: "{0:n2}",
                        attributes: { "style": "text-align:right !important;" },
                        filterable: FilterInColumn,
                        editor: function (container, options) {
                            rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                            var input = $('<input type="text" data-v-min="1" style="width:80%;height:15px;" class="form-control" id="' + options.model.get("ThanhPhamID") + '" name="DonGia" />');
                            input.appendTo(container);
                            input.autoNumeric(autoNumericOptionsSoLuong);
                        },
                    },
                    {
                        field: "SoLuong",
                        width: 100,
                        title: GetTextLanguage("soluong"),
                        format: "{0:n2}",
                        attributes: {
                            "class": "soluongcheck",
                            "style": "font-weight:bold"
                        },
                        filterable: FilterInColumn,
                        editor: function (container, options) {
                            rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                            var input = $('<input type="text" data-v-min="1" style="width:75%;height:15px;" class="form-control" id="' + options.model.get("ThanhPhamID") + '" name="SoLuong" />');
                            input.appendTo(container);
                            input.autoNumeric(autoNumericOptionsSoLuong);
                        },
                    }
                ],
                dataBound: function (e) {
                    showslideimg10();
                },
            });
            $("#gridDanhSachVatTu").data("kendoGrid").table.on("click", ".chon", function () {
                var dataItem = $("#gridDanhSachVatTu").data("kendoGrid").dataItem($(this).closest("tr"));
                if ($(this)[0].checked === true) {
                    dataItem.Chon = true;
                    $("#gridDanhSachVatTu").data("kendoGrid").select($("#gridDanhSachVatTu").data("kendoGrid").table.find("[data-uid=" + dataItem.uid + "]"));
                }
                else {
                    dataItem.Chon = false;
                    $("#gridDanhSachVatTu").data("kendoGrid").table.find("[data-uid=" + dataItem.uid + "]").attr("class", "");
                }
            });
            $("#gridDanhSachVatTu").data("kendoGrid").table.on("dblclick", "tr", function () {
                var dataItem = $("#gridDanhSachVatTu").data("kendoGrid").dataItem($(this).closest("tr"));
                if (dataItem.Chon === true) {
                    dataItem.Chon = false;
                    $(this).find(".chon").prop('checked', false);
                    $("#gridDanhSachVatTu").data("kendoGrid").table.find("[data-uid=" + dataItem.uid + "]").attr("class", "");
                }
                else {
                    dataItem.Chon = true;
                    $(this).find(".chon").prop('checked', true);
                    $("#gridDanhSachVatTu").data("kendoGrid").select($("#gridDanhSachVatTu").data("kendoGrid").table.find("[data-uid=" + dataItem.uid + "]"));
                }
            });
            $("#btnDongY").click(function () {
                var arrData = $("#gridChiTietNhap").data("kendoGrid").dataSource.data();
                var dataItem = $("#gridDanhSachVatTu").data("kendoGrid").dataSource.data();
                for (var i = 0; i < dataItem.length; i++) {
                    if (dataItem[i].SoLuong != null) {
                        var isExsit = false;
                        for (var j = 0; j < arrData.length; j++) {
                            if (dataItem[i].ThanhPhamID == arrData[j].ThanhPhamID) {
                                isExsit = true;
                                arrData[j].DonGia = dataItem[i].DonGia;
                                arrData[j].SoLuong = dataItem[i].SoLuong;
                                arrData[j].ThanhTien = (dataItem[i].DonGia == null ? 1 : dataItem[i].DonGia) * (dataItem[i].SoLuong == null ? 1 : dataItem[i].SoLuong);
                            }
                        }
                        if (!isExsit) {
                            dataItem[i].ThanhTien = (dataItem[i].DonGia == null ? 1 : dataItem[i].DonGia) * (dataItem[i].SoLuong == null ? 1 : dataItem[i].SoLuong);
                            arrData.push(dataItem[i]);
                        }
                    }
                }
                $("#mdlDanhSachVatTu").data("kendoWindow").close();
                $("#gridChiTietNhap").data("kendoGrid").dataSource.data(arrData);
            });
            $("#btnHuyChonVatTu").click(function () {
                $("#mdlDanhSachVatTu").data("kendoWindow").close();
            });
        }
    });
    $("#btnHuy").click(function () {
        $("#mdlNhapVatTuKhoTong").data("kendoWindow").close();
    });
    var dataimg = new FormData();
    $("#contenttabMuaVatTuKhoTong #files_0").change(function () {
        for (var x = 0; x < this.files.length; x++) {
            dataimg.append("uploads", this.files[x]);
        };
        loadimgSuaChua5(this, $("#contenttabMuaVatTuKhoTong"));
        $("#contenttabMuaVatTuKhoTong #files_0").val('').clone(true);
    })
    $("#btnDongYMua").click(function () {
        var validate = $("#contenttabMuaVatTuKhoTong").kendoValidator().data("kendoValidator");
        if (validate.validate()) {
            var dataChiTietNhap = $("#gridChiTietNhap").data("kendoGrid").dataSource.data();
            if (dataChiTietNhap.length == 0) {
                showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("chonvattu"));
                $("#mdlNhapVatTuKhoTong").unblock();
                return false;
            }
            for (var i = 0; i < dataChiTietNhap.length; i++) {
                if (dataChiTietNhap[i].DonGia == null || dataChiTietNhap[i].DonGia <= 0) {
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("dongiaphailonhonkhong") + (i + 1));
                    return false;
                }
                if (dataChiTietNhap[i].SoLuong == null || dataChiTietNhap[i].SoLuong <= 0) {
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("phainhapsoluong") + (i + 1));
                    return false;
                }
            }
            var thoigianmua = $("#ThoiGianNhap").data("kendoDateTimePicker").value();
            var doitacID = $("#drdDoiTac").data("kendoDropDownList").value();
            var hinhanh = "";
            var ins = dataimg.getAll("uploads").length;
            var DSHinhAnh = [];
            $('.dshinhanhadd .imgslider').each(function (index2, element2) {
                if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                    DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
                }
            });
            if (ins > 0) {
                var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", dataimg.getAll("uploads"), $(".dshinhanhadd", $('#contenttabMuaVatTuKhoTong'))).split(',');
                for (var k = 0; k < strUrl.length; k++) {
                    DSHinhAnh.push({ Url: strUrl[k] });
                }
            }
            for (var i = 0; i < DSHinhAnh.length; i++) {
                if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                    hinhanh += DSHinhAnh[i].Url + ",";
                }
            }
            ContentWatingOP("mdlNhapVatTuKhoTong", 0.9);
            setTimeout(function () {
                $.ajax({
                    url: currentController + "/Mua",
                    type: "POST",
                    data: {
                        ThoiGian: $("#ThoiGianNhap").val(),
                        DoiTacID: doitacID,
                        DataChiTietNhap: JSON.stringify(dataChiTietNhap),
                        DSHinhanh: hinhanh.substring(0, hinhanh.length - 1),
                    },
                    complete: function (e) {
                        alertToastr(e.responseJSON);
                        if (e.responseJSON.code === "success") {
                            $("#mdlNhapVatTuKhoTong").data("kendoWindow").close();
                            $("#gridQuanLyVatTuKhoTong").data("kendoGrid").dataSource.read();
                            $("#mdlNhapVatTuKhoTong").unblock();
                        }
                        else {
                            $("#mdlNhapVatTuKhoTong").unblock();
                        }
                    }
                });
            }, 1000);
        }
    });
});