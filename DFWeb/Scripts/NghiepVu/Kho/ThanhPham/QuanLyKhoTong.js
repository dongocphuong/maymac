﻿function getNoidungGroupLichSuNhap(value) {
    var datasource = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[i].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupLichSuNhap2(value) {
    var datasource = $("#gridtheophieu").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[i].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupLichSuXuat(value) {
    var datasource = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                result = group[i].NoiDung
            }
        }
    });
    return result;
};
$(document).ready(function () {
    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });

    var currentController = "/KhoThanhPham";

    var dataSourceKhoTongVatTu = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: currentController + "/GetDataKhoTong",
                    dataType: 'json',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                }
            },
            batch: true,
            pageSize: 20,
            sort: [{ field: "TenVatTu", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "ThanhPhamID",
                    fields: {
                        VatTuID: { type: "string" },
                        TenVatTu: { type: "string" },
                        TenDonVi: { type: "string" },
                        TonHienTai: { type: "number" },
                        GhiChu: { type: "string" }
                    },
                }
            },
        });

    $("#gridQuanLyVatTuKhoTong").kendoGrid({
        dataSource: dataSourceKhoTongVatTu,
        height: window.innerHeight * 0.80,
        width: "100%",
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
             {
                 field: "AnhDaiDien", title: "Ảnh",
                 attributes: { style: "text-align:left;" },
                 filterable: FilterInTextColumn,
                 template: kendo.template($("#tplAnh").html()),
                 width: 70
             },
            {
                field: "TenThanhPham",
                title: "Tên Thành phẩm",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200
            },
            {
                field: "MaThanhPham",
                title: "Mã Thành phẩm",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 150
            },
            {
                field: "TenDonVi",
                title: GetTextLanguage("tendonvi"),
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 100
            },
            {
                field: "SoLuongNhap",
                title: "Tổng nhập",
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongNhap"),
                width: 100
            },
          
            {
                field: "SoLuongNhap",
                title: "Tổng xuất",
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongXuat"),
                width: 100
            },
            {
                field: "SoLuongTon",
                title: GetTextLanguage("tonhientai"),
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongTon"),
                width: 100
            },
          
        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });

    $("#btnNhapVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlNhapVatTuKhoTong", "95%", "90%", null, "Mua");
        $("#mdlNhapVatTuKhoTong").load(currentController + "/PopupNhapKhoTong");
    });

    $("#btnXuatVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlXuatVatTuKhoTong", "95%", "90%", null, "Xuất");
        $("#mdlXuatVatTuKhoTong").load(currentController + "/PopupXuatKhoTong");
    });

    $("#btnLichSuNhapVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlLichSuNhapVatTuKhoTong", "99%", "90%", "tplLichSuNhapVatTuKhoTong", GetTextLanguage("lichsunhapvattukhotong"));
        var dataLoaiHinhNhap = [
            { text: GetTextLanguage("tatca"), value: "0" },
            { text: GetTextLanguage("dieuchuyen"), value: "1" },
            { text: GetTextLanguage("mua"), value: "2" }
        ];
        $("#ipLoaiHinhNhap").kendoDropDownList({
            dataSource: dataLoaiHinhNhap,
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var value = this.value();
                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
            }
        })
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y, m -1, d), change: function () {
                var value = this.value();
                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay").val() });
            }
        });
        $("#inputDenNgay").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });
        //Tạo Grid LichSuNhapVatTuKhoTong
        var dataLichSuNhapVatTuKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetDataLichSuNhapKhoTong",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: { LoaiHinhNhap: 0, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "ChiTietPhieuNhapThanhPhamID",
                    fields: {
                        PhieuNhapThanhPhamID: { type: "string" },
                        ThoiGian: { type: "date" }
                    }
                }
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
        });
        $("#gridLichSuNhapVatTuKhoTong").kendoGrid({
            dataSource: dataLichSuNhapVatTuKhoTong,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: innerHeight * 0.7,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"),
                    attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: formatToDateTime("ThoiGian"),
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuNhap(value)#</scpan>",
                },
                 {
                     field: "AnhDaiDien", title: "Ảnh",
                     attributes: { style: "text-align:left;" },
                     filterable: FilterInTextColumn,
                     template: kendo.template($("#tplAnh").html()),
                     width: 70
                 },
                 {
                     field: "MaPhieuNhap", title: GetTextLanguage("maphieu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                 },
                {
                    field: "TenThanhPham", title: "Tên Thành phẩm", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "MaThanhPham", title: "Mã Thành phẩm", attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 100
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 100
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                },
                {
                    field: "GiaNhap", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(GiaNhap,'n2') #",
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                    attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(ThanhTien,'n2') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                },
                {
                     field: "TenDoiTac", title: GetTextLanguage("doitac"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenNhanVien", title:"Nhân viên nhập", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
              },
            ],
            dataBound: function (e) {
                showslideimg10();
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });
        $("#gridLichSuNhapVatTuKhoTong .k-grid-content").on("dblclick", "td", function () {
            var rowct = $(this).closest("tr"),
                   gridct = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid"),
                   dataItem = gridct.dataItem(rowct);
            if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                console.log(dataItem);
                CreateModalWithSize("mdlChiTietPhieuNhap", "99%", "90%", null, "Chi Tiết Phiếu Nhập");
                $("#mdlChiTietPhieuNhap").load(currentController + "/PopupChiTietPhieuNhapVatTu?PhieuNhapVatTuID=" + dataItem.PhieuNhapThanhPhamID);
            }
        })
       

        $("#mdlLichSuNhapVatTuKhoTong .xuat-excel").click(function () {
            var filer = [];
            var filers = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid").dataSource.filter();
            if (filers != null && filers!=undefined) {
                for (var i = 0; i < filers.filters.length; i++) {
                    filers.filters[i].Operator = filers.filters[i].operator;
                    filer.push(filers.filters[i]);
                }
            }
            location.href = currentController + '/XuatExcelLichSuNhapVatTuKhoTong?filters=' + JSON.stringify(filer) + '&LoaiHinhNhap=' + $("#ipLoaiHinhNhap").val() + '&TuNgay=' + $("#inputTuNgay").val() + '&DenNgay=' + $("#inputDenNgay").val();
        });
    });

    $("#btnLichSuXuatVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlLichSuXuatVatTuKhoTong", "99%", "90%", "tplLichSuXuatVatTuKhoTong", "Lịch sử xuất vật tư kho tổng");
        var dataLoaiHinhXuat = [
            { text: GetTextLanguage("tatca"), value: "0" },
            { text: "Xuất cho đơn hàng", value: "1" },
            { text: "Xuất trả lại", value: "2" }
        ];
        $("#ipLoaiHinhXuat").kendoDropDownList({
            dataSource: dataLoaiHinhXuat,
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var value = this.value();
                $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource.read({ LoaiHinhXuat: value, CongTrinhDenID: $("#ipCongTrinhDen").data("kendoDropDownList").value(), TuNgay: $("#inputTuNgay2").val(), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        $(".lst-modal").popuptablefilters();
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay2").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                var value = this.value();
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhDenID:$("#ipCongTrinhDen").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        $("#inputDenNgay2").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(),CongTrinhDenID:$("#ipCongTrinhDen").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });
        var dataSourceKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetAllHoaDon",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8"
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        })
        var iskhodonvi = $("#gridQuanLyVatTuKhoDonVi").length > 0;
        $("#ipCongTrinhDen").kendoDropDownList({
            dataSource: dataSourceKhoTong,
            filter: "contains",
            dataTextField: "TextHienThi",
            dataValueField: "DonHangID",
            optionLabel: "Chọn đơn hàng",
            headerTemplate: kendo.template($("#HoaDonTemplateHeader").html()),
            template: kendo.template($("#HoaDonTemplate").html()),
            valueTemplate: kendo.template($("#HoaDonValueTemplateHeader").html()),
            change: function () {
                var value = this.value();
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhDenID: $("#ipCongTrinhDen").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        //Tạo Grid LichSuXuatVatTuKhoTong
        var dataLichSuXuatVatTuKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetLichSuXuatKhoTong",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        LoaiHinhXuat: 0,
                        CongTrinhDenID: $("#ipCongTrinhDen").val(),
                        TuNgay: $("#inputTuNgay2").val(),
                        DenNgay: $("#inputDenNgay2").val()
                    }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total",
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
        });
        $("#gridLichSuXuatVatTuKhoTong").kendoGrid({
            dataSource: dataLichSuXuatVatTuKhoTong,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: window.innerHeight * 0.9 - 100,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuXuat(value)#</span>",
                },
                  {
                      field: "AnhDaiDien", title: "Ảnh",
                      attributes: { style: "text-align:left;" },
                      filterable: FilterInTextColumn,
                      template: kendo.template($("#tplAnh").html()),
                      width: 70
                  },
                  {
                     field: "TenThanhPham", title: "Tên Thành phẩm", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                 },
                {
                    field: "MaThanhPham", title: "Mã Thành phẩm", attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 100
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "TenNhanVien", title: GetTextLanguage("nguoixuat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
               
            ],
            dataBound: function (e) {
                showslideimg10();
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });
        $("#gridLichSuXuatVatTuKhoTong .k-grid-content").on("dblclick", "td", function () {
            var rowct = $(this).closest("tr"),
                   gridct = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid"),
                   dataItem = gridct.dataItem(rowct);
            if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                CreateModalWithSize("mdlChiTietPhieuXuat", "99%", "90%", null, GetTextLanguage("chitietphieuxuat"));
                $("#mdlChiTietPhieuXuat").load(currentController + "/PopupXuatKhoTong?PhieuXuatVatTuID=" + dataItem.PhieuXuatThanhPhamID);
            }
        })
       
        $("#mdlLichSuXuatVatTuKhoTong .xuat-excel").click(function () {
            var filer = [];
            var filers = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource.filter();
            if (filers != null && filers != undefined) {
                for (var i = 0; i < filers.filters.length; i++) {
                    filers.filters[i].Operator = filers.filters[i].operator;
                    filer.push(filers.filters[i]);
                }
            }
            location.href = currentController + '/XuatExcelLichSuXuatVatTuKhoTong?filters=' + JSON.stringify(filer) + '&LoaiHinhXuat=' + $("#ipLoaiHinhXuat").val() + '&TuNgay=' + $("#inputTuNgay2").val() + '&DenNgay=' + $("#inputDenNgay2").val() + '&CongTrinhDenID=' + $("#ipCongTrinhDen").val();
        });
    });

});

