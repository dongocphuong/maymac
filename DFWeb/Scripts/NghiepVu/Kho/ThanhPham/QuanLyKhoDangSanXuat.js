﻿

$(document).ready(function () {
    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });

    var currentController = "/KhoThanhPhamDangSanXuat";

    var dataSourceKhoTongVatTu = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: currentController + "/GetDataTonKho",
                    dataType: 'json',
                    type: 'GET',
                    contentType: "application/json; charset=utf-8",
                    data:{DonHangID:""}
                }
            },
            batch: true,
            pageSize: 20,
            sort: [{ field: "TenVatTu", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "ThanhPhamID",
                    fields: {
                        VatTuID: { type: "string" },
                        TenVatTu: { type: "string" },
                        TenDonVi: { type: "string" },
                        TonHienTai: { type: "number" },
                        GhiChu: { type: "string" }
                    },
                }
            },
        });

    $("#gridKhoThanhPhamDangSanXuat").kendoGrid({
        dataSource: dataSourceKhoTongVatTu,
        height: window.innerHeight * 0.80,
        width: "100%",
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
             {
                 field: "MaDonHang",
                 title: "Mã đơn hang",
                 filterable: FilterInTextColumn,
                 attributes: alignLeft,
                 width: 200
             },
              {
                  field: "TenDonHang",
                  title: "Tên đơn hàng",
                  filterable: FilterInTextColumn,
                  attributes: alignLeft,
                  width: 200
              },
             {
                 field: "AnhDaiDien", title: "Ảnh",
                 attributes: { style: "text-align:left;" },
                 filterable: FilterInTextColumn,
                 template: kendo.template($("#tplAnh").html()),
                 width: 70
             },
            {
                field: "TenThanhPham",
                title: "Tên Thành phẩm",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200
            },
            {
                field: "MaThanhPham",
                title: "Mã Thành phẩm",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 150
            },
            {
                field: "TenDonVi",
                title: GetTextLanguage("tendonvi"),
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 100
            },
            {
                field: "SoLuongTon",
                title: GetTextLanguage("tonhientai"),
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongTon"),
                width: 100
            },
          
        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    var dataSourceKhoTong = new kendo.data.DataSource({
        transport: {
            read: {
                url: currentController + "/GetAllHoaDon",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8"
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    })
    $("#HoaDonID").kendoDropDownList({
        dataSource: dataSourceKhoTong,
        filter: "contains",
        dataTextField: "TextHienThi",
        dataValueField: "DonHangID",
        optionLabel: "Tất cả",
        headerTemplate: kendo.template($("#HoaDonTemplateHeader").html()),
        template: kendo.template($("#HoaDonTemplate").html()),
        valueTemplate: kendo.template($("#HoaDonValueTemplateHeader").html()),
        change: function () {
            var value = this.value();
            $("#gridKhoThanhPhamDangSanXuat").data("kendoGrid").dataSource.read({ DonHangID: value });
        }
    });
});

