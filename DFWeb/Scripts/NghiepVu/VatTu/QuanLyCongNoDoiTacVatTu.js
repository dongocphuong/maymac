﻿function getNoidungGroupLichSuNhap(value) {
    var datasource = $("#gridChiTietPhieuNhapVatTu").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i == 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung != group[i - 1].NoiDung) {
                        result += " & " + group[0].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};

$(document).ready(function () {
    var currentController = "/QuanLyCongNoDoiTacVatTu";
    var dateNow = new Date();

    
    $("#gridCongNoDoiTacVatTu").kendoGrid({
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: pageableAll,
        height: window.innerHeight * 0.78,
        width: "100%",
        toolbar: [
            {
                template: "<div class='pull-left'>" + GetTextLanguage('tungay') + ": <input id='ipTuNgay' />  " + GetTextLanguage('denngay') + ": <input id='ipDenNgay' /></div>"
            }
        ],
        columns: [
            {
                title: GetTextLanguage("stt"),
                template: "<span class='stt'></span>",
                width: 60,
                align: "center"
            },
            {
                title: GetTextLanguage("tendoitac"),
                field: "TenDoiTac",
                attributes: alignLeft,
                filterable: FilterInTextColumn
            },
            {
                title: GetTextLanguage("giatriphatsinh"),
                field: "GiaTriPhatSinh",
                template: formatToMoney("GiaTriPhatSinh"),
                attributes: alignRight,
                filterable: FilterInColumn
            },
            {
                title: GetTextLanguage("tonggiatri"),
                field: "TongGiaTri",
                template: formatToMoney("TongGiaTri"),
                attributes: alignRight,
                filterable: FilterInColumn,
                //footerTemplate: "<div style='text-align:right;color:red'>#=kendo.toString(sum,'n2')#</div>"
            }
        ],
        dataBound: function (e) {
            var rows = this.items();
            var dem = 0;
            $(rows).each(function () {
                dem++;
                var rowLabel = $(this).find(".stt");
                $(rowLabel).html(dem);
                if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                    dem = 0;
                }
            });
        },
    });
    console.log(dateNow.getDate())
    $("#ipTuNgay").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: dateNow.getDate() + "/" + (dateNow.getMonth() + 1) + "/" + (dateNow.getFullYear() - 1),
        open: function () {
            var denngay = $("#ipDenNgay").data("kendoDatePicker").value();
            if (denngay) {
                $("#ipTuNgay").data("kendoDatePicker").max(denngay);
            }
        },
        change: function () {
            var tungay = $("#ipTuNgay").val();
            var denngay = $("#ipDenNgay").val();
            if (tungay) {
                if (denngay) {
                    $("#gridCongNoDoiTacVatTu").data("kendoGrid").dataSource.read({ TuNgay: tungay, DenNgay: denngay });
                }
            }
        }
    });
    
    $("#ipDenNgay").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date(),
        open: function () {
            var tungay = $("#ipTuNgay").val();
            if (tungay) {
                $("#ipDenNgay").val();
            }
        },
        change: function () {
            var tungay = $("#ipTuNgay").val();
            var denngay = $("#ipDenNgay").val();
            if (tungay) {
                if (denngay) {
                    $("#gridCongNoDoiTacVatTu").data("kendoGrid").dataSource.read({ TuNgay: kendo.toString(tungay, "dd/MM/yyyy"), DenNgay: kendo.toString(denngay, "dd/MM/yyyy") });
                }
            }
        }
    });
    setDataCongNoDoiTacVattu();
    function setDataCongNoDoiTacVattu() {
        var dataSourceCongNoDoiTacVatTu = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetDataCongNoDoiTacVatTu",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        TuNgay: $("#ipTuNgay").val(),
                        DenNgay: $("#ipDenNgay").val()
                    }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "DoiTacID",
                    fields: {
                        DoiTacID: { type: "string" },
                        TenDoiTac: { type: "string" },
                        GiaTriPhatSinh: { type: "number" },
                        TongGiaTri: { type: "number" }
                    }
                }
            },
            aggregate: [{ field: "TongGiaTri", aggregate: "sum" }]
        });
        $("#gridCongNoDoiTacVatTu").data("kendoGrid").setDataSource(dataSourceCongNoDoiTacVatTu);
    }
    $("#gridCongNoDoiTacVatTu .k-grid-content").on("dblclick", "td", function () {
        CreateModalWithSize("mdlChiTietCongNoDoiTacVatTu", "90%", "90%", "tplChiTietCongNoDoiTacVatTu", "Chi Tiết");
        var dataItem = $("#gridCongNoDoiTacVatTu").data("kendoGrid").dataItem($(this).closest("tr"));
        //Tạo Grid LichSuNhapVatTuKhoTong
        var dataChiTietPhieuNhapVatTu = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetDataChiTietNhapVatTuTheoDoiTac",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        TuNgay: $("#ipTuNgay").val(),
                        DenNgay: $("#ipDenNgay").val(),
                        DoiTacID: dataItem.DoiTacID
                    }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "ChiTietNhapXuatVatTuID",
                    fields: {
                        PhieuNhapVatTuID: { type: "string" },
                        ThoiGian: { type: "date" }
                        //NhanVienNhap
                        //SoLuong
                        //DonGia
                        //ThanhTien
                        //TenDonVi
                        //TenVatTu
                        //GhiChu
                        //NoiDung
                    }
                }
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
        });
        $("#gridChiTietPhieuNhapVatTu").kendoGrid({
            dataSource: dataChiTietPhieuNhapVatTu,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: window.innerHeight * 0.9 - 100,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"),
                    attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: formatToDateTime("ThoiGian"),
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuNhap(value)#</scpan>",
                },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(DonGia,'n2') #",
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                    attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(ThanhTien,'n2') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                },
                {
                    field: "NhanVienNhap", title: GetTextLanguage("nhanviennhap"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                //{
                //    field: "NoiDung", title: GetTextLanguage("noidung"), filterable: FilterInColumn, width: 150
                //},
                {
                    field: "GhiChu", title: GetTextLanguage("ghichu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                }
            ],
            dataBound: function (e) {
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });
        
    });
})