﻿function getNoidungGroupLichSuNhap(value) {
    var datasource = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[i].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupLichSuNhap2(value) {
    var datasource = $("#gridtheophieu").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[i].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupLichSuXuat(value) {
    var datasource = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (group[i].LoaiHinhXuat === 2) {
                    //Điều Chuyển
                    if (i === 0) result = GetTextLanguage("dieuchuyendencongtrinh") + " : " + group[i].CongTrinhDen + " & " + GetTextLanguage("doithicong") + " : " + group[i].DoiThiCongDen;
                    else {
                        if (group[i].CongTrinhDen !== group[i - 1].CongTrinhDen && group[i].DoiThiCongDen !== group[i - 1].DoiThiCongDen) {
                            result += " & " + GetTextLanguage("dieuchuyendencongtrinh") + " : " + group[i].CongTrinhDen + " & " + GetTextLanguage("doithicong") + " : " + group[i].DoiThiCongDen;
                        }
                    }
                }
                if (group[i].LoaiHinhXuat === 3) {
                    result = "Trả lại đối tác";
                }
                else {
                    //Sử Dụng
                    result = GetTextLanguage("xuatsudung");
                }
            }
        }
    });
    return result;
};
$(document).ready(function () {
    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });

    var currentController = "/QuanLyVatTuKhoTong";

    var dataSourceKhoTongVatTu = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: currentController + "/GetDataVatTuKhoTong",
                    dataType: 'json',
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                }
            },
            batch: true,
            pageSize: 20,
            sort: [{ field: "TenVatTu", dir: "asc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "VatTuID",
                    fields: {
                        VatTuID: { type: "string" },
                        TenVatTu: { type: "string" },
                        TenDonVi: { type: "string" },
                        TonHienTai: { type: "number" },
                        GhiChu: { type: "string" }
                    },
                }
            },
        });

    $("#gridQuanLyVatTuKhoTong").kendoGrid({
        dataSource: dataSourceKhoTongVatTu,
        height: window.innerHeight * 0.80,
        width: "100%",
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
             {
                 field: "DsHinhAnh", title: "Ảnh",
                 attributes: { style: "text-align:left;" },
                 filterable: FilterInTextColumn,
                 template: kendo.template($("#tplAnh").html()),
                 width: 70
             },
           {
                   field: "Lever",
                   title: "Loại nguyên liệu",
                   filterable: FilterInColumn,
                   attributes: alignLeft,
                   template: kendo.template($("#tpllerver").html()),
                   width: 200
            },
            {
                field: "TenVatTu",
                title: "Tên nguyên liệu",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200
            },
            {
                field: "TenVietTat",
                title: "Mã",
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 150
            },
            {
                field: "TenDonVi",
                title: GetTextLanguage("tendonvi"),
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 100
            },
            {
                field: "NhapDauTu",
                title: GetTextLanguage("nhapdautu"),
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("NhapDauTu"),
                width: 100
            },
            {
                field: "NhapDieuChuyen",
                title: GetTextLanguage("nhapdieuchuyen"),
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("NhapDieuChuyen"),
                width: 100
            },
            {
                field: "SoLuongXuat",
                title: GetTextLanguage("tongxuat"),
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongXuat"),
                width: 100
            },
            {
                field: "SoLuongTon",
                title: GetTextLanguage("tonhientai"),
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongTon"),
                width: 100
            },
          
        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });

    $("#btnNhapVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlNhapVatTuKhoTong", "95%", "90%", null, GetTextLanguage("nhapvattukhotong"));
        $("#mdlNhapVatTuKhoTong").load(currentController + "/PopupNhapVatTuKhoTong");
    });

    $("#btnXuatVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlXuatVatTuKhoTong", "95%", "90%", null, "Xuất Vật Tư Kho Tổng");
        $("#mdlXuatVatTuKhoTong").load(currentController + "/PopupXuatVatTuKhoTong");
    });

    $("#btnLichSuNhapVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlLichSuNhapVatTuKhoTong", "99%", "90%", "tplLichSuNhapVatTuKhoTong", GetTextLanguage("lichsunhapvattukhotong"));
        var dataLoaiHinhNhap = [
            { text: GetTextLanguage("tatca"), value: "0" },
            { text: GetTextLanguage("dieuchuyen"), value: "1" },
            { text: GetTextLanguage("mua"), value: "2" }
        ];
        $("#ipLoaiHinhNhap").kendoDropDownList({
            dataSource: dataLoaiHinhNhap,
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var value = this.value();
                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
            }
        })
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y, m -1, d), change: function () {
                var value = this.value();
                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay").val() });
            }
        });
        $("#inputDenNgay").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridLichSuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });
        //Tạo Grid LichSuNhapVatTuKhoTong
        var dataLichSuNhapVatTuKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetDataLichSuNhapVatTuKhoTong",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: { LoaiHinhNhap: 0, TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "ChiTietNhapXuatVatTuID",
                    fields: {
                        PhieuNhapVatTuID: { type: "string" },
                        ThoiGian: { type: "date" }
                        //NhanVienNhap
                        //SoLuong
                        //DonGia
                        //ThanhTien
                        //TenDonVi
                        //TenVatTu
                        //GhiChu
                        //NoiDung
                    }
                }
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
        });
        $("#gridLichSuNhapVatTuKhoTong").kendoGrid({
            dataSource: dataLichSuNhapVatTuKhoTong,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: innerHeight * 0.7,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"),
                    attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: formatToDateTime("ThoiGian"),
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuNhap(value)#</scpan>",
                },
                 {
                     field: "DsHinhAnh", title: "Ảnh",
                     attributes: { style: "text-align:left;" },
                     filterable: FilterInTextColumn,
                     template: kendo.template($("#tplAnh").html()),
                     width: 70
                 },
                 {
                     field: "MaPhieu", title: GetTextLanguage("maphieu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                 },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 150
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(DonGia,'n2') #",
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                    attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(ThanhTien,'n2') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                },
                //{
                //    field: "NhanVienNhap", title: GetTextLanguage("nhanviennhap"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                //},
                 {
                     field: "DoiTac", title: GetTextLanguage("doitac"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                 },
                //{
                //    field: "NoiDung", title: GetTextLanguage("noidung"), filterable: FilterInColumn, width: 150
                //},
                {
                    field: "GhiChu", title: GetTextLanguage("ghichu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                }
            ],
            dataBound: function (e) {
                showslideimg10();
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });
        $("#gridLichSuNhapVatTuKhoTong .k-grid-content").on("dblclick", "td", function () {
            var rowct = $(this).closest("tr"),
                   gridct = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid"),
                   dataItem = gridct.dataItem(rowct);
            if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                var LoaiNhap = 1;
                if (dataItem.DoiTac != null && dataItem.DoiTac != "" || (dataItem.CongTrinhDieuChuyenID === "ffffffff-ffff-ffff-ffff-ffffffffffff" && dataItem.DoiThiCongDieuChuyenID === 'ffffffff-ffff-ffff-ffff-ffffffffffff')) {
                    LoaiNhap = 2;
                }
                CreateModalWithSize("mdlChiTietPhieuNhap", "90%", "90%", null, "Chi Tiết Phiếu Nhập");
                $("#mdlChiTietPhieuNhap").load(currentController + "/PopupChiTietPhieuNhapVatTu?PhieuNhapVatTuID=" + dataItem.PhieuNhapVatTuID + "&LoaiNhap=" + LoaiNhap + "&PhieuXuatVatTuID=" + dataItem.PhieuXuatVatTuID);
            }
        })
        //$("#gridLichSuNhapVatTuKhoTong").on("click", "tr .xemchitiet", function () {
        //    var dataItem = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid").dataItem($(this).closest("tr"));
        //    var LoaiNhap = 1;
        //    if (dataItem.CongTrinhDieuChuyenID === null && dataItem.DoiThiCongDieuChuyenID === null) {
        //        LoaiNhap = 2;
        //    }
        //    CreateModalWithSize("mdlChiTietPhieuNhap", "90%", "90%", null, "Chi Tiết Phiếu Nhập");
        //    $("#mdlChiTietPhieuNhap").load(currentController + "/PopupChiTietPhieuNhapVatTu?PhieuNhapVatTuID=" + dataItem.PhieuNhapVatTuID + "&LoaiNhap=" + LoaiNhap + "&PhieuXuatVatTuID=" + dataItem.PhieuXuatVatTuID);

        //});

        $("#mdlLichSuNhapVatTuKhoTong .xuat-excel").click(function () {
            var filer = [];
            var filers = $("#gridLichSuNhapVatTuKhoTong").data("kendoGrid").dataSource.filter();
            if (filers != null && filers!=undefined) {
                for (var i = 0; i < filers.filters.length; i++) {
                    filers.filters[i].Operator = filers.filters[i].operator;
                    filer.push(filers.filters[i]);
                }
            }
            location.href = currentController + '/XuatExcelLichSuNhapVatTuKhoTong?filters=' + JSON.stringify(filer) + '&LoaiHinhNhap=' + $("#ipLoaiHinhNhap").val() + '&TuNgay=' + $("#inputTuNgay").val() + '&DenNgay=' + $("#inputDenNgay").val();
        });
    });

    $("#btnLichSuXuatVatTuKhoTong").on("click", function () {
        CreateModalWithSize("mdlLichSuXuatVatTuKhoTong", "99%", "90%", "tplLichSuXuatVatTuKhoTong", "Lịch sử xuất vật tư kho tổng");
        var dataLoaiHinhXuat = [
            { text: GetTextLanguage("tatca"), value: "0" },
            { text: GetTextLanguage("xuatsudung"), value: "1" },
            { text: GetTextLanguage("xuatdieuchuyen"), value: "2" }
        ];
        $("#ipLoaiHinhXuat").kendoDropDownList({
            dataSource: dataLoaiHinhXuat,
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var value = this.value();
                $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource.read({ LoaiHinhXuat: value, CongTrinhDenID: $("#ipCongTrinhDen").data("kendoDropDownList").value(), DoiThiCongDenID: $("#ipDoiThiCongDen").data("kendoDropDownList").value(), TuNgay: $("#inputTuNgay2").val(), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        $(".lst-modal").popuptablefilters();
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay2").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                var value = this.value();
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhDenID:$("#ipCongTrinhDen").val(),DoiThiCongDenID:$("#ipDoiThiCongDen").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        $("#inputDenNgay2").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(),CongTrinhDenID:$("#ipCongTrinhDen").val(),DoiThiCongDenID:$("#ipDoiThiCongDen").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });
        $("#ipCongTrinhDen").kendoDropDownList({
            dataSource: getDataForDropdownlist(currentController + "/GetAllDataCongTrinh"),
            dataTextField: "TenDuAn",
            dataValueField: "DuAnID",
            optionLabel: GetTextLanguage("choncongtrinh"),
            change: function () {
                var value = this.value();
                $("#ipDoiThiCongDen").data("kendoDropDownList").setDataSource(getDataForDropdownlistWithParam(currentController + "/GetAllDataDoiThiCong", { CongTrinhID: value }));
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhDenID: $("#ipCongTrinhDen").val(), DoiThiCongDenID: $("#ipDoiThiCongDen").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        $("#ipDoiThiCongDen").kendoDropDownList({
            dataSource: getDataForDropdownlistWithParam(currentController + "/GetAllDataDoiThiCong", { CongTrinhID: $("#ipCongTrinhDen").data("kendoDropDownList").value() }),
            dataTextField: "TenDoi",
            dataValueField: "DoiThiCongID",
            optionLabel: GetTextLanguage("chondoithicong"),
            change: function () {
                var value = this.value();
                gridReload("gridLichSuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhDenID: $("#ipCongTrinhDen").val(), DoiThiCongDenID: $("#ipDoiThiCongDen").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        //Tạo Grid LichSuXuatVatTuKhoTong
        var dataLichSuXuatVatTuKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetLichSuXuatVatTuKhoTong",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        LoaiHinhXuat: 0,
                        CongTrinhDenID: $("#ipCongTrinhDen").val(),
                        DoiThiCongDenID: $("#ipDoiThiCongDen").val(),
                        TuNgay: $("#inputTuNgay2").val(),
                        DenNgay: $("#inputDenNgay2").val()
                    }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total",
                //model: {
                //    id: "",
                //    field: {
                //        PhieuXuatVatTuID,
                //        ThoiGian,
                //        LoaiHinhXuat,
                //        TenVatTu,
                //        TenVietTat,
                //        TenDonVi,
                //        SoLuong,
                //        DonGia,
                //        ThanhTien,
                //        CongTrinhDen,
                //        DoiThiCongDen,
                //        NhanVienNhap
                //    }
                //}
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
        });
        $("#gridLichSuXuatVatTuKhoTong").kendoGrid({
            dataSource: dataLichSuXuatVatTuKhoTong,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: window.innerHeight * 0.9 - 100,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuXuat(value)#</span>",
                },
                 {
                     field: "DsHinhAnh", title: "Ảnh",
                     attributes: { style: "text-align:left;" },
                     filterable: FilterInTextColumn,
                     template: kendo.template($("#tplAnh").html()),
                     width: 70
                 },
                 {
                     field: "MaPhieu", title: GetTextLanguage("maphieu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                 },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(DonGia,'n2') #"
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#=kendo.toString(ThanhTien,'n2') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "NhanVienNhap", title: GetTextLanguage("nguoixuat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
               
            ],
            dataBound: function (e) {
                showslideimg10();
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });
        $("#gridLichSuXuatVatTuKhoTong .k-grid-content").on("dblclick", "td", function () {
            var rowct = $(this).closest("tr"),
                   gridct = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid"),
                   dataItem = gridct.dataItem(rowct);
            if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                CreateModalWithSize("mdlChiTietPhieuXuat", "90%", "90%", null, GetTextLanguage("chitietphieuxuat"));
                $("#mdlChiTietPhieuXuat").load(currentController + "/PopupXuatVatTuKhoTong?PhieuXuatVatTuID=" + dataItem.PhieuXuatVatTuID + "&LoaiXuat=" + dataItem.LoaiHinhXuat);
            }
        })
        //$("#gridLichSuXuatVatTuKhoTong").on("click", "tr .xemchitiet", function () {
        //    var dataItem = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataItem($(this).closest("tr"));
        //    CreateModalWithSize("mdlChiTietPhieuXuat", "90%", "90%", null, GetTextLanguage("chitietphieuxuat"));
        //    $("#mdlChiTietPhieuXuat").load(currentController + "/PopupXuatVatTuKhoTong?PhieuXuatVatTuID=" + dataItem.PhieuXuatVatTuID + "&LoaiXuat=" + dataItem.LoaiHinhXuat);
        //});
        $("#mdlLichSuXuatVatTuKhoTong .xuat-excel").click(function () {
            var filer = [];
            var filers = $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource.filter();
            if (filers != null && filers != undefined) {
                for (var i = 0; i < filers.filters.length; i++) {
                    filers.filters[i].Operator = filers.filters[i].operator;
                    filer.push(filers.filters[i]);
                }
            }
            location.href = currentController + '/XuatExcelLichSuXuatVatTuKhoTong?filters=' + JSON.stringify(filer) + '&LoaiHinhXuat=' + $("#ipLoaiHinhXuat").val() + '&TuNgay=' + $("#inputTuNgay2").val() + '&DenNgay=' + $("#inputDenNgay2").val() + '&CongTrinhDenID=' + $("#ipCongTrinhDen").val() + '&DoiThiCongDenID=' + $("#ipDoiThiCongDen").val();
        });
    });
    $(".tab_tonkhotheogia").click(function () {
        $("#gridtheogia").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/TonVatTuTheoGia",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
                sort: [{ field: "TenVatTu", dir: "asc" }],
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        fields: {
                            TenVatTu: { type: "string" },
                            TenDonVi: { type: "string" },
                            NhapDieuChuyen: { type: "number" },
                            NhapDauTu: { type: "number" },
                            SoLuongXuat: { type: "number" },
                            SoLuongTon: { type: "number" },
                            GhiChu: { type: "string" }
                        },
                    }
                },
            }),
            height: window.innerHeight * 0.80,
            width: "100%",
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            dataBinding: function () {
                record = 0;
            },
            pageable: pageableAll,
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "#= ++record #",
                    width: 60,
                    attributes: alignCenter
                },
                {
                    field: "TenVatTu",
                    title: GetTextLanguage("tenvattu"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft,
                    width: 200
                },
                {
                    field: "TenVietTat",
                    title: GetTextLanguage("tenviettat"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft,
                    width: 200
                },
                {
                    field: "TenDonVi",
                    title: GetTextLanguage("tendonvi"),
                    filterable: FilterInTextColumn,
                    attributes: alignCenter,
                    width: 150
                },
                 {
                     field: "DonGia",
                     title: "Đơn giá",
                     filterable: FilterInColumn,
                     attributes: alignRight,
                     format: "{0:n2}",
                     width: 200,
                     filterable: {
                         cell: {
                             template: function (args) {
                                 // create a DropDownList of unique values (colors)
                                 args.element.kendoDropDownList({
                                     dataSource: [{ id: "0-50000", text: "0-50,000" },
                                         { id: "50001-100000", text: "50,001-200,000" },
                                         { id: "100001-500000", text: "100,001-500,000" },
                                      { id: "500001-1000000", text: "500,000-1,000,000" },
                                      { id: "1000001-2000000", text: "1,000,001-2,000,000" },
                                      { id: "2000001-2000000000", text: ">2,000,000" }],
                                     dataTextField: "text",
                                     dataValueField: "id",
                                     optionLabel: "Tất cả",
                                     valuePrimitive: true
                                 });
                             },
                             showOperators: false,
                             operator: "doesnotcontain",
                             suggestionOperator: "doesnotcontain"
                         },

                     },
                 },
                {
                    field: "NhapDauTu",
                    title: GetTextLanguage("nhapdautu"),
                    filterable: FilterInColumn,
                    attributes: alignRight,
                    format: "{0:n2}",
                    width: 100
                },
                {
                    field: "NhapDieuChuyen",
                    title: GetTextLanguage("nhapdieuchuyen"),
                    filterable: FilterInColumn,
                    attributes: alignRight,
                    format: "{0:n2}",
                    width: 100
                },
                {
                    field: "SoLuongXuat",
                    title: GetTextLanguage("tongxuat"),
                    filterable: FilterInColumn,
                    attributes: alignRight,
                    format: "{0:n2}",
                    width: 100
                },
                {
                    field: "SoLuongTon",
                    title: GetTextLanguage("tonhientai"),
                    filterable: FilterInColumn,
                    attributes: alignRight,
                    format:"{0:n2}",
                    width: 100
                },
               
            ],
        });

    });

    $(".tab_tonkhotheophieu").click(function () {
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay5").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y - 1, m, d), change: function () {
                var value = this.value();
                gridReload("gridtheophieu", { TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay5").val() });
            }
        });
        $("#inputDenNgay5").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridtheophieu", { TuNgay: $("#inputTuNgay5").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });
        $("#gridtheophieu").kendoGrid({
            dataSource: new kendo.data.DataSource({
                    transport: {
                        read: {
                            url: currentController + "/TonVatTuTheoPhieu",
                            dataType: "json",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            data: {
                                TuNgay: $("#inputTuNgay5").val(),
                                DenNgay: $("#inputDenNgay5").val()
                            }
                        },
                        parameterMap: function (options, type) {
                            return JSON.stringify(options);
                        }
                    },
                    type: "json",
                    group: {
                        field: "ThoiGian",
                        dir: "desc",
                        aggregates: [{
                            field: "ThanhTien",
                            aggregate: "sum"
                        }]
                    },
                    aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
                    sort: [{ field: "TenVatTu", dir: "asc" }],
                    schema: {
                        data: "data",
                        total: "total",
                        model: {
                            fields: {
                                TenVatTu: { type: "string" },
                                TenDonVi: { type: "string" },
                                TonHienTai: { type: "number" },
                                GhiChu: { type: "string" }
                            },
                        }
                    },
                }),

            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: innerHeight * 0.7,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"),
                    attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: formatToDateTime("ThoiGian"),
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuNhap2(value)#</scpan>",
                },
                 {
                     field: "MaPhieu", title: GetTextLanguage("maphieu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                 },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 150
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 100
                },
                {
                    field: "SoLuong", title: "Số lượng nhập", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                },
                {
                    field: "SoLuongXuat", title: "Số lượng xuất", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuongXuat,'n2') #",
                },
                  {
                      field: "SoLuongTon", title: "Số lượng tồn", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                      template: "#= kendo.toString(SoLuongTon,'n2') #",
                  },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, width: 200,
                    template: "#= kendo.toString(DonGia,'n2') #",
                    filterable: {
                        cell: {
                            template: function (args) {
                                // create a DropDownList of unique values (colors)
                                args.element.kendoDropDownList({
                                    dataSource: [{ id: "0-50000", text: "0-50,000" },
                                        { id: "50001-100000", text: "50,001-200,000" },
                                        { id: "100001-500000", text: "100,001-500,000" },
                                     { id: "500001-1000000", text: "500,000-1,000,000" },
                                     { id: "1000001-2000000", text: "1,000,001-2,000,000" },
                                     { id: "2000001-2000000000", text: ">2,000,000" }],
                                    dataTextField: "text",
                                    dataValueField: "id",
                                    optionLabel: "Tất cả",
                                    valuePrimitive: true
                                });
                            },
                            showOperators: false,
                            operator: "doesnotcontain",
                            suggestionOperator: "doesnotcontain"
                        },

                    },
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                    attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(ThanhTien,'n2') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                },
                 {
                     field: "DoiTac", title: GetTextLanguage("doitac"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                 },
               
            ],
            dataBound: function (e) {
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });

      
    })
});

