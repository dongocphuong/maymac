﻿function getNoidungGroupLichSuNhap3(value) {
    var datasource = $("#gridDanhSachVatTu").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[i].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
$(document).ready(function () {
    var currentController = "/QuanLyVatTuKhoTong";
    var LoaiHinh = 2;//Xuất Điều Chuyển
    var dataimg = new FormData();

    $("#btnXuatDieuChuyen").click(function () {
        LoaiHinh = 2;
        $("#btnXuatDieuChuyen").attr("class", "btn btn-success");
        $("#btnXuatSuDung").attr("class", "btn btn-default");
        $("#grNhan").show();
    });
    $("#btnXuatSuDung").click(function () {
        LoaiHinh = 1;
        $("#btnXuatSuDung").attr("class", "btn btn-success");
        $("#btnXuatDieuChuyen").attr("class", "btn btn-default");
        $("#grNhan").hide();
    });
    if ($("#ipPhieuXuatVatTuID").val() != null && $("#ipPhieuXuatVatTuID").val() != "" && ($("#CongTrinhID").val() == null || $("#CongTrinhID").val() == "")) {
        LoaiHinh = 1;
        $("#btnXuatSuDung").attr("class", "btn btn-success");
        $("#btnXuatDieuChuyen").attr("class", "btn btn-default");
        $("#grNhan").hide();
    }
    $(".lst-modal").popuptablefilters();
    var dataSourceDonVi = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/Home" + "/DSCongTrinhCoCongTrinhTong",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8"
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    });
    var dataSourceKhoTong = new kendo.data.DataSource({
        transport: {
            read: {
                url: currentController + "/GetAllDataCongTrinh",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8"
            }
        },
        schema: {
            type: "json",
            data: "data"
        }
    })
    var iskhodonvi = $("#gridQuanLyVatTuKhoDonVi").length > 0;
    $("#drdCongTrinhNhan").kendoDropDownList({
        dataSource: iskhodonvi ? dataSourceDonVi : dataSourceKhoTong,
        filter: "contains",
        dataTextField: iskhodonvi ? "TenCongTrinh" : "TenCongTrinh",
        dataValueField: iskhodonvi ? "CongTrinhID" : "CongTrinhId",
        optionLabel: GetTextLanguage("choncongtrinh"),
        value: $("#CongTrinhID").val(),
        enable: !iskhodonvi,
        change: function () {
            var value = this.value();
            $("#drdDoiThiCongNhan").data("kendoDropDownList").dataSource.read({ CongTrinhID: value });
        }
    });
    $(".lst-modal").popuptablefilters();
    $("#drdDoiThiCongNhan").kendoDropDownList({
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetAllDataDoiThiCong",
                    dataType: "json",
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        CongTrinhID: $("#drdCongTrinhNhan").data("kendoDropDownList").value()
                    }
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        }),
        filter: "contains",
        dataTextField: "TenDoi",
        value: $("#DoiID").val(),
        dataValueField: "DoiThiCongID",
        enable: !iskhodonvi,
        optionLabel: GetTextLanguage("chondoithicong")
    });
    if ($("#ThoiGianXuat").data("kendoDateTimePicker") === undefined) {
        $("#ThoiGianXuat").kendoDateTimePicker({
            format: "dd/MM/yyyy HH:mm",
        });
    }
    if ($("#drdDoiTac").data("kendoDropDownList") === undefined) {
        $("#drdDoiTac").kendoDropDownList({
            autoBind: false,
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/GetAllDataDoiTac",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8"
                    }
                },
                schema: {
                    type: "json",
                    data: "data"
                }
            }),
            dataTextField: "TenDoiTac",
            dataValueField: "DoiTacID",
            optionLabel: GetTextLanguage("chondoitac")
        });
    }
    var FileDinhKem = [];
    if ($("#gridChiTietPhieuXuatVatTu").data("kendoGrid") === undefined) {
        $("#gridChiTietPhieuXuatVatTu").kendoGrid({
            dataSource: new kendo.data.DataSource({
                schema: {
                    model: {
                        id: "ChiTietNhapXuatVatTuID",
                        fields: {
                            VatTuID: { editable: false },
                            TenDonVi: { type: "string", editable: false },
                            TenVatTu: { type: "string", editable: false },
                            TenVietTat: { type: "string", editable: false },
                            TonHienTai: { type: "number", editable: false },
                            ThanhTien: { type: "number", editable: false },
                            SoLuong: { type: "number", editable: true, validation: { min: 0, format: "n2" } },
                            DonGia: { type: "number", editable: false},
                            SoLuongTon: { type: "number", editable: false,},
                        }
                    }
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
            }),
            height: window.innerHeight * 0.5,
            filterable: {
                mode: "row"
            },
            editable: true,
            sortable: true,
            resizable: true,
            width: "100%",
            dataBound: function (e) {
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                });
            },
            columns:
            [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                { field: "VatTuID", hidden: true },
               
                {
                    field: "TenVatTu", width: 200,
                    title: GetTextLanguage("tenvattu"),
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInColumn
                },
                {
                    field: "TenVietTat",
                    width: 200,
                    attributes: { style: "text-align:left;" },
                    title: GetTextLanguage("tenviettat"),
                    filterable: FilterInTextColumn
                },
                {
                    field: "TenDonVi", width: 200,
                    title: GetTextLanguage("donvi"),
                    attributes: { "style": "text-align:left !important;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"),
                    format: "{0:n2}",
                    attributes: { style: "text-align:right;" },
                    filterable: FilterInColumn, width: 150,
                    editor: function (container, options) {
                        container.text(kendo.toString(options.model.DonGia, "n2"));
                    }
                  
                },
                {
                    field: "SoLuong", width: 100,
                    title: GetTextLanguage("soluong"),
                    format: "{0:n2}",
                    attributes: { "style": "text-align:right !important;" },
                    filterable: FilterInColumn,
                    editor: function (container, options) {
                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                        var input = $('<input type="text" style="width:75%;height:15px;" data-v-min="0" class="form-control" id="' + options.model.get("VatTuID") + '" name="SoLuong" />');
                        input.appendTo(container);
                        input.autoNumeric(autoNumericOptionsSoLuong);
                    },
                },
                {
                    field: "TonHienTai", width: 100,
                    title: GetTextLanguage("tonhientai"),
                    format: "{0:n2}",
                    attributes: { "style": "text-align:right !important;" },
                    filterable: FilterInColumn,
                    editor: function (container, options) {
                        container.text(kendo.toString(options.model.TonHienTai, "n2"));
                    }
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), format: "{0:n2}",
                    attributes: { "style": "text-align:right !important;" },
                    filterable: FilterInColumn, width: 150,
                    footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n2')#</div>",
                    editor: function (container, options) {
                        container.text(kendo.toString(options.model.ThanhTien, "n2"));
                    }
                },
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                    command: [
                        {
                            text: GetTextLanguage("xoa"),
                            name: "destroy"
                        }
                    ],
                    width: 100,
                    hidden: $("#ipPhieuXuatVatTuID").val() != null && $("#ipPhieuXuatVatTuID").val() != ""
                },
            ],
            save: function (e) {
                var items = $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").dataSource.data();
                //if (e.values.DonGia != null) {
                //    for (var i = 0; i < items.length; i++) {
                //        if (items[i].VatTuID == e.model.VatTuID) {
                //            items[i].ThanhTien = (e.values.DonGia == null ? 1 : e.values.DonGia) * (e.model.SoLuong == null ? 1 : e.model.SoLuong);
                //            items[i].DonGia = e.values.DonGia;
                //            break;
                //        }
                //    }
                //} else
                    if (e.values.SoLuong != null) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].VatTuID == e.model.VatTuID) {
                            items[i].ThanhTien = (e.model.DonGia == null ? 1 : e.model.DonGia) * (e.values.SoLuong == null ? 1 : e.values.SoLuong);
                            items[i].SoLuong = e.values.SoLuong;
                            break;
                        }
                    }
                }

                $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").dataSource.data(items);
            }
        });
    }
    if ($("#ipPhieuXuatVatTuID").val() != null && $("#ipPhieuXuatVatTuID").val() != "") {
        $("#btnChonVatTu").closest(".form-group").addClass("hidden");
        var dataSourceChiTietPhieuXuatVatTu = new kendo.data.DataSource(
            {
                transport: {
                    read: {
                        url: currentController + "/GetDataChiTietPhieuXuat",
                        dataType: 'json',
                        type: 'Get',
                        contentType: "application/json; charset=utf-8",
                        data: {
                            PhieuXuatVatTuID: $("#ipPhieuXuatVatTuID").val()
                        }
                    }
                },
                batch: true,
                pageSize: 20,
                sort: [{ field: "TenVatTu", dir: "asc" }],
                schema: {
                    data: 'data',
                    total: 'total',
                    model: {
                        id: "VatTuID",
                        fields: {
                            VatTuID: { type: "string", editable: false },
                            TenVatTu: { type: "string", editable: false },
                            TenVietTat: { type: "string", editable: false },
                            TenDonVi: { type: "string", editable: false },
                            SoLuong: { type: "number", editable: true },
                            DonGia: { type: "number", editable: false },
                            ThanhTien: { type: "number", editable: false },
                            SoLuongTon: { type: "number", editable: false }
                        },
                    }
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
            });

        $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").setDataSource(dataSourceChiTietPhieuXuatVatTu);
    }
    $("#mdlXuatVatTuKhoTong").on("click", ".xoaItemFile", function () {
        var file = $(this).data("file");
        for (var i = 0; i < FileDinhKem.length; i++) {
            if (FileDinhKem[i].name === file) {
                FileDinhKem.splice(i, 1);
                break;
            }
        }
        $(this).parent().remove();
    });
    var arrData = [];
    $("#btnChonVatTu").click(function () {
        CreateModalWithSize("mdlDanhSachVatTu", "95%", "90%", "tplDanhSachVatTu", GetTextLanguage("danhsachvattu"));
        var dataChon = [
            { text: GetTextLanguage("dachon"), value: "true" },
            { text: GetTextLanguage("chuachon"), value: "false" }
        ];
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay6").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y - 1, m, d), change: function () {
                var value = this.value();
                gridReload("gridtheophieu", { TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay6").val() });
            }
        });
        $("#inputDenNgay6").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridtheophieu", { TuNgay: $("#inputTuNgay6").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });
        
        $("#gridDanhSachVatTu").kendoGrid({
            dataSource:  new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/TonVatTuTheoPhieu",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            TuNgay: $("#inputTuNgay6").val(),
                            DenNgay: $("#inputDenNgay6").val()
                        }
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                group: {
                    field: "ThoiGian",
                    dir: "desc",
                    aggregates: [{
                        field: "ThanhTien",
                        aggregate: "sum"
                    }]
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
                sort: [{ field: "TenVatTu", dir: "asc" }],
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        fields: {
                            TenVatTu: { type: "string", editable: false },
                            TenVietTat: { type: "string", editable: false },
                            TenDonVi: { type: "string", editable: false },
                            MaPhieu: { type: "string", editable: false },
                            SoLuong: { type: "number", editable: true },
                            SoLuongTon: { type: "number", editable: false },
                            GhiChu: { type: "string", editable: false },
                            DoiTac: { type: "string", editable: false }
                        },
                    }
                },
            }),

            filterable: {
                mode: "row"
            },
            editable: true,
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: innerHeight * 0.7,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"),
                    attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: formatToDateTime("ThoiGian"),
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuNhap3(value)#</scpan>",
                },
                 {
                     field: "MaPhieu", title: GetTextLanguage("maphieu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                 },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 150
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 100
                },
                 {
                     field: "SoLuong2",
                     width: 100,
                     title: GetTextLanguage("soluong"),
                     format: "{0:n2}",
                     filterable: FilterInColumn,
                     editor: function (container, options) {
                         rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                         var input = $('<input type="text"  style="width:75%;height:15px;" class="form-control" id="' + options.model.get("VatTuID") + '" name="SoLuong2" />');
                         input.appendTo(container);
                         input.autoNumeric(autoNumericOptionsSoLuong);
                     },
                 },
                  {
                      field: "SoLuongTon", title: "Số lượng tồn", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                      template: "#= kendo.toString(SoLuongTon,'n2') #",
                  },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"),
                    attributes: { "style": "text-align:right !important;" },
                    width: 200,
                    format: "{0:n2}",
                    filterable: {
                        cell: {
                            template: function (args) {
                                // create a DropDownList of unique values (colors)
                                args.element.kendoDropDownList({
                                    dataSource: [{ id: "0-50000", text: "0-50,000" },
                                        { id: "50001-100000", text: "50,001-200,000" },
                                        { id: "100001-500000", text: "100,001-500,000" },
                                     { id: "500001-1000000", text: "500,000-1,000,000" },
                                     { id: "1000001-2000000", text: "1,000,001-2,000,000" },
                                     { id: "2000001-2000000000", text: ">2,000,000" }],
                                    dataTextField: "text",
                                    dataValueField: "id",
                                    optionLabel: "Tất cả",
                                    valuePrimitive: true
                                });
                            },
                            showOperators: false,
                            operator: "doesnotcontain",
                            suggestionOperator: "doesnotcontain"
                        },
                       
                    },
                },

                 {
                     field: "DoiTac", title: GetTextLanguage("doitac"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                 },

            ],
            dataBound: function (e) {
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
            },

        });

        

        $("#gridDanhSachVatTu").data("kendoGrid").table.on("click", ".chon", function () {
            var dataItem = $("#gridDanhSachVatTu").data("kendoGrid").dataItem($(this).closest("tr"));
            if ($(this)[0].checked === true) {
                dataItem.Chon = true;
                $("#gridDanhSachVatTu").data("kendoGrid").select($("#gridDanhSachVatTu").data("kendoGrid").table.find("[data-uid=" + dataItem.uid + "]"));
            }
            else {
                dataItem.Chon = false;
                $("#gridDanhSachVatTu").data("kendoGrid").table.find("[data-uid=" + dataItem.uid + "]").attr("class", "");
            }
        });
        $("#gridDanhSachVatTu").data("kendoGrid").table.on("change", ".chon", function () {

        });
        $("#gridDanhSachVatTu").data("kendoGrid").table.on("dblclick", "tr", function () {
            var dataItem = $("#gridDanhSachVatTu").data("kendoGrid").dataItem($(this).closest("tr"));
            if (dataItem.Chon === true) {
                dataItem.Chon = false;
                $("#gridDanhSachVatTu").data("kendoGrid").table.find("[data-uid=" + dataItem.uid + "]").attr("class", "");
            }
            else {
                dataItem.Chon = true;
                //console.log(dataItem);
                //$("#gridDanhSachVatTu").data("kendoGrid").select($("#gridDanhSachVatTu").data("kendoGrid").table.find("[data-uid=" + dataItem.uid + "]"));
            }
        });


        $("#btnHuyChonVatTu").click(function () {
            $("#mdlDanhSachVatTu").data("kendoWindow").close();
        });
        $("#btnTiepTuc").click(function () {
            var arrData = $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").dataSource.data();
            var dataItem = $("#gridDanhSachVatTu").data("kendoGrid").dataSource.data();
            var kiemtraton = true;
            for (var i = 0; i < dataItem.length; i++) {
                if (dataItem[i].DonGia != null && dataItem[i].SoLuong2 > 0) {
                    if (dataItem[i].SoLuong2 > dataItem[i].SoLuongTon) {
                        kiemtraton = false;
                        showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("soluongphainhohonsoluongton") + " : " + dataItem[i].TenVatTu);
                    }
                    else {
                        var isExsit = false;
                        for (var j = 0; j < arrData.length; j++) {
                            if (dataItem[i].ChiTietNhapXuatVatTuID == arrData[j].ChiTietNhapXuatVatTuID) {
                                arrData[j].DonGia = dataItem[i].DonGia;
                                arrData[j].SoLuong = dataItem[i].SoLuong2;
                                arrData[i].TonHienTai = dataItem[i].SoLuongTon;
                                arrData[j].ThanhTien = (arrData[j].DonGia == null ? 1 : arrData[j].DonGia) * (arrData[j].SoLuong == null ? 1 : arrData[j].SoLuong);
                                isExsit = true;
                            }
                        }
                        if (!isExsit) {
                            if (dataItem[i].SoLuong2 > 0) {
                                dataItem[i].ThanhTien = (dataItem[i].DonGia == null ? 1 : dataItem[i].DonGia) * (dataItem[i].SoLuong2 == null ? 1 : dataItem[i].SoLuong2);
                                dataItem[i].TonHienTai = dataItem[i].SoLuongTon;
                                dataItem[i].SoLuong = dataItem[i].SoLuong2;
                                arrData.push(dataItem[i]);
                            }

                        }
                    }
                }
            }
            if (kiemtraton) {
                $("#mdlDanhSachVatTu").data("kendoWindow").close();
                $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").dataSource.data(arrData);
            }
            else {
                //showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("soluongphainhohonsoluongton"));
            }
        });
        
    });
    $("#btnHuyPhieuXuatVatTu").click(function () {
        $("#mdlXuatVatTuKhoTong").data("kendoWindow").close();
    });
    if ($("#mdlChiTietPhieuXuat").text().lenth > 1) {
        showslideimg5($("#mdlChiTietPhieuXuat"));
        $("#mdlChiTietPhieuXuat #files_0").change(function () {
            for (var x = 0; x < this.files.length; x++) {
                dataimg.append("uploads", this.files[x]);
            };
            loadimgSuaChua5(this, $("#mdlChiTietPhieuXuat"));
            $("#mdlChiTietPhieuXuat #files_0").val('').clone(true);
        })
    } else {
        showslideimg5($("#mdlXuatVatTuKhoTong"));
        $("#mdlXuatVatTuKhoTong #files_0").change(function () {
            for (var x = 0; x < this.files.length; x++) {
                dataimg.append("uploads", this.files[x]);
            };
            loadimgSuaChua5(this, $("#mdlXuatVatTuKhoTong"));
            $("#mdlXuatVatTuKhoTong #files_0").val('').clone(true);
        })
    }
    $("#btnDongYXuat").click(function () {
        var hinhanh = "";
        var lists = $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").dataSource.data();
        if (LoaiHinh == 2) {
            if ($("#drdCongTrinhNhan").val() == "") {
                showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("choncongtrinh"));
                return false;
            }
            else if ($("#drdDoiThiCongNhan").val() == "") {
                showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("chondoithicong"));
                return false;
            }
            if (lists.length <= 0) {
                showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("chonvattu"));
                return false;
            }
            for (var i = 0; i < lists.length; i++) {
               
                if (lists[i].SoLuong == null || lists[i].SoLuong == undefined) {
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("nhapsoluong"));
                    return false;
                }
            }
        }
        else {
            for (var i = 0; i < lists.length; i++) {
              
                if (lists[i].SoLuong == null || lists[i].SoLuong == undefined) {
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("nhapsoluong"));
                    return false;
                }
            }
            if (lists.length <= 0) {
                showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("chonvattu"));
                return false;
            }
        }
        var ins = dataimg.getAll("uploads").length;
        var DSHinhAnh = [];
        $('.dshinhanhadd .imgslider').each(function (index2, element2) {
            if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
            }
        });
        if (ins > 0) {
            if ($("#mdlChiTietPhieuXuat").text().lenth > 1) {
                var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", dataimg.getAll("uploads"), $(".dshinhanhadd", $('#mdlChiTietPhieuXuat'))).split(',');
                for (var k = 0; k < strUrl.length; k++) {
                    DSHinhAnh.push({ Url: strUrl[k] });
                }
            } else {
                var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", dataimg.getAll("uploads"), $(".dshinhanhadd", $('#mdlXuatVatTuKhoTong'))).split(',');
                for (var k = 0; k < strUrl.length; k++) {
                    DSHinhAnh.push({ Url: strUrl[k] });
                }
            }
        }
        for (var i = 0; i < DSHinhAnh.length; i++) {
            if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                hinhanh += DSHinhAnh[i].Url + ",";
            }
        }
        if ($('#mdlChiTietPhieuXuat').text().length > 1) {
            $("#mdlChiTietPhieuXuat").data("kendoWindow").close();
            $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource.read();
            $("#gridQuanLyVatTuKhoTong").data("kendoGrid").dataSource.read();
        } else {
            $("#mdlXuatVatTuKhoTong").data("kendoWindow").close();
            $("#gridQuanLyVatTuKhoTong").data("kendoGrid").dataSource.read();
        }
       
        $.ajax({
            url: currentController + "/XuatVatTu",
            type: "POST",
            data: {
                PhieuXuatVatTuID: $("#ipPhieuXuatVatTuID").val(),
                LoaiHinh: LoaiHinh,
                CongTrinhNhanID: $("#drdCongTrinhNhan").data("kendoDropDownList").value(),
                DoiThiCongNhanID: $("#drdDoiThiCongNhan").data("kendoDropDownList").value(),
                DataChiTietPhieuXuat: JSON.stringify($("#gridChiTietPhieuXuatVatTu").data("kendoGrid").dataSource.data()),
                DSHinhanh: hinhanh.substring(0, hinhanh.length - 1),
                ThoiGian: $("#ThoiGianXuat").val()
            },
            complete: function (e) {
                alertToastr(e.responseJSON);
                if (e.responseJSON.code === "success") {
                    if ($('#mdlChiTietPhieuXuat').text().length > 1) {
                        $("#mdlChiTietPhieuXuat").data("kendoWindow").close();
                        $("#gridLichSuXuatVatTuKhoTong").data("kendoGrid").dataSource.read();
                        $("#gridQuanLyVatTuKhoTong").data("kendoGrid").dataSource.read();
                    } else {
                        $("#mdlXuatVatTuKhoTong").data("kendoWindow").close();
                        $("#gridQuanLyVatTuKhoTong").data("kendoGrid").dataSource.read();
                    }
                }
            }
        });
    });
});
function showslideimg() {
    $('.imgslider img').fullscreenslides();

    // All events are bound to this container element
    var $container = $('#fullscreenSlideshowContainer');
    $container
    $container
        //This is triggered once:
        .bind("init", function () {
            // The slideshow does not provide its own UI, so add your own
            // check the fullscreenstyle.css for corresponding styles
            if ($('#fullscreenSlideshowContainer #fs-close').length == 0) {
                $container
                    .append('<div class="ui" id="fs-close">&times;</div>')
                    .append('<div class="ui" id="fs-loader">' + GetTextLanguage("dangtai") + '</div > ')
                    .append('<div class="ui" id="fs-prev">&lt;</div>')
                    .append('<div class="ui" id="fs-next">&gt;</div>')
                    .append('<div class="ui" id="fs-caption"><span></span></div>');
                $('#fs-prev').click(function () {
                    // You can trigger the transition to the previous slide
                    $container.trigger("prevSlide");
                });
                $('#fs-next').click(function () {
                    // You can trigger the transition to the next slide
                    $container.trigger("nextSlide");
                });
                $('#fs-close').click(function () {
                    // You can close the slide show like this:
                    $container.trigger("close");
                });
            }
            // Bind to the ui elements and trigger slideshow events


        })
        // When a slide starts to load this is called
        .bind("startLoading", function () {
            // show spinner
            $('#fs-loader').show();
        })
        // When a slide stops to load this is called:
        .bind("stopLoading", function () {
            // hide spinner
            $('#fs-loader').hide();
        })
        // When a slide is shown this is called.
        // The "loading" events are triggered only once per slide.
        // The "start" and "end" events are called every time.
        // Notice the "slide" argument:
        .bind("startOfSlide", function (event, slide) {
            // set and show caption
            $('#fs-caption span').text(slide.title);
            $('#fs-caption').show();
        })
        // before a slide is hidden this is called:
        .bind("endOfSlide", function (event, slide) {
            $('#fs-caption').hide();
        });
}