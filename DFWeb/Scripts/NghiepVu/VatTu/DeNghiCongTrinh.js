﻿
$(document).ready(function () {

   
    var gridTonVatTu = $("#grid").kendoGrid({
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/DeNghiVatTu" + "/LayDanhSachDeNghiVatTu",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: { CongTrinhID: "", DoiThiCongID: "", TrangThai:1000 }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }

            },
            sort: [{ field: "ThoiGian", dir: "desc" }],
            type: "json",
            batch: true,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "DeNghiCongTrinhID",
                    fields: {
                        DeNghiCongTrinhID: { editable: false },
                        MaDeNghi: { editable: false },
                        ThoiGian: { type: "date", editable: false },
                        TenNhanVien: { type: "string", editable: false },
                        TenCongTrinh: { type: "string", editable: false },
                        TenDoi: { type: "string", editable: false },
          
                    }
                }
            },
        }),
        pageable: pageableAll,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: kendo.template($("#templtool").html()),
        columns: [
            {
                title: GetTextLanguage("stt"),
                template: "<span class='stt'></span>",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "ThoiGian",
                title: GetTextLanguage("thoigian"),
                attributes: { "style": "text-align:center !important;" },
                filterable: FilterInTextColumn,
                width: 150,
                format:"{0:dd/MM/yyyy HH:mm}"
            },
            {
                field: "TrangThai",
                title: GetTextLanguage("trangthai"),
                attributes: { "style": "text-align:left !important;" },
                filterable: FilterInTextColumn, width: 200,
                template: kendo.template($("#tempTrangThai").html())
            },
            {
                field: "MaDeNghi", title: GetTextLanguage("madenghi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
            },
            {
                field: "TenCongTrinh", title: "Dự Án", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
            },
            {
                field: "TenDoi", title: GetTextLanguage("doithicong"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
            },
              {
                  field: "TenNhanVien", title:"Người tạo", attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
              },
        ],
        dataBound: function (e) {
            var rows = this.items();
            var dem = 0;
            $(rows).each(function () {
                dem++;
                var rowLabel = $(this).find(".stt");
                $(rowLabel).html(dem);
                if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                    dem = 0;
                }
            });
        },
    });
    $("#grid .k-grid-add").click(function () {
        CreateModalWithSize("mdlChiTiet", "90%", "90%", null, "Đề nghị vật tư");
        $("#mdlChiTiet").load("/DeNghiVatTu" + "/TaoDeNghi_View");
    });
    $("#grid .k-grid-content").on("dblclick", "td", function () {
        var rowct = $(this).closest("tr"),
            grid = $("#grid").data("kendoGrid"),
            dataItem = grid.dataItem(rowct);
        if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
            CreateModalWithSize("mdlChiTiet", "90%", "90%", null, "Đề nghị vật tư");
            $("#mdlChiTiet").load("/DeNghiVatTu" + "/XemDeNghi_View?DeNghiCongTrinhID=" + dataItem.DeNghiCongTrinhID);
        }
    });


    var datatrangthai = [
      { text: "Chưa duyệt", val: 0 },
      { text: "Đã duyệt", val: 1 },
      { text: "Đề nghị mua", val: 2 },
      { text: "Đã mua", val: 3 },
      { text: "Đề nghị điều chuyển", val: 4 },
      { text: "Duyệt điều chuyển", val: 5},
      //{ text: "Đề nghị mua + điều chuyển", val: 6 },
      //{ text: "Mua + điều chuyển", val: 7 },
      { text: "Đã xuất", val: 8 },
      { text: "Đã nhận", val: 9 },
    ];
    $("#inputTrangThai").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "val",
        autoBind: false,
        optionLabel: "Tất cả trạng thái",
        dataSource: datatrangthai,
        change: function () {
            gridReload("grid", { CongTrinhID: $("#inputCongTrinhID").val(), DoiThiCongID: $("#inputDoiThiCongID").val(), TrangThai: this.value(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });


    $("#inputCongTrinhID").kendoDropDownList({
        dataTextField: "TenCongTrinh",
        dataValueField: "CongTrinhId",
        autoBind: false,
        optionLabel: "Tất cả Dự Án",
        dataSource: getDataDroplit("/DeNghiVatTu", "/LayDanhSachCongTrinhByNhanVien"),
        change: function () {
            $("#inputDoiThiCongID").data("kendoDropDownList").dataSource.read({ CongTrinhID: this.value() });
            $("#inputDoiThiCongID").data("kendoDropDownList").refresh();
            gridReload("grid", { CongTrinhID: $("#inputCongTrinhID").val(), DoiThiCongID: $("#inputDoiThiCongID").val(), TrangThai: $("#inputTrangThai").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });

    $("#inputDoiThiCongID").kendoDropDownList({
        dataTextField: "TenDoi",
        dataValueField: "DoiThiCongID",
        autoBind: false,
        optionLabel: "Tất cả đội",
        dataSource: getDataDroplitwithParam("/DeNghiVatTu", "/LayDanhSachDoiThiCongByNhanVien", { CongTrinhID: $("#inputCongTrinhID").val() }),
        change: function () {
            gridReload("grid", { CongTrinhID: $("#inputCongTrinhID").val(), DoiThiCongID: $("#inputDoiThiCongID").val(), TrangThai: $("#inputTrangThai").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });
    var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
    $("#inputTuNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(y - 5, m, 1), change: function () {
            gridReload("grid", { CongTrinhID: $("#inputCongTrinhID").val(), DoiThiCongID: $("#inputDoiThiCongID").val(), TrangThai: $("#inputTrangThai").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });
    $("#inputDenNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(), change: function () {
            gridReload("grid", { CongTrinhID: $("#inputCongTrinhID").val(), DoiThiCongID: $("#inputDoiThiCongID").val(), TrangThai: $("#inputTrangThai").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });
})