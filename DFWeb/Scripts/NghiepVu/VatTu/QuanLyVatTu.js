﻿function getSumThanhTien(value) {
    var datasource = $("#gridNhapXuatKho").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            console.log(element);
            //result = element.aggregates.ThanhTien.sum;
        }
    });
    return result;
}

$(document).ready(function () {
    //Init
    var crudServiceBaseUrl = "/QuanLyVatTu";
    var crudServiceBaseUrlDoi = "/DMDoiThiCong";
    var crudServiceBaseUrlCongTrinh = "/QuanLyCongTrinh";
    var crudServiceBaseUrlUploadFile = "/Upload";

    var CongTrinhTong = "FFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
    // var crudServiceBaseUrlDoi = "/QLDoiThiCong";
    var dataSourceGrid;
    var dataSourceCongTrinh;
    var tlb_drlCongTrinh;
    var tlb_drlLoaiHinh;
    var timeoutkeysearch = null;
    var mdlChooseForm;
    var mdlThietBi;

    var LoaiHinhThietBi = 0;//0:của công ty - 1:đi thuê
    var actionThietBi = { add: 10, edit: 15 };
    var IsNumTrangThai = { Duyet: 1, ChuaDuyet: 2, TuChoi: 3 };
    var LoaiHinhNhapXuat = { Nhap: 1, Xuat: 2 };

    var datafix_loaihinhxuat = [
        { text: GetTextLanguage("xuatdieuchuyen"), val: 0 },
        { text: GetTextLanguage("xuatsudung"), val: 1 }
    ]
    var currentDoiThiCongID;
    var currentCongTrinhID;
    //Build grid
  

    var DataTongVatTu = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetTonVatTu",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: { CongTrinhID: "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF", DoiThiCongID: "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" }
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        group: "TenNhom",
        sort: [{ field: "TenVatTu", dir: "asc" }],
        schema: {
            data: "data",
            total: "total"
        },
        aggregate: [{ field: "GiaTriTon", aggregate: "sum" }],
    });
    var gridTonVatTu = $("#gridTonVatTuTong").kendoGrid({
        dataSource: DataTongVatTu,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        /*pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },*/
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: kendo.template($("#btnQuanLy2").html()),
        columns: [
            {
                title: GetTextLanguage("stt"),
                template: "<span class='stt'></span>",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
            },
            {
                field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
            },
            {
                field: "DonGiaGanNhat", title: GetTextLanguage("gianhap"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
            },
            {
                field: "GiaXuatGanNhat", title: GetTextLanguage("giaxuat"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
            },
            {
                field: "TenNhom", title: GetTextLanguage("nhom"), hidden: true
            },
            {
                title: GetTextLanguage("soluong"),
                columns: [
                    {
                        field: "TongNhap", title: GetTextLanguage("nhap"), format: "{0:n2}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                    },
                    {
                        field: "TongXuat", title: GetTextLanguage("xuat"), format: "{0:n2}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                    },
                    {
                        field: "SoLuongTon", title: GetTextLanguage("ton"), format: "{0:n2}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                    },
                ]
            },
            {
                title: GetTextLanguage("gia"),
                columns: [
                    {
                        field: "GiaNhap", title: GetTextLanguage("nhap"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                    },
                    {
                        field: "GiaXuat", title: GetTextLanguage("xuat"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                    },
                    {
                        field: "GiaTriTon", title: GetTextLanguage("ton"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                        aggregates: ["sum"], footerTemplate: "<div style='text-align:right !important;'> #: kendo.toString(sum, \"n0\") #</div>"
                    },
                ]
            },
        ],
        dataBound: function (e) {
            var rows = this.items();
            var dem = 0;
            $(rows).each(function () {
                dem++;
                var rowLabel = $(this).find(".stt");
                $(rowLabel).html(dem);
                if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                    dem = 0;
                }
            });
        },
    });

    $('#gridTonVatTuTong #btnQlNhap').click(function () {
        quanlynhapkho(null, true);
    })
    $('#gridTonVatTuTong #btnQlXuat').click(function () {
        quanlyxuatkho(null, true);
    })
    $('#gridTonVatTuTong #btnexcel').click(function () {
        var url = "/QuanLyVatTu/ExportToExcelBaoCaoTon";
        location.href = url;
    });



    function kiemtratonkho(e) {
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        var mdl = $('#mdlTonVatTu').kendoWindow({
            width: "80%",
            height: "80%",
            title: GetTextLanguage("danhsachphieunhap"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#tempTonVatTu').html()));
        setTitleWindow("mdlTonVatTu", GetTextLanguage("baocaoton") + " - " + GetTextLanguage("doi") + " :" + dataItem.get("TenDoi") + " / " + GetTextLanguage("thuoc") + ": " + dataItem.get("TenCongTrinh"));

        var gridTonVatTu = $("#gridTonVatTu").kendoGrid({
            dataSource: getDataSourceGridSumNoPagination(crudServiceBaseUrl, "/GetTonVatTu", { CongTrinhID: dataItem.get("CongTrinhID"), DoiThiCongID: dataItem.get("DoiThiCongID") }, "TenNhom", [{ field: "GiaTriTon", aggregate: "sum" }]),
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            /*pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },*/
            height: window.innerHeight * 0.8,
            width: "100%",
            columns: [
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenNhom", title: GetTextLanguage("nhom"), hidden: true
                },
                {
                    field: "TongNhap", title: GetTextLanguage("tongnhap"), format: "{0:n2}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                },
                {
                    field: "TongXuat", title: GetTextLanguage("tongxuat"), format: "{0:n2}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                },
                {
                    field: "SoLuongTon", title: GetTextLanguage("ton"), format: "{0:n2}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                },
                {
                    field: "DonGiaGanNhat", title: GetTextLanguage("giagannhat"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                },
                {
                    field: "GiaTriTon", title: GetTextLanguage("giatriton"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    aggregates: ["sum"], footerTemplate: "<div style='text-align:right !important;'> #: kendo.toString(sum, \"n0\") #</div>"
                }
            ]
        });

        resizeWindow3("gridTonVatTu", "mdlTonVatTu");

    }
    function quanlynhapkho(dataItem, isTong) {
        var doithicongid = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
        var congtrinhid = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
        console.log(dataItem);
        if (!isTong) {
            currentDoiThiCongID = dataItem.get("DoiThiCongID");
            currentCongTrinhID = dataItem.get("CongTrinhID");
            doithicongid = dataItem.get("DoiThiCongID");
            congtrinhid = dataItem.get("CongTrinhID");
        }
        // currentCongTrinhID;
        var mdl = $('#mdlDanhSachNhapXuatKho').kendoWindow({
            width: "80%",
            height: "80%",
            title: GetTextLanguage("danhsachphieunhap"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#tempChiTiet').html()));
        if (!isTong) {
            setTitleWindow("mdlDanhSachNhapXuatKho", GetTextLanguage("danhsachphieunhap") + " - " + GetTextLanguage("doi") + " :" + dataItem.get("TenDoi") + " / " + GetTextLanguage("thuoc") + ": " + dataItem.get("TenCongTrinh"));
        } else {
            setTitleWindow("mdlDanhSachNhapXuatKho", GetTextLanguage("danhsachphieunhaptong"));
        }

        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();



        var dataNhapKho = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + "/getDataNhapXuatKho",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        CongTrinhID: congtrinhid,
                        DoiThiCongID: doithicongid,
                        LoaiHinh: LoaiHinhNhapXuat.Nhap
                    }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total"
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            aggregate: [{ field: "ThanhTien", aggregate: "sum" },{field: "SoLuong",aggregate: "sum"}]
        });
        var gridPhieuNhap = $("#gridNhapXuatKho").kendoGrid({
            dataSource: dataNhapKho,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            height: window.innerHeight * 0.7,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"),
                    attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    //+ " [#= getSumThanhTien(value) #]"
                    template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                    groupHeaderTemplate: GetTextLanguage("thoigian") + ": #= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') #" + "<div class='pull-right'><button class='btn btn-xs btn-primary xemchitiet'>" + GetTextLanguage("xemchitiet") + "</button></div>",
                },
                {
                    field: "TenNhanVien", title: GetTextLanguage("nhanviennhap"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n0')# </div>",
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(DonGia,'n0') #",
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                    attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(ThanhTien,'n0') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n0')# </div>",
                },
                {
                    field: "TrangThaiDuyet", title: GetTextLanguage("trangthaiduyet"), attributes: alignCenter, filterable: FilterInColumn, width: 150
                },
                {
                    field: "GhiChu", title: GetTextLanguage("ghichu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                }
            ],
            dataBound: function (e) {
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });

        resizeWindow("gridNhapXuatKho", "mdlDanhSachNhapXuatKho");

        //$(".btnTimKiemNhap").click(function () {
        //    gridReload("gridPhieuNhap", { DoiThiCongID: dataItem.get("DoiThiCongID"), TuNgay: $("#TuNgayNhap").val(), DenNgay: $("#DenNgayNhap").val(), LoaiHinh: LoaiHinhNhapXuat.Nhap });
        //});

        $("#gridNhapXuatKho").on("click", ".xemchitiet", function () {
            var rowct = $(this).closest("tr"),
                gridct = $("#gridNhapXuatKho").data("kendoGrid"),
                dataItemct = gridct.dataItem(rowct);
            var dataedit;
            $(".windows8").css("display", "block");
            setTimeout(function () {
                $.ajax({
                    cache: false,
                    async: false,
                    type: "POST",
                    url: crudServiceBaseUrl + "/GetOnePhieuNhapByID",
                    data: { PhieuNhapVatTuID: dataItemct.get("PhieuNhapVatTuID") },
                    success: function (data) {
                        altReturn(data);
                        if (data.code == "success") {
                            dataedit = data.data;
                            dataedit.isHeThong = data.isHeThong;
                            dataedit.AnhShow = data.AnhShow;
                        }
                        $(".windows8").css("display", "none");
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $(".windows8").css("display", "none");
                    }
                });
            }, 300);
            setTimeout(function () {
                addPhieuNhap(dataItem, actionThietBi.edit, dataedit, dataItemct.get("IsDuyet"), dataItemct.get("PhieuNhapVatTuID"), isTong);
            }, 500);
        });

        $("#btn-adds").click(function () {
            addPhieuNhap(dataItem, actionThietBi.add, null, null, null, isTong);
        });

        $("#btnexcelNhapXuat").click(function () {
            var url = "";
            if (!isTong) {
                url = "/QuanLyVatTu/ExportToExcelPhieuNhap?CongTrinhID=" + congtrinhid + "&DoiThiCongID=" + doithicongid + "&TenCongTrinh=" + dataItem.get("TenCongTrinh") + "&TenDoi=" + dataItem.get("TenDoi");
            } else {
                url = "/QuanLyVatTu/ExportToExcelPhieuNhap?CongTrinhID=" + congtrinhid + "&DoiThiCongID=" + doithicongid + "&TenCongTrinh=" + "&TenDoi=";
            }
            location.href = url;
        });
    }
    //function addphieu
    function addPhieuNhap(dataItem, action, dataedit, TrangThaiDuyet, PhieuID, isTong) {
        var icountrows = 0;
        if (action == actionThietBi.edit) {
            icountrows = dataedit.items.length;
        }
        var mdladd = $('#mdlAddPhieuNhap').kendoWindow({
            width: "90%",
            height: "90%",
            title: GetTextLanguage("danhsachphieunhap"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").maximize().open();
        mdladd.content(kendo.template($('#tempaddPhieuNhap').html()));


        //Init
        var dates = new Date(), ys = dates.getFullYear(), ms = dates.getMonth(), ds = dates.getDay(); hs = dates.getHours(); ms = dates.getMinutes();
        $("#ThoiGianNhap").kendoDateTimePicker({ value: dataedit == null ? new Date() : dataedit.ThoiGian, format: "dd/MM/yyyy HH:mm" });

        var dataobject = {};
        dataobject.DoiThiCongID = isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID");
        dataobject.CongTrinhID = isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID");
        dataobject.items = [];
        dataobject.action = action;
        dataobject.TrangThaiDuyet = 2;
        dataobject.Anh = [];

        var objrows = {
            ID: 0,
            VatTuID: "",
            TenVatTu: "",
            TenVietTat: "",
            TenDonVi: "",
            DonGia: 0,
            SoLuong: 0,
            ThanhTien: 0
        }

        $("#customers").kendoDropDownList({
            dataTextField: "TenVatTu",
            dataValueField: "VatTuID",
            filter: "contains",
            autoBind: false,
            optionLabel: "Chọn vật tư",
            template: "<div class='itemsVT'><h3>#: TenVatTu # -  #: TenVietTat #</h3><p>Giá: <span>#: DonGiaGanNhat #</span></div>",
            dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/LayDanhSachVatTuAdd", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Nhap }),
            change: function (e) {
                var value = this.value();
                if (value) {
                    var index = this.selectedIndex, dataItem;
                    if (this.options.optionLabel && index > 0) {
                        index = index;
                    }
                    dataItem = this.dataItem(index);
                    //set rowsvalue
                    objrows.VatTuID = dataItem.get("VatTuID");
                    objrows.TenDonVi = dataItem.get("TenDonVi");
                    objrows.TenVatTu = dataItem.get("TenVatTu");
                    objrows.DonGia = dataItem.get("DonGiaGanNhat");
                    objrows.TenVietTat = dataItem.get("TenVietTat");
                }
            }
        });

        $("#soluongs").kendoNumericTextBox({ value: 1, min: 1, format: "n2" });

        if (action == actionThietBi.add) {
            if (!isTong) {
                setTitleWindow("mdlAddPhieuNhap", GetTextLanguage("themphieunhapvattu") + " - " + GetTextLanguage("doi") + " :" + dataItem.get("TenDoi") + " / " + GetTextLanguage("thuoc") + ": " + dataItem.get("TenCongTrinh"));
            } else {
                setTitleWindow("mdlAddPhieuNhap", GetTextLanguage("themphieuhapvattukhotong"));
            }
            $(".toolVatTu").append(tooLuu());
        } else {
            if (!isTong) {
                setTitleWindow("mdlAddPhieuNhap", GetTextLanguage("chitietphieuxuatvattu") + " - " + GetTextLanguage("doi") + " :" + dataItem.get("TenDoi") + " / " + GetTextLanguage("thuoc") + ": " + dataItem.get("TenCongTrinh"));
            } else {
                setTitleWindow("mdlAddPhieuNhap", GetTextLanguage("chitietphieuxuatvattuvekhotong"));
            }

            dataobject = dataedit;
            $("#ThoiGianNhap").data("kendoDateTimePicker").enable(false);
            //if (dataedit.isHeThong == true) {
            //    //laf heej thoong thif ms duoc suar va xoa
            //    $(".toolVatTu").append(tooLuu());

            //}
            if (dataedit.isHeThong == true && (dataedit.PhieuXuatVatTuID == null || dataedit.PhieuXuatVatTuID == "")) {
                $(".toolVatTu").append(tooXoa());
                $(".toolVatTu").append(tooLuu());
            }
            $(".dshinhanhadd").html(loadslbuttonvatu(dataedit.AnhShow));
            // $(".slider-inner").html(loadsldinnerajaxvattu(dataedit.AnhShow));
            showslideimg('abcds');

            switch (TrangThaiDuyet) {
                case IsNumTrangThai.ChuaDuyet: $(".toolVatTu").append(tooDuyet() + tooTuChoi()); break;
                case IsNumTrangThai.TuChoi: $(".toolVatTu").append(tooDuyet()); break;
                case IsNumTrangThai.Duyet: $(".toolVatTu").append(toolDaDuyet()); break;
                default: $(".toolVatTu").append(tooDuyet() + tooTuChoi()); break;
            }
            if (dataedit.isHeThong == true && (dataedit.PhieuXuatVatTuID != null && dataedit.PhieuXuatVatTuID != "")) {
                $(".toolVatTu").append("<h3 class='text-danger'>"+GetTextLanguage('phieunayduoctaotuphieuxuatcungthoigian')+"</h3>");
            }

        }
        var gridChiTietNhap = $("#gridChiTietNhap").kendoGrid({
            editable: true,
            selectable: false,
            dataSource: {
                data: dataobject.items,
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false },
                            VatTuID: { editable: false },
                            TenDonVi: { type: "string", editable: false },
                            TenVatTu: { type: "string", editable: false },
                            TenVietTat: { type: "string", editable: false },
                            SoLuong: { type: "number", editable: true, validation: { min: 1, format: "n2" } },
                            DonGia: { type: "number", editable: true, validation: { min: 0, format: "n0" } },
                            ThanhTien: { type: "number", editable: false, validation: { format: "n2" } },
                            NhaCungCap: { defaultValue: { NhaCCID: null, NhaCCName: GetTextLanguage("nhacungcap") }, editable: true }// If change editable to true, "set" work, but i can manualy change the value.
                        }
                    }
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
            },
            dataBound: function () {
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });

                if (dataedit != null) {
                    //check hiển thị nút xóa dòng
                    $("#gridChiTietNhap tbody tr .xoaitem").each(function () {
                        var currentDataItem = $("#gridChiTietNhap").data("kendoGrid").dataItem($(this).closest("tr"));
                        ////Check in the current dataItem if the row is deletable
                        if (dataedit.isHeThong != true) {
                            //laf heej thoong thif ms duoc suar va xoa
                            $(this).remove();
                        }
                    });
                }
            },
            columns:
            [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                { field: "VatTuID", hidden: true },
                { field: "TenVatTu", width: 200, title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, footerTemplate: GetTextLanguage("tong"), filterable: FilterInTextColumn },
                { field: "TenVietTat", width: 200, attributes: { style: "text-align:left;" }, title: GetTextLanguage("tenviettat"), filterable: FilterInTextColumn },
                { field: "TenDonVi", width: 200, title: GetTextLanguage("donvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn },
                { field: "DonGia", title: GetTextLanguage("gianhap"), format: "{0:n0}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150 },
                { field: "SoLuong", width: 100, title: GetTextLanguage("soluong"), format: "{0:n2}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), format: "{0:n2}", attributes: {"style": "text-align:right !important;"}, filterable: FilterInColumn, width: 150,
                    aggregates: ["sum"], footerTemplate: "<div style='text-align:right !important;'> #: kendo.toString(sum, \"n2\") #</div>"
                },
                { width: 200, field: "NhaCungCap", title: GetTextLanguage("nhacungcap"), editor: NhaccDropDownEditor, template: "#=NhaCungCap.NhaCCName#", attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn },
                {
                    command: [
                        { text:GetTextLanguage("xoa"), click: xoarow, className: "xoaitem" }
                    ], width: 100
                },
            ],
            save: function (e) {
                if (e.values.SoLuong || e.values.DonGia) {
                    var sl = e.values.SoLuong || e.model.SoLuong;
                    var dg = e.values.DonGia || e.model.DonGia;
                    for (var i = 0; i < dataobject.items.length; i++) {
                        if (dataobject.items[i].ID === e.model.get("ID")) {
                            if (e.values.SoLuong) {
                                dataobject.items[i].SoLuong = sl;
                            }
                            else {
                                dataobject.items[i].DonGia = dg;
                            }
                            dataobject.items[i].ThanhTien = sl * dg
                            gridReload("gridChiTietNhap", {});
                            break;
                        }
                    }

                }
                if (e.values.NhaCungCap || e.values.NhaCungCap) {
                    var ncc = e.values.NhaCungCap || e.model.NhaCungCap;
                    for (var i = 0; i < dataobject.items.length; i++) {
                        if (dataobject.items[i].ID === e.model.get("ID")) {

                            if (e.values.NhaCungCap) {
                                dataobject.items[i].NhaCungCap = ncc;
                            }
                            console.log(dataobject);
                            gridReload("gridChiTietNhap", {});
                            break;
                        }

                    }
                }
            }
        }).data("kendoGrid");

        resizeWindow2("gridChiTietNhap", "mdlAddPhieuNhap", 270, 433);

        function xoarow(e) {
            e.preventDefault();
            var dataItemrow = this.dataItem($(e.currentTarget).closest("tr"));
            for (var i = 0; i < dataobject.items.length; i++) {
                if (dataobject.items[i].ID === dataItemrow.get("ID")) {
                    dataobject.items.splice(i, 1);
                    gridReload("gridChiTietNhap", {});
                    break;
                }
            }
        }

        // add row
        $("#addrow").click(function () {
            var checkedIds = {};
            var mdl = $('#mdlDanhSachVatTuNhapXuat').kendoWindow({
                width: "80%",
                height: "80%",
                title: GetTextLanguage("danhsachvattu"),
                modal: true,
                visible: false,
                resizable: false,
                actions: [
                    "Close"
                ],
                deactivate: function () {
                    $(this.element).empty();
                }
            }).data("kendoWindow").center().open();
            mdl.content(kendo.template($('#tempDanhSachVatTuNhapXuat').html()));
            setTitleWindow("mdlDanhSachVatTuNhapXuat", GetTextLanguage("danhsachvattu"));

            var gridDanhSachVatTuNhapXuat = $("#gridDanhSachVatTuNhapXuat").kendoGrid({
                dataSource: getDataDanhSachVatTu(crudServiceBaseUrl, "/LayDanhSachVatTuNhap", {}),
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                editable: true,
                pageable: {
                    refresh: true,
                    buttonCount: 5,
                    pageSizes: true,
                    messages: messagegrid
                },
                height: window.innerHeight * 0.8,
                width: "100%",
                dataBound: onDataBound,
                //toolbar: kendo.template($("#btnThem").html()),
                columns: [
                    {
                        field: "select",
                        title: "&nbsp;",
                        template: "<input type='checkbox' class='checkbox' />",
                        sortable: false,
                        filterable: false,
                        width: 30
                    },
                    {
                        field: "TenNhom", title: GetTextLanguage("nhom"), hidden: true
                    },
                    {
                        field: "TenVatTu", title: GetTextLanguage("vattu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                    },
                    {
                        field: "TenDonVi", title: GetTextLanguage("donvi"), width: 200, attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn
                    },
                    {
                        field: "DonGia", width: 150, title: GetTextLanguage("gianhap"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn
                    },
                    {
                        field: "SoLuong", width: 100, title: GetTextLanguage("soluong"), format: "{0:n2}", attributes: { "class": "soluongcheck" }, filterable: FilterInColumn
                    },
                ]
            }).data("kendoGrid");

            resizeWindow("gridDanhSachVatTuNhapXuat", "mdlDanhSachVatTuNhapXuat");
            gridDanhSachVatTuNhapXuat.table.on("click", ".checkbox", selectRow);
            gridDanhSachVatTuNhapXuat.table.on("click", ".soluongcheck", selectRowSoLuong);
            function selectRow() {
                var checked = this.checked,
                    rows = $(this).closest("tr"),
                    grids = $("#gridDanhSachVatTuNhapXuat").data("kendoGrid"),
                    dataItems = grids.dataItem(rows);
                checkedIds[dataItems.uid] = checked;
                if (checked) {
                    //-select the row
                    rows.addClass("k-state-selected");
                } else {
                    //-remove selection
                    rows.removeClass("k-state-selected");
                }
            }

            function selectRowSoLuong() {
                var rows = $(this).closest("tr"),
                    grids = $("#gridDanhSachVatTuNhapXuat").data("kendoGrid"),
                    dataItems = grids.dataItem(rows);
                checkedIds[dataItems.uid] = true;
                rows.addClass("k-state-selected").find(".checkbox").attr("checked", "checked");

            }

            function onDataBound(e) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (checkedIds[view[i].uid]) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                    }
                }
            }

            $("#btn-add-vt").on("click", function () {
                var grid = $("#gridDanhSachVatTuNhapXuat").data("kendoGrid");
                var selectedRows = $(".k-state-selected", "#gridDanhSachVatTuNhapXuat");
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length - 1; i++) {
                        var selectedItem = grid.dataItem(selectedRows[i]);
                        var dg = selectedItem.DonGia == null ? 0 : selectedItem.DonGia;
                        var sl = selectedItem.SoLuong == null ? 0 : selectedItem.SoLuong;
                        var bool = true;
                        for (var k = 0; k < dataobject.items.length; k++) {
                            if (dataobject.items[k].VatTuID === selectedItem.VatTuID && dataobject.items[k].DonGia === dg) {
                                dataobject.items[k].SoLuong = (parseFloat(dataobject.items[k].SoLuong) + parseFloat(sl));
                                dataobject.items[k].ThanhTien = (parseFloat(dataobject.items[k].SoLuong) * parseFloat(dataobject.items[k].DonGia));
                                dataobject.items[k].NhaCungCap = { NhaCCID: null, NhaCCName: GetTextLanguage("nhacungcap") }
                                bool = false;
                                break;
                            }
                        }

                        if (bool) {
                            icountrows++;
                            dataobject.items.push({
                                ID: icountrows,
                                VatTuID: selectedItem.VatTuID,
                                TenVatTu: selectedItem.TenVatTu,
                                TenVietTat: selectedItem.TenVietTat,
                                TenDonVi: selectedItem.TenDonVi,
                                DonGia: dg,
                                SoLuong: sl,
                                ThanhTien: (parseFloat(dg) * parseFloat(sl)),
                                NhaCungCap: { NhaCCID: null, NhaCCName: GetTextLanguage("nhacungcap") }
                            });

                        }
                    }
                    gridReload("gridChiTietNhap", {});
                    closeModel("mdlDanhSachVatTuNhapXuat");
                }
            });

        });

        /*

        */
        //Lưu Phiếu
        $("#saveas").click(function () {
            var x = confirm(GetTextLanguage("bancomuonluubanghinaykhong"));
            if (x) {
                if (dataobject.items.length == 0) {
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("khongcovattunaotrenphieu"));
                }
                else {
                    $(".windows8").css("display", "block");
                    //
                    if (storedFiles.length > 0) {
                        var dataimg = new FormData();
                        var strUrl = uploadMultipleFileVatTu(crudServiceBaseUrlUploadFile + "/Uploadfiles", storedFiles, null).split(',');
                        for (var k = 0; k < strUrl.length; k++) {
                            dataobject.Anh.push(strUrl[k]);
                        }
                    }
                    //Insert này
                    dataobject.ThoiGian = $("#ThoiGianNhap").val();
                    if (dataobject.ThoiGian != "" && dataobject.ThoiGian != null) {
                        //var grid = $("#gridChiTietNhap").data("kendoGrid");
                        //var selectedRows = $("tbody tr", "#gridChiTietNhap");
                        //if (selectedRows.length > 0) {
                        //    for (var i = 0; i < selectedRows.length - 1; i++) {
                        //        var selectedItem = grid.dataItem(selectedRows[i]);
                        //        var dg = selectedItem.DonGia == null ? 0 : selectedItem.DonGia;
                        //        var sl = selectedItem.SoLuong == null ? 0 : selectedItem.SoLuong;
                        //        var bool = true;
                        //        for (var k = 0 ; k < dataobject.items.length; k++) {
                        //            if (dataobject.items[k].VatTuID === selectedItem.VatTuID && dataobject.items[k].DonGia === dg) {
                        //                dataobject.items[k].NhaCungCap = selectedItem.NhaCungCap;
                        //                bool = false;
                        //                break;
                        //            }
                        //        }

                        //        if (bool) {
                        //            icountrows++;
                        //            dataobject.items.push({
                        //                ID: icountrows,
                        //                VatTuID: selectedItem.VatTuID,
                        //                TenVatTu: selectedItem.TenVatTu,
                        //                TenVietTat: selectedItem.TenVietTat,
                        //                TenDonVi: selectedItem.TenDonVi,
                        //                DonGia: dg,
                        //                SoLuong: sl,
                        //                ThanhTien: (parseFloat(dg) * parseFloat(sl)),
                        //                NhaCungCap: selectedItem.NhaCungCap
                        //            });

                        //        }
                        //    }
                        //    gridReload("gridChiTietNhap", {});
                        //    //closeModel("mdlDanhSachVatTuNhapXuat");
                        //}

                        ContentWatingOP("mdlAddPhieuNhap", 0.9);
                        setTimeout(function () {
                        $.ajax({
                            cache: false,
                            async: true,
                            type: "POST",
                            url: crudServiceBaseUrl + "/AddorUpdatePhieuNhap",
                            data: { data: JSON.stringify(dataobject), PhieuNhapID: PhieuID },
                            success: function (data) {
                                altReturn(data);
                                if (data.code == "success") {
                                    gridReload("gridNhapXuatKho", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Nhap });
                                    closeModel("mdlAddPhieuNhap");
                                    $("#mdlAddPhieuNhap").unblock();
                                    //remove
                                    storedFiles = [];
                                }
                                $(".windows8").css("display", "none");
                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                $(".windows8").css("display", "none");
                            }
                            });
                        }, 1000);
                        $(".windows8").css("display", "none");
                    } else {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("msg_banphaichonngaynhap"));
                        $(".windows8").css("display", "none");
                        $("#mdlAddPhieuNhap").unblock();
                    }

                    //
                }
            } else {
                return false;
            }
        });

        //Duyệt Phiếu
        $("#btnDuyet").click(function () {
            var x = confirm(GetTextLanguage("bancomuonduyetphieunaykhong"));
            if (x) {
                $(".windows8").css("display", "block");
                setTimeout(function () {
                    ContentWatingOP("mdlAddPhieuNhap", 0.9);
                    $.ajax({
                        cache: false,
                        async: true,
                        type: "POST",
                        url: crudServiceBaseUrl + "/DuyetPhieu",
                        data: { PhieuNhapVatTuID: PhieuID, CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID") },
                        success: function (data) {
                            altReturn(data);
                            if (data.code == "success") {
                                gridReload("gridPhieuNhap", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Nhap });
                                closeModel("mdlAddPhieuNhap");
                                $("#mdlAddPhieuNhap").unblock();
                            }
                            $(".windows8").css("display", "none");
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("coloitrongquatrinhxulydulieu"));
                            $(".windows8").css("display", "none");
                            $("#mdlAddPhieuNhap").unblock();
                        }
                    });
                }, 500);
            } else {
                return false;
            }
        });

        //Duyệt Phiếu
        $("#btnTuChoi").click(function () {
            var mdladd = $('#mdlTuChoi').kendoWindow({
                width: 500,
                height: 290,
                title: GetTextLanguage("noidungtuchoi"),
                modal: true,
                visible: false,
                resizable: false,
                actions: [
                    "Close"
                ],
                deactivate: function () {
                    $(this.element).empty();
                }
            }).data("kendoWindow").center().open();
            mdladd.content(kendo.template($('#tempTuChoi').html()));

            $("#tuchois").click(function () {
                if ($("#ndTuChoi").val() == "") {
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("banphainhapnoidungtuchoi"));
                    $("#ndTuChoi").focus();
                } else {
                    $(".windows8").css("display", "block");
                    $.ajax({
                        cache: false,
                        async: true,
                        type: "POST",
                        url: crudServiceBaseUrl + "/TuChoiDuyet",
                        data: { PhieuNhapVatTuID: PhieuID, NoiDung: $("#ndTuChoi").val() },
                        success: function (data) {
                            altReturn(data);
                            if (data.code == "success") {
                                gridReload("gridNhapXuatKho", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Nhap });
                                closeModel("mdlTuChoi");
                                closeModel("mdlAddPhieuNhap");
                            }
                            $(".windows8").css("display", "none");
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $(".windows8").css("display", "none");
                        }
                    });
                }
            });
        });

        //Xóa Phiếu
        $("#btnXoa").click(function () {
            var x = confirm(GetTextLanguage("bancomuonxoaphieunaykhong"));
            if (x) {
                $(".windows8").css("display", "block");
                ContentWatingOP("mdlAddPhieuNhap", 0.9);
                setTimeout(function () {
                    $.ajax({
                        cache: false,
                        async: false,
                        type: "POST",
                        url: crudServiceBaseUrl + "/XoaPhieuNhap",
                        data: { PhieuNhapVatTuID: PhieuID, CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID") },
                        success: function (data) {
                            altReturn(data);
                            if (data.code == "success") {
                                gridReload("gridNhapXuatKho", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Nhap });
                                closeModel("mdlAddPhieuNhap");
                                $("#mdlAddPhieuNhap").unblock();
                            }
                            $(".windows8").css("display", "none");
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $(".windows8").css("display", "none");
                        }
                    });
                }, 500);
            } else {
                return false;
            }
        });

        //Chinhr suwar anh khi loadlaij
        $("body").on("click", ".removeLoad", function () {
            var file = $(this).data("file");
            for (var i = 0; i < dataobject.Anh.length; i++) {
                if (dataobject.Anh[i] === file) {
                    dataobject.Anh.splice(i, 1);
                    break;
                }
            }
            $(this).parent().remove();
        });


    }

    function quanlyxuatkho(dataItem, isTong) {
        var doithicongid = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
        var congtrinhid = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
        console.log("ssssss");
        if (!isTong) {
            currentDoiThiCongID = dataItem.get("DoiThiCongID");
            currentCongTrinhID = dataItem.get("CongTrinhID");
            doithicongid = dataItem.get("DoiThiCongID");
            congtrinhid = dataItem.get("CongTrinhID");
        }
        var mdl = $('#mdlDanhSachNhapXuatKho').kendoWindow({
            width: "80%",
            height: "80%",
            title: GetTextLanguage("danhsachphieunhap"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#tempChiTiet').html()));
        if (!isTong) {
            setTitleWindow("mdlDanhSachNhapXuatKho", GetTextLanguage("danhsachphieuxuat") + "- " + GetTextLanguage("doi") + ": " + dataItem.get("TenDoi") + " /" + GetTextLanguage("thuoc") + ": " + dataItem.get("TenCongTrinh"));
        } else {
            setTitleWindow("mdlDanhSachNhapXuatKho", GetTextLanguage("danhsachphieuxuatkhotong"));
        }
        var dataXuatKho = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + "/getDataNhapXuatKho",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        CongTrinhID: congtrinhid,
                        DoiThiCongID: doithicongid,
                        LoaiHinh: LoaiHinhNhapXuat.Xuat
                    }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total"
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            aggregate: [{ field: "ThanhTien", aggregate: "sum" },{field: "SoLuong",aggregate: "sum"}]
        });
        var gridPhieuXuat = $("#gridNhapXuatKho").kendoGrid({
            dataSource: dataXuatKho,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            height: window.innerHeight * 0.7,
            width: "100%",
            //PhieuXuatVatTuID/ThoiGian/LoaiHinhXuat/NhanVienNhap/TenVatTu/TenDonVi/SoLuong/DonGia/ThanhTien/NhanVienNhan/CongTrinhNhan/DoiThiCongNhan
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                    groupHeaderTemplate: GetTextLanguage("thoigian") + " : #= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') #" + "<div class='pull-right'><button class='btn btn-xs btn-primary xemchitiet'> " + GetTextLanguage("xemchitiet") + "</button></div>",
                },
                {
                    field: "LoaiHinhXuat", title: GetTextLanguage("loaihinhxuat"), attributes: alignCenter, filterable: FilterInTextColumn, width: 100
                },
                {
                    field: "NhanVienNhap", title: GetTextLanguage("nhanviennhap"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n0') # </div>"
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(DonGia,'n0') #"
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#=kendo.toString(ThanhTien,'n0') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n0') # </div>"
                },
                {
                    field: "NhanVienNhan", title: GetTextLanguage("nhanviennhan"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "CongTrinhNhan", title: GetTextLanguage("congtrinhnhan"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "DoiThiCongNhan", title: GetTextLanguage("doithicongnhan"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                }
            ],
            dataBound: function (e) {
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });

        resizeWindow("gridPhieuXuat", "mdlDanhSachNhapXuatKho");

        $(".btnTimKiemNhap").click(function () {
            gridReload("gridPhieuXuat", { DoiThiCongID: dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Xuat });
        });

        $("#gridNhapXuatKho").on("click", ".xemchitiet", function () {
            var rowct = $(this).closest("tr"),
                gridct = $("#gridNhapXuatKho").data("kendoGrid"),
                dataItemct = gridct.dataItem(rowct);
            var dataedit;
            $(".windows8").css("display", "block");
            setTimeout(function () {
                $.ajax({
                    cache: false,
                    async: false,
                    type: "POST",
                    url: crudServiceBaseUrl + "/GetOnePhieuXuatByID",
                    data: { PhieuXuatVatTuID: dataItemct.get("PhieuXuatVatTuID") },
                    success: function (data) {
                        altReturn(data);
                        if (data.code == "success") {
                            dataedit = data.data;
                            dataedit.isHeThong = data.isHeThong;
                            dataedit.AnhShow = data.AnhShow;
                        }
                        $(".windows8").css("display", "none");
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $(".windows8").css("display", "none");
                    }
                });
            }, 300);
            setTimeout(function () {
                addPhieuXuat(dataItem, actionThietBi.edit, dataedit, dataItemct.get("PhieuXuatVatTuID"), isTong);
            }, 500);
        });

        $("#btn-adds").click(function () {
            addPhieuXuat(dataItem, actionThietBi.add, null, null, isTong);
        });

        $("#btnexcelNhapXuat").click(function () {
            var url = "";
            if (!isTong) {
                url = "/QuanLyVatTu/ExportToExcelPhieuXuat?CongTrinhID=" + congtrinhid + "&DoiThiCongID=" + doithicongid + "&TenCongTrinh=" + dataItem.get("TenCongTrinh") + "&TenDoi=" + dataItem.get("TenDoi");
            } else
            {
                url = "/QuanLyVatTu/ExportToExcelPhieuXuat?CongTrinhID=" + congtrinhid + "&DoiThiCongID=" + doithicongid + "&TenCongTrinh=" +"&TenDoi=";
            }
            location.href = url;
        });
    }

    //addphieu xuaat
    function addPhieuXuat(dataItem, action, dataedit, PhieuID, isTong) {
        var icountrows = 0;
        if (action == actionThietBi.edit) {
            icountrows = dataedit.items.length;
        }
        var mdladd = $('#mdlAddPhieuXuat').kendoWindow({
            width: "90%",
            height: "90%",
            title: GetTextLanguage("danhsachphieuxuat"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").maximize().open();
        mdladd.content(kendo.template($('#tempaddPhieuXuat').html()));


        //Init
        var dates = new Date(), ys = dates.getFullYear(), ms = dates.getMonth(), ds = dates.getDay(); hs = dates.getHours(); ms = dates.getMinutes();
        $("#ThoiGianNhap").kendoDateTimePicker({ value: dataedit == null ? new Date() : dataedit.ThoiGian, format: "dd/MM/yyyy HH:mm" });

        var dataobject = {};
        dataobject.DoiThiCongID = isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID");
        dataobject.CongTrinhID = isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID");
        dataobject.ThoiGian = $("#ThoiGianNhap").val();
        dataobject.DoiThiCongNhanID = "";
        dataobject.CongTrinhNhanID = "";
        dataobject.NguoiNhanID = "";
        dataobject.LoaiHinh = 0;
        dataobject.items = [];
        dataobject.Anh = [];
        dataobject.action = 10;
        dataobject.creatNhap = 0;

        var objrows = {
            ID: 0,
            VatTuID: "",
            TenVatTu: "",
            TenVietTat: "",
            TenDonVi: "",
            DonGia: 0,
            DonGiaNhap: 0,
            SoLuong: 0,
            ThanhTien: 0,
            SoLuongTon: 0,
        }

        $("#customers").kendoDropDownList({
            dataTextField: "TenVatTu",
            dataValueField: "VatTuID",
            filter: "contains",
            autoBind: false,
            optionLabel: "Chọn vật tư",
            template: "<div class='itemsVT'><h3>#: TenVatTu # -  #: TenVietTat #</h3><p>" + GetTextLanguage("gia") + ": <span>#: DonGiaGanNhat #</span>  - " + GetTextLanguage("ton") + " : <span>#: SoLuongTon #</span></div>",
            dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/LayDanhSachVatTuAdd", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Xuat }),
            change: function (e) {
                var value = this.value();
                if (value) {
                    var index = this.selectedIndex, dataItem;
                    if (this.options.optionLabel && index > 0) {
                        index = index;
                    }
                    dataItem = this.dataItem(index);
                    if (dataItem.get("SoLuongTon") > 0) {
                        //set rowsvalue
                        objrows.VatTuID = dataItem.get("VatTuID");
                        objrows.TenDonVi = dataItem.get("TenDonVi");
                        objrows.TenVatTu = dataItem.get("TenVatTu");
                        objrows.DonGia = dataItem.get("DonGiaGanNhat");
                        objrows.TenVietTat = dataItem.get("TenVietTat");
                        objrows.SoLuongTon = dataItem.get("SoLuongTon");
                    } else {
                        showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("soluongtonbang0nenkhongthexuat"));
                        this.value("");
                    }
                }
            }
        });

        $("#soluongs").kendoNumericTextBox({ value: 1, min: 1, format: "n2" });

        var cbbLoaiHinh = $("#loaihinhs").kendoDropDownList({
            dataTextField: "text",
            dataValueField: "val",
            autoBind: false,
            optionLabel: GetTextLanguage("loaihinhxuat"),
            dataSource: datafix_loaihinhxuat,
            change: function () {
                var value = this.value();
                if (value) {
                    if (value == 0) {
                        cbbCongTrinh.enable(true);
                        $("#nhapmois").prop("disabled", false);
                    }
                    else {
                        cbbCongTrinh.enable(false);
                        cbbDoiThiCong.enable(false);
                        cbbNguoiNhan.enable(false);
                        $("#nhapmois").prop("disabled", true);

                    }
                } else {
                    cbbCongTrinh.enable(false);
                    cbbDoiThiCong.enable(false);
                    cbbNguoiNhan.enable(false);
                    $("#nhapmois").prop("disabled", true);
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("banphaichonloaihinhxuat"));
                }
            }
        });

        var cbbCongTrinh = $("#congtrinhs").kendoDropDownList({
            //autoBind: false,
            //cascadeFrom: "inputCongTrinhDieuChuyen",
            filter: "contains",
            dataTextField: "TenCongTrinh",
            dataValueField: "CongTrinhID",
            optionLabel: GetTextLanguage("choncongtrinh"),
            dataSource: getDataDroplit(crudServiceBaseUrl, "/LayDanhSachCongTrinh"),
            change: function () {
                var value = this.value();
                if (value) {
                    console.log(value);
                    cbbDoiThiCong.enable(true);
                    $("#doithicongs").data("kendoDropDownList").dataSource.read({ CongTrinhId: value, DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID") });
                    $("#doithicongs").data("kendoDropDownList").refresh();

                } else {
                    cbbDoiThiCong.enable(false);
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("banphaichoncongtrinhnhan"));
                }
            }
        }).data("kendoDropDownList");
        cbbCongTrinh.enable(false);

        var cbbDoiThiCong = $("#doithicongs").kendoDropDownList({
            //autoBind: false,
            //cascadeFrom: "inputCongTrinhDieuChuyen",
            filter: "contains",
            dataTextField: "TenDoi",
            dataValueField: "DoiThiCongId",
            optionLabel: GetTextLanguage("chondoi"),
            dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/LayDanhSachDoiTheoCongTrinhPhieuXuat", { CongTrinhId: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID") }),
            change: function () {
                var value = this.value();
                if (value) {
                    cbbNguoiNhan.enable(true);
                    $("#nguoinhans").data("kendoDropDownList").dataSource.read({ DoiThiCongID: value });
                    $("#nguoinhans").data("kendoDropDownList").refresh();
                } else {
                    cbbNguoiNhan.enable(false);
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("banphaichondoinhanvattu"));
                }
            }
        }).data("kendoDropDownList");
        cbbDoiThiCong.enable(false);

        var cbbNguoiNhan = $("#nguoinhans").kendoDropDownList({
            // autoBind: false,
            //cascadeFrom: "inputCongTrinhDieuChuyen",
            filter: "contains",
            dataTextField: "TenNhanVien",
            dataValueField: "NhanVienID",
            optionLabel: GetTextLanguage("chonnhanvien"),
            template: "<h3>#: TenNhanVien #</h3><p>#: NgaySinh # - #: DiaChi #</p>",
            dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/LayDanhSachNhanVienTheoDoi", { DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID") }),
            change: function () {
                var value = this.value();
                if (value) {
                    objrows.NguoiNhanID = value;
                }
            }
        }).data("kendoDropDownList");
        cbbNguoiNhan.enable(false);

        if (action == actionThietBi.add) {
            if (!isTong) {
                setTitleWindow("mdlAddPhieuXuat", GetTextLanguage("themphieuxuatvattu") + " - " + GetTextLanguage("doi") + " :" + dataItem.get("TenDoi") + " / " + GetTextLanguage("thuoc") + ": " + dataItem.get("TenCongTrinh"));
            } else {
                setTitleWindow("mdlAddPhieuXuat", GetTextLanguage("themphieuxuatvattukhotong"));
            }
            $(".toolVatTu").append(tooLuu());
        } else {
            $(".windows8").css("display", "block");
            if (!isTong) {
                setTitleWindow("mdlAddPhieuXuat", GetTextLanguage("chitietphieuxuatvattu") + " - " + GetTextLanguage("doi") + " :" + dataItem.get("TenDoi") + " / " + GetTextLanguage("thuoc") + ": " + dataItem.get("TenCongTrinh"));
            } else {
                setTitleWindow("mdlAddPhieuXuat", GetTextLanguage("chitietphieuxuatvattuvekhotong"));
            }
            dataobject = dataedit;
            $("#ThoiGianNhap").data("kendoDateTimePicker").enable(false);
            $(".dshinhanhadd").html(loadslbuttonvatu(dataedit.AnhShow));
            showslideimg('abcds');
            if (dataedit.isHeThong == true) {
                $(".toolVatTu").append(tooLuu());
                $(".toolVatTu").append(tooXoa());
            }

            //$("#loaihinhs").val()
            $('#loaihinhs').data('kendoDropDownList').value(dataobject.LoaiHinh);
            if (dataobject.LoaiHinh == 0) {
                cbbCongTrinh.enable(true);
                cbbDoiThiCong.enable(true);
                cbbNguoiNhan.enable(true);
                $("#doithicongs").data("kendoDropDownList").dataSource.read({ CongTrinhId: dataobject.CongTrinhNhanID, DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID") });
                $("#doithicongs").data("kendoDropDownList").refresh();


                $("#nguoinhans").data("kendoDropDownList").dataSource.read({ DoiThiCongID: dataobject.DoiThiCongNhanID });
                $("#nguoinhans").data("kendoDropDownList").refresh();

                $("#nhapmois").prop("disabled", false);
                setTimeout(function () {
                    $('#congtrinhs').data('kendoDropDownList').value(dataobject.CongTrinhNhanID);
                    $("#doithicongs").data('kendoDropDownList').value(dataobject.DoiThiCongNhanID);
                    $("#nguoinhans").data('kendoDropDownList').value(dataobject.NguoiNhanID);
                    $(".windows8").css("display", "none");
                }, 1500);

            } else {
                $(".windows8").css("display", "none");
            }
        }

        var gridChiTietNhap = $("#gridChiTietXuat").kendoGrid({
            editable: true,
            selectable: true,
            dataSource: {
                data: dataobject.items,
                schema: {
                    model: {
                        id: "VatTuID",
                        fields: {
                            VatTuID: { editable: false, },
                            TenDonVi: { type: "string", editable: false },
                            TenVatTu: { type: "string", editable: false },
                            TenVietTat: { type: "string", editable: false },
                            SoLuong: { type: "number", editable: true, validation: { min: 1, format: "n2" } },
                            DonGia: { type: "number", editable: true, validation: { min: 0, format: "n0" } },
                            DonGiaNhap: { type: "number", editable: false, validation: { min: 0, format: "n0" } },
                            ThanhTien: { type: "number", editable: false } // If change editable to true, "set" work, but i can manualy change the value.
                        }
                    }
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
            },
            dataBound: function () {
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });

                if (dataedit != null) {
                    //check hiển thị nút xóa dòng
                    $("#gridChiTietXuat tbody tr .xoaitem").each(function () {
                        var currentDataItem = $("#gridChiTietXuat").data("kendoGrid").dataItem($(this).closest("tr"));
                        ////Check in the current dataItem if the row is deletable
                        if (dataedit.isHeThong != true) {
                            //laf heej thoong thif ms duoc suar va xoa
                            $(this).remove();
                        }
                    });
                }
            },
            columns:
            [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                { field: "VatTuID", hidden: true },
                { field: "TenVatTu", width: 200, title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, footerTemplate: GetTextLanguage("tong"), filterable: FilterInTextColumn },
                { field: "TenVietTat", width: 200, title: GetTextLanguage("tenviettat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn },
                { field: "TenDonVi", width: 200, title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn },
                { field: "DonGiaNhap", width:150, title: GetTextLanguage("gianhap"), format: "{0:n0}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn },
                { field: "DonGia",width:150, title: GetTextLanguage("giaxuat"), format: "{0:n0}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn },
                { field: "SoLuong", width: 100, title: GetTextLanguage("soluong"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), format: "{0:n0}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    aggregates: ["sum"], footerTemplate: "<div style='text-align:right !important;'> #: kendo.toString(sum, \"n0\") #</div>"
                },
                {
                    command: [
                        { text:GetTextLanguage("xoa"), click: xoarow, className: "xoaitem" }
                    ], width: 100
                },
            ],
            save: function (e) {
                if (e.values.SoLuong || e.values.DonGia) {
                    var sl = e.values.SoLuong || e.model.SoLuong;
                    var dg = e.values.DonGia || e.model.DonGia;
                    for (var i = 0; i < dataobject.items.length; i++) {
                        if (dataobject.items[i].ID === e.model.get("ID")) {
                            if (e.values.SoLuong) {
                                dataobject.items[i].SoLuong = sl;
                            }
                            else {
                                dataobject.items[i].DonGia = dg;
                            }
                            dataobject.items[i].ThanhTien = sl * dg
                            gridReload("gridChiTietXuat", {});
                            break;
                        }
                    }

                }
            }
        }).data("kendoGrid");
        resizeWindow2("gridChiTietXuat", "mdlAddPhieuXuat", 340, 503);

        function xoarow(e) {
            e.preventDefault();
            var dataItemrow = this.dataItem($(e.currentTarget).closest("tr"));
            for (var i = 0; i < dataobject.items.length; i++) {
                if (dataobject.items[i].VatTuID === dataItemrow.get("VatTuID")) {
                    dataobject.items.splice(i, 1);
                    gridReload("gridChiTietXuat", {});
                    break;
                }
            }
        }

        // add row
        $("#addrow").click(function () {
            var checkedIds = {};
            var mdl = $('#mdlDanhSachVatTuNhapXuat').kendoWindow({
                width: "80%",
                height: "80%",
                title: GetTextLanguage("danhsachvattu"),
                modal: true,
                visible: false,
                resizable: false,
                actions: [
                    "Close"
                ],
                deactivate: function () {
                    $(this.element).empty();
                }
            }).data("kendoWindow").center().open();
            mdl.content(kendo.template($('#tempDanhSachVatTuNhapXuat').html()));
            setTitleWindow("mdlDanhSachVatTuNhapXuat", GetTextLanguage("danhsachvattu"));

            var gridDanhSachVatTuNhapXuat = $("#gridDanhSachVatTuNhapXuat").kendoGrid({
                dataSource: getDataDanhSachVatTu(crudServiceBaseUrl, "/LayDanhSachVatTuXuat", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID") }),
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                editable: true,
                pageable: {
                    refresh: true,
                    buttonCount: 5,
                    pageSizes: true,
                    messages: messagegrid
                },
                height: window.innerHeight * 0.8,
                width: "100%",
                dataBound: onDataBound,
                //toolbar: kendo.template($("#btnThem").html()),
                columns: [
                    {
                        field: "select",
                        title: "&nbsp;",
                        template: "<input type='checkbox' class='checkbox' />",
                        sortable: false,
                        filterable: false,
                        width: 32
                    },
                    {
                        title: GetTextLanguage("stt"),
                        template: "<span class='stt'></span>",
                        width: 60,
                        align: "center"
                    },
                    {
                        field: "TenNhom", title: GetTextLanguage("nhom"), hidden: true
                    },
                    {
                        field: "TenVatTu", title: GetTextLanguage("vattu"), attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "TenDonVi", title: GetTextLanguage("donvi"), width: 100,
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "SoLuongTon", width: 100, title: GetTextLanguage("soluongton"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn
                    },
                    {
                        field: "DonGiaNhap", width: 150, title: GetTextLanguage("dongianhap"), attributes: { "style": "text-align:right !important;" }, format: "{0:n0}", filterable: FilterInColumn
                    },
                    {
                        field: "DonGia", width: 150, title: GetTextLanguage("giaxuat"), attributes: { "style": "text-align:right !important;" }, format: "{0:n0}", filterable: FilterInColumn
                    },
                    {
                        field: "SoLuong", width: 100, title: GetTextLanguage("soluong"), format: "{0:n2}", filterable: FilterInColumn, attributes: { "class": "soluongcheck" }
                    },

                    //{ command: "destroy", title: "&nbsp;", width: 150 }
                ]
            }).data("kendoGrid");

            resizeWindow("gridDanhSachVatTuNhapXuat", "mdlDanhSachVatTuNhapXuat");
            gridDanhSachVatTuNhapXuat.table.on("click", ".checkbox", selectRow);
            gridDanhSachVatTuNhapXuat.table.on("click", ".soluongcheck", selectRowSoLuong);
            function selectRow() {
                console.log(this.checked);
                var checked = this.checked,
                    rows = $(this).closest("tr"),
                    grids = $("#gridDanhSachVatTuNhapXuat").data("kendoGrid"),
                    dataItems = grids.dataItem(rows);
                checkedIds[dataItems.uid] = checked;
                if (checked) {
                    //-select the row
                    rows.addClass("k-state-selected");
                } else {
                    //-remove selection
                    rows.removeClass("k-state-selected");
                }
            }

            function selectRowSoLuong() {
                var rows = $(this).closest("tr"),
                    grids = $("#gridDanhSachVatTuNhapXuat").data("kendoGrid"),
                    dataItems = grids.dataItem(rows);
                checkedIds[dataItems.uid] = true;
                rows.addClass("k-state-selected").find(".checkbox").attr("checked", "checked");

            }

            function onDataBound(e) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (checkedIds[view[i].uid]) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                    }
                }

                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            }

            $("#btn-add-vt").on("click", function () {
                var grid = $("#gridDanhSachVatTuNhapXuat").data("kendoGrid");
                var selectedRows = $(".k-state-selected", "#gridDanhSachVatTuNhapXuat");
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length - 1; i++) {
                        var selectedItem = grid.dataItem(selectedRows[i]);
                        var slton = selectedItem.SoLuongTon == null ? 0 : selectedItem.SoLuongTon;
                        var sl = selectedItem.SoLuong == null ? 0 : selectedItem.SoLuong;
                        if (slton < sl) {
                            showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("soluongtontaidongthu") + " " + (i + 1) + " " + GetTextLanguage("khongdudexuat"));
                            return;
                        }
                    }
                }
                if (selectedRows.length > 0) {
                    for (var i = 0; i < selectedRows.length - 1; i++) {
                        var selectedItem = grid.dataItem(selectedRows[i]);
                        var dg = selectedItem.DonGia == null ? 0 : selectedItem.DonGia;
                        var dgn = selectedItem.DonGiaNhap == null ? 0 : selectedItem.DonGiaNhap;
                        var sl = selectedItem.SoLuong == null ? 0 : selectedItem.SoLuong;
                        console.log(selectedItem);
                        var bool = true;
                        for (var k = 0; k < dataobject.items.length; k++) {
                            if (dataobject.items[k].VatTuID === selectedItem.VatTuID && dataobject.items[k].DonGiaNhap === dgn) {
                                dataobject.items[k].SoLuong = (parseFloat(dataobject.items[k].SoLuong) + parseFloat(sl));
                                dataobject.items[k].ThanhTien = (parseFloat(dataobject.items[k].SoLuong) * parseFloat(dataobject.items[k].DonGia));
                                bool = false;
                                break;
                            }
                        }

                        if (bool) {
                            icountrows++;
                            dataobject.items.push({
                                ID: icountrows,
                                VatTuID: selectedItem.VatTuID,
                                TenVatTu: selectedItem.TenVatTu,
                                TenVietTat: selectedItem.TenVietTat,
                                TenDonVi: selectedItem.TenDonVi,
                                DonGia: dg,
                                DonGiaNhap: dgn,
                                SoLuong: sl,
                                ThanhTien: (parseFloat(dg) * parseFloat(sl)),
                            });

                        }
                    }
                    gridReload("gridChiTietXuat", {});
                    closeModel("mdlDanhSachVatTuNhapXuat");
                }
            });

           

        });

        //Lưu Phiếu
        $("#saveas").click(function () {
            var x = confirm(GetTextLanguage("luuhaykhong"));
            if (x) {
                if (dataobject.items.length == 0) {
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("khongcovattunaotrenphieu"));
                }
                else {
                    if ($("#loaihinhs").val() == "") {
                        showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("banphaichonloaihinhxuat"));
                    }
                    else {
                        var booladd = true;
                        if ($("#loaihinhs").val() == 0 && ($("#congtrinhs").val() == "" || $("#doithicongs").val() == "" || $("#nguoinhans").val() == "")) {
                            showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("banphaichoncongtringvadoinhanvattunguoinhan"));
                            booladd = false;
                        }

                        //Check lại số lượng tồn 1 lần nữa
                        for (var i = 0; i < dataobject.items.length; i++) {
                            if (dataobject.items[i].SoLuongTon < dataobject.items[i].SoLuong) {
                                showToast("error", GetTextLanguage("canhbao"), dataobject.items[i].TenVatTu + GetTextLanguage("odongthu") + (i + 1) + GetTextLanguage("chicon") + (" + dataobject.items[i].SoLuongTon + ") + GetTextLanguage("khongdusoluongtondexuat"));
                                booladd = false;
                                break;
                            }
                        }

                        dataobject.ThoiGian = $("#ThoiGianNhap").val();
                        dataobject.DoiThiCongNhanID = $("#doithicongs").val();
                        dataobject.CongTrinhNhanID = $("#congtrinhs").val();
                        dataobject.NguoiNhanID = $("#nguoinhans").val();
                        dataobject.LoaiHinh = $("#loaihinhs").val();
                        if ($("#nhapmois").is(':checked'))
                            dataobject.creatNhap = 1; // checked
                        else
                            dataobject.creatNhap = 0;

                        if (booladd == true) {
                            $(".windows8").css("display", "block");
                            var dataimg = new FormData();
                            var ins = storedFiles.length;
                            if (ins > 0) {
                                for (var x = 0; x < ins; x++) {
                                    dataimg.append("uploads", storedFiles[x]);
                                }

                            }

                            var objXhr = new XMLHttpRequest();
                            objXhr.onreadystatechange = function () {
                                if (objXhr.readyState == 4) {
                                    if (objXhr.responseText != "") {
                                        var strUrl = objXhr.responseText.split(',');
                                        for (var k = 0; k < strUrl.length; k++) {
                                            dataobject.Anh.push(strUrl[k]);
                                        }
                                    }
                                    ContentWatingOP("mdlAddPhieuXuat", 0.9);
                                    setTimeout(function () {
                                        $.ajax({
                                            cache: false,
                                            async: false,
                                            type: "POST",
                                            url: crudServiceBaseUrl + "/AddorUpdatePhieuXuat",
                                            data: { data: JSON.stringify(dataobject), PhieuID: PhieuID },
                                            success: function (data) {
                                                altReturn(data);
                                                if (data.code == "success") {
                                                    gridReload("gridNhapXuatKho", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Xuat });
                                                    closeModel("mdlAddPhieuXuat");
                                                    $("#mdlAddPhieuXuat").unblock();
                                                    //remove file anh
                                                    storedFiles = [];
                                                }
                                                $(".windows8").css("display", "none");
                                            },
                                            error: function (xhr, status, error) {
                                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                                $("#mdlAddPhieuXuat").unblock();
                                                $(".windows8").css("display", "none");
                                            }
                                        });
                                    }, 1000);
                                }
                            }

                            objXhr.open("POST", crudServiceBaseUrlUploadFile + "/Uploadfiles", true);
                            objXhr.send(dataimg);

                        }
                    }
                }
            } else {
                return false;
            }
        });

        //Xóa Phiếu
        $("#btnXoa").click(function () {
            var x = confirm(GetTextLanguage("bancomuonxoaphieunaykhong"));
            if (x) {
                $(".windows8").css("display", "block");
                ContentWatingOP("mdlAddPhieuXuat", 0.9);
                setTimeout(function () {
                    $.ajax({
                        cache: false,
                        async: false,
                        type: "POST",
                        url: crudServiceBaseUrl + "/XoaPhieuXuat",
                        data: { PhieuXuatVatTuID: PhieuID, CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID") },
                        success: function (data) {
                            altReturn(data);
                            if (data.code == "success") {
                                gridReload("gridNhapXuatKho", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Nhap });
                                closeModel("mdlAddPhieuXuat");
                                $("#mdlAddPhieuXuat").unblock();
                            }
                            $(".windows8").css("display", "none");
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $(".windows8").css("display", "none");
                            $("#mdlAddPhieuXuat").unblock();
                        }
                    });
                }, 500);
            } else {
                return false;
            }
        });

        //Chinhr suwar anh khi loadlaij
        $("body").on("click", ".removeLoad", function () {
            var file = $(this).data("file");
            for (var i = 0; i < dataobject.Anh.length; i++) {
                if (dataobject.Anh[i] === file) {
                    dataobject.Anh.splice(i, 1);
                    break;
                }
            }
            $(this).parent().remove();
        });
    }

    function detailInitNhap(e) {
        var detailRow = e.detailRow;
        detailRow.find(".chitiets").kendoGrid({
            dataSource: {
                transport: {
                    read: {
                        url: crudServiceBaseUrl + "/LayDanhSachPhieuNhap",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: { CongTrinhID: currentCongTrinhID, DoiThiCongID: currentDoiThiCongID, LoaiHinh: LoaiHinhNhapXuat.Nhap }
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                batch: true,
                //group: groups,
                schema: {
                    data: "datadetails",
                    total: "totaldetails"
                },
                //serverSorting: true,
                //serverFiltering: true,
                filter: { field: "PhieuNhapVatTuID", operator: "eq", value: e.data.PhieuNhapVatTuID }
            },
            scrollable: false,
            sortable: true,
            columns: [
                { field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200 },
                { field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200 },
                { field: "SoLuong", title: GetTextLanguage("soluong"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100 },
                { field: "DonGia", title: GetTextLanguage("dongia"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150 },
                { field: "ThanhTien", title: GetTextLanguage("thanhtien"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150 }
            ]
        });
    }

    function detailInitXuat(e) {
        var detailRow = e.detailRow;

        detailRow.find(".chitiets").kendoGrid({
            dataSource: {
                transport: {
                    read: {
                        url: crudServiceBaseUrl + "/LayDanhSachPhieuNhap",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: { CongTrinhID: currentCongTrinhID, DoiThiCongID: currentDoiThiCongID, LoaiHinh: LoaiHinhNhapXuat.Xuat }
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                batch: true,
                //group: groups,
                schema: {
                    data: "datadetails",
                    total: "totaldetails"
                },
                filter: { field: "PhieuXuatVatTuID", operator: "eq", value: e.data.PhieuXuatVatTuID }
            },
            scrollable: false,
            sortable: true,
            columns: [
                { field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200 },
                { field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200 },
                { field: "SoLuong", title: GetTextLanguage("soluong"), format: "{0:n2}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100 },
                { field: "DonGia", title: GetTextLanguage("dongia"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150 },
                { field: "ThanhTien", title: GetTextLanguage("thanhtien"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150 }
            ]
        });
    }

    function kiemtratonkho2(dataItem) {
        var mdl = $('#mdlTonVatTu').kendoWindow({
            width: "80%",
            height: "80%",
            title: GetTextLanguage("danhsachphieunhap"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#tempTonVatTu').html()));


        setTitleWindow("mdlTonVatTu", GetTextLanguage("baocaoton") + " - " + GetTextLanguage("doi") + " :" + dataItem.get("TenDoi") + " / " + GetTextLanguage("thuoc") + ": " + dataItem.get("TenCongTrinh"));

        var gridTonVatTu = $("#gridTonVatTu").kendoGrid({
            dataSource: getDataSourceGridSum(crudServiceBaseUrl, "/GetTonVatTu", { CongTrinhID: dataItem.get("CongTrinhID"), DoiThiCongID: dataItem.get("DoiThiCongID") }, "TenNhom", [{ field: "GiaTriTon", aggregate: "sum" }]),
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            /*pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },*/
            height: window.innerHeight * 0.8,
            width: "100%",
            toolbar: kendo.template($("#btnQuanLy").html()),
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    lockable: false, align: "center"
                },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {

                    field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {

                    field: "DonGiaGanNhat", title: GetTextLanguage("gianhap"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                },
                {

                    field: "GiaXuatGanNhat", title: GetTextLanguage("giaxuat"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                },
                {
                    field: "TenNhom", title: GetTextLanguage("nhom"), hidden: true
                },
                {
                    title: GetTextLanguage("soluong"),
                    columns: [
                        {
                            field: "TongNhap", title: GetTextLanguage("nhap"), format: "{0:n2}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                        },
                        {
                            field: "TongXuat", title: GetTextLanguage("xuat"), format: "{0:n2}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                        },
                        {
                            field: "SoLuongTon", title: GetTextLanguage("ton"), format: "{0:n2}", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                        },
                    ]
                },
                {
                    title: GetTextLanguage("gia"),
                    columns: [
                        {
                            field: "GiaNhap", title: GetTextLanguage("nhap"), format: "{0:n0}",
                            attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150
                        },
                        {
                            field: "GiaXuat", title: GetTextLanguage("xuat"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" },
                            attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150

                        },
                        {
                            field: "GiaTriTon", title: GetTextLanguage("ton"), format: "{0:n0}", attributes: { "style": "text-align:right !important;" },
                            attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                            aggregates: ["sum"], footerTemplate: "<div style='text-align:right !important;'> #: kendo.toString(sum, \"n0\") #</div>"
                        },
                    ]
                },
            ],
            dataBound: function (e) {
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });

        resizeWindow3("gridTonVatTu", "mdlTonVatTu");
        $(".xemdoi[data-tinhtrang=0]").addClass("selected");
        $('#gridTonVatTu #btnQlNhap').click(function () {
            quanlynhapkho(dataItem, false);

        });
        $('#gridTonVatTu #btnQlXuat').click(function () {
            quanlyxuatkho(dataItem, false);
        });
        $('#gridTonVatTu #btnexcel').click(function () {
            console.log(dataItem);
            var url = "/QuanLyVatTu/ExportToExcelBaoCaoTon?CongTrinhID=" + dataItem.get("CongTrinhID") + "&DoiThiCongID=" + dataItem.get("DoiThiCongID");
            location.href = url;
        });
    }

    $("#btnKhoTong").click(function () {
        var mdl = $('#mdldanhsachcongtrinh').kendoWindow({
            width: "90%",
            height: "90%",
            title: GetTextLanguage("danhsachphieunhap"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        mdl.content(kendo.template($('#tempTonTheoDoi').html()));

        setTitleWindow("mdldanhsachcongtrinh", GetTextLanguage("baocaotontheocongtrinh"));
        var grid = loaddata("/LayDanhSachDoiThiCong");
        $(".xemdoi").click(function () {
            var tinhtrang = $(this).data("tinhtrang");

            if (tinhtrang == undefined) {
                tinhtrang = null;
            } else {
                $(".xemdoi").removeClass("selected");
                $(this).addClass("selected");
            }
            gridReload("grid", { tinhtrang: tinhtrang });

        });

    });

    function loaddata(url) {
        var g = $("#grid").kendoGrid({
            dataSource: getDataSourceGrid(crudServiceBaseUrl, url, { tinhtrang: 0 }, "TenDoi"),
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            height: window.innerHeight * 0.8,
            width: "100%",
            toolbar: kendo.template($("#btnThem").html()),
            columns: [
                //{
                //    command: [
                //        { text: " Kiểm tra tồn kho", click: kiemtratonkho, className: "fa fa-list-alt" },
                //        { text: " Quản lý nhập kho", click: quanlynhapkho, className: "fa fa-download" },
                //        { text: " Quản lý xuất kho", click: quanlyxuatkho, className: "fa fa-upload" }
                //    ], width: 600
                //},
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    lockable: false, align: "center"
                },
                {
                    template: '<div class="point">#:TenDoi#</div>',
                    field: "TenDoi", title: GetTextLanguage("doi"), hidden: true, attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    template: '<div class="point">#:TenCongTrinh#</div>',
                    field: "TenCongTrinh", title: GetTextLanguage("congtrinh"),
                    attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    template: '<div class="point">#:DiaDiem#</div>',
                    field: "DiaDiem", title: GetTextLanguage("diachi"),
                    attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    template: '<div class="point">#=kendo.toString(NgayKhoiCong, "dd/MM/yyyy" )#</div>',
                    field: "NgayKhoiCong", title: GetTextLanguage("ngaybatdau"),
                    attributes: alignCenter, filterable: FilterInColumn, width: 150
                },
                {
                    template: '<div class="point">#= kendo.toString(NgayKetThuc, "dd/MM/yyyy" )#</div>',
                    field: "NgayKetThuc", title: GetTextLanguage("ngayketthuc"),
                    attributes: alignCenter, filterable: FilterInColumn, width: 150
                },
                {
                    template: '<div class="point">#:ChuDauTu#</div>',
                    field: "ChuDauTu", title: GetTextLanguage("chudautu"),
                    attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    template: '<div class="point">#= GhiChu #</div>',
                    field: "GhiChu",
                    title: GetTextLanguage("ghichu"),
                    attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                },

                //{ command: "destroy", title: "&nbsp;", width: 150 }
            ],
            dataBound: function (e) {
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
                e.sender.tbody.find(".k-button.fa-list-alt").each(function (idx, element) {
                    $(element).removeClass("fa fa-list-alt").find("span").addClass("fa fa-list-alt");
                });
                e.sender.tbody.find(".k-button.fa-download").each(function (idx, element) {
                    $(element).removeClass("fa fa-download").find("span").addClass("fa fa-download");
                });
                e.sender.tbody.find(".k-button.fa-upload").each(function (idx, element) {
                    $(element).removeClass("fa fa-upload").find("span").addClass("fa fa-upload");
                });


                e.sender.tbody.find(".k-button").each(function (idx, element) {
                    $(element).removeClass("k-button k-button-icontext").addClass("btn btn-primary btn-action-grid");
                });
            },
            selectable: "row",
            change: function (e) {
                kiemtratonkho2(this.dataItem(this.select()));
            }
        });
        return g;
    }

    function NhaccDropDownEditor(container, options) {
        $('<input name="' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                autoBind: false,
                dataTextField: "NhaCCName",
                dataValueField: "NhaCCID",
                dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/LayDanhSachNhaCungCap", {})
            });
    }
});

function XuatExcelQLVT() {
    var tinhtrang = $('#tinhtrang').val();
    var url = "/QuanLyVatTu/ExportToExcelQuanLyVatTu?TinhTrang=" + tinhtrang;
    location.href = url;
};

