﻿function getNoidungGroupPhieuNhapVatTuKhoDonVi(value) {
    var datasource = $("#gridPhieuNhapVatTuKhoDonVi").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[0].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupPhieuNhapVatTuKhoTong(value) {
    var datasource = $("#gridPhieuNhapVatTuKhoTong").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[0].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};

$(document).ready(function () {
    var CurrentController = "/QuanLyPhieuNhapVatTu";
    var dataLoaiHinhNhap = [
        { text: GetTextLanguage("tatca"), value: "0" },
        { text: GetTextLanguage("dieuchuyen"), value: "1" },
        { text: GetTextLanguage("mua"), value: "2" }
    ];

    $("#tabstripPhieuNhap").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        },
        select: function (e) {
            var tab = e.item.id;
            if (tab === "tab_khotong") PhieuNhapVatTuKhoTong();
        }
    });
    PhieuNhapVatTuKhoDonVi();
    function PhieuNhapVatTuKhoDonVi() {
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                var value = this.value();
                setDataSourcePhieuNhapVatTuKhoDonVi();
            }
        });
        $("#inputDenNgay").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                setDataSourcePhieuNhapVatTuKhoDonVi();
            }
        });
        $("#drdCongTrinh").kendoDropDownList({
            dataTextField: 'TenCongTrinh',
            dataValueField: 'CongTrinhID',
            filter: "contains",
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: CurrentController + "/GetAllDataCongTrinhDonVi",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                    },
                },
                schema: {
                    type: "json",
                    data: "data"
                }
            }),
            dataBound: function (e) {
                var valueDoiID = this.value();
                if (valueDoiID) {
                    $("#drdDoiThiCong").data("kendoDropDownList").dataSource.read({ CongTrinhID: valueDoiID });
                }
            },
            change: function (e) {
                var value = this.value();
                if (value) {
                    $("#drdDoiThiCong").data("kendoDropDownList").dataSource.read({ CongTrinhID: value });
                }
            },
        });
        $("#drdDoiThiCong").kendoDropDownList({
            dataTextField: 'TenDoiThiCong',
            dataValueField: 'DoiThiCongID',
            filter: "contains",
            index: 0,
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: CurrentController + "/GetAllDataDoiThiCong",
                        dataType: "json",
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#drdCongTrinh").data("kendoDropDownList").value()
                        },
                        complete: function (e) {
                            if (e.responseJSON.data.length > 0) {
                                $("#drdDoiThiCong").data("kendoDropDownList").select(0);
                                setDataSourcePhieuNhapVatTuKhoDonVi();
                            }
                        }
                    },
                },
                schema: {
                    type: "json",
                    data: "data"
                }
            }),
            change: function (e) {
                var valueDoiID = this.value();
                if (valueDoiID) {
                    setDataSourcePhieuNhapVatTuKhoDonVi();
                }
            },
        });

        function setDataSourcePhieuNhapVatTuKhoDonVi() {
            var dataPhieuNhapVatTuKhoDonVi = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: CurrentController + "/GetDataPhieuNhapVatTuKhoDonVi",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#drdCongTrinh").data("kendoDropDownList").value(),
                            DoiThiCongID: $("#drdDoiThiCong").data("kendoDropDownList").value(),
                            LoaiHinhNhap: 2,
                            TuNgay: $("#inputTuNgay").val(),
                            DenNgay: $("#inputDenNgay").val()
                        },
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                pageSize: 25,
                schema: {
                    data: "data",
                    total: "total"
                },
                group: {
                    field: "ThoiGian",
                    dir: "desc",
                    aggregates: [{
                        field: "ThanhTien",
                        aggregate: "sum"
                    }]
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
            });
            $("#gridPhieuNhapVatTuKhoDonVi").data("kendoGrid").setDataSource(dataPhieuNhapVatTuKhoDonVi);
        };
        $("#gridPhieuNhapVatTuKhoDonVi").kendoGrid({
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: window.innerHeight * 0.68,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"),
                    attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupPhieuNhapVatTuKhoDonVi(value)#</scpan>" + "<div class='pull-right'><button class='btn btn-xs btn-primary xemchitiet'>" + GetTextLanguage("xemchitiet") + "</button></div>",
                },
                {
                    field: "MaPhieu", title: GetTextLanguage("maphieu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    //footerTemplate: "<div style='float:right;color:red'>#=sum==null?0:kendo.toString(sum,'n2')# </div>",
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(DonGia,'n2') #",
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                    attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(ThanhTien,'n2') #",
                    //footerTemplate: "<div style='float:right;color:red'>#=sum==null?0:kendo.toString(sum,'n2')# </div>",
                },
                {
                    field: "NhanVienNhap", title: GetTextLanguage("nhanviennhap"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "GhiChu", title: GetTextLanguage("ghichu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                }
            ],
            dataBound: function (e) {
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });

        $("#gridPhieuNhapVatTuKhoDonVi").on("click", ".xemchitiet", function () {
            var dataItem = $("#gridPhieuNhapVatTuKhoDonVi").data("kendoGrid").dataItem($(this).closest("tr"));
            var LoaiNhap = 1;
            if (dataItem.CongTrinhDieuChuyenID === null && dataItem.DoiThiCongDieuChuyenID === null) {
                LoaiNhap = 2;
            }
            CreateModalWithSize("mdlChiTietPhieuNhap", "90%", "90%", null, "Chi Tiết Phiếu Nhập");
            $("#mdlChiTietPhieuNhap").load("/QuanLyVatTuKhoTong" + "/PopupChiTietPhieuNhapVatTu?PhieuNhapVatTuID=" + dataItem.PhieuNhapVatTuID + "&LoaiNhap=" + LoaiNhap + "&PhieuXuatVatTuID=" + dataItem.PhieuXuatVatTuID);

        });
        $("#btnThemPhieuNhapKhoDonVi").click(function () {
            CreateModalWithSize("mdlThemPhieuNhapKhoDonVi", "90%", "90%", null, "Xác Nhận Nhập");
            $("#mdlThemPhieuNhapKhoDonVi").load("/QuanLyVatTuKhoDonVi/PopupNhapVatTuKhoDonVi");
        });
    }

    function PhieuNhapVatTuKhoTong() {
        $("#ipLoaiHinhNhap").kendoDropDownList({
            dataSource: dataLoaiHinhNhap,
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var value = this.value();
                $("#gridPhieuNhapVatTuKhoTong").data("kendoGrid").dataSource.read({ LoaiHinhNhap: value, TuNgay: $("#inputTuNgay2").val(), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay2").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                var value = this.value();
                gridReload("gridPhieuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        $("#inputDenNgay2").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridPhieuNhapVatTuKhoTong", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });
        var dataPhieuNhapVatTuKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: CurrentController + "/GetDataPhieuNhapVatTuKhoTong",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        LoaiHinhNhap: 0,
                        TuNgay: $("#inputTuNgay2").val(),
                        DenNgay: $("#inputDenNgay2").val()
                    }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total",
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
        });
        $("#gridPhieuNhapVatTuKhoTong").kendoGrid({
            dataSource: dataPhieuNhapVatTuKhoTong,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: window.innerHeight * 0.68,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupPhieuNhapVatTuKhoTong(value)#</scpan>" + "<div class='pull-right'><button class='btn btn-xs btn-primary xemchitiet'> " + GetTextLanguage("xemchitiet") + "</button></div>",
                },
                {
                    field: "MaPhieu", title: GetTextLanguage("maphieu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(DonGia,'n2') #"
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#=kendo.toString(ThanhTien,'n2') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "DoiTac", title: GetTextLanguage("doitac"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "NhanVienNhap", title: GetTextLanguage("nhanviennhap"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                }
            ],
            dataBound: function (e) {
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });
        $("#gridPhieuNhapVatTuKhoTong").on("click", ".xemchitiet", function () {
            var dataItem = $("#gridPhieuNhapVatTuKhoTong").data("kendoGrid").dataItem($(this).closest("tr"));
            var LoaiNhap = 1;
            if (dataItem.CongTrinhDieuChuyenID === null && dataItem.DoiThiCongDieuChuyenID === null) {
                LoaiNhap = 2;
            }
            CreateModalWithSize("mdlChiTietPhieuNhap", "90%", "90%", null, "Chi Tiết Phiếu Nhập");
            $("#mdlChiTietPhieuNhap").load("/QuanLyVatTuKhoTong" + "/PopupChiTietPhieuNhapVatTu?PhieuNhapVatTuID=" + dataItem.PhieuNhapVatTuID + "&LoaiNhap=" + LoaiNhap + "&PhieuXuatVatTuID=" + dataItem.PhieuXuatVatTuID);
        });
        $("#btnThemPhieuNhapKhoTong").click(function () {
            CreateModalWithSize("mdlThemPhieuNhapKhoTong", "90%", "90%", null, GetTextLanguage("xacnhannhap"));
            $("#mdlThemPhieuNhapKhoTong").load("/QuanLyVatTuKhoTong/PopupNhapVatTuKhoTong");
        });
    }
});