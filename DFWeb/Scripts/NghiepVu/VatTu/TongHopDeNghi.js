﻿$(document).ready(function () {

    var datatrangthai = [
      { text: "Chưa duyệt", val: 0 },
      { text: "Đã duyệt", val: 1 },
    ];
    $("#inputTrangThai").kendoDropDownList({
        dataTextField: "text",
        dataValueField: "val",
        autoBind: false,
        optionLabel: "Tất cả trạng thái",
        dataSource: datatrangthai,
        change: function () {
            gridReload("BangTPNV", { CongTrinhID: $("#inputCongTrinhID").val(), DoiThiCongID: $("#inputDoiThiCongID").val(), TrangThai: this.value(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });
    $("#inputCongTrinhID").kendoDropDownList({
        dataTextField: "TenCongTrinh",
        dataValueField: "CongTrinhId",
        autoBind: false,
        optionLabel: "Tất cả Dự Án",
        dataSource: getDataDroplit("/TongHopDeNghiVatTu", "/LayDanhSachCongTrinh"),
        change: function () {
            $("#inputDoiThiCongID").data("kendoDropDownList").dataSource.read({ CongTrinhID: this.value() });
            $("#inputDoiThiCongID").data("kendoDropDownList").refresh();
            gridReload("BangTPNV", { CongTrinhID: $("#inputCongTrinhID").val(), DoiThiCongID: $("#inputDoiThiCongID").val(), TrangThai: $("#inputTrangThai").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });

    $("#inputDoiThiCongID").kendoDropDownList({
        dataTextField: "TenDoi",
        dataValueField: "DoiThiCongID",
        autoBind: false,
        optionLabel: "Tất cả đội",
        dataSource: getDataDroplitwithParam("/TongHopDeNghiVatTu", "/LayDanhSachDoiThiCong", { CongTrinhID: $("#inputCongTrinhID").val() }),
        change: function () {
            gridReload("BangTPNV", { CongTrinhID: $("#inputCongTrinhID").val(), DoiThiCongID: $("#inputDoiThiCongID").val(), TrangThai: $("#inputTrangThai").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });
    var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
    $("#inputTuNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(y - 3, m, 1), change: function () {
            gridReload("BangTPNV", { CongTrinhID: $("#inputCongTrinhID").val(), DoiThiCongID: $("#inputDoiThiCongID").val(), TrangThai: $("#inputTrangThai").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });
    $("#inputDenNgay").kendoDatePicker({
        format: "dd/MM/yyyy", value: new Date(), change: function () {
            gridReload("BangTPNV", { CongTrinhID: $("#inputCongTrinhID").val(), DoiThiCongID: $("#inputDoiThiCongID").val(), TrangThai: $("#inputTrangThai").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
        }
    });

   
    $("#BangTPNV").kendoGrid({
        width: "100%",
        height: heightGrid * 0.85,
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: "/TongHopDeNghiVatTu/TongHopDeNghiVatTu",
                    dataType: "json",
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                }
            },
            group: {
                field: "TenVatTu",
                dir: "desc",
                aggregates: [{
                    field: "SoLuong",
                    aggregate: "sum"
                }]
            },
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "VatTuID",
                    fields: {
                        VatTuID: { type: "string" },
                        TenVatTu: { type: "string", editable: false },
                        TenVietTat: { type: "string", editable: false },
                        TenDonVi: { type: "string", editable: false },
                        SoLuong: { type: "number" },
                        DonGia: { type: "number", editable: false },
                        ThoiGian: { type: "date", editable: false },
                    }
                }
            },
            change: function (e) {
                var data = this.data();
            }
        }),
        navigatable: true,
        filterable: {
            mode: "row"
        },
        sortable: true,
        pageable:
            {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
        dataBinding: function () {
            record = 0;
        },
        columns: [
           {
               title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
               template: "#= ++record #",
               width: 60,
               attributes: alignCenter
           },
            {
                field: "ThoiGian",
                title: "Thời Gian",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                format:"{0:dd/MM/yyyy}"
            },
            {
                field: "TenVatTu",
                title: "Tên Vật Tư",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            {
                field: "TenVietTat",
                title: "Mã vật tư",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignCenter,
            },
             {
                 field: "TenDonVi",
                 title: "Đơn vị",
                 width: 150,
                 filterable: FilterInTextColumn,
                 attributes: alignCenter,
                 groupFooterTemplate: "Tổng "
             },
            {
                field: "SoLuong",
                title: "Số Lượng",
                width: 80,
                filterable: FilterInTextColumn,
                attributes: alignRight,
                groupFooterTemplate: "<span style='color:red;float:right'> #=sum# </span>"
            },
            
            {
                field: "MaDeNghi",
                title: "Đề Nghị",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignCenter
            },
            {
                field: "TrangThai",
                title: "Trạng Thái",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                template: kendo.template($("#tempTrangThai").html())
            }, {
                field: "TenVatTu", title: "Vật Tư", hidden: true
            }, {
                field: "TenDoi",
                title: "Đội",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignCenter,
            }, {
                field: "TenCongTrinh",
                title: "Dự Án",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignCenter,
            },
        ]
    });
});