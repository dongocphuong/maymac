﻿function getNoidungGroupPhieuXuatVatTuKhoDonVi(value) {
    var datasource = $("#gridPhieuXuatVatTuKhoDonVi").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (group[i].LoaiHinhXuat == 2) {
                    //Điều Chuyển
                    if (i == 0) result = GetTextLanguage("dieuchuyendencongtrinh") + ": " + group[i].CongTrinhDen + " & " + GetTextLanguage("doithicong") + ": " + group[i].DoiThiCongDen;
                    else {
                        if (group[i].CongTrinhDen != group[i - 1].CongTrinhDen && group[i].DoiThiCongDen != group[i - 1].DoiThiCongDen) {
                            result += " & " + GetTextLanguage("dieuchuyendencongtrinh") + ": " + group[i].CongTrinhDen + " & " + GetTextLanguage("doithicong") + ": " + group[i].DoiThiCongDen;
                        }
                    }
                }
                else {
                    //Sử Dụng
                    result = GetTextLanguage("xuatsudung");
                }
            }
        }
    });
    return result;
};
function getNoidungGroupPhieuXuatVatTuKhoTong(value) {
    var datasource = $("#gridPhieuXuatVatTuKhoTong").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (group[i].LoaiHinhXuat == 2) {
                    //Điều Chuyển
                    if (i == 0) result = GetTextLanguage("dieuchuyendencongtrinh") + ": " + group[i].CongTrinhDen + " & " + GetTextLanguage("doithicong") + ": " + group[i].DoiThiCongDen;
                    else {
                        if (group[i].CongTrinhDen != group[i - 1].CongTrinhDen && group[i].DoiThiCongDen != group[i - 1].DoiThiCongDen) {
                            result += " &" + GetTextLanguage("dieuchuyendencongtrinh") + ": " + group[i].CongTrinhDen + " & " + GetTextLanguage("doithicong") + ": " + group[i].DoiThiCongDen;
                        }
                    }
                }
                else {
                    //Sử Dụng
                    result = GetTextLanguage("xuatsudung");
                }
            }
        }
    });
    return result;
};

$(document).ready(function () {
    var CurrentController = "/QuanLyPhieuXuatVatTu";
    var dataLoaiHinhXuat = [
        { text: GetTextLanguage("tatca"), value: "0" },
        { text: GetTextLanguage("xuatsudung"), value: "1" },
        { text: GetTextLanguage("xuatdieuchuyen"), value: "2" }
    ];

    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        },
        select: function (e) {
            var tab = e.item.id;
            if (tab == "tab_khotong") {

                PhieuXuatVatTuKhoTong();
            }
        }
    });
    PhieuXuatVatTuKhoDonVi();
    function PhieuXuatVatTuKhoDonVi() {
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                var value = this.value();
                gridReload("gridPhieuXuatVatTuKhoDonVi", { LoaiHinhXuat: $("#inputLoaiHinhXuat").val(), CongTrinhID: $("#drdCongTrinh").val(), DoiThiCongID: $("#drdDoiThiCong").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
            }
        });
        $("#inputDenNgay").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridPhieuXuatVatTuKhoDonVi", { LoaiHinhXuat: $("#inputLoaiHinhXuat").val(), CongTrinhID: $("#drdCongTrinh").val(), DoiThiCongID: $("#drdDoiThiCong").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
            }
        });
        $("#inputLoaiHinhXuat").kendoDropDownList({
            dataSource: dataLoaiHinhXuat,
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var value = this.value();
                gridReload("gridPhieuXuatVatTuKhoDonVi", { LoaiHinhXuat: $("#inputLoaiHinhXuat").val(), CongTrinhID: $("#drdCongTrinh").val(), DoiThiCongID: $("#drdDoiThiCong").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
            }
        });

        $("#drdCongTrinh").kendoDropDownList({
            dataTextField: 'TenCongTrinh',
            dataValueField: 'CongTrinhID',
            filter: "contains",
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: CurrentController + "/GetAllDataCongTrinhDonVi",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                    },
                },
                schema: {
                    type: "json",
                    data: "data"
                }
            }),
            dataBound: function (e) {
                var valueDoiID = this.value();
                if (valueDoiID) {
                    $("#drdDoiThiCong").data("kendoDropDownList").dataSource.read({ CongTrinhID: valueDoiID });
                    setDataSourcePhieuXuatVatTuKhoDonVi();
                }
            },
            change: function (e) {
                var value = this.value();
                if (value) {
                    $("#drdDoiThiCong").data("kendoDropDownList").dataSource.read({ CongTrinhID: value });
                }
            },
        });
        $("#drdDoiThiCong").kendoDropDownList({
            dataTextField: 'TenDoiThiCong',
            dataValueField: 'DoiThiCongID',
            filter: "contains",
            optionLabel: GetTextLanguage("chondoi"),
            index: 0,
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: CurrentController + "/GetAllDataDoiThiCong",
                        dataType: "json",
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#drdCongTrinh").val()
                        },
                        complete: function (e) {
                            if (e.responseJSON.data.length > 0) {
                                $("#drdDoiThiCong").data("kendoDropDownList").select(1);
                                setDataSourcePhieuXuatVatTuKhoDonVi();
                            }
                        }
                    },
                },
                schema: {
                    type: "json",
                    data: "data"
                }
            }),
            change: function (e) {
                var value = this.value();
                gridReload("gridPhieuXuatVatTuKhoDonVi", { LoaiHinhXuat: $("#inputLoaiHinhXuat").val(), CongTrinhID: $("#drdCongTrinh").val(), DoiThiCongID: $("#drdDoiThiCong").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
            },
        });
        function setDataSourcePhieuXuatVatTuKhoDonVi() {
            var dataPhieuXuatVatTuKhoDonVi = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: CurrentController + "/GetDataPhieuXuatVatTuKhoDonVi",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            LoaiHinhXuat: $("#inputLoaiHinhXuat").data("kendoDropDownList").value(),
                            CongTrinhID: $("#drdCongTrinh").data("kendoDropDownList").value(),
                            DoiThiCongID: $("#drdDoiThiCong").data("kendoDropDownList").value(),
                            TuNgay: $("#inputTuNgay").val(),
                            DenNgay: $("#inputDenNgay").val()
                        },
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                pageSize: 25,
                schema: {
                    data: "data",
                    total: "total"
                },
                group: {
                    field: "ThoiGian",
                    dir: "desc",
                    aggregates: [{
                        field: "ThanhTien",
                        aggregate: "sum"
                    }]
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
            });
            $("#gridPhieuXuatVatTuKhoDonVi").data("kendoGrid").setDataSource(dataPhieuXuatVatTuKhoDonVi);
        };

        $("#gridPhieuXuatVatTuKhoDonVi").kendoGrid({
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: window.innerHeight * 0.68,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupPhieuXuatVatTuKhoDonVi(value)#</scpan>"+ "<div class='pull-right'><button class='btn btn-xs btn-primary xemchitiet'>" + GetTextLanguage("xemchitiet") + "</button></div>",
                },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    //footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(DonGia,'n2') #"
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#=kendo.toString(ThanhTien,'n2') #",
                    //footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "NhanVienNhap", title: GetTextLanguage("nhanviennhap"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                }
            ],
            dataBound: function (e) {
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });
        $("#gridPhieuXuatVatTuKhoDonVi").on("click", ".xemchitiet", function () {
            var dataItem = $("#gridPhieuXuatVatTuKhoDonVi").data("kendoGrid").dataItem($(this).closest("tr"));
            CreateModalWithSize("mdlChiTietPhieuXuat", "90%", "90%", null, GetTextLanguage("chitietphieuxuat"));
            $("#mdlChiTietPhieuXuat").load("/QuanLyVatTuKhoTong" + "/PopupXuatVatTuKhoTong?PhieuXuatVatTuID=" + dataItem.PhieuXuatVatTuID + "&LoaiXuat=" + dataItem.LoaiHinhXuat);
        });
        $("#btnXuatKhoDonVi").click(function () {
            CreateModalWithSize("mdlThemPhieuXuatKhoDonVi", "90%", "90%", null, GetTextLanguage("xuatvattukhodonvi"));
            $("#mdlThemPhieuXuatKhoDonVi").load("/QuanLyVatTuKhoDonVi/PopupXuatVatTuKhoDonVi");
        });
    };

    function PhieuXuatVatTuKhoTong() {
        $("#ipLoaiHinhXuat").kendoDropDownList({
            dataSource: dataLoaiHinhXuat,
            dataTextField: "text",
            dataValueField: "value",
            change: function () {
                var value = this.value();
                gridReload("gridPhieuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay2").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                var value = this.value();
                gridReload("gridPhieuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay2").val() });
            }
        });
        $("#inputDenNgay2").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridPhieuXuatVatTuKhoTong", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });
        var dataPhieuXuatVatTuKhoTong = new kendo.data.DataSource({
            transport: {
                read: {
                    url: CurrentController + "/GetDataPhieuXuatVatTuKhoTong",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        LoaiHinhXuat: 0,
                        TuNgay: $("#inputTuNgay2").val(),
                        DenNgay: $("#inputDenNgay2").val()
                    }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            type: "json",
            pageSize: 25,
            schema: {
                data: "data",
                total: "total",
                //model: {
                //    id: "",
                //    field: {
                //        PhieuXuatVatTuID,
                //        ThoiGian,
                //        LoaiHinhXuat,
                //        TenVatTu,
                //        TenVietTat,
                //        TenDonVi,
                //        SoLuong,
                //        DonGia,
                //        ThanhTien,
                //        CongTrinhDen,
                //        DoiThiCongDen,
                //        NhanVienNhap
                //    }
                //}
            },
            group: {
                field: "ThoiGian",
                dir: "desc",
                aggregates: [{
                    field: "ThanhTien",
                    aggregate: "sum"
                }]
            },
            aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
        });
        $("#gridPhieuXuatVatTuKhoTong").kendoGrid({
            dataSource: dataPhieuXuatVatTuKhoTong,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: window.innerHeight * 0.73,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupPhieuXuatVatTuKhoTong(value)#</scpan>"+ "<div class='pull-right'><button class='btn btn-xs btn-primary xemchitiet'>" + GetTextLanguage("xemchitiet") + "</button></div>",
                },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(DonGia,'n2') #"
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#=kendo.toString(ThanhTien,'n2') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "NhanVienNhap", title: GetTextLanguage("nhanviennhap"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
            ],
            dataBound: function (e) {
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });
        $("#gridPhieuXuatVatTuKhoTong").on("click", ".xemchitiet", function () {
            var dataItem = $("#gridPhieuXuatVatTuKhoTong").data("kendoGrid").dataItem($(this).closest("tr"));
            CreateModalWithSize("mdlChiTietPhieuXuat", "90%", "90%", null, GetTextLanguage("chitietphieuxuat"));
            $("#mdlChiTietPhieuXuat").load("/QuanLyVatTuKhoTong" + "/PopupXuatVatTuKhoTong?PhieuXuatVatTuID=" + dataItem.PhieuXuatVatTuID + "&LoaiXuat=" + dataItem.LoaiHinhXuat);
        });
        $("#btnXuatKhoTong").click(function () {
            CreateModalWithSize("mdlThemPhieuXuatKhoTong", "90%", "90%", null, GetTextLanguage("xuatvattukhotong"));
            $("#mdlThemPhieuXuatKhoTong").load("/QuanLyVatTuKhoTong/PopupXuatVatTuKhoTong");
        });
    }
});