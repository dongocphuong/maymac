﻿$(document).ready(function () {
    var currentController = "/QuanLyVatTuKhoDonVi";
    var trangthai = 0;
    var LoaiHinh = 2;//Xuất Điều Chuyển
    var dataimg = new FormData();
    $("#btnXuatDieuChuyen").click(function () {
        LoaiHinh = 2;
        $("#btnXuatDieuChuyen").attr("class", "btn btn-success");
        $("#btnXuatSuDung").attr("class", "btn btn-default");
        $("#grNhan").show();
    });
    $("#btnXuatSuDung").click(function () {
        LoaiHinh = 1;
        $("#btnXuatSuDung").attr("class", "btn btn-success");
        $("#btnXuatDieuChuyen").attr("class", "btn btn-default");
        $("#grNhan").hide();
    });
    if ($("#ipPhieuXuatVatTuID").val() != null && $("#ipPhieuXuatVatTuID").val() != "" && ($("#CongTrinhID").val() == null || $("#CongTrinhID").val() == "")) {
        LoaiHinh = 1;
        $("#btnXuatSuDung").attr("class", "btn btn-success");
        $("#btnXuatDieuChuyen").attr("class", "btn btn-default");
        $("#grNhan").hide();
    }
    $(".lst-modal").popuptablefilters();
    $("#drdCongTrinhNhan").kendoDropDownList({
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetCongTrinhs?CongTrinhID="+$("#drdCongTrinh").data("kendoDropDownList").value(),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8"
                }
            },
            schema: {
                type: "json",
                data: "data"
            }
        }),
        filter: "contains",
        dataTextField: "TenDuAn",
        dataValueField: "DuAnID",
        optionLabel: GetTextLanguage("choncongtrinh"),
        
    });
   

    if ($("#ThoiGianXuat").data("kendoDateTimePicker") === undefined) {
        $("#ThoiGianXuat").kendoDateTimePicker({
            format: "dd/MM/yyyy hh:mm",
        });
    }

    if ($("#drdDoiTac").data("kendoDropDownList") === undefined) {
        $("#drdDoiTac").kendoDropDownList({
            autoBind: false,
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/GetAllDataDoiTac",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8"
                    }
                },
                schema: {
                    type: "json",
                    data: "data"
                }
            }),
            filter: "contains",
            dataTextField: "TenDoiTac",
            dataValueField: "DoiTacID",
            optionLabel: GetTextLanguage("chondoitac")
        });
    }
    var FileDinhKem = [];
    if ($("#gridChiTietPhieuXuatVatTu").data("kendoGrid") === undefined) {
        $("#gridChiTietPhieuXuatVatTu").kendoGrid({
            dataSource: new kendo.data.DataSource({
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            ID: { editable: false },
                            VatTuID: { editable: false },
                            TenDonVi: { type: "string", editable: false },
                            TenVatTu: { type: "string", editable: false },
                            TenVietTat: { type: "string", editable: false },
                            SoLuong: { type: "number", editable: true, validation: { min: 1, format: "n2" } },
                            DonGia: { type: "number", editable: false, validation: { min: 0, format: "n2" } },
                            ThanhTien: { type: "number", editable: false, validation: { format: "n2" } },
                            HeSo: { type: "number", editable: true,defaultValue: 100},
                            TonHienTai: { type: "number", editable: false }
                        }
                    }
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
            }),
            height: 500,
            filterable: {
                mode: "row"
            },
            editable: true,
            sortable: true,
            resizable: true,
            width: "100%",
            dataBound: function (e) {
                showslideimg10();
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                });
            },
            
            columns:
            [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "DsHinhAnh", title: "Ảnh",
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInTextColumn,
                    template: kendo.template($("#tplAnh").html()),
                    width: 70
                },
                {
                    field: "TinhTrang",
                    title: "Tình trạng",
                    filterable: FilterInTextColumn,
                    template: kendo.template($("#tplTinhTrang").html()),
                    attributes: alignCenter,
                    width: 150
                },
                {
                    field: "TenVatTu", width: 200,
                    title: GetTextLanguage("tenvattu"),
                    attributes: { style: "text-align:left;" },
                    filterable: FilterInColumn
                },
                {
                    field: "TenVietTat",
                    width: 150,
                    attributes: { style: "text-align:left;" },
                    title: "Mã",
                    filterable: FilterInTextColumn
                },
                {
                    field: "TenDonVi", width: 150,
                    title: GetTextLanguage("donvi"),
                    attributes: { "style": "text-align:left !important;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"),
                    format: "{0:n2}",
                    attributes: { style: "text-align:right;" },
                    filterable: FilterInColumn, width: 150,
                    editor: function (container, options) {
                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                        var input = $('<input type="text" style="width:75%;height:15px;" class="form-control" id="' + options.model.get("VatTuID") + '" name="DonGia" />');
                        input.appendTo(container);
                        input.autoNumeric(autoNumericOptionsSoLuong);
                    },
                },
                {
                    field: "SoLuong", width: 100,
                    title: GetTextLanguage("soluong"),
                    format: "{0:n2}",
                    attributes: { "style": "text-align:right !important;" },
                    filterable: FilterInColumn,
                    editor: function (container, options) {
                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                        var input = $('<input type="text" style="width:75%;height:15px;" class="form-control" id="' + options.model.get("VatTuID") + '" name="SoLuong" />');
                        input.appendTo(container);
                        input.autoNumeric(autoNumericOptionsSoLuong);
                    },
                },
                {
                    field: "TonHienTai", width: 100,
                    title: GetTextLanguage("tonhientai"),
                    format: "{0:n2}",
                    attributes: { "style": "text-align:right !important;" },
                    filterable: FilterInColumn,
                    editor: function (container, options) {
                        container.text(kendo.toString(options.model.TonHienTai, "n2"));
                    }
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), format: "{0:n2}",
                    attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    footerTemplate: "<div style='color:red;text-align:right;'>#=kendo.toString(sum, 'n2')#</div>",
                    editor: function (container, options) {
                        container.text(kendo.toString(options.model.ThanhTien, "n2"));
                    }
                },
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                    command: [
                        {
                            text: GetTextLanguage("xoa"),
                            name: "destroy"
                        }
                    ],
                    width: 100,
                    hidden: $("#ipPhieuXuatVatTuID").val() != null && $("#ipPhieuXuatVatTuID").val() != ""
                },
            ],
            save: function (e) {
                var items = $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").dataSource.data();
                if (e.values.DonGia != null) {
                    for (var i = 0; i < items.length; i++) {
                        if (items[i].VatTuID == e.model.VatTuID && items[i].TinhTrang == e.model.TinhTrang) {
                            items[i].ThanhTien = (e.values.DonGia == null ? 1 : kendo.parseFloat(e.values.DonGia)) * (e.model.SoLuong == null ? 1 : e.model.SoLuong);
                            items[i].DonGia = e.values.DonGia;
                            break;
                        }
                    }
                } else {
                    if (e.values.SoLuong != null) {
                        for (var i = 0; i < items.length; i++) {
                            if (items[i].VatTuID == e.model.VatTuID && items[i].TinhTrang == e.model.TinhTrang) {
                                items[i].SoLuong = e.values.SoLuong;
                                items[i].ThanhTien = (e.model.DonGia == null ? 1 : e.model.DonGia) * (e.values.SoLuong == null ? 1 : kendo.parseFloat(e.values.SoLuong));
                                break;
                            }
                        }
                    }
                }
              
                $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").dataSource.data(items);
                $('#gridChiTietPhieuXuatVatTu').data('kendoGrid').refresh();
            }
        });
    }
    if ($("#ipPhieuXuatVatTuID").val() !== null && $("#ipPhieuXuatVatTuID").val() !== "") {
        $("#btnChonVatTu").closest(".form-group").addClass("hidden");
        $("#ThoiGianXuat").data("kendoDateTimePicker").enable(false);
        var dataSourceChiTietPhieuXuatVatTu = new kendo.data.DataSource(
            {
                transport: {
                    read: {
                        url: currentController + "/GetDataChiTietPhieuXuat",
                        dataType: 'json',
                        type: 'Get',
                        contentType: "application/json; charset=utf-8",
                        data: {
                            PhieuXuatVatTuID: $("#ipPhieuXuatVatTuID").val()
                        }
                    }
                },
                batch: true,
                pageSize: 20,
                sort: [{ field: "TenVatTu", dir: "asc" }],
                schema: {
                    data: 'data',
                    total: 'total',
                    model: {
                        id: "VatTuID",
                        fields: {
                            VatTuID: { type: "string", editable: false },
                            TenVatTu: { type: "string", editable: false },
                            TenVietTat: { type: "string", editable: false },
                            TenDonVi: { type: "string", editable: false },
                            SoLuong: { type: "number" },
                            DonGia: { type: "number",editable: false },
                            TonHienTai: { type: "number", editable: false },
                            SoLuongTon: { type: "number", editable: false }
                        },
                    }
                },
            });

        $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").setDataSource(dataSourceChiTietPhieuXuatVatTu);
    }
    $("#mdlXuatVatTuKhoDonVi").on("click", ".xoaItemFile", function () {
        var file = $(this).data("file");
        for (var i = 0; i < FileDinhKem.length; i++) {
            if (FileDinhKem[i].name === file) {
                FileDinhKem.splice(i, 1);
                break;
            }
        }
        $(this).parent().remove();
    });
    var arrData = [];
    $("#btnChonVatTu").click(function () {
        CreateModalWithSize("mdlDanhSachVatTu", "95%", "90%", "tplDanhSachVatTu", GetTextLanguage("danhsachvattu"));
        $("#gridDanhSachVatTu").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/GetDataVatTuKhoDonVi",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#drdCongTrinh").data("kendoDropDownList").value(),
                        }
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
                sort: [{ field: "TenVatTu", dir: "asc" }],
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        fields: {
                            TenVatTu: { type: "string", editable: false },
                            TenVietTat: { type: "string", editable: false },
                            TenDonVi: { type: "string", editable: false },
                            MaPhieu: { type: "string", editable: false },
                            SoLuong: { type: "number", editable: true },
                            SoLuongTon: { type: "number", editable: false },
                            GhiChu: { type: "string", editable: false },
                            //DoiTac: { type: "string", editable: false }
                        },
                    }
                },
            }),
            filterable: {
                mode: "row"
            },
            editable: true,
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: innerHeight * 0.7,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
              {
                  field: "DsHinhAnh", title: "Ảnh",
                  attributes: { style: "text-align:left;" },
                  filterable: FilterInTextColumn,
                  template: kendo.template($("#tplAnh").html()),
                  width: 70
              },
               {
                   field: "TinhTrang",
                   title: "Tình trạng",
                   filterable: FilterInTextColumn,
                   template: kendo.template($("#tplTinhTrang").html()),
                   attributes: alignCenter,
                   width: 150
               },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 150
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 100
                },
               
                {
                      field: "SoLuongTon", title: "Số lượng tồn", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                      template: "#= kendo.toString(SoLuongTon,'n2') #",
                },
                 {
                     field: "SoLuong",
                     width: 100,
                     title: GetTextLanguage("soluong"),
                     format: "{0:n2}",
                     filterable: FilterInColumn,
                     editor: function (container, options) {
                         rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                         var input = $('<input type="text"  style="width:75%;height:15px;" class="form-control" id="' + options.model.get("VatTuID") + '" name="SoLuong" />');
                         input.appendTo(container);
                         input.autoNumeric(autoNumericOptionsSoLuong);
                     },
                 },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" },
                    width: 150,
                    format: "{0:n2}",
                    editor: function (container, options) {
                        rs = 'required data-required-msg="' + GetTextLanguage("khongtrong") + '"'
                        var input = $('<input type="text"  style="width:75%;height:15px;" class="form-control" id="' + options.model.get("VatTuID") + '" name="DonGia" />');
                        input.appendTo(container);
                        input.autoNumeric(autoNumericOptionsSoLuong);
                    },
                },
            ],
            dataBound: function (e) {
                showslideimg10();
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });

        $("#gridDanhSachVatTu").data("kendoGrid").table.on("click", ".chon", function () {
            var dataItem = $("#gridDanhSachVatTu").data("kendoGrid").dataItem($(this).closest("tr"));
            if ($(this)[0].checked === true) {
                dataItem.Chon = true;
                $("#gridDanhSachVatTu").data("kendoGrid").select($("#gridDanhSachVatTu").data("kendoGrid").table.find("[data-uid=" + dataItem.uid + "]"));
            }
            else {
                dataItem.Chon = false;
                $("#gridDanhSachVatTu").data("kendoGrid").table.find("[data-uid=" + dataItem.uid + "]").attr("class", "");
            }
        });
        $("#gridDanhSachVatTu").data("kendoGrid").table.on("change", ".chon", function () {

        });
        $("#gridDanhSachVatTu").data("kendoGrid").table.on("dblclick", "tr", function () {
            var dataItem = $("#gridDanhSachVatTu").data("kendoGrid").dataItem($(this).closest("tr"));
            if (dataItem.Chon === true) {
                dataItem.Chon = false;
                $("#gridDanhSachVatTu").data("kendoGrid").table.find("[data-uid=" + dataItem.uid + "]").attr("class", "");
            }
            else {
                dataItem.Chon = true;
              
            }
        });


        $("#btnHuyChonVatTu").click(function () {
            $("#mdlDanhSachVatTu").data("kendoWindow").close();
        });
        $("#btnTiepTuc").click(function () {
            var arrData = $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").dataSource.data();
            var dataItem = $("#gridDanhSachVatTu").data("kendoGrid").dataSource.data();
            var kiemtraton = true;
            for (var i = 0; i < dataItem.length; i++) {
                if (dataItem[i].DonGia != null && dataItem[i].SoLuong > 0) {
                    if (dataItem[i].SoLuong > dataItem[i].SoLuongTon) {
                        kiemtraton = false;
                        showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("soluongphainhohonsoluongton") + " : " + dataItem[i].TenVatTu);
                    }
                    else {
                        var isExsit = false;
                        for (var j = 0; j < arrData.length; j++) {
                            if (dataItem[i].ChiTietNhapXuatVatTuID == arrData[j].ChiTietNhapXuatVatTuID) {
                                arrData[j].DonGia = dataItem[i].DonGia;
                                arrData[j].SoLuong = dataItem[i].SoLuong;
                                arrData[i].TonHienTai = dataItem[i].SoLuongTon;
                                arrData[j].ThanhTien = (arrData[j].DonGia == null ? 1 : kendo.parseFloat(arrData[j].DonGia)) * (arrData[j].SoLuong == null ? 1 : kendo.parseFloat(arrData[j].SoLuong));
                                isExsit = true;
                            }
                        }
                        if (!isExsit) {
                            if (dataItem[i].SoLuong > 0) {
                                dataItem[i].ThanhTien = (dataItem[i].DonGia == null ? 1 : kendo.parseFloat(dataItem[i].DonGia)) * (dataItem[i].SoLuong == null ? 1 : kendo.parseFloat(dataItem[i].SoLuong));
                                dataItem[i].TonHienTai = dataItem[i].SoLuongTon;
                                arrData.push(dataItem[i]);
                            }

                        }
                    }
                }

            }
            if (kiemtraton) {
                $("#mdlDanhSachVatTu").data("kendoWindow").close();
                $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").dataSource.data(arrData);
            } else {
                //showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("soluongphainhohonsoluongton"));
            }
        });
    });

    $("#btnHuyPhieuXuatVatTu").click(function () {
        $("#mdlXuatVatTuKhoDonVi").data("kendoWindow").close();
    });
    if ($("#mdlChiTietPhieuXuat").text().lenth > 1) {
        showslideimg5($("#mdlChiTietPhieuXuat"));
        $("#mdlChiTietPhieuXuat #files_0").change(function () {
            for (var x = 0; x < this.files.length; x++) {
                dataimg.append("uploads", this.files[x]);
            };
            loadimgSuaChua5(this, $("#mdlChiTietPhieuXuat"));
            $("#mdlChiTietPhieuXuat #files_0").val('').clone(true);
        })
    } else {
        showslideimg5($("#mdlXuatVatTuKhoDonVi"));
        $("#mdlXuatVatTuKhoDonVi #files_0").change(function () {
            for (var x = 0; x < this.files.length; x++) {
                dataimg.append("uploads", this.files[x]);
            };
            loadimgSuaChua5(this, $("#mdlXuatVatTuKhoDonVi"));
            $("#mdlXuatVatTuKhoDonVi #files_0").val('').clone(true);
        })
    }
    $("#btnDongYXuat").click(function () {
        ContentWatingOP("mdlXuatVatTuKhoDonVi", 0.9);
        ContentWatingOP("mdlChiTietPhieuXuat", 0.9);
        var lists = $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").dataSource.data();
        if (LoaiHinh == 2) {
            if ($("#drdCongTrinhNhan").val() == "") {
                showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("choncongtrinh"));
                return false;
            }
           
            if (lists.length <= 0) {
                showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("chonvattu"));
                return false;
            }
            for (var i = 0; i < lists.length; i++) {
                
                if (lists[i].SoLuong == null || lists[i].SoLuong == undefined) {
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("nhapsoluong"));
                    return false;
                }
            }
        }
        else {
            if (lists.length <= 0) {
                showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("chonvattu"));
                return false;
            }
            for (var i = 0; i < lists.length; i++) {
              
                if (lists[i].SoLuong == null || lists[i].SoLuong == undefined) {
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("nhapsoluong"));
                    return false;
                }
            }
        }
        for (var i = 0; i < lists.length; i++) {
            if (lists[i].SoLuong > lists[i].TonHienTai) {
                showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("soluongphainhohonsoluongton") + " : " + lists[i].TenVatTu);
                return false;
            }
        }
        for (var i = 0; i < lists.length; i++) {
            if (lists[i].SoLuong > lists[i].TonHienTai) {
                showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("soluongphainhohonsoluongton") + " : " + lists[i].TenVatTu);
                return false;
            }
        }
        var hinhanh = "";
        var ins = dataimg.getAll("uploads").length;
        var DSHinhAnh = [];
        $('.dshinhanhadd .imgslider').each(function (index2, element2) {
            if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                DSHinhAnh.push({ Url: $(element2).find('img').data("file") });
            }
        });
        if (ins > 0) {
            if ($("#mdlChiTietPhieuXuat").text().lenth > 1) {
                var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", dataimg.getAll("uploads"), $(".dshinhanhadd", $('#mdlChiTietPhieuXuat'))).split(',');
                for (var k = 0; k < strUrl.length; k++) {
                    DSHinhAnh.push({ Url: strUrl[k] });
                }
            } else {
                var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", dataimg.getAll("uploads"), $(".dshinhanhadd", $('#mdlXuatVatTuKhoDonVi'))).split(',');
                for (var k = 0; k < strUrl.length; k++) {
                    DSHinhAnh.push({ Url: strUrl[k] });
                }
            }
        }
        for (var i = 0; i < DSHinhAnh.length; i++) {
            if (hinhanh.indexOf(DSHinhAnh[i].Url) < 0) {
                hinhanh += DSHinhAnh[i].Url + ",";
            }
        }
        var dt = [];
        var items = $("#gridChiTietPhieuXuatVatTu").data("kendoGrid").dataSource.data();
        for (var i = 0; i < items.length; i++) {
            items[i].SoLuong = kendo.parseFloat(items[i].SoLuong);
            items[i].DonGia = kendo.parseFloat(items[i].DonGia);
            dt.push(items[i]);
        }
        setTimeout(function () {
        $.ajax({
            url: currentController + "/XuatVatTu",
            type: "POST",
            data: {
                PhieuXuatVatTuID: $("#ipPhieuXuatVatTuID").val(),
                LoaiHinh: LoaiHinh,
                CongTrinhDiID: $("#drdCongTrinh").data("kendoDropDownList").value(),
                CongTrinhNhanID: $("#drdCongTrinhNhan").data("kendoDropDownList").value(),
                DataChiTietPhieuXuat: JSON.stringify(dt),
                DSHinhanh: hinhanh.substring(0, hinhanh.length - 1),
                ThoiGian: $("#ThoiGianXuat").val()
            },
            complete: function (e) {
                alertToastr(e.responseJSON);
                if (e.responseJSON.code === "success") {
                    if ($("#mdlXuatVatTuKhoDonVi").text().length > 1) {
                        $("#mdlXuatVatTuKhoDonVi").data("kendoWindow").close();
                        $("#mdlXuatVatTuKhoDonVi").unblock();
                        $("#gridQuanLyVatTuKhoDonVi").data("kendoGrid").dataSource.read({
                            CongTrinhID: $("#drdCongTrinh").data("kendoDropDownList").value(),
                        });
                    } else {
                        $("#mdlChiTietPhieuXuat").data("kendoWindow").close();
                        $("#mdlChiTietPhieuXuat").unblock();
                        $("#gridLichSuXuatVatTuKhoDonVi").data("kendoGrid").dataSource.read();
                        $("#gridQuanLyVatTuKhoDonVi").data("kendoGrid").dataSource.read();
                    }
                }
            }
            });
        }, 1000);
    });
    function xoarow(e) {
        e.preventDefault();
        var dataItemrow = this.dataItem($(e.currentTarget).closest("tr"));
        for (var i = 0; i < dataobject.items.length; i++) {
            if (arrData.items[i].ID === dataItemrow.get("ID")) {
                arrData.items.splice(i, 1);
                gridReload("gridChiTietPhieuXuatVatTu", {});
                break;
            }
        }
    }

});