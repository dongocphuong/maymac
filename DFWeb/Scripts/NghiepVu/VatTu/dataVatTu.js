﻿function getDataDanhSachVatTu(crudServiceBaseUrl, url, param) {
    var dataSources = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + url,
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: param
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        //group: groups,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "VatTuID",
                fields: {
                    VatTuID: { editable: false, nullable: true },
                    select: {editable: false},
                    TenDonVi: {
                        type: "string",
                        editable: false
                    },
                    TenVatTu: {
                        type: "string",
                        editable: false
                    },
                    TenNhom: {
                        type: "string",
                        editable: false
                    },
                    SoLuong: { type: "number", editable: true, validation: { min: 1, format: "n2" }, defaultValue: 0 },
                    SoLuongTon: { type: "number", editable: false},
                    DonGia: { type: "number", editable: true, validation: { min: 0, format: "n2" }, defaultValue: 0 },
                    DonGiaNhap: { type: "number", editable: false, validation: { min: 0, format: "n2" }, defaultValue: 0 },
                }
            }
        },
    });

    return dataSources;
}