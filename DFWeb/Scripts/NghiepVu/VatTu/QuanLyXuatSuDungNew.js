﻿$(document).ready(function () {

    var datafix_trangthai = [
            { text: GetTextLanguage("tatca"), value: 10 },
            { text: GetTextLanguage("daduyet"), value: 1 },
            { text: GetTextLanguage("chuaduyet"), value: 0 }
    ];
    var datafix_doithicong = [
            { text: "Đội Thi Công 1", value: 10 },
    ];
    var datafix_congtrinh = [
            { text: "Tàu AONDS", value: 10 }
    ];
    var datafix_hangmuc = [
            { text: "Hạng Mục D600", value: 10 }
    ];
    var date = new Date();
    $("#TuNgay").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date(date.getFullYear(), date.getMonth() - 1, date.getDate()),
        open: function (e) {
            if ($("#DenNgay").data("kendoDatePicker").value() !== null) {
                $("#TuNgay").data("kendoDatePicker").max($("#DenNgay").data("kendoDatePicker").value());
            }
        }
    });
    $("#DenNgay").kendoDatePicker({
        format: "dd/MM/yyyy",
        value: new Date(),
        open: function (e) {
            if ($("#TuNgay").data("kendoDatePicker").value() !== null) {
                $("#DenNgay").data("kendoDatePicker").min($("#TuNgay").data("kendoDatePicker").value());
            }
        }
    });

    $("#drdCongTrinh").kendoDropDownList({
        dataSource: datafix_congtrinh,
        dataTextField: "text",
        dataValueField: "value",
        autoBind: true,
    });
    $("#drdDoiThiCong").kendoDropDownList({
        dataSource: datafix_doithicong,
        dataTextField: "text",
        dataValueField: "value",
        autoBind: true,
    });
    $("#drdHangMucThiCong").kendoDropDownList({
        dataSource: datafix_hangmuc,
        dataTextField: "text",
        dataValueField: "value",
        autoBind: true,
    });

    var datasourceCT = new kendo.data.DataSource({
        transport: {
            read: {
                url: "/QLNhanVien/LayDanhSachNhanVien_New",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {
                    LoaiHinh: 1,
                    TrangThai: 1// lấy tất cả công trình
                }
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        sort: [{ field: "TenDoi", dir: "asc" }, { field: "TenNhanVien", dir: "asc" }],
        schema: {
            model: {
                id: "NhanVienID",
                fields: {
                    NhanVienID: { type: "string" }
                }
            },
            data: "data",
            total: "total",
        }
    });
    $("#BangTPNV").kendoGrid({
        width: "100%",
        height: heightGrid * 0.85,
        dataSource: {
            data: {
                "items": [
                  {
                      "ThoiGian": "11/12/2017",
                      "TenVatTu": "Thép Buộc",
                      "MaVatTu": "TB",
                      "SoLuong": 25,
                      "NguoiNhap": "Trần Văn Xuyên",
                      "HangMuc": "D1000"
                  },
                  {
                      "ThoiGian": "11/12/2017",
                      "TenVatTu": "Dầu Deseil",
                      "MaVatTu": "DDL",
                      "SoLuong": 15.5,
                      "NguoiNhap": "Trần Văn Xuyên",
                      "HangMuc": "D1000"
                  },
                  {
                      "ThoiGian": "11/01/2018",
                      "TenVatTu": "Que hàn",
                      "MaVatTu": "TB",
                      "SoLuong": 15,
                      "NguoiNhap": "Trần Văn Xuyên",
                      "HangMuc": "D1000"
                  },
                  {
                      "ThoiGian": "11/10/2017",
                      "TenVatTu": "Sắt Tấm 1",
                      "MaVatTu": "TB",
                      "SoLuong": 34.5,
                      "NguoiNhap": "Trần Văn Xuyên",
                      "HangMuc": "D1000"
                  },
                  {
                      "ThoiGian": "11/09/2017",
                      "TenVatTu": "Tôn Lợp",
                      "MaVatTu": "TB",
                      "SoLuong": 66,
                      "NguoiNhap": "Trần Văn Xuyên",
                      "HangMuc": "D1000"
                  },
                  {
                      "ThoiGian": "11/12/2017",
                      "TenVatTu": "Bê Tông",
                      "MaVatTu": "TB",
                      "SoLuong": 78,
                      "NguoiNhap": "Trần Văn Xuyên",
                      "HangMuc": "D1000"
                  },
                  {
                      "ThoiGian": "11/12/2017",
                      "TenVatTu": "Áo Bảo Hộ",
                      "MaVatTu": "TB",
                      "SoLuong": 25,
                      "NguoiNhap": "Trần Văn Xuyên",
                      "HangMuc": "D1000"
                  },
                  {
                      "ThoiGian": "22/12/2017",
                      "TenVatTu": "Dầu Desel",
                      "MaVatTu": "TB",
                      "SoLuong": 16,
                      "NguoiNhap": "Trần Văn Xuyên",
                      "HangMuc": "D1000"
                  },
                  {
                      "ThoiGian": "11/11/2017",
                      "TenVatTu": "Thép Buộc",
                      "MaVatTu": "TB",
                      "SoLuong": 25,
                      "NguoiNhap": "Trần Văn Xuyên",
                      "HangMuc": "D1000"
                  },
                   {
                       "ThoiGian": "31/12/2017",
                       "TenVatTu": "Áo Bảo Hộ",
                       "MaVatTu": "TB",
                       "SoLuong": 30,
                       "NguoiNhap": "Trần Văn Xuyên",
                       "HangMuc": "D1000"
                   },
                  {
                      "ThoiGian": "01/03/2017",
                      "TenVatTu": "Dầu Desel",
                      "MaVatTu": "TB",
                      "SoLuong": 31,
                      "NguoiNhap": "Trần Văn Xuyên",
                      "HangMuc": "D1000"
                  },
                ]
            },
            schema: {
                data: "items"
            },
            group: {
                field: "ThoiGian",
                dir: "asc"
            }
        },
        selectable: "multiple",
        navigatable: true,
        filterable: {
            mode: "row"
        },
        sortable: true,
        pageable:
            {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
        dataBinding: function () {
            record = 0;
        },
        columns: [
           {
               title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
               template: "#= ++record #",
               width: 60,
               attributes: alignCenter
           },
            {
                field: "ThoiGian",
                title: "Thời Gian",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            {
                field: "TenVatTu",
                title: "Tên Vật Tư",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            {
                field: "MaVatTu",
                title: "Mã vật tư",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            {
                field: "SoLuong",
                title: "Số Lượng",
                width: 250,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            {
                field: "NguoiNhap",
                title: "Người Nhập",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignLeft,
            },
            {
                field: "HangMuc",
                title: "Tên Hạng Mục",
                width: 150,
                filterable: FilterInTextColumn,
                attributes: alignCenter
            }
        ]
    });
});