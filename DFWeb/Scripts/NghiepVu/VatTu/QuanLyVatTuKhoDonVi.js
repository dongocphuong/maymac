﻿function getNoidungGroupLichSuNhap(value) {
    var datasource = $("#gridLichSuNhapVatTuKhoDonVi").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i == 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung != group[i - 1].NoiDung) {
                        result += " & " + group[0].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupLichSuNhap2(value) {
    var datasource = $("#gridtheophieu").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (i === 0) result = group[0].NoiDung;
                else {
                    if (group[i].NoiDung !== group[i - 1].NoiDung) {
                        result += " & " + group[i].NoiDung;
                    }
                }
            }
        }
    });
    return result;
};
function getNoidungGroupLichSuXuat(value) {
    var datasource = $("#gridLichSuXuatVatTuKhoDonVi").data("kendoGrid").dataSource;
    var result;
    $(datasource.view()).each(function (index, element) {
        if (element.value === value) {
            var group = element.items;
            for (var i = 0; i < group.length; i++) {
                if (group[i].LoaiHinhXuat == 2) {
                    //Điều Chuyển
                    if (i == 0) result = GetTextLanguage("dieuchuyendencongtrinh") + ": " + group[i].CongTrinhDen + " & " + GetTextLanguage("doithicong") + ": " + group[i].DoiThiCongDen;
                    else {
                        if (group[i].CongTrinhDen != group[i - 1].CongTrinhDen && group[i].DoiThiCongDen != group[i - 1].DoiThiCongDen) {
                            result += " & "+GetTextLanguage("dieuchuyendencongtrinh") + ": " + group[i].CongTrinhDen + " & "+GetTextLanguage("doithicong")+": " + group[i].DoiThiCongDen;
                        }
                    }
                }
                else {
                    //Sử Dụng
                    result = GetTextLanguage("xuatsudung");
                }
            }
        }
    });
    return result;
};
$(document).ready(function () {
    var currentController = "/QuanLyVatTuKhoDonVi";

    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });

    $(".lst-modal").popuptablefilters();
    $("#gridQuanLyVatTuKhoDonVi").kendoGrid({
        height: window.innerHeight * 0.80,
        width: "100%",
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                //attributes: alignCenterlichsunhap
            },
              {
                  field: "DsHinhAnh", title: "Ảnh",
                  attributes: { style: "text-align:left;" },
                  filterable: FilterInTextColumn,
                  template: kendo.template($("#tplAnh").html()),
                  width: 70
              },
              {
                  field: "Lever",
                  title: "Loại sản phẩm",
                  filterable: FilterInColumn,
                  attributes: alignLeft,
                  template: kendo.template($("#tpllerver").html()),
                  width: 150
              },
            {
                field: "TenVatTu",
                title: GetTextLanguage("tenvattu"),
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200
            },
            {
                field: "TenVietTat",
                title: GetTextLanguage("tenviettat"),
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200
            },
            {
                field: "TenDonVi",
                title: GetTextLanguage("tendonvi"),
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 150
            },
             {
                 field: "TinhTrang",
                 title: "Tình trạng",
                 filterable: FilterInTextColumn,
                 attributes: alignCenter,
                 template: kendo.template($("#tplTinhTrang").html()),
                 width: 150
             },
            {
                field: "SoLuongNhap",
                title: GetTextLanguage("soluongnhap"),
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongNhap"),
                width: 100
            },
            {
                field: "SoLuongXuat",
                title: GetTextLanguage("soluongxuat"),
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongXuat"),
                width: 100
            },
            {
                field: "SoLuongTon",
                title: GetTextLanguage("tonhientai"),
                filterable: FilterInColumn,
                attributes: alignRight,
                template: formatToDouble("SoLuongTon"),
                width: 100
            },
          
        ],
        dataBound: function (e) {
            showslideimg10();
        },
    });
    $("#drdCongTrinh").kendoDropDownList({
        dataTextField: 'TenDuAn',
        dataValueField: 'DuAnID',
        filter: "contains",
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetAllDataCongTrinh",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                },
            },
            schema: {
                type: "json",
                data: "data"
            }
        }),
        change: function (e) {
            var valueDoiID = this.value();
            if (valueDoiID) {
                setDataSourceKhoDonViVatTu();
            }
        },
    });
   
    function setDataSourceKhoDonViVatTu() {
        var dataSourceKhoDonViVatTu = new kendo.data.DataSource(
            {
                transport: {
                    read: {
                        url: currentController + "/GetDataVatTuKhoDonVi",
                        dataType: 'json',
                        type: 'Get',
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#drdCongTrinh").data("kendoDropDownList").value(),
                        },
                        complete: function (e) {
                           
                        }
                    },
                    parameterMap: function (options, type) {
                        var model;
                        if (type == "read") {
                            model = {
                                CongTrinhID: options.CongTrinhID,
                            }
                        }
                        return model;
                    }
                },
                batch: true,
                pageSize: 20,
                sort: [{ field: "TenVatTu", dir: "asc" }],
                schema: {
                    data: 'data',
                    total: 'total',
                    model: {
                        id: "VatTuID",
                        fields: {
                            VatTuID: { type: "string" },
                            TenVatTu: { type: "string" },
                            TenDonVi: { type: "string" },
                            TonHienTai: { type: "number" },
                            GhiChu: { type: "string" }
                        },
                    }
                },
            });
        $("#gridQuanLyVatTuKhoDonVi").data("kendoGrid").setDataSource(dataSourceKhoDonViVatTu);
    };
    
    $("#gridQuanLyVatTuKhoDonVi").on("click", "#btnNhapVatTuKhoDonVi", function () {
        if ($("#drdCongTrinh").kendoValidator().data("kendoValidator").validate()) {
            CreateModalWithSize("mdlNhapVatTuKhoDonVi", "99%", "90%", null, GetTextLanguage('xacnhannhapvattucongtrinh') + ": " + $("#drdCongTrinh").data("kendoDropDownList").text());
            $("#mdlNhapVatTuKhoDonVi").load(currentController + "/PopupNhapVatTuKhoDonVi");
        }
    });
    $("#btnXuatVatTuKhoDonVi").on("click", function () {
        if ($("#drdCongTrinh").kendoValidator().data("kendoValidator").validate()) {
            CreateModalWithSize("mdlXuatVatTuKhoDonVi", "99%", "90%", null, GetTextLanguage("xuatvattukhodonvi"));
            $("#mdlXuatVatTuKhoDonVi").load(currentController + "/PopupXuatVatTuKhoDonVi");
        }
    });
    $("#btnLichSuNhapVatTuKhoDonVi").on("click", function () {
        if ($("#drdCongTrinh").kendoValidator().data("kendoValidator").validate()) {
            CreateModalWithSize("mdlLichSuNhapVatTuKhoDonVi", "90%", "90%", "tplLichSuNhapVatTuKhoDonVi", GetTextLanguage("lichsunhapvattukhodonvi"));
            var dataLoaiHinhNhap = [
                { text: GetTextLanguage("tatca"), value: "0" },
                { text: GetTextLanguage("dieuchuyen"), value: "1" },
                { text: GetTextLanguage("mua"), value: "2" }
            ];
            $("#ipLoaiHinhNhap").kendoDropDownList({
                dataSource: dataLoaiHinhNhap,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    var value = this.value();
                    console.log($("#inputTuNgay").val());
                    gridReload("gridLichSuNhapVatTuKhoDonVi", { LoaiHinhNhap: value, CongTrinhID: $("#drdCongTrinh").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: $("#inputDenNgay").val() });
                }
            })
            var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
            $("#inputTuNgay").kendoDatePicker({
                format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                    var value = this.value();
                    gridReload("gridLichSuNhapVatTuKhoDonVi", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(), CongTrinhID: $("#drdCongTrinh").val(),TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay").val() });
                }
            });
            $("#inputDenNgay").kendoDatePicker({
                format: "dd/MM/yyyy", value: new Date(), change: function () {
                    var value = this.value();
                    gridReload("gridLichSuNhapVatTuKhoDonVi", { LoaiHinhNhap: $("#ipLoaiHinhNhap").val(),CongTrinhID: $("#drdCongTrinh").val(), TuNgay: $("#inputTuNgay").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
                }
            });

            //Tạo Grid LichSuNhapVatTuKhoDonVi
            var dataLichSuNhapVatTuKhoDonVi = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/GetDataLichSuNhapVatTuKhoDonVi",
                        dataType: "json",
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#drdCongTrinh").val(),
                            LoaiHinhNhap: 0,
                            TuNgay: $("#inputTuNgay").val(),
                            DenNgay: $("#inputDenNgay").val()
                        }
                    },
                    //parameterMap: function (options, type) {
                    //    return JSON.stringify(options);
                    //}
                },
                type: "json",
                pageSize: 25,
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        id: "",
                        fields: {
                            MaPhieu: { type: "string" },
                            TenDonVi:{ type: "string" },
                            TenVatTu:{ type: "string" },
                            GhiChu:{ type: "string" },
                            NoiDung:{ type: "string" }
                        }
                    }
                },
                group: {
                    field: "ThoiGian",
                    dir: "desc",
                    aggregates: [{
                        field: "ThanhTien",
                        aggregate: "sum"
                    }]
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
            });
            $("#gridLichSuNhapVatTuKhoDonVi").kendoGrid({
                dataSource: dataLichSuNhapVatTuKhoDonVi,
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: pageableAll,
                height: window.innerHeight * 0.9 - 100,
                width: "100%",
                columns: [
                    {
                        title: GetTextLanguage("stt"),
                        template: "<span class='stt'></span>",
                        width: 60,
                        align: "center"
                    },
                    {
                        field: "ThoiGian", title: GetTextLanguage("thoigian"),
                        attributes: alignCenter, filterable: FilterInColumn, width: 150,
                        hidden: true,
                        groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') # -- <span style='color:brown'>#=getNoidungGroupLichSuNhap(value)#</scpan>",
                    },
                     {
                         field: "DsHinhAnh", title: "Ảnh",
                         attributes: { style: "text-align:left;" },
                         filterable: FilterInTextColumn,
                         template: kendo.template($("#tplAnh").html()),
                         width: 70
                     },
                    {
                        field: "MaPhieu", title: GetTextLanguage("maphieu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                    },
                    {
                        field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                    },
                    {
                        field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 150
                    },
                    {
                        field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 150
                    },
                    {
                        field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                        template: "#= kendo.toString(SoLuong,'n2') #",
                        footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                    },
                    {
                        field: "DonGia", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                        template: "#= kendo.toString(DonGia,'n2') #",
                    },
                    {
                        field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                        attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                        template: "#= kendo.toString(ThanhTien,'n2') #",
                        footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                    },
                    {
                        field: "NhanVienNhap", title: GetTextLanguage("nhanviennhap"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                    },
                   
                    {
                        field: "GhiChu", title: GetTextLanguage("ghichu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                    },
                    
                ],
                dataBound: function (e) {
                    showslideimg10();
                    var rows = this.items();
                    var dem = 0;
                    $(rows).each(function () {
                        dem++;
                        var rowLabel = $(this).find(".stt");
                        $(rowLabel).html(dem);
                        if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                            dem = 0;
                        }
                    });
                },
            });
            $("#gridLichSuNhapVatTuKhoDonVi .k-grid-content").on("dblclick", "td", function () {
                var rowct = $(this).closest("tr"),
                       gridct = $("#gridLichSuNhapVatTuKhoDonVi").data("kendoGrid"),
                       dataItem = gridct.dataItem(rowct);
                if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                    var LoaiNhap = 1;
                    if (dataItem.CongTrinhDieuChuyenID === null && dataItem.DoiThiCongDieuChuyenID === null) {
                        LoaiNhap = 2;
                    }
                    CreateModalWithSize("mdlChiTietPhieuNhap", "90%", "90%", null, "Chi Tiết Phiếu Nhập");
                    $("#mdlChiTietPhieuNhap").load("/QuanLyVatTuKhoTong" + "/PopupChiTietPhieuNhapVatTu?PhieuNhapVatTuID=" + dataItem.PhieuNhapVatTuID + "&LoaiNhap=" + LoaiNhap + "&PhieuXuatVatTuID=" + dataItem.PhieuXuatVatTuID);
                }
            })
            //$("#gridLichSuNhapVatTuKhoDonVi").on("click", ".xemchitiet", function () {
            //    var dataItem = $("#gridLichSuNhapVatTuKhoDonVi").data("kendoGrid").dataItem($(this).closest("tr"));
            //    var LoaiNhap = 1;
            //    if (dataItem.CongTrinhDieuChuyenID === null && dataItem.DoiThiCongDieuChuyenID === null) {
            //        LoaiNhap = 2;
            //    }
            //    CreateModalWithSize("mdlChiTietPhieuNhap", "90%", "90%", null, "Chi Tiết Phiếu Nhập");
            //    $("#mdlChiTietPhieuNhap").load("/QuanLyVatTuKhoTong" + "/PopupChiTietPhieuNhapVatTu?PhieuNhapVatTuID=" + dataItem.PhieuNhapVatTuID + "&LoaiNhap=" + LoaiNhap + "&PhieuXuatVatTuID=" + dataItem.PhieuXuatVatTuID);

            //});
            $("#mdlLichSuNhapVatTuKhoDonVi .xuat-excel").click(function () {
                var filer = [];
                var filers = $("#gridLichSuNhapVatTuKhoDonVi").data("kendoGrid").dataSource.filter();
                if (filers != null && filers != undefined) {
                    for (var i = 0; i < filers.filters.length; i++) {
                        filers.filters[i].Operator = filers.filters[i].operator;
                        filer.push(filers.filters[i]);
                    }
                }
                location.href = currentController + '/XuatExcelLichSuNhapVatTuKhoDonVi?filters=' + JSON.stringify(filer) + '&LoaiHinhNhap=' + $("#ipLoaiHinhNhap").val() + '&TuNgay=' + $("#inputTuNgay").val() + '&DenNgay=' + $("#inputDenNgay").val()+'&CongTrinhID='+ $("#drdCongTrinh").val();
            });
        }
    });

    $("#btnLichSuXuatVatTuKhoDonVi").on("click", function () {
        if ($("#drdCongTrinh").kendoValidator().data("kendoValidator").validate()) {
            CreateModalWithSize("mdlLichSuXuatVatTuKhoDonVi", "90%", "90%", "tplLichSuXuatVatTuKhoDonVi", GetTextLanguage("lichsuxuatvattukhodonvi"));
            var dataLoaiHinhXuat = [
                { text: GetTextLanguage("tatca"), value: "0" },
                { text: "Sản xuất", value: "1" },
                { text: "Điều chuyển", value: "2" }
            ];
            $("#ipLoaiHinhXuat").kendoDropDownList({
                dataSource: dataLoaiHinhXuat,
                dataTextField: "text",
                dataValueField: "value",
                change: function () {
                    var value = this.value();
                    $("#gridLichSuXuatVatTuKhoDonVi").data("kendoGrid").dataSource.read({ LoaiHinhXuat: value, CongTrinhDenID: $("#ipCongTrinhDen").data("kendoDropDownList").value(), TuNgay: $("#inputTuNgay2").val(), DenNgay: $("#inputDenNgay2").val() });
                }
            });
            $(".lst-modal").popuptablefilters();
            $("#ipCongTrinhDen").kendoDropDownList({
                dataSource: getDataForDropdownlist(currentController + "/GetAllDataCongTrinh"),
                dataTextField: "TenDuAn",
                dataValueField: "DuAnID",
                optionLabel: "Chọn dự án",
                change: function () {
                    var value = this.value();
               
                    gridReload("gridLichSuXuatVatTuKhoDonVi", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhID: $("#drdCongTrinh").val(), CongTrinhDenID: $("#ipCongTrinhDen").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: $("#inputDenNgay2").val() });
                }
            });
            
            var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
            $("#inputTuNgay2").kendoDatePicker({
                format: "dd/MM/yyyy", value: new Date(y, m - 1, d), change: function () {
                    var value = this.value();
                    gridReload("gridLichSuXuatVatTuKhoDonVi", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhID: $("#drdCongTrinh").val(), CongTrinhDenID: $("#ipCongTrinhDen").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay2").val() });
                }
            });
            $("#inputDenNgay2").kendoDatePicker({
                format: "dd/MM/yyyy", value: new Date(), change: function () {
                    var value = this.value();
                    gridReload("gridLichSuXuatVatTuKhoDonVi", { LoaiHinhXuat: $("#ipLoaiHinhXuat").val(), CongTrinhID: $("#drdCongTrinh").val(),CongTrinhDenID: $("#ipCongTrinhDen").val(), TuNgay: $("#inputTuNgay2").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
                }
            });
            //Tạo Grid LichSuXuatVatTuKhoDonVi
            var dataLichSuXuatVatTuKhoDonVi = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/GetLichSuXuatVatTuKhoDonVi",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#drdCongTrinh").val(),
                            LoaiHinhXuat: 0,
                            TuNgay: $("#inputTuNgay2").val(),
                            DenNgay: $("#inputDenNgay2").val()
                        }
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                pageSize: 25,
                schema: {
                    data: "data",
                    total: "total",
                    //model: {
                    //    id: "",
                    //    field: {
                    //        PhieuXuatVatTuID,
                    //        ThoiGian,
                    //        LoaiHinhXuat,
                    //        TenVatTu,
                    //        TenVietTat,
                    //        TenDonVi,
                    //        SoLuong,
                    //        DonGia,
                    //        ThanhTien,
                    //        CongTrinhDen,
                    //        DoiThiCongDen,
                    //        NhanVienNhap
                    //    }
                    //}
                },
                group: {
                    field: "ThoiGian",
                    dir: "desc",
                    aggregates: [{
                        field: "ThanhTien",
                        aggregate: "sum"
                    }]
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
            });
            $("#gridLichSuXuatVatTuKhoDonVi").kendoGrid({
                dataSource: dataLichSuXuatVatTuKhoDonVi,
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: pageableAll,
                height: window.innerHeight * 0.9 - 100,
                width: "100%",
                columns: [
                    {
                        title: GetTextLanguage("stt"),
                        template: "<span class='stt'></span>",
                        width: 60,
                        align: "center"
                    },
                    {
                        field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                        hidden: true,
                        template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                        groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') #",
                    },
                     {
                         field: "DsHinhAnh", title: "Ảnh",
                         attributes: { style: "text-align:left;" },
                         filterable: FilterInTextColumn,
                         template: kendo.template($("#tplAnh").html()),
                         width: 70
                     },
                    {
                        field: "MaPhieu", title: GetTextLanguage("maphieu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                    },
                    {
                        field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                    },
                    {
                        field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                    },
                    {
                        field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 150
                    },
                    {
                        field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                        template: "#= kendo.toString(SoLuong,'n2') #",
                        footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                    },
                    {
                        field: "DonGia", title: GetTextLanguage("dongia"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                        template: "#= kendo.toString(DonGia,'n2') #"
                    },
                    {
                        field: "ThanhTien", title: GetTextLanguage("thanhtien"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                        template: "#=kendo.toString(ThanhTien,'n2') #",
                        footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                    },
                    {
                        field: "NhanVienNhap", title: GetTextLanguage("nhanviennhap"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                    },
                    
                ],
                dataBound: function (e) {
                    showslideimg10();
                    var rows = this.items();
                    var dem = 0;
                    $(rows).each(function () {
                        dem++;
                        var rowLabel = $(this).find(".stt");
                        $(rowLabel).html(dem);
                        if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                            dem = 0;
                        }
                    });
                },
            });
            $("#gridLichSuXuatVatTuKhoDonVi .k-grid-content").on("dblclick", "td", function () {
                var rowct = $(this).closest("tr"),
                       gridct = $("#gridLichSuXuatVatTuKhoDonVi").data("kendoGrid"),
                       dataItem = gridct.dataItem(rowct);
                if (!rowct.hasClass("k-grouping-row") && $(this).find(".k-grid-delete").length <= 0) {
                    CreateModalWithSize("mdlChiTietPhieuXuat", "90%", "90%", null, GetTextLanguage("chitietphieuxuat"));
                    $("#mdlChiTietPhieuXuat").load("/QuanLyVatTuKhoDonVi" + "/PopupXuatVatTuKhoDonVi?PhieuXuatVatTuID=" + dataItem.PhieuXuatVatTuID + "&LoaiXuat=" + dataItem.LoaiHinhXuat);
                }
            })
            //$("#gridLichSuXuatVatTuKhoDonVi").on("click", "tr .xemchitiet", function () {
            //    var dataItem = $("#gridLichSuXuatVatTuKhoDonVi").data("kendoGrid").dataItem($(this).closest("tr"));
            //    CreateModalWithSize("mdlChiTietPhieuXuat", "90%", "90%", null, GetTextLanguage("chitietphieuxuat"));
            //    $("#mdlChiTietPhieuXuat").load("/QuanLyVatTuKhoDonVi" + "/PopupXuatVatTuKhoDonVi?PhieuXuatVatTuID=" + dataItem.PhieuXuatVatTuID + "&LoaiXuat=" + dataItem.LoaiHinhXuat);
            //});
            $("#mdlLichSuXuatVatTuKhoDonVi .xuat-excel").click(function () {
                var filer = [];
                var filers = $("#gridLichSuXuatVatTuKhoDonVi").data("kendoGrid").dataSource.filter();
                if (filers != null && filers != undefined) {
                    for (var i = 0; i < filers.filters.length; i++) {
                        filers.filters[i].Operator = filers.filters[i].operator;
                        filer.push(filers.filters[i]);
                    }
                }
                location.href = currentController + '/XuatExcelLichSuXuatVatTuKhoDonVi?filters=' + JSON.stringify(filer) + '&LoaiHinhXuat=' + $("#ipLoaiHinhXuat").val() + '&TuNgay=' + $("#inputTuNgay2").val() + '&DenNgay=' + $("#inputDenNgay2").val() + '&CongTrinhDenID=' + $("#ipCongTrinhDen").val() + '&DoiThiCongDenID=' + $("#ipDoiThiCongDen").val() + '&CongTrinhID=' + $("#drdCongTrinh").val();
            });
        }
    });
    $(".tab_tonkhotheogia").click(function () {
        $("#gridtheogia").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/TonVatTuTheoGia",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#drdCongTrinh").data("kendoDropDownList").value(),
                        },
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
                sort: [{ field: "TenVatTu", dir: "asc" }],
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        fields: {
                            TenVatTu: { type: "string" },
                            TenDonVi: { type: "string" },
                            SoLuongNhap: { type: "number" },
                            SoLuongXuat: { type: "number" },
                            SoLuongTon: { type: "number" },
                            GhiChu: { type: "string" }
                        },
                    }
                },
            }),
            height: window.innerHeight * 0.80,
            width: "100%",
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            dataBinding: function () {
                record = 0;
            },
            pageable: pageableAll,
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    template: "#= ++record #",
                    width: 60,
                    attributes: alignCenter
                },
                {
                    field: "TenVatTu",
                    title: GetTextLanguage("tenvattu"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft,
                    width: 200
                },
                {
                    field: "TenVietTat",
                    title: GetTextLanguage("tenviettat"),
                    filterable: FilterInTextColumn,
                    attributes: alignLeft,
                    width: 200
                },
                {
                    field: "TenDonVi",
                    title: GetTextLanguage("tendonvi"),
                    filterable: FilterInTextColumn,
                    attributes: alignCenter,
                    width: 150
                },
                {
                    field: "DonGia",
                    title: "Đơn giá",
                    filterable: FilterInColumn,
                    attributes: alignRight,
                    format: "{0:n2}",
                    width: 100,
                    filterable: {
                        cell: {
                            template: function (args) {
                                // create a DropDownList of unique values (colors)
                                args.element.kendoDropDownList({
                                    dataSource: [{ id: "0-50000", text: "0-50,000" },
                                        { id: "50001-100000", text: "50,001-200,000" },
                                        { id: "100001-500000", text: "100,001-500,000" },
                                     { id: "500001-1000000", text: "500,000-1,000,000" },
                                     { id: "1000001-2000000", text: "1,000,001-2,000,000" },
                                     { id: "2000001-2000000000", text: ">2,000,000" }],
                                    dataTextField: "text",
                                    dataValueField: "id",
                                    optionLabel: "Tất cả",
                                    valuePrimitive: true
                                });
                            },
                            showOperators: false,
                            operator: "doesnotcontain",
                            suggestionOperator: "doesnotcontain"
                        },

                    },
                },
                {
                    field: "SoLuongNhap",
                    title: "Số lượng nhập",
                    filterable: FilterInColumn,
                    attributes: alignRight,
                    format: "{0:n2}",
                    width: 100
                },
              
                {
                    field: "SoLuongXuat",
                    title: GetTextLanguage("tongxuat"),
                    filterable: FilterInColumn,
                    attributes: alignRight,
                    format: "{0:n2}",
                    width: 100
                },
                {
                    field: "TonHienTai",
                    title: GetTextLanguage("tonhientai"),
                    filterable: FilterInColumn,
                    attributes: alignRight,
                    format: "{0:n2}",
                    width: 100
                },
                
            ],
        });

    });

    $(".tab_tonkhotheophieu").click(function () {
        $("#gridtheophieu").kendoGrid({
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: currentController + "/TonVatTuTheoPhieu",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#drdCongTrinh").data("kendoDropDownList").value(),
                            TuNgay: $("#inputTuNgay5").val(),
                            DenNgay: $("#inputDenNgay5").val()
                        }
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                group: {
                    field: "ThoiGian",
                    dir: "desc",
                    aggregates: [{
                        field: "ThanhTien",
                        aggregate: "sum"
                    }]
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
                sort: [{ field: "TenVatTu", dir: "asc" }],
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        fields: {
                            TenVatTu: { type: "string" },
                            TenDonVi: { type: "string" },
                            TonHienTai: { type: "number" },
                            GhiChu: { type: "string" }
                        },
                    }
                },
            }),

            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: pageableAll,
            height: innerHeight * 0.7,
            width: "100%",
            columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"),
                    attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: formatToDateTime("ThoiGian"),
                    groupHeaderTemplate: "#= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') #",
                },
                 {
                     field: "MaPhieu", title: GetTextLanguage("maphieu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                 },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVietTat", title: GetTextLanguage("tenviettat"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInColumn, width: 150
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("tendonvi"), attributes: { "style": "text-align:left !important;" }, filterable: FilterInTextColumn, width: 100
                },
                {
                    field: "SoLuong", title: "Số lượng nhập", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                },
                {
                    field: "SoLuongXuat", title: "Số lượng xuất", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuongXuat,'n2') #",
                },
                  {
                      field: "SoLuongTon", title: "Số lượng tồn", attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 100,
                      template: "#= kendo.toString(SoLuongTon,'n2') #",
                  },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { "style": "text-align:right !important;" }, width: 200,
                    template: "#= kendo.toString(DonGia,'n2') #",
                    filterable: {
                        cell: {
                            template: function (args) {
                                // create a DropDownList of unique values (colors)
                                args.element.kendoDropDownList({
                                    dataSource: [{ id: "0-50000", text: "0-50,000" },
                                        { id: "50001-100000", text: "50,001-200,000" },
                                        { id: "100001-500000", text: "100,001-500,000" },
                                     { id: "500001-1000000", text: "500,000-1,000,000" },
                                     { id: "1000001-2000000", text: "1,000,001-2,000,000" },
                                     { id: "2000001-2000000000", text: ">2,000,000" }],
                                    dataTextField: "text",
                                    dataValueField: "id",
                                    optionLabel: "Tất cả",
                                    valuePrimitive: true
                                });
                            },
                            showOperators: false,
                            operator: "doesnotcontain",
                            suggestionOperator: "doesnotcontain"
                        },

                    },
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                    attributes: { "style": "text-align:right !important;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(ThanhTien,'n2') #",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                },
            ],
            dataBound: function (e) {
                var rows = this.items();
                var dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling !== null && this.nextElementSibling.className === "k-grouping-row") {
                        dem = 0;
                    }
                });
            },
        });
        var date = new Date(), y = date.getFullYear(), m = date.getMonth(), d = date.getDay();
        $("#inputTuNgay5").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(y - 1, m, d), change: function () {
                var value = this.value();
                gridReload("gridtheophieu", { CongTrinhID: $("#drdCongTrinh").val(), TuNgay: kendo.toString(value, 'dd/MM/yyyy'), DenNgay: $("#inputDenNgay5").val() });
            }
        });
        $("#inputDenNgay5").kendoDatePicker({
            format: "dd/MM/yyyy", value: new Date(), change: function () {
                var value = this.value();
                gridReload("gridtheophieu", { CongTrinhID: $("#drdCongTrinh").val(), TuNgay: $("#inputTuNgay5").val(), DenNgay: kendo.toString(value, 'dd/MM/yyyy') });
            }
        });

    })
});