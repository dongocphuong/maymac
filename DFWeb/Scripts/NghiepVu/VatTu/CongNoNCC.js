﻿$(document).ready(function () {
    var crudServiceBaseUrl = "/CongNoNCC";

    $("#grid").kendoGrid({
        dataSource: getDataSourceGrid(crudServiceBaseUrl, "/LayCongNoNhaCungCap"),
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        columns: [
             {
                 title: GetTextLanguage("stt"),
                 template: "<span class='stt'></span>",
                 width: 50,
                 lockable: false, align: "center"
             },
            {
                title: "",
                width: 100,
                template: '<div class="text-center"><button class="btn btn-xs btn-primary xemchitiet">GetTextLanguage("xemchitiet")</button> </div>'
            },
           
             {
                 field: "NhaCungCapID",
                 title: "ID",
                 hidden:true,
                 filterable: {
                     cell: {
                         showOperators: false,
                         operator: "contains"

                     }
                 }
             },
            {
                field: "TenNhaCungCap",
                title: GetTextLanguage("tennhacungcap"),
                filterable: FilterInTextColumn
            },
            {
                field: "TongTien",
                title: GetTextLanguage("tongtiendanhap"),
                attributes: { style: "text-align:right;" },
                filterable: FilterInTextColumn,
                template: "#=kendo.toString(TongTien,'n2')#"
            },
             {
                 field: "SDT",
                 title: GetTextLanguage("sdt"),
                 filterable: FilterInTextColumn,
             },
              {
                  field: "Email",
                  title: GetTextLanguage("email"),
                  filterable: FilterInTextColumn,
              },
               
               

            //{ command: "destroy", title: "&nbsp;", width: 150 }
        ],
        dataBound: function (e) {
            var rows = this.items();
            dem = 0;
            $(rows).each(function () {
                dem++;
                var rowLabel = $(this).find(".stt");
                $(rowLabel).html(dem);
                if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                    dem = 0;
                }
            });
            e.sender.tbody.find(".k-button.fa-list-alt").each(function (idx, element) {
                $(element).removeClass("fa fa-list-alt").find("span").addClass("fa fa-list-alt");
            });
            e.sender.tbody.find(".k-button.fa-download").each(function (idx, element) {
                $(element).removeClass("fa fa-download").find("span").addClass("fa fa-download");
            });
            e.sender.tbody.find(".k-button.fa-upload").each(function (idx, element) {
                $(element).removeClass("fa fa-upload").find("span").addClass("fa fa-upload");
            });


            e.sender.tbody.find(".k-button").each(function (idx, element) {
                $(element).removeClass("k-button k-button-icontext").addClass("btn btn-primary btn-action-grid");
            });
        },
    });

    $("#grid").on("click", ".xemchitiet", xemchitiet);

    function xemchitiet() {
        row = $(this).closest("tr"),
        grid = $("#grid").data("kendoGrid"),
        dataItem = grid.dataItem(row);
        $("#mdlChiTiet").empty();
        //open modal
        var mdl = $('#mdlChiTiet').kendoWindow({
            width: "90%",
            height: "90%",
            title: GetTextLanguage("chitiet"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            },
        }).data("kendoWindow").center().open();
        setTitleWindow("mdlChiTiet", GetTextLanguage("chitiet"));
        mdl.content(kendo.template('<div id="gridchittiet"> </div>'));
        $("#gridchittiet").kendoGrid({
            dataSource: getDataSourceGrid(crudServiceBaseUrl, "/LayDanhSachPhieuNhapTheoNhaCungCap", { NhaCCID: dataItem.get("NhaCungCapID") }),
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            height: window.innerHeight * 0.8,
            width: "100%",
            columns: [
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"),
                    filterable: {
                        cell: {
                            showOperators: false,
                            operator: "contains"
                        }
                    },
                    hidden: true,
                    //+ " [#= getSumThanhTien(value) #]"
                    template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                    groupHeaderTemplate: "GetTextLanguage('thoigian'): #= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') #"  + "<div class='pull-right'><button class='btn btn-xs btn-primary xemchitiet'> GetTextLanguage('xemchitiet')</button></div>",
                },
                {
                    field: "TenNhanVien", title: GetTextLanguage("nhanviennhap"), filterable: FilterInTextColumn,
                },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), filterable: FilterInTextColumn,
                    attributes: {
                        style: "text-align:left",
                    }
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("tendonvi"), filterable: FilterInTextColumn,
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), filterable: {
                        cell: {
                            showOperators: false,
                            operator: "contains"

                        }
                    }
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), filterable: {
                        cell: {
                            showOperators: false,
                            operator: "contains"

                        }
                    },
                    attributes: {
                        style: "text-align:right",
                    },
                    template: "#= kendo.toString(DonGia,'n2') #",
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"),
                    attributes: { "style": "text-align:right !important;" },
                    filterable: {
                        cell: {
                            showOperators: false,
                            operator: "contains"

                        }
                    },
                    attributes: {
                        style: "text-align:right",
                    },
                    template: "#= kendo.toString(ThanhTien,'n2') #",
                    //groupFooterTemplate:"#=sum#",
                    footerTemplate: "<div style='float:right;color:red'>#=kendo.toString(sum,'n2')# </div>",
                },
                {
                    field: "TrangThaiDuyet", title: GetTextLanguage("trangthaiduyet"), filterable: FilterInTextColumn,
                },
                {
                    field: "GhiChu", title: GetTextLanguage("ghichu"), filterable: FilterInTextColumn,
                    attributes: {
                        style: "text-align:left",
                    },
                }
            ],
            dataBound: function (e) {
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
                e.sender.tbody.find(".k-button.fa-list-alt").each(function (idx, element) {
                    $(element).removeClass("fa fa-list-alt").find("span").addClass("fa fa-list-alt");
                });
                e.sender.tbody.find(".k-button.fa-download").each(function (idx, element) {
                    $(element).removeClass("fa fa-download").find("span").addClass("fa fa-download");
                });
                e.sender.tbody.find(".k-button.fa-upload").each(function (idx, element) {
                    $(element).removeClass("fa fa-upload").find("span").addClass("fa fa-upload");
                });


                e.sender.tbody.find(".k-button").each(function (idx, element) {
                    $(element).removeClass("k-button k-button-icontext").addClass("btn btn-primary btn-action-grid");
                });
            },
        });



       
    }

})