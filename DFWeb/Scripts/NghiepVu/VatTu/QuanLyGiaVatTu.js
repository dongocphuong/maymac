﻿$(document).ready(function () {
    var currentController = "/QuanLyGiaVatTu";
    var datasourceQuanLyGiaVatTu = new kendo.data.DataSource({
        transport: {
            read: {
                url: currentController + "/GetDataQuanLyGiaVatTu",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
            },
        },
        type: "json",
        pageSize: 25,
        batch: true,
        schema: {
            data: "data",
            total: "total",
            model: {
                id: "DonGiaDoiTacVatTuID",
                fields: {
                    VatTuID: { type: "string" },
                    MaVatTu: { type: "string" },
                    TenVatTu: { type: "string" },
                    TenDonVi: { type: "string" },
                    TenDoiTac: { type: "string" },
                    DoiTacID: { type: "string" },
                    DonGiaHienTai: { type: "number" },
                    NgayCapNhat: { type: "date" },
                },
            },
        },
    });
    var gridQuanLyGiaVatTu = $("#gridQuanLyGiaVatTu").kendoGrid({
        dataSource: datasourceQuanLyGiaVatTu,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        height: window.innerHeight * 0.78,
        width: "100%",
        detailTemplate: kendo.template($("#tplLichSuThayDoiGia").html()),
        detailInit: lichSuThayDoiGia,
        columns: [
            {
                title: GetTextLanguage("stt"),
                template: "<span class='stt'></span>",
                width: 60,
                align: "center"
            },
            {
                field: "TenVatTu", width: 200,
                title: GetTextLanguage("tenvattu"),
                attributes: { style: "text-align:left;" },
                filterable: FilterInTextColumn
            },
            {
                field: "MaVatTu",
                width: 200,
                attributes: { style: "text-align:left;" },
                title: GetTextLanguage("mavattu"),
                filterable: FilterInTextColumn
            },
            {
                field: "TenDonVi",
                width: 100,
                title: GetTextLanguage("donvi"),
                attributes: { "style": "text-align:left !important;" },
                filterable: FilterInTextColumn
            },
            {
                field: "TenDoiTac",
                width: 200,
                title: GetTextLanguage("tendoitac"),
                attributes: { "style": "text-align:left !important;" },
                filterable: FilterInTextColumn
            },
            {
                field: "DonGiaHienTai",
                title: GetTextLanguage("dongiahientai"),
                format: "{0:n2}",
                attributes: alignRight,
                filterable: FilterInColumn, width: 150,
                template: "#=DonGiaHienTai==0?'Chưa Cập Nhật':kendo.toString(DonGiaHienTai,'n2') #"
            },
            {
                field: "ThoiGianCapNhat",
                width: 200,
                title: GetTextLanguage("thoigiancapnhat"),
                attributes: alignCenter,
                filterable: FilterInTextColumn,
                template: "#=ThoiGianCapNhat==null?'Chưa Cập Nhật': kendo.toString(kendo.parseDate(ThoiGianCapNhat), 'dd/MM/yyyy') #",
            },

        ],
        dataBound: function (e) {
            this.expandRow(this.tbody.find("tr.k-master-row").first());
            var rows = this.items();
            var dem = 0;
            $(rows).each(function () {
                dem++;
                var rowLabel = $(this).find(".stt");
                $(rowLabel).html(dem);
                if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                    dem = 0;
                }
            });
        },
    });
    function lichSuThayDoiGia(e) {
        var detailRow = e.detailRow;
        var datasourceLichSuThayDoiGia = new kendo.data.DataSource({
            transport: {
                read: {
                    url: currentController + "/GetDataLichSuGiaVatTu",
                    dataType: "json",
                    type: "GET",
                    data: {
                        VatTuID: e.data.VatTuID
                    },
                    contentType: "application/json; charset=utf-8",
                },
            },
            type: "json",
            pageSize: 25,
            batch: true,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "DonGiaDoiTacVatTuID",
                    fields: {
                        TenDoiTac: { type: "string" },
                        DonGia: { type: "number" },
                        ThoiGianCapNhat: { type: "date" },
                    },
                },
            },
        });

        detailRow.find(".tplLichSuThayDoiGia").kendoGrid({
            dataSource: datasourceLichSuThayDoiGia,
            scrollable: false,
            sortable: true,
            pageable: true,
            columns: [
                {
                    field: "TenDoiTac",
                    width: 200,
                    title: GetTextLanguage("doitac"),
                    attributes: { "style": "text-align:left !important;" },
                    filterable: FilterInTextColumn
                },
                {
                    field: "DonGia",
                    title: GetTextLanguage("dongia"),
                    format: "{0:n2}",
                    attributes: alignRight,
                    filterable: FilterInColumn, width: 150,
                    template: "#=DonGia==0?'" + GetTextLanguage("chuacapnhat") + "':kendo.toString(DonGia,'n2') #"
                },
                {
                    field: "ThoiGianCapNhat",
                    width: 200,
                    title: GetTextLanguage("thoigiancapnhat"),
                    attributes: alignCenter,
                    filterable: FilterInTextColumn,
                    template: "#=ThoiGianCapNhat==null?'" + GetTextLanguage("chuacapnhat") + "': kendo.toString(kendo.parseDate(ThoiGianCapNhat), 'dd/MM/yyyy') #",
                },
            ]
        });
    }
    //$("#gridQuanLyGiaVatTu .k-grid-content").on("dblclick", "td", function () {
    //    var dataItem = $("#gridQuanLyGiaVatTu").data("kendoGrid").dataItem($(this).closest("tr"));
    //    CreateModalWithSize("mdlCapNhatGiaVatTu", "30%", "40%", "tplQuanLyGiaVatTu", GetTextLanguage("capnhatdongiavattu") + dataItem.TenVatTu + " - " + dataItem.MaVatTu)
    //    $("#ipVatTuID").val(dataItem.VatTuID);
    //    $("#ipThoiGian").kendoDateTimePicker({
    //        format: "dd/MM/yyyy HH:mm",
    //        value: new Date()
    //    });
    //    $("#ipDonGia").autoNumeric(autoNumericOptionsMoney);
    //    $("#drdDoiTac").kendoDropDownList({
    //        dataTextField: 'TenDoiTac',
    //        dataValueField: 'DoiTacID',
    //        filter: "contains",
    //        dataSource: new kendo.data.DataSource({
    //            transport: {
    //                read: {
    //                    url: "/QuanLyVatTuKhoTong/GetAllDataDoiTac",
    //                    dataType: "json",
    //                    type: "POST",
    //                    contentType: "application/json; charset=utf-8",
    //                },
    //            },
    //            schema: {
    //                type: "json",
    //                data: "data"
    //            }
    //        }),
    //    });
    //    $("#btnHuy").click(function () {
    //        $("#mdlCapNhatGiaVatTu").data("kendoWindow").close();
    //    });
    //    $("#btnCapNhat").click(function () {
    //        var VatTuID = $("#ipVatTuID").val();
    //        var DoiTacID = $("#drdDoiTac").data("kendoDropDownList").value();
    //        var DonGia = $("#ipDonGia").autoNumeric("get");
    //        var ThoiGian = $("#ipThoiGian").data("kendoDateTimePicker").value();
    //        if (DoiTacID == null || DoiTacID == "" || DonGia == null || DonGia == "" || ThoiGian == null || ThoiGian == "") {
    //            alertToastr({ code: "error", message: GetTextLanguage("vuilongnhapdayducactruong") });
    //        }
    //        else
    //        {
    //            $.ajax({
    //                url: currentController + "/CapNhatGiaVatTu",
    //                type: "POST",
    //                data: {
    //                    VatTuID: VatTuID,
    //                    DoiTacID: DoiTacID,
    //                    DonGia: DonGia,
    //                    ThoiGianCapNhat: kendo.toString(ThoiGian,"dd/MM/yyyy HH:mm")
    //                },
    //                complete: function (e) {
    //                    alertToastr(e.responseJSON);
    //                    if (e.responseJSON.code == "success") {
    //                        $("#mdlCapNhatGiaVatTu").data("kendoWindow").close();
    //                        $("#gridQuanLyGiaVatTu").data("kendoGrid").dataSource.read();
    //                    }
    //                }
    //            });
    //        }
    //    });

    //});

});