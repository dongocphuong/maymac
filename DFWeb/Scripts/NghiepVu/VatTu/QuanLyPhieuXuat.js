﻿$(document).ready(function () {
    var crudServiceBaseUrl = "/QuanLyPhieuXuat";
    var doithicongid = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
    var congtrinhid = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
    var crudServiceBaseUrl_CongTrinh = "/G"

    $("#inputCongTrinh").kendoDropDownList({
        dataTextField: 'TenCongTrinh',
        dataValueField: 'CongTrinhID',
        filter: "contains",
        optionLabel: GetTextLanguage("choncongtrinh"),
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + "/GetAllDataCongTrinh",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                },
            },
            schema: {
                type: "json",
                data: "data"
            }
        }),
        change: function (e) {
            var value = this.value();
            $("#inputDoiThiCong").data("kendoDropDownList").dataSource.read({ CongTrinhID: value });
            $("#gridXuatKhoDonVi").data("kendoGrid").dataSource.read({ CongTrinhID: value, DoiThiCongID: $("#inputDoiThiCong").data("kendoDropDownList").value() })
        },
    });
    $("#inputDoiThiCong").kendoDropDownList({
        dataTextField: 'TenDoiThiCong',
        dataValueField: 'DoiThiCongID',
        filter: "contains",
        optionLabel: GetTextLanguage("chondoi"),
        dataSource: new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + "/GetAllDataDoiThiCong",
                    dataType: "json",
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    data: {
                        CongTrinhID: $("#inputCongTrinh").data("kendoDropDownList").value()
                    }

                },
            },
            schema: {
                type: "json",
                data: "data"
            }
        }),
        change: function (e) {
            var value = this.value();
            $("#gridXuatKhoDonVi").data("kendoGrid").dataSource.read({ CongTrinhID: $("#inputCongTrinh").data("kendoDropDownList").value(), DoiThiCongID: $("#inputDoiThiCong").data("kendoDropDownList").value() })
        },
    });

    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });
    
    var dataXuatKhoDonVi = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetDataXuatKho",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {
                    CongTrinhID: $("#inputCongTrinh").data("kendoDropDownList").value(),
                    DoiThiCongID: $("#inputDoiThiCong").data("kendoDropDownList").value(),

                }
            },
            parameterMap: function (options, type) {
                console.log(options);
                return JSON.stringify(options);
            }
        },
        type: "json",
        pageSize: 25,
        schema: {
            data: "data",
            total: "total"
        },
        group: {
            field: "ThoiGian",
            dir: "desc",
            aggregates: [{
                field: "ThanhTien",
                aggregate: "sum"
            }]
        },
        aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
    });
    var dataXuatKhoTong = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetDataXuatKho",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {
                    CongTrinhID: congtrinhid,
                    DoiThiCongID: doithicongid,

                }
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        pageSize: 25,
        schema: {
            data: "data",
            total: "total"
        },
        group: {
            field: "ThoiGian",
            dir: "desc",
            aggregates: [{
                field: "ThanhTien",
                aggregate: "sum"
            }]
        },
        aggregate: [{ field: "ThanhTien", aggregate: "sum" }, { field: "SoLuong", aggregate: "sum" }]
    });

    $("#gridXuatKhoDonVi").kendoGrid({
        dataSource: dataXuatKhoDonVi,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        height: window.innerHeight * 0.7,
        width: "100%",
        columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                    groupHeaderTemplate: GetTextLanguage("thoigian") + " : #= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') #" + "<div class='pull-right'><button class='btn btn-xs btn-primary xemchitiet'> " + GetTextLanguage("xemchitiet") + "</button></div>",
                },
                {
                    field: "LoaiHinhXuat", title: GetTextLanguage("loaihinhxuat"), attributes: alignCenter, filterable: FilterInTextColumn, width: 100,
                    template: "#=LoaiHinhXuat==2?'Xuất Điều Chuyển':'Xuất Sử Dụng'#"
                },
                {
                    field: "NhanVienNhap", title: GetTextLanguage("nhanviennhap"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(DonGia,'n2') #"
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#=kendo.toString(ThanhTien,'n2') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "NhanVienNhan", title: GetTextLanguage("nhanviennhan"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "CongTrinhDen", title: GetTextLanguage("congtrinhnhan"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "DoiDen", title: GetTextLanguage("doithicongnhan"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "NoiDung", title: GetTextLanguage("ghichu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                }
        ],
        dataBound: function (e) {
            var rows = this.items();
            var dem = 0;
            $(rows).each(function () {
                dem++;
                var rowLabel = $(this).find(".stt");
                $(rowLabel).html(dem);
                if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                    dem = 0;
                }
            });
        },
    });
    $("#gridXuatKhoDonVi").on("click", ".xemchitiet", xemchitiet);
    $("#btnAddDonVi").click(function () {
        $('#mdlThemDonVi').kendoWindow({
            width: "100%",
            height: "100%",
            title: GetTextLanguage("themphieuxuatkhodonvi"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        $('#mdlThemDonVi').data("kendoWindow").content(kendo.template($('#tempthemdonvi').html()));

        //Lưu Phiếu
        $("#saveas").click(function () {
            var x = confirm(GetTextLanguage("luuhaykhong"));
            if (x) {
                if (dataobject.items.length == 0) {
                    showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("khongcovattunaotrenphieu"));
                }
                else {
                    if ($("#LoaiHinh").val() == "") {
                        showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("banphaichonloaihinhxuat"));
                    }
                    else {
                        var booladd = true;
                        if ($("#LoaiHinh").val() == 0 && ($("#CongTrinh").val() == "" || $("#DoiThiCong").val() == "" || $("#NhanVien").val() == "")) {
                            showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("banphaichoncongtringvadoinhanvattunguoinhan"));
                            booladd = false;
                        }

                        //Check lại số lượng tồn 1 lần nữa
                        for (var i = 0; i < dataobject.items.length; i++) {
                            if (dataobject.items[i].SoLuongTon < dataobject.items[i].SoLuong) {
                                showToast("error", GetTextLanguage("canhbao"), dataobject.items[i].TenVatTu + GetTextLanguage("odongthu") + (i + 1) + GetTextLanguage("chicon") + (" + dataobject.items[i].SoLuongTon + ") + GetTextLanguage("khongdusoluongtondexuat"));
                                booladd = false;
                                break;
                            }
                        }

                        dataobject.ThoiGian = $("#ThoiGianNhap").val();
                        dataobject.DoiThiCongNhanID = $("#DoiThiCong").val();
                        dataobject.CongTrinhNhanID = $("#CongTrinh").val();
                        dataobject.NguoiNhanID = $("#NhanVien").val();
                        dataobject.LoaiHinh = $("#LoaiHinh").val();
                        if ($("#nhapmois").is(':checked'))
                            dataobject.creatNhap = 1; // checked
                        else
                            dataobject.creatNhap = 0;

                        if (booladd == true) {
                            $(".windows8").css("display", "block");
                            var dataimg = new FormData();
                            var ins = storedFiles.length;
                            if (ins > 0) {
                                for (var x = 0; x < ins; x++) {
                                    dataimg.append("uploads", storedFiles[x]);
                                }

                            }

                            var objXhr = new XMLHttpRequest();
                            objXhr.onreadystatechange = function () {
                                if (objXhr.readyState == 4) {
                                    if (objXhr.responseText != "") {
                                        var strUrl = objXhr.responseText.split(',');
                                        for (var k = 0; k < strUrl.length; k++) {
                                            dataobject.Anh.push(strUrl[k]);
                                        }
                                    }

                                    setTimeout(function () {
                                        $.ajax({
                                            cache: false,
                                            async: false,
                                            type: "POST",
                                            url: crudServiceBaseUrl + "/AddorUpdatePhieuXuat",
                                            data: { data: JSON.stringify(dataobject), PhieuID: PhieuID },
                                            success: function (data) {
                                                altReturn(data);
                                                if (data.code == "success") {
                                                    gridReload("gridNhapXuatKho", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Xuat });
                                                    closeModel("mdlAddPhieuXuat");
                                                    //remove file anh
                                                    storedFiles = [];
                                                }
                                                $(".windows8").css("display", "none");
                                            },
                                            error: function (xhr, status, error) {
                                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                                $(".windows8").css("display", "none");
                                            }
                                        });
                                    }, 1000);
                                }
                            }

                            objXhr.open("POST", crudServiceBaseUrlUploadFile + "/Uploadfiles", true);
                            objXhr.send(dataimg);

                        }
                    }
                }
            } else {
                return false;
            }
        });
        //Xóa Phiếu
        $("#btnXoa").click(function () {
            var x = confirm(GetTextLanguage("bancomuonxoaphieunaykhong"));
            if (x) {
                $(".windows8").css("display", "block");
                setTimeout(function () {
                    $.ajax({
                        cache: false,
                        async: false,
                        type: "POST",
                        url: crudServiceBaseUrl + "/XoaPhieuXuat",
                        data: { PhieuXuatVatTuID: PhieuID, CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID") },
                        success: function (data) {
                            altReturn(data);
                            if (data.code == "success") {
                                gridReload("gridNhapXuatKho", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Nhap });
                                closeModel("mdlAddPhieuXuat");
                            }
                            $(".windows8").css("display", "none");
                        },
                        error: function (xhr, status, error) {
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                            $(".windows8").css("display", "none");
                        }
                    });
                }, 500);
            } else {
                return false;
            }
        });

        $("#ThoiGianNhap").kendoDateTimePicker({
            optionLabel: GetTextLanguage("thoigiannhap"),
            format: "dd/MM/yyyy HH:mm"
        });
        $("#CongTrinh").kendoDropDownList({
            dataTextField: 'TenCongTrinh',
            dataValueField: 'CongTrinhID',
            filter: "contains",
            optionLabel: GetTextLanguage("choncongtrinh"),
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: crudServiceBaseUrl + "/GetAllDataCongTrinh",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                    },
                },
                schema: {
                    type: "json",
                    data: "data"
                }
            }),
            change: function (e) {
                var value = this.value();
                $("#DoiThiCong").data("kendoDropDownList").dataSource.read({ CongTrinhID: value });
                $("#tempthemdonvi").data("kendoGrid").dataSource.read({ CongTrinhID: value, DoiThiCongID: $("#DoiThiCong").data("kendoDropDownList").value() })
            },
        });
        $("#DoiThiCong").kendoDropDownList({
            dataTextField: 'TenDoiThiCong',
            dataValueField: 'DoiThiCongID',
            filter: "contains",
            optionLabel: GetTextLanguage("chondoi"),
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: crudServiceBaseUrl + "/GetAllDataDoiThiCong",
                        dataType: "json",
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#CongTrinh").data("kendoDropDownList").value()
                        }
                    },
                },
                schema: {
                    type: "json",
                    data: "data"
                }
            }),
            change: function (e) {
                var value = this.value();
                $("#tempthemdonvi").data("kendoGrid").dataSource.read({ CongTrinhID: $("#CongTrinh").data("kendoDropDownList").value(), DoiThiCongID: $("#DoiThiCong").data("kendoDropDownList").value() })
            },
        });
        $("#LoaiHinh").kendoDropDownList({
            filter: "contains",
            optionLabel: GetTextLanguage("loaihinhxuat"),
            dataSource: {
                data: [GetTextLanguage("xuatdieuchuyen"), GetTextLanguage("xuatsudung")]
            },
        });
        $("#NhanVien").kendoDropDownList({
            filter: "contains",
            optionLabel: GetTextLanguage("chonnhanvien"),
            dataSource: {
                data: ["Ngọc Khấc", "Pog Xạo Loz"]
            },
        });

        $("#gridThemPhieuXuat").kendoGrid({
            editable: true,
            selectable: true,
            height: window.innerHeight * 0.4,
            dataSource: {
                //data: dataXuatKhoDonVi,
                schema: {
                    model: {
                        id: "VatTuID",
                        fields: {
                            VatTuID: { editable: false, },
                            TenDonVi: { type: "string", editable: false },
                            TenVatTu: { type: "string", editable: false },
                            TenVietTat: { type: "string", editable: false },
                            SoLuong: { type: "number", editable: true, validation: { min: 1, format: "n2" } },
                            DonGia: { type: "number", editable: true, validation: { min: 0, format: "n2" } },
                            DonGiaNhap: { type: "number", editable: false, validation: { min: 0, format: "n2" } },
                            ThanhTien: { type: "number", editable: false } // If change editable to true, "set" work, but i can manualy change the value.
                        }
                    }
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
            },
            databound: function () {
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowlabel = $(this).find(".stt");
                    $(rowlabel).html(dem);
                    if (this.nextelementsibling != null && this.nextelementsibling.classname == "k-grouping-row") {
                        dem = 0;
                    }
                });

                if (dataedit != null) {
                    //check hiển thị nút xóa dòng
                    $("#gridThemPhieuXuat tbody tr .xoaitem").each(function () {
                        var currentdataitem = $("#gridThemPhieuXuat").data("kendogrid").dataitem($(this).closest("tr"));
                        ////check in the current dataitem if the row is deletable
                        if (dataedit.ishethong != true) {
                            //laf heej thoong thif ms duoc suar va xoa
                            $(this).remove();
                        }
                    });
                }
            },
            columns:
            [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                { field: "VatTuID", hidden: true },
                { field: "TenVatTu", width: 200, title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, footerTemplate: GetTextLanguage("tong"), filterable: FilterInTextColumn },
                { field: "TenVietTat", width: 200, title: GetTextLanguage("tenviettat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn },
                { field: "TenDonVi", width: 200, title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn },
                { field: "DonGiaNhap", width: 150, title: GetTextLanguage("gianhap"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn },
                { field: "DonGia", width: 150, title: GetTextLanguage("giaxuat"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn },
                { field: "SoLuong", width: 100, title: GetTextLanguage("soluong"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    aggregates: ["sum"], footerTemplate: "<div style='text-align:right !important;'> #: kendo.toString(sum, \"n2\") #</div>"
                },
                {
                    command: [
                        { text: GetTextLanguage("xoa"), click: xoarow, className: "xoaitem" }
                    ], width: 100
                },
            ],
            save: function (e) {
                if (e.values.SoLuong || e.values.DonGia) {
                    var sl = e.values.SoLuong || e.model.SoLuong;
                    var dg = e.values.DonGia || e.model.DonGia;
                    for (var i = 0; i < dataobject.items.length; i++) {
                        if (dataobject.items[i].ID === e.model.get("ID")) {
                            if (e.values.SoLuong) {
                                dataobject.items[i].SoLuong = sl;
                            }
                            else {
                                dataobject.items[i].DonGia = dg;
                            }
                            dataobject.items[i].ThanhTien = sl * dg
                            gridReload("gridThemPhieuXuat", {});
                            break;
                        }
                    }

                }
            }
        }).data("kendoGrid");
        function xoarow(e) {
            e.preventDefault();
            var dataItemrow = this.dataItem($(e.currentTarget).closest("tr"));
            for (var i = 0; i < dataobject.items.length; i++) {
                if (dataobject.items[i].VatTuID === dataItemrow.get("VatTuID")) {
                    dataobject.items.splice(i, 1);
                    gridReload("gridThemPhieuXuat", {});
                    break;
                }
            }
        };

        $('#addrow').click(function () {
            var checkedIds = {};
            $('#mdlDanhSachVatTuXuat').kendoWindow({
                width: "80%",
                height: "80%",
                title: GetTextLanguage("danhsachvattu"),
                modal: true,
                visible: false,
                resizable: false,
                actions: [
                    "Close"
                ],
                deactivate: function () {
                    $(this.element).empty();
                }
            }).data("kendoWindow").center().open();
            $('#mdlDanhSachVatTuXuat').data("kendoWindow").content(kendo.template($('#tempDanhSachVatTuXuat').html()));

            var dataDanhSachVatTuXuat = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: crudServiceBaseUrl + "/GetDanhSachVatTuXuat",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                    },
                    parameterMap: function (options, type) {
                        console.log(options);
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                pageSize: 25,
                schema: {
                    data: "data",
                    total: "total",
                    model: {
                        id: "VatTuID",
                        fields: {
                            VatTuID: { type: "string" },
                            TenNhom: { type: "string" },
                            TenVatTu: { type: "string" },
                            TenDonVi: { type: "string" },
                            SoLuongTon: { type: "number" },
                            DonGiaNhap: {type: "number"},
                            SoLuong: { type: "number" },
                            DonGia: { type: "number" },
                            select: {type: "boolean"}
                        }
                    }
                }
            });

            var gridDanhSachVatTuXuat = $("#gridDanhSachVatTuXuat").kendoGrid({
                dataSource: dataDanhSachVatTuXuat,
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                editable: true,
                pageable: {
                    refresh: true,
                    buttonCount: 5,
                    pageSizes: true,
                    messages: messagegrid
                },
                height: window.innerHeight * 0.6,
                width: "100%",
                dataBound: onDataBound,
                //toolbar: kendo.template($("#btnThem").html()),
                columns: [
                    {
                        field: "select",
                        title: "&nbsp;",
                        template: "<input type='checkbox' class='checkbox' />",
                        sortable: false,
                        filterable: false,
                        width: 32
                    },
                    {
                        title: GetTextLanguage("stt"),
                        template: "<span class='stt'></span>",
                        width: 60,
                        align: "center"
                    },
                    {
                        field: "TenNhom", title: GetTextLanguage("nhom"), hidden: true
                    },
                    {
                        field: "TenVatTu", title: GetTextLanguage("vattu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn
                    },
                    {
                        field: "TenDonVi", title: GetTextLanguage("donvi"), width: 100, filterable: FilterInTextColumn
                    },
                    {
                        field: "SoLuongTon", width: 100, title: GetTextLanguage("soluongton"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn
                    },
                    {
                        field: "DonGiaNhap", width: 150, title: GetTextLanguage("dongianhap"), attributes: { "style": "text-align:right !important;" }, format: "{0:n2}", filterable: FilterInColumn
                    },
                    {
                        field: "DonGia", width: 150, title: GetTextLanguage("giaxuat"), attributes: { "style": "text-align:right !important;" }, format: "{0:n2}", filterable: FilterInColumn
                    },
                    {
                        field: "SoLuong", width: 100, title: GetTextLanguage("soluong"), format: "{0:n2}", filterable: FilterInColumn, attributes: { "class": "soluongcheck" }
                    },

                    //{ command: "destroy", title: "&nbsp;", width: 150 }
                ]
            }).data("kendoGrid");

            $("#btn-add-vt").on("click", function () {
                var data = $("#gridDanhSachVatTuXuat").data("kendoGrid").dataSource.data();
                console.log(data);
            });

            //resizeWindow("gridDanhSachVatTuXuat", "mdlDanhSachVatTuXuat");
            //gridDanhSachVatTuXuat.table.on("click", ".checkbox", function () {
                
            //});
            //gridDanhSachVatTuXuat.table.on("click", ".soluongcheck", selectRowSoLuong);

            function selectRow() {
                console.log(this.checked);
                var checked = this.checked,
                    rows = $(this).closest("tr"),
                    grids = $("#gridDanhSachVatTuXuat").data("kendoGrid"),
                    dataItems = grids.dataItem(rows);
                checkedIds[dataItems.uid] = checked;
                if (checked) {
                    //-select the row
                    rows.addClass("k-state-selected");
                } else {
                    //-remove selection
                    rows.removeClass("k-state-selected");
                }
            }
            function selectRowSoLuong() {
                var rows = $(this).closest("tr"),
                    grids = $("#gridDanhSachVatTuXuat").data("kendoGrid"),
                    dataItems = grids.dataItem(rows);
                checkedIds[dataItems.uid] = true;
                rows.addClass("k-state-selected").find(".checkbox").attr("checked", "checked");

            }
            function onDataBound(e) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (checkedIds[view[i].uid]) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                    }
                }

                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            }
        });
    });

    $("#gridXuatKhoTong").kendoGrid({
        dataSource: dataXuatKhoTong,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        height: window.innerHeight * 0.7,
        width: "100%",
        columns: [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                {
                    field: "ThoiGian", title: GetTextLanguage("thoigian"), attributes: alignCenter, filterable: FilterInColumn, width: 150,
                    hidden: true,
                    template: "#= kendo.toString(kendo.parseDate(ThoiGian), 'dd/MM/yyyy hh:mm') #",
                    groupHeaderTemplate: GetTextLanguage("thoigian") + " : #= kendo.toString(kendo.parseDate(value), 'dd/MM/yyyy hh:mm') #" + "<div class='pull-right'><button class='btn btn-xs btn-primary xemchitiet'> " + GetTextLanguage("xemchitiet") + "</button></div>",
                },
                {
                    field: "LoaiHinhXuat", title: GetTextLanguage("loaihinhxuat"), attributes: alignCenter, filterable: FilterInTextColumn, width: 100
                },
                {
                    field: "NhanVienNhap", title: GetTextLanguage("nhanviennhap"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenVatTu", title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "TenDonVi", title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "SoLuong", title: GetTextLanguage("soluong"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 100,
                    template: "#= kendo.toString(SoLuong,'n2') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "DonGia", title: GetTextLanguage("dongia"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#= kendo.toString(DonGia,'n2') #"
                },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    template: "#=kendo.toString(ThanhTien,'n2') #",
                    footerTemplate: "<div style='color:red;text-align:right'> #= kendo.toString(sum,'n2') # </div>"
                },
                {
                    field: "NhanVienNhan", title: GetTextLanguage("nhanviennhan"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "CongTrinhNhan", title: GetTextLanguage("congtrinhnhan"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "DoiThiCongNhan", title: GetTextLanguage("doithicongnhan"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                },
                {
                    field: "GhiChu", title: GetTextLanguage("ghichu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn, width: 200
                }
        ],
        dataBound: function (e) {
            var rows = this.items();
            var dem = 0;
            $(rows).each(function () {
                dem++;
                var rowLabel = $(this).find(".stt");
                $(rowLabel).html(dem);
                if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                    dem = 0;
                }
            });
        },
    });

    function xemchitiet() {
        row = $(this).closest("tr"),
        grid = $("#gridXuatKhoDonVi").data("kendoGrid"),
        dataItem = grid.dataItem(row);

        $('#mdlChiTietPhieuXuat').kendoWindow({
            width: "100%",
            height: "100%",
            title: GetTextLanguage("chitietphieuxuatkhodonvi"),
            modal: true,
            visible: false,
            resizable: false,
            actions: [
                "Close"
            ],
            deactivate: function () {
                $(this.element).empty();
            }
        }).data("kendoWindow").center().open();
        $('#mdlChiTietPhieuXuat').data("kendoWindow").content(kendo.template($('#tempchitietphieuxuat').html()));

        $("#thoigians").kendoDateTimePicker({
            optionLabel: "Thời Gian Nhập",
            format: "dd/MM/yyyy HH:mm"
        });
        $("#congtrinhs").kendoDropDownList({
            dataTextField: 'TenCongTrinh',
            dataValueField: 'CongTrinhID',
            filter: "contains",
            optionLabel: GetTextLanguage("choncongtrinh"),
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: crudServiceBaseUrl + "/GetAllDataCongTrinh",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                    },
                },
                schema: {
                    type: "json",
                    data: "data"
                }
            }),
            change: function (e) {
                var value = this.value();
                $("#doithiongs").data("kendoDropDownList").dataSource.read({ CongTrinhID: value });
                $("#tempchitietphieuxuat").data("kendoGrid").dataSource.read({ CongTrinhID: value, DoiThiCongID: $("#doithiongs").data("kendoDropDownList").value() })
            },
        });
        $("#doithiongs").kendoDropDownList({
            dataTextField: 'TenDoiThiCong',
            dataValueField: 'DoiThiCongID',
            filter: "contains",
            optionLabel: GetTextLanguage("chondoi"),
            dataSource: new kendo.data.DataSource({
                transport: {
                    read: {
                        url: crudServiceBaseUrl + "/GetAllDataDoiThiCong",
                        dataType: "json",
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        data: {
                            CongTrinhID: $("#congtrinhs").data("kendoDropDownList").value()
                        }
                    },
                },
                schema: {
                    type: "json",
                    data: "data"
                }
            }),
            change: function (e) {
                var value = this.value();
                $("#tempchitietphieuxuat").data("kendoGrid").dataSource.read({ CongTrinhID: $("#congtrinhs").data("kendoDropDownList").value(), DoiThiCongID: $("#doithiongs").data("kendoDropDownList").value() })
            },
        });
        $("#loaihinhs").kendoDropDownList({
            filter: "contains",
            optionLabel: GetTextLanguage("loaihinhxuat"),
            dataSource: {
                data: [GetTextLanguage("xuatdieuchuyen"), GetTextLanguage("xuatsudung")]
            },
        });
        $("#nhanviens").kendoDropDownList({
            filter: "contains",
            optionLabel: GetTextLanguage("chonnhanvien"),
            dataSource: {
                data: ["Ngọc Khấc", "Pog Xạo Loz"]
            },
        });

        $("#gridChiTietPhieuXuat").kendoGrid({
            editable: true,
            selectable: true,
            height: window.innerHeight * 0.4,
            dataSource: {
                //data: dataXuatKhoDonVi,
                schema: {
                    model: {
                        id: "VatTuID",
                        fields: {
                            VatTuID: { editable: false, },
                            TenDonVi: { type: "string", editable: false },
                            TenVatTu: { type: "string", editable: false },
                            TenVietTat: { type: "string", editable: false },
                            SoLuong: { type: "number", editable: true, validation: { min: 1, format: "n2" } },
                            DonGia: { type: "number", editable: true, validation: { min: 0, format: "n2" } },
                            DonGiaNhap: { type: "number", editable: false, validation: { min: 0, format: "n2" } },
                            ThanhTien: { type: "number", editable: false } // If change editable to true, "set" work, but i can manualy change the value.
                        }
                    }
                },
                aggregate: [{ field: "ThanhTien", aggregate: "sum" }],
            },
            databound: function () {
                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowlabel = $(this).find(".stt");
                    $(rowlabel).html(dem);
                    if (this.nextelementsibling != null && this.nextelementsibling.classname == "k-grouping-row") {
                        dem = 0;
                    }
                });

                if (dataedit != null) {
                    //check hiển thị nút xóa dòng
                    $("#gridThemPhieuXuat tbody tr .xoaitem").each(function () {
                        var currentdataitem = $("#gridThemPhieuXuat").data("kendogrid").dataitem($(this).closest("tr"));
                        ////check in the current dataitem if the row is deletable
                        if (dataedit.ishethong != true) {
                            //laf heej thoong thif ms duoc suar va xoa
                            $(this).remove();
                        }
                    });
                }
            },
            columns:
            [
                {
                    title: GetTextLanguage("stt"),
                    template: "<span class='stt'></span>",
                    width: 60,
                    align: "center"
                },
                { field: "VatTuID", hidden: true },
                { field: "TenVatTu", width: 200, title: GetTextLanguage("tenvattu"), attributes: { style: "text-align:left;" }, footerTemplate: GetTextLanguage("tong"), filterable: FilterInTextColumn },
                { field: "TenVietTat", width: 200, title: GetTextLanguage("tenviettat"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn },
                { field: "TenDonVi", width: 200, title: GetTextLanguage("donvi"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn },
                { field: "DonGiaNhap", width: 150, title: GetTextLanguage("gianhap"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn },
                { field: "DonGia", width: 150, title: GetTextLanguage("giaxuat"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn },
                { field: "SoLuong", width: 100, title: GetTextLanguage("soluong"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn },
                {
                    field: "ThanhTien", title: GetTextLanguage("thanhtien"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn, width: 150,
                    aggregates: ["sum"], footerTemplate: "<div style='text-align:right !important;'> #: kendo.toString(sum, \"n2\") #</div>"
                },
                {
                    command: [
                        { text: GetTextLanguage("xoa"), click: xoarow, className: "xoaitem" }
                    ], width: 100
                },
            ],
            save: function (e) {
                if (e.values.SoLuong || e.values.DonGia) {
                    var sl = e.values.SoLuong || e.model.SoLuong;
                    var dg = e.values.DonGia || e.model.DonGia;
                    for (var i = 0; i < dataobject.items.length; i++) {
                        if (dataobject.items[i].ID === e.model.get("ID")) {
                            if (e.values.SoLuong) {
                                dataobject.items[i].SoLuong = sl;
                            }
                            else {
                                dataobject.items[i].DonGia = dg;
                            }
                            dataobject.items[i].ThanhTien = sl * dg
                            gridReload("gridThemPhieuXuat", {});
                            break;
                        }
                    }

                }
            }
        }).data("kendoGrid");
        function xoarow(e) {
            e.preventDefault();
            var dataItemrow = this.dataItem($(e.currentTarget).closest("tr"));
            for (var i = 0; i < dataobject.items.length; i++) {
                if (dataobject.items[i].VatTuID === dataItemrow.get("VatTuID")) {
                    dataobject.items.splice(i, 1);
                    gridReload("gridThemPhieuXuat", {});
                    break;
                }
            }
        };

        $('#themchitiet').click(function () {
            $('#mdlChiTietVatTu').kendoWindow({
                width: "80%",
                height: "80%",
                title: GetTextLanguage("chitietvattuxuat"),
                modal: true,
                visible: false,
                resizable: false,
                actions: [
                    "Close"
                ],
                deactivate: function () {
                    $(this.element).empty();
                }
            }).data("kendoWindow").center().open();
            $('#mdlChiTietVatTu').data("kendoWindow").content(kendo.template($('#tempChiTietVatTu').html()));

            //CreateModalWithSize("mdlChiTietVatTu", "80%", "80%", "tempChiTietVatTu", "Chi Tiết Vật Tư Xuất");

            //var dataDanhSachVatTuXuat = new kendo.data.DataSource({
            //    transport: {
            //        read: {
            //            url: crudServiceBaseUrl + "/GetDanhSachVatTuXuat",
            //            dataType: "json",
            //            type: "POST",
            //            contentType: "application/json; charset=utf-8",
            //        },
            //        parameterMap: function (options, type) {
            //            console.log(options);
            //            return JSON.stringify(options);
            //        }
            //    },
            //    type: "json",
            //    pageSize: 25,
            //    schema: {
            //        data: "data",
            //        total: "total",
            //        model: {
            //            id: "VatTuID",
            //            feilds: {
            //                SoLuong: { type: "number" },
            //                DonGia: { type: "number" }
            //            }
            //        }
            //    }
            //});
            $("#gridChiTietVatTu").kendoGrid({
                //dataSource: dataChiTietVatTu,
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                editable: true,
                pageable: {
                    refresh: true,
                    buttonCount: 5,
                    pageSizes: true,
                    messages: messagegrid
                },
                height: window.innerHeight * 0.6,
                width: "100%",
                dataBound: onDataBound,
                //toolbar: kendo.template($("#btnThem").html()),
                columns: [
                    {
                        field: "select",
                        title: "&nbsp;",
                        template: "<input type='checkbox' class='checkbox' />",
                        sortable: false,
                        filterable: false,
                        width: 32
                    },
                    {
                        title: GetTextLanguage("stt"),
                        template: "<span class='stt'></span>",
                        width: 60,
                        align: "center"
                    },
                    {
                        field: "TenNhom", title: GetTextLanguage("nhom"), hidden: true
                    },
                    {
                        field: "TenVatTu", title: GetTextLanguage("vattu"), attributes: { style: "text-align:left;" }, filterable: FilterInTextColumn
                    },
                    {
                        field: "TenDonVi", title: GetTextLanguage("donvi"), width: 100, filterable: FilterInTextColumn
                    },
                    {
                        field: "SoLuongTon", width: 100, title: GetTextLanguage("soluongton"), format: "{0:n2}", attributes: { style: "text-align:right;" }, filterable: FilterInColumn
                    },
                    {
                        field: "DonGiaNhap", width: 150, title: GetTextLanguage("dongianhap"), attributes: { "style": "text-align:right !important;" }, format: "{0:n2}", filterable: FilterInColumn
                    },
                    {
                        field: "DonGia", width: 150, title: GetTextLanguage("giaxuat"), attributes: { "style": "text-align:right !important;" }, format: "{0:n2}", filterable: FilterInColumn
                    },
                    {
                        field: "SoLuong", width: 100, title: GetTextLanguage("soluong"), format: "{0:n2}", filterable: FilterInColumn, attributes: { "class": "soluongcheck" }
                    },

                    //{ command: "destroy", title: "&nbsp;", width: 150 }
                ]
            }).data("kendoGrid");

            gridChiTietVatTu.table.on("click", ".checkbox", selectRow);
            gridChiTietVatTu.table.on("click", ".soluongcheck", selectRowSoLuong);

            function selectRow() {
                console.log(this.checked);
                var checked = this.checked,
                    rows = $(this).closest("tr"),
                    grids = $("#gridChiTietVatTu").data("kendoGrid"),
                    dataItems = grids.dataItem(rows);
                checkedIds[dataItems.uid] = checked;
                if (checked) {
                    //-select the row
                    rows.addClass("k-state-selected");
                } else {
                    //-remove selection
                    rows.removeClass("k-state-selected");
                }
            }
            function selectRowSoLuong() {
                var rows = $(this).closest("tr"),
                    grids = $("#gridChiTietVatTu").data("kendoGrid"),
                    dataItems = grids.dataItem(rows);
                checkedIds[dataItems.uid] = true;
                rows.addClass("k-state-selected").find(".checkbox").attr("checked", "checked");

            }
            function onDataBound(e) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {
                    if (checkedIds[view[i].uid]) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected")
                            .find(".checkbox")
                            .attr("checked", "checked");
                    }
                }

                var rows = this.items();
                dem = 0;
                $(rows).each(function () {
                    dem++;
                    var rowLabel = $(this).find(".stt");
                    $(rowLabel).html(dem);
                    if (this.nextElementSibling != null && this.nextElementSibling.className == "k-grouping-row") {
                        dem = 0;
                    }
                });
            }

            //Lưu Phiếu
            $("#saveas").click(function () {
                var x = confirm(GetTextLanguage("luuhaykhong"));
                if (x) {
                    if (dataobject.items.length == 0) {
                        showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("khongcovattunaotrenphieu"));
                    }
                    else {
                        if ($("#loaihinhs").val() == "") {
                            showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("banphaichonloaihinhxuat"));
                        }
                        else {
                            var booladd = true;
                            if ($("#loaihinhs").val() == 0 && ($("#congtrinhs").val() == "" || $("#doithicongs").val() == "" || $("#nguoinhans").val() == "")) {
                                showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("banphaichoncongtringvadoinhanvattunguoinhan"));
                                booladd = false;
                            }

                            //Check lại số lượng tồn 1 lần nữa
                            for (var i = 0; i < dataobject.items.length; i++) {
                                if (dataobject.items[i].SoLuongTon < dataobject.items[i].SoLuong) {
                                    showToast("error", GetTextLanguage("canhbao"), dataobject.items[i].TenVatTu + GetTextLanguage("odongthu") + (i + 1) + GetTextLanguage("chicon") + (" + dataobject.items[i].SoLuongTon + ") + GetTextLanguage("khongdusoluongtondexuat"));
                                    booladd = false;
                                    break;
                                }
                            }

                            dataobject.ThoiGian = $("#ThoiGianNhap").val();
                            dataobject.DoiThiCongNhanID = $("#doithicongs").val();
                            dataobject.CongTrinhNhanID = $("#congtrinhs").val();
                            dataobject.NguoiNhanID = $("#nguoinhans").val();
                            dataobject.LoaiHinh = $("#loaihinhs").val();
                            if ($("#nhapmois").is(':checked'))
                                dataobject.creatNhap = 1; // checked
                            else
                                dataobject.creatNhap = 0;

                            if (booladd == true) {
                                $(".windows8").css("display", "block");
                                var dataimg = new FormData();
                                var ins = storedFiles.length;
                                if (ins > 0) {
                                    for (var x = 0; x < ins; x++) {
                                        dataimg.append("uploads", storedFiles[x]);
                                    }

                                }

                                var objXhr = new XMLHttpRequest();
                                objXhr.onreadystatechange = function () {
                                    if (objXhr.readyState == 4) {
                                        if (objXhr.responseText != "") {
                                            var strUrl = objXhr.responseText.split(',');
                                            for (var k = 0; k < strUrl.length; k++) {
                                                dataobject.Anh.push(strUrl[k]);
                                            }
                                        }

                                        setTimeout(function () {
                                            $.ajax({
                                                cache: false,
                                                async: false,
                                                type: "POST",
                                                url: crudServiceBaseUrl + "/AddorUpdatePhieuXuat",
                                                data: { data: JSON.stringify(dataobject), PhieuID: PhieuID },
                                                success: function (data) {
                                                    altReturn(data);
                                                    if (data.code == "success") {
                                                        gridReload("gridNhapXuatKho", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Xuat });
                                                        closeModel("mdlAddPhieuXuat");
                                                        //remove file anh
                                                        storedFiles = [];
                                                    }
                                                    $(".windows8").css("display", "none");
                                                },
                                                error: function (xhr, status, error) {
                                                    showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                                    $(".windows8").css("display", "none");
                                                }
                                            });
                                        }, 1000);
                                    }
                                }

                                objXhr.open("POST", crudServiceBaseUrlUploadFile + "/Uploadfiles", true);
                                objXhr.send(dataimg);

                            }
                        }
                    }
                } else {
                    return false;
                }
            });
            //Xóa Phiếu
            $("#btnXoa").click(function () {
                var x = confirm(GetTextLanguage("bancomuonxoaphieunaykhong"));
                if (x) {
                    $(".windows8").css("display", "block");
                    setTimeout(function () {
                        $.ajax({
                            cache: false,
                            async: false,
                            type: "POST",
                            url: crudServiceBaseUrl + "/XoaPhieuXuat",
                            data: { PhieuXuatVatTuID: PhieuID, CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID") },
                            success: function (data) {
                                altReturn(data);
                                if (data.code == "success") {
                                    gridReload("gridNhapXuatKho", { CongTrinhID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("CongTrinhID"), DoiThiCongID: isTong == true ? "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF" : dataItem.get("DoiThiCongID"), LoaiHinh: LoaiHinhNhapXuat.Nhap });
                                    closeModel("mdlAddPhieuXuat");
                                }
                                $(".windows8").css("display", "none");
                            },
                            error: function (xhr, status, error) {
                                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                                $(".windows8").css("display", "none");
                            }
                        });
                    }, 500);
                } else {
                    return false;
                }
            });

            //$("#btn-add-chitiet-vt").on("click", function () {
            //    var grid = $("#gridChiTietVatTu").data("kendoGrid");
            //    var selectedRows = $(".k-state-selected", "#gridChiTietVatTu");
            //    if (selectedRows.length > 0) {
            //        for (var i = 0; i < selectedRows.length - 1; i++) {
            //            var selectedItem = grid.dataItem(selectedRows[i]);
            //            var slton = selectedItem.SoLuongTon == null ? 0 : selectedItem.SoLuongTon;
            //            var sl = selectedItem.SoLuong == null ? 0 : selectedItem.SoLuong;
            //            if (slton < sl) {
            //                showToast("warning", GetTextLanguage("canhbao"), GetTextLanguage("soluongtontaidongthu") + " " + (i + 1) + " " + GetTextLanguage("khongdudexuat"));
            //                return;
            //            }
            //        }
            //    }
            //    if (selectedRows.length > 0) {
            //        for (var i = 0; i < selectedRows.length - 1; i++) {
            //            var selectedItem = grid.dataItem(selectedRows[i]);
            //            var dg = selectedItem.DonGia == null ? 0 : selectedItem.DonGia;
            //            var dgn = selectedItem.DonGiaNhap == null ? 0 : selectedItem.DonGiaNhap;
            //            var sl = selectedItem.SoLuong == null ? 0 : selectedItem.SoLuong;
            //            console.log(selectedItem);
            //            var bool = true;
            //            for (var k = 0; k < dataobject.items.length; k++) {
            //                if (dataobject.items[k].VatTuID === selectedItem.VatTuID && dataobject.items[k].DonGiaNhap === dgn) {
            //                    dataobject.items[k].SoLuong = (parseFloat(dataobject.items[k].SoLuong) + parseFloat(sl));
            //                    dataobject.items[k].ThanhTien = (parseFloat(dataobject.items[k].SoLuong) * parseFloat(dataobject.items[k].DonGia));
            //                    bool = false;
            //                    break;
            //                }
            //            }
            //            if (bool) {
            //                icountrows++;
            //                dataobject.items.push({
            //                    ID: icountrows,
            //                    VatTuID: selectedItem.VatTuID,
            //                    TenVatTu: selectedItem.TenVatTu,
            //                    TenVietTat: selectedItem.TenVietTat,
            //                    TenDonVi: selectedItem.TenDonVi,
            //                    DonGia: dg,
            //                    DonGiaNhap: dgn,
            //                    SoLuong: sl,
            //                    ThanhTien: (parseFloat(dg) * parseFloat(sl)),
            //                });
            //            }
            //        }
            //        gridReload("gridChiTietVatTu", {});
            //        closeModel("mdlChiTietVatTu");
            //    }
            //});
        });

    };
});