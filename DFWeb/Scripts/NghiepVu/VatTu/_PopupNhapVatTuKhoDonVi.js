﻿$(document).ready(function () {
    var currentController = "/QuanLyVatTuKhoDonVi";
    showslideimg();
    var dataSourceXacNhanNhapVatTuKhoDonVi = new kendo.data.DataSource(
        {
            transport: {
                read: {
                    url: "/QuanLyVatTuKhoDonVi/GetDataXacNhanNhapVatTuKhoDonVi",
                    dataType: 'json',
                    type: 'GET',
                    contentType: "application/json; charset=utf-8",
                    data: {
                        CongTrinhID: $("#drdCongTrinh").data("kendoDropDownList").value(),
                        DoiThiCongID: $("#drdDoiThiCong").data("kendoDropDownList").value(),
                    }
                }
            },
            batch: true,
            pageSize: 20,
            sort: [{ field: "ThoiGian", dir: "desc" }],
            schema: {
                data: 'data',
                total: 'total',
                model: {
                    id: "PhieuXuatVatTuID",
                    fields: {
                        PhieuXuatVatTuID: { type: "string" },
                        ThoiGian: { type: "date" },
                        TenCongTrinhDi: { type: "string" },
                        TenDoiThiCongDi: { type: "string" }
                    },
                }
            },
        });
    $("#gridXacNhanNhapVatTuKhoDonVi").kendoGrid({
        dataSource: dataSourceXacNhanNhapVatTuKhoDonVi,
        height: $("#mdlNhapVatTuKhoDonVi").height()-20,
        width: "100%",
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        dataBinding: function () {
            record = 0;
        },
        pageable: pageableAll,
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                template: "#= ++record #",
                width: 60,
                attributes: alignCenter
            },
            {
                field: "ThoiGian",
                title: GetTextLanguage("thoigian"),
                filterable: FilterInTextColumn,
                attributes: alignCenter,
                width: 250,
                template: formatToDateTime("ThoiGian")
            },

            {
                field: "MaPhieu",
                title: GetTextLanguage("maphieu"),
                filterable: FilterInTextColumn,
                attributes: alignLeft,
                width: 200,
            },
            {
                title: GetTextLanguage("khodi"),
                columns: [
                    {
                        field: "TenCongTrinhDi",
                        title: GetTextLanguage("congtrinhdi"),
                        filterable: FilterInColumn
                    },
                    {
                        field: "TenDoiThiCongDi",
                        title: GetTextLanguage("doidi"),
                        filterable: FilterInColumn
                    }
                ]
            },
            {
                field: "TongGiaTri",
                title: GetTextLanguage("tongtien"),
                filterable: FilterInTextColumn,
                attributes: alignRight,
                width: 200,
                format: "{0:n2}"
            },
        ],
    });
    $("#gridXacNhanNhapVatTuKhoDonVi .k-grid-content").on("dblclick", "td", function () {
        CreateModalWithSize("mdlThongTinPhieuNhapVatTuKhoDonVi", "60%", "80%", null, GetTextLanguage("thongtinphieu"));
        var rowData = $("#gridXacNhanNhapVatTuKhoDonVi").data("kendoGrid").dataItem($(this).closest("tr"));
        $("#mdlThongTinPhieuNhapVatTuKhoDonVi").load("/QuanLyVatTuKhoDonVi/ThongTinPhieuNhapVatTuKhoDonVi?PhieuXuatVatTuID=" + rowData.PhieuXuatVatTuID);
    });
});