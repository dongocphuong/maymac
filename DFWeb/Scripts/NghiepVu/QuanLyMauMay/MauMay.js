﻿$(document).ready(function () {
    //Init
    var dataimg = new FormData();
    var crudServiceBaseUrl = "/QuanLyMauMay";
    var dataTrangThai = [
       { text: "Chưa bắt đầu", value: 1 },
       { text: "Chưa báo giá", value: 7 },
       { text: "Đã duyệt", value: 5 },
       { text: "Đang sản xuất", value: 2 },
       { text: "Hoàn thành SX", value: 3 },
       { text: "Đã giao hàng", value: 4 },
       { text: "Đã thanh toán xong", value: 6 },
       { text: "Đã hủy", value: 8 },
    ];
    var dataTinhTrang = [
       { text: "Bình thường", value: 1 },
       { text: "Gấp", value: 2 },
    ];
    var dataLoaiHinh = [
      { text: "May", value: 1 },
      { text: "Dệt", value: 2 },
    ];
    var datagrid = new kendo.data.DataSource({
        transport: {
            read: {
                url: crudServiceBaseUrl + "/GetAllDataMauMay",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: {}
            },
            parameterMap: function (options, type) {
                return JSON.stringify(options);
            }
        },
        type: "json",
        batch: true,
        pageSize: 50,
        schema: {
            type: 'json',
            data: 'data',
            total: "total",
            model: {
                id: "MauMayID",
                fields: {
                    MauMayID: { editable: false, nullable: true },
                }
            }
        },
    });
    //Build grid
    var grid = $("#grid").kendoGrid({
        dataSource: datagrid,
        filterable: {
            mode: "row"
        },
        sortable: true,
        resizable: true,
        pageable: {
            refresh: true,
            buttonCount: 5,
            pageSizes: true,
            messages: messagegrid
        },
        dataBinding: function () {
            record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
        },
        height: window.innerHeight * 0.8,
        width: "100%",
        toolbar: [{ template: kendo.template($("#btnThem").html()) }],
        columns: [
            {
                title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                width: 60,
                attributes: alignCenter,
                template: "#= ++record #",
            },
            {
                field: "ThoiGian",
                title: "Thời gian",
                width: 100,
                attributes: alignCenter,
                filterable: false,
                template: formatToDate("ThoiGian"),
            },
             {
                 field: "LoaiHinhSanXuat",
                 title: "Loại hinh",
                 filterable: FilterInTextColumn,
                 filterable: {
                     cell: {
                         template: function (args) {
                             args.element.kendoDropDownList({
                                 dataSource: dataLoaiHinh,
                                 dataTextField: "text",
                                 dataValueField: "value",
                                 valuePrimitive: true
                             });
                         },
                         showOperators: false
                     }
                 },
                 width: 150,
                 template: kendo.template($("#tplLoaiHinh").html())
             },
            {
                field: "MaMauMay",
                title: "Mã thiết kế",
                width: 100,
                attributes: alignCenter,
                filterable: FilterInTextColumn,
            },
            {
                field: "TenMauMay",
                title: "Tên thiết kế",
                filterable: FilterInTextColumn,
                attributes: { style: "text-align:left;" },
                width: 300
            },
        ],
    });
    //btnChiTiet
    $("#grid").on("dblclick", "td", function () {
        var rows = $(this).closest("tr"),
            grids = $("#grid").data("kendoGrid"),
            dataItems = grids.dataItem(rows);
        CreateModalWithSize("mdlThemMoiMauMay", "100%", "100%", "tplThemMoiMauMay", "Chi tiết thiết kế");
        $("#LoaiHinhSanXuat").kendoDropDownList({

            dataTextField: 'text',
            dataValueField: 'value',
            filter: "contains",
            optionLabel: "Chọn loại hình",
            dataSource: dataLoaiHinh
        });
        $.ajax({
            url: crudServiceBaseUrl + "/GetAllDatabyMauMayID",
            method: "GET",
            data: { MauMayID: dataItems.MauMayID },
            success: function (data) {
                dataobjectload = data.data[0];

                var viewModel = kendo.observable({
                    MauMayID: dataobjectload == null ? "00000000-0000-0000-0000-000000000000" : dataobjectload.MauMayID,
                    MaMauMay: dataobjectload == null ? "" : dataobjectload.MaMauMay,
                    TenMauMay: dataobjectload == null ? "" : dataobjectload.TenMauMay,
                    DanhSachHinhAnhs: dataobjectload == null || dataobjectload == undefined ? "" : dataobjectload.DanhSachHinhAnhs,
                    YeuCauKhachHangID: dataobjectload == null ? "" : dataobjectload.YeuCauKhachHang,
                    DoiTacID: dataobjectload == null ? "" : dataobjectload.TenDoiTac,
                    NoiDung: dataobjectload == null ? "" : dataobjectload.NoiDung,
                    LoaiHinhSanXuat: dataobjectload == null ? "" : dataobjectload.LoaiHinhSanXuat
                });
                kendo.bind($("#frmEdit"), viewModel);
                var htmls = "";
                if (dataobjectload.HinhAnh != null && dataobjectload.HinhAnh != "") {
                    for (var i = 0; i < dataobjectload.HinhAnh.split(',').length ; i++) {
                        htmls += "<div class='img-responsive media-preview imgslider' style='float: left; margin-right: 10px;'>";
                        htmls += "<a rel='gallery' href='" + (localQLVanBan + dataobjectload.HinhAnh.split(',')[i]) + "'>";
                        htmls += '<img src="' + (localQLVanBan + dataobjectload.HinhAnh.split(',')[i]) + '" class="img-responsive img-rounded media-preview" style="width: 70px;height: 70px;" /></a></div>';
                    }
                }
                $('#lstImageDisplay2 #tbody2').html(htmls);
                showToast("success", GetTextLanguage("thanhcong"), GetTextLanguage("thanhcong"));
                //
                var lt = dataobjectload.DanhSachHinhAnhs;
                if (lt == "" || lt == null) {
                    var lst = lt;
                }
                else {
                    var lst = lt.split(',');
                    if (lst.length > 0 && lst[0] != "") {
                        for (var i = 0; i < lst.length; i++) {
                            lstHinhAnh.push({ img: lst[i], name: lst[i] });
                        }
                        var html = "";
                        for (var i = 0; i < lstHinhAnh.length; i++) {
                            if (lstHinhAnh[i].img.match(/\.(jpg|jpeg|png|gif)$/)) {
                                html += "<div class='imgslider' style='float:left; position:relative; width:70px;height:70px; margin:5px;'>";
                                html += "<div style='margin:0px;' class='img-con ahihi3 img-thumbnail'><a rel='gallery' href='" + (localQLVT + lstHinhAnh[i].img) + "'>";
                                html += "<img src='" + localQLVT + lstHinhAnh[i].img + "'  data-file='" + (lstHinhAnh[i].img) + "' style='width: 70px;height:70px;margin: 0;opacity: 1;'></a></div>";
                                html += "</div>";
                            }
                        }
                        $('.dshinhanhadd').append(html);
                        showslideimg5($('#frmEdit'));
                    }
                }
                //
                var lt = dataobjectload.DanhSachHinhAnhs;
                if (lt == "" || lt == null) {
                    var lst = lt;
                } else {
                    var lst = lt.split(',');
                    if (lst.length > 0 && lst[0] != "") {
                        for (var i = 0; i < lst.length; i++) {
                            lstHinhAnh.push({ img: lst[i], name: lst[i] });
                        }
                        var htmls = "";
                        if (lst != null && lst != "") {
                            for (var i = 0; i < lst.length ; i++) {
                                htmls += "<div class='img-responsive media-preview imgslider' style='float: left; margin-right: 10px;'>";
                                htmls += "<a rel='gallery' href='" + (localQLVanBan + lst[i]) + "'>";
                                htmls += '<img src="' + (localQLVanBan + lst[i]) + '" class="img-responsive img-rounded media-preview" style="width: 70px;height: 70px;" /></a></div>';
                            }
                        }
                        $('#lstImageDisplay #tbody').html(htmls);
                        showslideimg5("#lstImageDisplay");
                    }
                }
            },
            error: function (xhr, status, error) {
                showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
            }
        });
        gridChiTietMauMay(dataItems.MauMayID, dataItems.YeuCauKhachHangID);
        $("#btnXoa").removeClass("hidden");
        $("#btnXoa").click(function () {
            var x = confirm("Bạn có chắc chắn muốn xóa thiết kế này không?");
            if (x) {
                ContentWatingOP("mdlThemMoiMauMay", 0.9);
                $.ajax({
                    url: crudServiceBaseUrl + "/XoaMauMay",
                    method: "POST",
                    data: { MauMayID: dataItems.MauMayID },
                    success: function (data) {
                        $("#mdlThemMoiMauMay").unblock();
                        $(".windows8").css("display", "none");
                        closeModel("mdlThemMoiMauMay");
                        gridReload("grid", {});
                    },
                    error: function (xhr, status, error) {
                        showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        $("#mdlThemMoiMauMay").unblock();
                        $(".windows8").css("display", "none");
                        gridReload("grid", {});
                    }
                });
            }
        });
    });
    $("#grid").on("click", "#btnAdd", function () {
        CreateModalWithSize("mdlThemMoiMauMay", "100%", "95%", "tplThemMoiMauMay", "Thêm mới thiết kế");
        //btnThemmoi
        $("#LoaiHinhSanXuat").kendoDropDownList({

            dataTextField: 'text',
            dataValueField: 'value',
            filter: "contains",
            optionLabel: "Chọn loại hình",
            dataSource: dataLoaiHinh
        });
        gridChiTietMauMay();
    })
    function gridChiTietMauMay(MauMayID, YeuCauKhachHangID) {
        lstHinhAnh = [];
        var datagridThemMoiMauMay = new kendo.data.DataSource({
            transport: {
                read: {
                    url: crudServiceBaseUrl + "/GetAllDataChiTietMauMay",
                    dataType: 'json',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: { ChiTietMauMayID: MauMayID }
                },
                parameterMap: function (options, type) {
                    return JSON.stringify(options);
                }
            },
            batch: true,
            pageSize: 50,
            schema: {
                type: 'json',
                data: 'data',
                total: "total",
                model: {
                    id: "MauMayID",
                    fields: {
                        MauMayID: { editable: false, nullable: true },
                        NoiDungChiTiet: { editable: false, nullable: true },
                        TenNguoiThucHien: { editable: false, nullable: true },
                        NguoiThucHien: { editable: false, nullable: true },
                        TenNguoiPhuTrach: { editable: false, nullable: true },
                        NguoiPhuTrach: { editable: false, nullable: true },
                        YeuCauPAThucHien: { editable: false, nullable: true },
                        GhiChu: { editable: false, nullable: true },
                        ThoiGianThucHien: { editable: false, nullable: true },
                    }
                }
            },
        });
        var gridChiTietMauMay = $("#gridChiTietMauMay").kendoGrid({
            dataSource: datagridThemMoiMauMay,
            filterable: {
                mode: "row"
            },
            sortable: true,
            resizable: true,
            messages: {
                commands: {
                    update: GetTextLanguage("capnhat"),
                    canceledit: GetTextLanguage("huy")
                }
            },
            editable: true,
            pageable: {
                refresh: true,
                buttonCount: 5,
                pageSizes: true,
                messages: messagegrid
            },
            height: window.innerHeight * 0.8,
            width: "100%",
            dataBinding: function () {
                record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
            },
            toolbar: [{ template: kendo.template($("#btnThemDong").html()) }],
            columns: [
                {
                    title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                    width: 60,
                    attributes: alignCenter,
                    template: "#= ++record #",
                },
                {
                    field: "NoiDungChiTiet",
                    title: "Nội dung",
                    width: 180,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn,
                },
                {
                    field: "TenNguoiThucHien",
                    title: "Người thực hiện",
                    width: 80,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn,
                },
                {
                    field: "NguoiThucHien",
                    width: 80,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn,
                    hidden: true
                },
                {
                    field: "TenNguoiPhuTrach",
                    title: "Người phụ trách",
                    filterable: FilterInTextColumn,
                    attributes: alignCenter,
                    width: 80
                },
                {
                    field: "NguoiPhuTrach",
                    title: "Người phụ trách",
                    filterable: FilterInTextColumn,
                    attributes: alignCenter,
                    width: 80,
                    hidden: true
                },
                {
                    field: "YeuCauPAThucHien",
                    title: "Yêu cầu hoặc phương án thực hiện",
                    width: 230,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn
                },
                {
                    field: "ThoiGianThucHien",
                    title: "Thời hạn làm việc (giờ)",
                    width: 80,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn
                },
                {
                    field: "GhiChu",
                    title: "Ghi chú",
                    width: 100,
                    attributes: alignCenter,
                    filterable: FilterInTextColumn
                },
                  {
                      title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("thaotac") + "</a>",
                      command: [{ name: "destroy", text: GetTextLanguage("xoa") }], width: 100
                  },
            ],
        });
        $("#btnChonYeuCauKH").click(function () {
            CreateModalWithSize("mdlChonYeuCauKhachHang", "80%", "80%", "tplChonYeuCauKhachHang", "Chọn yêu cầu khách hàng");
            var datagridChonYCKH = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: crudServiceBaseUrl + "/GetDataYeuCauKhachHang",
                        dataType: "json",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        data: {}
                    },
                    parameterMap: function (options, type) {
                        return JSON.stringify(options);
                    }
                },
                type: "json",
                batch: true,
                pageSize: 50,
                sort: [{ field: "ThoiGian", dir: "desc" }, { field: "TinhTrang", dir: "asc" }],
                schema: {
                    type: 'json',
                    data: 'data',
                    total: "total",
                    model: {
                        id: "YeuCauKhachHangID",
                        fields: {
                            YeuCauKhachHangID: { editable: false, nullable: true },
                        }
                    }
                },
            });
            //Build grid
            var grids = $("#gridChonYeuCauKhachHang").kendoGrid({
                dataSource: datagridChonYCKH,
                filterable: {
                    mode: "row"
                },
                sortable: true,
                resizable: true,
                pageable: {
                    refresh: true,
                    buttonCount: 5,
                    pageSizes: true,
                    messages: messagegrid
                },
                dataBinding: function () {
                    record = (this.dataSource.page() - 1) * this.dataSource.pageSize();
                },
                height: window.innerHeight * 0.8,
                width: "100%",
                columns: [
                    {
                        title: "<a href='javascript:void(0)' class='k-link'>" + GetTextLanguage("stt") + "</a>",
                        width: 60,
                        attributes: alignCenter,
                        template: "#= ++record #",
                    },
                    {
                        field: "ThoiGian",
                        title: "Thời Gian",
                        filterable: FilterInTextColumn,
                        attributes: alignCenter,
                        width: 100,
                        template: formatToDateTime("ThoiGian"),
                    },
                    {
                        field: "TrangThai",
                        title: "Trạng thái",
                        filterable: FilterInTextColumn,
                        filterable: {
                            cell: {
                                template: function (args) {
                                    args.element.kendoDropDownList({
                                        dataSource: dataTrangThai,
                                        dataTextField: "text",
                                        dataValueField: "value",
                                        valuePrimitive: true
                                    });
                                },
                                showOperators: false
                            }
                        },
                        width: 150,
                        template: kendo.template($("#tplTrangThai").html())
                    },
                    {
                        field: "TinhTrang",
                        title: "Loại yêu cầu",
                        filterable: FilterInTextColumn,
                        filterable: {
                            cell: {
                                template: function (args) {
                                    args.element.kendoDropDownList({
                                        dataSource: dataTinhTrang,
                                        dataTextField: "text",
                                        dataValueField: "value",
                                        valuePrimitive: true
                                    });
                                },
                                showOperators: false
                            }
                        },
                        width: 150,
                        template: kendo.template($("#tplTinhTrang").html())
                    },
                     {
                         field: "LoaiHinhSanXuat",
                         title: "Loại hinh",
                         filterable: FilterInTextColumn,
                         filterable: {
                             cell: {
                                 template: function (args) {
                                     args.element.kendoDropDownList({
                                         dataSource: dataLoaiHinh,
                                         dataTextField: "text",
                                         dataValueField: "value",
                                         valuePrimitive: true
                                     });
                                 },
                                 showOperators: false
                             }
                         },
                         width: 150,
                         template: kendo.template($("#tplLoaiHinh").html())
                     },
                    {
                        field: "MaDonHang",
                        title: "Mã Đơn Hàng",
                        width: 100,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn,
                    },
                    {
                        field: "TenDoiTac",
                        title: "Tên Đối Tác",
                        width: 200,
                        attributes: alignCenter,
                        filterable: FilterInTextColumn
                    },
                    {
                        field: "NoiDung",
                        title: "Nội Dung",
                        width: 400,
                        attributes: { style: "text-align:left;" },
                        filterable: FilterInTextColumn,
                    },
                ],
            });
            $("#gridChonYeuCauKhachHang").on("dblclick", "td", function () {
                var rowsct = $(this).closest("tr"),
                    gridsct = $("#gridChonYeuCauKhachHang").data("kendoGrid"),
                    dataItemsct = gridsct.dataItem(rowsct);
                //
                $("#DoiTacID").val(dataItemsct.TenDoiTac);
                $("#NoiDung").val(dataItemsct.NoiDung);
                $("#YeuCauKhachHang").val(dataItemsct.YeuCauKhachHangID);
                var htmls = "";
                if (dataItemsct.HinhAnh != null && dataItemsct.HinhAnh != "") {
                    for (var i = 0; i < dataItemsct.HinhAnh.split(',').length ; i++) {
                        htmls += "<div class='img-responsive media-preview imgslider' style='float: left; margin-right: 10px;'>";
                        htmls += "<a rel='gallery' href='" + (localQLVanBan + dataItemsct.HinhAnh.split(',')[i]) + "'>";
                        htmls += '<img src="' + (localQLVanBan + dataItemsct.HinhAnh.split(',')[i]) + '" class="img-responsive img-rounded media-preview" style="width: 70px;height: 70px;" /></a></div>';
                    }
                }
                $('#lstImageDisplay2 #tbody2').html(htmls);
                showslideimg();
                closeModel("mdlChonYeuCauKhachHang");
            });
        })
        $("#frmEdit #files_0").change(function () {
            for (var x = 0; x < this.files.length; x++) {
                dataimg.append("uploads", this.files[x]);
            };
            loadimgSuaChua5(this, $("#frmEdit"));
            $("#frmEdit #files_0").val('').clone(true);
        });
        $("#gridChiTietMauMay").on("click", "#btnThemDong", function () {
            CreateModalWithSize("mdlThemDongChiTietMauMay", "40%", "80%", "tplThemDongChiTietMauMay", "Thêm mới");
            InitChiTietMauMay();
            $("#btnLuuChiTiet").click(function () {
                var validator = $("#frmThemMoi").kendoValidator().data("kendoValidator");
                if (validator.validate()) {
                    var arrData = $("#gridChiTietMauMay").data("kendoGrid").dataSource.data();
                    var dataItem = {
                        NoiDungChiTiet: $("#NoiDungChiTiet").val(),
                        TenNguoiThucHien: $("#NguoiThucHien").data("kendoDropDownList").text(),
                        NguoiThucHien: $("#NguoiThucHien").val(),
                        TenNguoiPhuTrach: $("#NguoiPhuTrach").data("kendoDropDownList").text(),
                        NguoiPhuTrach: $("#NguoiPhuTrach").val(),
                        ThoiGianThucHien: $("#ThoiGianThucHien").val() == null || $("#ThoiGianThucHien").val() == "" ? 0 : $("#ThoiGianThucHien").val(),
                        GhiChu: $("#GhiChu").val(),
                        YeuCauPAThucHien: $("#YeuCauPAThucHien").val()
                    };
                    arrData.push(dataItem);
                    $("#gridChiTietMauMay").data("kendoGrid").dataSource.data(arrData);
                    $("#mdlThemDongChiTietMauMay").data("kendoWindow").close();
                }
            });
        });
        $("#btnLuuMauMay").click(function () {
            var hinhanh = "";
            var validator = $("#frmEdit").kendoValidator().data("kendoValidator");
            if (validator.validate()) {
                var items = $("#gridChiTietMauMay").data("kendoGrid").dataSource.data();
                //image
                var hinhanh = "";
                var ins = dataimg.getAll("uploads").length;
                var lstHinhAnh = [];
                $('.dshinhanhadd .imgslider').each(function (index2, element2) {
                    if ($(element2).find('img').attr('src').indexOf("data:image") < 0) {
                        lstHinhAnh.push({ Url: $(element2).find('img').data("file") });
                    }
                });
                if (ins > 0) {
                    var strUrl = uploadMultipleFileParagamWidthFile("/upload" + "/Uploadfiles", dataimg.getAll("uploads"), $(".dshinhanhadd", $('#frmEdit'))).split(',');
                    for (var k = 0; k < strUrl.length; k++) {
                        lstHinhAnh.push({ Url: strUrl[k] });
                    }
                }
                for (var i = 0; i < lstHinhAnh.length; i++) {
                    if (hinhanh.indexOf(lstHinhAnh[i].Url) < 0) {
                        hinhanh += lstHinhAnh[i].Url + ",";
                    }
                }
                //
                var objData = {
                    MauMayID: $("#MauMayID").val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#MauMayID").val(),
                    YeuCauKhachHangID: $("YeuCauKhachHang").val() == "" ? "00000000-0000-0000-0000-000000000000" : $("#YeuCauKhachHang").val(),
                    MaMauMay: $("#MaMauMay").val(),
                    TenMauMay: $("#TenMauMay").val(),
                    LoaiHinhSanXuat: $("#LoaiHinhSanXuat").val(),
                    DanhSachHinhAnhs: hinhanh.substring(0, hinhanh.length - 1)
                }
                //
                ContentWatingOP("mdlThemDongChiTietMauMay", 0.9);
                ContentWatingOP("mdlThemMoiMauMay", 0.9);
                setTimeout(function () {
                    $.ajax({
                        cache: false,
                        async: false,
                        type: "POST",
                        url: crudServiceBaseUrl + "/AddOrUpdate",
                        data: { data: JSON.stringify(objData), chitiet: JSON.stringify(items) },
                        success: function (data) {
                            altReturn(data);
                            $("#mdlThemDongChiTietMauMay").unblock();
                            $("#mdlThemMoiMauMay").unblock();
                            closeModel("mdlThemMoiMauMay");
                            gridReload("grid", {});
                        },
                        error: function (xhr, status, error) {
                            $("#mdlThemDongChiTietMauMay").unblock();
                            $("#mdlThemMoiMauMay").unblock();
                            showToast("error", GetTextLanguage("thatbai"), GetTextLanguage("mgs_coloitrongquatrinhxulydulieu"));
                        }
                    });
                }, 1000);

            }
        });
        $("#gridChiTietMauMay").on("dblclick", "td", function () {
            var rowsct = $(this).closest("tr"),
                gridsct = $("#gridChiTietMauMay").data("kendoGrid"),
                dataItemsct = gridsct.dataItem(rowsct);
            CreateModalWithSize("mdlThemDongChiTietMauMay", "40%", "80%", "tplThemDongChiTietMauMay", "Chi tiết");
            console.log(dataItemsct);
            $("#btnXoaChiTiet").removeClass("hidden");
            var dataobjectload = dataItemsct;
            var viewModel = kendo.observable({
                ChiTietMauMayID: dataobjectload == null ? "00000000-0000-0000-0000-000000000000" : dataobjectload.ChiTietMauMayID,
                NoiDungChiTiet: dataobjectload == null ? "" : dataobjectload.NoiDungChiTiet,
                ToSanXuat: dataobjectload == null ? "" : dataobjectload.PhongBanID,
                NguoiThucHien: dataobjectload == null ? "" : dataobjectload.NguoiThucHien,
                NguoiPhuTrach: dataobjectload == null ? "" : dataobjectload.NguoiPhuTrach,
                ThoiGianThucHien: dataobjectload == null ? "" : dataobjectload.ThoiGianThucHien,
                GhiChu: dataobjectload == null ? "" : dataobjectload.GhiChu,
                YeuCauPAThucHien: dataobjectload == null ? "" : dataobjectload.YeuCauPAThucHien,
            });
            kendo.bind($("#frmThemMoi"), viewModel);
            InitChiTietMauMay();
            $("#btnLuuChiTiet").click(function () {
                var validator = $("#frmThemMoi").kendoValidator().data("kendoValidator");
                if (validator.validate()) {
                    var arrData = $("#gridChiTietMauMay").data("kendoGrid").dataSource.data();
                    for (var i = 0; i <= arrData.length; i++) {
                        if (arrData[i].uid == dataobjectload.uid) {
                            arrData[i].NoiDungChiTiet = $("#NoiDungChiTiet").val(),
                            arrData[i].NguoiThucHien = $("#NguoiThucHien").val(),
                            arrData[i].TenNguoiThucHien = $("#NguoiThucHien").data("kendoDropDownList").text(),
                            arrData[i].NguoiPhuTrach = $("#NguoiPhuTrach").val(),
                            arrData[i].TenNguoiPhuTrach = $("#NguoiPhuTrach").data("kendoDropDownList").text(),
                            arrData[i].ThoiGianThucHien = $("#ThoiGianThucHien").val(),
                            arrData[i].GhiChu = $("#GhiChu").val(),
                            arrData[i].YeuCauPAThucHien = $("#YeuCauPAThucHien").val()
                            $("#gridChiTietMauMay").data("kendoGrid").dataSource.data(arrData);
                            $("#mdlThemDongChiTietMauMay").data("kendoWindow").close();
                            console.log(arrData);
                        }
                    }
                }
            });
        });
    };
    function InitChiTietMauMay() {
        $("#NguoiPhuTrach").kendoDropDownList({
            autoBind: true,
            dataTextField: 'TenNhanVien',
            dataValueField: 'NhanVienID',
            filter: "contains",
            optionLabel: "Chọn người phụ trách",
            dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/getDataNhanVienPhuTrach", {}),
        });
        $("#ToSanXuat").kendoDropDownList({
            autoBind: true,
            dataTextField: 'TenPhong',
            dataValueField: 'PhongBanID',
            filter: "contains",
            optionLabel: "Chọn phòng ban",
            dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/GetDataPhongBan", {}),
            change: function () {
                $("#NguoiThucHien").data("kendoDropDownList").dataSource.read({ PhongBanID: this.value() });
                $("#NguoiThucHien").data("kendoDropDownList").refresh();
                $("#frmNguoiThucHien").removeClass("hidden");
            }
        });
        $("#NguoiThucHien").kendoDropDownList({
            autoBind: true,
            dataTextField: 'TenNhanVien',
            dataValueField: 'NhanVienID',
            filter: "contains",
            optionLabel: "Chọn người thực hiện",
            dataSource: getDataDroplitwithParam(crudServiceBaseUrl, "/getDataNhanVienThucHien", { PhongBanID: $("#ToSanXuat").val() }),
        });
    }
});
