﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using DFWeb.Models;

namespace DFWeb.Security
{
    public class CustomPrincipal : IPrincipal
    {
        private AccountInfo Account;
        public CustomPrincipal(AccountInfo acc)
        {
            this.Account = acc;
            this.Identity = new GenericIdentity(acc.Username);
        }
        public IIdentity Identity { get; set; }
        public bool IsInRole(string role)
        {
            var roles = role.Split(new char[] { ',' });
            return roles.Any(r => this.Account.Roles.Contains(r));
        }
    }
}