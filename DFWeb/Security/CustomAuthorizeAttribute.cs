﻿using DF.Utilities;
using DFWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Script.Serialization;

namespace DFWeb.Security
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            string usernameSessionvar = "user";
            string passDecrypt = "dfs@321";
            var checkRequestAjax = filterContext.HttpContext.Request.IsAjaxRequest();
            HttpCookie Cookie = HttpContext.Current.Request.Cookies[usernameSessionvar];
            if (Cookie == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Account", action = "Login" }));
            }
            else
            {
                if (string.IsNullOrEmpty(Cookie.Value))
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Account", action = "Login" }));
                }
                else
                {
                    AccountInfo f2 = new AccountInfo();
                    try
                    {
                        string DecryptedString = Utitilies.DecryptString(Cookie.Value, passDecrypt);
                        f2 = new JavaScriptSerializer().Deserialize<AccountInfo>(DecryptedString);
                        CustomPrincipal mp = new CustomPrincipal(f2);
                        if (!mp.IsInRole(Roles))
                            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "AccessDenied", action = "Index" }));
                    }
                    catch (Exception ex)
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "AccessDenied", action = "Index" }));
                    }
                    
                }
            }
            //if (CookiePersister.getAcountInfo() == null)
            //{
            //    if (checkRequestAjax)
            //    {

            //        filterContext.HttpContext.Response.StatusCode = 401;
            //        filterContext.HttpContext.Response.End();
            //    }
            //    else
            //        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Account", action = "Login" }));
            //}

            //else
            //{
            //    CustomPrincipal mp = new CustomPrincipal(CookiePersister.getAcountInfo());
            //    if (!mp.IsInRole(Roles))
            //        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "AccessDenied", action = "Index" }));
            //}
        }
    }
}