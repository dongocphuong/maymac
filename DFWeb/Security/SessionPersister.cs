﻿using DFWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Security
{
    public static class SessionPersister
    {
        static string usernameSessionvar = "user";
        public static AccountInfo User
        {
            get {
                if (HttpContext.Current == null)
                    return null;
                var sessionVar = HttpContext.Current.Session[usernameSessionvar];
                if (sessionVar != null) {
                    return sessionVar as AccountInfo;
                }
                return null;
            }
            set{
                HttpContext.Current.Session[usernameSessionvar] = value;
            }
        }
    }
}