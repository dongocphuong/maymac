﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DFWeb.Models;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using DF.Utilities;

namespace DFWeb.Security
{
    public static class CookiePersister
    {
        static string usernameSessionvar = "user";
        static string passDecrypt = "dfs@321";
        //public static AccountInfo User
        //{
        //    get
        //    {
        //        if (HttpContext.Current == null)
        //            return null;
        //        var sessionVar = HttpContext.Current.Session[usernameSessionvar];
        //        if (sessionVar != null)
        //        {
        //            return sessionVar as AccountInfo;
        //        }
        //        return null;
        //    }
        //    set
        //    {
        //        HttpContext.Current.Session[usernameSessionvar] = value;
        //    }
        //}

        public static AccountInfo getAcountInfo()
        {
            HttpCookie Cookie = HttpContext.Current.Request.Cookies[usernameSessionvar];
            if (Cookie == null)
            {
                return null;
            }
            else
            {
                if (string.IsNullOrEmpty(Cookie.Value))
                {
                    return null;
                }
                else
                {
                    AccountInfo f2 = new AccountInfo();
                    try
                    {
                        string DecryptedString = Utitilies.DecryptString(Cookie.Value, passDecrypt);
                        f2 = new JavaScriptSerializer().Deserialize<AccountInfo>(DecryptedString);
                        return f2;
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }
        }

        public static void setAccoutInfo(AccountInfo acc)
        {
            string strAccount = new JavaScriptSerializer().Serialize(acc);
            string EncryptedString = Utitilies.EncryptString(strAccount, passDecrypt);
            HttpCookie myCookie = new HttpCookie(usernameSessionvar);
            myCookie.Value = EncryptedString;
            myCookie.Expires = DateTime.Now.AddDays(7);
            HttpContext.Current.Response.Cookies.Add(myCookie);
        }

        public static void removeAccountInfo()
        {
            try
            {
                HttpCookie myCookie = HttpContext.Current.Request.Cookies[usernameSessionvar];
                myCookie.Expires = DateTime.Now.AddMinutes(-20);
                myCookie.Value = null;
                HttpContext.Current.Response.Cookies.Add(myCookie);
            }
            catch (Exception ex)
            {

            }
        }


    }
}