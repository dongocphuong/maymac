﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Models
{
    public class VatTuModels
    {
        //a.DonGiaKhoan as DonGiaKhoan, a.DonViId as DonViId, a.NhomVatTuID  as NhomVatTuID, a.TenVatTu as TenVatTu, a.TenVietTat as TenVietTat, 
        //a.VatTuID as VatTuID, b.TenNhom as TenNhom, c.TenDonVi as TenDonVi from VatTu a 
        public Nullable<Guid> VatTuID { get; set; }
        public Nullable<double> DonGiaKhoan { get; set; }
        public Guid DonViTinhId { get; set; }
        public Guid NhomVatTuID { get; set; }
        public string TenVatTu { get; set; }
        public string TenVietTat { get; set; }
        public string TenNhom { get; set; }
        public string TenDonViTinh { get; set; }
    }
    public class NhomVatTuModels
    {
        public Guid? NhomVatTuID { get; set; }
        public string TenNhom { get; set; }
    }
    public class VatTuAdd_Request
    {
        public Nullable<Guid> VatTuID { get; set; }
        public string DonGiaKhoan { get; set; }
        public Nullable<Guid> DonViTinhId { get; set; }
        public Nullable<Guid> NhomVatTuID { get; set; }
        public string TenVatTu { get; set; }
        public string TenVietTat { get; set; }
        public string TenNhom { get; set; }
        public string TenDonViTinh { get; set; }
    }
    public class DoiThiCongCongTrinhModels
    {
        public int STT { get; set; }
        public Guid DoiThiCongID { get; set; }
        public string TenDoi { get; set; }
        public string TenCongTrinh { get; set; }
        public Guid CongTrinhID { get; set; }

        public string DiaDiem { get; set; }

        public string NgayKhoiCong { get; set; }
        public string NgayKetThuc { get; set; }

        public string ChuDauTu { get; set; }

        public string GhiChu { get; set; }
    }

    public class DSPhieuNhapModels
    {
        public Guid PhieuNhapVatTuID { get; set; }
        public string TenNhanVien { get; set; }
        public DateTime ThoiGian { get; set; }
        public string TenVatTu { get; set; }
        public string TenDonVi { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public double ThanhTien { get; set; }
        public string TrangThaiDuyet { get; set; }
        public string GhiChu { get; set; }
        public string ThoiGianMT { get; set; }
    }

    public class DSChiTietPhieuNhapModels
    {
        public Guid ChiTietNhapXuatVatTuID { get; set; }
        public Nullable<double> DonGia { get; set; }
        public string TenVatTu { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public string TenDonVi { get; set; }
        public Nullable<double> ThanhTien { get; set; }
        public Guid PhieuNhapVatTuID { get; set; }
    }

    public class DSPhieuXuatModels
    {
        public Guid PhieuXuatVatTuID { get; set; }
        public DateTime ThoiGian { get; set; }
        public string LoaiHinhXuat { get; set; }
        public string NhanVienNhap { get; set; }
        public string TenVatTu { get; set; }
        public string TenDonVi { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public double ThanhTien { get; set; }
        public string NhanVienNhan { get; set; }
        public string CongTrinhNhan { get; set; }
        public string DoiThiCongNhan { get; set; }
    }

    public class DSChiTietPhieuXuatModels
    {
        public Guid ChiTietNhapXuatVatTuID { get; set; }
        public Nullable<double> DonGia { get; set; }
        public string TenVatTu { get; set; }
        public Nullable<double> SoLuong { get; set; }
        public string TenDonVi { get; set; }
        public Nullable<double> ThanhTien { get; set; }
        public Guid PhieuXuatVatTuID { get; set; }
    }

    public class DsVatTuFilterAddModels
    {
        public Guid VatTuID { get; set; }
        public string TenVatTu { get; set; }
        public string TenVietTat { get; set; }
        public string TenDonVi { get; set; }
        public double DonGia { get; set; }
        public string TenNhom { get; set; }
        public double? SoLuongTon { get; set; }
        public double? DonGiaNhap { get; set; }
    }

    public class ItemsAddPhieu
    {
        public int ID { get; set; }
        public double DonGia { get; set; }
        public double? DonGiaNhap { get; set; }
        public double SoLuong { get; set; }
        public string TenDonVi { get; set; }
        public string TenVatTu { get; set; }
        public string TenVietTat { get; set; }
        public double ThanhTien { get; set; }
        public Guid VatTuID { get; set; }
        public NhaCungCapModel NhaCungCap { get; set; }
    }
    public class NhaCungCapModel
    {
        public Nullable<System.Guid> NhaCCID { get; set; }
        public string SDT { get; set; }
        public string Email { get; set; }
        public string DiaChi { get; set; }
        public string NhaCCName { get; set; }
        public bool IsActive { get; set; }
    }
    public class AddPhieuNhapModels
    {
        public List<ItemsAddPhieu> items { get; set; }
        public Guid CongTrinhID { get; set; }
        public Guid DoiThiCongID { get; set; }
        public string ThoiGian { get; set; }
        public List<string> Anh { get; set; }
        public int action { get; set; }
        public int TrangThaiDuyet { get; set; }
        public string PhieuXuatVatTuID { get; set; }

       
    }

    public class AddPhieuXuatModels
    {
        public List<ItemsAddPhieu> items { get; set; }
        public int LoaiHinh { get; set; }
        public Guid CongTrinhID { get; set; }
        public Guid DoiThiCongID { get; set; }
        public string CongTrinhNhanID { get; set; }
        public string DoiThiCongNhanID { get; set; }
        public string NguoiNhanID { get; set; }
        public string ThoiGian { get; set; }
        public List<string> Anh { get; set; }
        public int action { get; set; }
        public int creatNhap { get; set; }
    }

    public class DSTonVatTuModels
    {
        public Guid VatTuID { get; set; }
        public Guid NhomVatTuID { get; set; }
        public string TenVatTu { get; set; }
        public string TenVietTat { get; set; }
        public string TenNhom { get; set; }
        public double SoLuongTon { get; set;}
        public double DonGiaGanNhat { get; set; }
        public double GiaTriTon { get; set; }
        public double TongNhap { get; set; }
        public double TongXuat { get; set; }
        public string TenDonVi { get; set; }
        public double GiaNhap { get; set; }
        public double GiaXuat { get; set; }
        public double GiaXuatGanNhat { get; set; }

    }
}