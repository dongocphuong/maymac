﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Models
{
    public class AccountModel
    {
        private List<Account> listAccount = new List<Account>();
        public AccountModel() {
            listAccount.Add(new Account { Username = "acc1", Password = "123", Roles = new string[] { "boss", "manager", "employee" } });
            listAccount.Add(new Account { Username = "acc2", Password = "123", Roles = new string[] { "manager", "employee" } });
            listAccount.Add(new Account { Username = "acc3", Password = "123", Roles = new string[] { "employee" } });
        }
        public Account Find(string Username) {
            return listAccount.Where(acc => acc.Username.Equals(Username)).FirstOrDefault();
        }
        public Account Login(string Username, string Password) {
            return listAccount.Where(acc => acc.Username.Equals(Username) && acc.Password.Equals(Password)).FirstOrDefault();
        }
    }
}