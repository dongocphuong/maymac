﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Models
{
    public class MenuModel
    {
        public Guid MenuID { get; set; }
        public string TenMenu { get; set; }
        public string DuongDan { get; set; }
        public int Level { get; set; }
        public Nullable<Guid> MenuIDCha { get; set; }
        public string Class { get; set; }
        public string icon { get; set; }
        public bool IsHead { get; set; } 
        public bool isLoadContent { get; set; }
        public bool done { get; set; }
    }
}