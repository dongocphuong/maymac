﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Models.Enums
{
    public enum LoaiHinhDoiThiCong
    {
        DOI_THI_CONG = 0,
        DOI_TAC_THUE = 1,
        DOI_TAC_GUI = 2,
        XUONG_CONG_TY = 3,
        PHONG_BAN = 4
    }
}