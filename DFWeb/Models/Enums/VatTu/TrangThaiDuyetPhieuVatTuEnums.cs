﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Models.Enums.VatTu
{
    public enum TrangThaiDuyetPhieuVatTuEnums
    {
        DADUYET = 1,
        CHUADUYET = 2,
        TUCHOI = 3
    }
}