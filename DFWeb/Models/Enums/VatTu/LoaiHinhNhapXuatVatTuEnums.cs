﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Models.Enums.VatTu
{
    public enum LoaiHinhNhapXuatVatTuEnums
    {
        NHAP = 1,
        XUAT = 2
    }
}