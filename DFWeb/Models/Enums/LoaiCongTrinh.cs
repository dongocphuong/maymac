﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Models.Enums
{
    public enum LoaiCongTrinh
    {
        CONG_TRINH = 0,
        PHONG_BAN = 1,
        CONG_TRINH_DI_THUE = 2,
        CONG_TRINH_GUI = 3
    }
}