﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Models
{
    public class CongNoNCCModels
    {
        public Guid NhaCungCapID { set; get; }
        public string TenNhaCungCap { set; get; }
        public string Email { set; get; }
        public string SDT { set; get; }
        public Nullable<double> TongTien { set; get; }

    }
}