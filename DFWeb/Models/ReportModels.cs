﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FlexCel.Core;
using FlexCel.XlsAdapter;
using FlexCel.Report;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.IO;
using System.Data;
//using HospitalPay.DBMapping.ModelEntity;
//using Oracle.DataAccess.Client;
//using HospitalPay.DataAccess;
//using HospitalPay.Web.Common.Attributes;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
//using HospitalPay.DBMapping.ModelsExt;
namespace DFWeb.Models.ReportModels
{
    /// <summary>
    /// Hàm xử lý dữ liệu trên Report
    /// Author: PhuongLT15 (09/09/2014)
    /// </summary>
    public class ReportModels
    {
        #region Lấy thông tin chữ ký
        /// <summary>
        /// Lấy thông tin chữ ký theo người sử dụng
        /// </summary>
        /// <param name="fr"></param>
        /// <param name="ControllerName"></param>
        /// <param name="isDisplay"></param>
        /// <returns></returns>
        //public static FlexCelReport getInfoSign(FlexCelReport fr, String ControllerName, string userName, Boolean isDisplay = true)
        //{
        //    ConnectEntities context = new ConnectEntities();
        //    DataTable dt = new DataTable();
        //    var conn = context.Database.Connection;
        //    var connectionState = conn.State;
        //    try
        //    {
        //        using (context)
        //        {
        //            if (connectionState != ConnectionState.Open)
        //                conn.Open();
        //            using (var cmd = conn.CreateCommand())
        //            {
        //                cmd.CommandText = "SELECT *  FROM CONFIG_REPORT WHERE CONFIG_CONTROLLER_NAME=@CONFIG_CONTROLLER_NAME AND USERNAME=@USERNAME";
        //                cmd.CommandType = CommandType.Text;
        //                cmd.Parameters.Add(new OracleParameter("@CONFIG_CONTROLLER_NAME", ControllerName));
        //                cmd.Parameters.Add(new OracleParameter("@USERNAME", userName));
        //                using (var reader = cmd.ExecuteReader())
        //                {
        //                    dt.Load(reader);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        if (connectionState != ConnectionState.Open)
        //            conn.Close();
        //        conn.Dispose();
        //        context.Dispose();

        //    }
        //    if (isDisplay == true && dt != null && dt.Rows.Count > 0)
        //    {

        //        fr.SetValue("ThuaLenh1", dt.Rows[0]["ThuaLenh1"]);
        //        fr.SetValue("ThuaLenh2", dt.Rows[0]["ThuaLenh2"]);
        //        fr.SetValue("ThuaLenh3", dt.Rows[0]["ThuaLenh3"]);
        //        fr.SetValue("ThuaLenh4", dt.Rows[0]["ThuaLenh4"]);
        //        fr.SetValue("ThuaLenh5", dt.Rows[0]["ThuaLenh5"]);

        //        fr.SetValue("ChucDanh1", dt.Rows[0]["ChucDanh1"]);
        //        fr.SetValue("ChucDanh2", dt.Rows[0]["ChucDanh2"]);
        //        fr.SetValue("ChucDanh3", dt.Rows[0]["ChucDanh3"]);
        //        fr.SetValue("ChucDanh4", dt.Rows[0]["ChucDanh4"]);
        //        fr.SetValue("ChucDanh5", dt.Rows[0]["ChucDanh5"]);

        //        fr.SetValue("Ten1", dt.Rows[0]["Ten1"]);
        //        fr.SetValue("Ten2", dt.Rows[0]["Ten2"]);
        //        fr.SetValue("Ten3", dt.Rows[0]["Ten3"]);
        //        fr.SetValue("Ten4", dt.Rows[0]["Ten4"]);
        //        fr.SetValue("Ten5", dt.Rows[0]["Ten5"]);

        //    }
        //    else
        //    {

        //        fr.SetValue("ThuaLenh1", "");
        //        fr.SetValue("ThuaLenh2", "");
        //        fr.SetValue("ThuaLenh3", "");
        //        fr.SetValue("ThuaLenh4", "");
        //        fr.SetValue("ThuaLenh5", "");

        //        fr.SetValue("ChucDanh1", "");
        //        fr.SetValue("ChucDanh2", "");
        //        fr.SetValue("ChucDanh3", "");
        //        fr.SetValue("ChucDanh4", "");
        //        fr.SetValue("ChucDanh5", "");

        //        fr.SetValue("Ten1", "");
        //        fr.SetValue("Ten2", "");
        //        fr.SetValue("Ten3", "");
        //        fr.SetValue("Ten4", "");
        //        fr.SetValue("Ten5", "");
        //    }
        //    return fr;
        //}

        #endregion

        #region "#"


        /// <summary>
        /// Lấy giá trị ngày tháng năm hiên tại
        /// </summary>
        /// <returns></returns>
        public static String Ngay_Thang_Nam_HienTai()
        {
            String Ngay = "";
            int day = DateTime.Now.Day;
            int month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            Ngay = " ngày " + day + " tháng " + month + " năm " + year;
            return Ngay;
        }
        /// <summary>
        /// Chọn khổ giấy in
        /// </summary>
        /// <returns></returns>
        public static DataTable LoaiKhoGiay()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("MaKhoGiay", typeof(String));
            dt.Columns.Add("TenKhoGiay", typeof(String));
            DataRow R1 = dt.NewRow();
            dt.Rows.Add(R1);
            R1[0] = "1";
            R1[1] = "In khổ giấy A3";
            DataRow R2 = dt.NewRow();
            dt.Rows.Add(R2);
            R2[0] = "2";
            R2[1] = "In khổ giấy A4";
            dt.Dispose();
            return dt;
        }
        /// <summary>
        /// Tên đơn vị sử dụng
        /// </summary>
        /// <param name="LoaiDonVi">1: Đơn vị cấp 1 2:Đơn vị cấp 2 3:Đơn vị cấp 3</param>
        /// <returns></returns>
        //public static void getInfoHospital(string userLogin, ref string hospitalName, ref string hospitalCode)
        //{
        //    var serializeModel = new AppUserPrincipalSerializeModel();
        //    serializeModel = HospitalPay.Web.Utils.DataTemplate.GetInfoUser(userLogin);
        //    hospitalName = serializeModel.HealthNameX;
        //    hospitalCode = serializeModel.HealthIdX;
        //}
        /// <summary>
        /// Hàm lấy cấu hình để thiết lập báo cáo
        /// </summary>
        /// <returns></returns>
        //public static CauHinhModel GetCauHinh(string hospitalCode)
        //{
        //    var serializeModel = new CauHinhModel();
        //    return serializeModel;
        //}
        /// <summary>
        /// Thay đổi ngon ngữ để hiện thị dấu "." thành ","
        /// </summary>
        public static void Language()
        {
            string lang = "ca-ES";
            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture(lang);
            System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.CreateSpecificCulture(lang);
        }
        /// <summary>
        /// Lọc từ dữ liệu Datatable ra các trường hợp trùng nhau
        /// </summary>
        /// <param name="TableName"></param>
        /// <param name="SourceTable"></param>
        /// <param name="FieldName"></param>
        /// <param name="sFieldAdd"></param>
        /// <param name="sField_DK"></param>
        /// <param name="sField_DK_Value"></param>
        /// <param name="strSort"></param>
        /// <returns></returns>
        public static DataTable SelectDistinct(string TableName, DataTable SourceTable, string FieldName, string sFieldAdd, 
            string sField_DK = "", string sField_DK_Value = "", string strSort = "")
        {
            DataTable dt = new DataTable(TableName);
            String[] arrFieldAdd = sFieldAdd.Split(',');
            String[] arrFieldName = FieldName.Split(',');
            for (int i = 0; i < arrFieldAdd.Length; i++)
            {
                dt.Columns.Add(arrFieldAdd[i], SourceTable.Columns[arrFieldAdd[i]].DataType);
            }
            if (SourceTable.Rows.Count > 0)
            {
                object[] LastValue = new object[arrFieldName.Length];
                for (int i = 0; i < LastValue.Length; i++)
                {
                    LastValue[i] = null;
                }
                foreach (DataRow dr in SourceTable.Select("", FieldName + " " + strSort))
                {
                    Boolean ok = true;
                    for (int i = 0; i < arrFieldName.Length; i++)
                    {
                        if (LastValue[i] != null && (ColumnEqual(LastValue[i], dr[arrFieldName[i]])))
                        {
                            ok = false;
                        }
                        else
                        {
                            ok = true;
                            break;
                        }
                    }
                    for (int i = 0; i < arrFieldName.Length; i++)
                    {
                        if (ok)
                        {
                            LastValue[i] = dr[arrFieldName[i]];
                        }
                    }
                    if (ok)
                    {
                        DataRow R = dt.NewRow();
                        for (int j = 0; j < arrFieldAdd.Length; j++)
                        {
                            var obj = "";
                            R[arrFieldAdd[j]] = dr[arrFieldAdd[j]];
                            obj = dr[arrFieldAdd[j]].ToString();
                        }
                        //if (String.IsNullOrEmpty(sField_DK) == false || String.IsNullOrEmpty(sField_DK_Value) == false)
                        //{
                        //    R[arrFieldAdd[arrFieldAdd.Length - 1]] = "";
                        //}                            
                        dt.Rows.Add(R);
                    }
                }
            }
            return dt;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        private static bool ColumnEqual(object A, object B)
        {

            // Compares two values to see if they are equal. Also compares DBNULL.Value.
            // Note: If your DataTable contains object fields, then you must extend this
            // function to handle them in a meaningful way if you intend to group on them.

            if ((A == DBNull.Value || A == null) && (B == DBNull.Value || B == null)) //  both are DBNull.Value
                return true;
            if ((A == DBNull.Value || A == null) || (B == DBNull.Value || B == null)) //  only one is DBNull.Value
                return false;
            return (A.Equals(B));  // value type standard comparison
        }
        #endregion
        #region "Chuyển tiền kiểu số thành chữ - phục vụ báo cáo"
        public static String TienRaChu(long Tien)
        {
            String vR = "";
            if (Tien < 0)
                return vR;
            String vR1 = "";
            long d = 0, So1, So2, So3;
            String ChuSo = "không,một,hai,ba,bốn,năm,sáu,bảy,tám,chín";
            String DonViTien = ",nghìn,triệu,tỷ,nghìn tỷ, triệu tỷ, tỷ tỷ";
            String[] arr1 = ChuSo.Split(',');
            String[] arr2 = DonViTien.Split(',');
            do
            {
                So1 = Tien % 10;
                Tien = (Tien - So1) / 10;
                So2 = Tien % 10;
                Tien = (Tien - So2) / 10;
                So3 = Tien % 10;
                Tien = (Tien - So3) / 10;
                if (!(So1 == 0 && So2 == 0 && So3 == 0))
                {
                    vR1 = "";
                    if (So3 != 0 || Tien != 0)
                    {
                        vR1 = arr1[So3] + " trăm";
                    }
                    if (So2 == 0)
                    {
                        if (vR1 != "" && So1 != 0)
                        {
                            vR1 += " linh";
                        }
                    }
                    else if (So2 == 1)
                    {
                        vR1 += " mười";
                    }
                    else
                    {
                        vR1 += " " + arr1[So2] + " mươi";
                    }
                    if (So1 != 0)
                    {
                        if (So1 == 1 && So2 >= 2)
                        {
                            vR1 += " mốt";
                        }
                        else if (So1 == 5 && So2 >= 1)
                        {
                            vR1 += " lăm";
                        }
                        else
                        {
                            vR1 += " " + arr1[So1];
                        }
                    }
                    vR1 = vR1.Trim();
                    if (vR1 != "")
                    {
                        vR = vR1 + " " + arr2[d] + " " + vR.Trim();
                    }
                }
                d = d + 1;
            } while (Tien != 0);
            vR = vR.Trim();
            if (vR == "")
            {
                vR = "không";
            }
            vR = vR.Substring(0, 1).ToUpper() + vR.Substring(1);
            return vR + " đồng";
        }

        #endregion
    }
    /// <summary>
    /// Lớp đối tượng để xử lý xuất dữ liệu ra file Excel
    /// Author: PhuongLT15 (09/09/2014)
    /// </summary>
    public class clsExcelResult : ActionResult
    {
        public string FileName { get; set; }
        public string Path { get; set; }
        public MemoryStream ms { get; set; }
        public String type { get; set; }
        public override void ExecuteResult(ControllerContext context)
        {
            try
            {
                context.HttpContext.Response.Buffer = true;
                context.HttpContext.Response.Clear();
                context.HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + FileName);
                context.HttpContext.Response.ContentType = "application/vnd." + type;
                if (string.IsNullOrEmpty(Path) == false)
                {
                    context.HttpContext.Response.WriteFile(Path);
                }
                else
                {
                    context.HttpContext.Response.BinaryWrite(ms.ToArray());
                }
                context.HttpContext.Response.End();
            }
            catch { }
        }
    }

    //public class Report79_80
    //{
    //    //Mã bệnh viện     
    //    public string HealthId { get; set; }
    //    public string HealthName { get; set; }
    //    //Khoa phòng     
    //    public string DeparmentId { get; set; }
    //    //Kiểu so sánh
    //    public string Compare { get; set; }
    //    //Đối tượng

    //    public string ObjectId { get; set; }
    //    //Dịch vụ
    //    public string CM_DMNHOMDV1_ID { get; set; }
    //    //Kỳ báo cáo       
    //    public string TimeReport { get; set; }
    //    //Từ ngày     

    //    public string FromDate { get; set; }
    //    //Đến ngày   
    //    [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "MsgEmptyCodeDepartment", ErrorMessageResourceType = typeof(Resources.Localization))]
    //    public string ToDate { get; set; }
    //    //Tháng      
    //    public string Month { get; set; }
    //    //Quý      
    //    public string ThreeMonth { get; set; }
    //    //Năm      
    //    public string Year { get; set; }
    //    //Loại báo cáo      
    //    public string TypeReport { get; set; }
    //    //Nhóm báo cáo      
    //    public string GroupReport { get; set; }
    //    //Vùng dữ liệu báo cáo     
    //    public string AreaReport { get; set; }
    //    //Tuyến      
    //    public string RouteReport { get; set; }
    //    //kieu tong hop du lieu
    //    public string TongHop { get; set; }
    //    //kieu tong hop bao cao
    //    public string BaoCao { get; set; }
    //    public string KieuBaoCao { get; set; }
    //}

    //public class Report89_80
    //{
    //    //Mã bệnh viện     
    //    public string HealthId { get; set; }
    //    public string HealthName { get; set; }
    //    //Khoa phòng     
    //    public string DeparmentId { get; set; }
    //    //Kiểu so sánh
    //    public string Compare { get; set; }
    //    //Đối tượng

    //    public string ObjectId { get; set; }
    //    //Dịch vụ
    //    public string CM_DMNHOMDV1_ID { get; set; }
    //    //Kỳ báo cáo       
    //    public string TimeReport { get; set; }
    //    //Từ ngày     

    //    public string FromDate { get; set; }
    //    //Đến ngày   
    //    [Required(AllowEmptyStrings = false, ErrorMessageResourceName = "MsgEmptyCodeDepartment", ErrorMessageResourceType = typeof(Resources.Localization))]
    //    public string ToDate { get; set; }
    //    //Tháng      
    //    public string Month { get; set; }
    //    //Quý      
    //    public string ThreeMonth { get; set; }
    //    //Năm      
    //    public string Year { get; set; }
    //    //Loại báo cáo      
    //    public string TypeReport { get; set; }
    //    //Nhóm báo cáo      
    //    public string GroupReport { get; set; }
    //    //Vùng dữ liệu báo cáo     
    //    public string AreaReport { get; set; }
    //    //Tuyến      
    //    public string RouteReport { get; set; }
    //    //Kiểu nhóm      
    //    public bool GroupReportType { get; set; }
    //    //Danh sách thuốc hoặc dịch vụ  
    //    //LuanPM2
    //    public string listCheck { get; set; }
    //    //kieu tong hop du lieu
    //    public string TongHop { get; set; }
    //    public string KieuBaoCao { get; set; }

    //}

}