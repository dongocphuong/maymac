﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Models
{
    public class CongTrinhHopDongModels
    {
        public List<DF.DataAccess.DBML.Web_HopDong_LayDanhSachHopDong_New_CongTrinhResult> lst { get; set; }
        public string CongTrinhID { get; set; }
    }
}