﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Models.CongTrinhPhongBan
{
    public class CongTrinhPhongBanModel
    {
        public Guid? CongTrinhID { get; set; }
        public string TenCongTrinh { get; set; }
        public string ChuDauTu { get; set; }
        public string NgayKhoiCong { get; set; }
        public string NgayKetThuc { get; set; }
        public string ThoiGianCapNhat { get; set; }
        public string TinhTrang { get; set; }
        public string TrangThai { get; set; }
        public string DiaDiem { get; set; }
        public int? TinhTrangInt { get; set; }
        public int? TrangThaiInt { get; set; }
        public Nullable<double> TiLeCongTrinh { get; set; }
        public Nullable<double> TiLeCongTy { get; set; }

        public Nullable<double> GTTTCongTy { get; set; }
        public Nullable<double> GTKTCongTy { get; set; }
        public Nullable<double> GTTTCongTrinh { get; set; }
        public Nullable<double> GTKTCongTrinh { get; set; }

        public Nullable<bool> LoaiBaoCaoSanLuong { get; set; }
        public Nullable<bool> KieuPhong { get; set; }
        public string GhiChu { get; set; }
        public double TongTienDuToan { get; set; }
        public double TongChiPhiHienTai { get; set; }
        public double LoiNhuanHienTai { get; set; }
        public double GiaTriHopDong { get; set; }
    }

    public class DoiPhongBanModel
    {
        public Guid? DoiThiCongId { get; set; }
        public string TenDoi { get; set; }
        public Guid? CongTrinhDoiID { get; set; }
    }

    public class listItemDoiThiCongModel
    {
        public Nullable<Guid> CongTrinhDoiHangMucID { get; set; }
        public Nullable<Guid> CongTrinhDoiID { get; set; }
        public Guid CongTrinhId { get; set; }
        public Guid DoiThiCongID { get; set; }
        public int SoLuongCoc { get; set; }
        public Nullable<double> SoMetDat { get; set; }
        public Nullable<double> SoMetDa { get; set; }
        public Nullable<Guid> HangMucThiCongID { get; set; }
    }

    public class PhongBanModel
    {
        public Guid? DuAnID { get; set; }
        public string TenDuAn { get; set; }
        public Nullable<int> Dept {get;set;}
    }

    public class DoiThiCongNewsModel
    {
        public Guid? DoiTacID { get; set; }
        public Guid? NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public string TenDoiTac { get; set; }
        public string Email { get; set; }
        public string SDT { get; set; }
        public string DiaChi { get; set; }
        public string MaDoiTac { get; set; }
        public int KieuDoiTac { get; set; }
        public string MaSoThue { get; set; }
    }
    public class DanhSachNhanVienModel
    {
        public Guid? NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
    }

    public class DoiThiCongTheoCongTrinhModel
    {
        public Nullable<Guid> CongTrinhDoiID { get; set; }
        public Guid DoiThiCongID { get; set; }
        public int SoLuongCoc { get; set; }
        public Nullable<double> SoMetDat { get; set; }
        public Nullable<double> SoMetDa { get; set; }
        public Nullable<Guid> HangMucThiCongID { get; set; }
        public string TenHangMuc { get; set; }
        public Nullable<double> GiaTriThucTe { get; set; }
        public Nullable<double> GiaTriKhauTru { get; set; }
        public string TenDoi { get; set; }
        public Nullable<double> KgOngSieuAm { get; set; }
        public Nullable<double> SoMePhiVCChatThaitDa { get; set; }
        public Nullable<double> KgThep { get; set; }
        public Nullable<Guid> CongTrinhDoiHangMucID { get; set; }
        public Nullable<double> ThanhTien { get; set; }
        public Nullable<double> HeSo { get; set; }
    }

    public class DSDoiThiCongModels
    {
        public Guid DoiThiCongID { get; set; }
        public Guid CongTrinhID { get; set; }
        public string TenCongTrinh { get; set; }
        public string TenDoi { get; set; }
        public Nullable<double> GiaTriHopDong { get; set; }
        public Nullable<double> TongTienDuToan { get; set; }
        public Nullable<double> TongChiPhiHienTai { get; set; }
        public Nullable<double> TongLoiNhuanHienTai { get; set; }
    }

    public class PhongBan_DoiThiCongModel
    {
        public string CongTrinhID { get; set; }
        //public List<DF.DataAccess.DBML.Web_PhongBan_LayDanhSachDoiThiCongResult> DSDoiThiCong { get; set; }
    }

    public class AddDoi_PhongBan
    {
        public Nullable<Guid> CongTrinhID { get; set; }
        public List<ItemDoiThiCong> items { get; set; }
    }

    public class ItemDoiThiCong
    {
        public Nullable<Guid> DoiThiCongID { get; set; }
    }
}