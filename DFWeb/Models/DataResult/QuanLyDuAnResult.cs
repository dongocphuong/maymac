﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Models.DataResult
{
    public class QuanLyDuAnResult
    {
        public int STT { get; set; }
        public Guid CongTrinhID { get; set; }
        public string ChuDauTu { get; set; }
        public string DiaDiem { get; set; }
        public DateTime NgayKhoiCong { get; set; }
        public Nullable<DateTime> NgayKetThuc { get; set; }
        public Nullable<DateTime> CapNhatLanCuoi { get; set; }
        public string TinhTrang { get; set; }
        public string TienDo { get; set; }
        public List<QuanLyDuAn_Doi> listDoi_DuAn { get; set; }
    }
    public class QuanLyDuAn_Doi
    {
        public Guid DoiThiCongID { get; set; }
        public string TenDoi { get; set; }
        public double TongTien { get; set; }
    }
    public class ChiTietQuanLyDuAn
    {
        public Guid CongTrinhID { get; set; }
        public Guid DoiThiCongID { get; set; }
        public string GiaTriHopDongKyVoiChuDauTu { get; set; }
        public string GiaTriHopDongKyVoiDoi { get; set; }
        public string GiaTriKhoan_ThietBi { get; set; }
        public string GiaTriKhoan_CCDC { get; set; }
        public string GiaTriKhoan_VatTu { get; set; }
        public string GiaTriKhoan_NhanCong { get; set; }
        public string GiaTriKhoan_Khac { get; set; }
        public string GiaTriHienTai_SanLuong { get; set; }
        public string GiaTriHienTai_ThietBi { get; set; }
        public string GiaTriHienTai_ThietBi_CaMay { get; set; }
        public string GiaTriHienTai_ThietBi_SuaChua { get; set; }
        public string GiaTriHienTai_ThietBi_ThanhLy { get; set; }
        public string GiaTriHienTai_CCDC { get; set; }
        public string GiaTriHienTai_CCDC_CaMay { get; set; }
        public string GiaTriHienTai_CCDC_SuaChua { get; set; }
        public string GiaTriHienTai_CCDC_ThanhLy { get; set; }
        public string GiaTriHienTai_VatTu { get; set; }
        public string GiaTriHienTai_NhanCong { get; set; }
        public string GiaTriHienTai_Khac { get; set; }
        public string ChenhLech_ThietBi { get; set; }
        public string ChenhLech_CCDC { get; set; }
        public string ChenhLech_VatTu { get; set; }
        public string ChenhLech_NhanCong { get; set; }
        public string ChenhLech_Khac { get; set; }
        public string ChenhLech_VanChuyen { get; set; }
        public string LoiNhuanTamTinh { get; set; }
        public string GiaTriKhoan_VanChuyen { get; set; }
        public string GiaTriHienTai_VanChuyen { get; set; }
    }
    public class QuanLyDuAn_SanLuongChiTietResult
    {
        public int STT { get; set; }
        public string TenHangMuc { get; set; }
        public DateTime ThoiGian { get; set; }
        public string NhanVien { get; set; }
        public double SoMetDat { get; set; }
        public double SoMetDa { get; set; }
        public double SoCoc { get; set; }
    }
    public class QuanLyDuAn_ThietBi_CaMay
    {
        public int STT { get; set; }
        public DateTime? Ngay { get; set; }
        public string SoPhieu { get; set; }
        public string MaMay { get; set; }
        public string TenMay { get; set; }
        public string CongTrinhDen { get; set; }
        public string CongTrinhDi { get; set; }
        public DateTime? NgayDen { get; set; }
        public DateTime? NgayDi { get; set; }
        public double SoNgayOCongTrinh { get; set; }
        public string NguoiTao { get; set; }
        public double DonGiaCaMay { get; set; }
        public double ThanhTien { get; set; }
    }
    public class QuanLyDuAn_ThietBi_SuaChua
    {
        public int STT { get; set; }
        public string Lenh { get; set; }
        public string MaMay { get; set; }
        public string TenMay { get; set; }
        public string DonViThucHien { get; set; }
        public string DonViMuaVatTu { get; set; }
        public DateTime? ThoiGianKeHoach { get; set; }
        public DateTime? ThoiGianHoanThanh { get; set; }
        public double GiaTriNhanCong { get; set; }
        public double Luong { get; set; }
        public double GiaTriVatTu { get; set; }
        public double TongTien { get; set; }
    }
    public class QuanLyDuAn_ThietBi_ThanhLy
    {
        public int STT { get; set; }
        public string MaThietBi_PhuTung { get; set; }
        public string TenThietBi_PhuTung { get; set; }
        public string DonVi { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public double ThanhTien { get; set; }
        public DateTime? NgayBan { get; set; }
        public string GhiChu { get; set; }
    }
    public class QuanLyDuAn_ThietBi_ThuongPhat
    {
        public int STT { get; set; }
        public string LoaiHinhThuongPhat { get; set; }
        public DateTime? ThoiGian { get; set; }
        public double SoTien { get; set; }
    }
    public class QuanLyDuAn_CCDC_CaMay
    {
        public int STT { get; set; }
        public DateTime Ngay { get; set; }
        public string SoPhieu { get; set; }
        public string MaMay { get; set; }
        public string TenMay { get; set; }
        public string CongTrinhDen { get; set; }
        public string CongTrinhDi { get; set; }
        public DateTime NgayDen { get; set; }
        public DateTime NgayDi { get; set; }
        public double SoNgayOCongTrinh { get; set; }
        public string NguoiTao { get; set; }
        public double DonGiaCaMay { get; set; }
        public double ThanhTien { get; set; }
    }
    public class QuanLyDuAn_CCDC_SuaChua
    {
        public int STT { get; set; }
        public string Lenh { get; set; }
        public string MaMay { get; set; }
        public string TenMay { get; set; }
        public string DonViThucHien { get; set; }
        public string DonViMuaVatTu { get; set; }
        public DateTime ThoiGianKeHoach { get; set; }
        public DateTime ThoiGianHoanThanh { get; set; }
        public double GiaTriNhanCong { get; set; }
        public double Luong { get; set; }
        public double GiaTriVatTu { get; set; }
        public double TongTien { get; set; }
    }
    public class QuanLyDuAn_CCDC_ThanhLy
    {
        public int STT { get; set; }
        public string MaThietBi_PhuTung { get; set; }
        public string TenThietBi_PhuTung { get; set; }
        public string DonVi { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public double ThanhTien { get; set; }
        public DateTime NgayBan { get; set; }
        public string GhiChu { get; set; }
    }
    public class QuanLyDuAn_CCDC_ThuongPhat
    {
        public int STT { get; set; }
        public string LoaiHinhThuongPhat { get; set; }
        public DateTime ThoiGian { get; set; }
        public double SoTien { get; set; }
    }
    public class QuanLyDuAn_VatTu_Nhap
    {
        public int STT { get; set; }
        public DateTime ThoiGian { get; set; }
        public string TenNhanVien { get; set; }
        public string TenVatTu { get; set; }
        public string TenDonVi { get; set; }
        public double SoLuong { get; set; }
        public double DonGia { get; set; }
        public double ThanhTien { get; set; }
        public bool TrangThaiDuyet { get; set; }
        public string GhiChu { get; set; }
    }
    public class QuanLyDuAn_LuongCongTrinh
    {
        public int STT { get; set; }
        public string TenNhanVien { get; set; }
        public string TuNgay { get; set; }
        public string DenNgay { get; set; }
        public double MucLuong { get; set; }
        public int SoNgayCong { get; set; }
        public double ThanhTien { get; set; }
    }

    public class QuanLyDuAn_ChiPhiVanChuyen
    {
        public int STT { get; set; }
        public string TenLan { get; set; }
        public string Tu { get; set; }
        public string Den { get; set; }
        public DateTime? ThoiGian { get; set; }
        public double SoTien { get; set; }
        public string MoTa { get; set; }
    }
    public class QuanLyDuAn_BaoCaoSanLuong
    {
        public int STT { get; set; }
        public string HangMucThiCong { get; set; }
        public double MetDaHopDong { get; set; }
        public double MetDatHopDong { get; set; }
        public double SoCocHopDong { get; set; }
        public double MetDaThucTe { get; set; }
        public double MetDatThucTe { get; set; }
        public double SoCocThucTe { get; set; }
    }

    public class QuanLyDuAn_Khoan_VatTu
    {
        public int STT { get; set; }
        public string TenVatTu { get; set; }
        public string MaVatTu { get; set; }
        public double DonGia { get; set; }
        public double SoLuong { get; set; }
        public double ThanhTien { get; set; }
    }
    public class QuanLyDuAn_Khoan_NhanCong
    {
        public int STT { get; set; }
        public string NoiDung { get; set; }
        public double DonGia { get; set; }
        public double SoCoc { get; set; }
        public double MetDat { get; set; }
        public double MetDa { get; set; }
        public double ThanhTienMetDat { get; set; }
        public double ThanhTienMetDa { get; set; }
        public double ThanhTien { get; set; }
        public string GhiChu { get; set; }
    }
    public class QuanLyDuAn_Khoan_VanChuyen
    {
        public int STT { get; set; }
        public string NoiDung { get; set; }
        public double DonGia { get; set; }
        public double KhoiLuong { get; set; }
        public double ThanhTien { get; set; }
    }
    public class QuanLyDuAn_Khoan_Khac
    {
        public int STT { get; set; }
        public string NoiDung { get; set; }
        public double SoTien { get; set; }
       
    }
}