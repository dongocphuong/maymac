﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DFWeb.Models.DataResult.HoSoCaNhan
{
    public class ThongTinCaNhanResult
    {
        public Guid NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public string Avatar { get; set; }
        public string linkAvatar { get; set; }
        public Nullable<DateTime> NgaySinh { get; set; }
        public int GioiTinh { get; set; }
        public string SDT { get; set; }
        public string TenNganHang { get; set; }
        public string SoTaiKhoanNganHang { get; set; }
        public string MaSoThue { get; set; }
        public string CMND { get; set; }
        public Nullable<DateTime> NgayCapCMND { get; set; }
        public string NoiCapCMND { get; set; }
        public string NganhNghe { get; set; }
        public string NoiCapBang { get; set; }
        public string DiaChi { get; set; }
        public string TenDoi { get; set; }
        public string TenCongTrinh { get; set; }
        public Nullable<Guid> ChucVuId { get; set; }
        public string TinhTrang { get; set; }
        public string LoaiHopDong { get; set; }
        public string NgayDiLam { get; set; }
        public string NgayHetHanHopDong { get;set; }
        public string NgayThoiViec { get; set; }
        public Nullable<Guid> DoiThiCongID { get; set; }
        public Nullable<Guid> CongTrinhID { get; set; }
        public string TenChucVu { get; set; }
        public int? SoNgayKhaiCong { get; set; }
        public int? Lever { get; set; }
        public int? SoNgayGioiHan { get; set; }

    }
    public class LichSuLamViecResult
    {
        public int STT { get; set; }
        public DateTime? ThoiGianBatDau { get; set; }
        public string DonVi { get; set; }
        public string ChucVu { get; set; }
        public double MucLuong { get; set; }
        public DateTime? ThoiGianKetThuc { get; set; }
        public List<string> FileDinhKem { get; set; }
    }
    public class BangCapChungChiResult
    {
        public int STT { get; set; }
        public Guid BangCapChungChiID { get; set; }
        public string TenChungChi { get; set; }
        public List<string> AnhChiTiet { get; set; }
    }
    public class HopDongCaNhanResult
    {
        public int STT { get; set; }
        public string TenHopDong { get; set; }
        public DateTime ThoiGian { get; set; }
        public string DonViKhoiTao { get; set; }
        public List<string> AnhChiTiet { get; set; }
        public Guid HopDongID { get; set; }
    }
    public class TaiSanQuanLyResult
    {
        public int STT { get; set; }
        public DateTime NgayBanGiao { get; set; }
        public Nullable<DateTime> NgayTra { get; set; }
        public string TenTaiSan { get; set; }
        public string MaTaiSan { get; set; }
        public List<string> BienBanBanGiao { get; set; }
        public List<string> BienBanTra { get; set; }
    }
    public class LichSuDanhGiaResult
    {
        public int STT { get; set; }
        public DateTime ThoiGian { get; set; }
        public string DanhGia { get; set; }
        public string NguoiDanhGia { get; set; }
        public string NhanXet { get; set; }
        public string ThoiGianDanhGia { get; set; }
    }
    // string ngaybatdauhdstr, string ngaydauthangdtstr, string ngaycuoithangdautienstr, string CheckTinhBHXH, string Checktinhthue, double SoTienTinhThue
    public class ThuTheoThang
    {
        public int STT { get; set; }
        public DateTime ThoiGian { get; set; }
        public double LuongSanLuong { get; set; }
        public Guid NhanVienID { get; set; }
        public string NgayBatDauHD { get; set; }
        public string NgayDauThangDauTien { get; set; }
        public string NgayCuoiThangDauTien { get; set; }
        public bool CheckTinhBHXH { get; set; }
        public bool CheckTinhThue { get; set; }
        public double SoTienTinhThue { get; set; }
        public double LuongCoBan { get; set; }
    }
    public class LichSuNhanThanhToan
    {
        public int STT { get; set; }
        public DateTime ThoiGian { get; set; }
        public string NoiDung { get; set; }
        public double SoTienThanhToan { get; set; }
    }
    public class LichSuNhanLuongResult
    {
        public double TongThuNhap { get; set; }
        public double ThuNhapDaThanhToan { get; set; }
        public double TongGiamTru { get; set; }
        public double TongThueThuNhapCaNhanTheoNam { get; set; }
        public double TongThueThuNhapCaNhanTheoThang { get; set; }
        public double ChenhLech { get; set; }
        public double ThuNhapChuaDuocThanhToan { get; set; }
        public List<ThuTheoThang> ListThuTheoThang { get; set; }
        public List<LichSuNhanThanhToan> ListLichSuNhanThanhToan { get; set; }
        public List<LichSuDieuChinhLuong> ListLichSuDieuChinhLuong { get; set; }
    }
    public class LichSuDieuChinhLuong
    {
        public int STT { get; set; }
        public DateTime TuNgay { get; set; }
        public Nullable<DateTime> DenNgay { get; set; }
        public Nullable<double> LuongCoBan { get; set; }
        public Nullable<double> LuongBHXH { get; set; }
        public Nullable<double> LuongNgoaiGio { get; set; }
        public Nullable<double> SoNguoiGTGC { get; set; }
        public string HinhThucLuong { get; set; }
        public string TenNganHang { get; set; }
        public string SoTK { get; set; }
    }
    public class ChiTietThuTheoThangResult
    {
        public double NgayCongDuocXacNhan { get; set; }
        public double NgayCongDaKhaiBao { get; set; }
        public double SoGioLamThem { get; set; }
        public string HeSoDanhGiaCongViec { get; set; }
        public double LuongCoBan { get; set; }
        public double LuongLamThemNgoaiGio { get; set; }
        public double LuongBaoHiemXaHoi { get; set; }
        public double SoNguoiGiamTruGiaCanh { get; set; }
        public double Luong { get; set; }
        public double LuongNgoaiGio { get; set; }
        public double LuongThuong { get; set; }
        public double LuongPhat { get; set; }
        public double TongLuong { get; set; }
        public List<GiamTru> dataGiamTru { get; set; }
        public double TongGiamTru { get; set; }
        public double TongThuNhap { get; set; }
    }
    public class GiamTru
    {
        public int STT { get; set; }
        public string HangMuc { get; set; }
        public string TiLe { get; set; }
        public double SoTien { get; set; }
    }
}