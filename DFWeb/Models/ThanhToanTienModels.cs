﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Models
{
    public class ThanhToanTienModels
    {
        public System.Guid ThanhToanTienID { get; set; }
        public string ThoiGian { get; set; }
        public string NoiDung { get; set; }
        public Nullable<System.Guid> NhanVienID { get; set; }
        public Nullable<double> SoTien { get; set; }
        public Nullable<System.Guid> DoiTacID { get; set; }
        public int LoaiHinh { get; set; }
    }
}