﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using DF.DBMapping.Models;

namespace DFWeb.Models
{
    public class Account
    {
        [Display(Name = "Username")]
        public string Username { get; set; }
        [Display(Name = "Password")]
        public string Password { get; set; }
        public string[] Roles { get; set; }
    }
    public class AccountLogin
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public AccountInfo AccInfo { get; set; }
    }
    public class AccountInfo
    {
        public string UserID { get; set; }
        public string Username { get; set; }
        public string NhanVienID { get; set; }
        public string PhongBanId { get; set; }
        public string HoTen { get; set; }
        public string[] Roles { get; set; }
        public string Token { get; set; }
        public string cookie { get; set; }
        public string Avatar { get; set; }
    }
}