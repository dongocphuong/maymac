﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DFWeb.Models
{
    public class NhanVienModel
    {
        public int STT { get; set; }
        public string NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public DateTime NgaySinh { get; set; }
        public int GioiTinh { get; set; }
        public string SDT { get; set; }
        public string Email { get; set; }
        public string Cmt { get; set; }
        public string DiaChi { get; set; }
        public string ChucVu { get; set; }
        public string PhongBanId { get; set; }
        public int TinhTrang { get; set; }
        public string BangCap { get; set; }
        public int LoaiHopDong { get; set; }
        public DateTime NgayDiLam { get; set; }
        public DateTime NgayThoiViec { get; set; }
        public string LyDoThoiViec { get; set; }
        public bool IsActive { get; set; }
        public Guid? CongtrinhId { get; set; }
        public Guid? CaId { get; set; }
    }

    public class NhanVienDoiDieuChuyenModel
    {
        public Guid? NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public string NgaySinh { get; set; }
        public string DiaChi { get; set; }
        public string CMT { get; set; }
        public string GioiTinh { get; set;}
        public string SDT { get; set; }
        public Guid NhanVienDoiThiCongId { get; set; }
        public Guid DoiThiCongID { get; set; }
        public string ChucVuTen { get; set; }
        public Nullable<Guid> ChucVuID { get; set; }
    }
    public class NhanVienDoiDieuChuyenModel2
    {
        public Guid? NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public string NgaySinh { get; set; }
        public string DiaChi { get; set; }
        public string CMT { get; set; }
    }
    public class NhanVienModelReturn
    {
        public Guid NhanVienID { get; set; }
    }

    public class Add_Update_ModelNhanVien
    {
        public Nullable<Guid> NhanVienID { get; set; }
        public DF.DataAccess.DBML.Web_NhanVien_LayDanhChiTietNhanVien_NewResult ItemNhanVien { get; set; }
        public List<itemCongTrinh_NhanVien> CT { get; set; }
        public List<string> CongTrinh { get; set; }
    }

    public class itemCongTrinh_NhanVien
    {
        public Guid value { get; set; }
        public string label { get; set; }
    }

    public class NhanVienAdd_Request
    {
        public Nullable<System.Guid> NhanVienID { get; set; }
        public string TenNhanVien { get; set; }
        public string NgaySinh { get; set; }
        public int GioiTinh { get; set; }
        public string SDT { get; set; }
        public string Email { get; set; }
        public string Cmt { get; set; }
        public string DiaChi { get; set; }
        public int TinhTrang { get; set; }
        public string BangCap { get; set; }
        public string NgayDiLam { get; set; }
        public string NgayThoiViec { get; set; }
        public string LyDoThoiViec { get; set; }
        public string GhiChu { get; set; }
        public Nullable<bool> DaNopBang { get; set; }
        public Nullable<bool> DaNopCmt { get; set; }
        public string NgayCapCMT { get; set; }
        public string NoiCapCMT { get; set; }
        public string NamCapBang { get; set; }
        public string NganhNghe { get; set; }
        public string TenNganHang { get; set; }
        public string SoTaiKhoanNganHang { get; set; }
        public string TruongCapBang { get; set; }
        public string MaSoThue { get; set; }
        public string Avatar { get; set; }
        public Nullable<Guid> PhongBan { get; set; }
        public Nullable<int> SoNgayGioiHan { get; set; }
        public Nullable<int> SoNgayKhaiCong { get; set; }
        public Nullable<int> Lever { get; set; }
        public List<string> CT { get; set; }
        public Nullable<Guid> DoiThiCongID { get; set; }
        public Nullable<Guid> ChucVu { get; set; }
        public int Dept { get; set; }
        public string TaiKhoan { get; set; }
        public Nullable<int> ThamGiaPhongChat { get; set; }
        public bool ThoatKhiNghiViec { get; set; }
    }

    public class DieuChuyenNhanVien_Request
    {
        public Nullable<System.Guid> NhanVienID { get; set; }
        public List<string> CT { get; set; }
        public Nullable<Guid> DoiThiCongID { get; set; }
        public Nullable<Guid> ChucVu { get; set; }
        public string ThoiGian { get; set; }

        public Nullable<int> Dept { get; set; }
    }

    public class DieuChuyenNhanVien_RequestMultiple
    {
        public List<string> CT { get; set; }
        public Nullable<Guid> DoiThiCongID { get; set; }
        public Nullable<Guid> ChucVu { get; set; }
        public string ThoiGian { get; set; }
        public Nullable<int> Dept { get; set; }
        public List<ItemDieuChuyenNhanVien_RequestMultiple> items { get; set; }
    }
    public class ItemDieuChuyenNhanVien_RequestMultiple
    {
        public Nullable<System.Guid> NhanVienID { get; set; }
        public Nullable<System.Guid> ChucVuID { get; set; }
    }

    public class DieuChuyenMultiple
    {
        public List<ItemDieuChuyenMultiple> NhanVien { get; set; }
    }

    public class ItemDieuChuyenMultiple
    {
        public string NhanVienID { get; set; }
        public string ChucVu { get; set;}
        public string TenDoi { get; set; }
        public string TenNhanVien { get; set; }

    }

    public class DieuChuyenViewModels
    {
        public DieuChuyenMultiple DSNhanVien { get; set; }
        public List<itemCongTrinh_NhanVien> CT { get; set; }
        public List<DSChucVu> ChucVu { get; set; }
    }

    public class DSChucVu
    {
        public Nullable<System.Guid> ChucVuID { get; set; }
        public string TenChucVu { get; set; }
        public int MaChucVu { get; set; }
    }
}
