﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;
namespace DFWeb.Models
{
    public class Language
    {
        public static readonly ObjectCache Cache = MemoryCache.Default;    
        public static Dictionary<string, Dictionary<string, string>> GetLanguage()
        {
            if (Cache.Get("language") == null)
            {
                Dictionary<string, Dictionary<string, string>> dr = new Dictionary<string, Dictionary<string, string>>();
                var db = new DF.DataAccess.DBML.DBMLDFDataContext();
                var lst = db.Web_Language_GetLanguage();
                foreach (var item in lst) {
                    dr.Add(item.langkey, new Dictionary<string, string> { { "vn", item.vn }, { "en", item.en }, { "cn", item.cn } });
                };
                Cache.Add("language", dr, DateTime.Now.AddDays(365));
                return dr;
            }
            else
            {
                return (Dictionary<string, Dictionary<string, string>>)Cache.Get("language");
            }
        }

        public static bool ResetLanguage()
        {
            Cache.Remove("language");
            Dictionary<string, Dictionary<string, string>> dr = new Dictionary<string, Dictionary<string, string>>();
            var db = new DF.DataAccess.DBML.DBMLDFDataContext();
            var lst = db.Web_Language_GetLanguage();
            foreach (var item in lst)
            {
                dr.Add(item.langkey, new Dictionary<string, string> { { "vn", item.vn }, { "en", item.en }, { "cn", item.cn } });
            };
            Cache.Add("language", dr, DateTime.Now.AddDays(365));
            return true;
        }

        public static string GetText(string key)
        {
            try
            {
                var language=GetKeyLang();
                return GetLanguage()[key][language];
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string GetKeyLang()
        {
            if (HttpContext.Current.Request.Cookies["DFS_Language"] == null || HttpContext.Current.Request.Cookies["DFS_Language"].Value==null)
            {
                return "vn";
            }
            else
            {
                HttpCookie Cookie = HttpContext.Current.Request.Cookies["DFS_Language"];
                return Cookie.Value;
            }

        }

        public static void SetLanguage(string language)
        {
            if (language != "")
            {
                HttpCookie Cookie = HttpContext.Current.Request.Cookies["DFS_Language"];
                if (Cookie != null)
                {
                    Cookie.Value = language;
                    Cookie.Expires = DateTime.Now.AddDays(365);
                    HttpContext.Current.Response.Cookies.Set(Cookie);
                    HttpContext.Current.Request.Cookies.Set(Cookie);
                    
                }
                else
                {
                    HttpCookie myCookie = new HttpCookie("DFS_Language");
                    myCookie.Value = language;
                    myCookie.Expires = DateTime.Now.AddDays(365);
                    HttpContext.Current.Response.Cookies.Add(myCookie);
                    HttpContext.Current.Request.Cookies.Set(myCookie);
                }
            }
        }
        public static string Get_TinhTrangNhanVien(int key, string lang)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            if (key == 1) // Đang làm việc
            {
                values.Add("vn", "Đang làm việc");
                values.Add("en", "Working");
                values.Add("cn", "工作的");
            }
            else if (key == 2)
            {
                values.Add("vn", "Nghỉ chờ việc");
                values.Add("en", "Waiting");
                values.Add("cn", "等待");
            }
            else if (key == 3)
            {
                values.Add("vn", "Đã nghỉ việc");
                values.Add("en", "Leave Job");
                values.Add("cn", "离开");
            }
            else
            {
                values.Add("vn", "Không Xác Định");
                values.Add("en", "Undefine");
                values.Add("cn", "不明");
            }
            string test;
            values.TryGetValue(lang, out test);
            return test;

        }

        public static string Get_LoaiHopDongNhanVien(int key, string lang)
        {
            //1:Dài hạn, 2: 12-36 tháng, 3: thử việc , 4: mùa vụ,  khác: đang xác định
            Dictionary<string, string> values = new Dictionary<string, string>();
            if (key == 1) // Đang làm việc
            {
                values.Add("vn", "Dài hạn");
                values.Add("en", "Long");
                values.Add("cn", "LT");
            }
            else if (key == 2)
            {
                values.Add("vn", "12 - 36 tháng");
                values.Add("en", "12 - 36 month");
                values.Add("cn", "12 - 36 月");
            }
            else if (key == 3)
            {
                values.Add("vn", "Thử việc");
                values.Add("en", "English");
                values.Add("cn", "缓刑");
            }
            else if (key == 4)
            {
                values.Add("vn", "Mùa Vụ");
                values.Add("en", "English");
                values.Add("cn", "China");
            }
            else
            {
                values.Add("vn", "Không Xác Định");
                values.Add("en", "Undefine");
                values.Add("cn", "不明");
            }
            string test;
            values.TryGetValue(lang, out test);
            return test;

        }

        public static string Get_MessageLanguage(string key, string lang)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            switch (key)
            {
                case "success":
                    values.Add("vn", "Thành Công");
                    values.Add("en", "Success");
                    values.Add("cn", "成功");
                    break;
                case "error":
                    values.Add("vn", "Có lỗi trong quá trình xử lý dữ liệu");
                    values.Add("en", "Error");
                    values.Add("cn", "错误");
                    break;
                default:
                    values.Add("vn", "Thành Công");
                    values.Add("en", "Success");
                    values.Add("cn", "成功");
                    break;
            }
            string test;
            values.TryGetValue(lang, out test);
            return test;

        }

        public static string Get_TitleChiPhiTB(int key, string lang)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            switch (key)
            {
                case 1:
                    values.Add("vn", "Đầu tư thiết bị");
                    values.Add("en", "unknow");
                    values.Add("cn", "成功");
                    break;
                case 2:
                    values.Add("vn", "Thuê thiết bị");
                    values.Add("en", "Error");
                    values.Add("cn", "错误");
                    break;
                case 3:
                    values.Add("vn", "Ca Máy");
                    values.Add("en", "Error");
                    values.Add("cn", "错误");
                    break;
                case 4:
                    values.Add("vn", "Sửa Chữa");
                    values.Add("en", "Error");
                    values.Add("cn", "错误");
                    break;
                case 5:
                    values.Add("vn", "Thanh Lý Máy");
                    values.Add("en", "Error");
                    values.Add("cn", "错误");
                    break;
                case 6:
                    values.Add("vn", "Thanh Lý Thiết Bị");
                    values.Add("en", "Error");
                    values.Add("cn", "错误");
                    break;
                default:
                    values.Add("vn", "Đầu tư thiết bị");
                    values.Add("en", "Success");
                    values.Add("cn", "成功");
                    break;
            }
            string test;
            values.TryGetValue(lang, out test);
            return test;

        }

        public static string Get_HinhThucTinhLuong(string key, string lang)
        {
            Dictionary<string, string> values = new Dictionary<string, string>();
            switch (key)
            {
                case "1":
                    values.Add("vn", "Chấm công");
                    values.Add("en", "Success");
                    values.Add("cn", "成功");
                    break;
                default:
                    values.Add("vn", "Không Chấm Công");
                    values.Add("en", "Success");
                    values.Add("cn", "成功");
                    break;
            }
            string test;
            values.TryGetValue(lang, out test);
            return test;
        }

        public static string Get_ThongBaoHoanTraThietBi(int key, string lang)
        {
            //1:Hoàn trả, 2: thuê tiếp, 3: mua lại , 4: mua mới thiết bị, 5: Thanh lý,  khác: đang xác định
            Dictionary<string, string> values = new Dictionary<string, string>();
            if (key == 1) // Đang làm việc
            {
                values.Add("vn", "Hoàn trả thiết bị cho");
                values.Add("en", "Long");
                values.Add("cn", "LT");
            }
            else if (key == 2)
            {
                values.Add("vn", "Thuê lại thiết bị từ");
                values.Add("en", "12 - 36 month");
                values.Add("cn", "12 - 36 月");
            }
            else if (key == 3)
            {
                values.Add("vn", "Mua lại thiết bị từ");
                values.Add("en", "English");
                values.Add("cn", "缓刑");
            }
            else if (key == 4)
            {
                values.Add("vn", "Mua mới thiết bị từ");
                values.Add("en", "English");
                values.Add("cn", "China");
            }
            else if (key == 5)
            {
                values.Add("vn", "Thanh lý thiết bị cho");
                values.Add("en", "English");
                values.Add("cn", "China");
            }
            else
            {
                values.Add("vn", "Không Xác Định");
                values.Add("en", "Undefine");
                values.Add("cn", "不明");
            }
            string test;
            values.TryGetValue(lang, out test);
            return test;
        }
    }
}