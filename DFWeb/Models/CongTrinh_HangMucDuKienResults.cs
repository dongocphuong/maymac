﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DF.DataAccess.DBML;

namespace DFWeb.Models
{
    public class CongTrinh_HangMucDuKienResults
    {
        public List<DF.DataAccess.DBML.Web_HangMucThiCongDuKien_LayDanhSachHangMucTheoCongTrinh_NewResult> DanhSachHangMuc { get; set; }
        public List<DanhMucHangMucThiCong> DanhSachDMHangMuc { get; set; }
        public string CongTrinhID { get; set; }
    }

    public class HangMucDuKienRequests
    {
        public Nullable<Guid> HangMucThiCongID { get; set; }
        public Nullable<int> SoCoc { get; set; }
        public string BatDau { get; set; }
        public string KetThuc { get; set; }
        public Nullable<double> SoMetDat { get; set; }
        public Nullable<double> SoMetDa { get; set; }
        public Nullable<double> DGMetDat { get; set; }
        public Nullable<double> DGMetDa { get; set; }
        public Nullable<double> MetRong { get; set; }
        public Nullable<double> DGMetRong { get; set; }
        public Nullable<Guid> CongTrinhID { get; set; }
        public Nullable<Guid> DMHangMucThiCongID { get; set; }
        public List<ItemThuocTinh> ThuocTinh { get; set; }
        public Nullable<bool> InsertHopDong { get; set; }
        public Nullable<Guid> CongTrinhDoiHangMucID { get; set; }
    }

    public class ItemThuocTinh
    {
        public Nullable<Guid> ThuocTinhID { get; set; }
        public Nullable<double> GiaTri { get; set; }
        public Nullable<double> DonGia { get; set; }

    }

    public class CongTrinh_HangMucResults
    {
        public List<Web_HangMucThiCong_LayDanhSachHangMucTheoCongTrinh_NewResult> DanhSachHangMuc { get; set; }
        
        public ChiTietDinhMucHangMuc DinhMucHangMucThiCong { get; set; }
        public string CongTrinhID { get; set; }
        public string HangMucThiCongID { get; set; }
        public List<Web_PhanBoHangMuc_DanhSachDoiThiCongTrongCongTrinhResult> lstDoi { get; set; }
        public List<Web_PhanBoHangMuc_LayPhanBoVatTu_NewResult> lstDSPhanBoVatTu { get; set; }
        public string CongTrinhDoiID { get; set; }
    }
    public class ChiTietDinhMucHangMuc
    {
        public List<Web_LayDinhMucHangMucByHangMucIDResult> DinhMucHangMucThiCong { get; set; }
        public string HangMucID { get; set; }
        public string TenHangMuc { get; set; }
        public string DongMoi { get; set; }
        public string SuaChua { get; set; }
        public string NangCap { get; set; }
    }

    public class CongTrinh_PhanBoHangMucNewResults
    {
        public List<Web_HangMucThiCong_LayDanhSachHangMucTheoCongTrinh_NewResult> DanhSachHangMuc { get; set; }
        public List<Web_PhanBoHangMuc_DanhSachDoiThiCongTrongCongTrinhResult> lstDoi { get; set; }
        public string CongTrinhID { get; set; }
        public List<Web_CongTrinh_ChiTietPhanBoResult> chiTietPhanBo { get; set; }
    }

    public class ItemAddDinhMuc
    {
        public Guid VatTuID { get; set; }
        public double SoLuong { get; set; }
    }

    public class AddDinhMucHangMucRequest
    {
        public List<ItemAddDinhMuc> items { get; set; }
        public Guid HangMucID { get; set; }
        public string TenHangMuc { get; set; }
        public int LoaiHinh { get; set; }
    }

    public class PhanBoDinhMucRequest
    {
        public List<ItemAddDinhMuc> items { get; set; }
        public Guid HangMucID { get; set; }
        public Guid CongTrinhDoiID { get; set; }
    }

    public class PhanBoHangMuc_Results
    {
        public List<Web_PhanBoHangMuc_DanhSachDoiThiCongTrongCongTrinhResult> DSDoiTrongCongTrinh { get; set; }
        public List<Web_PhanBoHangMuc_LayDanhSachCongTrinhDoiHangMucResult> DSCongTrinhDoiHangMuc { get; set; }
        public List<Web_PhanBoHangMuc_LayDanhSachHangMucThiCongResult> DSHangMucThiCong { get; set; }
        public List<Web_PhanBoHangMuc_LayDanhSachDoiChuaPhanBoResult> DSDoiChuaPhanBo { get; set; }
        public string CongTrinhID { get; set; }
        public Nullable<Guid> DMHangMucID { get; set; }
        public Nullable<Guid> HangMucThiCongID { get; set; }
    }
}