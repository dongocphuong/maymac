﻿using DF.DataAccess;
using DF.DBMapping.Models;
using DFWeb.Models;
using DFWeb.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DFWeb.Common
{
    public class GenMenu
    {
        public static List<MenuModel> GetListMenu()
        {
            List<MenuModel> lstMenu = new List<MenuModel>();
            List<DF.DataAccess.DBML.Web_PhanQuyen_GenMenuByUserIDResult> lstMenu2 = new List<DF.DataAccess.DBML.Web_PhanQuyen_GenMenuByUserIDResult>();
            List<DF.DataAccess.DBML.Web_PhanQuyen_GenMenuByUserIDResult> lstMenu3 = new List<DF.DataAccess.DBML.Web_PhanQuyen_GenMenuByUserIDResult>();
            List<DF.DataAccess.DBML.Web_PhanQuyen_GenMenuByUserIDResult> lstMenu4 = new List<DF.DataAccess.DBML.Web_PhanQuyen_GenMenuByUserIDResult>();
            AccountInfo accInfo = CookiePersister.getAcountInfo();


            UnitOfWork unitofwork = new UnitOfWork();
            lstMenu2 = unitofwork.tblGroupUsers.GetMenuByUserID(Guid.Parse(accInfo.UserID));
            lstMenu3 = lstMenu2.Where(z => z.IsHeader == true).OrderBy(z=>z.STT).ToList();
            int i = 0;
            foreach (var item in lstMenu3)
            {
                lstMenu.Add(new MenuModel
                {
                    MenuID = item.GroupGuid,
                    TenMenu = item.GroupName,
                    DuongDan = "#",
                    IsHead = true
                });
                i++;
               var lstMenuPr = lstMenu2.Where(z => z.IsHeader == false && z.Nhom == item.Nhom && z.GroupParent == null && z.GroupGuid != item.GroupGuid).OrderBy(z => z.STT).ToList();
                foreach (var itemPR in lstMenuPr)
                {
                    lstMenu.Add(new MenuModel
                    {
                        MenuID = itemPR.GroupGuid,
                        TenMenu = itemPR.GroupName,
                        DuongDan = itemPR.UrlLink,
                        Level = 1,
                        icon = itemPR.Icon,
                        IsHead = false,
                    });
                    i++;
                    var lstMenuChi = lstMenu2.Where(z => z.GroupGuid != item.GroupGuid && z.GroupParent == itemPR.GroupGuid).OrderBy(z => z.STT).ToList();
                    foreach (var itemChi in lstMenuChi)
                    {
                        lstMenu.Add(new MenuModel
                        {
                            MenuID = itemChi.GroupGuid,
                            TenMenu = itemChi.GroupName,
                            DuongDan = itemChi.UrlLink,
                            Level = 2,
                            icon = itemPR.Icon,
                            IsHead = false,
                            MenuIDCha = itemPR.GroupGuid,
                        });
                        i++;
                    }
                }
            }
            if (accInfo != null)
            {
                var userID = Guid.Parse(accInfo.UserID);
                //#region Hồ Sơ Cá Nhân
                //lstMenu.Add(new MenuModel
                //{
                //    MenuID = 1,
                //    TenMenu = Language.GetText("hosocanhan"),
                //    DuongDan = "#",
                //    IsHead = true
                //});
                //lstMenu.Add(new MenuModel
                //{
                //    MenuID = 12,
                //    TenMenu = Language.GetText("thongtincanhan"),
                //    DuongDan = "/ThongTinCaNhan/Index",
                //    Level = 1,
                //    icon = "fa fa-user",
                //    IsHead = false,
                //});
                //lstMenu.Add(new MenuModel
                //{
                //    MenuID = 13,
                //    TenMenu = Language.GetText("khaibaocongviec"),
                //    DuongDan = "/QLKhaiBaoCV/NhapBaoCaoCongViec",
                //    Level = 1,
                //    icon = "fa fa-pencil-square",
                //});
                //lstMenu.Add(new MenuModel
                //{
                //    MenuID = 14,
                //    TenMenu = Language.GetText("quanlythunhapcanhan"),
                //    DuongDan = "/ThongTinCaNhan/ThuNhapCaNhan",
                //    Level = 1,
                //    icon = "fa fa-money",
                //});
                //#endregion

                //#region Nghiệp Vụ
                //lstMenu.Add(new MenuModel
                //{
                //    MenuID = 3,
                //    TenMenu = Language.GetText("nghiepvu"),
                //    DuongDan = "#",
                //    IsHead = true
                //});
                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebPhongKTKH))
                //{
                //    //Kinh tế kế hoạch
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 70,
                //        TenMenu = Language.GetText("quanlyduan"),
                //        DuongDan = "#",
                //        Level = 1,
                //        icon = "fa fa-building",
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 71,
                //        TenMenu = Language.GetText("khoitaoduan"),
                //        DuongDan = "/QuanLyCongTrinh/KhoiTaoDuAn",
                //        Level = 2,
                //        icon = "fa fa-newspaper-o",
                //        IsHead = false,
                //        MenuIDCha = 70,
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 72,
                //        TenMenu = Language.GetText("danhsachduan"),
                //        DuongDan = "/QuanLyCongTrinh/Index",
                //        Level = 2,
                //        icon = "fa fa-newspaper-o",
                //        IsHead = false,
                //        MenuIDCha = 70,
                //    });

                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 74,
                //        TenMenu = Language.GetText("quanlythicong"),
                //        DuongDan = "/QuanLyThiCong/Index",
                //        Level = 2,
                //        icon = "fa fa-newspaper-o",
                //        IsHead = false,
                //        MenuIDCha = 70,

                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 75,
                //        TenMenu = Language.GetText("quanlysanluong"),
                //        DuongDan = "/QuanLySanLuong/Index",
                //        Level = 2,
                //        icon = "fa fa-newspaper-o",
                //        IsHead = false,
                //        MenuIDCha = 70,

                //    });

                //    //lstMenu.Add(new MenuModel
                //    //{
                //    //    MenuID = 76,
                //    //    TenMenu = Language.GetText("quanlycocthicong"),
                //    //    DuongDan = "/QuanLyCocThiCong/Index",
                //    //    Level = 2,
                //    //    icon = "fa fa-newspaper-o",
                //    //    MenuIDCha = 70,

                //    //});
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 75,
                //        TenMenu = Language.GetText("quyettoancongtrinh"),
                //        DuongDan = "/QuanLyCongTrinh/QuyetToanDuAn",
                //        Level = 2,
                //        icon = "fa fa-newspaper-o",
                //        IsHead = false,
                //        MenuIDCha = 70,
                //        done = true
                //    });

                //}
                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebThietBi))
                //{
                //    //Thiết Bị
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 31,
                //        TenMenu = Language.GetText("thietbi"),
                //        DuongDan = "#",
                //        Level = 1,
                //        icon = "fa fa-steam",
                //        IsHead = false,
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 311,
                //        TenMenu = Language.GetText("danhsachthietbi"),
                //        DuongDan = "/DanhSachThietBi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 31,
                //        IsHead = false
                //    });

                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 313,
                //        TenMenu = Language.GetText("quanlydieuchuyen"),
                //        DuongDan = "/QuanLyDieuChuyenThietBi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 31,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 314,
                //        TenMenu = Language.GetText("quanlydongia"),
                //        DuongDan = "/QuanLyDonGiaThietBi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 31,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 312,
                //        TenMenu = Language.GetText("quanlysuachua"),
                //        DuongDan = "/QuanLySuaChuaThietBi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 31,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 316,
                //        TenMenu = Language.GetText("quanlytaichinhthietbi"),
                //        DuongDan = "/QuanLyTaiChinhThietBi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 31,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 317,
                //        TenMenu = Language.GetText("quanlydautu"),
                //        DuongDan = "/QuanLyDauTuThietBi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 31,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 318,
                //        TenMenu = Language.GetText("quanlythanhly"),
                //        DuongDan = "/QuanLyThanhLyThietBi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 31,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 315,
                //        TenMenu = Language.GetText("quanlydithue"),
                //        DuongDan = "/QuanLyDiThueThietBi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 31,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 319,
                //        TenMenu = Language.GetText("baocaotinhtrang"),
                //        DuongDan = "/TinhTrangThietBi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 31,
                //        IsHead = false
                //    });

                //}
                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebCCDC))
                //{
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 100,
                //        TenMenu = Language.GetText("thietbibotro"),
                //        DuongDan = "#",
                //        Level = 1,
                //        icon = "fa fa-steam",
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 101,
                //        TenMenu = Language.GetText("danhsachthietbibotro"),
                //        DuongDan = "/QuanLyCCDC/Index",
                //        Level = 2,
                //        MenuIDCha = 100,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 102,
                //        TenMenu = Language.GetText("quanlydieuchuyen"),
                //        DuongDan = "/QuanLyDieuChuyenCCDC/Index",
                //        Level = 2,
                //        MenuIDCha = 100,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 103,
                //        TenMenu = Language.GetText("quanlydongia"),
                //        DuongDan = "/QuanLyDonGiaCCDC/Index",
                //        Level = 2,
                //        MenuIDCha = 100,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 104,
                //        TenMenu = Language.GetText("quanlysuachua"),
                //        DuongDan = "/QuanLySuaChuaCCDC/Index",
                //        Level = 2,
                //        MenuIDCha = 100,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 105,
                //        TenMenu = Language.GetText("quanlytaichinh"),
                //        DuongDan = "/QuanLyTaiChinhCCDC/Index",
                //        Level = 2,
                //        MenuIDCha = 100,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 106,
                //        TenMenu = Language.GetText("quanlydautu"),
                //        DuongDan = "/QuanLyDauTuCCDC/Index",
                //        Level = 2,
                //        MenuIDCha = 100,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 107,
                //        TenMenu = Language.GetText("quanlythanhly"),
                //        DuongDan = "/QuanLyThanhLyCCDC/Index",
                //        Level = 2,
                //        MenuIDCha = 100,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 108,
                //        TenMenu = Language.GetText("quanlydithue"),
                //        DuongDan = "/QuanLyDiThueCCDC/Index",
                //        Level = 2,
                //        MenuIDCha = 100,
                //        IsHead = false
                //    });
                //}

                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebPhongVatTu))
                //{
                //    //Vật Tư
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 32,
                //        TenMenu = Language.GetText("vattu"),
                //        DuongDan = "/QuanLyVatTu/Index",
                //        Level = 1,
                //        icon = "fa fa-wrench",
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 321,
                //        TenMenu = Language.GetText("khotong"),
                //        DuongDan = "/QuanLyVatTuKhoTong/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 32,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 322,
                //        TenMenu = Language.GetText("quanlykhodonvi"),
                //        DuongDan = "/QuanLyVatTuKhoDonVi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 32,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 323,
                //        TenMenu = Language.GetText("quanlyphieunhap"),
                //        DuongDan = "/QuanLyPhieuNhapVatTu/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 32,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 324,
                //        TenMenu = Language.GetText("quanlyphieuxuat"),
                //        DuongDan = "/QuanLyPhieuXuatVatTu/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 32,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 325,
                //        TenMenu = Language.GetText("quanlygia"),
                //        DuongDan = "/QuanLyGiaVatTu/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 32,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 326,
                //        TenMenu = Language.GetText("quanlycongnodoitac"),
                //        DuongDan = "/QuanLyCongNoDoiTacVatTu/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 32,
                //        IsHead = false
                //    });
                //}
                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebCCDC))
                //{
                //    //Vật Tư
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 34,
                //        TenMenu = Language.GetText("ccdcnhom2"),
                //        DuongDan = "/QuanLyCCDC_V2/Index",
                //        Level = 1,
                //        icon = "fa fa-wrench",
                //        IsHead = false
                //    });
                //    //lstMenu.Add(new MenuModel
                //    //{
                //    //    MenuID = 347,
                //    //    TenMenu = Language.GetText("danhsachthietbibotro"),
                //    //    DuongDan = "/DanhSachCCDC_V2/Index",
                //    //    Level = 2,
                //    //    icon = "fa fa-steam",
                //    //    MenuIDCha = 34,
                //    //    IsHead = false
                //    //});
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 341,
                //        TenMenu = Language.GetText("khotong"),
                //        DuongDan = "/QuanLyCCDC_V2KhoTong/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 34,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 342,
                //        TenMenu = Language.GetText("quanlykhodonvi"),
                //        DuongDan = "/QuanLyCCDC_V2KhoDonVi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 34,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 343,
                //        TenMenu = Language.GetText("quanlyphieunhap"),
                //        DuongDan = "/QuanLyPhieuNhapCCDC_V2/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 34,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 344,
                //        TenMenu = Language.GetText("quanlyphieuxuat"),
                //        DuongDan = "/QuanLyPhieuXuatCCDC_V2/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 34,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 345,
                //        TenMenu = Language.GetText("quanlygia"),
                //        DuongDan = "/QuanLyGiaCCDC_V2/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 34,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 346,
                //        TenMenu = Language.GetText("quanlycongnodoitac"),
                //        DuongDan = "/QuanLyCongNoDoiTacCCDC_V2/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 34,
                //        IsHead = false
                //    });
                //}
                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebPhongKeToan))
                //{
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 327,
                //        TenMenu = Language.GetText("quanlychiphi"),
                //        DuongDan = "#",
                //        Level = 1,
                //        icon = "fa fa-money",
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 328,
                //        TenMenu = Language.GetText("denghitaichinh"),
                //        DuongDan = "/QuanLyPhieuChi/Index",
                //        Level = 2,
                //        icon = "fa fa-wrench",
                //        MenuIDCha = 327,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 329,
                //        TenMenu = Language.GetText("phanbophieuchi"),
                //        DuongDan = "/PhanBoChiPhi/Index",
                //        Level = 2,
                //        icon = "fa fa-wrench",
                //        MenuIDCha = 327,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 330,
                //        TenMenu = Language.GetText("trinhky"),
                //        DuongDan = "/QuanLyPhieuChi/TrinhKyPhieuChi",
                //        Level = 2,
                //        icon = "fa fa-wrench",
                //        MenuIDCha = 327,
                //        IsHead = false
                //    });
                //}
                ////lstMenu.Add(new MenuModel
                ////{
                ////    MenuID = 328,
                ////    TenMenu = Language.GetText("chiphivanphong"),
                ////    DuongDan = "/QLTaiChinh/TaiChinhVanPhong",
                ////    Level = 1,
                ////    icon = "fa fa-wrench",
                ////    IsHead = false
                ////});


                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebNhanSu))
                //{
                //    //Hành Chính
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 36,
                //        TenMenu = Language.GetText("hanhchinh"),
                //        DuongDan = "#",
                //        Level = 1,
                //        icon = "fa fa-book",
                //        IsHead = false,
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 361,
                //        TenMenu = Language.GetText("danhsachnhanvien"),
                //        DuongDan = "/QLNhanVien/Index",
                //        Level = 2,
                //        MenuIDCha = 36,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 363,
                //        TenMenu = Language.GetText("lichsudieuchuyen"),
                //        DuongDan = "/LichSuDieuChuyen/Index",
                //        Level = 2,
                //        MenuIDCha = 36,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 364,
                //        TenMenu = Language.GetText("danhgiacongviec"),
                //        DuongDan = "/DanhGiaCongViec/Index",
                //        Level = 2,
                //        MenuIDCha = 36,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 366,
                //        TenMenu = Language.GetText("quanlyngaynghiphep"),
                //        DuongDan = "/PhepNhanVien/Index",
                //        Level = 2,
                //        MenuIDCha = 36,
                //        IsHead = false
                //    });
                //    if (accInfo.NhanVienID.ToLower() != WebConst.NhanVienBand.ToLower())
                //    {
                //        lstMenu.Add(new MenuModel
                //        {
                //            MenuID = 362,
                //            TenMenu = Language.GetText("quanlytaisan"),
                //            DuongDan = "/QuanLyTaiSan/Index",
                //            Level = 2,
                //            MenuIDCha = 36,
                //            IsHead = false
                //        });
                //    }
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 364,
                //        TenMenu = Language.GetText("thongkekhaibaocongviec"),
                //        DuongDan = "/QLKhaiBaoCV/TongHopKBCV",
                //        Level = 2,
                //        MenuIDCha = 36,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 365,
                //        TenMenu = Language.GetText("thongkekhaibaocongviectheonhanvien"),
                //        DuongDan = "/QLKhaiBaoCV/QLTongHopKBCVTheoNV",
                //        Level = 2,
                //        MenuIDCha = 36,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 366,
                //        TenMenu = Language.GetText("tonghopcongnhanvien"),
                //        DuongDan = "/QLKhaiBaoCV/TongHopCongNhanVien",
                //        Level = 2,
                //        MenuIDCha = 36,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 367,
                //        TenMenu = Language.GetText("quanlytaikhoan"),
                //        DuongDan = "/QuanLyTaiKhoan/Index",
                //        Level = 2,
                //        MenuIDCha = 36,
                //        IsHead = false
                //    });
                //}

                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebNhanSu))
                //{

                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 61,
                //        TenMenu = Language.GetText("quanlyphongchat"),
                //        DuongDan = "/QuanLyPhongChat/Index",
                //        Level = 1,
                //        icon = "fa-steam",
                //    });

                //}
                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebPhongKTKH) || accInfo.Roles.Contains(WebConst.WebPhongDuAn) || accInfo.Roles.Contains(WebConst.WebPhongKeToan))
                //{

                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 40,
                //        TenMenu = Language.GetText("quanlyhopdongvanban"),
                //        DuongDan = "/QLHopDong/Index",
                //        Level = 1,
                //        icon = "fa-file-text-o",
                //    });

                //}
                ////lstMenu.Add(new MenuModel
                ////{
                ////    MenuID = 60,
                ////    TenMenu = Language.GetText("quanlyvanchuyen"),
                ////    DuongDan = "#",
                ////    Level = 1,
                ////    icon = "fa-truck",
                ////});
                ////lstMenu.Add(new MenuModel
                ////{
                ////    MenuID = 601,
                ////    TenMenu = Language.GetText("baogia"),
                ////    DuongDan = "/QuanLyVanChuyen/Index",
                ////    Level = 2,
                ////    MenuIDCha = 60,
                ////    IsHead = false
                ////});
                //lstMenu.Add(new MenuModel
                //{
                //    MenuID = 601,
                //    TenMenu = Language.GetText("vanchuyen"),
                //    DuongDan = "/QuanLyVanChuyen/QuanLyChuyenXe",
                //    Level = 2,
                //    MenuIDCha = 60,
                //    IsHead = false
                //});
                //lstMenu.Add(new MenuModel
                //{
                //    MenuID = 62,
                //    TenMenu = Language.GetText("quanlydoitac"),
                //    DuongDan = "/QuanLyDoiTac/Index",
                //    Level = 1,
                //    icon = "fa-users",
                //});
                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebPhongKeToan))
                //{
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 90,
                //        TenMenu = Language.GetText("quanlythunhap"),
                //        DuongDan = "#",
                //        Level = 1,
                //        IsHead = false,
                //        icon = "fa fa-money"
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 91,
                //        TenMenu = Language.GetText("thunhapcanhan"),
                //        DuongDan = "/ThuNhapCaNhan/ThuNhapCaNhanVP",
                //        Level = 2,
                //        MenuIDCha = 90,
                //        icon = "fa-money",
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 97,
                //        TenMenu = Language.GetText("cauhinhtinhluong"),
                //        DuongDan = "/ThamSoTinhLuong/Index",
                //        Level = 2,
                //        MenuIDCha = 90,
                //        icon = "fa-money",
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 92,
                //        TenMenu = Language.GetText("dieuchinhluong"),
                //        DuongDan = "/DieuChinhLuong/DieuChinhLuongVP",
                //        Level = 2,
                //        MenuIDCha = 90,
                //        icon = "fa-money",
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 93,
                //        TenMenu = Language.GetText("thuongphatphucap"),
                //        DuongDan = "/ThuongPhatNhanVien/Index",
                //        Level = 2,
                //        MenuIDCha = 90,
                //        icon = "fa-money",
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 94,
                //        TenMenu = Language.GetText("luongkhoan"),
                //        DuongDan = "/QLLuongKhoanNhanVien/Index",
                //        Level = 2,
                //        MenuIDCha = 90,
                //        icon = "fa-money",
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 97,
                //        TenMenu = Language.GetText("luongkhoanvp"),
                //        DuongDan = "/QLLuongKhoanNhanVien/LuongKhoanVP",
                //        Level = 2,
                //        MenuIDCha = 90,
                //        icon = "fa-money",
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 95,
                //        TenMenu = Language.GetText("tamungluong"),
                //        DuongDan = "/TamUngLuong/TamUngLuongVP",
                //        Level = 2,
                //        MenuIDCha = 90,
                //        icon = "fa-money",
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 96,
                //        TenMenu = Language.GetText("tamungluongcongtrinh"),
                //        DuongDan = "/TamUngLuong/TamUngLuongCT",
                //        Level = 2,
                //        MenuIDCha = 90,
                //        icon = "fa-money",
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 98,
                //        TenMenu = Language.GetText("thongkeluong"),
                //        DuongDan = "/ThongKeLuong/Index",
                //        Level = 2,
                //        MenuIDCha = 90,
                //        icon = "fa-money",
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 99,
                //        TenMenu = Language.GetText("thongketamungvanphong"),
                //        DuongDan = "/TamUngLuong/ThongKeTamUngLuongVanPhong",
                //        Level = 2,
                //        MenuIDCha = 90,
                //        icon = "fa-money",
                //    });
                //}

                //#endregion

                //if (accInfo.Roles.Contains(WebConst.HeThong) )
                //{
                //    #region Quản trị Hệ Thống
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 4,
                //        TenMenu = Language.GetText("quantrihethong"),
                //        DuongDan = "#",
                //        IsHead = true
                //    });

                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 41,
                //        TenMenu = Language.GetText("qphanquyenchucnang"),
                //        DuongDan = "/PhanQuyen/Index",
                //        Level = 1,
                //        icon = "fa fa-cogs",
                //        IsHead = false
                //    });


                //}
                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebNhanSu))
                //{
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 43,
                //        TenMenu = Language.GetText("phanquyenphongchat"),
                //        DuongDan = "/PhanQuyenPhongChat/Index",
                //        Level = 1,
                //        icon = "fa fa-cogs",
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 42,
                //        TenMenu = Language.GetText("phanquyenduyetcong"),
                //        DuongDan = "/PhanQuyenDuyetCong/Index",
                //        Level = 1,
                //        icon = "fa fa-cogs",
                //        IsHead = false,
                //    });
                //}
                //#endregion
                //#region Quản Lý Danh Mục
                //lstMenu.Add(new MenuModel
                //{
                //    MenuID = 2,
                //    TenMenu = Language.GetText("quanlydanhmuc"),
                //    DuongDan = "#",
                //    IsHead = true
                //});
                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebThietBi))
                //{
                //    //Thiết Bị
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 21,
                //        TenMenu = Language.GetText("thietbi"),
                //        DuongDan = "#",
                //        Level = 1,
                //        icon = "fa fa-steam",
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 215,
                //        TenMenu = Language.GetText("danhmucthietbi"),
                //        DuongDan = "/DMThietBi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 21,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 211,
                //        TenMenu = Language.GetText("nhomthietbi"),
                //        DuongDan = "/DMNhomThietBi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 21,
                //        IsHead = false
                //    });
                //    //lstMenu.Add(new MenuModel
                //    //{
                //    //    MenuID = 212,
                //    //    TenMenu = Language.GetText("phutungdikem"),
                //    //    DuongDan = "/DMNhomThietBiDiKem/Index",
                //    //    Level = 2,
                //    //    icon = "fa fa-steam",
                //    //    MenuIDCha = 21,
                //    //    IsHead = false
                //    //});
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 213,
                //        TenMenu = Language.GetText("hangbaohiem"),
                //        DuongDan = "/DMHangBaoHiem/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 21,
                //        IsHead = false
                //    });
                //    //lstMenu.Add(new MenuModel
                //    //{
                //    //    MenuID = 214,
                //    //    TenMenu = Language.GetText("hangmucbaoduong"),
                //    //    DuongDan = "/DMHangMucBaoDuong/Index",
                //    //    Level = 2,
                //    //    icon = "fa fa-steam",
                //    //    MenuIDCha = 21,
                //    //    IsHead = false
                //    //});
                //    //lstMenu.Add(new MenuModel
                //    //{
                //    //    MenuID = 215,
                //    //    TenMenu = Language.GetText("khoanggiobaoduong"),
                //    //    DuongDan = "/DMKhoangGioBaoDuong/Index",
                //    //    Level = 2,
                //    //    icon = "fa fa-steam",
                //    //    MenuIDCha = 21,
                //    //    IsHead = false
                //    //});
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 216,
                //        TenMenu = Language.GetText("hangsanxuat"),
                //        DuongDan = "/DMHangSanXuat/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 21,
                //        IsHead = false
                //    });
                //    //lstMenu.Add(new MenuModel
                //    //{
                //    //    MenuID = 217,
                //    //    TenMenu = Language.GetText("nhomphutungduphong"),
                //    //    DuongDan = "/DMNhomPhuTungDuPhong/Index",
                //    //    Level = 2,
                //    //    icon = "fa fa-steam",
                //    //    MenuIDCha = 21,
                //    //    IsHead = false
                //    //});
                //}
                ////CCDC
                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebCCDC))
                //{
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 25,
                //        TenMenu = Language.GetText("ccdc"),
                //        DuongDan = "#",
                //        Level = 1,
                //        icon = "fa fa-steam",
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 251,
                //        TenMenu = Language.GetText("nhomccdc"),
                //        DuongDan = "/DMNhomCCDC/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 25,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 252,
                //        TenMenu = Language.GetText("danhmucccdc"),
                //        DuongDan = "/DMCCDC/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 25,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 253,
                //        TenMenu = Language.GetText("danhmucccdcnhom2"),
                //        DuongDan = "/DMCCDC_V2/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 25,
                //        IsHead = false
                //    });
                //}
                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebPhongVatTu))
                //{
                //    // Vật Tư
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 22,
                //        TenMenu = Language.GetText("vattu"),
                //        DuongDan = "#",
                //        Level = 1,
                //        icon = "fa fa-wrench",
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 221,
                //        TenMenu = Language.GetText("danhmucvattu"),
                //        DuongDan = "/DMVatTu/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 22,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 222,
                //        TenMenu = Language.GetText("nhomvattu"),
                //        DuongDan = "/DMNhomVatTu/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 22,
                //        IsHead = false
                //    });
                //}

                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebNhanSu))
                //{
                //    // Hành Chính
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 23,
                //        TenMenu = Language.GetText("hanhchinh"),
                //        DuongDan = "#",
                //        Level = 1,
                //        icon = "fa fa-book",
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 231,
                //        TenMenu = Language.GetText("phongban"),
                //        DuongDan = "/QuanLyPhongBan/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 23,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 232,
                //        TenMenu = Language.GetText("nhomtaisan"),
                //        DuongDan = "/DMNhomTaiSan/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 23,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 233,
                //        TenMenu = Language.GetText("chucvu"),
                //        DuongDan = "/DMChucVu/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 23,
                //        IsHead = false
                //    });
                //}
                //if (accInfo.Roles.Contains(WebConst.HeThong) || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebPhongKTKH) || accInfo.Roles.Contains(WebConst.WebPhongVatTu) || accInfo.Roles.Contains(WebConst.WebCCDC))
                //{
                //    // Chung
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 24,
                //        TenMenu = Language.GetText("chung"),
                //        DuongDan = "#",
                //        Level = 1,
                //        icon = "fa fa-th-large",
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 241,
                //        TenMenu = Language.GetText("hangmucthicong"),
                //        DuongDan = "/DMHangMucThiCong/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 24,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 242,
                //        TenMenu = Language.GetText("danhmucdonvi"),
                //        DuongDan = "/DMDonVi/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 24,
                //        IsHead = false
                //    });
                //    //lstMenu.Add(new MenuModel
                //    //{
                //    //    MenuID = 243,
                //    //    TenMenu = Language.GetText("danhmucdoitac"),
                //    //    DuongDan = "/DMDoiTacDiThue/Index",
                //    //    Level = 2,
                //    //    icon = "fa fa-steam",
                //    //    MenuIDCha = 24,
                //    //    IsHead = false
                //    //});

                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 246,
                //        TenMenu = Language.GetText("danhmucdoi"),
                //        DuongDan = "/DMDoiThiCong/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 24,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 247,
                //        TenMenu = Language.GetText("danhmucnhomdichvu"),
                //        DuongDan = "/DMNhomDichVu/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 24,
                //        IsHead = false
                //    });
                //    //lstMenu.Add(new MenuModel
                //    //{
                //    //    MenuID = 247,
                //    //    TenMenu = Language.GetText("danhmucdauxe"),
                //    //    DuongDan = "/DMDauXe/Index",
                //    //    Level = 2,
                //    //    MenuIDCha = 24,
                //    //    IsHead = false
                //    //});
                //}
                //#endregion
                //if (accInfo.Roles.Contains(WebConst.HeThong)  || accInfo.Roles.Contains(WebConst.DingQu) || accInfo.Roles.Contains(WebConst.WebBanGiamDoc) || accInfo.Roles.Contains(WebConst.WebPhongKTKH))
                //{
                //    #region Quản Lý Dự Toán
                //    lstMenu.Add(new MenuModel

                //    {
                //        MenuID = 8,
                //        TenMenu = Language.GetText("quanlydutoan"),
                //        DuongDan = "#",
                //        IsHead = true
                //    });
                //    //Dự Toán Danh Mục
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 80,
                //        TenMenu = Language.GetText("danhmucdutoan"),
                //        DuongDan = "#",
                //        Level = 1,
                //        icon = "fa fa-th-large",
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 801,
                //        TenMenu = Language.GetText("loainhancong"),
                //        DuongDan = "/DMLoaiNhanCong/Index",
                //        Level = 2,
                //        MenuIDCha = 80,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 802,
                //        TenMenu = Language.GetText("chiphiphanbo"),
                //        DuongDan = "/DMChiPhiPhanBo/Index",
                //        Level = 2,
                //        MenuIDCha = 80,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 803,
                //        TenMenu = Language.GetText("loaivanchuyen"),
                //        DuongDan = "/DMLoaiVanChuyen/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 80,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 804,
                //        TenMenu = Language.GetText("chiphiqlda"),
                //        DuongDan = "/DMChiPhiQLDA/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 80,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 805,
                //        TenMenu = Language.GetText("chiphikhac"),
                //        DuongDan = "/DMChiPhiKhac/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 80,
                //        IsHead = false
                //    });
                //    //Định mức
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 81,
                //        TenMenu = Language.GetText("dinhmuc"),
                //        DuongDan = "#",
                //        Level = 1,
                //        icon = "fa fa-th-large",
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 811,
                //        TenMenu = Language.GetText("vattu"),
                //        DuongDan = "/DinhMucVatTu/Index",
                //        Level = 2,
                //        MenuIDCha = 81,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 812,
                //        TenMenu = Language.GetText("nhancong"),
                //        DuongDan = "/DinhMucNhanCong/Index",
                //        Level = 2,
                //        MenuIDCha = 81,
                //        IsHead = false
                //    }); lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 813,
                //        TenMenu = Language.GetText("suachuathietbi"),
                //        DuongDan = "/DinhMucSuaChuaThietBi/Index",
                //        Level = 2,
                //        MenuIDCha = 81,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 814,
                //        TenMenu = Language.GetText("dauthietbi"),
                //        DuongDan = "/DinhMucDauThietBi/Index",
                //        Level = 2,
                //        MenuIDCha = 81,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 815,
                //        TenMenu = Language.GetText("suachuaccdc"),
                //        DuongDan = "/DinhMucSuaChuaCCDC/Index",
                //        Level = 2,
                //        MenuIDCha = 81,
                //        IsHead = false
                //    });

                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 816,
                //        TenMenu = Language.GetText("loaivanchuyen"),
                //        DuongDan = "/DinhMucLoaiVanChuyen/Index",
                //        //Level = 2,
                //        MenuIDCha = 81,
                //        IsHead = false
                //    });
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 817,
                //        TenMenu = Language.GetText("chiphiphanbo"),
                //        DuongDan = "/DinhMucChiPhiPhanBo/Index",
                //        Level = 2,
                //        MenuIDCha = 81,
                //        IsHead = false
                //    });
                //    //lstMenu.Add(new MenuModel
                //    //{
                //    //    MenuID = 818,
                //    //    TenMenu = Language.GetText("chiphikhac"),
                //    //    DuongDan = "/DinhMucChiPhiKhac/Index",
                //    //    Level = 2,
                //    //    icon = "fa fa-steam",
                //    //    MenuIDCha = 81,
                //    //    IsHead = false,
                //    //});
                //    lstMenu.Add(new MenuModel
                //    {
                //        MenuID = 819,
                //        TenMenu = Language.GetText("chiphiqlda"),
                //        DuongDan = "/DinhMucChiPhiQLDA/Index",
                //        Level = 2,
                //        icon = "fa fa-steam",
                //        MenuIDCha = 81,
                //        IsHead = false
                //    });
                //}
                //#endregion
                //#region Tiện Ích
                //lstMenu.Add(new MenuModel
                //{
                //    MenuID = 5,
                //    TenMenu = Language.GetText("tienich"),
                //    DuongDan = "#",
                //    IsHead = true
                //});

                //lstMenu.Add(new MenuModel
                //{
                //    MenuID = 51,
                //    TenMenu = Language.GetText("tracuudanhba"),
                //    DuongDan = "/TraCuuDanhBa/Index",
                //    Level = 1,
                //    icon = "fa fa-search",
                //    IsHead = false,
                //});
                //lstMenu.Add(new MenuModel
                //{
                //    MenuID = 52,
                //    TenMenu = Language.GetText("donggopykien"),
                //    DuongDan = "/YKienDongGop/Index",
                //    Level = 1,
                //    icon = "fa fa-lightbulb-o",
                //    IsHead = false
                //});
                //#endregion


            }


            ////tìm menu active dựa vào đường dẫn
            if (lstMenu.Count == 0)
                return null;
            string currentUri = HttpContext.Current.Request.Url.AbsoluteUri;
            try
            {
                MenuModel mn = lstMenu.Where(m => currentUri.Contains(m.DuongDan)).SingleOrDefault();
                if (mn != null)
                {
                    mn.Class = "active";
                    if (mn.Level > 1)
                    {
                        MenuModel mncha = lstMenu.Where(m => m.MenuID == mn.MenuIDCha).SingleOrDefault();
                        mncha.Class = "active";
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return lstMenu;
        }


    }
}