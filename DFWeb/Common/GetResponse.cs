﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace DKKhamModule.Common
{
    public class GetResponse
    {
        public string getResponsePost(string url) 
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            //request.ContentLength = DATA.Length;
            StreamWriter requestWriter = new StreamWriter(request.GetRequestStream(), System.Text.Encoding.ASCII);
            //requestWriter.Write(DATA);
            requestWriter.Close();
            try
            {
                WebResponse webResponse = request.GetResponse();
                Stream webStream = webResponse.GetResponseStream();
                StreamReader responseReader = new StreamReader(webStream);
                string response = responseReader.ReadToEnd();
                responseReader.Close();
                webStream.Close();
                webResponse.Close();
                return response;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string getNewResponsePost(string url, string param) 
        {
            try
            {
                var nvcDocument = new NameValueCollection();
                nvcDocument.Add("param", param);
                string strJson = "";
                string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
                HttpWebRequest httpWebRequest2 = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest2.ContentType = "multipart/form-data; boundary=" + boundary;
                httpWebRequest2.Method = "POST";
                httpWebRequest2.KeepAlive = true;
                httpWebRequest2.Credentials =
                    System.Net.CredentialCache.DefaultCredentials;
                byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

                string formdataTemplate = "\r\n--" + boundary +
                                          "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";

                System.IO.Stream memStream = new System.IO.MemoryStream();
                foreach (string key in nvcDocument.Keys)
                {
                    string formitem = string.Format(formdataTemplate, key, nvcDocument[key]);
                    byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                    memStream.Write(formitembytes, 0, formitembytes.Length);
                }

                memStream.Write(boundarybytes, 0, boundarybytes.Length);
                memStream.Position = 0;
                byte[] tempBuffer = new byte[memStream.Length];
                memStream.Read(tempBuffer, 0, tempBuffer.Length);
                memStream.Close();
                Stream requestStream = httpWebRequest2.GetRequestStream();

                requestStream.Write(tempBuffer, 0, tempBuffer.Length);
                requestStream.Close();
                System.Net.ServicePointManager.Expect100Continue = false;
                WebResponse webResponse2 = httpWebRequest2.GetResponse();

                Stream stream2 = webResponse2.GetResponseStream();
                using (var reader = new StreamReader(stream2))
                {
                    strJson = reader.ReadToEnd().ToString();
                }
                return strJson;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string getNewResponsePostWithValue(string url, NameValueCollection param)
        {
            try
            {
                var nvcDocument = param;
                string strJson = "";
                string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
                HttpWebRequest httpWebRequest2 = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest2.ContentType = "multipart/form-data; boundary=" + boundary;
                httpWebRequest2.Method = "POST";
                httpWebRequest2.KeepAlive = true;
                httpWebRequest2.Credentials =
                    System.Net.CredentialCache.DefaultCredentials;
                byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

                string formdataTemplate = "\r\n--" + boundary +
                                          "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";

                System.IO.Stream memStream = new System.IO.MemoryStream();
                foreach (string key in nvcDocument.Keys)
                {
                    string formitem = string.Format(formdataTemplate, key, nvcDocument[key]);
                    byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                    memStream.Write(formitembytes, 0, formitembytes.Length);
                }

                memStream.Write(boundarybytes, 0, boundarybytes.Length);
                memStream.Position = 0;
                byte[] tempBuffer = new byte[memStream.Length];
                memStream.Read(tempBuffer, 0, tempBuffer.Length);
                memStream.Close();
                Stream requestStream = httpWebRequest2.GetRequestStream();

                requestStream.Write(tempBuffer, 0, tempBuffer.Length);
                requestStream.Close();
                System.Net.ServicePointManager.Expect100Continue = false;
                WebResponse webResponse2 = httpWebRequest2.GetResponse();

                Stream stream2 = webResponse2.GetResponseStream();
                using (var reader = new StreamReader(stream2))
                {
                    strJson = reader.ReadToEnd().ToString();
                }
                return strJson;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }

}