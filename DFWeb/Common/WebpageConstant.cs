﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace DFWeb.Common
{
    public class WebConst
    {
        //#region ----------- Quản lý nhóm quyền -----------

        public const string HeThong = "10";
        public const string DingQu = "2000";
        public const string WebThietBiXemTheoDoi = "18";
        public const string WebThietBi = "11";
        public const string WebKTKH = "12";
        public const string WebNhanSu = "16";
        public const string WebPhongKeToan = "17";
        public const string WebPhongDuAn = "13";
        public const string WebPhongVatTu = "14";
        public const string WebPhongKTKH = "12";
        public const string WebBanGiamDoc = "15";
        public const string WebCCDC = "21";
        public const string WebDeNghiSuaChua = "22";
        public const string NhapBCBanGiamDocRole = "23";
        public const string PhongXuatNhapKhau = "101";

        public const string QuanLyThuNhapRole = "15";
        public const string QLLeDong = "15";
        public const string QLDangKien = "15";
        public const string QUngLuong = "15";

        public const string BaoCaoLanhDao = "76";
        public const string BaoCaoCapDoiTruong = "77";
        public const string CapVanPhong = "78";

        // Nhận thông báo mobile
        public const int ThongBao_CongTrinh = 30;
        public const int ThongBao_VatTu = 31;
        public const int ThongBao_CCDC = 32;
        public const int ThongBao_HanhChinh = 33;
        public const int ThongBao_ThietBi = 34;

        public const int LaiMay = 5;

        public const string CONGTRINHDOITACDITHUE = "b0baeb25-1f1e-4fdf-8823-f6a8d5c6ff23";
        public const string CONGTRINHGUI = "dad564ac-6748-49e5-9347-cc4d08c9e833";
        public const string CONGTRINHTONG = "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF";
        public const string XUONGCONGTY = "1525AC01-0387-4D66-8FB5-B3A3DD15FC85";
        public const string CONGTRINHCHOVIEC = "00000000-0000-0000-0000-000000000000";

        public const string HOANHOADON = "B6F0E64F-DD46-458A-A635-6DA6184B4BE5";


        public const string LeDongID = "577948C2-54C5-47E8-B045-87B4828BB157";
        public const string DangKienID = "A8827C06-388B-44C2-B9F0-5283AFBB1636";
        public const string LeDongText = "Lê Đông";
        public const string DangKienText = "Đăng Kiên";
        public const string NhanVienBand = "983C3840-DA72-4963-8A07-505BC547F6B2";

        public static string SHOWIMGCCDC = ConfigurationManager.AppSettings["ShowImgCCDC"].ToString();
        public static string SHOWIMGCHAT = ConfigurationManager.AppSettings["ShowImgChat"].ToString();
        public static string SHOWIMGDN = ConfigurationManager.AppSettings["ShowImgDN"].ToString();
        public static string SHOWIMGTB = ConfigurationManager.AppSettings["ShowImgTB"].ToString();
        public static string SHOWIMGVT = ConfigurationManager.AppSettings["ShowImgVT"].ToString();
        public static string SHOWIMGNV = ConfigurationManager.AppSettings["ShowImgNV"].ToString();
        public static string SHOWIMGHOPDONG = ConfigurationManager.AppSettings["ShowImgHopDong"].ToString();
        public static string SHOWIMGCHITHI = ConfigurationManager.AppSettings["UrlUploadChiThi"].ToString();
        public static string PASSDUYETPHIEU = ConfigurationManager.AppSettings["pass_DuyetTamUngLuong"].ToString();
        public static string SiteName = ConfigurationManager.AppSettings["SiteName"].ToString();

        public static Guid DOITRUONG = Guid.Parse("F8CC4A9C-B040-4E16-8F19-1683DBC875EC");
        public static Guid TRUONGPHONG = Guid.Parse("3A8517A0-A721-4A7A-BE7E-E03B46760B79");

        //Nhóm dịch vụ
        public const int NHOMDICHVUTAICHINH = 5;
        public const int NHOMDICHVUTHIETBI = 2;
        public const int NHOMDICHVUCHUDAUTU = 6;
        public const int NHOMDICHVUCCDC = 1;
        public const int NHOMDICHVUVATTU = 3;
        public const int NHOMDICHVUVANCHUYEN = 4;
        public const int NHOMDICHVUKHAC = 7;
    }
}