﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using DF.DBMapping.Models;
using System.Net.Mail;
using System.Net;
using Resources;
using System.Configuration;
using System.Data;
using System.IO;
using DFWeb.Controllers;
using DF.DataAccess;
using System.Globalization;
using System.Data.OleDb;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Xml;
using System.Xml.Xsl;
using DFWeb.Models;
using FlexCel.XlsAdapter;
using FlexCel.Report;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using DFWeb.Common;
//using DFWeb.Models.BaoCaoModels;

namespace DFWeb.Utils
{

    public static class Utilities
    {
        [DllImport("user32.dll")]

        
        static extern int GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);
        /// <summary>
        /// AnhNT - Chuyển đổ số sang số tiền Việt Nam
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string NumberToTextVN(double total)
        {
            try
            {
                string chan = "";
                string rs = "";
                total = Math.Round(total, 0);
                string[] ch = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
                string[] rch = { "lẻ", "mốt", "", "", "", "lăm" };
                string[] u = { "", "mươi", "trăm", "ngàn", "", "", "triệu", "", "", "tỷ", "", "", "ngàn", "", "", "triệu" };
                string nstr = total.ToString();
                // string set = nstr.Substring(nstr.Length - 3, 2);
                if (nstr.Substring(nstr.Length - 3, 2) == "00") { chan = " chẵn"; }

                int[] n = new int[nstr.Length];
                int len = n.Length;
                for (int i = 0; i < len; i++)
                {
                    n[len - 1 - i] = Convert.ToInt32(nstr.Substring(i, 1));
                }

                for (int i = len - 1; i >= 0; i--)
                {
                    if (i % 3 == 2)// số 0 ở hàng trăm
                    {
                        if (n[i] == 0 && n[i - 1] == 0 && n[i - 2] == 0) continue;//nếu cả 3 số là 0 thì bỏ qua không đọc
                    }
                    else if (i % 3 == 1) // số ở hàng chục
                    {
                        if (n[i] == 0)
                        {
                            if (n[i - 1] == 0) { continue; }// nếu hàng chục và hàng đơn vị đều là 0 thì bỏ qua.
                            else
                            {
                                rs += " " + rch[n[i]]; continue;// hàng chục là 0 thì bỏ qua, đọc số hàng đơn vị
                            }
                        }
                        if (n[i] == 1)//nếu số hàng chục là 1 thì đọc là mười
                        {
                            rs += " mười"; continue;
                        }
                    }
                    else if (i != len - 1)// số ở hàng đơn vị (không phải là số đầu tiên)
                    {
                        if (n[i] == 0)// số hàng đơn vị là 0 thì chỉ đọc đơn vị
                        {
                            if (i + 2 <= len - 1 && n[i + 2] == 0 && n[i + 1] == 0) continue;
                            rs += " " + (i % 3 == 0 ? u[i] : u[i % 3]);
                            continue;
                        }
                        if (n[i] == 1)// nếu là 1 thì tùy vào số hàng chục mà đọc: 0,1: một / còn lại: mốt
                        {
                            rs += " " + ((n[i + 1] == 1 || n[i + 1] == 0) ? ch[n[i]] : rch[n[i]]);
                            rs += " " + (i % 3 == 0 ? u[i] : u[i % 3]);
                            continue;
                        }
                        if (n[i] == 5) // cách đọc số 5
                        {
                            if (n[i + 1] != 0) //nếu số hàng chục khác 0 thì đọc số 5 là lăm
                            {
                                rs += " " + rch[n[i]];// đọc số 
                                rs += " " + (i % 3 == 0 ? u[i] : u[i % 3]);// đọc đơn vị
                                continue;
                            }
                        }
                    }

                    rs += (rs == "" ? " " : " ") + ch[n[i]];// đọc số
                    rs += " " + (i % 3 == 0 ? u[i] : u[i % 3]);// đọc đơn vị
                }
                if (rs[rs.Length - 1] != ' ')
                    rs += " đồng" + chan;
                else
                    rs += "đồng" + chan;

                if (rs.Length > 2)
                {
                    string rs1 = rs.Substring(0, 2);
                    rs1 = rs1.ToUpper();
                    rs = rs.Substring(2);
                    rs = rs1 + rs;
                }
                return rs.Trim().Replace("lẻ,", "lẻ").Replace("mươi,", "mươi").Replace("trăm,", "trăm").Replace("mười,", "mười");
            }
            catch (Exception ex)
            {
                return "";
            }

        }
        public static string GetUserNameFromToken(string token)
        {
            try
            {
                string key = Encoding.UTF8.GetString(Convert.FromBase64String(token));

                string[] parts = key.Split(new char[] { ':' });
                if (parts.Length == 3)
                {
                    return parts[1];
                }
            }
            catch (Exception e)
            {
                //Log.Error("Authorize/GetUserNameFromToken----Message: " + e.Message + "Tray: " + e.StackTrace);
            }
            return null;
        }

        public static string GetRuleTypeFromToken(string token)
        {
            UnitOfWork unitofwork = new UnitOfWork();
            try
            {
                string key = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                bool BCLanhDao = false;
                bool BCDoiTruong = false;
                bool BCCapVanPhong = false;
                string[] parts = key.Split(new char[] { ':' });
                if (parts.Length == 3)
                {
                    var lst = unitofwork.NhanViens.LayDSRuleTypeByUserName(parts[1]);
                    foreach(var item in lst)
                    {
                        if (item.RuleType == null) continue;
                        if (WebConst.BaoCaoLanhDao == item.RuleType.Value.ToString())
                        {
                            BCLanhDao = true;
                            continue;
                        }

                        if(WebConst.CapVanPhong == item.RuleType.Value.ToString())
                        {
                            BCCapVanPhong = true;
                            continue;
                        }

                        if (WebConst.BaoCaoCapDoiTruong == item.RuleType.Value.ToString())
                        {
                            BCDoiTruong = true;
                            continue;
                        }
                    }

                    if (BCLanhDao)
                    {
                        return WebConst.BaoCaoLanhDao;
                    }
                    if (BCCapVanPhong)
                    {
                        return WebConst.CapVanPhong;
                    }
                    else if(BCDoiTruong)
                    {
                        return WebConst.BaoCaoCapDoiTruong;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception e)
            {
                return "";
            }
            return "";
        }

        //public static string GetDoiThiCongFromToken(string token)
        //{
        //    UnitOfWork unitofwork = new UnitOfWork();
        //    try
        //    {
        //        string key = Encoding.UTF8.GetString(Convert.FromBase64String(token));
        //        string[] parts = key.Split(new char[] { ':' });
        //        if (parts.Length == 3)
        //        {
        //            var lst = unitofwork.NhanViens.LayDSDoiThiCongByUserName(parts[1]).FirstOrDefault();
        //            return lst.DoiThiCongID.Value.ToString();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return "";
        //    }
        //    return "";
        //}

        public static int TrongKhoang(int so, int dauKhoang, int cuoiKhoang)
        {
            // hàm dùng để tính xem một số có bao nhiêu phần của nó lọt vào giữa đầu và cuối khoảng
            // nếu số nhỏ hơn đầu khoảng thì hàm trả về giê rô

            if (so <= dauKhoang) return 0;
            if (so > cuoiKhoang) return cuoiKhoang - dauKhoang;
            return so - dauKhoang;
        }
        /// <summary>
        /// KienMT - Tính lương nhân viên theo tháng
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string unsign(string text)
        {
            var regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string strFormD = text.Normalize(NormalizationForm.FormD);
            return regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static string convertCodeData(String codeData)
        {
            if (codeData != null && codeData.Trim() != "")
            {
                for (int i = 0; i < codeData.Length; i++)
                {
                    char c = codeData[i];
                    if (!(c == 45                      // '-'
                        || (c >= 48 && c <= 57)     // 0 -> 9
                        || (c >= 65 && c <= 90)     // A -> Z
                        || (c >= 97 && c <= 122)    // a -> z
                        ))
                    {
                        codeData.Remove(i, 1);
                    }
                }
                codeData = codeData.Trim();
            }
            return codeData;
        }






        private static char[] unichars = {
            'Ă','Â','Ê','Ô','Ơ','Ư','Đ','ă','â','ê',
            'ô','ơ','ư','đ','n','n','n','n','n','n',
            'à','ả','ã','á','ạ','n','ằ','ẳ','ẵ','ắ',
            'n','n','n','n','n','n','n','ặ','ầ','ẩ',
            'ẫ','ấ','ậ','è','n','ẻ','ẽ','é','ẹ','ề',
            'ể','ễ','ế','ệ','ì','ỉ','n','n','n','ĩ',
            'í','ị','ò','n','ỏ','õ','ó','ọ','ồ','ổ',
            'ỗ','ố','ộ','ờ','ở','ỡ','ớ','ợ','ù','n',
            'ủ','ũ','ú','ụ','ừ','ử','ữ','ứ','ự','ỳ',
            'ỷ','ỹ','ý','ỵ'
        };



        /// <summary>
        /// created by: HienDM1
        /// cretae date: 14/05/2015
        /// hàm chuyển đổi chuỗi là unicode hay tcvn3
        /// </summary>
        /// <param name="input">chuỗi cần kiểm tra</param>
        /// <returns>null: không thể chuyển đổi</returns>
        public static string ConvertUnicode(string input)
        {
            // HienDM1 sử dụng cách convert font mới 
            char[] chars = input.ToCharArray();
            for (int i = 0; i < chars.Length; i++)
            {
                char e = chars[i];
                if ((e >= 161 && e <= 174) || (e >= 181 && e <= 190 && e != 186) ||
                    (e >= 198 && e <= 216 && e != 205) || (e >= 220 && e <= 254 && e != 240 && e != 224))
                {
                    chars[i] = unichars[chars[i] - 161];
                }
            }
            return new string(chars);
        }

        /// <summary>
        /// created by: HienDM1
        /// cretae date: 14/05/2015
        /// hàm kiểm tra chuỗi là unicode hay tcvn3
        /// </summary>
        /// <param name="input">chuỗi cần kiểm tra</param>
        /// <returns>1: unicode, 0: tcvn3, -1: không xác định</returns>
        public static int checkUnicodeString(string input)
        {
            char[] chars = input.ToCharArray();
            int[] arrayUnknown = {161, 173, 200, 201, 202, 204, 208, 210, 211, 213, 212, 221,
                                  225, 226, 227, 232, 233, 234, 236, 237, 242, 243,
                                  244, 245, 249, 250, 253};
            for (int i = 0; i < chars.Length; i++)
            {
                char e = chars[i];
                if ((e >= 161 && e <= 174) || (e >= 181 && e <= 190 && e != 186) ||
                    (e >= 198 && e <= 216 && e != 205) || (e >= 220 && e <= 254 && e != 224 && e != 240))
                {
                    if (arrayUnknown.Contains(e))
                    {
                        // Không xác định
                        return -1;
                    }
                    else
                    {
                        // là tcvn3
                        return 0;
                    }
                }
            }
            return 1;
        }



        /// <summary>
        /// created by: tamhd
        /// cretae date: 6/11/2014
        /// hàm chuyển datatable sang json
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public static string DataTableToJSON(DataTable dt)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            object[] arr = new object[dt.Rows.Count + 1];
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                arr[i] = dt.Rows[i].ItemArray;
            }
            dict.Add("table", arr);
            JavaScriptSerializer json = new JavaScriptSerializer();
            return json.Serialize(dict);
        }

        #region Excel Utils
        /// <summary>
        /// dinhlv6
        /// 13/3/2015
        /// export file excel tu dataset voi nhieu sheet  
        /// </summary>
        /// <param name="source"></param>
        /// <param name="fileName">duong dan luu file</param>
        public static void exportToExcel(DataSet source, string fileName)
        {
            NumberStyles style1 = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign;
            CultureInfo culture1 = CultureInfo.CreateSpecificCulture("en-US");
            System.IO.StreamWriter excelDoc;
            excelDoc = new System.IO.StreamWriter(fileName);
            // cau truc file xml dinh dang cho xuat excel 
            const string startExcelXML = "<xml version>\r\n<Workbook " +
                  "xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\r\n" +
                  " xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n " +
                  "xmlns:x=\"urn:schemas-    microsoft-com:office:" +
                  "excel\"\r\n xmlns:ss=\"urn:schemas-microsoft-com:" +
                  "office:spreadsheet\">\r\n <Styles>\r\n " +
                  "<Style ss:ID=\"Default\" ss:Name=\"Normal\">\r\n " +
                  "<Alignment ss:Vertical=\"Bottom\"/>\r\n <Borders/>" +
                  "\r\n <Font/>\r\n <Interior/>\r\n <NumberFormat/>" +
                  "\r\n <Protection/>\r\n </Style>\r\n " +
                  "<Style ss:ID=\"BoldColumn\">\r\n <Font " +
                  "x:Family=\"Swiss\" ss:Bold=\"1\"/>\r\n </Style>\r\n " +
                  "<Style     ss:ID=\"StringLiteral\">\r\n <NumberFormat" +
                  " ss:Format=\"@\"/>\r\n </Style>\r\n <Style " +
                  "ss:ID=\"Decimal\">\r\n <NumberFormat " +
                  "ss:Format=\"0.0000\"/>\r\n </Style>\r\n " +
                  "<Style ss:ID=\"Integer\">\r\n <NumberFormat " +
                  "ss:Format=\"0\"/>\r\n </Style>\r\n <Style " +
                  "ss:ID=\"DateLiteral\">\r\n <NumberFormat " +
                  "ss:Format=\"mm/dd/yyyy;@\"/>\r\n </Style>\r\n " +
                  "</Styles>\r\n ";
            const string endExcelXML = "</Workbook>";

            int rowCount = 0;
            int sheetCount = 1;

            excelDoc.Write(startExcelXML);
            for (int tb = 0; tb < source.Tables.Count; tb++)
            {
                excelDoc.Write("<Worksheet ss:Name=\"" + source.Tables[tb].TableName.Replace("$", "") + "\">");
                excelDoc.Write("<Table>");
                excelDoc.Write("<Row>");
                for (int x = 0; x < source.Tables[tb].Columns.Count; x++)
                {
                    excelDoc.Write("<Cell ss:StyleID=\"BoldColumn\"><Data ss:Type=\"String\">");
                    excelDoc.Write(source.Tables[tb].Columns[x].ColumnName);
                    excelDoc.Write("</Data></Cell>");
                }
                excelDoc.Write("</Row>");
                foreach (DataRow x in source.Tables[tb].Rows)
                {
                    rowCount++;
                    //if the number of rows is > 64000 create a new page to continue output
                    if (rowCount == 64000)
                    {
                        rowCount = 0;
                        excelDoc.Write("</Table>");
                        excelDoc.Write(" </Worksheet>");
                        excelDoc.Write("<Worksheet ss:Name=\"" + source.Tables[tb].TableName.Replace("$", "") + sheetCount + "\">");
                        excelDoc.Write("<Table>");
                        sheetCount++;
                    }
                    excelDoc.Write("<Row>"); //ID=" + rowCount + "
                    decimal dValue = 0;
                    for (int y = 0; y < source.Tables[tb].Columns.Count; y++)
                    {
                        System.Type rowType;
                        rowType = x[y].GetType();
                        if (decimal.TryParse(x[y].ToString(), style1, culture1, out dValue))
                        {
                            rowType = System.Type.GetType("System.Int64");
                        }
                        switch (rowType.ToString())
                        {
                            case "System.String":
                                string XMLstring = x[y].ToString();
                                XMLstring = XMLstring.Trim();
                                XMLstring = XMLstring.Replace("&", "&");
                                XMLstring = XMLstring.Replace(">", ">");
                                XMLstring = XMLstring.Replace("<", "<");
                                excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\">" +
                                               "<Data ss:Type=\"String\">");
                                excelDoc.Write(XMLstring);
                                excelDoc.Write("</Data></Cell>");
                                break;
                            case "System.DateTime":
                                //Excel has a specific Date Format of YYYY-MM-DD followed by  
                                //the letter 'T' then hh:mm:sss.lll Example 2005-01-31T24:01:21.000
                                //The Following Code puts the date stored in XMLDate 
                                //to the format above
                                DateTime XMLDate = (DateTime)x[y];
                                string XMLDatetoString = ""; //Excel Converted Date
                                XMLDatetoString = XMLDate.Year.ToString() +
                                     "-" +
                                     (XMLDate.Month < 10 ? "0" +
                                     XMLDate.Month.ToString() : XMLDate.Month.ToString()) +
                                     "-" +
                                     (XMLDate.Day < 10 ? "0" +
                                     XMLDate.Day.ToString() : XMLDate.Day.ToString()) +
                                     "T" +
                                     (XMLDate.Hour < 10 ? "0" +
                                     XMLDate.Hour.ToString() : XMLDate.Hour.ToString()) +
                                     ":" +
                                     (XMLDate.Minute < 10 ? "0" +
                                     XMLDate.Minute.ToString() : XMLDate.Minute.ToString()) +
                                     ":" +
                                     (XMLDate.Second < 10 ? "0" +
                                     XMLDate.Second.ToString() : XMLDate.Second.ToString()) +
                                     ".000";
                                excelDoc.Write("<Cell ss:StyleID=\"DateLiteral\">" +
                                             "<Data ss:Type=\"DateTime\">");
                                excelDoc.Write(XMLDatetoString);
                                excelDoc.Write("</Data></Cell>");
                                break;
                            case "System.Boolean":
                                excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\">" +
                                            "<Data ss:Type=\"String\">");
                                excelDoc.Write(x[y].ToString());
                                excelDoc.Write("</Data></Cell>");
                                break;
                            case "System.Int16":
                            case "System.Int32":
                            case "System.Int64":
                            case "System.Byte":
                                excelDoc.Write("<Cell ss:StyleID=\"Integer\">" +
                                        "<Data ss:Type=\"Number\">");
                                excelDoc.Write(x[y].ToString());
                                excelDoc.Write("</Data></Cell>");
                                break;
                            case "System.Decimal":
                            case "System.Double":
                                excelDoc.Write("<Cell ss:StyleID=\"Decimal\">" +
                                      "<Data ss:Type=\"Number\">");
                                excelDoc.Write(x[y].ToString());
                                excelDoc.Write("</Data></Cell>");
                                break;
                            case "System.DBNull":
                                excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\">" +
                                      "<Data ss:Type=\"String\">");
                                excelDoc.Write("");
                                excelDoc.Write("</Data></Cell>");
                                break;
                            default:
                                throw (new Exception(rowType.ToString() + " not handled."));
                        }
                    }
                    excelDoc.Write("</Row>");
                }
                excelDoc.Write("</Table>");
                excelDoc.Write(" </Worksheet>");
            }
            excelDoc.Write(endExcelXML);
            excelDoc.Close();
        }
        /// <summary>
        /// created by: tamhd
        /// create date: 14/1/2014
        /// hàm chuyển từ datatable thành file excel
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="urlDown"></param>
        public static void ExportToExcel(DataTable dt, string urlDown)
        {
            try
            {
                //open file
                StreamWriter wr = new StreamWriter(HttpContext.Current.Request.PhysicalApplicationPath + urlDown, true, Encoding.Unicode);
                try
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        wr.Write(dt.Columns[i].ToString().ToUpper() + "\t");
                    }
                    wr.WriteLine();
                    //write rows to excel file
                    for (int i = 0; i < (dt.Rows.Count); i++)
                    {
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            if (dt.Rows[i][j] != null)
                            {
                                wr.Write(Convert.ToString(dt.Rows[i][j]) + "\t");
                            }
                            else
                            {
                                wr.Write("\t");
                            }
                        }
                        //go to next line
                        wr.WriteLine();
                    }
                    //close file
                    wr.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Doc du lieu trong file Excel va tra ve DataSet
        /// </summary>
        /// <param name="FileName">Duong dan file</param>
        /// <param name="hasHeaders">Dung de xac dinh bien HDR trong Excel Connection String</param>
        /// <param name="iColumnCount">So cot can lay trong 1 Sheet cua file Excel.</param>
        /// <returns></returns>
        public static DataSet ReadExcelToDataSet(string FileName, bool hasHeaders)//, int iColumnCount = 250)
        {
            try
            {
                string HDR = hasHeaders ? "Yes" : "No";
                string strConn;
                if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                else
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";

                DataSet output = new DataSet();

                using (OleDbConnection conn = new OleDbConnection(strConn))
                {
                    conn.Open();

                    DataTable schemaTable = conn.GetOleDbSchemaTable(
                        OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    foreach (DataRow schemaRow in schemaTable.Rows)
                    {
                        string sheet = schemaRow["TABLE_NAME"].ToString();

                        if (sheet.EndsWith("$") || sheet.EndsWith("$'"))
                        {
                            try
                            {
                                //string sExcelColumnName = GetExcelEndDataColumn(iColumnCount);
                                //OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheet + "A:" + sExcelColumnName + "]", conn);
                                OleDbCommand cmd = new OleDbCommand("select * from [" + sheet + "]", conn);
                                cmd.CommandType = CommandType.Text;
                                DataTable outputTable = new DataTable(sheet);
                                output.Tables.Add(outputTable);
                                OleDbDataAdapter adt = new OleDbDataAdapter(cmd);
                                adt.Fill(outputTable);
                                adt.Dispose();
                                cmd.Dispose();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message + string.Format("Sheet:{0}.File:F{1}", sheet, FileName), ex);
                            }
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                }
                return output;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable ReadExcelToDataTable(string FileName, bool hasHeaders, int iColumnCount = 250)
        {
            try
            {
                string HDR = hasHeaders ? "Yes" : "No";
                string strConn;
                if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                else
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";

                DataTable outputTable = new DataTable();

                using (OleDbConnection conn = new OleDbConnection(strConn))
                {
                    conn.Open();

                    DataTable schemaTable = conn.GetOleDbSchemaTable(
                        OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    foreach (DataRow schemaRow in schemaTable.Rows)
                    {
                        string sheet = schemaRow["TABLE_NAME"].ToString();

                        if (sheet.EndsWith("$") || sheet.EndsWith("$'"))
                        {
                            try
                            {
                                string sExcelColumnName = GetExcelEndDataColumn(iColumnCount);
                                OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheet + "A:" + sExcelColumnName + "]", conn);
                                cmd.CommandType = CommandType.Text;
                                outputTable = new DataTable(sheet);

                                OleDbDataAdapter adt = new OleDbDataAdapter(cmd);
                                adt.Fill(outputTable);
                                adt.Dispose();
                                cmd.Dispose();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message + string.Format("Sheet:{0}.File:F{1}", sheet, FileName), ex);
                            }
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                }
                return outputTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetExcelEndDataColumn(int iNumberOfColumn)
        {
            if (iNumberOfColumn <= 250 && iNumberOfColumn > 0)// 250 la so cot lon nhat trong DataTable
            {
                string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                string sChar1, sChar2;
                int iQuotient = iNumberOfColumn / 26;
                int iSurplus = iNumberOfColumn % 26;

                if (iSurplus == 0)
                {
                    iQuotient--;
                    iSurplus = 26;
                }

                sChar1 = iQuotient <= 0 ? "" : characters.Substring(iQuotient - 1, 1);
                sChar2 = characters.Substring(iSurplus - 1, 1);
                return sChar1 + sChar2;
            }
            else
            {
                return null;
            }
        }
        #endregion


        //Biến lưu giá trị URL đầu tiên
        public static string getUrlDefault = "";
        /// <summary>
        /// Chuyển đổi ngày tháng từ string mm/dd/yyyy sang date
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static DateTime? ConvetDateVNToDate(string strDate)
        {
            if (strDate == null || strDate == "")
            {
                return null;
            }
            else
            {
                try
                {
                    strDate = strDate.Trim();
                    if (String.IsNullOrEmpty(strDate)) return DateTime.Today;
                    string[] strs = strDate.Split("/".ToCharArray());

                    int day = Convert.ToInt32(strs[0]);
                    int month = Convert.ToInt32(strs[1]);
                    int year = Convert.ToInt32(strs[2]);
                    try { return new DateTime(year, month, day); }
                    catch { return new DateTime(year, day, month); }
                }
                catch
                {
                    return null;
                }

            }
        }
        public static DateTime ConvetDateVNToDateTrue(string strDate)
        {
            if (strDate == null || strDate == "")
            {
                return DateTime.Now;
            }
            else
            {
                try
                {
                    strDate = strDate.Trim();
                    if (String.IsNullOrEmpty(strDate)) return DateTime.Today;
                    string[] strs = strDate.Split("/".ToCharArray());

                    int day = Convert.ToInt32(strs[0]);
                    int month = Convert.ToInt32(strs[1]);
                    int year = Convert.ToInt32(strs[2]);
                    try { return new DateTime(year, month, day); }
                    catch { return new DateTime(year, day, month); }
                }
                catch
                {
                    return DateTime.Now;
                }

            }
        }
        public static string ToString(object obj)
        {
            try
            {
                return Convert.ToString(obj);
            }
            catch
            {
                return "";
            }
        }
        public static DateTime ToDateTime(object obj, string strFormat)
        {
            try
            {
                string dtString = ToString(obj);
                string[] arr = new string[2];
                DateTime dt = DateTime.MinValue;

                if (dtString.IndexOf("/") > -1)
                {
                    arr = dtString.Split('/');
                }
                else if (dtString.IndexOf("-") > -1)
                {
                    arr = dtString.Split('-');
                }
                else
                {
                    return dt;
                }

                switch (strFormat)
                {
                    case "dd/mm/yyyy":
                    case "dd-mm-yyyy":
                        dt = ToDateTime(arr[1] + "/" + arr[0] + "/" + arr[2]);
                        break;
                    case "yyyy/mm/dd":
                    case "yyyy-mm-dd":
                        dt = ToDateTime(arr[1] + "/" + arr[2] + "/" + arr[0]);
                        break;
                    case "yyyy/dd/mm":
                    case "yyyy-dd-mm":
                        dt = ToDateTime(arr[2] + "/" + arr[1] + "/" + arr[0]);
                        break;
                    default:
                        dt = ToDateTime(obj);
                        break;
                }
                return dt;
            }
            catch
            {
                return DateTime.MinValue;
            }
        }
        public static DateTime ToDateTime(object obj)
        {
            try
            {
                return Convert.ToDateTime(obj);
            }
            catch
            {
                return DateTime.Today;
            }
        }
        /// <summary>
        /// Chuyển đổi ngày tháng từ string dd/mm/yyyy sang date
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static DateTime? ConvetDateVNToDates(string strDate)
        {
            if (strDate == null || strDate == "")
            {
                return null;
            }
            else
            {
                try
                {
                    strDate = strDate.Trim();
                    if (String.IsNullOrEmpty(strDate)) return DateTime.Today;
                    string[] strs = strDate.Split("/".ToCharArray());

                    int day = Convert.ToInt32(strs[0]);
                    int month = Convert.ToInt32(strs[1]);
                    int year = Convert.ToInt32(strs[2]);
                    try { return new DateTime(year, month, day); }
                    catch { return new DateTime(year, day, month); }
                }
                catch
                {
                    return null;
                }

            }
        }
        public static DateTime? sysDate()
        {
            return DateTime.Now;
        }
        public static DateTime GetDateTime()
        {
            return DateTime.Now;
        }
        /// <summary>
        /// Thực hiện cắt bỏ các dấu cách thừa ở 2 đầu đối với các trường kiểu string
        /// </summary>
        /// <param name="obj"></param>
        public static void TrimObject(object obj)
        {
            Type t = obj.GetType();
            PropertyInfo[] ps = t.GetProperties();

            foreach (PropertyInfo p in ps)
            {
                if (p.PropertyType == typeof(string) || p.PropertyType == typeof(String))
                {
                    string str = (string)p.GetValue(obj, null);
                    str = str == null ? null : str.Trim();
                    p.SetValue(obj, str, null);
                }
            }
        }
        public static string htmlEndCode(string val)
        {
            string trueEncode = "";
            if (val == null)
                return trueEncode;
            string encode = HttpUtility.HtmlEncode(val.ToString());
            if (encode.Contains("&#192;"))
                encode = encode.Replace("&#192;", "À");
            if (encode.Contains("&#193;"))
                encode = encode.Replace("&#193;", "Á");
            if (encode.Contains("&#194;"))
                encode = encode.Replace("&#194;", "Â");
            if (encode.Contains("&#195;"))
                encode = encode.Replace("&#195;", "Ã");
            if (encode.Contains("&#200;"))
                encode = encode.Replace("&#200;", "È");
            if (encode.Contains("&#201;"))
                encode = encode.Replace("&#201;", "É");
            if (encode.Contains("&#202;"))
                encode = encode.Replace("&#202;", "Ê");
            if (encode.Contains("&#204;"))
                encode = encode.Replace("&#204;", "Ì");
            if (encode.Contains("&#205;"))
                encode = encode.Replace("&#205;", "Í");
            if (encode.Contains("&#208;"))
                encode = encode.Replace("&#208;", "Đ");
            if (encode.Contains("&#210;"))
                encode = encode.Replace("&#210;", "Ò");
            if (encode.Contains("&#211;"))
                encode = encode.Replace("&#211;", "Ó");
            if (encode.Contains("&#212;"))
                encode = encode.Replace("&#212;", "Ô");
            if (encode.Contains("&#213;"))
                encode = encode.Replace("&#213;", "Õ");
            if (encode.Contains("&#217;"))
                encode = encode.Replace("&#217;", "Ù");
            if (encode.Contains("&#218;"))
                encode = encode.Replace("&#218;", "Ú");
            if (encode.Contains("&#221;"))
                encode = encode.Replace("&#221;", "Ý");
            if (encode.Contains("&#224;"))
                encode = encode.Replace("&#224;", "à");
            if (encode.Contains("&#225;"))
                encode = encode.Replace("&#225;", "á");
            if (encode.Contains("&#226;"))
                encode = encode.Replace("&#226;", "â");
            if (encode.Contains("&#227;"))
                encode = encode.Replace("&#227;", "ã");
            if (encode.Contains("&amp;#7841;"))
                encode = encode.Replace("&amp;#7841;", "ạ");
            if (encode.Contains("&#232;"))
                encode = encode.Replace("&#232;", "è");
            if (encode.Contains("&#233;"))
                encode = encode.Replace("&#233;", "é");
            if (encode.Contains("&#234;"))
                encode = encode.Replace("&#234;", "ê");
            if (encode.Contains("&#236;"))
                encode = encode.Replace("&#236;", "ì");
            if (encode.Contains("&#237;"))
                encode = encode.Replace("&#237;", "í");
            if (encode.Contains("&#242;"))
                encode = encode.Replace("&#242;", "ò");
            if (encode.Contains("&#243;"))
                encode = encode.Replace("&#243;", "ó");
            if (encode.Contains("&#244;"))
                encode = encode.Replace("&#244;", "ô");
            if (encode.Contains("&#245;"))
                encode = encode.Replace("&#245;", "õ");
            if (encode.Contains("&#249;"))
                encode = encode.Replace("&#249;", "ù");
            if (encode.Contains("&#250;"))
                encode = encode.Replace("&#250;", "ú");
            if (encode.Contains("&#253;"))
                encode = encode.Replace("&#253;", "ý");

            trueEncode = encode;
            return trueEncode;
        }
        public static string htmlEndCode(object obj)
        {
            string trueEncode = "";
            if (obj == null)
                return trueEncode;
            string encode = HttpUtility.HtmlEncode(obj.ToString());
            if (encode.Contains("&#192;"))
                encode = encode.Replace("&#192;", "À");
            if (encode.Contains("&#193;"))
                encode = encode.Replace("&#193;", "Á");
            if (encode.Contains("&#194;"))
                encode = encode.Replace("&#194;", "Â");
            if (encode.Contains("&#195;"))
                encode = encode.Replace("&#195;", "Ã");
            if (encode.Contains("&#200;"))
                encode = encode.Replace("&#200;", "È");
            if (encode.Contains("&#201;"))
                encode = encode.Replace("&#201;", "É");
            if (encode.Contains("&#202;"))
                encode = encode.Replace("&#202;", "Ê");
            if (encode.Contains("&#204;"))
                encode = encode.Replace("&#204;", "Ì");
            if (encode.Contains("&#205;"))
                encode = encode.Replace("&#205;", "Í");
            if (encode.Contains("&#208;"))
                encode = encode.Replace("&#208;", "Đ");
            if (encode.Contains("&#210;"))
                encode = encode.Replace("&#210;", "Ò");
            if (encode.Contains("&#211;"))
                encode = encode.Replace("&#211;", "Ó");
            if (encode.Contains("&#212;"))
                encode = encode.Replace("&#212;", "Ô");
            if (encode.Contains("&#213;"))
                encode = encode.Replace("&#213;", "Õ");
            if (encode.Contains("&#217;"))
                encode = encode.Replace("&#217;", "Ù");
            if (encode.Contains("&#218;"))
                encode = encode.Replace("&#218;", "Ú");
            if (encode.Contains("&#221;"))
                encode = encode.Replace("&#221;", "Ý");
            if (encode.Contains("&#224;"))
                encode = encode.Replace("&#224;", "à");
            if (encode.Contains("&#225;"))
                encode = encode.Replace("&#225;", "á");
            if (encode.Contains("&#226;"))
                encode = encode.Replace("&#226;", "â");
            if (encode.Contains("&#227;"))
                encode = encode.Replace("&#227;", "ã");
            if (encode.Contains("&amp;#7841;"))
                encode = encode.Replace("&amp;#7841;", "ạ");
            if (encode.Contains("&#232;"))
                encode = encode.Replace("&#232;", "è");
            if (encode.Contains("&#233;"))
                encode = encode.Replace("&#233;", "é");
            if (encode.Contains("&#234;"))
                encode = encode.Replace("&#234;", "ê");
            if (encode.Contains("&#236;"))
                encode = encode.Replace("&#236;", "ì");
            if (encode.Contains("&#237;"))
                encode = encode.Replace("&#237;", "í");
            if (encode.Contains("&#242;"))
                encode = encode.Replace("&#242;", "ò");
            if (encode.Contains("&#243;"))
                encode = encode.Replace("&#243;", "ó");
            if (encode.Contains("&#244;"))
                encode = encode.Replace("&#244;", "ô");
            if (encode.Contains("&#245;"))
                encode = encode.Replace("&#245;", "õ");
            if (encode.Contains("&#249;"))
                encode = encode.Replace("&#249;", "ù");
            if (encode.Contains("&#250;"))
                encode = encode.Replace("&#250;", "ú");
            if (encode.Contains("&#253;"))
                encode = encode.Replace("&#253;", "ý");

            trueEncode = encode;
            return trueEncode;

        }
        /// <summary>
        ///Author: PhuongLT
        /// Kiem tra ngày tháng
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static bool checkDatetime(string date)
        {
            Regex reg = new Regex(@"^(((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00|[048])))$");
            if (reg.IsMatch(date) == false)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        /// <summary>
        /// Chuyển đổi tiếng Việt có dấu thành không dấu
        /// PhuongLt15: 20/05/2014
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertVN(string value)
        {
            //---------------------------------a^
            value = value.Replace("ấ", "a");
            value = value.Replace("ầ", "a");
            value = value.Replace("ẩ", "a");
            value = value.Replace("ẫ", "a");
            value = value.Replace("ậ", "a");
            //---------------------------------A^ 
            value = value.Replace("Ấ", "A");
            value = value.Replace("Ầ", "A");
            value = value.Replace("Ẩ", "A");
            value = value.Replace("Ẫ", "A");
            value = value.Replace("Ậ", "A");
            //---------------------------------a( 
            value = value.Replace("ắ", "a");
            value = value.Replace("ằ", "a");
            value = value.Replace("ẳ", "a");
            value = value.Replace("ẵ", "a");
            value = value.Replace("ặ", "a");
            //---------------------------------A(
            value = value.Replace("Ắ", "A");
            value = value.Replace("Ằ", "A");
            value = value.Replace("Ẳ", "A");
            value = value.Replace("Ẵ", "A");
            value = value.Replace("Ặ", "A");

            //---------------------------------a
            value = value.Replace("á", "a");
            value = value.Replace("à", "a");
            value = value.Replace("ả", "a");
            value = value.Replace("ã", "a");
            value = value.Replace("ạ", "a");
            value = value.Replace("â", "a");
            value = value.Replace("ă", "a");

            //---------------------------------A
            value = value.Replace("Á", "A");
            value = value.Replace("À", "A");
            value = value.Replace("Ả", "A");
            value = value.Replace("Ã", "A");
            value = value.Replace("Ạ", "A");
            value = value.Replace("Â", "A");
            value = value.Replace("Ă", "A");
            //---------------------------------A
            value = value.Replace("ế", "e");
            value = value.Replace("ề", "e");
            value = value.Replace("ể", "e");
            value = value.Replace("ễ", "e");
            value = value.Replace("ệ", "e");
            //---------------------------------E^
            value = value.Replace("Ế", "E");
            value = value.Replace("Ề", "E");
            value = value.Replace("Ể", "E");
            value = value.Replace("Ễ", "E");
            value = value.Replace("Ệ", "E");
            //---------------------------------e
            value = value.Replace("é", "e");
            value = value.Replace("è", "e");
            value = value.Replace("ẻ", "e");
            value = value.Replace("ẽ", "e");
            value = value.Replace("ẹ", "e");
            value = value.Replace("ê", "e");

            //---------------------------------E
            value = value.Replace("É", "E");
            value = value.Replace("È", "E");
            value = value.Replace("Ẻ", "E");
            value = value.Replace("Ẽ", "E");
            value = value.Replace("Ẹ", "E");
            value = value.Replace("Ê", "E");
            //---------------------------------i
            value = value.Replace("í", "i");
            value = value.Replace("ì", "i");
            value = value.Replace("ỉ", "i");
            value = value.Replace("ĩ", "i");
            value = value.Replace("ị", "i");

            //---------------------------------I
            value = value.Replace("Í", "I");
            value = value.Replace("Ì", "I");
            value = value.Replace("Ỉ", "I");
            value = value.Replace("Ĩ", "I");
            value = value.Replace("Ị", "I");
            //---------------------------------o^ 
            value = value.Replace("ố", "o");
            value = value.Replace("ồ", "o");
            value = value.Replace("ổ", "o");
            value = value.Replace("ỗ", "o");
            value = value.Replace("ộ", "o");
            //---------------------------------O^ 
            value = value.Replace("Ố", "O");
            value = value.Replace("Ồ", "O");
            value = value.Replace("Ổ", "O");
            value = value.Replace("Ô", "O");
            value = value.Replace("Ộ", "O");
            //---------------------------------o*
            value = value.Replace("ớ", "o");
            value = value.Replace("ờ", "o");
            value = value.Replace("ở", "o");
            value = value.Replace("ỡ", "o");
            value = value.Replace("ợ", "o");
            //---------------------------------O*
            value = value.Replace("Ớ", "O");
            value = value.Replace("Ờ", "O");
            value = value.Replace("Ở", "O");
            value = value.Replace("Ỡ", "O");
            value = value.Replace("Ợ", "O");
            //---------------------------------u*
            value = value.Replace("ứ", "u");
            value = value.Replace("ử", "u");
            value = value.Replace("ừ", "u");
            value = value.Replace("ữ", "u");
            value = value.Replace("ự", "u");
            //---------------------------------U*
            value = value.Replace("Ứ", "U");
            value = value.Replace("Ừ", "U");
            value = value.Replace("Ử", "U");
            value = value.Replace("Ữ", "U");
            value = value.Replace("Ự", "U");

            //---------------------------------y
            value = value.Replace("ý", "y");
            value = value.Replace("ỳ", "y");
            value = value.Replace("ỷ", "y");
            value = value.Replace("ỹ", "y");
            value = value.Replace("ỵ", "y");
            //---------------------------------y
            value = value.Replace("Ý", "Y");
            value = value.Replace("Ỳ", "Y");
            value = value.Replace("Ỷ", "Y");
            value = value.Replace("Ỹ", "Y");
            value = value.Replace("Ỵ", "Y");

            //---------------------------------DD 
            value = value.Replace("Đ", "D");
            value = value.Replace("Đ", "D");
            value = value.Replace("đ", "d");

            //---------------------------------0
            value = value.Replace("ó", "o");
            value = value.Replace("ò", "o");
            value = value.Replace("ỏ", "o");
            value = value.Replace("õ", "o");
            value = value.Replace("ọ", "o");

            value = value.Replace("ô", "o");
            value = value.Replace("ơ", "o");
            //---------------------------------0
            value = value.Replace("Ó", "O");
            value = value.Replace("Ò", "O");
            value = value.Replace("Ỏ", "O");
            value = value.Replace("Õ", "O");
            value = value.Replace("Ọ", "O");
            value = value.Replace("Ô", "O");
            value = value.Replace("Ơ", "O");

            //---------------------------------u
            value = value.Replace("ú", "u");
            value = value.Replace("ù", "u");
            value = value.Replace("ủ", "u");
            value = value.Replace("ũ", "u");
            value = value.Replace("ụ", "u");
            value = value.Replace("ư", "u");

            //---------------------------------U
            value = value.Replace("Ú", "U");
            value = value.Replace("Ù", "U");
            value = value.Replace("Ủ", "U");
            value = value.Replace("Ũ", "U");
            value = value.Replace("Ụ", "U");
            value = value.Replace("Ư", "U");
            return value;
        }
        /// <summary>
        /// Cắt bỏ các ký tự HTML
        /// Phuonglt15: 20/05/2014
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string StripHTML(string source)
        {
            try
            {
                string result;

                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                result = source.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating spaces because browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                // Remove the header (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result,
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty,
                //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV> and <TR> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything that's enclosed inside < >
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // replace special characters:
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove all others. More can be added, see
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // for testing
                //System.Text.RegularExpressions.Regex.Replace(result,
                //       this.txtRegex.Text,string.Empty,
                //       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // make line breaking consistent
                result = result.Replace("\n", "\r");

                // Remove extra line breaks and tabs:
                // replace over 2 breaks with 2 and over 4 tabs with 4.
                // Prepare first to remove any whitespaces in between
                // the escaped characters and remove redundant tabs in between line breaks
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "\t\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "\t\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove redundant tabs
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove multiple tabs following a line break with just one tab
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Initial replacement target string for line breaks
                string breaks = "\r\r\r";
                // Initial replacement target string for tabs
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }

                // That's it.
                return result;
            }
            catch
            {

                return source;
            }
        }
        /// <summary>
        ///Author: PhuongLT
        /// Hàm gửi mail
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool SendMsg(string subject = "N/A", string msg = "N/A", string urlfile = "", int doctorId = 0)
        {
            string _mailServer = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ftpServer"]);
            int _mailPort = Int32.Parse(ConfigurationManager.AppSettings["sslPort"].ToString());
            string _user = Convert.ToString(ConfigurationManager.AppSettings["ftpUser"]);
            string _pass = Convert.ToString(ConfigurationManager.AppSettings["ftpPassword"]);
            MailMessage mailMessage = new MailMessage();
            SmtpClient mailClient = new SmtpClient(_mailServer, _mailPort);
            mailClient.Timeout = 15000;
            mailClient.Credentials = new NetworkCredential(_user, _pass);
            mailClient.EnableSsl = true;        //mailClient.UseDefaultCredentials = true; // no work   
            mailMessage.IsBodyHtml = true;
            mailMessage.From = new MailAddress(_user);
            mailMessage.Subject = subject;
            mailMessage.Body = msg;
            mailMessage.Priority = MailPriority.High;
            string fileName = urlfile;
            if (!String.IsNullOrEmpty(fileName) && fileName.Length > 0)
            {
                string[] vArray = fileName.Split(';');
                if (vArray.Length > 0)
                {
                    for (int i = 0; i < vArray.Length; i++)
                    {
                        string file = vArray[i].ToString();
                        string pathfile = System.Web.HttpContext.Current.Server.MapPath(file);
                        mailMessage.Attachments.Add(new Attachment(pathfile));
                    }
                }

            }
            //-----------------------//
            string mailto = "";// từ DOCTORS_ID lấy ra mail của bác sĩ
            //----------------------//
            mailMessage.To.Add(mailto);
            try
            {
                mailClient.Send(mailMessage);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                mailMessage.Dispose();
                mailClient.Dispose();
            }
        }
        /// <summary>
        ///Author: PhuongLT
        /// Đếm số ký tự
        /// </summary>
        /// <param name="value"></param>
        /// <param name="chr"></param>
        /// <returns></returns>
        public static int Count(string value, char chr)
        {

            int result = 0;
            // char c = ';';
            foreach (char c in value)
            {
                if (c == chr)
                {
                    result++;
                }

            }

            return result;
        }
        /// <summary>
        ///Author: PhuongLT
        /// Bôi màu đoạn văn tìm kiếm trên lưới
        /// </summary>
        /// <param name="strChuoi"></param>
        /// <param name="strTuKhoa"></param>
        /// <returns></returns>
        public static string DrawSearch(string strChuoi, string strTuKhoa)
        {
            string str = strTuKhoa.Trim();
            string result = "";
            if (Count(str, ' ') != 0)
            {
                string[] spt = new string[Count(str, ' ')];
                spt = str.Split(' ');
                for (int i = 0; i <= Count(str, ' '); i++)
                {
                    string sp = spt[i].Trim();
                    if (sp != "" && sp != null)
                    {
                        string gd = spt[i].Trim();
                        string gt = gd[0].ToString();
                        result += " " + gd.Remove(0, 1).Insert(0, gt.ToUpper());
                    }
                }

            }
            else
            {
                string gt = str[0].ToString();
                result = str.Remove(0, 1).Insert(0, gt.ToUpper());
            }
            return strChuoi.Replace(result.Trim(), "<span class='searchLight'>" + result.Trim() + "</span>");


        }
        /// <summary>
        /// Chuyển đổi ngày tháng sang thứ
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string GetDayName(DateTime date)
        {
            var day = date.DayOfWeek;
            var result = string.Empty;
            switch (day)
            {
                case DayOfWeek.Friday:
                    result = "Thứ 6";
                    break;
                case DayOfWeek.Monday:
                    result = "Thứ 2";
                    break;
                case DayOfWeek.Saturday:
                    result = "Thứ 7";
                    break;
                case DayOfWeek.Sunday:
                    result = "Chủ nhật";
                    break;
                case DayOfWeek.Thursday:
                    result = "Thứ 5";
                    break;
                case DayOfWeek.Tuesday:
                    result = "Thứ 3";
                    break;
                case DayOfWeek.Wednesday:
                    result = "Thứ 4";
                    break;
            }
            return result;
        }
        public static string GetDayNameVT(DateTime date)
        {
            var day = date.DayOfWeek;
            var result = string.Empty;
            switch (day)
            {
                case DayOfWeek.Friday:
                    result = "T6";
                    break;
                case DayOfWeek.Monday:
                    result = "T2";
                    break;
                case DayOfWeek.Saturday:
                    result = "T7";
                    break;
                case DayOfWeek.Sunday:
                    result = "CN";
                    break;
                case DayOfWeek.Thursday:
                    result = "T5";
                    break;
                case DayOfWeek.Tuesday:
                    result = "T3";
                    break;
                case DayOfWeek.Wednesday:
                    result = "T4";
                    break;
            }
            return result;
        }

        public static string GetRootForder
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["RootContentFolder"]; }
        }

        public static string GetForderAvatar
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["RootContentFolder"] + @"Images\"; }
        }

        /// <summary>
        /// Hàm đổi đường dẫn ảnh từ sang StringBase64.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ImageToBase64(string path)
        {
            if (string.IsNullOrEmpty(path) || !File.Exists(path.Trim()))
            {
                //path = GetRootForder + @"\Images\UserAvartars\doctor_avatar_default.png";
                path = GetRootForder + @"/Images/accent.png";
                if (!File.Exists(path))
                    return string.Empty;
            }

            var file = new FileStream(path, FileMode.Open, FileAccess.Read);
            var dtByte = new byte[file.Length];
            file.Read(dtByte, 0, System.Convert.ToInt32(file.Length));
            //Close the File Stream
            file.Close();
            //return dtByte; //return the byte data
            //Byte[] _byte = file.ex
            var extension = Path.GetExtension(path).Trim().Replace(".", "");
            return string.Format("data:image/{0};base64,{1}", extension, Convert.ToBase64String(dtByte));
        }


        /// <summary>
        /// Lấy avatar dưới dạng stringbase4
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetPathUserImage(string url)
        {
            string path = GetRootForder + "/Images/";
            var fullPath = path + url;
            if (File.Exists(fullPath))
                return ImageToBase64(fullPath);
            else
                return ImageToBase64(path + "accent.png");
        }

        public static string GetIP4Address()
        {
            string IP4Address = String.Empty;

            foreach (IPAddress IPA in Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            if (IP4Address != String.Empty)
            {
                return IP4Address;
            }

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            return IP4Address;
        }
        /// <summary>
        /// Địa chỉ IP trên máy tính cá nhân
        /// </summary>
        /// <returns></returns>
        public static string GetComputerLanIP()
        {
            string strHostName = Dns.GetHostName();

            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);

            foreach (IPAddress ipAddress in ipEntry.AddressList)
            {
                if (ipAddress.AddressFamily.ToString() == "InterNetwork")
                {
                    return ipAddress.ToString();
                }
            }

            return "-";
        }
        /// <summary>
        /// Địa chỉ IP trên Server
        /// </summary>
        /// <returns></returns>
        private static string GetComputer_InternetIP()
        {
            // check IP using DynDNS's service
            WebRequest request = WebRequest.Create("http://checkip.dyndns.org");
            WebResponse response = request.GetResponse();
            StreamReader stream = new StreamReader(response.GetResponseStream());

            // IMPORTANT: set Proxy to null, to drastically INCREASE the speed of request
            request.Proxy = null;

            // read complete response
            string ipAddress = stream.ReadToEnd();

            // replace everything and keep only IP
            return ipAddress.
                Replace("<html><head><title>Current IP Check</title></head><body>Current IP Address: ", string.Empty).
                Replace("</body></html>", string.Empty);
        }

        #region "Design Calendar"
        public static DateTime ConvertToDateTime(string day, string month, string year)
        {
            DateTime dtime = Convert.ToDateTime(day + "/" + Convert.ToString(month) + "/" + Convert.ToString(year));
            return dtime;
        }
        /// <summary>
        /// Lịch tháng của ban giám đốc
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DataTable CalendarWeekly(DateTime date)
        {
            int iMonth = date.Month;
            int iYear = date.Year;
            DataTable dt = new DataTable();
            dt.Columns.Add("Weekly", typeof(string));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Time", typeof(string));
            dt.Columns.Add("Start", typeof(int));
            dt.Columns.Add("End", typeof(int));
            int countDay = GetDaysInMonth(iMonth, iYear);
            DataRow dr = dt.NewRow();
            dr["Weekly"] = "Tuần 1 (01-07/" + iMonth + "/" + iYear + ")";
            dr["Name"] = "Tuần 1";
            dr["Time"] = "01-07/" + iMonth + "/" + iYear + "";
            dr["Start"] = 1;
            dr["End"] = 7;
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Weekly"] = "Tuần 2 (08-14/" + iMonth + "/" + iYear + ")";
            dr["Name"] = "Tuần 2";
            dr["Time"] = "08-14/" + iMonth + "/" + iYear + "";
            dr["Start"] = 8;
            dr["End"] = 14;
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Weekly"] = "Tuần 3 (15-21/" + iMonth + "/" + iYear + ")";
            dr["Name"] = "Tuần 3";
            dr["Time"] = "15-21/" + iMonth + "/" + iYear + "";
            dr["Start"] = 15;
            dr["End"] = 21;
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Weekly"] = "Tuần 4 (22-" + countDay + "/" + iMonth + "/" + iYear + ")";
            dr["Name"] = "Tuần 4";
            dr["Time"] = "22-" + countDay + "/" + iMonth + "/" + iYear + "";
            dr["Start"] = 22;
            dr["End"] = countDay;
            dt.Rows.Add(dr);
            dr = null;
            return dt;

        }
        /// <summary>
        ///Author:  PhuongLT15
        /// Xác định ngày đầu tiên trong háng thuộc thứ mấy
        /// </summary>
        /// <param name="date1"></param>
        /// <returns></returns>
        public static int FirstDate(DateTime date1)
        {
            //tham so thang,nam hien tai
            int month = date1.Month;
            int year = date1.Year;
            //////////
            // DateTime dtime = Convert.ToDateTime(Convert.ToString(month) + "/" + "1" + "/" + Convert.ToString(year));           
            DateTime dtime = ConvertToDateTime("1", Convert.ToString(month), Convert.ToString(year));
            int date = 0;
            string thu = Getday(dtime);
            switch (thu)
            {

                case "ThuHai":
                    date = 0;
                    break;
                case "ThuBa":
                    date = 1;
                    break;
                case "ThuTu":
                    date = 2;
                    break;
                case "ThuNam":
                    date = 3;
                    break;
                case "ThuSau":
                    date = 4;
                    break;
                case "ThuBay":
                    date = 5;
                    break;
                case "ChuNhat":
                    date = 6;
                    break;

            }
            return date;

        }
        /// <summary>
        /// Tạo bảng động lưu dữ liệu thứ
        /// </summary>
        /// <returns></returns>
        public static DataTable CreatTb()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ThuHai", typeof(string));
            dt.Columns.Add("ThuBa", typeof(string));
            dt.Columns.Add("ThuTu", typeof(string));
            dt.Columns.Add("ThuNam", typeof(string));
            dt.Columns.Add("ThuSau", typeof(string));
            dt.Columns.Add("ThuBay", typeof(string));
            dt.Columns.Add("ChuNhat", typeof(string));
            return dt;

        }
        /// <summary>
        /// Đổ toàn bộ dữ liệu ngày của tháng lên Datatable
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DataTable Fill(DateTime date)
        {
            string[,] arr = new string[8, 7];
            arr[0, 0] = "ThuHai";
            arr[0, 1] = "ThuBa";
            arr[0, 2] = "ThuTu";
            arr[0, 3] = "ThuNam";
            arr[0, 4] = "ThuSau";
            arr[0, 5] = "ThuBay";
            arr[0, 6] = "ChuNhat";
            int dem = 0;
            for (int j = 1; j <= CountRow(date); j++)
            {
                if (j == 1)
                {
                    for (int i = FirstDate(date); i < j * 7; i++)
                    {
                        dem = dem + 1;
                        //tham so thang,nam hien tai
                        int month = date.Month;
                        int year = date.Year;
                        //////////                   
                        DateTime dtime = ConvertToDateTime(Convert.ToString(dem), Convert.ToString(month), Convert.ToString(year));
                        int day = dtime.Day;
                        string thu = Getday(dtime);
                        // lblDay.Text = lblDay.Text + thu;
                        switch (thu)
                        {


                            case "ThuHai":
                                arr[j, 0] = day.ToString();
                                break;
                            case "ThuBa":
                                arr[j, 1] = day.ToString();
                                break;
                            case "ThuTu":
                                arr[j, 2] = day.ToString();
                                break;
                            case "ThuNam":
                                arr[j, 3] = day.ToString();
                                break;
                            case "ThuSau":
                                arr[j, 4] = day.ToString();
                                break;
                            case "ThuBay":
                                arr[j, 5] = day.ToString();
                                break;
                            case "ChuNhat":
                                arr[j, 6] = day.ToString();
                                break;

                        }
                    }
                }
                else
                {
                    for (int i = (j - 1) * 7 + 1; i <= j * 7; i++)
                    {
                        dem = dem + 1;
                        if (dem <= GetDaysInMonth(date.Month, date.Year))
                        {
                            //tham so thang,nam hien tai
                            int month = date.Month;
                            int year = date.Year;
                            //////////                          
                            DateTime dtime = ConvertToDateTime(Convert.ToString(dem), Convert.ToString(month), Convert.ToString(year));
                            int day = dtime.Day;
                            string thu = Getday(dtime);
                            // lblDay.Text = lblDay.Text + thu;
                            switch (thu)
                            {
                                case "ThuHai":
                                    arr[j, 0] = day.ToString();
                                    break;
                                case "ThuBa":
                                    arr[j, 1] = day.ToString();
                                    break;
                                case "ThuTu":
                                    arr[j, 2] = day.ToString();
                                    break;
                                case "ThuNam":
                                    arr[j, 3] = day.ToString();
                                    break;
                                case "ThuSau":
                                    arr[j, 4] = day.ToString();
                                    break;
                                case "ThuBay":
                                    arr[j, 5] = day.ToString();
                                    break;
                                case "ChuNhat":
                                    arr[j, 6] = day.ToString();
                                    break;

                            }
                        }

                    }
                }

            }


            DataTable dt = CreatTb();
            //dong chay
            for (int i = 1; i <= CountRow(date); i++)
            {
                DataRow dr = dt.NewRow();
                //cot chay
                for (int j = 0; j <= 6; j++)
                {
                    if (arr[0, j] == "ChuNhat")
                    {
                        dr["ChuNhat"] = arr[i, j];
                    }
                    else if (arr[0, j] == "ThuHai")
                    {
                        dr["ThuHai"] = arr[i, j];
                    }
                    else if (arr[0, j] == "ThuBa")
                    {
                        dr["ThuBa"] = arr[i, j];
                    }
                    else if (arr[0, j] == "ThuTu")
                    {
                        dr["ThuTu"] = arr[i, j];
                    }
                    else if (arr[0, j] == "ThuNam")
                    {
                        dr["ThuNam"] = arr[i, j];
                    }
                    else if (arr[0, j] == "ThuSau")
                    {
                        dr["ThuSau"] = arr[i, j];
                    }
                    else
                    {
                        dr["ThuBay"] = arr[i, j];
                    }
                }
                dt.Rows.Add(dr);
            }
            return dt;

        }

        /// <summary>
        /// Xác định 1 ngày nào đó là thuộc thứ mấy
        /// </summary>
        /// <param name="ngay"></param>
        /// <returns></returns>
        public static string Getday(DateTime ngay, int? dayType = null)
        {
            var lstDay = new List<string>();
            string[] arrDaysDefault = { "Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy" };
            string[] arrDays = { "Chủ nhật", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7" };
            string[] thu = { "ChuNhat", "ThuHai", "ThuBa", "ThuTu", "ThuNam", "ThuSau", "ThuBay" };
            if (dayType.HasValue)
            {
                //DayType == 1 or = 2
                //Có thể bổ sung enum cho các trường hợp dùng thứ dạng text, hay dạng số
                lstDay = dayType.Value == 1 ? arrDays.ToList() : arrDaysDefault.ToList();
            }
            else
            {
                lstDay = thu.ToList();
            }
            string re = lstDay[System.Convert.ToInt32(ngay.DayOfWeek)].ToString();
            return re;
        }
        /// <summary>
        /// Xác định số ngày trong 1 tháng là bao ngày
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static int GetDaysInMonth(int month, int year)
        {
            if (month < 1 || month > 12)
            {

            }
            if (1 == month || 3 == month || 5 == month || 7 == month || 8 == month ||
            10 == month || 12 == month)
            {
                return 31;
            }
            else if (2 == month)
            {

                if (0 == (year % 4))
                {

                    if (0 == (year % 400))
                    {
                        return 29;
                    }
                    else if (0 == (year % 100))
                    {
                        return 28;
                    }


                    return 29;
                }

                return 28;
            }
            return 30;
        }
        /// <summary>
        /// Đếm số dòng sẽ hiển thị ra khi dựa vào ngày 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int CountRow(DateTime date)
        {
            //tham so thang,nam hien tai
            int month = date.Month;
            int year = date.Year;
            //////////       
            DateTime dtime = ConvertToDateTime("1", Convert.ToString(month), Convert.ToString(year));
            int countrow = GetDaysInMonth(date.Month, date.Year);
            string thu = Getday(dtime);

            if (countrow < 29 && thu == "ThuHai")
            {
                return 4;
            }
            else if ((countrow >= 30 && thu == "ChuNhat") || (countrow == 31 && thu == "ThuBay"))
            {
                return 6;
            }

            else
            {
                return 5;
            }
        }
        /// <summary>
        /// Trở về tháng trước đó
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime NextMonth(DateTime date)
        {

            DateTime pass = date.AddMonths(-1);
            return pass;
        }
        /// <summary>
        /// Trở về tháng phía sau đó
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime PreMonth(DateTime date)
        {
            DateTime pass = date.AddMonths(1);
            return pass;
        }
        /// <summary>
        /// Lấy trạng thái lịch trực
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        #endregion

        //Biến lấy định dạng ngôn ngữ
        public static string strLanguageFormat = System.Configuration.ConfigurationManager.AppSettings["LanguageFormat"];
        public static bool IsImage(this HttpPostedFileBase postedFile)
        {
            try
            {
                using (var bitmap = new System.Drawing.Bitmap(postedFile.InputStream))
                {
                    if (bitmap.Size.IsEmpty)
                    {
                        return false;
                    }
                    else return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }



        /// <summary>
        /// created by: HienDM1
        /// create date: 07/04/2015
        /// Hàm xuất dữ liệu từ datatable ra excel 
        /// </summary>
        /// <param name="firstTime">Lần đầu tiên ghi file</param>
        /// /// <param name="fileName">Đường dẫn file excel</param>
        /// /// <param name="ResultsData">Dữ liệu trong datatable</param>
        /// <returns></returns>
        ///


        /// <summary>
        /// created by: HienDM1
        /// create date: 07/04/2015
        /// Hàm kiểm tra dữ liệu ngày giờ 
        /// </summary>
        /// <param name="obj">Thông tin kiểm tra</param>
        /// <returns></returns>
        ///
        public static bool validateDateTime(string obj)
        {
            if (obj == null || obj.Trim() == "") return true;
            obj = obj.Trim();
            bool check = false;
            try
            {
                if (obj.Length == 8)
                {
                    obj += "0000";
                }
                if (obj.Length == 12)
                {
                    int year = int.Parse(obj.Substring(0, 4));
                    int month = int.Parse(obj.Substring(4, 2));
                    int day = int.Parse(obj.Substring(6, 2));
                    int hour = int.Parse(obj.Substring(8, 2));
                    int minute = int.Parse(obj.Substring(10, 2));

                    if (!(year >= 1900 && year <= DateTime.Now.Year))
                    {
                        return false;
                    }
                    if (!(month >= 1 && month <= 12))
                    {
                        return false;
                    }
                    if (!(day >= 1 && day <= DateTime.DaysInMonth(year, month)))
                    {
                        return false;
                    }
                    if (!(hour >= 0 && hour <= 23))
                    {
                        return false;
                    }
                    if (!(minute >= 0 && minute <= 59))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                check = true;
            }
            catch (Exception ex)
            {
                // Nếu ép kiểu lỗi thì check = false
            }
            return check;
        }

        /// <summary>
        /// created by: HienDM1
        /// create date: 07/04/2015
        /// Hàm kiểm tra dữ liệu ngày
        /// </summary>
        /// <param name="obj">Thông tin kiểm tra</param>
        /// <returns></returns>
        ///
        public static bool validateDate(string obj)
        {
            if (obj == null || obj.Trim() == "") return true;
            obj = obj.Trim();
            bool check = false;
            try
            {
                if (obj.Length == 8)
                {
                    obj += "0000";
                }
                if (obj.Length == 12)
                {
                    int year = int.Parse(obj.Substring(0, 4));
                    int month = int.Parse(obj.Substring(4, 2));
                    int day = int.Parse(obj.Substring(6, 2));
                    int hour = int.Parse(obj.Substring(8, 2));
                    int minute = int.Parse(obj.Substring(10, 2));

                    if (!(year >= 1900 && year <= DateTime.Now.Year))
                    {
                        return false;
                    }
                    if (!(month >= 1 && month <= 12))
                    {
                        return false;
                    }
                    if (!(day >= 1 && day <= DateTime.DaysInMonth(year, month)))
                    {
                        return false;
                    }
                    if (!(hour >= 0 && hour <= 23))
                    {
                        return false;
                    }
                    if (!(minute >= 0 && minute <= 59))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                check = true;
            }
            catch (Exception ex)
            {
                // Nếu ép kiểu lỗi thì check = false
            }
            return check;
        }
        /// <summary>
        /// created by: HienDM1
        /// create date: 07/04/2015
        /// Hàm kiểm tra dữ liệu ngày
        /// </summary>
        /// <param name="obj">Thông tin kiểm tra</param>
        /// <returns></returns>
        ///
        public static bool validateDateAllowFuture(string obj)
        {
            if (obj == null || obj.Trim() == "") return true;
            obj = obj.Trim();
            bool check = false;
            try
            {
                if (obj.Length == 8)
                {
                    obj += "0000";
                }
                if (obj.Length == 12)
                {
                    int year = int.Parse(obj.Substring(0, 4));
                    int month = int.Parse(obj.Substring(4, 2));
                    int day = int.Parse(obj.Substring(6, 2));
                    int hour = int.Parse(obj.Substring(8, 2));
                    int minute = int.Parse(obj.Substring(10, 2));

                    if (!(year >= 2000 && year <= 9999))
                    {
                        return false;
                    }
                    if (!(month >= 1 && month <= 12))
                    {
                        return false;
                    }
                    if (!(day >= 1 && day <= DateTime.DaysInMonth(year, month)))
                    {
                        return false;
                    }
                    if (!(hour >= 0 && hour <= 23))
                    {
                        return false;
                    }
                    if (!(minute >= 0 && minute <= 59))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                check = true;
            }
            catch (Exception ex)
            {
                // Nếu ép kiểu lỗi thì check = false
            }
            return check;
        }
        /// <summary>
        /// hinhpt5 
        /// Hàm lấy dữ liệu từ file Excel
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="hasHeaders"></param>
        /// <param name="sheetName"></param>
        /// <param name="dongbatdau"></param>
        /// <param name="soluong"></param>
        /// <param name="iColumnCount"></param>
        /// <returns></returns>
        public static DataTable GetPreviewExcelDataDMThuoc(string FileName, bool hasHeaders, string sheetName, string dongbatdau, int soluong, int iColumnCount = 250)
        {
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            else
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
            int intDongBatDau = Convert.ToInt32(dongbatdau);
            int intDongKetThuc = intDongBatDau + 15;
            DataTable tableTemp = new DataTable(sheetName);

            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                conn.Open();

                DataTable schemaTable = conn.GetOleDbSchemaTable(
                    OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                foreach (DataRow schemaRow in schemaTable.Rows)
                {
                    string sheet = schemaRow["TABLE_NAME"].ToString();
                    if (sheetName == sheet)
                    {
                        if (sheet.EndsWith("$") || sheet.EndsWith("$'"))
                        {
                            try
                            {
                                string cauLenh = "SELECT top " + intDongKetThuc + " * FROM [" + sheet + "]";
                                if (soluong == 0)
                                {
                                    cauLenh = "SELECT  * FROM [" + sheet + "]";
                                }
                                OleDbCommand cmd = new OleDbCommand(cauLenh, conn);
                                cmd.CommandType = CommandType.Text;
                                new OleDbDataAdapter(cmd).Fill(tableTemp);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message + string.Format("Sheet:{0}.File:F{1}", sheet, FileName), ex);
                            }
                            break;
                        }
                    }
                }
            }
            return tableTemp;
        }
        /// <summary>
        /// Hàm đọc sheet từ file Excel
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public static List<string> getSheetInExxcel(string FileName)
        {
            string strConn;
            if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1\"";
            else
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1\"";

            List<string> output = new List<string>();
            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                conn.Open();
                DataTable schemaTable = conn.GetOleDbSchemaTable(
                    OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                foreach (DataRow schemaRow in schemaTable.Rows)
                {
                    string sheet = schemaRow["TABLE_NAME"].ToString();
                    output.Add(sheet);
                }
            }
            return output;
        }

        public static List<string> getColumnInExcel(string FileName, string header, string sheetname)
        {
            string HDR = "Yes";
            if (header.ToLower().Trim() == "no")
            {
                HDR = "No";
            }
            string strConn;
            if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            else
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";

            List<string> output = new List<string>();
            sheetname = sheetname.ToLower().Trim();
            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                conn.Open();
                DataTable schemaTable = conn.GetOleDbSchemaTable(
                    OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                foreach (DataRow schemaRow in schemaTable.Rows)
                {
                    string sheet = schemaRow["TABLE_NAME"].ToString().ToLower().Trim();
                    if (sheet == sheetname)
                    {
                        //string sExcelColumnName = GetExcelEndDataColumn(250);
                        //OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheet + "A:" + sExcelColumnName + "]", conn);
                        OleDbCommand cmd = new OleDbCommand("select * from [" + sheet + "]", conn);
                        cmd.CommandType = CommandType.Text;

                        DataTable outputTable = new DataTable(sheet);
                        new OleDbDataAdapter(cmd).Fill(outputTable);
                        foreach (DataColumn column in outputTable.Columns)
                        {
                            output.Add(column.ColumnName);
                        }
                        break;
                    }
                }
            }
            return output;
        }
        public static DataTable ToDataTable<T>(this IEnumerable<T> collection)
        {
            DataTable dt = new DataTable("DataTable");
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();

            //Inspect the properties and create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                Type ColumnType = pi.PropertyType;
                if ((ColumnType.IsGenericType))
                {
                    ColumnType = ColumnType.GetGenericArguments()[0];
                }
                dt.Columns.Add(pi.Name, ColumnType);
            }

            //Populate the data table
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {
                    if (pi.GetValue(item, null) != null)
                    {
                        dr[pi.Name] = pi.GetValue(item, null);
                    }
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }


    }

}
