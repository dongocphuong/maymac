﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Utils
{
    public static class ErrorToast
    {
        public static string ERROR = "error";
        public static string SUCCESS = "success";
        public static string INFO = "info";
        public static string WARNING = "warning";
    }
}