﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DFWeb.Utils
{
    public class NotificationToastr
    {
       public string message { get; set; }
       public string type { get; set; }
       public string title { get; set; }
    }
}