﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using DF.DBMapping.Models;
using System.Net.Mail;
using System.Net;
using Resources;
using System.Configuration;
using System.Data;
using System.IO;
using DFWeb.Controllers;
using DF.DataAccess;
using System.Globalization;
using System.Data.OleDb;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Xml;
using System.Xml.Xsl;
using DFWeb.Models;
using FlexCel.XlsAdapter;
using FlexCel.Report;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using DFWeb.Common;
using DF.DBMapping.ModelsExt.Enums.CongTrinh;

namespace DFWeb.Utils
{
    public class ListNhanVienLuong
    {
        public Guid NhanVienId {get; set;}
        public DateTime tungay { get; set; }
        public DateTime denngay { get; set; }
    }
    public static class Utils
    {
        [DllImport("user32.dll")]
        static extern int GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);
        //public static ThoiDoiKhoanThucTeModel ThoiDoiKhoanThucTe(string CongTrinhID, string ThoiGian)
        //{
        //    UnitOfWork unitofwork = new UnitOfWork();
        //    BaoCaoThuChiModel model = new BaoCaoThuChiModel();
        //    string[] TGArray = ThoiGian.Split('-');
        //    DateTime tungay = DateTime.ParseExact(TGArray[0].Trim().ToString(), "dd/MM/yyyy", null);
        //    DateTime denngay = DateTime.ParseExact(TGArray[1].Trim().ToString(), "dd/MM/yyyy", null).AddDays(1).AddMinutes(-1);
        //    DataTable kq = null;
        //    if (CongTrinhID == "" || CongTrinhID.Contains("0000000000") || CongTrinhID == null)
        //    {
        //        kq = unitofwork.BaoCaoChiPhis.getBaoCaoDoanhThu(tungay, denngay);
        //    }
        //    else
        //    {
        //        kq = unitofwork.BaoCaoChiPhis.getBaoCaoDoanhThuTheoNhomCT(tungay, denngay, CongTrinhID);
        //    }
        //    kq.Columns.Add("CPTB", typeof(Double));
        //    kq.Columns.Add("CPTBThueNgoai", typeof(Double));
        //    foreach (DataColumn col in kq.Columns)
        //        col.ReadOnly = false;
        //    DateTime bgindate = Convert.ToDateTime("Jan 01, 1900");
        //    DateTime date = DateTime.Now;
        //    //thiet bi
        //    double tngay = (tungay - bgindate).TotalDays;
        //    DateTime denngaytemp = denngay;
        //    if (denngay > date)
        //    {
        //        // string ddd = date.Month + " " + date.Day + ", " + date.Year;
        //        denngay = Convert.ToDateTime(date.ToShortDateString());//.AddDays(1);
        //    }
        //    double dngay = (denngay - bgindate).TotalDays;
        //    foreach (DataRow r in kq.Rows)
        //    {
        //        if (r["CongTrinhId"].ToString().ToUpper() != "FFFFFFFF-FFFF-FFFF-FFFF-FFFFFFFFFFFF")
        //        {
        //            double chiphitb = 0;
        //            Guid ctid = Guid.Parse(r[0].ToString());
        //            DataTable tbden = unitofwork.TrangThaiThietBis.getTBChuyenDen(ctid);
        //            DataTable tbdi = unitofwork.TrangThaiThietBis.getTBChuyenDi(ctid);
        //            double chiphiTbThueNgoai = 0;
        //            // DataTable temp = tbden.Clone();
        //            foreach (DataRow r1 in tbden.Rows)
        //            {
        //                string thietbidenid = r1[0].ToString();
        //                double tgdi = 0;
        //                double giamuathue = 0;
        //                double tgden = Convert.ToDouble(r1[4].ToString());
        //                string isTBCongTy = r1["ThietBiNoiBo"].ToString();
        //                if (tgden <= dngay)
        //                {
        //                    foreach (DataRow r2 in tbdi.Rows)
        //                    {
        //                        if (r2[0].ToString() == thietbidenid)
        //                        {
        //                            tgdi = Convert.ToDouble(r2[4].ToString());
        //                            giamuathue = Convert.ToDouble(r2[2].ToString());
        //                            tbdi.Rows.Remove(r2);
        //                            break;
        //                        }
        //                    }
        //                    if (tgdi == 0 && r1[7].ToString() == "1" && r1[6].ToString() != null && r1[6].ToString() != "")
        //                    {
        //                        tgdi = Convert.ToDouble(r1[6].ToString());
        //                        giamuathue = Convert.ToDouble(r1[2].ToString());
        //                    }
        //                    if (tgdi != 0) // nếu đã đi
        //                    {
        //                        if (tgdi < tngay) // nếu đã đi trước thời gian tìm kiếm => không tính
        //                        {

        //                        }
        //                        else
        //                        {
        //                            if (tgden < tngay) // nếu đến trước ngày bắt đầu tìm kiêm
        //                            {
        //                                tgden = tngay;
        //                            }
        //                            if (tgdi > dngay) // nếu đi sau ngay kết thúc tìm kiếm
        //                            {
        //                                tgdi = dngay;
        //                            }
        //                            chiphitb = chiphitb + (tgdi - tgden) * (giamuathue / 30);
        //                            if (isTBCongTy != "True" && r1["LoaiHinh"].ToString() == "1")
        //                            {
        //                                chiphiTbThueNgoai = chiphiTbThueNgoai + (tgdi - tgden) * (giamuathue / 30);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    { //nếu chưa đi
        //                        if (tgden < tngay) // nếu đến trước ngày bắt đầu tìm kiêm
        //                        {
        //                            tgden = tngay;
        //                        }
        //                        chiphitb = chiphitb + (dngay - tgden) * (Convert.ToDouble(r1[2].ToString()) / 30);
        //                        if (isTBCongTy != "True" && r1["LoaiHinh"].ToString() == "1")
        //                        {
        //                            chiphiTbThueNgoai = chiphiTbThueNgoai + ((dngay - tgden) * (Convert.ToDouble(r1[2].ToString()) / 30));
        //                        }
        //                    }
        //                }
        //                double tongchiphitbtb = chiphitb;
        //                DataTable tongcpsuachua = unitofwork.BCThietBis.getSuaChuaBCThietBi(ctid, tungay, denngay);
        //                if (tongcpsuachua.Rows[0][2].ToString() != "" && tongcpsuachua.Rows[0][2].ToString() != null)
        //                {
        //                    tongchiphitbtb += Convert.ToDouble(tongcpsuachua.Rows[0][2].ToString());
        //                }
        //                DataTable tongcpsuachuanoibo = unitofwork.BCThietBis.getSuaChuaBCThietBiNoiBo(ctid, tungay, denngay);
        //                if (tongcpsuachuanoibo.Rows[0][2].ToString() != "" && tongcpsuachuanoibo.Rows[0][2].ToString() != null)
        //                {
        //                    chiphiTbThueNgoai += Convert.ToDouble(tongcpsuachuanoibo.Rows[0][2].ToString());
        //                }
        //                r["CPTB"] = tongchiphitbtb;
        //                r["CPTBThueNgoai"] = chiphiTbThueNgoai;
        //            }
        //        }
        //        else
        //        {
        //            r["thuvt"] = "0";
        //        }
        //    }
        //    DataTable tongvc = unitofwork.BCVanChuyens.getTongVanChuyen(tungay, denngay);
        //    if (tongvc.Rows[0][0].ToString() != "")
        //    {
        //        model.TongVanChuyen = Convert.ToDouble(tongvc.Rows[0][0].ToString());
        //    }
        //    model.TKDoanhThu = kq;
        //    model.ThoiGian = ThoiGian;
        //    //tính theo view

        //    double tttchi = 0;
        //    double tttthu = 0;
        //    double thuhd = 0;
        //    double tongthuhd = 0;
        //    double thuhd2 = 0;
        //    double tongthuhd2 = 0;
        //    double tongthukhac = 0;
        //    double thukhac = 0;
        //    double tttdoanhthu = 0;
        //    double tttdoanhthuNoiBo = 0;

        //    double tongdonhthuchia11 = 0;

        //    double tongchiVattu = 0;
        //    double tongchithietbi = 0;
        //    double tongchithietbiNoiBo = 0;

        //    double tongchiccdc = 0;

        //    double tongchinhancong = 0;

        //    double tongchiluongvp = 0;

        //    double tongchivanchuyen = 0;
        //    int rr = 0;
        //    ThoiDoiKhoanThucTeModel result = new ThoiDoiKhoanThucTeModel();
        //        double tongchi = 0;
        //        if (model.TKDoanhThu.Rows[rr][2].ToString() != "")
        //        {
        //            tongchi = double.Parse(model.TKDoanhThu.Rows[rr][2].ToString());
        //            tttchi += tongchi;

        //        }
        //        double chiVattu = 0;
        //        if (model.TKDoanhThu.Rows[rr][5].ToString() != "")
        //        {
        //            chiVattu = double.Parse(model.TKDoanhThu.Rows[rr][5].ToString());
        //            tongchiVattu += chiVattu;

        //        }

        //        double chiccdc = 0;
        //        if (model.TKDoanhThu.Rows[rr][6].ToString() != "")
        //        {
        //            chiccdc = double.Parse(model.TKDoanhThu.Rows[rr][6].ToString());
        //            tongchiccdc += chiccdc;

        //        }

        //        double chinhancong = 0;
        //        if (model.TKDoanhThu.Rows[rr][7].ToString() != "")
        //        {
        //            chinhancong = double.Parse(model.TKDoanhThu.Rows[rr][7].ToString());
        //            tongchinhancong += chinhancong;

        //        }

        //        double chivanchuyen = 0;
        //        if (model.TKDoanhThu.Rows[rr][8].ToString() != "")
        //        {
        //            chivanchuyen = double.Parse(model.TKDoanhThu.Rows[rr][8].ToString());
        //            tongchivanchuyen += chivanchuyen;
        //        }

        //        double chithietbi = 0;
        //        if (model.TKDoanhThu.Rows[rr][10].ToString() != "")
        //        {
        //            chithietbi = double.Parse(model.TKDoanhThu.Rows[rr][10].ToString());
        //            tongchithietbi += chithietbi;

        //        }

        //        double chithietbiNoiBo = 0;
        //        if (model.TKDoanhThu.Rows[rr]["CPTBThueNgoai"].ToString() != "")
        //        {
        //            chithietbiNoiBo = double.Parse(model.TKDoanhThu.Rows[rr]["CPTBThueNgoai"].ToString());
        //            tongchithietbiNoiBo += chithietbiNoiBo;

        //        }
        //        thuhd = 0;
        //        thukhac = 0;
        //        if (model.TKDoanhThu.Rows[rr][3].ToString() != "")
        //        {
        //            thuhd = double.Parse(model.TKDoanhThu.Rows[rr][3].ToString());
        //        }
        //        thuhd2 = 0;
        //        if (model.TKDoanhThu.Rows[rr][9].ToString() != "")
        //        {
        //            thuhd2 = double.Parse(model.TKDoanhThu.Rows[rr][9].ToString());
        //        }

        //        if (model.TKDoanhThu.Rows[rr][0].ToString() == "af3cc397-da63-4154-8b70-8277b3fdc345")
        //        {
        //            thukhac = model.TongVanChuyen;
        //        }
        //        else if (model.TKDoanhThu.Rows[rr][4].ToString() != "")
        //        {
        //            thukhac = double.Parse(model.TKDoanhThu.Rows[rr][4].ToString());
        //        }
        //        tttthu += (thuhd + thukhac);
        //        tongthuhd += thuhd;
        //        tongthuhd2 += thuhd2;
        //        tongthukhac += thukhac;
        //        tongchiluongvp += (thuhd * 0.07);
        //        double tongdoanhthu = thukhac + (thuhd / 1.1) - tongchi - chiVattu - chiccdc - chinhancong - chivanchuyen - chithietbi - (thuhd * 0.07);
        //        tttdoanhthu += tongdoanhthu;
        //        //thiet bi noi bo
        //        double tongdoanhthuNoiBo = thukhac + (thuhd / 1.1) - tongchi - chiVattu - chiccdc - chinhancong - chivanchuyen - chithietbiNoiBo - (thuhd * 0.07);
        //        tttdoanhthuNoiBo += tongdoanhthuNoiBo;
        //        tongdonhthuchia11 += (thuhd / 1.1);

        //    //gan gia tri
        //        result.LoiNhuan = tongdoanhthu;
        //        result.SanLuong = thukhac + (thuhd / 1.1);
        //        result.ThietBi = chithietbi;
        //        result.CCDC = chiccdc;
        //        result.VatTu = chiVattu;
        //        result.VanChuyen = chivanchuyen;
        //        result.NhanCong = chinhancong;
        //        result.LuongVanPhong = (thuhd * 0.07);
        //        result.Khac = tongchi;
        //        return result;
        //}
        public static double TrongKhoang(double so, double dauKhoang, double cuoiKhoang)
        {
            // hàm dùng để tính xem một số có bao nhiêu phần của nó lọt vào giữa đầu và cuối khoảng
            // nếu số nhỏ hơn đầu khoảng thì hàm trả về giê rô

            if (so <= dauKhoang) return 0;
            if (so > cuoiKhoang) return cuoiKhoang - dauKhoang;
            return so - dauKhoang;
        }
        //public static QLThuNhapModel FuncTinhLuongNV(string NhanVienID, int Nam, Guid ThoiGianChotID)
        //{
        //    IUnitOfWork unitofwork = new UnitOfWork();
        //    QLThuNhapModel kq = new QLThuNhapModel();
        //    Guid defaultNVId = Guid.Parse(NhanVienID);
        //    double tongluongchuatt = 0;
        //    double tongluongdatt = 0;
        //    double tongtatca = 0;
        //    double tongluongtinhthue = 0;
        //    double tongthueTheoThang = 0;
        //    double tongthueChenhLech = 0;
        //    double TongLuongChuaGiamTru = 0;
        //    double TongGiamTru = 0;
        //    double TGTThueThang = 0;
        //    double TGTBHXH = 0;
        //    double TGTBHYT = 0;
        //    double TGTCongDoan = 0;
        //    double TGTBHTN = 0;
        //    DataTable dt = new DataTable();
        //    dt.Clear();
        //    dt.Columns.Add("ThoiGian");
        //    dt.Columns.Add("LuongCoBan");
        //    dt.Columns.Add("LuongSanLuong");
        //    dt.Columns.Add("CongNo");
        //    dt.Columns.Add("NhanVienID");
        //    dt.Columns.Add("ngaybatdauhd");
        //    dt.Columns.Add("ngaydauthangdt");
        //    dt.Columns.Add("ngaycuoithangdautien");
        //    dt.Columns.Add("checktinhbhxh");
        //    dt.Columns.Add("checktinhthue");
        //    dt.Columns.Add("SoTienTinhThue");

        //    List<LuongNhanVien> nvtllst = unitofwork.LuongNhanViens.GetAll().Where(p => p.NhanVienID == defaultNVId && p.IsActive == true).OrderBy(p => p.ThoiGianTinh).ToList();
        //    if (nvtllst != null && nvtllst.Count > 0)
        //    {
        //        //foreach(var nvtl in nvtllst){
        //        DateTime ngaybatdauhd = nvtllst[0].ThoiGianTinh;
        //        //mặc định năm tính lương là năm hiện tại => chỉ những thằng có hđ từ năm hiện tại về trước mới có lương
        //        if (ngaybatdauhd.Year <= Nam)
        //        {
        //            //nếu năm bắt đầu hđ nhở hơn năm hiện tại thì => bắt đầu tính từ đầu năm hiện tại
        //            if (ngaybatdauhd.Year < Nam)
        //            {
        //                ngaybatdauhd = DateTime.ParseExact("01/01/" + Nam, "dd/MM/yyyy", null);
        //            }
        //            DateTime Ngayngungtinh = DateTime.ParseExact("01/12/" + Nam, "dd/MM/yyyy", null).AddMonths(1).AddMinutes(-1);
        //            //fix ngày ngừng tính là ngày cuối tháng12 nam chọn, nếu ngày ngừng tính > ngày hiện tại thì ngày ngừng tính bằng ngày hiện tại
        //            DateTime datenow = DateTime.Now;
        //            if (Ngayngungtinh > datenow)
        //            {
        //                Ngayngungtinh = DateTime.ParseExact(datenow.ToString("dd/MM/yyyy"), "dd/MM/yyyy", null).AddDays(1).AddMinutes(-1);
        //            }
        //            string ngayctdt = "01/" + ngaybatdauhd.AddMonths(1).ToString("MM/yyyy");
        //            DateTime ngaycuoithangdautien = DateTime.ParseExact(ngayctdt, "dd/MM/yyyy", null).AddMinutes(-1);
        //            DateTime curday = Ngayngungtinh;
        //            DateTime ngaydauthangdt = DateTime.ParseExact("01/" + ngaybatdauhd.ToString("MM/yyyy"), "dd/MM/yyyy", null);
        //            DateTime NgayBatDauTinhLSnhanLuong = ngaybatdauhd;
        //            int songuoigiantrugiacanh = 0;
        //            List<TinhLuongNVModel> tlnvs = new List<TinhLuongNVModel>();
        //            if (ThoiGianChotID != new Guid())
        //            {
        //                // lấy các khoản giảm trừ
        //                List<Chot_ThamSoTinhLuong> thamsos = unitofwork.Chot_ThamSoTinhLuongs.GetAll().Where(p => p.ThoiGianChotID == ThoiGianChotID).ToList();
        //                //Xác định xem là luong Dự Án hay luong phòng ban
        //                DataTable tbdept = unitofwork.Chot_CongTrinhs.layCTDept(defaultNVId, ThoiGianChotID);
        //                while (curday.AddMonths(1) > ngaycuoithangdautien && (ngaydauthangdt <= nvtllst[nvtllst.Count - 1].ThoiGianHetHieuLuc || nvtllst[nvtllst.Count - 1].ThoiGianHetHieuLuc == null))
        //                {
        //                    if (curday < ngaycuoithangdautien)
        //                        ngaycuoithangdautien = curday;
        //                    tlnvs = TinhLuongNVChot(defaultNVId, ngaydauthangdt, ngaycuoithangdautien, ThoiGianChotID, thamsos, tbdept);
        //                    foreach (TinhLuongNVModel tlnv in tlnvs)
        //                    {
        //                        DataRow newrow = dt.NewRow();
        //                        newrow["ThoiGian"] = ngaybatdauhd.Month + "/" + ngaybatdauhd.Year;
        //                        newrow["LuongCoBan"] = tlnv.LuongCoBan;
        //                        newrow["LuongSanLuong"] = tlnv.LuongNhanVien;
        //                        newrow["CongNo"] = 0;
        //                        newrow["NhanVienID"] = defaultNVId.ToString();
        //                        newrow["ngaybatdauhd"] = tlnv.ngaybatdauhd.ToString("dd/MM/yyyy"); //ngaybatdauhd.ToString("dd/MM/yyyy");
        //                        newrow["ngaydauthangdt"] = tlnv.tungaynew.ToString("dd/MM/yyyy");//ngaydauthangdt.ToString("dd/MM/yyyy");
        //                        newrow["ngaycuoithangdautien"] = tlnv.denngaynew.ToString("dd/MM/yyyy");//ngaycuoithangdautien.ToString("dd/MM/yyyy");
        //                        newrow["checktinhbhxh"] = tlnv.checktinhbh;
        //                        newrow["checktinhthue"] = tlnv.checktinhthue;
        //                        newrow["SoTienTinhThue"] = tlnv.SoTienTinhThueTheoThang;
        //                        dt.Rows.Add(newrow);
        //                        tongtatca += tlnv.LuongNhanVien;
        //                        songuoigiantrugiacanh = tlnv.SoNguoiGiamTruGC;
        //                        tongluongtinhthue += tlnv.LuongTinhThue;
        //                        tongthueTheoThang += tlnv.Thue;
        //                        TongLuongChuaGiamTru += tlnv.TongLuongChuaGiamTru;
        //                        TongGiamTru += tlnv.TongLuongDaGiamTru;
        //                        //chi tiet giam tru
        //                        TGTThueThang += tlnv.Thue;
        //                        TGTBHXH += tlnv.GTBHXH;
        //                        TGTBHYT += tlnv.GTBHYT;
        //                        TGTCongDoan += tlnv.GTCongDoan;
        //                        TGTBHTN += tlnv.GTBHTN;
        //                    }
        //                    ngaycuoithangdautien = ngaydauthangdt.AddMonths(2).AddMinutes(-1);
        //                    ngaydauthangdt = ngaydauthangdt.AddMonths(1);
        //                    ngaybatdauhd = ngaydauthangdt;
        //                }
        //            }
        //            else
        //            {
        //                List<ThamSoTinhLuong> thamsos = unitofwork.ThamSoTinhLuongs.GetAll().ToList();
        //                //Xác định xem là luong Dự Án hay luong phòng ban
        //                DataTable tbdept = unitofwork.CongTrinhs.layCTDept(defaultNVId);
        //                while (curday.AddMonths(1) > ngaycuoithangdautien && (ngaydauthangdt <= nvtllst[nvtllst.Count - 1].ThoiGianHetHieuLuc || nvtllst[nvtllst.Count - 1].ThoiGianHetHieuLuc == null))
        //                {
        //                    if (curday < ngaycuoithangdautien)
        //                        ngaycuoithangdautien = curday;
        //                    tlnvs = TinhLuongNV(defaultNVId, ngaydauthangdt, ngaycuoithangdautien, thamsos, tbdept);
        //                    foreach (TinhLuongNVModel tlnv in tlnvs)
        //                    {
        //                        DataRow newrow = dt.NewRow();
        //                        newrow["ThoiGian"] = ngaybatdauhd.Month + "/" + ngaybatdauhd.Year;
        //                        newrow["LuongCoBan"] = tlnv.LuongCoBan;
        //                        newrow["LuongSanLuong"] = tlnv.LuongNhanVien;
        //                        newrow["CongNo"] = 0;
        //                        newrow["NhanVienID"] = defaultNVId.ToString();
        //                        newrow["ngaybatdauhd"] = tlnv.ngaybatdauhd.ToString("dd/MM/yyyy"); //ngaybatdauhd.ToString("dd/MM/yyyy");
        //                        newrow["ngaydauthangdt"] = tlnv.tungaynew.ToString("dd/MM/yyyy");//ngaydauthangdt.ToString("dd/MM/yyyy");
        //                        newrow["ngaycuoithangdautien"] = tlnv.denngaynew.ToString("dd/MM/yyyy");//ngaycuoithangdautien.ToString("dd/MM/yyyy");
        //                        newrow["checktinhbhxh"] = tlnv.checktinhbh;
        //                        newrow["checktinhthue"] = tlnv.checktinhthue;
        //                        newrow["SoTienTinhThue"] = tlnv.SoTienTinhThueTheoThang;
        //                        dt.Rows.Add(newrow);
        //                        tongtatca += tlnv.LuongNhanVien;
        //                        songuoigiantrugiacanh = tlnv.SoNguoiGiamTruGC;
        //                        tongluongtinhthue += tlnv.LuongTinhThue;
        //                        tongthueTheoThang += tlnv.Thue;
        //                        TongLuongChuaGiamTru += tlnv.TongLuongChuaGiamTru;
        //                        TongGiamTru += tlnv.TongLuongDaGiamTru;
        //                        //chi tiet giam tru
        //                        TGTThueThang += tlnv.Thue;
        //                        TGTBHXH += tlnv.GTBHXH;
        //                        TGTBHYT += tlnv.GTBHYT;
        //                        TGTCongDoan += tlnv.GTCongDoan;
        //                        TGTBHTN += tlnv.GTBHTN;
        //                    }
        //                    ngaycuoithangdautien = ngaydauthangdt.AddMonths(2).AddMinutes(-1);
        //                    ngaydauthangdt = ngaydauthangdt.AddMonths(1);
        //                    ngaybatdauhd = ngaydauthangdt;
        //                }
        //            }

        //            //}

        //            kq.listLSNhanLuong = unitofwork.LuongNhanViens.getLSnhanluongTheoNam(defaultNVId, NgayBatDauTinhLSnhanLuong, Ngayngungtinh);
        //            foreach (DataRow r in kq.listLSNhanLuong.Rows)
        //            {
        //                tongluongdatt += Convert.ToDouble(r[1]);
        //            }

        //            double thue = 0;
        //            if (nvtllst[0].ThueMacDinh == true)
        //            {
        //                thue = tongluongtinhthue * 0.1;
        //            }
        //            else
        //            {
        //                double luongnvtinhthue = tongluongtinhthue - ((9000000 + (songuoigiantrugiacanh * 3600000)) * (datenow.Year > Nam ? 12 : datenow.Month));
        //                thue = luongnvtinhthue > 0 ? (5 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 0, 5000000 * 12)) / 100)
        //                                         + (10 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 5000000 * 12, 10000000 * 12)) / 100)
        //                                         + (15 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 10000000 * 12, 18000000 * 12)) / 100)
        //                                         + (20 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 18000000 * 12, 32000000 * 12)) / 100)
        //                                         + (25 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 32000000 * 12, 52000000 * 12)) / 100)
        //                                         + (30 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 52000000 * 12, 80000000 * 12)) / 100)
        //                                          + (35 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 80000000 * 12, 9999999999)) / 100) : 0;
        //            }

        //            kq.ThueThuNhapCongTheoThang = tongthueTheoThang;
        //            kq.ThueThuNhapChenhLech = tongthueTheoThang - thue;
        //            tongluongchuatt = tongtatca - tongluongdatt + kq.ThueThuNhapChenhLech;
        //            kq.TongLuongChuaTT = tongluongchuatt;
        //            kq.TongLuongDaTT = tongluongdatt;
        //            kq.TongLuongtatca = tongtatca;
        //            kq.lisluongnv = dt;
        //            kq.ThueThuNhap = thue;
        //            kq.TongLuongTruocGiamTru = TongLuongChuaGiamTru;
        //            kq.TongGiamTru = TongGiamTru;
        //            // chi tiet giam tru
        //            kq.TGTThueThang = TGTThueThang;
        //            kq.TGTBHXH = TGTBHXH;
        //            kq.TGTBHYT = TGTBHYT;
        //            kq.TGTCongDoan = TGTCongDoan;
        //            kq.TGTBHTN = TGTBHTN;
        //        }
        //    }
        //    return kq;
        //}

        ///// <summary>
        ///// KienMT - Tính lương nhân viên theo tháng
        ///// </summary>
        ///// <param name="text"></param>
        ///// <returns></returns>
        //public static List<TinhLuongNVModel> TinhLuongNV(Guid NhanVienId, DateTime tungay, DateTime denngay, List<ThamSoTinhLuong> thamsos, DataTable tbdept)
        //{
        //    IUnitOfWork unitofwork = new UnitOfWork();
        //    List<TinhLuongNVModel> kqs = new List<TinhLuongNVModel>();
        //    double luongnv = 0;
        //    //lấy bảng lương nhân viên
        //    bool checktinhbhxh = false;
        //    double SoTienTongThangDeTinhThue = 0;

        //    List<LuongNhanVien> lnvs = unitofwork.LuongNhanViens.GetAll().Where(p => p.NhanVienID == NhanVienId && p.ThoiGianTinh < denngay && (p.ThoiGianHetHieuLuc == null || p.ThoiGianHetHieuLuc > tungay)).OrderBy(p => p.ThoiGianTinh).ToList();
        //    int dem = 0;
        //    foreach (LuongNhanVien lnv in lnvs)
        //    {
        //        double TongGiamTru = 0;
        //        double TongLuongChuaGiamTru = 0;
        //        TinhLuongNVModel kq = new TinhLuongNVModel();
        //        if (lnv != null && lnv.ThoiGianHetHieuLuc != null && lnv.ThoiGianHetHieuLuc < tungay)
        //        {
        //            return kqs;
        //        }
        //        if (lnv != null)
        //        {
        //            kq.LuongNVID = lnv.LuongNVID;
        //            //kq.HeSo = lnv.HeSo;
        //            kq.LuongBHXH = lnv.LuongBHXH;
        //            kq.LuongCoBan = lnv.LuongCoBan;
        //            kq.LuongLamThem = lnv.LuongNgoaiGio;
        //            kq.SoNguoiGiamTruGC = lnv.SoNguoiGiamTruGC != null ? (int)lnv.SoNguoiGiamTruGC : 0;
        //            //tinh so ngay lam viec
        //            //kiem tra xem ngay het han co nho hon đến ngày không, nếu nhỏ hơn thì đến ngày = ngày hết hạn
        //            DateTime denngaynew = denngay;
        //            DateTime tungaynew = tungay;
        //            if (lnv.ThoiGianHetHieuLuc != null && lnv.ThoiGianHetHieuLuc < denngay)
        //            {
        //                denngaynew = Convert.ToDateTime(lnv.ThoiGianHetHieuLuc);
        //            }
        //            if (lnv.ThoiGianTinh > tungay)
        //            {
        //                tungaynew = Convert.ToDateTime(lnv.ThoiGianTinh);
        //            }
        //            kq.tungaynew = tungaynew;
        //            kq.denngaynew = denngaynew;
        //            kq.ngaybatdauhd = lnv.ThoiGianTinh;
        //            float songaylamviec = Functions.BusinessDaysUntil(tungay, denngay, null);
        //            //songaylamviec = songaylamviec == 0 ? 1 : songaylamviec;
        //            //DateTime tempt = tungay;
        //            //if (lnv.ThoiGianTinh > tungay)
        //            //{
        //            //    tempt = lnv.ThoiGianTinh;
        //            //}
        //            //if (lnv.ThoiGianHetHieuLuc != null && lnv.ThoiGianHetHieuLuc < denngay)
        //            //{
        //            //    denngay = Convert.ToDateTime(lnv.ThoiGianHetHieuLuc);
        //            //}
        //            float songaythucte = Functions.BusinessDaysUntil(tungaynew, denngaynew, null);
        //            float songaythucte1 = Functions.BusinessDaysUntil(tungaynew, denngaynew, null);
        //            float tongsongaylvtrongthang = Functions.BusinessDaysUntil(tungay, DateTime.ParseExact("01/" + tungay.ToString("MM/yyyy"), "dd/MM/yyyy", null).AddMonths(1).AddMinutes(-1), null);
        //            //Lây tổng Luongcoban denngay
        //            DataTable tongLuongcoban = unitofwork.LuongNhanViens.getTongLuongCoBan(denngay);
        //            double tongLuongcobandouble = Convert.ToDouble(tongLuongcoban.Rows[0][0]);
        //            // tính tổng san luongj trong tháng

        //            //--Không tính lương theo sản lượng nữa edit by anhnt 06/06/17
        //            //DataTable sanluongdd = unitofwork.BaoCaoSanLuongs.getBaoCaoSanLuongTheoCongTrinhall(tungay, denngay);
        //            //double tongsanluong = Convert.ToDouble(sanluongdd.Rows[0][0].ToString() == "" ? "0" : sanluongdd.Rows[0][0].ToString()) / 1.1;
        //            double tongsanluong = 0;
        //            //DAnh gia cong viec
        //            DateTime ngaydanhgiacongviec = DateTime.ParseExact("01/" + tungay.ToString("MM/yyyy") + " 00:00", "dd/MM/yyyy HH:mm", null);
        //            var danhgiacongviec = unitofwork.DanhGiaCongViecs.GetHeSoDanhGiaCV(NhanVienId, ngaydanhgiacongviec);
        //            kq.TongSL = tongsanluong;
        //            if (lnv.LuongCoBan == 0 || songaylamviec == 0)
        //            {
        //                kq.LuongNhanVien = 0;
        //            }
        //            else
        //            {
        //                // lấy các khoản giảm trừ
        //                //List<ThamSoTinhLuong> thamsos = unitofwork.ThamSoTinhLuongs.GetAll().ToList();
        //                ThamSoTinhLuong tile = thamsos.Where(p => p.Loai == 1).FirstOrDefault();
        //                //Lấy xác nhận công việc
        //                DataTable lstKhaiBaoCV = unitofwork.KhaiBaoCVs.LayDSKhaiBaoCV(NhanVienId, tungaynew, denngaynew);
        //                double sogiolv = 0;
        //                double sogiolt = 0;
        //                songaythucte = 0;
        //                foreach (DataRow r in lstKhaiBaoCV.Rows)
        //                {
        //                    if (r["TrangThai"].ToString() == "2")
        //                    {
        //                        sogiolv += Convert.ToDouble(r["SoGioLamViec"].ToString() != "" ? r["SoGioLamViec"].ToString() : "0");
        //                        sogiolt += Convert.ToDouble(r["OverTime"].ToString() != "" ? r["OverTime"].ToString() : "0");
        //                        songaythucte += 1;
        //                    }
        //                }
        //                //Xác định xem là luong Dự Án hay luong phòng ban
        //                // DataTable tbdept = unitofwork.CongTrinhs.layCTDept(NhanVienId);
        //                //tính lương
        //                //chot ngay
        //                DateTime ngaychot = DateTime.ParseExact("01/06/2016", "dd/MM/yyyy", null);
        //                if (lnv.HinhThucTinhLuong == 2 || denngay < ngaychot)
        //                {

        //                    //luong Dự Án hay lương phòng ban
        //                    if (tbdept.Rows[0][0].ToString() == "0" || lnv.TinhLuongSanLuong == 1)
        //                    {
        //                        luongnv = lnv.LuongCoBan * (songaythucte1 / tongsongaylvtrongthang);
        //                        kq.SanLuongNhanVien = lnv.LuongCoBan;
        //                    }
        //                    else
        //                    {
        //                        luongnv = tongsanluong * ((tile != null ? Convert.ToDouble(tile.GiaTri) : 0) / 100) * (lnv.LuongCoBan / tongLuongcobandouble) * (songaythucte1 / songaylamviec);
        //                        kq.SanLuongNhanVien = tongsanluong * ((tile != null ? Convert.ToDouble(tile.GiaTri) : 0) / 100) * (lnv.LuongCoBan / tongLuongcobandouble);
        //                    }
        //                    kq.SoNgaycong = songaythucte1;
        //                    kq.NgayCong = songaythucte1;
        //                    kq.LuongSanLuong = luongnv;
        //                    //luong danh gia cong viec
        //                    if (danhgiacongviec != null && danhgiacongviec.Rows.Count > 0)
        //                    {
        //                        luongnv = luongnv + ((luongnv * Convert.ToDouble(danhgiacongviec.Rows[0]["TiLeLuong"]) / 100) - luongnv);
        //                        kq.GTGiamTruHS = (luongnv - (luongnv * Convert.ToDouble(danhgiacongviec.Rows[0]["TiLeLuong"]) / 100));
        //                        kq.LoaiDanhGiaCV = danhgiacongviec.Rows[0]["TenHeSo"] + "(" + danhgiacongviec.Rows[0]["TiLeLuong"] + ")";
        //                    }
        //                    else
        //                    {
        //                        kq.GTGiamTruHS = 0;
        //                    }

        //                    //Thuong phat
        //                    DataTable luongthuongphat = unitofwork.LuongNhanViens.getThuongPhat(NhanVienId, tungaynew, denngaynew);
        //                    if (luongthuongphat != null && luongthuongphat.Rows.Count > 0)
        //                    {
        //                        kq.LuongThuong = Convert.ToDouble(luongthuongphat.Rows[0][1].ToString());
        //                        kq.GTPhat = Convert.ToDouble(luongthuongphat.Rows[0][2].ToString());
        //                        luongnv = luongnv + (Convert.ToDouble(luongthuongphat.Rows[0][1].ToString()) - Convert.ToDouble(luongthuongphat.Rows[0][2].ToString()));
        //                    }
        //                    TongLuongChuaGiamTru += luongnv;
        //                    //double luonglamthemgio = 0;
        //                    //luonglamthemgio = lnv.LuongNgoaiGio * sogiolt;
        //                    //luongnv += luonglamthemgio;
        //                    if (checktinhbhxh == false && lnv.LuongBHXH != 0)
        //                    {
        //                        List<ThamSoTinhLuong> giamtrus = thamsos.Where(p => p.Loai == 2).ToList();
        //                        foreach (var gt in giamtrus)
        //                        {
        //                            if (gt.TenThamSo.ToUpper().Contains("BHXH"))
        //                            {
        //                                kq.GTBHXH = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                                kq.TLBHXH = Convert.ToDouble(gt.GiaTri);
        //                            }
        //                            else if (gt.TenThamSo.ToUpper().Contains("BHYT"))
        //                            {
        //                                kq.GTBHYT = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                                kq.TLBHYT = Convert.ToDouble(gt.GiaTri);
        //                            }
        //                            else if (gt.TenThamSo.ToUpper().Contains("THẤT NGHIỆP"))
        //                            {
        //                                kq.GTBHTN = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                                kq.TLTroCapThatNghiep = Convert.ToDouble(gt.GiaTri);
        //                            }
        //                            luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                            TongGiamTru += (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                        }
        //                        checktinhbhxh = true;
        //                        kq.checktinhbh = checktinhbhxh;
        //                    }
        //                    else
        //                    {
        //                        kq.checktinhbh = false;
        //                    }
        //                    double luongnvtinhthue = luongnv;
        //                    SoTienTongThangDeTinhThue += luongnvtinhthue;

        //                    if (dem == lnvs.Count - 1)
        //                    {
        //                        List<ThamSoTinhLuong> giamtrus2 = thamsos.Where(p => p.Loai == 3).ToList();
        //                        foreach (var gt in giamtrus2)
        //                        {
        //                            kq.GTCongDoan = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //                            luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //                            kq.TLCongDoan = Convert.ToDouble(gt.GiaTri);
        //                            TongGiamTru += kq.GTCongDoan;
        //                        }
        //                        double thue = 0;
        //                        if (lnv.ThueMacDinh == true)
        //                        {
        //                            thue = SoTienTongThangDeTinhThue * 0.1;
        //                        }
        //                        else
        //                        {
        //                            double luongnvtinhthuetemp = SoTienTongThangDeTinhThue - ((9000000 + ((lnv.SoNguoiGiamTruGC != null ? Convert.ToDouble(lnv.SoNguoiGiamTruGC) : 0) * 3600000)));
        //                            thue = luongnvtinhthuetemp > 0 ? (5 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 0, 5000000)) / 100)
        //                                                    + (10 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 5000000, 10000000)) / 100)
        //                                                    + (15 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 10000000, 18000000)) / 100)
        //                                                    + (20 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 18000000, 32000000)) / 100)
        //                                                    + (25 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 32000000, 52000000)) / 100)
        //                                                    + (30 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 52000000, 80000000)) / 100)
        //                                                     + (35 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 80000000, 9999999999)) / 100) : 0;
        //                        }
        //                        kq.Thue = (double)Math.Round(Convert.ToDecimal(thue), 0);
        //                        kq.LuongNhanVien = (double)Math.Round(Convert.ToDecimal(luongnv - thue), 0);
        //                        kq.checktinhthue = true;
        //                        kq.SoTienTinhThueTheoThang = SoTienTongThangDeTinhThue;
        //                        TongGiamTru += kq.Thue;
        //                    }
        //                    else
        //                    {
        //                        kq.Thue = 0;
        //                        kq.checktinhthue = false;
        //                        kq.LuongNhanVien = (double)Math.Round(Convert.ToDecimal(luongnv), 0);
        //                        kq.SoTienTinhThueTheoThang = 0;
        //                        kq.GTCongDoan = 0;
        //                        kq.TLCongDoan = 0;
        //                    }
        //                    kq.LuongTinhThue = (double)Math.Round(Convert.ToDecimal(luongnvtinhthue), 0);
        //                    //  kq.checktinhthue = false;

        //                    kq.TongThu = kq.LuongSanLuong + kq.LuongNgoaiGio + kq.LuongThuong;
        //                    kq.TongGiamTru = kq.GTBHXH + kq.GTBHYT + kq.GTBHTN + kq.GTCongDoan + kq.GTPhat + kq.Thue;
        //                    kq.ThucLinh = kq.LuongNhanVien;
        //                    kq.SoNguoiGiamTruGC = lnv.SoNguoiGiamTruGC != null ? Convert.ToInt16(lnv.SoNguoiGiamTruGC) : 0;
        //                    kq.TongLuongDaGiamTru = TongGiamTru;
        //                    kq.TongLuongChuaGiamTru = TongLuongChuaGiamTru;
        //                }
        //                else //if (sogiolv != 0)
        //                {
        //                    //luong Dự Án hay lương phòng ban
        //                    if (tbdept.Rows[0][0].ToString() == "0" || lnv.TinhLuongSanLuong == 1)
        //                    {
        //                        luongnv = lnv.LuongCoBan * ((sogiolv / 8) / tongsongaylvtrongthang);
        //                        kq.SanLuongNhanVien = lnv.LuongCoBan;
        //                    }
        //                    else
        //                    {
        //                        luongnv = tongsanluong * ((tile != null ? Convert.ToDouble(tile.GiaTri) : 0) / 100) * (lnv.LuongCoBan / tongLuongcobandouble) * ((sogiolv / 8) / songaylamviec);
        //                        kq.SanLuongNhanVien = tongsanluong * ((tile != null ? Convert.ToDouble(tile.GiaTri) : 0) / 100) * (lnv.LuongCoBan / tongLuongcobandouble);
        //                    }

        //                    kq.SoNgaycong = (sogiolv / 8);
        //                    kq.LuongSanLuong = luongnv;
        //                    kq.NgayCong = (sogiolv / 8);
        //                    //luong danh gia cong viec
        //                    if (danhgiacongviec != null && danhgiacongviec.Rows.Count > 0)
        //                    {
        //                        luongnv = luongnv + ((luongnv * Convert.ToDouble(danhgiacongviec.Rows[0]["TiLeLuong"]) / 100) - luongnv);
        //                        kq.GTGiamTruHS = (luongnv - (luongnv * Convert.ToDouble(danhgiacongviec.Rows[0]["TiLeLuong"]) / 100));
        //                        kq.LoaiDanhGiaCV = danhgiacongviec.Rows[0]["TenHeSo"] + "(" + danhgiacongviec.Rows[0]["TiLeLuong"] + ")";
        //                    }
        //                    else
        //                    {
        //                        kq.GTGiamTruHS = 0;
        //                    }

        //                    //Thuong phat
        //                    DataTable luongthuongphat = unitofwork.LuongNhanViens.getThuongPhat(NhanVienId, tungaynew, denngaynew);
        //                    if (luongthuongphat != null && luongthuongphat.Rows.Count > 0)
        //                    {
        //                        kq.LuongThuong = Convert.ToDouble(luongthuongphat.Rows[0][1].ToString());
        //                        kq.GTPhat = Convert.ToDouble(luongthuongphat.Rows[0][2].ToString());
        //                        luongnv = luongnv + (Convert.ToDouble(luongthuongphat.Rows[0][1].ToString()) - Convert.ToDouble(luongthuongphat.Rows[0][2].ToString()));
        //                    }
        //                    double luonglamthemgio = 0;
        //                    luonglamthemgio = lnv.LuongNgoaiGio * sogiolt * 1.5;
        //                    kq.LuongNgoaiGio = luonglamthemgio;
        //                    luongnv += luonglamthemgio;
        //                    TongLuongChuaGiamTru += luongnv;
        //                    if (checktinhbhxh == false && lnv.LuongBHXH != 0)
        //                    {
        //                        List<ThamSoTinhLuong> giamtrus = thamsos.Where(p => p.Loai == 2).ToList();
        //                        foreach (var gt in giamtrus)
        //                        {
        //                            if (gt.TenThamSo.ToUpper().Contains("BHXH"))
        //                            {
        //                                kq.GTBHXH = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                                kq.TLBHXH = Convert.ToDouble(gt.GiaTri);
        //                            }
        //                            else if (gt.TenThamSo.ToUpper().Contains("BHYT"))
        //                            {
        //                                kq.GTBHYT = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                                kq.TLBHYT = Convert.ToDouble(gt.GiaTri);
        //                            }
        //                            else if (gt.TenThamSo.ToUpper().Contains("THẤT NGHIỆP"))
        //                            {
        //                                kq.GTBHTN = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                                kq.TLTroCapThatNghiep = Convert.ToDouble(gt.GiaTri);
        //                            }
        //                            luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                            TongGiamTru += (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                        }
        //                        checktinhbhxh = true;
        //                        kq.checktinhbh = checktinhbhxh;
        //                    }
        //                    else
        //                    {
        //                        kq.checktinhbh = false;
        //                    }
        //                    double luongnvtinhthue = luongnv;
        //                    SoTienTongThangDeTinhThue += luongnvtinhthue;

        //                    if (dem == lnvs.Count - 1)
        //                    {
        //                        List<ThamSoTinhLuong> giamtrus2 = thamsos.Where(p => p.Loai == 3).ToList();
        //                        foreach (var gt in giamtrus2)
        //                        {
        //                            kq.GTCongDoan = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //                            luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //                            kq.TLCongDoan = Convert.ToDouble(gt.GiaTri);
        //                            TongGiamTru += kq.GTCongDoan;
        //                        }
        //                        double thue = 0;
        //                        if (lnv.ThueMacDinh == true)
        //                        {
        //                            thue = SoTienTongThangDeTinhThue * 0.1;
        //                        }
        //                        else
        //                        {
        //                            double luongnvtinhthuetemp = SoTienTongThangDeTinhThue - ((9000000 + ((lnv.SoNguoiGiamTruGC != null ? Convert.ToDouble(lnv.SoNguoiGiamTruGC) : 0) * 3600000)));
        //                            thue = luongnvtinhthuetemp > 0 ? (5 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 0, 5000000)) / 100)
        //                                                    + (10 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 5000000, 10000000)) / 100)
        //                                                    + (15 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 10000000, 18000000)) / 100)
        //                                                    + (20 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 18000000, 32000000)) / 100)
        //                                                    + (25 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 32000000, 52000000)) / 100)
        //                                                    + (30 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 52000000, 80000000)) / 100)
        //                                                     + (35 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 80000000, 9999999999)) / 100) : 0;
        //                        }
        //                        kq.Thue = (double)Math.Round(Convert.ToDecimal(thue), 0);
        //                        kq.LuongNhanVien = (double)Math.Round(Convert.ToDecimal(luongnv - thue), 0);
        //                        kq.checktinhthue = true;
        //                        kq.SoTienTinhThueTheoThang = SoTienTongThangDeTinhThue;
        //                        TongGiamTru += kq.Thue;
        //                    }
        //                    else
        //                    {
        //                        kq.Thue = 0;
        //                        kq.checktinhthue = false;
        //                        kq.LuongNhanVien = (double)Math.Round(Convert.ToDecimal(luongnv), 0);
        //                        kq.SoTienTinhThueTheoThang = 0;
        //                        kq.GTCongDoan = 0;
        //                        kq.TLCongDoan = 0;
        //                    }
        //                    kq.LuongTinhThue = (double)Math.Round(Convert.ToDecimal(luongnvtinhthue), 0);
        //                    kq.TongThu = kq.LuongSanLuong + kq.LuongNgoaiGio + kq.LuongThuong;
        //                    //   kq.TongGiamTru = kq.GTBHXH + kq.GTBHYT + kq.GTBHTN + kq.GTCongDoan + kq.GTPhat + kq.GTThueThuNhapCN;
        //                    kq.TongGiamTru = kq.GTBHXH + kq.GTBHYT + kq.GTBHTN + kq.GTCongDoan + kq.GTPhat + kq.Thue + kq.GTGiamTruHS;
        //                    kq.ThucLinh = kq.LuongNhanVien;
        //                    kq.QuiDuPhong = (kq.SanLuongNhanVien - kq.LuongSanLuong) + kq.GTGiamTruHS + kq.GTPhat;
        //                    kq.SoNguoiGiamTruGC = lnv.SoNguoiGiamTruGC != null ? Convert.ToInt16(lnv.SoNguoiGiamTruGC) : 0;
        //                    kq.TongLuongDaGiamTru = TongGiamTru;
        //                    kq.TongLuongChuaGiamTru = TongLuongChuaGiamTru;
        //                    //Thưởng phat
        //                }
        //            }
        //        }
        //        kqs.Add(kq);
        //        dem += 1;
        //    }

        //    return kqs;
        //}

        ///// <summary>
        ///// KienMT - Tính lương nhân viên theo tháng
        ///// </summary>
        ///// <param name="text"></param>
        ///// <returns></returns>
        //public static List<TinhLuongNVModel> TinhLuongNVChot(Guid NhanVienId, DateTime tungay, DateTime denngay, Guid TGChot, List<Chot_ThamSoTinhLuong> thamsos, DataTable tbdept)
        //{
        //    IUnitOfWork unitofwork = new UnitOfWork();
        //    List<TinhLuongNVModel> kqs = new List<TinhLuongNVModel>();
        //    double luongnv = 0;
        //    //lấy bảng lương nhân viên
        //    bool checktinhbhxh = false;
        //    double SoTienTongThangDeTinhThue = 0;

        //    List<Chot_LuongNhanVien> lnvs = unitofwork.Chot_LuongNhanViens.GetAll().Where(p => p.ThoiGianChotID == TGChot && p.NhanVienID == NhanVienId && p.ThoiGianTinh < denngay && (p.ThoiGianHetHieuLuc == null || p.ThoiGianHetHieuLuc > tungay)).OrderBy(p => p.ThoiGianTinh).ToList();
        //    int dem = 0;
        //    foreach (Chot_LuongNhanVien lnv in lnvs)
        //    {
        //        double TongGiamTru = 0;
        //        double TongLuongChuaGiamTru = 0;
        //        TinhLuongNVModel kq = new TinhLuongNVModel();
        //        if (lnv != null && lnv.ThoiGianHetHieuLuc != null && lnv.ThoiGianHetHieuLuc < tungay)
        //        {
        //            return kqs;
        //        }
        //        if (lnv != null)
        //        {
        //            kq.LuongNVID = lnv.LuongNVID;
        //            kq.HeSo = lnv.HeSo;
        //            kq.LuongBHXH = lnv.LuongBHXH;
        //            kq.LuongCoBan = lnv.LuongCoBan;
        //            kq.LuongLamThem = lnv.LuongNgoaiGio;
        //            kq.SoNguoiGiamTruGC = lnv.SoNguoiGiamTruGC != null ? (int)lnv.SoNguoiGiamTruGC : 0;
        //            //tinh so ngay lam viec
        //            //kiem tra xem ngay het han co nho hon đến ngày không, nếu nhỏ hơn thì đến ngày = ngày hết hạn
        //            DateTime denngaynew = denngay;
        //            DateTime tungaynew = tungay;
        //            if (lnv.ThoiGianHetHieuLuc != null && lnv.ThoiGianHetHieuLuc < denngay)
        //            {
        //                denngaynew = Convert.ToDateTime(lnv.ThoiGianHetHieuLuc);
        //            }
        //            if (lnv.ThoiGianTinh > tungay)
        //            {
        //                tungaynew = Convert.ToDateTime(lnv.ThoiGianTinh);
        //            }
        //            kq.tungaynew = tungaynew;
        //            kq.denngaynew = denngaynew;
        //            kq.ngaybatdauhd = lnv.ThoiGianTinh;
        //            float songaylamviec = Functions.BusinessDaysUntil(tungay, denngay, null);
        //            //songaylamviec = songaylamviec == 0 ? 1 : songaylamviec;
        //            //DateTime tempt = tungay;
        //            //if (lnv.ThoiGianTinh > tungay)
        //            //{
        //            //    tempt = lnv.ThoiGianTinh;
        //            //}
        //            //if (lnv.ThoiGianHetHieuLuc != null && lnv.ThoiGianHetHieuLuc < denngay)
        //            //{
        //            //    denngay = Convert.ToDateTime(lnv.ThoiGianHetHieuLuc);
        //            //}
        //            float songaythucte = Functions.BusinessDaysUntil(tungaynew, denngaynew, null);
        //            float songaythucte1 = Functions.BusinessDaysUntil(tungaynew, denngaynew, null);
        //            float tongsongaylvtrongthang = Functions.BusinessDaysUntil(tungay, DateTime.ParseExact("01/" + tungay.ToString("MM/yyyy"), "dd/MM/yyyy", null).AddMonths(1).AddMinutes(-1), null);
        //            //Lây tổng Luongcoban denngay
        //            DataTable tongLuongcoban = unitofwork.Chot_LuongNhanViens.getTongLuongCoBan(denngay, TGChot);
        //            double tongLuongcobandouble = Convert.ToDouble(tongLuongcoban.Rows[0][0]);
        //            // tính tổng san luongj trong tháng
        //            //bỏ sản lượng tính lương
        //            //DataTable sanluongdd = unitofwork.Chot_BaoCaoSanLuongs.getChot_BaoCaoSanLuongTheoCongTrinhall(tungay, denngay, TGChot);
        //            double tongsanluong = 0;// Convert.ToDouble(sanluongdd.Rows[0][0].ToString() == "" ? "0" : sanluongdd.Rows[0][0].ToString()) / 1.1;
        //            //DAnh gia cong viec
        //            DateTime ngaydanhgiacongviec = DateTime.ParseExact("01/" + tungay.ToString("MM/yyyy") + " 00:00", "dd/MM/yyyy HH:mm", null);
        //            var danhgiacongviec = unitofwork.Chot_DanhGiaCongViecs.GetHeSoDanhGiaCV(NhanVienId, ngaydanhgiacongviec, TGChot);
        //            kq.TongSL = tongsanluong;
        //            if (lnv.LuongCoBan == 0 || songaylamviec == 0)
        //            {
        //                kq.LuongNhanVien = 0;
        //            }
        //            else
        //            {
        //                Chot_ThamSoTinhLuong tile = thamsos.Where(p => p.Loai == 1).FirstOrDefault();
        //                //Lấy xác nhận công việc
        //                DataTable lstKhaiBaoCV = unitofwork.Chot_KhaiBaoCVs.LayDSChot_KhaiBaoCV(NhanVienId, tungaynew, denngaynew, TGChot);
        //                double sogiolv = 0;
        //                double sogiolt = 0;
        //                songaythucte = 0;
        //                foreach (DataRow r in lstKhaiBaoCV.Rows)
        //                {
        //                    if (r["TrangThai"].ToString() == "2")
        //                    {
        //                        sogiolv += Convert.ToDouble(r["SoGioLamViec"].ToString() != "" ? r["SoGioLamViec"].ToString() : "0");
        //                        sogiolt += Convert.ToDouble(r["OverTime"].ToString() != "" ? r["OverTime"].ToString() : "0");
        //                        songaythucte += 1;
        //                    }
        //                }
        //                //tính lương
        //                //chot ngay
        //                DateTime ngaychot = DateTime.ParseExact("01/06/2016", "dd/MM/yyyy", null);
        //                if (lnv.HinhThucTinhLuong == 2 || denngay < ngaychot)
        //                {
        //                    //luong Dự Án hay lương phòng ban
        //                    if (tbdept.Rows[0][0].ToString() == "0" || lnv.TinhLuongSanLuong == 1)
        //                    {
        //                        luongnv = lnv.LuongCoBan * (songaythucte1 / tongsongaylvtrongthang);
        //                        kq.SanLuongNhanVien = lnv.LuongCoBan;
        //                    }
        //                    else
        //                    {
        //                        luongnv = tongsanluong * ((tile != null ? Convert.ToDouble(tile.GiaTri) : 0) / 100) * (lnv.LuongCoBan / tongLuongcobandouble) * (songaythucte1 / songaylamviec);
        //                        kq.SanLuongNhanVien = tongsanluong * ((tile != null ? Convert.ToDouble(tile.GiaTri) : 0) / 100) * (lnv.LuongCoBan / tongLuongcobandouble);
        //                    }
        //                    kq.SoNgaycong = songaythucte1;
        //                    kq.NgayCong = songaythucte1;
        //                    kq.LuongSanLuong = luongnv;
        //                    //luong danh gia cong viec
        //                    if (danhgiacongviec != null && danhgiacongviec.Rows.Count > 0)
        //                    {
        //                        luongnv = luongnv + ((luongnv * Convert.ToDouble(danhgiacongviec.Rows[0]["TiLeLuong"]) / 100) - luongnv);
        //                        kq.GTGiamTruHS = (luongnv - (luongnv * Convert.ToDouble(danhgiacongviec.Rows[0]["TiLeLuong"]) / 100));
        //                        kq.LoaiDanhGiaCV = danhgiacongviec.Rows[0]["TenHeSo"] + "(" + danhgiacongviec.Rows[0]["TiLeLuong"] + ")";
        //                    }
        //                    else
        //                    {
        //                        kq.GTGiamTruHS = 0;
        //                    }

        //                    //Thuong phat
        //                    DataTable luongthuongphat = unitofwork.Chot_LuongNhanViens.getThuongPhat(NhanVienId, TGChot);
        //                    if (luongthuongphat != null && luongthuongphat.Rows.Count > 0)
        //                    {
        //                        kq.LuongThuong = Convert.ToDouble(luongthuongphat.Rows[0][1].ToString());
        //                        kq.GTPhat = Convert.ToDouble(luongthuongphat.Rows[0][2].ToString());
        //                        luongnv = luongnv + (Convert.ToDouble(luongthuongphat.Rows[0][1].ToString()) - Convert.ToDouble(luongthuongphat.Rows[0][2].ToString()));
        //                    }
        //                    TongLuongChuaGiamTru += luongnv;
        //                    //double luonglamthemgio = 0;
        //                    //luonglamthemgio = lnv.LuongNgoaiGio * sogiolt;
        //                    //luongnv += luonglamthemgio;
        //                    if (checktinhbhxh == false && lnv.LuongBHXH != 0)
        //                    {
        //                        List<Chot_ThamSoTinhLuong> giamtrus = thamsos.Where(p => p.Loai == 2).ToList();
        //                        foreach (var gt in giamtrus)
        //                        {
        //                            if (gt.TenThamSo.ToUpper().Contains("BHXH"))
        //                            {
        //                                kq.GTBHXH = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                                kq.TLBHXH = Convert.ToDouble(gt.GiaTri);
        //                            }
        //                            else if (gt.TenThamSo.ToUpper().Contains("BHYT"))
        //                            {
        //                                kq.GTBHYT = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                                kq.TLBHYT = Convert.ToDouble(gt.GiaTri);
        //                            }
        //                            else if (gt.TenThamSo.ToUpper().Contains("THẤT NGHIỆP"))
        //                            {
        //                                kq.GTBHTN = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                                kq.TLTroCapThatNghiep = Convert.ToDouble(gt.GiaTri);
        //                            }
        //                            luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                            TongGiamTru += (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                        }
        //                        checktinhbhxh = true;
        //                        kq.checktinhbh = checktinhbhxh;
        //                    }
        //                    else
        //                    {
        //                        kq.checktinhbh = false;
        //                    }
        //                    double luongnvtinhthue = luongnv;
        //                    SoTienTongThangDeTinhThue += luongnvtinhthue;

        //                    if (dem == lnvs.Count - 1)
        //                    {
        //                        List<Chot_ThamSoTinhLuong> giamtrus2 = thamsos.Where(p => p.Loai == 3).ToList();
        //                        foreach (var gt in giamtrus2)
        //                        {
        //                            kq.GTCongDoan = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //                            luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //                            kq.TLCongDoan = Convert.ToDouble(gt.GiaTri);
        //                            TongGiamTru += kq.GTCongDoan;
        //                        }
        //                        double thue = 0;
        //                        if (lnv.ThueMacDinh == true)
        //                        {
        //                            thue = SoTienTongThangDeTinhThue * 0.1;
        //                        }
        //                        else
        //                        {
        //                            double luongnvtinhthuetemp = SoTienTongThangDeTinhThue - ((9000000 + ((lnv.SoNguoiGiamTruGC != null ? Convert.ToDouble(lnv.SoNguoiGiamTruGC) : 0) * 3600000)));
        //                            thue = luongnvtinhthuetemp > 0 ? (5 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 0, 5000000)) / 100)
        //                                                    + (10 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 5000000, 10000000)) / 100)
        //                                                    + (15 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 10000000, 18000000)) / 100)
        //                                                    + (20 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 18000000, 32000000)) / 100)
        //                                                    + (25 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 32000000, 52000000)) / 100)
        //                                                    + (30 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 52000000, 80000000)) / 100)
        //                                                     + (35 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 80000000, 9999999999)) / 100) : 0;
        //                        }
        //                        kq.Thue = (double)Math.Round(Convert.ToDecimal(thue), 0);
        //                        kq.LuongNhanVien = (double)Math.Round(Convert.ToDecimal(luongnv - thue), 0);
        //                        kq.checktinhthue = true;
        //                        kq.SoTienTinhThueTheoThang = SoTienTongThangDeTinhThue;
        //                        TongGiamTru += kq.Thue;
        //                    }
        //                    else
        //                    {
        //                        kq.Thue = 0;
        //                        kq.checktinhthue = false;
        //                        kq.LuongNhanVien = (double)Math.Round(Convert.ToDecimal(luongnv), 0);
        //                        kq.SoTienTinhThueTheoThang = 0;
        //                        kq.GTCongDoan = 0;
        //                        kq.TLCongDoan = 0;
        //                    }
        //                    kq.LuongTinhThue = (double)Math.Round(Convert.ToDecimal(luongnvtinhthue), 0);
        //                    kq.TongThu = kq.LuongSanLuong + kq.LuongNgoaiGio + kq.LuongThuong;
        //                    kq.TongGiamTru = kq.GTBHXH + kq.GTBHYT + kq.GTBHTN + kq.GTCongDoan + kq.GTPhat + kq.Thue;
        //                    kq.ThucLinh = kq.LuongNhanVien;
        //                    kq.SoNguoiGiamTruGC = lnv.SoNguoiGiamTruGC != null ? Convert.ToInt16(lnv.SoNguoiGiamTruGC) : 0;
        //                    kq.TongLuongDaGiamTru = TongGiamTru;
        //                    kq.TongLuongChuaGiamTru = TongLuongChuaGiamTru;
        //                    //kq.checktinhbh = checktinhbhxh;
        //                }
        //                else //if (sogiolv != 0)
        //                {
        //                    //luong Dự Án hay lương phòng ban
        //                    if (tbdept.Rows[0][0].ToString() == "0" || lnv.TinhLuongSanLuong == 1)
        //                    {
        //                        luongnv = lnv.LuongCoBan * ((sogiolv / 8) / tongsongaylvtrongthang);
        //                        kq.SanLuongNhanVien = lnv.LuongCoBan;
        //                    }
        //                    else
        //                    {
        //                        luongnv = tongsanluong * ((tile != null ? Convert.ToDouble(tile.GiaTri) : 0) / 100) * (lnv.LuongCoBan / tongLuongcobandouble) * ((sogiolv / 8) / songaylamviec);
        //                        kq.SanLuongNhanVien = tongsanluong * ((tile != null ? Convert.ToDouble(tile.GiaTri) : 0) / 100) * (lnv.LuongCoBan / tongLuongcobandouble);
        //                    }

        //                    kq.SoNgaycong = (sogiolv / 8);
        //                    kq.LuongSanLuong = luongnv;
        //                    kq.NgayCong = (sogiolv / 8);
        //                    //luong danh gia cong viec
        //                    if (danhgiacongviec != null && danhgiacongviec.Rows.Count > 0)
        //                    {
        //                        luongnv = luongnv + ((luongnv * Convert.ToDouble(danhgiacongviec.Rows[0]["TiLeLuong"]) / 100) - luongnv);
        //                        kq.GTGiamTruHS = (luongnv - (luongnv * Convert.ToDouble(danhgiacongviec.Rows[0]["TiLeLuong"]) / 100));
        //                        kq.LoaiDanhGiaCV = danhgiacongviec.Rows[0]["TenHeSo"] + "(" + danhgiacongviec.Rows[0]["TiLeLuong"] + ")";
        //                    }
        //                    else
        //                    {
        //                        kq.GTGiamTruHS = 0;
        //                    }

        //                    //Thuong phat
        //                    DataTable luongthuongphat = unitofwork.Chot_LuongNhanViens.getThuongPhat(NhanVienId, TGChot);
        //                    if (luongthuongphat != null && luongthuongphat.Rows.Count > 0)
        //                    {
        //                        kq.LuongThuong = Convert.ToDouble(luongthuongphat.Rows[0][1].ToString());
        //                        kq.GTPhat = Convert.ToDouble(luongthuongphat.Rows[0][2].ToString());
        //                        luongnv = luongnv + (Convert.ToDouble(luongthuongphat.Rows[0][1].ToString()) - Convert.ToDouble(luongthuongphat.Rows[0][2].ToString()));
        //                    }
        //                    double luonglamthemgio = 0;
        //                    luonglamthemgio = lnv.LuongNgoaiGio * sogiolt * 1.5;
        //                    kq.LuongNgoaiGio = luonglamthemgio;
        //                    luongnv += luonglamthemgio;
        //                    TongLuongChuaGiamTru += luongnv;
        //                    if (checktinhbhxh == false && lnv.LuongBHXH != 0)
        //                    {
        //                        List<Chot_ThamSoTinhLuong> giamtrus = thamsos.Where(p => p.Loai == 2).ToList();
        //                        foreach (var gt in giamtrus)
        //                        {
        //                            if (gt.TenThamSo.ToUpper().Contains("BHXH"))
        //                            {
        //                                kq.GTBHXH = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                                kq.TLBHXH = Convert.ToDouble(gt.GiaTri);
        //                            }
        //                            else if (gt.TenThamSo.ToUpper().Contains("BHYT"))
        //                            {
        //                                kq.GTBHYT = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                                kq.TLBHYT = Convert.ToDouble(gt.GiaTri);
        //                            }
        //                            else if (gt.TenThamSo.ToUpper().Contains("THẤT NGHIỆP"))
        //                            {
        //                                kq.GTBHTN = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                                kq.TLTroCapThatNghiep = Convert.ToDouble(gt.GiaTri);
        //                            }
        //                            luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                            TongGiamTru += (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                        }
        //                        checktinhbhxh = true;
        //                        kq.checktinhbh = checktinhbhxh;
        //                    }
        //                    else
        //                    {
        //                        kq.checktinhbh = false;
        //                    }
        //                    double luongnvtinhthue = luongnv;
        //                    SoTienTongThangDeTinhThue += luongnvtinhthue;

        //                    if (dem == lnvs.Count - 1)
        //                    {
        //                        List<Chot_ThamSoTinhLuong> giamtrus2 = thamsos.Where(p => p.Loai == 3).ToList();
        //                        foreach (var gt in giamtrus2)
        //                        {
        //                            kq.GTCongDoan = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //                            luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //                            kq.TLCongDoan = Convert.ToDouble(gt.GiaTri);
        //                            TongGiamTru += kq.GTCongDoan;
        //                        }
        //                        double thue = 0;
        //                        if (lnv.ThueMacDinh == true)
        //                        {
        //                            thue = SoTienTongThangDeTinhThue * 0.1;
        //                        }
        //                        else
        //                        {
        //                            double luongnvtinhthuetemp = SoTienTongThangDeTinhThue - ((9000000 + ((lnv.SoNguoiGiamTruGC != null ? Convert.ToDouble(lnv.SoNguoiGiamTruGC) : 0) * 3600000)));
        //                            thue = luongnvtinhthuetemp > 0 ? (5 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 0, 5000000)) / 100)
        //                                                    + (10 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 5000000, 10000000)) / 100)
        //                                                    + (15 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 10000000, 18000000)) / 100)
        //                                                    + (20 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 18000000, 32000000)) / 100)
        //                                                    + (25 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 32000000, 52000000)) / 100)
        //                                                    + (30 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 52000000, 80000000)) / 100)
        //                                                     + (35 * Convert.ToDouble(TrongKhoang(luongnvtinhthuetemp, 80000000, 9999999999)) / 100) : 0;
        //                        }
        //                        kq.Thue = (double)Math.Round(Convert.ToDecimal(thue), 0);
        //                        kq.LuongNhanVien = (double)Math.Round(Convert.ToDecimal(luongnv - thue), 0);
        //                        kq.checktinhthue = true;
        //                        kq.SoTienTinhThueTheoThang = SoTienTongThangDeTinhThue;
        //                        TongGiamTru += kq.Thue;
        //                    }
        //                    else
        //                    {
        //                        kq.Thue = 0;
        //                        kq.checktinhthue = false;
        //                        kq.LuongNhanVien = (double)Math.Round(Convert.ToDecimal(luongnv), 0);
        //                        kq.SoTienTinhThueTheoThang = 0;
        //                        kq.GTCongDoan = 0;
        //                        kq.TLCongDoan = 0;
        //                    }
        //                    kq.LuongTinhThue = (double)Math.Round(Convert.ToDecimal(luongnvtinhthue), 0);
        //                    kq.TongThu = kq.LuongSanLuong + kq.LuongNgoaiGio + kq.LuongThuong;
        //                    kq.TongGiamTru = kq.GTBHXH + kq.GTBHYT + kq.GTBHTN + kq.GTCongDoan + kq.GTPhat + kq.Thue + kq.GTGiamTruHS;
        //                    kq.ThucLinh = kq.LuongNhanVien;
        //                    kq.QuiDuPhong = (kq.SanLuongNhanVien - kq.LuongSanLuong) + kq.GTGiamTruHS + kq.GTPhat;
        //                    kq.SoNguoiGiamTruGC = lnv.SoNguoiGiamTruGC != null ? Convert.ToInt16(lnv.SoNguoiGiamTruGC) : 0;
        //                    kq.TongLuongDaGiamTru = TongGiamTru;
        //                    kq.TongLuongChuaGiamTru = TongLuongChuaGiamTru;
        //                    //Thưởng phat
        //                }
        //            }
        //        }
        //        kqs.Add(kq);
        //        dem += 1;
        //    }
        //    return kqs;
        //}
        ///// <summary>
        ///// KienMT - Tính lương nhân viên cho đăng kiên
        ///// </summary>
        ///// <param name="text"></param>
        ///// <returns></returns>
        //public static TinhLuongNVModel TinhLuongNVDangKien(Guid NhanVienId, DateTime tungay, DateTime denngay)
        //{
        //    IUnitOfWork unitofwork = new UnitOfWork();
        //    TinhLuongNVModel kq = new TinhLuongNVModel();
        //    double luongnv = 0;
        //    //lấy bảng lương nhân viên
        //    double tongsongaylvtrongthang = Functions.BusinessDaysUntil(tungay, tungay.AddMonths(1).AddMinutes(-1), null);
        //    NhanVien nhanv = unitofwork.NhanViens.GetAll().Where(p => p.NhanVienID == NhanVienId && p.IsActive == true).FirstOrDefault();
        //    //if (nhanv.LoaiNhanVienDangKien == 1) --- tạm thời comment dòng này vì phải sửa thằng nhân viên trước, sửa xong phải xóa comment này đi.
        //    //{
        //    //    tongsongaylvtrongthang = (tungay.AddMonths(1).AddMinutes(-1) - tungay).TotalDays;
        //    //}
        //    LuongNhanVien lnv = unitofwork.LuongNhanViens.GetAll().Where(p => p.NhanVienID == NhanVienId && p.ThoiGianTinh < denngay).OrderByDescending(p => p.ThoiGianTinh).FirstOrDefault();
        //    if (lnv != null && lnv.ThoiGianHetHieuLuc != null && lnv.ThoiGianHetHieuLuc < tungay)
        //    {
        //        return kq;
        //    }
        //    if (lnv != null)
        //    {
        //        kq.LuongNVID = lnv.LuongNVID;
        //        kq.HeSo = lnv.HeSo;
        //        kq.LuongBHXH = lnv.LuongBHXH;
        //        kq.LuongCoBan = lnv.LuongCoBan;
        //        kq.LuongLamThem = lnv.LuongNgoaiGio;
        //        kq.SoNguoiGiamTruGC = lnv.SoNguoiGiamTruGC != null ? (int)lnv.SoNguoiGiamTruGC : 0;

        //        // lấy các khoản giảm trừ
        //        List<ThamSoTinhLuong> thamsos = unitofwork.ThamSoTinhLuongs.GetAll().ToList();
        //        ThamSoTinhLuong tile = thamsos.Where(p => p.Loai == 1).FirstOrDefault();
        //        //Lấy xác nhận công việc
        //        DKChamCong songaycong = unitofwork.DKChamCongs.GetAll().Where(p => p.NhanVienID == NhanVienId && p.ThoiGian < denngay && p.ThoiGian >= tungay).FirstOrDefault();
        //        double songaythucte1 = 0;
        //        if (songaycong != null)
        //        {
        //            songaythucte1 = songaycong.SoNgayCong;
        //        }
        //        luongnv = lnv.LuongCoBan * (songaythucte1 / tongsongaylvtrongthang);
        //        kq.SanLuongNhanVien = lnv.LuongCoBan;

        //        kq.SoNgaycong = songaythucte1;
        //        kq.NgayCong = songaythucte1;
        //        kq.LuongSanLuong = luongnv;
        //        //Thuong phat
        //        DataTable luongthuongphat = unitofwork.LuongNhanViens.getThuongPhat(NhanVienId, tungay, denngay);
        //        if (luongthuongphat != null && luongthuongphat.Rows.Count > 0)
        //        {
        //            kq.LuongThuong = Convert.ToDouble(luongthuongphat.Rows[0][1].ToString());
        //            kq.GTPhat = Convert.ToDouble(luongthuongphat.Rows[0][2].ToString());
        //            luongnv = luongnv + (Convert.ToDouble(luongthuongphat.Rows[0][1].ToString()) - Convert.ToDouble(luongthuongphat.Rows[0][2].ToString()));
        //        }

        //        //double luonglamthemgio = 0;
        //        //luonglamthemgio = lnv.LuongNgoaiGio * sogiolt;
        //        //luongnv += luonglamthemgio;
        //        List<ThamSoTinhLuong> giamtrus = thamsos.Where(p => p.Loai == 2).ToList();
        //        foreach (var gt in giamtrus)
        //        {
        //            if (gt.TenThamSo.ToUpper().Contains("BHXH"))
        //            {
        //                kq.GTBHXH = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                kq.TLBHXH = Convert.ToDouble(gt.GiaTri);
        //            }
        //            else if (gt.TenThamSo.ToUpper().Contains("BHYT"))
        //            {
        //                kq.GTBHYT = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                kq.TLBHYT = Convert.ToDouble(gt.GiaTri);
        //            }
        //            else if (gt.TenThamSo.ToUpper().Contains("THẤT NGHIỆP"))
        //            {
        //                kq.GTBHTN = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                kq.TLTroCapThatNghiep = Convert.ToDouble(gt.GiaTri);
        //            }
        //            luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //        }
        //        int luongnvtinhthue = Convert.ToInt32(luongnv) - 9000000 - ((lnv.SoNguoiGiamTruGC != null ? Convert.ToInt16(lnv.SoNguoiGiamTruGC) : 0) * 3600000);
        //        List<ThamSoTinhLuong> giamtrus2 = thamsos.Where(p => p.Loai == 3).ToList();
        //        foreach (var gt in giamtrus2)
        //        {
        //            kq.GTCongDoan = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //            luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //            kq.TLCongDoan = Convert.ToDouble(gt.GiaTri);
        //        }
        //        double thue = luongnvtinhthue > 0 ? (5 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 0, 5000000)) / 100)
        //                    + (10 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 5000000, 10000000)) / 100)
        //                    + (15 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 10000000, 18000000)) / 100)
        //                    + (20 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 18000000, 32000000)) / 100)
        //                    + (25 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 32000000, 52000000)) / 100)
        //                    + (30 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 52000000, 80000000)) / 100)
        //                     + (35 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 80000000, 999999999)) / 100) : 0;
        //        // ThamSoTinhLuong thuecanhan = thamsos.Where(p => p.Loai == 4).FirstOrDefault();
        //        // luongnv -= (luongnv * (Convert.ToDouble(thuecanhan.GiaTri) / 100));
        //        kq.GTThueThuNhapCN = thue;
        //        luongnv -= thue;
        //        kq.LuongNhanVien = (double)Math.Round(Convert.ToDecimal(luongnv), 0);
        //        kq.TongThu = kq.LuongSanLuong + kq.LuongNgoaiGio + kq.LuongThuong;
        //        kq.TongGiamTru = kq.GTBHXH + kq.GTBHYT + kq.GTBHTN + kq.GTCongDoan + kq.GTPhat + kq.GTThueThuNhapCN;
        //        kq.ThucLinh = kq.LuongNhanVien;
        //        kq.SoNguoiGiamTruGC = lnv.SoNguoiGiamTruGC != null ? Convert.ToInt16(lnv.SoNguoiGiamTruGC) : 0;
        //    }
        //    return kq;
        //}
        //public static List<TinhLuongNVModel> TinhLuongNVList(List<ListNhanVienLuong> lstNvl)
        //{
        //    IUnitOfWork unitofwork = new UnitOfWork();
        //    List<TinhLuongNVModel> lstkq = new List<TinhLuongNVModel>();
        //    for (int lnlno = 0; lnlno < lstNvl.Count; lnlno++)
        //    {
        //        Guid NhanVienId = lstNvl[lnlno].NhanVienId;
        //        DateTime tungay = lstNvl[lnlno].tungay;
        //        DateTime denngay = lstNvl[lnlno].denngay;
        //        TinhLuongNVModel kq = new TinhLuongNVModel();
        //        double luongnv = 0;
        //        //lấy bảng lương nhân viên
        //        LuongNhanVien lnv = unitofwork.LuongNhanViens.GetAll().Where(p => p.NhanVienID == NhanVienId && p.ThoiGianTinh < denngay).OrderByDescending(p => p.ThoiGianTinh).FirstOrDefault();
        //        if (lnv != null)
        //        {
        //            kq.LuongNVID = lnv.LuongNVID;
        //            kq.HeSo = lnv.HeSo;
        //            kq.LuongBHXH = lnv.LuongBHXH;
        //            kq.LuongCoBan = lnv.LuongCoBan;
        //            kq.LuongLamThem = lnv.LuongNgoaiGio;
        //            kq.SoNguoiGiamTruGC = lnv.SoNguoiGiamTruGC != null ? (int)lnv.SoNguoiGiamTruGC : 0;
        //            //tinh so ngay lam viec
        //            float songaylamviec = Functions.BusinessDaysUntil(tungay, denngay, null);
        //            DateTime tempt = tungay;
        //            if (lnv.ThoiGianTinh > tungay)
        //            {
        //                tempt = lnv.ThoiGianTinh;
        //            }
        //            float songaythucte = Functions.BusinessDaysUntil(tempt, denngay, null);
        //            float songaythucte1 = Functions.BusinessDaysUntil(tempt, denngay, null);
        //            //Lây tổng hệ số 
        //            DataTable tonghs = unitofwork.LuongNhanViens.getTongHS(denngay);
        //            double ths = Convert.ToDouble(tonghs.Rows[0][0]);
        //            kq.TongHS = ths;
        //            //Lây tổng Luongcoban 
        //            DataTable tongLuongcoban = unitofwork.LuongNhanViens.getTongLuongCoBan(denngay);
        //            double tongLuongcobandouble = Convert.ToDouble(tongLuongcoban.Rows[0][0]);
        //            // tính tổng san luongj trong tháng
        //            DataTable sanluongdd = unitofwork.BaoCaoSanLuongs.getBaoCaoSanLuongTheoCongTrinhall(tungay, denngay);
        //            double tongsanluong = Convert.ToDouble(sanluongdd.Rows[0][0].ToString() == "" ? "0" : sanluongdd.Rows[0][0].ToString()) / 1.1;
        //            //DAnh gia cong viec
        //            DateTime ngaydanhgiacongviec = DateTime.ParseExact("01/" + tungay.ToString("MM/yyyy") + " 00:00", "dd/MM/yyyy HH:mm", null);
        //            var danhgiacongviec = unitofwork.DanhGiaCongViecs.GetHeSoDanhGiaCV(NhanVienId, ngaydanhgiacongviec);
        //            kq.TongSL = tongsanluong;
        //            if (lnv.LuongCoBan == 0)
        //            {
        //                kq.LuongNhanVien = 0;
        //            }
        //            else
        //            {
        //                // lấy các khoản giảm trừ
        //                List<ThamSoTinhLuong> thamsos = unitofwork.ThamSoTinhLuongs.GetAll().ToList();
        //                ThamSoTinhLuong tile = thamsos.Where(p => p.Loai == 1).FirstOrDefault();
        //                //Lấy xác nhận công việc
        //                DataTable lstKhaiBaoCV = unitofwork.KhaiBaoCVs.LayDSKhaiBaoCV(NhanVienId, tungay, denngay);
        //                double sogiolv = 0;
        //                double sogiolt = 0;
        //                songaythucte = 0;
        //                foreach (DataRow r in lstKhaiBaoCV.Rows)
        //                {
        //                    if (r["TrangThai"].ToString() == "2")
        //                    {
        //                        sogiolv += Convert.ToDouble(r["SoGioLamViec"].ToString() != "" ? r["SoGioLamViec"].ToString() : "0");
        //                        sogiolt += Convert.ToDouble(r["OverTime"].ToString() != "" ? r["OverTime"].ToString() : "0");
        //                        songaythucte += 1;
        //                    }
        //                }
        //                //tính lương
        //                if (lnv.HinhThucTinhLuong == 2)
        //                {
        //                    luongnv = tongsanluong * ((tile != null ? Convert.ToDouble(tile.GiaTri) : 0) / 100) * (lnv.LuongCoBan / tongLuongcobandouble) * (songaythucte1 / songaylamviec);
        //                    kq.SoNgaycong = songaythucte1;
        //                    kq.NgayCong = songaythucte1;
        //                    kq.SanLuongNhanVien = tongsanluong * ((tile != null ? Convert.ToDouble(tile.GiaTri) : 0) / 100) * (lnv.LuongCoBan / tongLuongcobandouble);
        //                    kq.LuongSanLuong = luongnv;
        //                    //luong danh gia cong viec
        //                    if (danhgiacongviec != null && danhgiacongviec.Rows.Count > 0)
        //                    {
        //                        luongnv = luongnv + ((luongnv * Convert.ToDouble(danhgiacongviec.Rows[0]["TiLeLuong"]) / 100) - luongnv);
        //                        kq.GTGiamTruHS = (luongnv - (luongnv * Convert.ToDouble(danhgiacongviec.Rows[0]["TiLeLuong"]) / 100));
        //                        kq.LoaiDanhGiaCV = danhgiacongviec.Rows[0]["TenHeSo"] + "(" + danhgiacongviec.Rows[0]["TiLeLuong"] + ")";
        //                    }
        //                    else
        //                    {
        //                        kq.GTGiamTruHS = 0;
        //                    }

        //                    //Thuong phat
        //                    DataTable luongthuongphat = unitofwork.LuongNhanViens.getThuongPhat(NhanVienId, tungay, denngay);
        //                    if (luongthuongphat != null && luongthuongphat.Rows.Count > 0)
        //                    {
        //                        kq.LuongThuong = Convert.ToDouble(luongthuongphat.Rows[0][1].ToString());
        //                        kq.GTPhat = Convert.ToDouble(luongthuongphat.Rows[0][2].ToString());
        //                        luongnv = luongnv + (Convert.ToDouble(luongthuongphat.Rows[0][1].ToString()) - Convert.ToDouble(luongthuongphat.Rows[0][2].ToString()));
        //                    }

        //                    //double luonglamthemgio = 0;
        //                    //luonglamthemgio = lnv.LuongNgoaiGio * sogiolt;
        //                    //luongnv += luonglamthemgio;
        //                    List<ThamSoTinhLuong> giamtrus = thamsos.Where(p => p.Loai == 2).ToList();
        //                    foreach (var gt in giamtrus)
        //                    {
        //                        if (gt.TenThamSo.ToUpper().Contains("BHXH"))
        //                        {
        //                            kq.GTBHXH = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                            kq.TLBHXH = Convert.ToDouble(gt.GiaTri);
        //                        }
        //                        else if (gt.TenThamSo.ToUpper().Contains("BHYT"))
        //                        {
        //                            kq.GTBHYT = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                            kq.TLBHYT = Convert.ToDouble(gt.GiaTri);
        //                        }
        //                        else if (gt.TenThamSo.ToUpper().Contains("THẤT NGHIỆP"))
        //                        {
        //                            kq.GTBHTN = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                            kq.TLTroCapThatNghiep = Convert.ToDouble(gt.GiaTri);
        //                        }
        //                        luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                    }
        //                    double luongnvtinhthue = luongnv;
        //                    List<ThamSoTinhLuong> giamtrus2 = thamsos.Where(p => p.Loai == 3).ToList();
        //                    foreach (var gt in giamtrus2)
        //                    {
        //                        kq.GTCongDoan = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //                        luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //                        kq.TLCongDoan = Convert.ToDouble(gt.GiaTri);
        //                    }
        //                    //double thue = luongnvtinhthue > 0 ? (5 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 0, 5000000)) / 100)
        //                    //            + (10 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 5000000, 10000000)) / 100)
        //                    //            + (15 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 10000000, 18000000)) / 100)
        //                    //            + (20 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 18000000, 32000000)) / 100)
        //                    //            + (25 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 32000000, 52000000)) / 100)
        //                    //            + (30 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 52000000, 80000000)) / 100)
        //                    //             + (35 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 80000000, 999999999)) / 100) : 0;
        //                    //// ThamSoTinhLuong thuecanhan = thamsos.Where(p => p.Loai == 4).FirstOrDefault();
        //                    //// luongnv -= (luongnv * (Convert.ToDouble(thuecanhan.GiaTri) / 100));
        //                    //kq.GTThueThuNhapCN = thue;
        //                    //luongnv -= thue;
        //                    kq.LuongTinhThue = (double)Math.Round(Convert.ToDecimal(luongnvtinhthue), 0);
        //                    kq.LuongNhanVien = (double)Math.Round(Convert.ToDecimal(luongnv), 0);
        //                    kq.TongThu = kq.LuongSanLuong + kq.LuongNgoaiGio + kq.LuongThuong;
        //                    kq.TongGiamTru = kq.GTBHXH + kq.GTBHYT + kq.GTBHTN + kq.GTCongDoan + kq.GTPhat + kq.GTThueThuNhapCN;
        //                    kq.ThucLinh = kq.LuongNhanVien;
        //                }
        //                else //if (sogiolv != 0)
        //                {
        //                    luongnv = tongsanluong * ((tile != null ? Convert.ToDouble(tile.GiaTri) : 0) / 100) * (lnv.LuongCoBan / tongLuongcobandouble) * ((sogiolv / 8) / songaylamviec);
        //                    kq.SoNgaycong = (sogiolv / 8);
        //                    kq.LuongSanLuong = luongnv;
        //                    kq.NgayCong = (sogiolv / 8);
        //                    kq.SanLuongNhanVien = tongsanluong * ((tile != null ? Convert.ToDouble(tile.GiaTri) : 0) / 100) * (lnv.LuongCoBan / tongLuongcobandouble);
        //                    //luong danh gia cong viec
        //                    if (danhgiacongviec != null && danhgiacongviec.Rows.Count > 0)
        //                    {
        //                        luongnv = luongnv + ((luongnv * Convert.ToDouble(danhgiacongviec.Rows[0]["TiLeLuong"]) / 100) - luongnv);
        //                        kq.GTGiamTruHS = (luongnv - (luongnv * Convert.ToDouble(danhgiacongviec.Rows[0]["TiLeLuong"]) / 100));
        //                        kq.LoaiDanhGiaCV = danhgiacongviec.Rows[0]["TenHeSo"] + "(" + danhgiacongviec.Rows[0]["TiLeLuong"] + ")";
        //                    }
        //                    else
        //                    {
        //                        kq.GTGiamTruHS = 0;
        //                    }

        //                    //Thuong phat
        //                    DataTable luongthuongphat = unitofwork.LuongNhanViens.getThuongPhat(NhanVienId, tungay, denngay);
        //                    if (luongthuongphat != null && luongthuongphat.Rows.Count > 0)
        //                    {
        //                        kq.LuongThuong = Convert.ToDouble(luongthuongphat.Rows[0][1].ToString());
        //                        kq.GTPhat = Convert.ToDouble(luongthuongphat.Rows[0][2].ToString());
        //                        luongnv = luongnv + (Convert.ToDouble(luongthuongphat.Rows[0][1].ToString()) - Convert.ToDouble(luongthuongphat.Rows[0][2].ToString()));
        //                    }
        //                    double luonglamthemgio = 0;
        //                    luonglamthemgio = lnv.LuongNgoaiGio * sogiolt;
        //                    kq.LuongNgoaiGio = luonglamthemgio;
        //                    luongnv += luonglamthemgio;

        //                    List<ThamSoTinhLuong> giamtrus = thamsos.Where(p => p.Loai == 2).ToList();
        //                    foreach (var gt in giamtrus)
        //                    {
        //                        if (gt.TenThamSo.ToUpper().Contains("BHXH"))
        //                        {
        //                            kq.GTBHXH = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                            kq.TLBHXH = Convert.ToDouble(gt.GiaTri);
        //                        }
        //                        else if (gt.TenThamSo.ToUpper().Contains("BHYT"))
        //                        {
        //                            kq.GTBHYT = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                            kq.TLBHYT = Convert.ToDouble(gt.GiaTri);
        //                        }
        //                        else if (gt.TenThamSo.ToUpper().Contains("THẤT NGHIỆP"))
        //                        {
        //                            kq.GTBHTN = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                            kq.TLTroCapThatNghiep = Convert.ToDouble(gt.GiaTri);
        //                        }
        //                        luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongBHXH;
        //                    }
        //                    double luongnvtinhthue = luongnv;
        //                    List<ThamSoTinhLuong> giamtrus2 = thamsos.Where(p => p.Loai == 3).ToList();
        //                    foreach (var gt in giamtrus2)
        //                    {
        //                        kq.GTCongDoan = (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //                        luongnv -= (Convert.ToDouble(gt.GiaTri) / 100) * lnv.LuongCoBan;
        //                        kq.TLCongDoan = Convert.ToDouble(gt.GiaTri);
        //                    }
        //                    //ThamSoTinhLuong thuecanhan = thamsos.Where(p => p.Loai == 4).FirstOrDefault();
        //                    //luongnv -= (luongnv * (Convert.ToDouble(thuecanhan.GiaTri) / 100));
        //                    //kq.LuongNhanVien = luongnv;
        //                    //double thue = luongnvtinhthue > 0 ? (5 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 0, 5000000)) / 100)
        //                    //            + (10 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 5000000, 10000000)) / 100)
        //                    //            + (15 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 10000000, 18000000)) / 100)
        //                    //            + (20 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 18000000, 32000000)) / 100)
        //                    //            + (25 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 32000000, 52000000)) / 100)
        //                    //            + (30 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 52000000, 80000000)) / 100)
        //                    //             + (35 * Convert.ToDouble(TrongKhoang(luongnvtinhthue, 80000000, 999999999)) / 100) : 0;
        //                    //// ThamSoTinhLuong thuecanhan = thamsos.Where(p => p.Loai == 4).FirstOrDefault();
        //                    //// luongnv -= (luongnv * (Convert.ToDouble(thuecanhan.GiaTri) / 100));
        //                    //kq.GTThueThuNhapCN = thue;
        //                    //luongnv -= thue;
        //                    kq.LuongTinhThue = (double)Math.Round(Convert.ToDecimal(luongnvtinhthue), 0);
        //                    kq.LuongNhanVien = (double)Math.Round(Convert.ToDecimal(luongnv), 0);
        //                    kq.TongThu = kq.LuongSanLuong + kq.LuongNgoaiGio + kq.LuongThuong;
        //                    kq.TongGiamTru = kq.GTBHXH + kq.GTBHYT + kq.GTBHTN + kq.GTCongDoan + kq.GTPhat + kq.GTThueThuNhapCN + kq.GTGiamTruHS;
        //                    kq.ThucLinh = kq.LuongNhanVien;
        //                    kq.QuiDuPhong = (kq.SanLuongNhanVien - kq.LuongSanLuong) + kq.GTGiamTruHS + kq.GTPhat;
        //                    //Thưởng phat
        //                }
        //            }
        //        }
        //        lstkq.Add(kq);
        //    }
        //    return lstkq;
        //}

        public static string GetstrTinhTrang(int TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case (int)CongTrinh_TinhTrangEnums.CHUA_THI_CONG: strTinhTrang = Language.GetText("chuathicong"); break;
                case (int)CongTrinh_TinhTrangEnums.DANG_THI_CONG: strTinhTrang = Language.GetText("dangthicong"); break;
                case (int)CongTrinh_TinhTrangEnums.DA_HOAN_THIEN: strTinhTrang = Language.GetText("dahoanthien") ; break;
                case (int)CongTrinh_TinhTrangEnums.DA_QUYET_TOAN: strTinhTrang = Language.GetText("dahoanthien"); break;
                case (int)CongTrinh_TinhTrangEnums.DANG_BAO_GIA: strTinhTrang = Language.GetText("dangbaogia"); break;
                case (int)CongTrinh_TinhTrangEnums.DA_BAO_GIA: strTinhTrang = Language.GetText("dabaogia"); break;
                case (int)CongTrinh_TinhTrangEnums.TAM_DUNG: strTinhTrang = Language.GetText("tamdung"); break;
                default: strTinhTrang = Language.GetText("chuathicong"); ; break;
            }
            return strTinhTrang;
        }

        public static string GetstrTenLoaiHopDong(int LoaiHopDong)
        {
            string strTinhTrang = "";
            switch (LoaiHopDong)
            {
                case (int)CongTrinh_HopDong_LoaiHopDongEnums.KINH_TE: strTinhTrang = Language.GetText("hopdongkinhte"); break;
                case (int)CongTrinh_HopDong_LoaiHopDongEnums.LAO_DONG: strTinhTrang = Language.GetText("hopdonglaodong"); ; break;
                case (int)CongTrinh_HopDong_LoaiHopDongEnums.MUA_BAN: strTinhTrang = Language.GetText("hopdongmuaban"); break;
                case (int)CongTrinh_HopDong_LoaiHopDongEnums.SUA_CHUA: strTinhTrang = Language.GetText("hopdongsuachua"); break;
                case (int)CongTrinh_HopDong_LoaiHopDongEnums.THI_CONG: strTinhTrang = Language.GetText("hopdongthicong"); break;
                case (int)CongTrinh_HopDong_LoaiHopDongEnums.CONG_VAN_DEN: strTinhTrang = Language.GetText("congvanden"); break;
                case (int)CongTrinh_HopDong_LoaiHopDongEnums.CONG_VAN_DI: strTinhTrang = Language.GetText("congvandi"); break;
                case (int)CongTrinh_HopDong_LoaiHopDongEnums.BIEN_BAN: strTinhTrang = Language.GetText("bienban"); break;
                case (int)CongTrinh_HopDong_LoaiHopDongEnums.BIEU_MAU: strTinhTrang = Language.GetText("bieumau"); break;
                case (int)CongTrinh_HopDong_LoaiHopDongEnums.TAI_LIEU: strTinhTrang = Language.GetText("tailieu"); break;
                default: strTinhTrang = Language.GetText("hopdongkinhte"); break;
            }
            return strTinhTrang;
        }

        public static string GetstrXoaCongTrinh(int TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case 0: strTinhTrang = Language.GetText("mgs_xoathanhcong"); break;
                case 1: strTinhTrang = Language.GetText("mgs_dacothietbihotrotaicongtrinh"); break;
                case 2: strTinhTrang = Language.GetText("mgs_dacothietbitaicongtrinh"); break;
                case 3: strTinhTrang = Language.GetText("mgs_dacodenghitaicongtrinh"); break;
                case 4: strTinhTrang = Language.GetText("mgs_danhapvattuchocongtrinh"); break;
                case 5: strTinhTrang = Language.GetText("mgs_daxuatvattutucongtrinh"); break;
                case 6: strTinhTrang = Language.GetText("mgs_daconhanviendangthicongtaicongtrinh"); break;
                case 7: strTinhTrang = Language.GetText("mgs_congtrinhdacohopdong"); break;
                default: strTinhTrang = Language.GetText("mgs_xoathanhcong"); break;
            }
            return strTinhTrang;
        }

        public static string GetstrHinhThucTinhLuong(int TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case 0: strTinhTrang = Language.GetText("chamcong"); break;
                case 1: strTinhTrang = Language.GetText("chamcong"); break;
                case 2: strTinhTrang = Language.GetText("khongchamcong"); break;
                default: strTinhTrang = Language.GetText("chamcong"); break;
            }
            return strTinhTrang;
        }
        public static string GetstrLoaiNhanCong(int TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case 0: strTinhTrang = Language.GetText("nhanviencongtrinh"); break;
                case 1: strTinhTrang = Language.GetText("nhanvienvanphong"); break;
                default: strTinhTrang = Language.GetText("nhanvienvanphong"); break;
            }
            return strTinhTrang;
        }

        public static string GetstrLoaiHopDong(int TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case 0: strTinhTrang = Language.GetText("nhanviencongtrinh"); break;
                case 1: strTinhTrang = Language.GetText("nhanvienvanphong"); break;
                default: strTinhTrang = Language.GetText("nhanvienvanphong"); break;
            }
            return strTinhTrang;
        }

        public static string GetstrTinhTrangNhanVien(int TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case 1: strTinhTrang = Language.GetText("danglamviec"); break;
                case 2: strTinhTrang = Language.GetText("nghichoviec"); break;
                case 3: strTinhTrang = Language.GetText("thoiviec"); break;
                default: strTinhTrang = Language.GetText("danglamviec"); break;
            }
            return strTinhTrang;
        }

        public static string GetstrLoaiHopDongNhanVien(int TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case 1: strTinhTrang = Language.GetText("daihan"); break;
                case 2: strTinhTrang = Language.GetText("thang1236"); break;
                case 3: strTinhTrang = Language.GetText("thuviec"); break;
                case 4: strTinhTrang = Language.GetText("muavu"); break;
                default: strTinhTrang = Language.GetText("chuaky"); break;
            }
            return strTinhTrang;
        }

        public static string GetstrGioiTinhNhanVien(int TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case 0: strTinhTrang = Language.GetText("boy"); break;
                case 1: strTinhTrang = Language.GetText("nu"); break;
                default: strTinhTrang = Language.GetText("khongxacdinh"); break;
            }
            return strTinhTrang;
        }

        public static string GetstrTrangThaiTaiSan(int TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case 0: strTinhTrang = Language.GetText("dangokho"); break;
                case 1: strTinhTrang = Language.GetText("dabangiao"); break;
                case 2: strTinhTrang = Language.GetText("dathanhly"); break;
                default: strTinhTrang = Language.GetText("khongxacdinh"); break;
            }
            return strTinhTrang;
        }
        public static string GetstrDoiTacNoiBo(int TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case 1: strTinhTrang = Language.GetText("doitac"); break;
                case 2: strTinhTrang = Language.GetText("noibo"); break;
                default: strTinhTrang = Language.GetText("khongxacdinh"); break;
            }
            return strTinhTrang;
        }
        public static string GetstrTinhTrangThietBi(int? TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case 0: strTinhTrang = Language.GetText("honghoc"); break;
                case 1: strTinhTrang = Language.GetText("binhthuong"); break;
                default: strTinhTrang = Language.GetText("chuabaocao"); break;
            }
            return strTinhTrang;
        }

        public static string GetstrTrangThaiThietBi(int? TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case 0: strTinhTrang = Language.GetText("khonghoatdong"); break;
                case 1: strTinhTrang = Language.GetText("danghoatdong"); break;
                default: strTinhTrang = Language.GetText("chuabaocao"); break;
            }
            return strTinhTrang;
        }
        public static string GetstrLoaiHinhDoiThiCong(int? TinhTrang)
        {
            string strTinhTrang = "";
            switch (TinhTrang)
            {
                case 0: strTinhTrang = Language.GetText("doithicong"); break;
                case 4: strTinhTrang = Language.GetText("phongban"); break;
                default: strTinhTrang = Language.GetText("doithicong"); break;
            }
            return strTinhTrang;
        }

        public static string GetSoLaMa(string So, bool IsHeader)
        {
            string str = "";
            if (IsHeader)
            {
                switch (So)
                {
                    case "1": str = "I";break;
                    case "2": str = "II"; break;
                    case "3": str = "III"; break;
                    case "4": str = "IV"; break;
                    case "5": str = "V"; break;
                    case "6": str = "VI"; break;
                    case "7": str = "VII"; break;
                    case "8": str = "VIII"; break;
                    case "9": str = "IX"; break;
                    case "10": str = "X"; break;
                    default: str = "I";break;
                }
            }
            else
            {
                str = So;
            }
            return str;
        }
        public static string unsign(string text)
        {
            var regex = new Regex(@"\p{IsCombiningDiacriticalMarks}+");
            string strFormD = text.Normalize(NormalizationForm.FormD);
            return regex.Replace(strFormD, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static string convertCodeData(String codeData)
        {
            if (codeData != null && codeData.Trim() != "")
            {
                for (int i = 0; i < codeData.Length; i++)
                {
                    char c = codeData[i];
                    if (!(c == 45                      // '-'
                        || (c >= 48 && c <= 57)     // 0 -> 9
                        || (c >= 65 && c <= 90)     // A -> Z
                        || (c >= 97 && c <= 122)    // a -> z
                        ))
                    {
                        codeData.Remove(i, 1);
                    }
                }
                codeData = codeData.Trim();
            }
            return codeData;
        }

     

      

    
        private static char[] unichars = {
            'Ă','Â','Ê','Ô','Ơ','Ư','Đ','ă','â','ê',
            'ô','ơ','ư','đ','n','n','n','n','n','n',
            'à','ả','ã','á','ạ','n','ằ','ẳ','ẵ','ắ',
            'n','n','n','n','n','n','n','ặ','ầ','ẩ',
            'ẫ','ấ','ậ','è','n','ẻ','ẽ','é','ẹ','ề',
            'ể','ễ','ế','ệ','ì','ỉ','n','n','n','ĩ',
            'í','ị','ò','n','ỏ','õ','ó','ọ','ồ','ổ',
            'ỗ','ố','ộ','ờ','ở','ỡ','ớ','ợ','ù','n',
            'ủ','ũ','ú','ụ','ừ','ử','ữ','ứ','ự','ỳ',
            'ỷ','ỹ','ý','ỵ'
        };

      
        
        /// <summary>
        /// created by: HienDM1
        /// cretae date: 14/05/2015
        /// hàm chuyển đổi chuỗi là unicode hay tcvn3
        /// </summary>
        /// <param name="input">chuỗi cần kiểm tra</param>
        /// <returns>null: không thể chuyển đổi</returns>
        public static string ConvertUnicode(string input)
        {
            // HienDM1 sử dụng cách convert font mới 
            char[] chars = input.ToCharArray();
            for (int i = 0; i < chars.Length; i++)
            {
                char e = chars[i];
                if ((e >= 161 && e <= 174) || (e >= 181 && e <= 190 && e != 186) ||
                    (e >= 198 && e <= 216 && e != 205) || (e >= 220 && e <= 254 && e != 240 && e != 224))
                {
                    chars[i] = unichars[chars[i] - 161];
                }
            }
            return new string(chars);
        }

        /// <summary>
        /// created by: HienDM1
        /// cretae date: 14/05/2015
        /// hàm kiểm tra chuỗi là unicode hay tcvn3
        /// </summary>
        /// <param name="input">chuỗi cần kiểm tra</param>
        /// <returns>1: unicode, 0: tcvn3, -1: không xác định</returns>
        public static int checkUnicodeString(string input)
        {
            char[] chars = input.ToCharArray();
            int[] arrayUnknown = {161, 173, 200, 201, 202, 204, 208, 210, 211, 213, 212, 221,
                                  225, 226, 227, 232, 233, 234, 236, 237, 242, 243,
                                  244, 245, 249, 250, 253};
            for (int i = 0; i < chars.Length; i++)
            {
                char e = chars[i];
                if ((e >= 161 && e <= 174) || (e >= 181 && e <= 190 && e != 186) ||
                    (e >= 198 && e <= 216 && e != 205) || (e >= 220 && e <= 254 && e != 224 && e != 240))
                {
                    if (arrayUnknown.Contains(e))
                    {
                        // Không xác định
                        return -1;
                    }
                    else
                    {
                        // là tcvn3
                        return 0;
                    }
                }
            }
            return 1;
        }

 
  
        /// <summary>
        /// created by: tamhd
        /// cretae date: 6/11/2014
        /// hàm chuyển datatable sang json
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public static string DataTableToJSON(DataTable dt)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            object[] arr = new object[dt.Rows.Count + 1];
            for (int i = 0; i <= dt.Rows.Count - 1; i++)
            {
                arr[i] = dt.Rows[i].ItemArray;
            }
            dict.Add("table", arr);
            JavaScriptSerializer json = new JavaScriptSerializer();
            return json.Serialize(dict);
        }

        #region Excel Utils
        /// <summary>
        /// dinhlv6
        /// 13/3/2015
        /// export file excel tu dataset voi nhieu sheet  
        /// </summary>
        /// <param name="source"></param>
        /// <param name="fileName">duong dan luu file</param>
        public static void exportToExcel(DataSet source, string fileName)
        {
            NumberStyles style1 = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign;
            CultureInfo culture1 = CultureInfo.CreateSpecificCulture("en-US");
            System.IO.StreamWriter excelDoc;
            excelDoc = new System.IO.StreamWriter(fileName);
            // cau truc file xml dinh dang cho xuat excel 
            const string startExcelXML = "<xml version>\r\n<Workbook " +
                  "xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\r\n" +
                  " xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n " +
                  "xmlns:x=\"urn:schemas-    microsoft-com:office:" +
                  "excel\"\r\n xmlns:ss=\"urn:schemas-microsoft-com:" +
                  "office:spreadsheet\">\r\n <Styles>\r\n " +
                  "<Style ss:ID=\"Default\" ss:Name=\"Normal\">\r\n " +
                  "<Alignment ss:Vertical=\"Bottom\"/>\r\n <Borders/>" +
                  "\r\n <Font/>\r\n <Interior/>\r\n <NumberFormat/>" +
                  "\r\n <Protection/>\r\n </Style>\r\n " +
                  "<Style ss:ID=\"BoldColumn\">\r\n <Font " +
                  "x:Family=\"Swiss\" ss:Bold=\"1\"/>\r\n </Style>\r\n " +
                  "<Style     ss:ID=\"StringLiteral\">\r\n <NumberFormat" +
                  " ss:Format=\"@\"/>\r\n </Style>\r\n <Style " +
                  "ss:ID=\"Decimal\">\r\n <NumberFormat " +
                  "ss:Format=\"0.0000\"/>\r\n </Style>\r\n " +
                  "<Style ss:ID=\"Integer\">\r\n <NumberFormat " +
                  "ss:Format=\"0\"/>\r\n </Style>\r\n <Style " +
                  "ss:ID=\"DateLiteral\">\r\n <NumberFormat " +
                  "ss:Format=\"mm/dd/yyyy;@\"/>\r\n </Style>\r\n " +
                  "</Styles>\r\n ";
            const string endExcelXML = "</Workbook>";

            int rowCount = 0;
            int sheetCount = 1;

            excelDoc.Write(startExcelXML);
            for (int tb = 0; tb < source.Tables.Count; tb++)
            {
                excelDoc.Write("<Worksheet ss:Name=\"" + source.Tables[tb].TableName.Replace("$", "") + "\">");
                excelDoc.Write("<Table>");
                excelDoc.Write("<Row>");
                for (int x = 0; x < source.Tables[tb].Columns.Count; x++)
                {
                    excelDoc.Write("<Cell ss:StyleID=\"BoldColumn\"><Data ss:Type=\"String\">");
                    excelDoc.Write(source.Tables[tb].Columns[x].ColumnName);
                    excelDoc.Write("</Data></Cell>");
                }
                excelDoc.Write("</Row>");
                foreach (DataRow x in source.Tables[tb].Rows)
                {
                    rowCount++;
                    //if the number of rows is > 64000 create a new page to continue output
                    if (rowCount == 64000)
                    {
                        rowCount = 0;
                        excelDoc.Write("</Table>");
                        excelDoc.Write(" </Worksheet>");
                        excelDoc.Write("<Worksheet ss:Name=\"" + source.Tables[tb].TableName.Replace("$", "") + sheetCount + "\">");
                        excelDoc.Write("<Table>");
                        sheetCount++;
                    }
                    excelDoc.Write("<Row>"); //ID=" + rowCount + "
                    decimal dValue = 0;
                    for (int y = 0; y < source.Tables[tb].Columns.Count; y++)
                    {
                        System.Type rowType;
                        rowType = x[y].GetType();
                        if (decimal.TryParse(x[y].ToString(), style1, culture1, out dValue))
                        {
                            rowType = System.Type.GetType("System.Int64");
                        }
                        switch (rowType.ToString())
                        {
                            case "System.String":
                                string XMLstring = x[y].ToString();
                                XMLstring = XMLstring.Trim();
                                XMLstring = XMLstring.Replace("&", "&");
                                XMLstring = XMLstring.Replace(">", ">");
                                XMLstring = XMLstring.Replace("<", "<");
                                excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\">" +
                                               "<Data ss:Type=\"String\">");
                                excelDoc.Write(XMLstring);
                                excelDoc.Write("</Data></Cell>");
                                break;
                            case "System.DateTime":
                                //Excel has a specific Date Format of YYYY-MM-DD followed by  
                                //the letter 'T' then hh:mm:sss.lll Example 2005-01-31T24:01:21.000
                                //The Following Code puts the date stored in XMLDate 
                                //to the format above
                                DateTime XMLDate = (DateTime)x[y];
                                string XMLDatetoString = ""; //Excel Converted Date
                                XMLDatetoString = XMLDate.Year.ToString() +
                                     "-" +
                                     (XMLDate.Month < 10 ? "0" +
                                     XMLDate.Month.ToString() : XMLDate.Month.ToString()) +
                                     "-" +
                                     (XMLDate.Day < 10 ? "0" +
                                     XMLDate.Day.ToString() : XMLDate.Day.ToString()) +
                                     "T" +
                                     (XMLDate.Hour < 10 ? "0" +
                                     XMLDate.Hour.ToString() : XMLDate.Hour.ToString()) +
                                     ":" +
                                     (XMLDate.Minute < 10 ? "0" +
                                     XMLDate.Minute.ToString() : XMLDate.Minute.ToString()) +
                                     ":" +
                                     (XMLDate.Second < 10 ? "0" +
                                     XMLDate.Second.ToString() : XMLDate.Second.ToString()) +
                                     ".000";
                                excelDoc.Write("<Cell ss:StyleID=\"DateLiteral\">" +
                                             "<Data ss:Type=\"DateTime\">");
                                excelDoc.Write(XMLDatetoString);
                                excelDoc.Write("</Data></Cell>");
                                break;
                            case "System.Boolean":
                                excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\">" +
                                            "<Data ss:Type=\"String\">");
                                excelDoc.Write(x[y].ToString());
                                excelDoc.Write("</Data></Cell>");
                                break;
                            case "System.Int16":
                            case "System.Int32":
                            case "System.Int64":
                            case "System.Byte":
                                excelDoc.Write("<Cell ss:StyleID=\"Integer\">" +
                                        "<Data ss:Type=\"Number\">");
                                excelDoc.Write(x[y].ToString());
                                excelDoc.Write("</Data></Cell>");
                                break;
                            case "System.Decimal":
                            case "System.Double":
                                excelDoc.Write("<Cell ss:StyleID=\"Decimal\">" +
                                      "<Data ss:Type=\"Number\">");
                                excelDoc.Write(x[y].ToString());
                                excelDoc.Write("</Data></Cell>");
                                break;
                            case "System.DBNull":
                                excelDoc.Write("<Cell ss:StyleID=\"StringLiteral\">" +
                                      "<Data ss:Type=\"String\">");
                                excelDoc.Write("");
                                excelDoc.Write("</Data></Cell>");
                                break;
                            default:
                                throw (new Exception(rowType.ToString() + " not handled."));
                        }
                    }
                    excelDoc.Write("</Row>");
                }
                excelDoc.Write("</Table>");
                excelDoc.Write(" </Worksheet>");
            }
            excelDoc.Write(endExcelXML);
            excelDoc.Close();
        }
        /// <summary>
        /// created by: tamhd
        /// create date: 14/1/2014
        /// hàm chuyển từ datatable thành file excel
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="urlDown"></param>
        public static void ExportToExcel(DataTable dt, string urlDown)
        {
            try
            {
                //open file
                StreamWriter wr = new StreamWriter(HttpContext.Current.Request.PhysicalApplicationPath + urlDown, true, Encoding.Unicode);
                try
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        wr.Write(dt.Columns[i].ToString().ToUpper() + "\t");
                    }
                    wr.WriteLine();
                    //write rows to excel file
                    for (int i = 0; i < (dt.Rows.Count); i++)
                    {
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            if (dt.Rows[i][j] != null)
                            {
                                wr.Write(Convert.ToString(dt.Rows[i][j]) + "\t");
                            }
                            else
                            {
                                wr.Write("\t");
                            }
                        }
                        //go to next line
                        wr.WriteLine();
                    }
                    //close file
                    wr.Close();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
        /// <summary>
        /// Doc du lieu trong file Excel va tra ve DataSet
        /// </summary>
        /// <param name="FileName">Duong dan file</param>
        /// <param name="hasHeaders">Dung de xac dinh bien HDR trong Excel Connection String</param>
        /// <param name="iColumnCount">So cot can lay trong 1 Sheet cua file Excel.</param>
        /// <returns></returns>
        public static DataSet ReadExcelToDataSet(string FileName, bool hasHeaders)//, int iColumnCount = 250)
        {
            try
            {
                string HDR = hasHeaders ? "Yes" : "No";
                string strConn;
                if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                else
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";

                DataSet output = new DataSet();

                using (OleDbConnection conn = new OleDbConnection(strConn))
                {
                    conn.Open();

                    DataTable schemaTable = conn.GetOleDbSchemaTable(
                        OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    foreach (DataRow schemaRow in schemaTable.Rows)
                    {
                        string sheet = schemaRow["TABLE_NAME"].ToString();

                        if (sheet.EndsWith("$") || sheet.EndsWith("$'"))
                        {
                            try
                            {
                                //string sExcelColumnName = GetExcelEndDataColumn(iColumnCount);
                                //OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheet + "A:" + sExcelColumnName + "]", conn);
                                OleDbCommand cmd = new OleDbCommand("select * from [" + sheet + "]", conn);
                                cmd.CommandType = CommandType.Text;
                                DataTable outputTable = new DataTable(sheet);
                                output.Tables.Add(outputTable);
                                OleDbDataAdapter adt = new OleDbDataAdapter(cmd);
                                adt.Fill(outputTable);
                                adt.Dispose();
                                cmd.Dispose();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message + string.Format("Sheet:{0}.File:F{1}", sheet, FileName), ex);
                            }
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                }
                return output;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static DataTable ReadExcelToDataTable(string FileName, bool hasHeaders, int iColumnCount = 250)
        {
            try
            {
                string HDR = hasHeaders ? "Yes" : "No";
                string strConn;
                if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                else
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";

                DataTable outputTable = new DataTable();

                using (OleDbConnection conn = new OleDbConnection(strConn))
                {
                    conn.Open();

                    DataTable schemaTable = conn.GetOleDbSchemaTable(
                        OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    foreach (DataRow schemaRow in schemaTable.Rows)
                    {
                        string sheet = schemaRow["TABLE_NAME"].ToString();

                        if (sheet.EndsWith("$") || sheet.EndsWith("$'"))
                        {
                            try
                            {
                                string sExcelColumnName = GetExcelEndDataColumn(iColumnCount);
                                OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheet + "A:" + sExcelColumnName + "]", conn);
                                cmd.CommandType = CommandType.Text;
                                outputTable = new DataTable(sheet);

                                OleDbDataAdapter adt = new OleDbDataAdapter(cmd);
                                adt.Fill(outputTable);
                                adt.Dispose();
                                cmd.Dispose();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message + string.Format("Sheet:{0}.File:F{1}", sheet, FileName), ex);
                            }
                        }
                    }
                    conn.Close();
                    conn.Dispose();
                }
                return outputTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static string GetExcelEndDataColumn(int iNumberOfColumn)
        {
            if (iNumberOfColumn <= 250 && iNumberOfColumn > 0)// 250 la so cot lon nhat trong DataTable
            {
                string characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                string sChar1, sChar2;
                int iQuotient = iNumberOfColumn / 26;
                int iSurplus = iNumberOfColumn % 26;

                if (iSurplus == 0)
                {
                    iQuotient--;
                    iSurplus = 26;
                }

                sChar1 = iQuotient <= 0 ? "" : characters.Substring(iQuotient - 1, 1);
                sChar2 = characters.Substring(iSurplus - 1, 1);
                return sChar1 + sChar2;
            }
            else
            {
                return null;
            }
        }
        #endregion
     
  
        //Biến lưu giá trị URL đầu tiên
        public static string getUrlDefault = "";
        /// <summary>
        /// Chuyển đổi ngày tháng từ string mm/dd/yyyy sang date
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static DateTime? ConvetDateVNToDate(string strDate)
        {
            if (strDate == null || strDate == "")
            {
                return null;
            }
            else
            {
                try
                {
                    strDate = strDate.Trim();
                    if (String.IsNullOrEmpty(strDate)) return DateTime.Today;
                    string[] strs = strDate.Split("/".ToCharArray());

                    int day = Convert.ToInt32(strs[0]);
                    int month = Convert.ToInt32(strs[1]);
                    int year = Convert.ToInt32(strs[2]);
                    try { return new DateTime(year, month, day); }
                    catch { return new DateTime(year, day, month); }
                }
                catch
                {
                    return null;
                }

            }
        }
        public static DateTime ConvetDateVNToDateTrue(string strDate)
        {
            if (strDate == null || strDate == "")
            {
                return DateTime.Now;
            }
            else
            {
                try
                {
                    strDate = strDate.Trim();
                    if (String.IsNullOrEmpty(strDate)) return DateTime.Today;
                    string[] strs = strDate.Split("/".ToCharArray());

                    int day = Convert.ToInt32(strs[0]);
                    int month = Convert.ToInt32(strs[1]);
                    int year = Convert.ToInt32(strs[2]);
                    try { return new DateTime(year, month, day); }
                    catch { return new DateTime(year, day, month); }
                }
                catch
                {
                    return DateTime.Now;
                }

            }
        }
        public static string ToString(object obj)
        {
            try
            {
                return Convert.ToString(obj);
            }
            catch
            {
                return "";
            }
        }
        public static DateTime ToDateTime(object obj, string strFormat)
        {
            try
            {
                string dtString = ToString(obj);
                string[] arr = new string[2];
                DateTime dt = DateTime.MinValue;

                if (dtString.IndexOf("/") > -1)
                {
                    arr = dtString.Split('/');
                }
                else if (dtString.IndexOf("-") > -1)
                {
                    arr = dtString.Split('-');
                }
                else
                {
                    return dt;
                }

                switch (strFormat)
                {
                    case "dd/mm/yyyy":
                    case "dd-mm-yyyy":
                        dt = ToDateTime(arr[1] + "/" + arr[0] + "/" + arr[2]);
                        break;
                    case "yyyy/mm/dd":
                    case "yyyy-mm-dd":
                        dt = ToDateTime(arr[1] + "/" + arr[2] + "/" + arr[0]);
                        break;
                    case "yyyy/dd/mm":
                    case "yyyy-dd-mm":
                        dt = ToDateTime(arr[2] + "/" + arr[1] + "/" + arr[0]);
                        break;
                    default:
                        dt = ToDateTime(obj);
                        break;
                }
                return dt;
            }
            catch
            {
                return DateTime.MinValue;
            }
        }
        public static DateTime ToDateTime(object obj)
        {
            try
            {
                return Convert.ToDateTime(obj);
            }
            catch
            {
                return DateTime.Today;
            }
        }
        /// <summary>
        /// Chuyển đổi ngày tháng từ string dd/mm/yyyy sang date
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static DateTime? ConvetDateVNToDates(string strDate)
        {
            if (strDate == null || strDate == "")
            {
                return null;
            }
            else
            {
                try
                {
                    strDate = strDate.Trim();
                    if (String.IsNullOrEmpty(strDate)) return DateTime.Today;
                    string[] strs = strDate.Split("/".ToCharArray());

                    int day = Convert.ToInt32(strs[0]);
                    int month = Convert.ToInt32(strs[1]);
                    int year = Convert.ToInt32(strs[2]);
                    try { return new DateTime(year, month, day); }
                    catch { return new DateTime(year, day, month); }
                }
                catch
                {
                    return null;
                }

            }
        }
        public static DateTime? sysDate()
        {
            return DateTime.Now;
        }
        public static DateTime GetDateTime()
        {
            return DateTime.Now;
        }
        /// <summary>
        /// Thực hiện cắt bỏ các dấu cách thừa ở 2 đầu đối với các trường kiểu string
        /// </summary>
        /// <param name="obj"></param>
        public static void TrimObject(object obj)
        {
            Type t = obj.GetType();
            PropertyInfo[] ps = t.GetProperties();

            foreach (PropertyInfo p in ps)
            {
                if (p.PropertyType == typeof(string) || p.PropertyType == typeof(String))
                {
                    string str = (string)p.GetValue(obj, null);
                    str = str == null ? null : str.Trim();
                    p.SetValue(obj, str, null);
                }
            }
        }
        public static string htmlEndCode(string val)
        {
            string trueEncode = "";
            if (val == null)
                return trueEncode;
            string encode = HttpUtility.HtmlEncode(val.ToString());
            if (encode.Contains("&#192;"))
                encode = encode.Replace("&#192;", "À");
            if (encode.Contains("&#193;"))
                encode = encode.Replace("&#193;", "Á");
            if (encode.Contains("&#194;"))
                encode = encode.Replace("&#194;", "Â");
            if (encode.Contains("&#195;"))
                encode = encode.Replace("&#195;", "Ã");
            if (encode.Contains("&#200;"))
                encode = encode.Replace("&#200;", "È");
            if (encode.Contains("&#201;"))
                encode = encode.Replace("&#201;", "É");
            if (encode.Contains("&#202;"))
                encode = encode.Replace("&#202;", "Ê");
            if (encode.Contains("&#204;"))
                encode = encode.Replace("&#204;", "Ì");
            if (encode.Contains("&#205;"))
                encode = encode.Replace("&#205;", "Í");
            if (encode.Contains("&#208;"))
                encode = encode.Replace("&#208;", "Đ");
            if (encode.Contains("&#210;"))
                encode = encode.Replace("&#210;", "Ò");
            if (encode.Contains("&#211;"))
                encode = encode.Replace("&#211;", "Ó");
            if (encode.Contains("&#212;"))
                encode = encode.Replace("&#212;", "Ô");
            if (encode.Contains("&#213;"))
                encode = encode.Replace("&#213;", "Õ");
            if (encode.Contains("&#217;"))
                encode = encode.Replace("&#217;", "Ù");
            if (encode.Contains("&#218;"))
                encode = encode.Replace("&#218;", "Ú");
            if (encode.Contains("&#221;"))
                encode = encode.Replace("&#221;", "Ý");
            if (encode.Contains("&#224;"))
                encode = encode.Replace("&#224;", "à");
            if (encode.Contains("&#225;"))
                encode = encode.Replace("&#225;", "á");
            if (encode.Contains("&#226;"))
                encode = encode.Replace("&#226;", "â");
            if (encode.Contains("&#227;"))
                encode = encode.Replace("&#227;", "ã");
            if (encode.Contains("&amp;#7841;"))
                encode = encode.Replace("&amp;#7841;", "ạ");
            if (encode.Contains("&#232;"))
                encode = encode.Replace("&#232;", "è");
            if (encode.Contains("&#233;"))
                encode = encode.Replace("&#233;", "é");
            if (encode.Contains("&#234;"))
                encode = encode.Replace("&#234;", "ê");
            if (encode.Contains("&#236;"))
                encode = encode.Replace("&#236;", "ì");
            if (encode.Contains("&#237;"))
                encode = encode.Replace("&#237;", "í");
            if (encode.Contains("&#242;"))
                encode = encode.Replace("&#242;", "ò");
            if (encode.Contains("&#243;"))
                encode = encode.Replace("&#243;", "ó");
            if (encode.Contains("&#244;"))
                encode = encode.Replace("&#244;", "ô");
            if (encode.Contains("&#245;"))
                encode = encode.Replace("&#245;", "õ");
            if (encode.Contains("&#249;"))
                encode = encode.Replace("&#249;", "ù");
            if (encode.Contains("&#250;"))
                encode = encode.Replace("&#250;", "ú");
            if (encode.Contains("&#253;"))
                encode = encode.Replace("&#253;", "ý");

            trueEncode = encode;
            return trueEncode;
        }
        public static string htmlEndCode(object obj)
        {
            string trueEncode = "";
            if (obj == null)
                return trueEncode;
            string encode = HttpUtility.HtmlEncode(obj.ToString());
            if (encode.Contains("&#192;"))
                encode = encode.Replace("&#192;", "À");
            if (encode.Contains("&#193;"))
                encode = encode.Replace("&#193;", "Á");
            if (encode.Contains("&#194;"))
                encode = encode.Replace("&#194;", "Â");
            if (encode.Contains("&#195;"))
                encode = encode.Replace("&#195;", "Ã");
            if (encode.Contains("&#200;"))
                encode = encode.Replace("&#200;", "È");
            if (encode.Contains("&#201;"))
                encode = encode.Replace("&#201;", "É");
            if (encode.Contains("&#202;"))
                encode = encode.Replace("&#202;", "Ê");
            if (encode.Contains("&#204;"))
                encode = encode.Replace("&#204;", "Ì");
            if (encode.Contains("&#205;"))
                encode = encode.Replace("&#205;", "Í");
            if (encode.Contains("&#208;"))
                encode = encode.Replace("&#208;", "Đ");
            if (encode.Contains("&#210;"))
                encode = encode.Replace("&#210;", "Ò");
            if (encode.Contains("&#211;"))
                encode = encode.Replace("&#211;", "Ó");
            if (encode.Contains("&#212;"))
                encode = encode.Replace("&#212;", "Ô");
            if (encode.Contains("&#213;"))
                encode = encode.Replace("&#213;", "Õ");
            if (encode.Contains("&#217;"))
                encode = encode.Replace("&#217;", "Ù");
            if (encode.Contains("&#218;"))
                encode = encode.Replace("&#218;", "Ú");
            if (encode.Contains("&#221;"))
                encode = encode.Replace("&#221;", "Ý");
            if (encode.Contains("&#224;"))
                encode = encode.Replace("&#224;", "à");
            if (encode.Contains("&#225;"))
                encode = encode.Replace("&#225;", "á");
            if (encode.Contains("&#226;"))
                encode = encode.Replace("&#226;", "â");
            if (encode.Contains("&#227;"))
                encode = encode.Replace("&#227;", "ã");
            if (encode.Contains("&amp;#7841;"))
                encode = encode.Replace("&amp;#7841;", "ạ");
            if (encode.Contains("&#232;"))
                encode = encode.Replace("&#232;", "è");
            if (encode.Contains("&#233;"))
                encode = encode.Replace("&#233;", "é");
            if (encode.Contains("&#234;"))
                encode = encode.Replace("&#234;", "ê");
            if (encode.Contains("&#236;"))
                encode = encode.Replace("&#236;", "ì");
            if (encode.Contains("&#237;"))
                encode = encode.Replace("&#237;", "í");
            if (encode.Contains("&#242;"))
                encode = encode.Replace("&#242;", "ò");
            if (encode.Contains("&#243;"))
                encode = encode.Replace("&#243;", "ó");
            if (encode.Contains("&#244;"))
                encode = encode.Replace("&#244;", "ô");
            if (encode.Contains("&#245;"))
                encode = encode.Replace("&#245;", "õ");
            if (encode.Contains("&#249;"))
                encode = encode.Replace("&#249;", "ù");
            if (encode.Contains("&#250;"))
                encode = encode.Replace("&#250;", "ú");
            if (encode.Contains("&#253;"))
                encode = encode.Replace("&#253;", "ý");

            trueEncode = encode;
            return trueEncode;

        }
        /// <summary>
        ///Author: PhuongLT
        /// Kiem tra ngày tháng
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static bool checkDatetime(string date)
        {
            Regex reg = new Regex(@"^(((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00|[048])))$");
            if (reg.IsMatch(date) == false)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        /// <summary>
        /// Chuyển đổi tiếng Việt có dấu thành không dấu
        /// PhuongLt15: 20/05/2014
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ConvertVN(string value)
        {
            //---------------------------------a^
            value = value.Replace("ấ", "a");
            value = value.Replace("ầ", "a");
            value = value.Replace("ẩ", "a");
            value = value.Replace("ẫ", "a");
            value = value.Replace("ậ", "a");
            //---------------------------------A^ 
            value = value.Replace("Ấ", "A");
            value = value.Replace("Ầ", "A");
            value = value.Replace("Ẩ", "A");
            value = value.Replace("Ẫ", "A");
            value = value.Replace("Ậ", "A");
            //---------------------------------a( 
            value = value.Replace("ắ", "a");
            value = value.Replace("ằ", "a");
            value = value.Replace("ẳ", "a");
            value = value.Replace("ẵ", "a");
            value = value.Replace("ặ", "a");
            //---------------------------------A(
            value = value.Replace("Ắ", "A");
            value = value.Replace("Ằ", "A");
            value = value.Replace("Ẳ", "A");
            value = value.Replace("Ẵ", "A");
            value = value.Replace("Ặ", "A");

            //---------------------------------a
            value = value.Replace("á", "a");
            value = value.Replace("à", "a");
            value = value.Replace("ả", "a");
            value = value.Replace("ã", "a");
            value = value.Replace("ạ", "a");
            value = value.Replace("â", "a");
            value = value.Replace("ă", "a");

            //---------------------------------A
            value = value.Replace("Á", "A");
            value = value.Replace("À", "A");
            value = value.Replace("Ả", "A");
            value = value.Replace("Ã", "A");
            value = value.Replace("Ạ", "A");
            value = value.Replace("Â", "A");
            value = value.Replace("Ă", "A");
            //---------------------------------A
            value = value.Replace("ế", "e");
            value = value.Replace("ề", "e");
            value = value.Replace("ể", "e");
            value = value.Replace("ễ", "e");
            value = value.Replace("ệ", "e");
            //---------------------------------E^
            value = value.Replace("Ế", "E");
            value = value.Replace("Ề", "E");
            value = value.Replace("Ể", "E");
            value = value.Replace("Ễ", "E");
            value = value.Replace("Ệ", "E");
            //---------------------------------e
            value = value.Replace("é", "e");
            value = value.Replace("è", "e");
            value = value.Replace("ẻ", "e");
            value = value.Replace("ẽ", "e");
            value = value.Replace("ẹ", "e");
            value = value.Replace("ê", "e");

            //---------------------------------E
            value = value.Replace("É", "E");
            value = value.Replace("È", "E");
            value = value.Replace("Ẻ", "E");
            value = value.Replace("Ẽ", "E");
            value = value.Replace("Ẹ", "E");
            value = value.Replace("Ê", "E");
            //---------------------------------i
            value = value.Replace("í", "i");
            value = value.Replace("ì", "i");
            value = value.Replace("ỉ", "i");
            value = value.Replace("ĩ", "i");
            value = value.Replace("ị", "i");

            //---------------------------------I
            value = value.Replace("Í", "I");
            value = value.Replace("Ì", "I");
            value = value.Replace("Ỉ", "I");
            value = value.Replace("Ĩ", "I");
            value = value.Replace("Ị", "I");
            //---------------------------------o^ 
            value = value.Replace("ố", "o");
            value = value.Replace("ồ", "o");
            value = value.Replace("ổ", "o");
            value = value.Replace("ỗ", "o");
            value = value.Replace("ộ", "o");
            //---------------------------------O^ 
            value = value.Replace("Ố", "O");
            value = value.Replace("Ồ", "O");
            value = value.Replace("Ổ", "O");
            value = value.Replace("Ô", "O");
            value = value.Replace("Ộ", "O");
            //---------------------------------o*
            value = value.Replace("ớ", "o");
            value = value.Replace("ờ", "o");
            value = value.Replace("ở", "o");
            value = value.Replace("ỡ", "o");
            value = value.Replace("ợ", "o");
            //---------------------------------O*
            value = value.Replace("Ớ", "O");
            value = value.Replace("Ờ", "O");
            value = value.Replace("Ở", "O");
            value = value.Replace("Ỡ", "O");
            value = value.Replace("Ợ", "O");
            //---------------------------------u*
            value = value.Replace("ứ", "u");
            value = value.Replace("ử", "u");
            value = value.Replace("ừ", "u");
            value = value.Replace("ữ", "u");
            value = value.Replace("ự", "u");
            //---------------------------------U*
            value = value.Replace("Ứ", "U");
            value = value.Replace("Ừ", "U");
            value = value.Replace("Ử", "U");
            value = value.Replace("Ữ", "U");
            value = value.Replace("Ự", "U");

            //---------------------------------y
            value = value.Replace("ý", "y");
            value = value.Replace("ỳ", "y");
            value = value.Replace("ỷ", "y");
            value = value.Replace("ỹ", "y");
            value = value.Replace("ỵ", "y");
            //---------------------------------y
            value = value.Replace("Ý", "Y");
            value = value.Replace("Ỳ", "Y");
            value = value.Replace("Ỷ", "Y");
            value = value.Replace("Ỹ", "Y");
            value = value.Replace("Ỵ", "Y");

            //---------------------------------DD 
            value = value.Replace("Đ", "D");
            value = value.Replace("Đ", "D");
            value = value.Replace("đ", "d");

            //---------------------------------0
            value = value.Replace("ó", "o");
            value = value.Replace("ò", "o");
            value = value.Replace("ỏ", "o");
            value = value.Replace("õ", "o");
            value = value.Replace("ọ", "o");

            value = value.Replace("ô", "o");
            value = value.Replace("ơ", "o");
            //---------------------------------0
            value = value.Replace("Ó", "O");
            value = value.Replace("Ò", "O");
            value = value.Replace("Ỏ", "O");
            value = value.Replace("Õ", "O");
            value = value.Replace("Ọ", "O");
            value = value.Replace("Ô", "O");
            value = value.Replace("Ơ", "O");

            //---------------------------------u
            value = value.Replace("ú", "u");
            value = value.Replace("ù", "u");
            value = value.Replace("ủ", "u");
            value = value.Replace("ũ", "u");
            value = value.Replace("ụ", "u");
            value = value.Replace("ư", "u");

            //---------------------------------U
            value = value.Replace("Ú", "U");
            value = value.Replace("Ù", "U");
            value = value.Replace("Ủ", "U");
            value = value.Replace("Ũ", "U");
            value = value.Replace("Ụ", "U");
            value = value.Replace("Ư", "U");
            return value;
        }
        /// <summary>
        /// Cắt bỏ các ký tự HTML
        /// Phuonglt15: 20/05/2014
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string StripHTML(string source)
        {
            try
            {
                string result;

                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                result = source.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating spaces because browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                // Remove the header (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result,
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty,
                //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV> and <TR> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything that's enclosed inside < >
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // replace special characters:
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove all others. More can be added, see
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // for testing
                //System.Text.RegularExpressions.Regex.Replace(result,
                //       this.txtRegex.Text,string.Empty,
                //       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // make line breaking consistent
                result = result.Replace("\n", "\r");

                // Remove extra line breaks and tabs:
                // replace over 2 breaks with 2 and over 4 tabs with 4.
                // Prepare first to remove any whitespaces in between
                // the escaped characters and remove redundant tabs in between line breaks
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "\t\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "\t\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove redundant tabs
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove multiple tabs following a line break with just one tab
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Initial replacement target string for line breaks
                string breaks = "\r\r\r";
                // Initial replacement target string for tabs
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }

                // That's it.
                return result;
            }
            catch
            {

                return source;
            }
        }
        /// <summary>
        ///Author: PhuongLT
        /// Hàm gửi mail
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool SendMsg(string subject = "N/A", string msg = "N/A", string urlfile = "", int doctorId = 0)
        {
            string _mailServer = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ftpServer"]);
            int _mailPort = Int32.Parse(ConfigurationManager.AppSettings["sslPort"].ToString());
            string _user = Convert.ToString(ConfigurationManager.AppSettings["ftpUser"]);
            string _pass = Convert.ToString(ConfigurationManager.AppSettings["ftpPassword"]);
            MailMessage mailMessage = new MailMessage();
            SmtpClient mailClient = new SmtpClient(_mailServer, _mailPort);
            mailClient.Timeout = 15000;
            mailClient.Credentials = new NetworkCredential(_user, _pass);
            mailClient.EnableSsl = true;        //mailClient.UseDefaultCredentials = true; // no work   
            mailMessage.IsBodyHtml = true;
            mailMessage.From = new MailAddress(_user);
            mailMessage.Subject = subject;
            mailMessage.Body = msg;
            mailMessage.Priority = MailPriority.High;
            string fileName = urlfile;
            if (!String.IsNullOrEmpty(fileName) && fileName.Length > 0)
            {
                string[] vArray = fileName.Split(';');
                if (vArray.Length > 0)
                {
                    for (int i = 0; i < vArray.Length; i++)
                    {
                        string file = vArray[i].ToString();
                        string pathfile = System.Web.HttpContext.Current.Server.MapPath(file);
                        mailMessage.Attachments.Add(new Attachment(pathfile));
                    }
                }

            }
            //-----------------------//
            string mailto = "";// từ DOCTORS_ID lấy ra mail của bác sĩ
            //----------------------//
            mailMessage.To.Add(mailto);
            try
            {
                mailClient.Send(mailMessage);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                mailMessage.Dispose();
                mailClient.Dispose();
            }
        }
        /// <summary>
        ///Author: PhuongLT
        /// Đếm số ký tự
        /// </summary>
        /// <param name="value"></param>
        /// <param name="chr"></param>
        /// <returns></returns>
        public static int Count(string value, char chr)
        {

            int result = 0;
            // char c = ';';
            foreach (char c in value)
            {
                if (c == chr)
                {
                    result++;
                }

            }

            return result;
        }
        /// <summary>
        ///Author: PhuongLT
        /// Bôi màu đoạn văn tìm kiếm trên lưới
        /// </summary>
        /// <param name="strChuoi"></param>
        /// <param name="strTuKhoa"></param>
        /// <returns></returns>
        public static string DrawSearch(string strChuoi, string strTuKhoa)
        {
            string str = strTuKhoa.Trim();
            string result = "";
            if (Count(str, ' ') != 0)
            {
                string[] spt = new string[Count(str, ' ')];
                spt = str.Split(' ');
                for (int i = 0; i <= Count(str, ' '); i++)
                {
                    string sp = spt[i].Trim();
                    if (sp != "" && sp != null)
                    {
                        string gd = spt[i].Trim();
                        string gt = gd[0].ToString();
                        result += " " + gd.Remove(0, 1).Insert(0, gt.ToUpper());
                    }
                }

            }
            else
            {
                string gt = str[0].ToString();
                result = str.Remove(0, 1).Insert(0, gt.ToUpper());
            }
            return strChuoi.Replace(result.Trim(), "<span class='searchLight'>" + result.Trim() + "</span>");


        }
        /// <summary>
        /// Chuyển đổi ngày tháng sang thứ
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static string GetDayName(DateTime date)
        {
            var day = date.DayOfWeek;
            var result = string.Empty;
            switch (day)
            {
                case DayOfWeek.Friday:
                    result = "Thứ 6";
                    break;
                case DayOfWeek.Monday:
                    result = "Thứ 2";
                    break;
                case DayOfWeek.Saturday:
                    result = "Thứ 7";
                    break;
                case DayOfWeek.Sunday:
                    result = "Chủ nhật";
                    break;
                case DayOfWeek.Thursday:
                    result = "Thứ 5";
                    break;
                case DayOfWeek.Tuesday:
                    result = "Thứ 3";
                    break;
                case DayOfWeek.Wednesday:
                    result = "Thứ 4";
                    break;
            }
            return result;
        }
        public static string GetDayNameVT(DateTime date)
        {
            var day = date.DayOfWeek;
            var result = string.Empty;
            switch (day)
            {
                case DayOfWeek.Friday:
                    result = "T6";
                    break;
                case DayOfWeek.Monday:
                    result = "T2";
                    break;
                case DayOfWeek.Saturday:
                    result = "T7";
                    break;
                case DayOfWeek.Sunday:
                    result = "CN";
                    break;
                case DayOfWeek.Thursday:
                    result = "T5";
                    break;
                case DayOfWeek.Tuesday:
                    result = "T3";
                    break;
                case DayOfWeek.Wednesday:
                    result = "T4";
                    break;
            }
            return result;
        }

        public static string GetRootForder
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["RootContentFolder"]; }
        }

        public static string GetForderAvatar
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["RootContentFolder"] + @"Images\"; }
        }
        public static string GetFolderImageThiCongCoc
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["FolderImageThiCongCoc"]; }
        }
        public static string GetFolderImageAvatar
        {
            get { return System.Configuration.ConfigurationManager.AppSettings["FolderImageAvatar"]; }
        }
        /// <summary>
        /// Hàm đổi đường dẫn ảnh từ sang StringBase64.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ImageToBase64(string path)
        {
            if (string.IsNullOrEmpty(path) || !File.Exists(path.Trim()))
            {
                //path = GetRootForder + @"\Images\UserAvartars\doctor_avatar_default.png";
                path = GetRootForder + @"accent.png";
                if (!File.Exists(path))
                    return string.Empty;
            }

            var file = new FileStream(path, FileMode.Open, FileAccess.Read);
            var dtByte = new byte[file.Length];
            file.Read(dtByte, 0, System.Convert.ToInt32(file.Length));
            //Close the File Stream
            file.Close();
            //return dtByte; //return the byte data
            //Byte[] _byte = file.ex
            var extension = Path.GetExtension(path).Trim().Replace(".", "");
            return string.Format("data:image/{0};base64,{1}", extension, Convert.ToBase64String(dtByte));
        }


        /// <summary>
        /// Lấy avatar dưới dạng stringbase4
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetPathUserImage(string url)
        {
            string path = GetRootForder;// +"/Images/";
            var fullPath = path+url;
            if (File.Exists(fullPath))
                return ImageToBase64(fullPath);
            else
                return ImageToBase64(path + "accent.png");
        }
        public static string GetPathImageThiCongCoc(string url)
        {
            string path = GetFolderImageThiCongCoc;
            var fullPath = path + url;
            if (File.Exists(fullPath))
                return ImageToBase64(fullPath);
            else
                return "";// ImageToBase64(path + "accent.png");
        }
        public static string GetPathImageAvatar(string url)
        {
            string path = GetFolderImageAvatar;
            var fullPath = path + url;
            if (File.Exists(fullPath))
                return ImageToBase64(fullPath);
            else
                return ImageToBase64(path + "noavatar.png");
        }
        public static string GetIP4Address()
        {
            string IP4Address = String.Empty;

            foreach (IPAddress IPA in Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            if (IP4Address != String.Empty)
            {
                return IP4Address;
            }

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            return IP4Address;
        }
        /// <summary>
        /// Địa chỉ IP trên máy tính cá nhân
        /// </summary>
        /// <returns></returns>
        public static string GetComputerLanIP()
        {
            string strHostName = Dns.GetHostName();

            IPHostEntry ipEntry = Dns.GetHostEntry(strHostName);

            foreach (IPAddress ipAddress in ipEntry.AddressList)
            {
                if (ipAddress.AddressFamily.ToString() == "InterNetwork")
                {
                    return ipAddress.ToString();
                }
            }

            return "-";
        }
        /// <summary>
        /// Địa chỉ IP trên Server
        /// </summary>
        /// <returns></returns>
        private static string GetComputer_InternetIP()
        {
            // check IP using DynDNS's service
            WebRequest request = WebRequest.Create("http://checkip.dyndns.org");
            WebResponse response = request.GetResponse();
            StreamReader stream = new StreamReader(response.GetResponseStream());

            // IMPORTANT: set Proxy to null, to drastically INCREASE the speed of request
            request.Proxy = null;

            // read complete response
            string ipAddress = stream.ReadToEnd();

            // replace everything and keep only IP
            return ipAddress.
                Replace("<html><head><title>Current IP Check</title></head><body>Current IP Address: ", string.Empty).
                Replace("</body></html>", string.Empty);
        }

        #region "Design Calendar"
        public static DateTime ConvertToDateTime(string day, string month, string year)
        {
            DateTime dtime = Convert.ToDateTime(day + "/" + Convert.ToString(month) + "/" + Convert.ToString(year));
            return dtime;
        }
        /// <summary>
        /// Lịch tháng của ban giám đốc
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DataTable CalendarWeekly(DateTime date)
        {
            int iMonth = date.Month;
            int iYear = date.Year;
            DataTable dt = new DataTable();
            dt.Columns.Add("Weekly", typeof(string));
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Time", typeof(string));
            dt.Columns.Add("Start", typeof(int));
            dt.Columns.Add("End", typeof(int));
            int countDay = GetDaysInMonth(iMonth, iYear);
            DataRow dr = dt.NewRow();
            dr["Weekly"] = "Tuần 1 (01-07/" + iMonth + "/" + iYear + ")";
            dr["Name"] = "Tuần 1";
            dr["Time"] = "01-07/" + iMonth + "/" + iYear + "";
            dr["Start"] = 1;
            dr["End"] = 7;
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Weekly"] = "Tuần 2 (08-14/" + iMonth + "/" + iYear + ")";
            dr["Name"] = "Tuần 2";
            dr["Time"] = "08-14/" + iMonth + "/" + iYear + "";
            dr["Start"] = 8;
            dr["End"] = 14;
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Weekly"] = "Tuần 3 (15-21/" + iMonth + "/" + iYear + ")";
            dr["Name"] = "Tuần 3";
            dr["Time"] = "15-21/" + iMonth + "/" + iYear + "";
            dr["Start"] = 15;
            dr["End"] = 21;
            dt.Rows.Add(dr);
            dr = dt.NewRow();
            dr["Weekly"] = "Tuần 4 (22-" + countDay + "/" + iMonth + "/" + iYear + ")";
            dr["Name"] = "Tuần 4";
            dr["Time"] = "22-" + countDay + "/" + iMonth + "/" + iYear + "";
            dr["Start"] = 22;
            dr["End"] = countDay;
            dt.Rows.Add(dr);
            dr = null;
            return dt;

        }
        /// <summary>
        ///Author:  PhuongLT15
        /// Xác định ngày đầu tiên trong háng thuộc thứ mấy
        /// </summary>
        /// <param name="date1"></param>
        /// <returns></returns>
        public static int FirstDate(DateTime date1)
        {
            //tham so thang,nam hien tai
            int month = date1.Month;
            int year = date1.Year;
            //////////
            // DateTime dtime = Convert.ToDateTime(Convert.ToString(month) + "/" + "1" + "/" + Convert.ToString(year));           
            DateTime dtime = ConvertToDateTime("1", Convert.ToString(month), Convert.ToString(year));
            int date = 0;
            string thu = Getday(dtime);
            switch (thu)
            {

                case "ThuHai":
                    date = 0;
                    break;
                case "ThuBa":
                    date = 1;
                    break;
                case "ThuTu":
                    date = 2;
                    break;
                case "ThuNam":
                    date = 3;
                    break;
                case "ThuSau":
                    date = 4;
                    break;
                case "ThuBay":
                    date = 5;
                    break;
                case "ChuNhat":
                    date = 6;
                    break;

            }
            return date;

        }
        /// <summary>
        /// Tạo bảng động lưu dữ liệu thứ
        /// </summary>
        /// <returns></returns>
        public static DataTable CreatTb()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ThuHai", typeof(string));
            dt.Columns.Add("ThuBa", typeof(string));
            dt.Columns.Add("ThuTu", typeof(string));
            dt.Columns.Add("ThuNam", typeof(string));
            dt.Columns.Add("ThuSau", typeof(string));
            dt.Columns.Add("ThuBay", typeof(string));
            dt.Columns.Add("ChuNhat", typeof(string));
            return dt;

        }
        /// <summary>
        /// Đổ toàn bộ dữ liệu ngày của tháng lên Datatable
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DataTable Fill(DateTime date)
        {
            string[,] arr = new string[8, 7];
            arr[0, 0] = "ThuHai";
            arr[0, 1] = "ThuBa";
            arr[0, 2] = "ThuTu";
            arr[0, 3] = "ThuNam";
            arr[0, 4] = "ThuSau";
            arr[0, 5] = "ThuBay";
            arr[0, 6] = "ChuNhat";
            int dem = 0;
            for (int j = 1; j <= CountRow(date); j++)
            {
                if (j == 1)
                {
                    for (int i = FirstDate(date); i < j * 7; i++)
                    {
                        dem = dem + 1;
                        //tham so thang,nam hien tai
                        int month = date.Month;
                        int year = date.Year;
                        //////////                   
                        DateTime dtime = ConvertToDateTime(Convert.ToString(dem), Convert.ToString(month), Convert.ToString(year));
                        int day = dtime.Day;
                        string thu = Getday(dtime);
                        // lblDay.Text = lblDay.Text + thu;
                        switch (thu)
                        {


                            case "ThuHai":
                                arr[j, 0] = day.ToString();
                                break;
                            case "ThuBa":
                                arr[j, 1] = day.ToString();
                                break;
                            case "ThuTu":
                                arr[j, 2] = day.ToString();
                                break;
                            case "ThuNam":
                                arr[j, 3] = day.ToString();
                                break;
                            case "ThuSau":
                                arr[j, 4] = day.ToString();
                                break;
                            case "ThuBay":
                                arr[j, 5] = day.ToString();
                                break;
                            case "ChuNhat":
                                arr[j, 6] = day.ToString();
                                break;

                        }
                    }
                }
                else
                {
                    for (int i = (j - 1) * 7 + 1; i <= j * 7; i++)
                    {
                        dem = dem + 1;
                        if (dem <= GetDaysInMonth(date.Month, date.Year))
                        {
                            //tham so thang,nam hien tai
                            int month = date.Month;
                            int year = date.Year;
                            //////////                          
                            DateTime dtime = ConvertToDateTime(Convert.ToString(dem), Convert.ToString(month), Convert.ToString(year));
                            int day = dtime.Day;
                            string thu = Getday(dtime);
                            // lblDay.Text = lblDay.Text + thu;
                            switch (thu)
                            {
                                case "ThuHai":
                                    arr[j, 0] = day.ToString();
                                    break;
                                case "ThuBa":
                                    arr[j, 1] = day.ToString();
                                    break;
                                case "ThuTu":
                                    arr[j, 2] = day.ToString();
                                    break;
                                case "ThuNam":
                                    arr[j, 3] = day.ToString();
                                    break;
                                case "ThuSau":
                                    arr[j, 4] = day.ToString();
                                    break;
                                case "ThuBay":
                                    arr[j, 5] = day.ToString();
                                    break;
                                case "ChuNhat":
                                    arr[j, 6] = day.ToString();
                                    break;

                            }
                        }

                    }
                }

            }


            DataTable dt = CreatTb();
            //dong chay
            for (int i = 1; i <= CountRow(date); i++)
            {
                DataRow dr = dt.NewRow();
                //cot chay
                for (int j = 0; j <= 6; j++)
                {
                    if (arr[0, j] == "ChuNhat")
                    {
                        dr["ChuNhat"] = arr[i, j];
                    }
                    else if (arr[0, j] == "ThuHai")
                    {
                        dr["ThuHai"] = arr[i, j];
                    }
                    else if (arr[0, j] == "ThuBa")
                    {
                        dr["ThuBa"] = arr[i, j];
                    }
                    else if (arr[0, j] == "ThuTu")
                    {
                        dr["ThuTu"] = arr[i, j];
                    }
                    else if (arr[0, j] == "ThuNam")
                    {
                        dr["ThuNam"] = arr[i, j];
                    }
                    else if (arr[0, j] == "ThuSau")
                    {
                        dr["ThuSau"] = arr[i, j];
                    }
                    else
                    {
                        dr["ThuBay"] = arr[i, j];
                    }
                }
                dt.Rows.Add(dr);
            }
            return dt;

        }

        /// <summary>
        /// Xác định 1 ngày nào đó là thuộc thứ mấy
        /// </summary>
        /// <param name="ngay"></param>
        /// <returns></returns>
        public static string Getday(DateTime ngay, int? dayType = null)
        {
            var lstDay = new List<string>();
            string[] arrDaysDefault = { "Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy" };
            string[] arrDays = { "Chủ nhật", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7" };
            string[] thu = { "ChuNhat", "ThuHai", "ThuBa", "ThuTu", "ThuNam", "ThuSau", "ThuBay" };
            if (dayType.HasValue)
            {
                //DayType == 1 or = 2
                //Có thể bổ sung enum cho các trường hợp dùng thứ dạng text, hay dạng số
                lstDay = dayType.Value == 1 ? arrDays.ToList() : arrDaysDefault.ToList();
            }
            else
            {
                lstDay = thu.ToList();
            }
            string re = lstDay[System.Convert.ToInt32(ngay.DayOfWeek)].ToString();
            return re;
        }
        /// <summary>
        /// Xác định số ngày trong 1 tháng là bao ngày
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        public static int GetDaysInMonth(int month, int year)
        {
            if (month < 1 || month > 12)
            {

            }
            if (1 == month || 3 == month || 5 == month || 7 == month || 8 == month ||
            10 == month || 12 == month)
            {
                return 31;
            }
            else if (2 == month)
            {

                if (0 == (year % 4))
                {

                    if (0 == (year % 400))
                    {
                        return 29;
                    }
                    else if (0 == (year % 100))
                    {
                        return 28;
                    }


                    return 29;
                }

                return 28;
            }
            return 30;
        }
        /// <summary>
        /// Đếm số dòng sẽ hiển thị ra khi dựa vào ngày 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int CountRow(DateTime date)
        {
            //tham so thang,nam hien tai
            int month = date.Month;
            int year = date.Year;
            //////////       
            DateTime dtime = ConvertToDateTime("1", Convert.ToString(month), Convert.ToString(year));
            int countrow = GetDaysInMonth(date.Month, date.Year);
            string thu = Getday(dtime);

            if (countrow < 29 && thu == "ThuHai")
            {
                return 4;
            }
            else if ((countrow >= 30 && thu == "ChuNhat") || (countrow == 31 && thu == "ThuBay"))
            {
                return 6;
            }

            else
            {
                return 5;
            }
        }
        /// <summary>
        /// Trở về tháng trước đó
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime NextMonth(DateTime date)
        {

            DateTime pass = date.AddMonths(-1);
            return pass;
        }
        /// <summary>
        /// Trở về tháng phía sau đó
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime PreMonth(DateTime date)
        {
            DateTime pass = date.AddMonths(1);
            return pass;
        }
        /// <summary>
        /// Lấy trạng thái lịch trực
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
  
        #endregion

        //Biến lấy định dạng ngôn ngữ
        public static string strLanguageFormat = System.Configuration.ConfigurationManager.AppSettings["LanguageFormat"];
        public static bool IsImage(this HttpPostedFileBase postedFile)
        {
            try
            {
                using (var bitmap = new System.Drawing.Bitmap(postedFile.InputStream))
                {
                    if (bitmap.Size.IsEmpty)
                    {
                        return false;
                    }
                    else return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }



        /// <summary>
        /// created by: HienDM1
        /// create date: 07/04/2015
        /// Hàm xuất dữ liệu từ datatable ra excel 
        /// </summary>
        /// <param name="firstTime">Lần đầu tiên ghi file</param>
        /// /// <param name="fileName">Đường dẫn file excel</param>
        /// /// <param name="ResultsData">Dữ liệu trong datatable</param>
        /// <returns></returns>
        ///

    
        /// <summary>
        /// created by: HienDM1
        /// create date: 07/04/2015
        /// Hàm kiểm tra dữ liệu ngày giờ 
        /// </summary>
        /// <param name="obj">Thông tin kiểm tra</param>
        /// <returns></returns>
        ///
        public static bool validateDateTime(string obj)
        {
            if (obj == null || obj.Trim() == "") return true;
            obj = obj.Trim();
            bool check = false;
            try
            {
                if (obj.Length == 8)
                {
                    obj += "0000";
                }
                if (obj.Length == 12)
                {
                    int year = int.Parse(obj.Substring(0, 4));
                    int month = int.Parse(obj.Substring(4, 2));
                    int day = int.Parse(obj.Substring(6, 2));
                    int hour = int.Parse(obj.Substring(8, 2));
                    int minute = int.Parse(obj.Substring(10, 2));

                    if (!(year >= 1900 && year <= DateTime.Now.Year))
                    {
                        return false;
                    }
                    if (!(month >= 1 && month <= 12))
                    {
                        return false;
                    }
                    if (!(day >= 1 && day <= DateTime.DaysInMonth(year, month)))
                    {
                        return false;
                    }
                    if (!(hour >= 0 && hour <= 23))
                    {
                        return false;
                    }
                    if (!(minute >= 0 && minute <= 59))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                check = true;
            }
            catch (Exception ex)
            {
                // Nếu ép kiểu lỗi thì check = false
            }
            return check;
        }

        /// <summary>
        /// created by: HienDM1
        /// create date: 07/04/2015
        /// Hàm kiểm tra dữ liệu ngày
        /// </summary>
        /// <param name="obj">Thông tin kiểm tra</param>
        /// <returns></returns>
        ///
        public static bool validateDate(string obj)
        {
            if (obj == null || obj.Trim() == "") return true;
            obj = obj.Trim();
            bool check = false;
            try
            {
                if (obj.Length == 8)
                {
                    obj += "0000";
                }
                if (obj.Length == 12)
                {
                    int year = int.Parse(obj.Substring(0, 4));
                    int month = int.Parse(obj.Substring(4, 2));
                    int day = int.Parse(obj.Substring(6, 2));
                    int hour = int.Parse(obj.Substring(8, 2));
                    int minute = int.Parse(obj.Substring(10, 2));

                    if (!(year >= 1900 && year <= DateTime.Now.Year))
                    {
                        return false;
                    }
                    if (!(month >= 1 && month <= 12))
                    {
                        return false;
                    }
                    if (!(day >= 1 && day <= DateTime.DaysInMonth(year, month)))
                    {
                        return false;
                    }
                    if (!(hour >= 0 && hour <= 23))
                    {
                        return false;
                    }
                    if (!(minute >= 0 && minute <= 59))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                check = true;
            }
            catch (Exception ex)
            {
                // Nếu ép kiểu lỗi thì check = false
            }
            return check;
        }
        /// <summary>
        /// created by: HienDM1
        /// create date: 07/04/2015
        /// Hàm kiểm tra dữ liệu ngày
        /// </summary>
        /// <param name="obj">Thông tin kiểm tra</param>
        /// <returns></returns>
        ///
        public static bool validateDateAllowFuture(string obj)
        {
            if (obj == null || obj.Trim() == "") return true;
            obj = obj.Trim();
            bool check = false;
            try
            {
                if (obj.Length == 8)
                {
                    obj += "0000";
                }
                if (obj.Length == 12)
                {
                    int year = int.Parse(obj.Substring(0, 4));
                    int month = int.Parse(obj.Substring(4, 2));
                    int day = int.Parse(obj.Substring(6, 2));
                    int hour = int.Parse(obj.Substring(8, 2));
                    int minute = int.Parse(obj.Substring(10, 2));

                    if (!(year >= 2000 && year <= 9999))
                    {
                        return false;
                    }
                    if (!(month >= 1 && month <= 12))
                    {
                        return false;
                    }
                    if (!(day >= 1 && day <= DateTime.DaysInMonth(year, month)))
                    {
                        return false;
                    }
                    if (!(hour >= 0 && hour <= 23))
                    {
                        return false;
                    }
                    if (!(minute >= 0 && minute <= 59))
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                check = true;
            }
            catch (Exception ex)
            {
                // Nếu ép kiểu lỗi thì check = false
            }
            return check;
        }
        /// <summary>
        /// hinhpt5 
        /// Hàm lấy dữ liệu từ file Excel
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="hasHeaders"></param>
        /// <param name="sheetName"></param>
        /// <param name="dongbatdau"></param>
        /// <param name="soluong"></param>
        /// <param name="iColumnCount"></param>
        /// <returns></returns>
        public static DataTable GetPreviewExcelDataDMThuoc(string FileName, bool hasHeaders, string sheetName, string dongbatdau, int soluong, int iColumnCount = 250)
        {
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            else
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
            int intDongBatDau = Convert.ToInt32(dongbatdau);
            int intDongKetThuc = intDongBatDau + 15;
            DataTable tableTemp = new DataTable(sheetName);

            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                conn.Open();

                DataTable schemaTable = conn.GetOleDbSchemaTable(
                    OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                foreach (DataRow schemaRow in schemaTable.Rows)
                {
                    string sheet = schemaRow["TABLE_NAME"].ToString();
                    if (sheetName == sheet)
                    {
                        if (sheet.EndsWith("$") || sheet.EndsWith("$'"))
                        {
                            try
                            {
                                string cauLenh = "SELECT top " + intDongKetThuc + " * FROM [" + sheet + "]";
                                if (soluong == 0)
                                {
                                    cauLenh = "SELECT  * FROM [" + sheet + "]";
                                }
                                OleDbCommand cmd = new OleDbCommand(cauLenh, conn);
                                cmd.CommandType = CommandType.Text;
                                new OleDbDataAdapter(cmd).Fill(tableTemp);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message + string.Format("Sheet:{0}.File:F{1}", sheet, FileName), ex);
                            }
                            break;
                        }
                    }
                }
            }
            return tableTemp;
        }
        /// <summary>
        /// Hàm đọc sheet từ file Excel
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public static List<string> getSheetInExxcel(string FileName)
        {
            string strConn;
            if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1\"";
            else
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1\"";

            List<string> output = new List<string>();
            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                conn.Open();
                DataTable schemaTable = conn.GetOleDbSchemaTable(
                    OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                foreach (DataRow schemaRow in schemaTable.Rows)
                {
                    string sheet = schemaRow["TABLE_NAME"].ToString();
                    output.Add(sheet);
                }
            }
            return output;
        }

        public static List<string> getColumnInExcel(string FileName, string header, string sheetname)
        {
            string HDR = "Yes";
            if (header.ToLower().Trim() == "no")
            {
                HDR = "No";
            }
            string strConn;
            if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            else
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";

            List<string> output = new List<string>();
            sheetname = sheetname.ToLower().Trim();
            using (OleDbConnection conn = new OleDbConnection(strConn))
            {
                conn.Open();
                DataTable schemaTable = conn.GetOleDbSchemaTable(
                    OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                foreach (DataRow schemaRow in schemaTable.Rows)
                {
                    string sheet = schemaRow["TABLE_NAME"].ToString().ToLower().Trim();
                    if (sheet == sheetname)
                    {
                        //string sExcelColumnName = GetExcelEndDataColumn(250);
                        //OleDbCommand cmd = new OleDbCommand("SELECT * FROM [" + sheet + "A:" + sExcelColumnName + "]", conn);
                        OleDbCommand cmd = new OleDbCommand("select * from [" + sheet + "]", conn);
                        cmd.CommandType = CommandType.Text;

                        DataTable outputTable = new DataTable(sheet);
                        new OleDbDataAdapter(cmd).Fill(outputTable);
                        foreach (DataColumn column in outputTable.Columns)
                        {
                            output.Add(column.ColumnName);
                        }
                        break;
                    }
                }
            }
            return output;
        }

    }
    public class ConvertType
    {
        private static NumberStyles style = NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign;
        private static CultureInfo culture = CultureInfo.CreateSpecificCulture("en-US");
        //private static CultureInfo culture2 = CultureInfo.CreateSpecificCulture("en-US");
        public static short ToShort(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return 0;

            short iValue = 0;
            sValue = sValue.Trim();

            if (short.TryParse(sValue, style, culture, out iValue))
                return iValue;
            else
                throw new Exception("Không thể chuyển giá trị: \"" + sValue + "\" sang kiểu short.");
        }

        public static short? ToShortNull(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return null;

            short iValue = 0;
            sValue = sValue.Trim();

            if (short.TryParse(sValue, style, culture, out iValue))
                return iValue;
            else
                throw new Exception("Không thể chuyển giá trị: \"" + sValue + "\" sang kiểu short.");
        }
        public static int ToShortNgayDieuTri(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return 1;

            short iValue = 0;
            sValue = sValue.Trim();

            if (short.TryParse(sValue, style, culture, out iValue))
                return iValue;
            else
                return 1;
        }

        public static int ToInt(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return 0;

            int iValue = 0;
            sValue = sValue.Trim();

            if (int.TryParse(sValue, style, culture, out iValue))
                return iValue;
            else
                throw new Exception("Không thể chuyển giá trị: \"" + sValue + "\" sang kiểu int.");
        }

        public static int? ToIntNull(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return null;

            int iValue = 0;
            sValue = sValue.Trim();

            if (int.TryParse(sValue, style, culture, out iValue))
                return iValue;
            else
                throw new Exception("Không thể chuyển giá trị: \"" + sValue + "\" sang kiểu int.");
        }

        public static decimal ToDecimal(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return 0;

            decimal dValue = 0;
            sValue = sValue.Trim();

            if (decimal.TryParse(sValue, style, culture, out dValue))
                return dValue;
            else
                throw new Exception("Không thể chuyển giá trị: \"" + sValue + "\" sang kiểu decimal.");
        }

        public static decimal? ToDecimalNull(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return null;

            decimal dValue = 0;
            sValue = sValue.Trim();

            if (decimal.TryParse(sValue, style, culture, out dValue))
                return dValue;
            else
                throw new Exception("Không thể chuyển giá trị: \"" + sValue + "\" sang kiểu decimal.");
        }
        public static decimal? ToDecimalisDecimal(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return null;

            decimal dValue = 0;
            sValue = sValue.Trim();

            if (decimal.TryParse(sValue, style, culture, out dValue))
                return dValue;
            else
                throw new Exception("\"" + sValue + "\" không phải là số.");
        }
        public static bool isDecimal(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return true;

            decimal dValue = 0;
            sValue = sValue.Trim();

            if (decimal.TryParse(sValue, style, culture, out dValue))
                return true;
            else
                return false;
        }
        public static bool isInt(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return true;

            int dValue = 0;
            sValue = sValue.Trim();

            if (int.TryParse(sValue, out dValue))
                return true;
            else
                return false;
        }
        public static decimal ToDecimalToNull(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return 0;

            decimal dValue = 0;
            sValue = sValue.Trim();

            if (decimal.TryParse(sValue, style, culture, out dValue))
                return dValue;
            else
                return 0;
        }
        public static decimal ToDecimalTo1(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return 0;

            decimal dValue = 0;
            sValue = sValue.Trim();

            if (decimal.TryParse(sValue, style, culture, out dValue))
                return dValue;
            else
                return -1;
        }


        public static short ToShortToNull(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return 0;

            short iValue = 0;
            sValue = sValue.Trim();

            if (short.TryParse(sValue, style, culture, out iValue))
                return iValue;
            else
                return 0;
        }

        public static DateTime? VNDateToDateNull(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return null;

            try
            {

                sValue = sValue.Trim();
                string[] strs = sValue.Split("/".ToCharArray());

                int day = Convert.ToInt32(strs[0]);
                int month = Convert.ToInt32(strs[1]);
                int year = Convert.ToInt32(strs[2].Substring(0, 4));
                try { return new DateTime(year, month, day); }
                catch { return new DateTime(year, day, month); }

            }
            catch
            {
                throw new Exception("Giá trị  \"" + sValue + "\" sai định dạng ngày tháng, định dạng đúng dd/mm/yyyy.");
            }
        }
        public static DateTime? VNDateToDayMoth(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return null;

            try
            {

                sValue = sValue.Trim();
                if (sValue.IndexOf("/") < 0)
                {
                    int datevalue;
                    int.TryParse(sValue, out datevalue);
                    return DateTime.FromOADate(datevalue);
                }
                else
                {
                    string[] strs = sValue.Split("/".ToCharArray());

                    int day = Convert.ToInt32(strs[0]);
                    int month = Convert.ToInt32(strs[1]);
                    int year = Convert.ToInt32(strs[2].Substring(0, 4));
                    try { return new DateTime(year, month, day); }
                    catch { return new DateTime(year, day, month); }
                }

            }
            catch
            {
                return null;
            }
        }
        public static DateTime? VNDateToDayMothIsView(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return null;

            try
            {

                sValue = sValue.Trim();
                if (sValue.IndexOf("/") < 0)
                {
                    int datevalue;
                    int.TryParse(sValue, out datevalue);
                    return DateTime.FromOADate(datevalue);
                }
                else
                {
                    string[] strs = sValue.Split("/".ToCharArray());

                    int day = Convert.ToInt32(strs[0]);
                    int month = Convert.ToInt32(strs[1]);
                    int year = Convert.ToInt32(strs[2].Substring(0, 4));
                    try { return new DateTime(year, month, day); }
                    catch { return new DateTime(year, day, month); }
                }

            }
            catch
            {
                return null;
            }
        }
        public static DateTime? VNDateToMothDay(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return null;

            try
            {
                sValue = sValue.Trim();
                string[] strs = sValue.Split("/".ToCharArray());

                int day = Convert.ToInt32(strs[0]);
                int month = Convert.ToInt32(strs[1]);
                int year = Convert.ToInt32(strs[2].Substring(0, 4));
                try { return new DateTime(year, day, month); }
                catch { return new DateTime(year, month, day); }

            }
            catch
            {
                throw new Exception("Giá trị  \"" + sValue + "\" sai định dạng ngày tháng, định dạng đúng dd/mm/yyyy.");
            }
        }
        public static bool isVNDateToDate(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return false;

            try
            {
                sValue = sValue.Trim();
                DateTime a;
                if (sValue.IndexOf("/") < 0)
                {
                    int datevalue;
                    if (int.TryParse(sValue, out datevalue))
                    {
                        a = DateTime.FromOADate(datevalue);
                        return true;
                    }
                    else return false;
                }
                else
                {
                    string[] strs = sValue.Split("/".ToCharArray());
                    int day = Convert.ToInt32(strs[0]);
                    int month = Convert.ToInt32(strs[1]);
                    int year = Convert.ToInt32(strs[2].Substring(0, 4));

                    try
                    {
                        a = new DateTime(year, month, day);
                    }
                    catch
                    {
                        a = new DateTime(year, day, month);

                    }

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }


        public static DateTime? VNDateToDateNull_ENUS(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return null;

            try
            {
                sValue = sValue.Trim();
                string[] strs = sValue.Split("/".ToCharArray());

                int day = Convert.ToInt32(strs[1]);
                int month = Convert.ToInt32(strs[0]);
                int year = Convert.ToInt32(strs[2].Substring(0, 4));
                try { return new DateTime(year, month, day); }
                catch { return new DateTime(year, day, month); }
            }
            catch
            {
                throw new Exception("Giá trị  \"" + sValue + "\" sai định dạng ngày tháng, định dạng đúng dd/mm/yyyy.");
            }
        }
        public static DateTime? VNDateToNull_ENUS(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return null;

            try
            {
                sValue = sValue.Trim();
                string[] strs = sValue.Split("/".ToCharArray());

                int day = Convert.ToInt32(strs[1]);
                int month = Convert.ToInt32(strs[0]);
                int year = Convert.ToInt32(strs[2].Substring(0, 4));
                return new DateTime(year, month, day);
            }
            catch
            {
                return null;
            }
        }
        public static DateTime VNDateToDate(string sValue)
        {
            try
            {
                sValue = sValue.Trim();
                string[] strs = sValue.Split("/".ToCharArray());

                int day = Convert.ToInt32(strs[0]);
                int month = Convert.ToInt32(strs[1]);
                int year = Convert.ToInt32(strs[2]);
                try { return new DateTime(year, month, day); }
                catch { return new DateTime(year, day, month); }
            }
            catch
            {
                throw new Exception("Giá trị  \"" + sValue + "\" sai định dạng ngày tháng, định dạng đúng dd/mm/yyyy.");
            }
        }
        public static DateTime toDateTimeIn(string sValue)
        {
            try
            {
                return Convert.ToDateTime(sValue);
            }
            catch
            {
                throw new Exception("Xin lỗi không thể chuyển đổi giá trị  \"" + sValue + "\" sang kiểu ngày tháng.");
            }
        }
        // Đọc file DBF 
        // VD chuyền địa chỉ file: dbfFileName =@"D:\bdf\kham_nt.dbf"
        public static DataTable Docdbf(string dbfFileName)
        {
            DataTable dt = new DataTable();
            string constr = "Provider=VFPOLEDB.1;Data Source=" + Directory.GetParent(dbfFileName).FullName;
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                var sql = "select * from " + Path.GetFileName(dbfFileName) + ";";
                OleDbCommand cmd = new OleDbCommand(sql, con);
                try
                {
                    con.Open();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                if (con.State == ConnectionState.Open)
                {
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(dt);
                }
                if (con.State == ConnectionState.Open)
                {
                    try
                    {
                        con.Close();
                        con.Dispose();
                    }
                    catch { }
                }
            }
            return dt;


            //     try
            //     {
            //         VTDBF.Service1Client MyClient =
            //new VTDBF.Service1Client();
            //         VTDBF.Employee emp =
            //             new VTDBF.Employee();
            //         emp = MyClient.Docdbf(dbfFileName);
            //         dt = emp.EmployeeTable;
            //         return dt;
            //     }
            //     catch (Exception ex)
            //     {
            //         return dt;
            //     }

        }
        public static DataTable DocXML(string dbfFileName)
        {
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            ds.ReadXml(dbfFileName);
            DataTable d = ds.Tables[0];
            dt = d.Copy();
            return dt;
        }
        public static DataSet DocXML1(string dbfFileName)
        {
            //DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            ds.ReadXml(dbfFileName);
            //dt = ds.Tables[0];
            return ds;
        }

        //luanpm2
        //03/04/2015
        public static DateTime? VNDateToDatetimeNull(string sValue)
        {
            if (string.IsNullOrWhiteSpace(sValue))
                return null;

            try
            {
                if (sValue.Length == 8)
                {
                    sValue += "0000";
                }
                if (sValue.Length >= 12)
                {
                    sValue = sValue.Trim();

                    int year = Convert.ToInt32(sValue.Substring(0, 4));
                    int month = Convert.ToInt32(sValue.Substring(4, 2));
                    int day = Convert.ToInt32(sValue.Substring(6, 2));
                    int h = Convert.ToInt32(sValue.Substring(8, 2));
                    int m = Convert.ToInt32(sValue.Substring(10, 2));

                    return new DateTime(year, month, day, h, m, 0);
                }
                else if (sValue.Length == 8)
                {
                    int year = Convert.ToInt32(sValue.Substring(0, 4));
                    int month = Convert.ToInt32(sValue.Substring(4, 2));
                    int day = Convert.ToInt32(sValue.Substring(6, 2));
                    return new DateTime(year, month, day, 0, 0, 0);
                }
                return null;
            }
            catch
            {
                throw new Exception("Giá trị  \"" + sValue + "\" sai định dạng ngày tháng, định dạng đúng dd/mm/yyyy.");
            }
        }
        //luanpm2
        public static DateTime? DateNull(string sValue)
        {
            // HienDM1: sửa lại để convert được cả ngày tháng 12 ký tự và ngày tháng 8 ký tự
            if (string.IsNullOrWhiteSpace(sValue))
                return null;

            try
            {
                if (sValue.Length == 8)
                {
                    sValue += "0000";
                }
                if (sValue.Length >= 12)
                {
                    sValue = sValue.Trim();

                    int year = Convert.ToInt32(sValue.Substring(0, 4));
                    int month = Convert.ToInt32(sValue.Substring(4, 2));
                    int day = Convert.ToInt32(sValue.Substring(6, 2));
                    int h = Convert.ToInt32(sValue.Substring(8, 2));
                    int m = Convert.ToInt32(sValue.Substring(10, 2));

                    return new DateTime(year, month, day, h, m, 0);
                }
                else if (sValue.Length == 8)
                {
                    int year = Convert.ToInt32(sValue.Substring(0, 4));
                    int month = Convert.ToInt32(sValue.Substring(4, 2));
                    int day = Convert.ToInt32(sValue.Substring(6, 2));
                    return new DateTime(year, month, day, 0, 0, 0);
                }
                return null;
            }
            catch
            {
                throw new Exception("Giá trị  \"" + sValue + "\" sai định dạng ngày tháng, định dạng đúng dd/mm/yyyy.");
            }
        }


    }
    public static class KiemTraThe
    {
        public static string taiKhoanSv = ConfigurationSettings.AppSettings["TaiKhoanServiceKiemTraThe"].ToString();
      
        private static StringBuilder ktDulieuLoi(StringBuilder str, string chkSoThe, string chkDu5nam, string chkHoten, string chkNgaysinh,
            string chkGioitinh, string chkHanthe, string chkMaKcbBd, string chkMakhuvuc, ref int du5nam, ref int oK)
        {
            if (chkSoThe == "1")
            {
                oK = 3;
                str.Append(" Mã thẻ không tồn tại trong cơ sở dữ liệu thẻ;");
            }
            else if (chkHanthe != "0")
            {
                oK = 3;
                if (chkHanthe == "1")
                {
                    str.Append(" Thẻ chưa đến hạn sử dụng;");
                }
                else if (chkHanthe == "2")
                {
                    str.Append(" Thẻ đã quá hạn sử dụng;");
                }
            }
            else
            {
                oK = 2;
                if (chkDu5nam == "1")
                {
                    du5nam = 0;
                    //str.Append(" Thẻ không đủ 05 năm liên tục;");
                }
                else
                {
                    du5nam = 1;
                    //str.Append(" Thẻ đủ 05 năm liên tục;");
                }
                if (chkHoten == "1")
                {
                    str.Append(" Họ và tên không hợp lệ;");
                }
                if (chkNgaysinh == "1")
                {
                    str.Append(" Ngày sinh không hợp lệ;");
                }
                if (chkGioitinh == "1")
                {
                    str.Append(" Giới tính không hợp lệ;");
                }
                if (chkMaKcbBd == "1")
                {
                    str.Append(" Nơi đăng ký khám chữa bệnh không hợp lệ;");
                }
                if (chkMakhuvuc == "1")
                {
                    str.Append(" Mã khu vực không hợp lệ;");
                }
            }
            return str;
        }

    }

    public static class ThongBaoMobiles
    {
        public static List<string> DanhSachNguoiNhan(int RuleType, Guid NhanVienID, string NguoiTaoPhieuID)
        {
            UnitOfWork uof = new UnitOfWork();
            List<string> lst = new List<string>();
            try
            {
                var listDanhSachNhanVienID = (from a in uof.Context.tblGroups
                                              join b in uof.Context.tblGroupUsers on a.GroupGuid equals b.GroupGuid
                                              join c in uof.Context.Users on b.UserGuid equals c.UserId
                                              where a.RuleType == RuleType && c.IsActive == true && c.NhanVienId != NhanVienID
                                              select new { NhanVienID = c.NhanVienId }).ToList();
                bool addNguoiTao = true;
                foreach (var item in listDanhSachNhanVienID)
                {
                    if (item.NhanVienID.ToString() == NguoiTaoPhieuID) addNguoiTao = false;
                    lst.Add(item.NhanVienID.ToString());
                }
                if (addNguoiTao == true) lst.Add(NguoiTaoPhieuID);
                return lst;
            }
            catch (Exception ex)
            {
                return lst;
            }
            
        }

        public static List<string> DanhSachNguoiNhan(int RuleType, Guid NhanVienID)
        {
            UnitOfWork uof = new UnitOfWork();
            List<string> lst = new List<string>();
            try
            {
                var listDanhSachNhanVienID = (from a in uof.Context.tblGroups
                                              join b in uof.Context.tblGroupUsers on a.GroupGuid equals b.GroupGuid
                                              join c in uof.Context.Users on b.UserGuid equals c.UserId
                                              where a.RuleType == RuleType && c.IsActive == true && c.NhanVienId != NhanVienID
                                              select new { NhanVienID = c.NhanVienId }).ToList();
              
                foreach (var item in listDanhSachNhanVienID)
                {
                    lst.Add(item.NhanVienID.ToString());
                }
                return lst;
            }
            catch (Exception ex)
            {
                return lst;
            }

        }
    }

}
