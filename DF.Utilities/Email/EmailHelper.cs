﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace DF.Utilities
{
    public class EmailHelper
    {
        public static bool SendMsg(string fromEmail, string toEmail, string smtpserver, int mailport, string username, string password, string msg, string subject)
        {
            MailMessage mailMessage = new MailMessage();

            SmtpClient mailClient = new SmtpClient(smtpserver, mailport);

            mailClient.Timeout = 15000;

            mailClient.Credentials = new NetworkCredential(username, password);

            mailClient.EnableSsl = true;

            mailMessage.IsBodyHtml = true;

            mailMessage.From = new MailAddress(fromEmail);

            mailMessage.Subject = subject;

            mailMessage.Body = msg;

            mailMessage.Priority = MailPriority.High;

            mailMessage.To.Add(toEmail);

            try
            {
                mailClient.Send(mailMessage);
                return true;
            }

            catch (Exception ex)
            {
                var eror = ex.ToString();
                return false;
            }

            finally
            {
                mailMessage.Dispose();

                mailClient.Dispose();

            }
        }

        public static bool SendMsg(string fromEmail, string toEmail, List<string> ccEmail, string smtpserver, int mailport, string username, string password, string msg, string subject)
        {
            MailMessage mailMessage = new MailMessage();

            SmtpClient mailClient = new SmtpClient(smtpserver, mailport);

            mailClient.Timeout = 15000;

            mailClient.Credentials = new NetworkCredential(username, password);

            mailClient.EnableSsl = true;        //mailClient.UseDefaultCredentials = true; // no work  

            mailMessage.IsBodyHtml = true;

            mailMessage.From = new MailAddress(fromEmail);
            
            foreach (var email in ccEmail)
            {
                MailAddress ccemail = new MailAddress(email);
                mailMessage.CC.Add(ccemail);
            }            

            mailMessage.Subject = subject;

            mailMessage.Body = msg;

            mailMessage.Priority = MailPriority.High;   

            mailMessage.To.Add(toEmail);          

            try
            {
                mailClient.Send(mailMessage);
                return true;
            }

            catch (Exception)
            {
                return false;
            }

            finally
            {
                mailMessage.Dispose();

                mailClient.Dispose();

            }
        }

        public static bool SendMsg(string fromEmail, string toEmail, string smtpserver, int mailport, string username, string password, string msg, string subject , string filepath)
        {
            MailMessage mailMessage = new MailMessage();

            SmtpClient mailClient = new SmtpClient(smtpserver, mailport);

            mailClient.Timeout = 15000;

            mailClient.Credentials = new NetworkCredential(username, password);

            mailClient.EnableSsl = true;        //mailClient.UseDefaultCredentials = true; // no work  

            mailMessage.IsBodyHtml = true;

            mailMessage.From = new MailAddress(fromEmail);

            mailMessage.Subject = subject;

            mailMessage.Body = msg;

            mailMessage.Priority = MailPriority.High;
            if (!String.IsNullOrEmpty(filepath) && filepath.Length > 0)
            {

                string[] vArray = filepath.Split(';');

                if (vArray.Length > 0)
                {

                    for (int i = 0; i < vArray.Length; i++)
                    {

                        string file = vArray[i].ToString();

                        string pathfile = System.Web.HttpContext.Current.Server.MapPath(file);

                        mailMessage.Attachments.Add(new Attachment(pathfile));
                    }
                }
            }

            mailMessage.To.Add(toEmail);

            try
            {
                mailClient.Send(mailMessage);
                return true;
            }

            catch (Exception)
            {
                return false;
            }

            finally
            {
                mailMessage.Dispose();

                mailClient.Dispose();
            }
        }
        public static bool SendMsg(string fromEmail, string toEmail, List<string> ccEmail, string smtpserver, int mailport, string username, string password, string msg, string subject , string filepath)
        {
            MailMessage mailMessage = new MailMessage();

            SmtpClient mailClient = new SmtpClient(smtpserver, mailport);

            mailClient.Timeout = 15000;

            mailClient.Credentials = new NetworkCredential(username, password);

            mailClient.EnableSsl = true;        //mailClient.UseDefaultCredentials = true; // no work  

            mailMessage.IsBodyHtml = true;

            mailMessage.From = new MailAddress(fromEmail);

            mailMessage.To.Add(toEmail);

            foreach (var email in ccEmail)
            {
                MailAddress ccemail = new MailAddress(email);
                mailMessage.CC.Add(ccemail);
            }    

            mailMessage.Subject = subject;

            mailMessage.Body = msg;

            mailMessage.Priority = MailPriority.High;
            if (!String.IsNullOrEmpty(filepath) && filepath.Length > 0)
            {

                string[] vArray = filepath.Split(';');

                if (vArray.Length > 0)
                {

                    for (int i = 0; i < vArray.Length; i++)
                    {

                        string file = vArray[i].ToString();

                        string pathfile = System.Web.HttpContext.Current.Server.MapPath(file);

                        mailMessage.Attachments.Add(new Attachment(pathfile));
                    }
                }
            }


            

            try
            {
                mailClient.Send(mailMessage);
                return true;
            }

            catch (Exception)
            {
                return false;
            }

            finally
            {
                mailMessage.Dispose();

                mailClient.Dispose();
            }
        }       
    }
}
