﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace DF.Utilities
{
    public static class CheckSum
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string Sha1OfFile(string filePath)
        {
            using (var stream = File.OpenRead(filePath))
            {
                return Sha1OfFile(stream);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fileStream"></param>
        /// <returns></returns>
        public static string Sha1OfFile(Stream fileStream)
        {
            SHA1Managed sha = new SHA1Managed();
            byte[] checksum = sha.ComputeHash(fileStream);
            string sha1CheckSum = BitConverter.ToString(checksum).Replace("-", string.Empty);
            return sha1CheckSum;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static string Md5OfFile(Stream stream)
        {
            using (var md5 = MD5.Create())
            {
                byte[] checksum = md5.ComputeHash(stream);
                string md5CheckSum = BitConverter.ToString(checksum).Replace("-", string.Empty);
                return md5CheckSum;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string Md5OfFile(string filePath)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filePath))
                {
                    byte[] checksum = md5.ComputeHash(stream);
                    string md5CheckSum = BitConverter.ToString(checksum).Replace("-", string.Empty);
                    return md5CheckSum;
                }
            }
        }

        public static double SoTienChiaDieu(double total, int icount, bool last)
        {
            if (icount == 1) return total;
            else
            {
                if (last)
                {
                    return total - (Math.Floor(total / icount) * icount) + (Math.Floor(total / icount));
                }
                return Math.Floor(total / icount);
            }
        }
    }
}
