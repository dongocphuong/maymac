﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace DF.Utilities
{
    public static class ConvertDate
    {
        public static int getSoNgayConLai(DateTime ngay1, DateTime ngay2)
        {
            try
            {

                double t = (ngay2 - ngay1).TotalDays;
                return Convert.ToInt32(t) + 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static string getDateChuan(string date)
        {
            try
            {
                string[] date1 = date.Split('/');
                return date1[1] + "/" + date1[0] + "/" + date1[2];
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static DateTime? getDateChuanDT(string date)
        {
            try
            {
                DateTime Date = Convert.ToDateTime(getDateChuan(date) + " 00:00:00");
                return Date;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static DateTime? getDateChuanDT(string date, string hours)
        {
            try
            {
                DateTime Date = Convert.ToDateTime(getDateChuan(date) + " " + hours + ":00");
                return Date;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static DateTime? getDateChuanDT2(string date)
        {
            try
            {
                DateTime Date = Convert.ToDateTime(getDateChuan(date) + " 23:59:59");
                return Date;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static string autokey()
        {
            string str = "";
            str += DateTime.Now.ToString("ddMMyyhhmmss");
            return str;
        }


        public static DateTime getDateTimeDT(string date)
        {
            try
            {
                string[] date1 = date.Split(' ');
                string[] date2 = date1[0].Split('/');
                string idate = date2[1] + "/" + date2[0] + "/" + date2[2];
                DateTime Date = Convert.ToDateTime(idate + " " + date1[1]);
                return Date;
            }
            catch (Exception ex)
            {
               return DateTime.Now;
            }
        }
    }
}
