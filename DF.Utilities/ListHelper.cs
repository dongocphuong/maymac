﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.Utilities
{
    public static class ListHelper
    {
        /// <summary>
        /// Kiểm tra xem tất cả các phần tử của b có nằm trong a hay không?
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool ContainsAllItems<T>(List<T> a, List<T> b)
        {
            return !b.Except(a).Any();
        }
    }
}
