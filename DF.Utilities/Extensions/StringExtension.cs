﻿/*
 * Được tạo bởi hiepth6
 * Nếu bạn thấy class có vấn đề, hoặc có cách viết tốt hơn, xin liên hệ với hiepth6@viettel.com.vn để thông tin cho tác giả
 */

using System.Text;

namespace DF.Utilities.Extensions
{
    public static class StringExtension
    {
        /// <summary>
        /// Chuyển chữ có dấu (tiếng việt) sang chữ không dấu
        /// </summary>
        /// <param name="s">Chuỗi cần chuyển đổi</param>
        /// <returns></returns>
        public static string ConvertNoAccent(this string s)
        {
            string stFormD = s.Normalize(NormalizationForm.FormD);
            var sb = new StringBuilder();
            for (int ich = 0; ich < stFormD.Length; ich++)
            {
                System.Globalization.UnicodeCategory uc = System.Globalization.CharUnicodeInfo.GetUnicodeCategory(stFormD[ich]);
                if (uc != System.Globalization.UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(stFormD[ich]);
                }
            }
            sb = sb.Replace('Đ', 'D');
            sb = sb.Replace('đ', 'd');
            return (sb.ToString().Normalize(NormalizationForm.FormD));
        }
    }
}