﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace DF.Utilities
{
    public static class Utitilies
    {
        public static int BusinessDaysUntil(this DateTime firstDay, DateTime lastDay, bool lastweek, params DateTime[] bankHolidays)
        {
            //Thoi gian nghi tet
            DateTime[] bankHolidays1 = new DateTime[20];
            string namtinh = firstDay.Year.ToString();
            if (firstDay.Year < 2017)
            {
                namtinh = "2017";
            }
            bankHolidays1[0] = DateTime.ParseExact("01/01/" + namtinh, "dd/MM/yyyy", null);
            bankHolidays1[1] = DateTime.ParseExact("30/04/" + namtinh, "dd/MM/yyyy", null);
            bankHolidays1[2] = DateTime.ParseExact("01/05/" + namtinh, "dd/MM/yyyy", null);
            bankHolidays1[3] = DateTime.ParseExact("02/09/" + namtinh, "dd/MM/yyyy", null);


            //nghỉ tết âm 2017
            bankHolidays1[4] = DateTime.ParseExact("26/01/2017", "dd/MM/yyyy", null);
            bankHolidays1[5] = DateTime.ParseExact("27/01/2017", "dd/MM/yyyy", null);
            bankHolidays1[6] = DateTime.ParseExact("30/01/2017", "dd/MM/yyyy", null);
            bankHolidays1[7] = DateTime.ParseExact("31/01/2017", "dd/MM/yyyy", null);
            bankHolidays1[8] = DateTime.ParseExact("01/02/2017", "dd/MM/yyyy", null);
            bankHolidays1[9] = DateTime.ParseExact("02/02/2017", "dd/MM/yyyy", null);
            bankHolidays1[10] = DateTime.ParseExact("03/02/2017", "dd/MM/yyyy", null);
            //nghỉ giỗ tổ hùng vương 2017
            bankHolidays1[11] = DateTime.ParseExact("06/04/2017", "dd/MM/yyyy", null);
            //nghỉ 2/5/2017
            bankHolidays1[12] = DateTime.ParseExact("02/05/2017", "dd/MM/yyyy", null);
            //nghir 4/9/2017
            bankHolidays1[13] = DateTime.ParseExact("04/09/2017", "dd/MM/yyyy", null);
            firstDay = firstDay.Date;
            lastDay = lastDay.Date;
            if (firstDay > lastDay)
                throw new ArgumentException("Incorrect last day " + lastDay);

            TimeSpan span = lastDay - firstDay;
            int businessDays = span.Days + 1;
            int fullWeekCount = businessDays / 7;
            if (!lastweek)
            {
                // find out if there are weekends during the time exceedng the full weeks
                if (businessDays > fullWeekCount * 7)
                {
                    // we are here to find out if there is a 1-day or 2-days weekend
                    // in the time interval remaining after subtracting the complete weeks
                    int firstDayOfWeek = firstDay.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)firstDay.DayOfWeek;
                    int lastDayOfWeek = lastDay.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)lastDay.DayOfWeek;
                    if (lastDayOfWeek < firstDayOfWeek)
                        lastDayOfWeek += 7;
                    if (firstDayOfWeek <= 6)
                    {
                        if (lastDayOfWeek >= 7)// Both Saturday and Sunday are in the remaining time interval
                            businessDays -= 2;
                        else if (lastDayOfWeek >= 6)// Only Saturday is in the remaining time interval
                            businessDays -= 1;
                    }
                    else if (firstDayOfWeek <= 7 && lastDayOfWeek >= 7)// Only Sunday is in the remaining time interval
                        businessDays -= 1;
                }

                // subtract the weekends during the full weeks in the interval
                businessDays -= fullWeekCount + fullWeekCount;
            }
            // subtract the number of bank holidays during the time interval
            if (bankHolidays1 != null)
            {
                foreach (DateTime bankHoliday in bankHolidays1)
                {
                    DateTime bh = bankHoliday.Date;
                    if (firstDay <= bh && bh <= lastDay)
                        --businessDays;
                }
            }
            return businessDays;
        }

        public static int BusinessDaysUntilT7(this DateTime firstDay, DateTime lastDay, bool lastweek, params DateTime[] bankHolidays)
        {
            //Thoi gian nghi tet
            DateTime[] bankHolidays1 = new DateTime[20];
            string namtinh = firstDay.Year.ToString();
            if (firstDay.Year < 2017)
            {
                namtinh = "2017";
            }
            bankHolidays1[0] = DateTime.ParseExact("01/01/" + namtinh, "dd/MM/yyyy", null);
            bankHolidays1[1] = DateTime.ParseExact("30/04/" + namtinh, "dd/MM/yyyy", null);
            bankHolidays1[2] = DateTime.ParseExact("01/05/" + namtinh, "dd/MM/yyyy", null);
            bankHolidays1[3] = DateTime.ParseExact("02/09/" + namtinh, "dd/MM/yyyy", null);


            //nghỉ tết âm 2017
            bankHolidays1[4] = DateTime.ParseExact("26/01/2017", "dd/MM/yyyy", null);
            bankHolidays1[5] = DateTime.ParseExact("27/01/2017", "dd/MM/yyyy", null);
            bankHolidays1[6] = DateTime.ParseExact("30/01/2017", "dd/MM/yyyy", null);
            bankHolidays1[7] = DateTime.ParseExact("31/01/2017", "dd/MM/yyyy", null);
            bankHolidays1[8] = DateTime.ParseExact("01/02/2017", "dd/MM/yyyy", null);
            bankHolidays1[9] = DateTime.ParseExact("02/02/2017", "dd/MM/yyyy", null);
            bankHolidays1[10] = DateTime.ParseExact("03/02/2017", "dd/MM/yyyy", null);
            //nghỉ giỗ tổ hùng vương 2017
            bankHolidays1[11] = DateTime.ParseExact("06/04/2017", "dd/MM/yyyy", null);
            //nghỉ 2/5/2017
            bankHolidays1[12] = DateTime.ParseExact("02/05/2017", "dd/MM/yyyy", null);
            //nghir 4/9/2017
            bankHolidays1[13] = DateTime.ParseExact("04/09/2017", "dd/MM/yyyy", null);
            firstDay = firstDay.Date;
            lastDay = lastDay.Date;
            if (firstDay > lastDay)
                throw new ArgumentException("Incorrect last day " + lastDay);

            TimeSpan span = lastDay - firstDay;
            int businessDays = span.Days + 1;
            int fullWeekCount = businessDays / 7;
            if (!lastweek)
            {
                // find out if there are weekends during the time exceedng the full weeks
                if (businessDays > fullWeekCount * 7)
                {
                    // we are here to find out if there is a 1-day or 2-days weekend
                    // in the time interval remaining after subtracting the complete weeks
                    int firstDayOfWeek = firstDay.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)firstDay.DayOfWeek;
                    int lastDayOfWeek = lastDay.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)lastDay.DayOfWeek;
                    if (lastDayOfWeek < firstDayOfWeek)
                        lastDayOfWeek += 7;
                    if (firstDayOfWeek <= 6)
                    {
                        if (lastDayOfWeek >= 7)// Both Saturday and Sunday are in the remaining time interval
                            businessDays -= 1;
                        else if (lastDayOfWeek >= 6)// Only Saturday is in the remaining time interval
                            businessDays -= 0;
                    }
                    else if (firstDayOfWeek <= 7 && lastDayOfWeek >= 7)// Only Sunday is in the remaining time interval
                        businessDays -= 1;
                }

                // subtract the weekends during the full weeks in the interval
                businessDays -= fullWeekCount;
            }
            // subtract the number of bank holidays during the time interval
            if (bankHolidays1 != null)
            {
                foreach (DateTime bankHoliday in bankHolidays1)
                {
                    DateTime bh = bankHoliday.Date;
                    if (firstDay <= bh && bh <= lastDay)
                        --businessDays;
                }
            }
            return businessDays;
        }

        public static int BusinessDaysUntil2(this DateTime firstDay, DateTime lastDay, params DateTime[] bankHolidays)
        {
            firstDay = firstDay.Date;
            lastDay = lastDay.Date;
            if (firstDay > lastDay)
                throw new ArgumentException("Incorrect last day " + lastDay);

            TimeSpan span = lastDay - firstDay;
            int businessDays = span.Days + 1;
            int fullWeekCount = businessDays / 7;
            // find out if there are weekends during the time exceedng the full weeks
            if (businessDays > fullWeekCount * 7)
            {
                // we are here to find out if there is a 1-day or 2-days weekend
                // in the time interval remaining after subtracting the complete weeks
                int firstDayOfWeek = (int)firstDay.DayOfWeek;
                int lastDayOfWeek = (int)lastDay.DayOfWeek;
                if (lastDayOfWeek < firstDayOfWeek)
                    lastDayOfWeek += 7;
                if (firstDayOfWeek <= 6)
                {
                    if (lastDayOfWeek >= 7)// Both Saturday and Sunday are in the remaining time interval
                        businessDays -= 2;
                    else if (lastDayOfWeek >= 6)// Only Saturday is in the remaining time interval
                        businessDays -= 1;
                }
                else if (firstDayOfWeek <= 7 && lastDayOfWeek >= 7)// Only Sunday is in the remaining time interval
                    businessDays -= 1;
            }

            // subtract the weekends during the full weeks in the interval
            businessDays -= fullWeekCount + fullWeekCount;

            // subtract the number of bank holidays during the time interval
            //foreach (DateTime bankHoliday in bankHolidays)
            //{
            //    DateTime bh = bankHoliday.Date;
            //    if (firstDay <= bh && bh <= lastDay)
            //        --businessDays;
            //}

            return businessDays;
        }
        public static double TrongKhoang(double so, double dauKhoang, double cuoiKhoang)
        {
            // hàm dùng để tính xem một số có bao nhiêu phần của nó lọt vào giữa đầu và cuối khoảng
            // nếu số nhỏ hơn đầu khoảng thì hàm trả về giê rô

            if (so <= dauKhoang) return 0;
            if (so > cuoiKhoang) return cuoiKhoang - dauKhoang;
            return so - dauKhoang;
        }

        public static bool CheckTrongKhoangTGLamThu7(DateTime TG, bool ReleasDuaFat)
        {
            DateTime TuNgay = DateTime.ParseExact("10/08/2017", "dd/MM/yyyy", null);
            DateTime DenNgay = DateTime.ParseExact("23/10/2017", "dd/MM/yyyy", null);
            if (ReleasDuaFat)
            {
                if(TG.Date >= TuNgay.Date && TG.Date <= DenNgay.Date)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        public static double SoTienThue(double so)
        {
            double soReturn = 0;
            if (so <= 5000000)
            {
                soReturn = (5 * so / 100) - 0;
            }
            else if (so > 5000000 && so <= 10000000)
            {
                soReturn = (10 * so / 100) - 250000;
            }
            else if (so > 10000000 && so <= 18000000)
            {
                soReturn = (15 * so / 100) - 750000;
            }
            else if (so > 18000000 && so <= 32000000)
            {
                soReturn = (20 * so / 100) - 1650000;
            }
            else if (so > 32000000 && so <= 52000000)
            {
                soReturn = (25 * so / 100) - 3250000;
            }
            else if (so > 52000000 && so <= 80000000)
            {
                soReturn = (30 * so / 100) - 5850000;
            }
            else if (so > 80000000)
            {
                soReturn = (35 * so / 100) - 9850000;
            }
            return soReturn;
        }

        public static double SoTienThueNam(double so, int sothang)
        {
            double soReturn = 0;
            if (so <= 5000000)
            {
                soReturn = (5 * so / 100) - 0;
            }
            else if (so > 5000000  && so <= 10000000)
            {
                soReturn = (10 * so / 100) - 250000;
            }
            else if (so > 10000000  && so <= 18000000 )
            {
                soReturn = (15 * so / 100) - 750000;
            }
            else if (so > 18000000 && so <= 32000000 )
            {
                soReturn = (20 * so / 100) - 1650000;
            }
            else if (so > 32000000 && so <= 52000000 )
            {
                soReturn = (25 * so / 100) - 3250000;
            }
            else if (so > 52000000  && so <= 80000000 )
            {
                soReturn = (30 * so / 100) - 5850000;
            }
            else if (so > 80000000 )
            {
                soReturn = (35 * so / 100) - 9850000;
            }
            return soReturn;
        }

        public static string EncryptString(string Message, string Passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            // Buoc 1: Bam chuoi su dung MD5
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));
            // Step 2. Tao doi tuong TripleDESCryptoServiceProvider moi
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            // Step 3. Cai dat bo ma hoa
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            // Step 4. Convert chuoi (Message) thanh dang byte[]
            byte[] DataToEncrypt = UTF8.GetBytes(Message);
            // Step 5. Ma hoa chuoi
            try
            {
                ICryptoTransform Encryptor = TDESAlgorithm.CreateEncryptor();
                Results = Encryptor.TransformFinalBlock(DataToEncrypt, 0, DataToEncrypt.Length);
            }
            finally
            {
                // Xoa moi thong tin ve Triple DES va HashProvider de dam bao an toan
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            // Step 6. Tra ve chuoi da ma hoa bang thuat toan Base64
            return Convert.ToBase64String(Results);
        }

        public static string DecryptString(string Message, string Passphrase)
        {
            byte[] Results;
            System.Text.UTF8Encoding UTF8 = new System.Text.UTF8Encoding();
            // Step 1. Bam chuoi su dung MD5
            MD5CryptoServiceProvider HashProvider = new MD5CryptoServiceProvider();
            byte[] TDESKey = HashProvider.ComputeHash(UTF8.GetBytes(Passphrase));
            // Step 2. Tao doi tuong TripleDESCryptoServiceProvider moi
            TripleDESCryptoServiceProvider TDESAlgorithm = new TripleDESCryptoServiceProvider();
            // Step 3. Cai dat bo giai ma
            TDESAlgorithm.Key = TDESKey;
            TDESAlgorithm.Mode = CipherMode.ECB;
            TDESAlgorithm.Padding = PaddingMode.PKCS7;
            // Step 4. Convert chuoi (Message) thanh dang byte[]
            byte[] DataToDecrypt = Convert.FromBase64String(Message);
            // Step 5. Bat dau giai ma chuoi
            try
            {
                ICryptoTransform Decryptor = TDESAlgorithm.CreateDecryptor();
                Results = Decryptor.TransformFinalBlock(DataToDecrypt, 0, DataToDecrypt.Length);
            }
            finally
            {
                // Xoa moi thong tin ve Triple DES va HashProvider de dam bao an toan
                TDESAlgorithm.Clear();
                HashProvider.Clear();
            }
            return UTF8.GetString(Results);
        }
    }
}
