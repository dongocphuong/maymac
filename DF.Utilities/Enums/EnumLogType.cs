﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.Utilities.Enums
{
    /// <summary>
    /// Enum Log type
    /// </summary>
    /// Description: Biểu diễn các log
    public enum enLogType
    {
        [StringValue("Log thông thường")]
        NomalLog = 0,
        [StringValue("Log theo người dùng")]
        UserLog = 1,
        [StringValue("Log hệ thống")]
        SystemLog = 2,
        [StringValue("Log tấn công")]
        AttackLog = 3,
    }

    /// <summary>
    /// Các hành động của Log.
    /// </summary>
    /// Author: Hungcd1
    /// Description: Định nghĩa các hành động của nhật ký.
    /// Change: 09:05 01-05-2014
    public enum enActionType
    {
        [StringValue("Xem thông tin")]
        View = 0,

        [StringValue("Cập nhật thông tin")]
        Update = 1,

        [StringValue("Xóa thông tin")]
        Delete = 2,

        [StringValue("Xuất dữ liệu ra file")]
        ExportExcel = 3,

        [StringValue("Nhập dữ liệu từ file")]
        ImporttExcel = 4,

        [StringValue("Thêm mới")]
        Insert = 5,    
		
        [StringValue("Gửi duyệt")]
        SendApproved = 6,

        [StringValue("Phê duyệt")]
        Approve = 7,

        [StringValue("Hủy duyệt")]
        ApproveCancel = 8,

        [StringValue("Hành động khác")]
        Other = 100,

        [StringValue("Thay đổi trạng thái")]
        ActiveChange = 9,

        [StringValue("Đồng bộ dữ liệu")]
        Sync = 10,
        [StringValue("Đăng ký khám")]
        Dangkykham = 11,

        [StringValue("Gửi tin nhắn SMS")]
        SendSMS = 13,
        
    }

    /// <summary>
    /// Các hành động của Log.
    /// </summary>
    /// Author: Hungcd1
    /// Description: Định nghĩa các hành động của nhật ký.
    /// Change: 09:05 01-05-2014
    public enum enPhuongThucThanhToanType
    {
        [StringValue("Thanh toán trực tuyến")]
        PayOnline = 1,

        [StringValue("Thanh toán khi đến khám")]
        PayOffline = 2,
    }
}
