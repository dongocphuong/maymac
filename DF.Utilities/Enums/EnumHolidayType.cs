﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.Utilities.Enums
{
    /// <summary>
    /// Enum holiday of type
    /// </summary>
    /// Description: Biểu diễn các ngày nghỉ lễ
    public enum enHolidayType
    {
        [StringValue("Tết nguyên đán")]
        Tetnguyendan = 1,

        [StringValue("Tết dương lịch")]
        Tetduonglich = 2,

        [StringValue("Giỗ Tổ Hùng Vương")]
        Nghigioto = 3,

        [StringValue("Nghỉ lễ 30/4 và Quốc tế Lao Động 1/5")]
        Nghi30_4Va1_5 = 4,

        [StringValue("Nghỉ lễ Quốc Khánh 2/9")]
        Nghiquockhanh = 5,
    }
}
