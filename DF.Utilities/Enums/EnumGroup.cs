﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.Utilities.Enums
{
   public enum Sex
    {
       [StringValue("Nam")]
       Nam = 1,
       [StringValue("Nữ")]
       Nu = 0,
    }

   public enum DataType
   {
       [StringValue(@"Single line of text")]
       Type1 = 1,
       [StringValue(@"Multiple lines of text")]
       Type2 = 2,
       [StringValue(@"Number (1, 1.0, 100)")]
       Type3 = 3,
       [StringValue(@"Date and Time")]
       Type4 = 4,
       [StringValue(@"Lookup")]
       Type5 = 5,
       [StringValue(@"Yes/No (check box)")]
       Type6 = 6,
   }

    public enum Step
    {
        [StringValue("Chờ active")]
        Active = 1,
        [StringValue("Chờ thay đổi mật khâu")]
        ChangePass =2,
        [StringValue("Chờ đăng ký CA")]
        RegistCa=3,
        [StringValue("Đã có thể sử dụng")]
        Allow =0,
    }
    public enum DutyType
    {
        [StringValue("Lịch lãnh đạo")]
        Leader = 1,
        [StringValue("Thường trú Ban giám đốc")]
        Directors = 2,
        [StringValue("Lịch Phòng ban/Khoa/Viện/Trung tâm")]
        Deparment = 3,
        [StringValue("Lịch toàn bệnh viên")]
        Hospital = 4,
    }
    public enum LevelDeparment
    {
        [StringValue("Cấp 1")]
        Level1 = 1,
        [StringValue("Cấp 2")]
        Level2 = 2,
        [StringValue("Cấp 3")]
        Level3 = 3,      
    }
}
