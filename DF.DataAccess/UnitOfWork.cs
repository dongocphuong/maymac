﻿/*
 * Được tạo bởi hiepth6
 * Nếu bạn thấy class có vấn đề, hoặc có cách viết tốt hơn, xin liên hệ với hiepth6@viettel.com.vn để thông tin cho tác giả
 */

using System;
using DF.DBMapping.Models;
using DF.DataAccess.Repository;

namespace DF.DataAccess
{
    /// <summary>
    /// Quản lý các repository và context kết nối đến csdl
    /// Khởi tạo transaction
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Context kết nối đến CSDL
        /// Khi áp dụng framework này, BẠN PHẢI THAY fwContext BẰNG CONTEXT CỦA BẠN
        /// </summary>
        private MAYMACNEWContext _context;

        public MAYMACNEWContext Context
        {
            get { return this._context; }
            set { this._context = value; }
        }

        public UnitOfWork()
        {
            InitializeContext();
        }

        /// <summary>
        /// Khởi tạo kết nối
        /// </summary>
        protected void InitializeContext()
        {
            _context = new MAYMACNEWContext();
            _context.Configuration.LazyLoadingEnabled = true;
        }

        #region REPOSITORY
        public IUserRepository Users { get { return new UserRepository(_context); } }
        public INhanVienRepository NhanViens { get { return new NhanVienRepository(_context); } }
        public IGroupUserRepository GroupUsers { get { return new GroupUserRepository(_context); } }
        public IPhanQuyenRepository tblGroupUsers { get { return new PhanQuyenRepository(_context); } }
        public IChucVuRepository ChucVus { get { return new ChucVuRepository(_context); } }
        public IGroupRepository Groups { get { return new GroupRepository(_context); } }
        public IDonViRepository DonVis { get { return new DonViRepository(_context); } }
        public ILanguageRepository tblLanguages { get { return new LanguageRepository(_context); } }
        public IKhoNguyenLieuRepository KhoNguyenLieus { get { return new KhoNguyenLieuRepository(_context); } }
        public IDanhMucNguyenLieuRepository NguyenLieus { get { return new DanhMucNguyenLieuRepository(_context); } }
        public IDanhMucSanPhamRepository SanPhams { get { return new DanhMucSanPhamRepository(_context); } }
        public IDanhMucThanhPhamRepository ThanhPhams { get { return new DanhMucThanhPhamRepository(_context); } }
        public INhanVienPhongBanRepository NhanVienPhongBans { get{ return new NhanVienPhongBanRepository(_context); }  }
        public IPhongBanRepository PhongBans { get { return new PhongBanRepository(_context); } }
        public IKhoThanhPhamRepository KhoThanhPhams { get { return new KhoThanhPhamRepository(_context); } }
        public IKhoSanPhamRepository KhoSanPhams { get { return new KhoSanPhamRepository(_context); } }
        public IQuanLyDonHangRepository DonHangs { get { return new QuanLyDonHangRepository(_context); } }
        public IQuanLySanXuatRepository QuanLySanXuats { get { return new QuanLySanXuatRepository(_context); } }
        public IQuanLySanXuatNhomRepository QuanLySanXuatNhoms { get { return new QuanLySanXuatNhomRepository(_context); } }
        public IDeNghiNguyenLieuRepository DeNghiNguyenLieus { get { return new DeNghiNguyenLieuRepository(_context); } }
        public IQuanLyMauMayRepository MauMays { get { return new QuanLyMauMayRepository(_context); } }
        public IQuanLyChiTietMauMayRepository ChiTietMauMays { get { return new QuanLyChiTietMauMayRepository(_context); } }
        public IDanhMucThuocTinhSanPhamRepository ThuocTinhSanPhams { get { return new DanhMucThuocTinhSanPhamRepository(_context); } }
        public IYeuCauKhachHangRepository YeuCauKhachHangs { get { return new YeuCauKhachHangRepository(_context); } }
        public IQuanLyVanBanDinhKemRepository VanBanDinhKems { get { return new QuanLyVanBanDinhKemRepository(_context); } }
        #endregion

        #region TRANSACTION
        /// <summary>
        /// Lấy thông tin về transaction hiện tai
        /// </summary>
        /// <returns></returns>
        public ITransaction BeginTransaction()
        {
            return new Transaction(this);
        }

        /// <summary>
        /// Kết thúc trangsactions
        /// </summary>
        /// <param name="transaction"></param>
        public void EndTransaction(ITransaction transaction)
        {
            if (transaction != null)
            {
                (transaction as IDisposable).Dispose();
                transaction = null;
            }
        }
        #endregion TRANSACTION

        #region SAVE AND DISPOSE DATA

        /// <summary>
        /// Lưu dữ liệu xuống server
        /// </summary>
        public void Save()
        {
            _context.SaveChanges();
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        /// <summary>
        /// Lưu vào giải phóng kết nối
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion SAVE AND DISPOSE DATA
    }
}
