﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;


namespace DF.DataAccess.Repository
{
    public interface IDonViRepository : IRepository<DonVi>
    {
        List<DonVi> getdata();
        int createdata(string data);
        int updatedata(string data);
        int deletedata(string data);
    }

    public class DonViRepository : EFRepository<DonVi>, IDonViRepository
    {
        protected UnitOfWork unitofwork;
        public DonViRepository(DbContext dbContext)
            : base(dbContext)
        {

        }
        public List<DonVi> getdata()
        {
            unitofwork = new UnitOfWork();
            try
            {
                List<DonVi> list = unitofwork.Context.DonVis.Where(m => m.IsActive == true).OrderBy(m => m.TenDonVi).ToList();
                return list;
            }
            catch(Exception e)
            {
                List<DonVi> list = unitofwork.Context.DonVis.Where(m => m.IsActive == true).OrderBy(m => m.TenDonVi).ToList();
                return list;
            }
            
        }
        //create by changngoktb - 11/01/2017 - thêm đơn vị tính
        public int createdata(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var DonVi = new DonVi();
                bool kt = unitofwork.Context.DonVis.Where(m => m.IsActive == true && m.TenDonVi.ToUpper() == data.ToUpper()).Count() > 0 ? true : false;
                if (kt == true)
                {
                    // đã tồn tại
                    return 1;
                }
                else
                {
                    // thêm
                    DonVi.IsActive = true;
                    DonVi.DonViId = Guid.NewGuid();
                    DonVi.TenDonVi = data;
                    unitofwork.DonVis.Add(DonVi);
                    unitofwork.Save();
                    return 2;
                }
            }
            catch
            {
                return 0;
            }
        }
        //create by changngoktb - 11/01/2017 - sửa đơn vị tính
        public int updatedata(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                DonVi banghi = new JavaScriptSerializer().Deserialize<DonVi>(data);
                bool kt = unitofwork.Context.DonVis.Where(m => m.IsActive == true && m.TenDonVi.ToUpper() == banghi.TenDonVi.ToUpper() && m.DonViId != banghi.DonViId).Count() > 0 ? true : false;
                if (kt == true)
                {
                    //Đã tồn tại
                    return 1;
                }
                else
                {
                    //update
                    unitofwork.DonVis.Update(banghi);
                    return 2;
                }
            }
            catch
            {
                return 0;
            }
        }
        //create by changngoktb - 11/01/2017 - xóa đơn vị tính
        public int deletedata(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                DonVi banghi = new JavaScriptSerializer().Deserialize<DonVi>(data);
                //bool kt = unitofwork.Context.VatTus.Where(m => m.IsActive == true && m.DonViId == banghi.DonViId).Count() > 0 ? true : false;
                //if (kt == true)
                //{
                //    //Bảng vật tư đang sử dụng đơn vị tính này!
                //    return 1;
                //}
                //else
                //{
                    //update
                    unitofwork.Context.DonVis.Find(banghi.DonViId).IsActive = false;
                    unitofwork.Save();
                    return 2;
                //}
            }
            catch
            {
                return 0;
            }
        }
    }
}
