﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data;
using System.Web.Script.Serialization;
using DF.DataAccess.DBML;

namespace DF.DataAccess.Repository
{
    public interface IDanhMucThuocTinhSanPhamRepository : IRepository<ThuocTinhSanPham>
    {
        List<Web_DanhMucThuocTinhSanPham_GetAllDataResult> GetAllDataDMThuocTinhSanPham();
        int AddOrUpdateDMThuocTinhSanPham(string data);
        int DeleteDMThuocTinhSanPham(string data);
    }
    public class DanhMucThuocTinhSanPhamRepository : EFRepository<ThuocTinhSanPham>, IDanhMucThuocTinhSanPhamRepository
    {
        protected UnitOfWork unitofwork;
        public DanhMucThuocTinhSanPhamRepository(DbContext dbContext) : base(dbContext) { }
        protected DBMLDFDataContext db;
        public List<Web_DanhMucThuocTinhSanPham_GetAllDataResult> GetAllDataDMThuocTinhSanPham()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            List<Web_DanhMucThuocTinhSanPham_GetAllDataResult> list = db.Web_DanhMucThuocTinhSanPham_GetAllData().OrderBy(t => t.TenThuocTinh).ToList();
            return list;
        }
        public int AddOrUpdateDMThuocTinhSanPham(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var banghi = new JavaScriptSerializer().Deserialize<ThuocTinhSanPham>(data);
                //bool kt = (unitofwork.Context.ThuocTinhSanPhams.Where(m => m.ThuocTinhSanPhamID != banghi.ThuocTinhSanPhamID &&
                //                                                (m.TenThuocTinh.ToUpper() == banghi.TenThuocTinh.ToUpper() ||
                //                                                    m.TenThuocTinh.ToUpper() == banghi.TenThuocTinh.ToUpper())).Count() > 0 ? true : false);
                //if (kt == true)
                //{
                //    // đã tồn tại
                //    return 1;
                //}
                if (banghi.ThuocTinhSanPhamID == null || banghi.ThuocTinhSanPhamID == Guid.Empty)
                {
                    // thêm
                    banghi.LoaiHinh = 1;
                    banghi.ThuocTinhSanPhamID = Guid.NewGuid();
                    unitofwork.ThuocTinhSanPhams.Add(banghi);
                    unitofwork.Save();
                    return 2;
                }
                else // sửa
                {
                    var vt = unitofwork.Context.ThuocTinhSanPhams.Find(banghi.ThuocTinhSanPhamID);
                    vt.TenThuocTinh = banghi.TenThuocTinh;
                    vt.KieuDuLieu = banghi.KieuDuLieu;
                    vt.LoaiHinhSanXuat = banghi.LoaiHinhSanXuat;
                    unitofwork.Context.SaveChanges();
                    return 2;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int DeleteDMThuocTinhSanPham(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                db = new DBMLDFDataContext();
                db.Web_DanhMucThuocTinhSanPham_DeleteThuocTinhSanPham(Guid.Parse(data));
                unitofwork.Context.SaveChanges();
                return 2;
            }
            catch
            {
                return 0;
            }
        }
    }
}