﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;

namespace DF.DataAccess.Repository
{
    public interface IDanhMucNguyenLieuRepository : IRepository<NguyenLieu>
    {
        List<Web_DanhMucNguyenLieu_GetAllDataResult> GetAllDataDMNguyenLieu();
        int AddOrUpdateDMNguyenLieu(string data, string manguyenlieu);
        int DeleteDMNguyenLieu(string data);
    }
    public class DanhMucNguyenLieuRepository : EFRepository<NguyenLieu>, IDanhMucNguyenLieuRepository
    {
        protected UnitOfWork unitofwork;
        public DanhMucNguyenLieuRepository(DbContext dbContext): base(dbContext){}
        protected DBMLDFDataContext db;
        public List<Web_DanhMucNguyenLieu_GetAllDataResult> GetAllDataDMNguyenLieu()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            List<Web_DanhMucNguyenLieu_GetAllDataResult> list = db.Web_DanhMucNguyenLieu_GetAllData().OrderBy(t => t.TenNguyenLieu).ToList();
            return list;
        }
        public int AddOrUpdateDMNguyenLieu(string data, string manguyenlieu)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var banghi = new JavaScriptSerializer().Deserialize<NguyenLieu>(data);
                if (banghi.NguyenLieuID == null || banghi.NguyenLieuID == Guid.Empty)
                {
                    // thêm
                    banghi.MaNguyenLieu = manguyenlieu;
                    banghi.TenNguyenLieu = banghi.TenNguyenLieu + " " + banghi.MaMau;
                    banghi.IsActive = true;
                    banghi.NguyenLieuID = Guid.NewGuid();
                    if(banghi.HeSoQuyDoi == null || banghi.HeSoQuyDoi.ToString() == "")
                    {
                        banghi.HeSoQuyDoi = 0;
                    }
                    unitofwork.NguyenLieus.Add(banghi);
                    unitofwork.Save();
                    return 2;
                }
                else // sửa
                {
                    var vt = unitofwork.Context.NguyenLieux.Find(banghi.NguyenLieuID);
                    vt.MaNguyenLieu = manguyenlieu;
                    vt.TenNguyenLieu = banghi.TenNguyenLieu + " " + banghi.MaMau;
                    vt.MaMau = banghi.MaMau;
                    vt.NhomNguyenLieuID = banghi.NhomNguyenLieuID;
                    vt.DonViID = banghi.DonViID;
                    vt.DanhSachHinhAnhs = banghi.DanhSachHinhAnhs;
                    vt.AnhDaiDien = banghi.AnhDaiDien;
                    vt.Type = banghi.Type;
                    if (banghi.HeSoQuyDoi == null || banghi.HeSoQuyDoi.ToString() == "")
                    {
                        vt.HeSoQuyDoi = 0;
                    }
                    else
                    {
                        vt.HeSoQuyDoi = banghi.HeSoQuyDoi;
                    }
                    unitofwork.Context.SaveChanges();
                    return 2;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int DeleteDMNguyenLieu(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var banghi = new NguyenLieu();
                unitofwork.Context.NguyenLieux.Find(Guid.Parse(data)).IsActive = false;
                unitofwork.Context.SaveChanges();
                return 2;
            }
            catch
            {
                return 0;
            }
        }
    }
}