﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu;

namespace DF.DataAccess.Repository
{
    public interface IDanhMucThanhPhamRepository : IRepository<ThanhPham>
    {
        List<Web_DanhMucThanhPham_GetAllDataResult> GetAllDataDMThanhPham();
        Web_DanhMucThanhPham_GetAllDataThanhPhamIDResult AddOrUpdateDMThanhPham(string data, string listdinhnghia, string SanPhamID = "");
        Web_DanhMucThanhPham_GetAllDataThanhPhamIDResult AddOrUpdateDMThanhPhamToanBoMau(string data, string listdinhnghia, string SanPhamID = "");
        int DeleteDMThanhPham(string data);
    }
    public class DanhMucThanhPhamRepository:EFRepository<ThanhPham>, IDanhMucThanhPhamRepository
    {
        protected UnitOfWork unitofwork;
        public DanhMucThanhPhamRepository(DbContext dbContext): base(dbContext){ }
        protected DBMLDFDataContext db;
        public List<Web_DanhMucThanhPham_GetAllDataResult> GetAllDataDMThanhPham()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            List<Web_DanhMucThanhPham_GetAllDataResult> list = db.Web_DanhMucThanhPham_GetAllData().OrderBy(t => t.TenThanhPham).ToList();
            return list;
        }
        public Web_DanhMucThanhPham_GetAllDataThanhPhamIDResult AddOrUpdateDMThanhPham(string data, string listdinhnghia, string SanPhamID = "")
        {
            try
            {
                unitofwork = new UnitOfWork();
                var banghi = new JavaScriptSerializer().Deserialize<ThanhPham>(data);
               
                //bool kt = (unitofwork.Context.ThanhPhams.Where(m => m.ThanhPhamID != banghi.ThanhPhamID && m.IsActive == true && (m.MaThanhPham.ToUpper() == banghi.MaThanhPham.ToUpper() || m.MaThanhPham.ToUpper() == banghi.MaThanhPham.ToUpper())).Count() > 0 ? true : false);
                //if (kt == true)
                //{
                //    // đã tồn tại
                //    return 1;
                //}
                if (banghi.ThanhPhamID == null || banghi.ThanhPhamID == Guid.Empty)
                {
                    // thêm
                    banghi.IsActive = true;
                    banghi.ThanhPhamID = Guid.NewGuid();
                    if (SanPhamID != "")
                    {
                        banghi.SanPhamID = Guid.Parse(SanPhamID);
                    }
                    unitofwork.ThanhPhams.Add(banghi);
                    var lstdinhnghia = new JavaScriptSerializer().Deserialize<List<DanhSachNguyenLieuDinhNghiaModel>>(listdinhnghia);
                    foreach (var item in lstdinhnghia)
                    {
                        var dntp = new DinhNghiaThanhPham()
                        {
                            DinhNghiaThanhPhamID = Guid.NewGuid(),
                            ID = item.NguyenLieuID,
                            IsActive = true,
                            LoaiHinh = item.LoaiHinh,
                            SoLuong = item.SoLuong.GetValueOrDefault(),
                            ThanhPhamID = banghi.ThanhPhamID,
                            LoaiVai = item.LoaiVai
                        };
                        unitofwork.Context.DinhNghiaThanhPhams.Add(dntp);
                    }


                    unitofwork.Save();
                    unitofwork.Dispose();
                    db = new DBMLDFDataContext();
                    return db.Web_DanhMucThanhPham_GetAllDataThanhPhamID(banghi.ThanhPhamID).FirstOrDefault();
                }
                else // sửa
                {
                    var vt = unitofwork.Context.ThanhPhams.Find(banghi.ThanhPhamID);
                    vt.MaThanhPham = banghi.MaThanhPham;
                    vt.TenThanhPham = banghi.TenThanhPham;
                    vt.DonViID = banghi.DonViID;
                    vt.DanhSachHinhAnhs = banghi.DanhSachHinhAnhs;
                    vt.AnhDaiDien = banghi.AnhDaiDien;
                    vt.DonGia = banghi.DonGia;
                    //if (SanPhamID != "")
                    //{
                    //    vt.SanPhamID = Guid.Parse(SanPhamID);
                    //}
                    //
                    var dntp = unitofwork.Context.DinhNghiaThanhPhams.Where(p => p.ThanhPhamID == vt.ThanhPhamID);
                    unitofwork.Context.DinhNghiaThanhPhams.RemoveRange(dntp);
                    var lstdinhnghia = new JavaScriptSerializer().Deserialize<List<DanhSachNguyenLieuDinhNghiaModel>>(listdinhnghia);
                    foreach (var item in lstdinhnghia)
                    {
                        var dntp2 = new DinhNghiaThanhPham()
                        {
                            DinhNghiaThanhPhamID = Guid.NewGuid(),
                            ID = item.NguyenLieuID,
                            IsActive = true,
                            LoaiHinh = item.LoaiHinh,
                            SoLuong = item.SoLuong.GetValueOrDefault(),
                            ThanhPhamID = vt.ThanhPhamID,
                            LoaiVai = item.LoaiVai
                        };
                        unitofwork.Context.DinhNghiaThanhPhams.Add(dntp2);
                    }

                    unitofwork.Context.SaveChanges();
                    unitofwork.Dispose();
                    db = new DBMLDFDataContext();
                    return db.Web_DanhMucThanhPham_GetAllDataThanhPhamID(banghi.ThanhPhamID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Web_DanhMucThanhPham_GetAllDataThanhPhamIDResult AddOrUpdateDMThanhPhamToanBoMau(string data, string listdinhnghia, string SanPhamID = "")
        {
            try
            {
                unitofwork = new UnitOfWork();
                var banghi = new JavaScriptSerializer().Deserialize<ThanhPham>(data);
                if (banghi.ThanhPhamID == null || banghi.ThanhPhamID == Guid.Empty)
                {
                    // thêm
                    banghi.IsActive = true;
                    banghi.ThanhPhamID = Guid.NewGuid();
                    if (SanPhamID != "")
                    {
                        banghi.SanPhamID = Guid.Parse(SanPhamID);
                    }
                    unitofwork.ThanhPhams.Add(banghi);
                    var lstdinhnghia = new JavaScriptSerializer().Deserialize<List<DanhSachNguyenLieuDinhNghiaModel>>(listdinhnghia);
                    foreach (var item in lstdinhnghia)
                    {
                        var dntp = new DinhNghiaThanhPham()
                        {
                            DinhNghiaThanhPhamID = Guid.NewGuid(),
                            ID = item.NguyenLieuID,
                            IsActive = true,
                            LoaiHinh = item.LoaiHinh,
                            SoLuong = item.SoLuong.GetValueOrDefault(),
                            ThanhPhamID = banghi.ThanhPhamID,
                            LoaiVai = item.LoaiVai
                        };
                        unitofwork.Context.DinhNghiaThanhPhams.Add(dntp);
                    }
                    if (SanPhamID != "")
                    {
                        var spid = Guid.Parse(SanPhamID);
                        var sp = unitofwork.Context.SanPhams.FirstOrDefault(p => p.SanPhamID == spid);
                        var dntp = unitofwork.Context.DinhNghiaThanhPhams.ToList();
                        var lstsp = unitofwork.Context.SanPhams.Where(p => p.MauMayID == sp.MauMayID && sp.Mau == p.Mau).ToList();
                        var cdsp = (from a in unitofwork.Context.DMCongDoanHangMucs
                                    join b in unitofwork.Context.DMCongDoanThanhPhams on a.DMCongDoanHangMucID equals b.DMCongDoanHangMucID
                                    join c in unitofwork.Context.DMHangMucSanXuats on a.DMHangMucSanXuatID equals c.DMHangMucSanXuatID
                                    where c.ViTri == 0
                                    select new { b.ThanhPhamID, c.SanPhamID }).ToList().Where(p => lstsp.Any(p3 => p3.SanPhamID == p.SanPhamID)).Select(p => p.ThanhPhamID).Distinct().ToList();
                        foreach (var tp in cdsp)
                        {
                            var lstdntp = dntp.Where(p => p.ThanhPhamID == tp);
                            unitofwork.Context.DinhNghiaThanhPhams.RemoveRange(lstdntp);
                            foreach (var item in lstdinhnghia)
                            {
                                var dntp2 = new DinhNghiaThanhPham()
                                {
                                    DinhNghiaThanhPhamID = Guid.NewGuid(),
                                    ID = item.NguyenLieuID,
                                    IsActive = true,
                                    LoaiHinh = item.LoaiHinh,
                                    SoLuong = item.SoLuong.GetValueOrDefault(),
                                    ThanhPhamID = tp,
                                    LoaiVai = item.LoaiVai
                                };
                                unitofwork.Context.DinhNghiaThanhPhams.Add(dntp2);
                            }
                        }
                        
                    }

                    unitofwork.Save();
                    unitofwork.Dispose();

                    

                    db = new DBMLDFDataContext();
                    return db.Web_DanhMucThanhPham_GetAllDataThanhPhamID(banghi.ThanhPhamID).FirstOrDefault();
                }
                else // sửa
                {
                    var vt = unitofwork.Context.ThanhPhams.Find(banghi.ThanhPhamID);
                    vt.MaThanhPham = banghi.MaThanhPham;
                    vt.TenThanhPham = banghi.TenThanhPham;
                    vt.DonViID = banghi.DonViID;
                    vt.DanhSachHinhAnhs = banghi.DanhSachHinhAnhs;
                    vt.AnhDaiDien = banghi.AnhDaiDien;
                    vt.DonGia = banghi.DonGia;
                    //if (SanPhamID != "")
                    //{
                    //    vt.SanPhamID = Guid.Parse(SanPhamID);
                    //}
                    //
                    var dntp = unitofwork.Context.DinhNghiaThanhPhams.Where(p => p.ThanhPhamID == vt.ThanhPhamID).ToList(); ;
                    unitofwork.Context.DinhNghiaThanhPhams.RemoveRange(dntp);
                    var lstdinhnghia = new JavaScriptSerializer().Deserialize<List<DanhSachNguyenLieuDinhNghiaModel>>(listdinhnghia);
                    foreach (var item in lstdinhnghia)
                    {
                        var dntp2 = new DinhNghiaThanhPham()
                        {
                            DinhNghiaThanhPhamID = Guid.NewGuid(),
                            ID = item.NguyenLieuID,
                            IsActive = true,
                            LoaiHinh = item.LoaiHinh,
                            SoLuong = item.SoLuong.GetValueOrDefault(),
                            ThanhPhamID = vt.ThanhPhamID,
                            LoaiVai = item.LoaiVai
                        };
                        unitofwork.Context.DinhNghiaThanhPhams.Add(dntp2);
                    }
                    if (vt.SanPhamID != null)
                    {
                        var spid = vt.SanPhamID;
                        var sp = unitofwork.Context.SanPhams.FirstOrDefault(p => p.SanPhamID == spid);
                        var dntps = unitofwork.Context.DinhNghiaThanhPhams.ToList();
                        var lstsp = unitofwork.Context.SanPhams.Where(p => p.MauMayID == sp.MauMayID && sp.Mau == p.Mau && p.SanPhamID!=sp.SanPhamID).ToList();
                        var cdsp = (from a in unitofwork.Context.DMCongDoanHangMucs
                                    join b in unitofwork.Context.DMCongDoanThanhPhams on a.DMCongDoanHangMucID equals b.DMCongDoanHangMucID
                                    join c in unitofwork.Context.DMHangMucSanXuats on a.DMHangMucSanXuatID equals c.DMHangMucSanXuatID
                                    where c.ViTri == 0
                                    select new {b.ThanhPhamID,c.SanPhamID }).ToList().Where(p => lstsp.Any(p3 => p3.SanPhamID == p.SanPhamID)).Select(p=>p.ThanhPhamID).Distinct().ToList();
                        foreach (var tp in cdsp)
                        {
                            var lstdntp = dntps.Where(p => p.ThanhPhamID == tp);
                            unitofwork.Context.DinhNghiaThanhPhams.RemoveRange(lstdntp);
                            foreach (var item in lstdinhnghia)
                            {
                                var dntp2 = new DinhNghiaThanhPham()
                                {
                                    DinhNghiaThanhPhamID = Guid.NewGuid(),
                                    ID = item.NguyenLieuID,
                                    IsActive = true,
                                    LoaiHinh = item.LoaiHinh,
                                    SoLuong = item.SoLuong.GetValueOrDefault(),
                                    ThanhPhamID = tp,
                                    LoaiVai = item.LoaiVai
                                };
                                unitofwork.Context.DinhNghiaThanhPhams.Add(dntp2);
                            }
                        }

                    }
                    unitofwork.Context.SaveChanges();
                    unitofwork.Dispose();
                    db = new DBMLDFDataContext();
                    return db.Web_DanhMucThanhPham_GetAllDataThanhPhamID(banghi.ThanhPhamID).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public int DeleteDMThanhPham(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var banghi = new ThanhPham();
                unitofwork.Context.ThanhPhams.Find(Guid.Parse(data)).IsActive = false;
                unitofwork.Context.SaveChanges();
                return 2;
            }
            catch
            {
                return 0;
            }
        }
    }
}
