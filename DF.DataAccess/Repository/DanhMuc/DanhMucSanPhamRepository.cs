﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu;


namespace DF.DataAccess.Repository
{
    public interface IDanhMucSanPhamRepository : IRepository<SanPham>
    {
        List<Web_DanhMucSanPham_GetAllDataResult> GetAllDataDMSanPham();
        Guid AddOrUpdateDMSanPham(string data, string tts);
        Guid CapNhatCongDoan(Guid SanPhamID, string lstcongdoan, string congdoancuoi);
        int DeleteDMSanPham(string data);
        SanXuatModel SanXuatSanPhamChuaBatDau(string SanPhamID, string DonHangID);
        SanXuatModel SanXuatSanPhamDangSanXuat(string SanPhamID, string DonHangID);
        bool BatDauSanXuat(string SanPhamID, string DonHangID);
        bool SanXuatThanhPham(string DonHangID, string SanPhamID, string HangMucSanXuatID, string datanhap, string dataxuat, Guid NhanVienID);
        bool SanXuatThanhPhamCongDoan(string DonHangID, string SanPhamID, int SoLuong, string dataxuat, Guid NhanVienID);
        int TaoCongDoanTuDong(Guid YeuCauKhachHangID, string data);
        bool CopySanPham(Guid SanPhamID, string Sizes);
        int LuuDanhSachNguyenLieuChoYeuCau(string YeuCauKhachHangID, string SanPhamID, string data, string size);
    }
    public class DanhMucSanPhamRepository : EFRepository<SanPham>, IDanhMucSanPhamRepository
    {
        protected UnitOfWork unitofwork;
        public DanhMucSanPhamRepository(DbContext dbContext) : base(dbContext) { }
        protected DBMLDFDataContext db;
        public List<Web_DanhMucSanPham_GetAllDataResult> GetAllDataDMSanPham()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            List<Web_DanhMucSanPham_GetAllDataResult> list = db.Web_DanhMucSanPham_GetAllData().OrderBy(t => t.TenSanPham).ToList();
            return list;
        }
        public bool CopySanPham(Guid SanPhamID, string Sizes)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            try
            {
                var lstsize = new JavaScriptSerializer().Deserialize<List<SizeModel>>(Sizes);
                var lstdmsx = unitofwork.Context.DMHangMucSanXuats.ToList();
                var lstcongdoan = unitofwork.Context.DMCongDoanHangMucs.ToList();
                var lstcdthanhpham = unitofwork.Context.DMCongDoanThanhPhams.ToList();
                var lstthanhpham = unitofwork.Context.ThanhPhams.ToList();
                var lstdntp = unitofwork.Context.DinhNghiaThanhPhams.ToList();
                var lstdnsp = unitofwork.Context.DinhNghiaSanPhams.ToList();
                foreach (var item in lstsize)
                {//thêm sản phẩm
                    //set màu của vải chính là màu của sản phẩm
                    var mau = db.Web_DonHang_LayDanhSachThuocTinhCuaSanPham(SanPhamID).FirstOrDefault(p => p.LoaiVai == 1).MaMau;
                    var sp = unitofwork.Context.SanPhams.FirstOrDefault(p => p.SanPhamID == SanPhamID);
                    //
                    SanPham newsp = new SanPham();
                    newsp.SanPhamID = Guid.NewGuid();
                    newsp.TenSanPham = sp.TenSanPham;
                    newsp.MaSanPham = CreateMaSanPham(sp.Loai.GetValueOrDefault(), sp.GioiTinh.GetValueOrDefault(), item.Size);
                    newsp.DanhSachHinhAnhs = sp.DanhSachHinhAnhs;
                    newsp.DonViID = sp.DonViID;
                    newsp.AnhDaiDien = sp.AnhDaiDien;
                    newsp.DonGia = sp.DonGia;
                    newsp.IsActive = true;
                    newsp.Size = item.Size;
                    newsp.SizeMauGoc = sp.SizeMauGoc;
                    newsp.TiLeSize = sp.TiLeSize;
                    newsp.MauMayID = sp.MauMayID;
                    newsp.Mau = mau;
                    newsp.Loai = sp.Loai;
                    newsp.GioiTinh = sp.GioiTinh;
                    newsp.ThoiGian = sp.ThoiGian;
                    newsp.LoaiHinhSanXuat = sp.LoaiHinhSanXuat;
                    unitofwork.Context.SanPhams.Add(newsp);
                    //copy thuộc tính
                    var lst = unitofwork.Context.GiaTriThuocTinhSanPhams.Where(p => p.SanPhamID == SanPhamID).ToList();
                    foreach (var tt in lst)
                    {
                        var tt2 = new GiaTriThuocTinhSanPham()
                        {
                            GiaTriThuocTinhID = Guid.NewGuid(),
                            SanPhamID = newsp.SanPhamID,
                            GiaTri = tt.GiaTri,
                            ThuocTinhSanPhamID = tt.ThuocTinhSanPhamID
                        };
                        unitofwork.Context.GiaTriThuocTinhSanPhams.Add(tt2);
                    }
                    //copy công đoạn
                    var tpcuoi = Guid.Empty;
                    var lsthamgmucsanxuats = lstdmsx.Where(p => p.SanPhamID == SanPhamID).OrderBy(p => p.ViTri).ToList();
                    foreach (var hmsx in lsthamgmucsanxuats)
                    {
                        var hmsx2 = new DMHangMucSanXuat
                        {
                            DMHangMucSanXuatID = Guid.NewGuid(),
                            SanPhamID = newsp.SanPhamID,
                            GiaTriDinhMuc = hmsx.GiaTriDinhMuc,
                            HaoHut = 0,
                            IsActive = hmsx.IsActive,
                            MoTa = hmsx.MoTa,
                            ThoiGianHoanThanh = hmsx.ThoiGianHoanThanh,
                            ViTri = hmsx.ViTri,
                            DsHinhAnh = hmsx.DsHinhAnh,
                            GiaTri = hmsx.GiaTri
                        };
                        //công đoạn
                        var congdoan = lstcongdoan.FirstOrDefault(p => p.DMHangMucSanXuatID == hmsx.DMHangMucSanXuatID);
                        //Thành phẩm
                        var lstcongdoanthanhpham = lstcdthanhpham.Where(p => p.DMCongDoanHangMucID == congdoan.DMCongDoanHangMucID).ToList();

                        //hạng muc
                        unitofwork.Context.DMHangMucSanXuats.Add(hmsx2);
                        var cd2 = new DMCongDoanHangMuc()
                        {
                            DMCongDoanHangMucID = Guid.NewGuid(),
                            DMHangMucSanXuatID = hmsx2.DMHangMucSanXuatID,
                            IsActive = congdoan.IsActive,
                            TenCongDoan = congdoan.TenCongDoan
                        };
                        unitofwork.Context.DMCongDoanHangMucs.Add(cd2);

                        foreach (var dcthp in lstcongdoanthanhpham)
                        {
                            var dcthp2 = new DMCongDoanThanhPham()
                            {
                                DMCongDoanThanhPhamID = Guid.NewGuid(),
                                DMCongDoanHangMucID = cd2.DMCongDoanHangMucID,
                                SoLuong = dcthp.SoLuong,
                                IsActive = true,
                            };

                            var tp = lstthanhpham.FirstOrDefault(p => p.ThanhPhamID == dcthp.ThanhPhamID);
                            var lstdinhgnhia = lstdntp.Where(p => p.ThanhPhamID == tp.ThanhPhamID).ToList();

                            var tpmoi = new ThanhPham()
                            {
                                SanPhamID = newsp.SanPhamID,
                                ThanhPhamID = Guid.NewGuid(),
                                AnhDaiDien = tp.AnhDaiDien,
                                DanhSachHinhAnhs = tp.DanhSachHinhAnhs,
                                DonGia = tp.DonGia,
                                DonViID = tp.DonViID,
                                IsActive = tp.IsActive,
                                TenThanhPham = tp.TenThanhPham,
                                MaThanhPham = sp.MaSanPham + hmsx.ViTri
                            };
                            dcthp2.ThanhPhamID = tpmoi.ThanhPhamID;
                            unitofwork.Context.DMCongDoanThanhPhams.Add(dcthp2);
                            unitofwork.Context.ThanhPhams.Add(tpmoi);
                            foreach (var dn in lstdinhgnhia)
                            {
                                var dn2 = new DinhNghiaThanhPham();
                                dn2.ThanhPhamID = tpmoi.ThanhPhamID;
                                dn2.DinhNghiaThanhPhamID = Guid.NewGuid();
                                dn2.IsActive = dn.IsActive;
                                dn2.LoaiHinh = dn.LoaiHinh;
                                dn2.LoaiVai = dn.LoaiVai;
                                dn2.SoLuong = dn.SoLuong;
                                foreach (var i in item.Mau)
                                {
                                    if (dn.LoaiVai == int.Parse(i.LoaiVai))
                                    {
                                        dn2.ID = dn.LoaiHinh == 1 ? i.NguyenLieuID.GetValueOrDefault() : tpcuoi;
                                    }
                                }
                                unitofwork.Context.DinhNghiaThanhPhams.Add(dn2);
                            }
                            tpcuoi = tpmoi.ThanhPhamID;
                        }
                        //
                    }
                    //công đoạn cuối
                    var lstdn = lstdnsp.Where(p => p.SanPhamID == SanPhamID).ToList();
                    foreach (var dnsp in lstdn)
                    {
                        var dnsp2 = new DinhNghiaSanPham()
                        {
                            SanPhamID = newsp.SanPhamID,
                            ThanhPhamID = tpcuoi,
                            DinhNghiaSanPhamID = Guid.NewGuid(),
                            IsActive = dnsp.IsActive,
                            MoTa = dnsp.MoTa,
                            SoLuongThanhPham = dnsp.SoLuongThanhPham,
                            ThoiGianHoanThanh = dnsp.ThoiGianHoanThanh
                        };
                        unitofwork.Context.DinhNghiaSanPhams.Add(dnsp2);
                    }
                    unitofwork.Context.SaveChanges();
                    unitofwork.Save();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string CreateMaSanPham(int Loai, int GioiTinh, string Size)
        {
            unitofwork = new UnitOfWork();
            var ma = "";
            ma += DateTime.Now.ToString("yy");
            if (Loai == 1)
            {
                ma += "L";
            }
            else
            {
                ma += "B";
            }
            if (GioiTinh == 1)
            {
                ma += "T";
            }
            else
            {
                ma += "G";
            }
            ma += Size.ToUpper();
            var count = unitofwork.Context.SanPhams.Count(p => p.MaSanPham.Substring(0, p.MaSanPham.Length - 3).ToUpper() == ma);
            var sodem = 3 - (count.ToString().Length);
            for (int i = 0; i < sodem; i++)
            {
                ma += "0";
            }

            ma += count > 0 ? (count + 1).ToString() : "1";
            return ma;
        }
        public int LuuDanhSachNguyenLieuChoYeuCau(string YeuCauKhachHangID, string SanPhamID, string data, string size)
        {
            db = new DBMLDFDataContext();
            unitofwork = new UnitOfWork();
            Guid yckh, spid;
            var danhsachnguyenlieu = new JavaScriptSerializer().Deserialize<List<DanhSachNguyenLieuModel>>(data);
            var danhsachsize = new JavaScriptSerializer().Deserialize<List<DanhSachSizeModel>>(size);
            if (YeuCauKhachHangID == null || YeuCauKhachHangID == "")
            {
                yckh = Guid.Empty;
            }
            else
            {
                yckh = Guid.Parse(YeuCauKhachHangID);
            }
            if (SanPhamID == null || SanPhamID == "")
            {
                spid = Guid.Empty;
            }
            else
            {
                spid = Guid.Parse(SanPhamID);
            }
            try
            {
                db.Web_SanPham_XoaCacSanPhamCuTheoYCKH(yckh, spid);
            }
            catch { }
            try
            {
                db.Web_SanPham_DeleteAllNguyenLieuTheoYCKH(yckh);
            }
            catch { }
            for (var i = 0; i < danhsachnguyenlieu.Count; i++)
            {
                for (var j = 0; j < danhsachsize.Count; j++)
                {
                    Guid sanphamid = spid;
                    if (j == 0 && i == 0)
                    {
                        var temp = db.Web_DanhMucSanPham_GetAllDataSanPhamID(spid).FirstOrDefault();
                        var msp = CreateMaSanPham(temp.Loai.GetValueOrDefault(), temp.GioiTinh.GetValueOrDefault(), danhsachsize[0].Size);
                        db.Web_SanPham_ThayDoiDuLieuSanPham(danhsachsize[0].Size, danhsachnguyenlieu[0].Mau.ToString(), msp, spid);
                    }
                    else
                    {
                        var sanpham = db.Web_DanhMucSanPham_GetAllDataSanPhamID(spid).FirstOrDefault();
                        var sp = new SanPham();
                        sp.SanPhamID = Guid.NewGuid();
                        sp.TenSanPham = sanpham.TenSanPham;
                        sp.MaSanPham = CreateMaSanPham(sanpham.Loai.GetValueOrDefault(), sanpham.GioiTinh.GetValueOrDefault(), danhsachsize[j].Size);
                        sp.DanhSachHinhAnhs = sanpham.DanhSachHinhAnhs;
                        sp.DonViID = sanpham.DonViID;
                        sp.AnhDaiDien = sanpham.AnhDaiDien;
                        sp.DonGia = sanpham.DonGia;
                        sp.IsActive = true;
                        sp.Size = danhsachsize[j].Size;
                        sp.SizeMauGoc = sanpham.SizeMauGoc;
                        sp.TiLeSize = sanpham.TiLeSize;
                        sp.MauMayID = sanpham.MauMayID;
                        sp.Mau = danhsachnguyenlieu[i].Mau;
                        sp.Loai = sanpham.Loai;
                        sp.GioiTinh = sanpham.GioiTinh;
                        sp.ThoiGian = sanpham.ThoiGian;
                        sp.LoaiHinhSanXuat = sanpham.LoaiHinhSanXuat;
                        unitofwork.Context.SanPhams.Add(sp);
                        sanphamid = sp.SanPhamID;
                    }
                    if (danhsachnguyenlieu[i].VaiChinhID.Count > 0 )
                    {
                        var countnl = danhsachnguyenlieu[i].VaiChinhID;
                        for (var k = 0; k < countnl.Count; k++)
                        {
                            var nlyckh = new NguyenLieuYeuCauKhachHang()
                            {
                                NguyenLieuYeuCauKhachHangID = Guid.NewGuid(),
                                NguyenLieuID = Guid.Parse(countnl[k].NguyenLieuID),
                                LoaiVai = 1,
                                SanPhamID = sanphamid,
                                YeuCauKhachHangID = yckh
                            };
                            unitofwork.Context.NguyenLieuYeuCauKhachHangs.Add(nlyckh);
                        }
                    }
                    if (danhsachnguyenlieu[i].VaiPhoiLotID.Count > 0)
                    {
                        var countnl = danhsachnguyenlieu[i].VaiPhoiLotID;
                        for (var k = 0; k < countnl.Count; k++)
                        {
                            var nlyckh = new NguyenLieuYeuCauKhachHang()
                            {
                                NguyenLieuYeuCauKhachHangID = Guid.NewGuid(),
                                NguyenLieuID = Guid.Parse(countnl[k].NguyenLieuID),
                                LoaiVai = 2,
                                SanPhamID = sanphamid,
                                YeuCauKhachHangID = yckh
                            };
                            unitofwork.Context.NguyenLieuYeuCauKhachHangs.Add(nlyckh);
                        }
                    }
                    if (danhsachnguyenlieu[i].VaiPhoiP1ID.Count > 0)
                    {
                        var countnl = danhsachnguyenlieu[i].VaiPhoiP1ID;
                        for (var k = 0; k < countnl.Count; k++)
                        {
                            var nlyckh = new NguyenLieuYeuCauKhachHang()
                            {
                                NguyenLieuYeuCauKhachHangID = Guid.NewGuid(),
                                NguyenLieuID = Guid.Parse(countnl[k].NguyenLieuID),
                                LoaiVai = 3,
                                SanPhamID = sanphamid,
                                YeuCauKhachHangID = yckh
                            };
                            unitofwork.Context.NguyenLieuYeuCauKhachHangs.Add(nlyckh);
                        }
                    }
                    if (danhsachnguyenlieu[i].VaiPhoiP2ID.Count > 0)
                    {
                        var countnl = danhsachnguyenlieu[i].VaiPhoiP2ID;
                        for (var k = 0; k < countnl.Count; k++)
                        {
                            var nlyckh = new NguyenLieuYeuCauKhachHang()
                            {
                                NguyenLieuYeuCauKhachHangID = Guid.NewGuid(),
                                NguyenLieuID = Guid.Parse(countnl[k].NguyenLieuID),
                                LoaiVai = 4,
                                SanPhamID = sanphamid,
                                YeuCauKhachHangID = yckh
                            };
                            unitofwork.Context.NguyenLieuYeuCauKhachHangs.Add(nlyckh);
                        }
                    }
                    if (danhsachnguyenlieu[i].ChiMayID.Count > 0)
                    {
                        var countnl = danhsachnguyenlieu[i].ChiMayID;
                        for (var k = 0; k < countnl.Count; k++)
                        {
                            var nlyckh = new NguyenLieuYeuCauKhachHang()
                            {
                                NguyenLieuYeuCauKhachHangID = Guid.NewGuid(),
                                NguyenLieuID = Guid.Parse(countnl[k].NguyenLieuID),
                                LoaiVai = 5,
                                SanPhamID = sanphamid,
                                YeuCauKhachHangID = yckh
                            };
                            unitofwork.Context.NguyenLieuYeuCauKhachHangs.Add(nlyckh);
                        }
                    }
                    if (danhsachnguyenlieu[i].MacID.Count > 0)
                    {
                        var countnl = danhsachnguyenlieu[i].MacID;
                        for (var k = 0; k < countnl.Count; k++)
                        {
                            var nlyckh = new NguyenLieuYeuCauKhachHang()
                            {
                                NguyenLieuYeuCauKhachHangID = Guid.NewGuid(),
                                NguyenLieuID = Guid.Parse(countnl[k].NguyenLieuID),
                                LoaiVai = 6,
                                SanPhamID = sanphamid,
                                YeuCauKhachHangID = yckh
                            };
                            unitofwork.Context.NguyenLieuYeuCauKhachHangs.Add(nlyckh);
                        }
                    }
                    if (danhsachnguyenlieu[i].CucID.Count > 0)
                    {
                        var countnl = danhsachnguyenlieu[i].CucID;
                        for (var k = 0; k < countnl.Count; k++)
                        {
                            var nlyckh = new NguyenLieuYeuCauKhachHang()
                            {
                                NguyenLieuYeuCauKhachHangID = Guid.NewGuid(),
                                NguyenLieuID = Guid.Parse(countnl[k].NguyenLieuID),
                                LoaiVai = 7,
                                SanPhamID = sanphamid,
                                YeuCauKhachHangID = yckh
                            };
                            unitofwork.Context.NguyenLieuYeuCauKhachHangs.Add(nlyckh);
                        }
                    }
                    if (danhsachnguyenlieu[i].PKKhacID.Count > 0)
                    {
                        var countnl = danhsachnguyenlieu[i].PKKhacID;
                        for (var k = 0; k < countnl.Count; k++)
                        {
                            var nlyckh = new NguyenLieuYeuCauKhachHang()
                            {
                                NguyenLieuYeuCauKhachHangID = Guid.NewGuid(),
                                NguyenLieuID = Guid.Parse(countnl[k].NguyenLieuID),
                                LoaiVai = 8,
                                SanPhamID = sanphamid,
                                YeuCauKhachHangID = yckh
                            };
                            unitofwork.Context.NguyenLieuYeuCauKhachHangs.Add(nlyckh);
                        }
                    }
                    unitofwork.Save();
                }
            }
            return 1;
        }
        public Guid AddOrUpdateDMSanPham(string data, string tts)
        {

            try
            {
                unitofwork = new UnitOfWork();
                var banghi = new JavaScriptSerializer().Deserialize<SanPham>(data);
                var ts = new JavaScriptSerializer().Deserialize<List<ThuocTinhSanPhamModel>>(tts);

                if (banghi.SanPhamID == null || banghi.SanPhamID == Guid.Empty)
                {
                    // thêm
                    banghi.IsActive = true;
                    banghi.SanPhamID = Guid.NewGuid();
                    banghi.MaSanPham = CreateMaSanPham(banghi.Loai.GetValueOrDefault(), banghi.GioiTinh.GetValueOrDefault(), banghi.Size);
                    banghi.ThoiGian = DateTime.Now;
                    unitofwork.SanPhams.Add(banghi);

                    foreach (var item in ts)
                    {
                        if (item.LoaiHinh == 1)
                        {
                            var gt = new GiaTriThuocTinhSanPham()
                            {
                                GiaTri = item.GiaTri,
                                GiaTriThuocTinhID = Guid.NewGuid(),
                                SanPhamID = banghi.SanPhamID,
                                ThuocTinhSanPhamID = item.ThuocTinhSanPhamID
                            };
                            unitofwork.Context.GiaTriThuocTinhSanPhams.Add(gt);
                        }
                        else
                        {
                            var ttsp = new ThuocTinhSanPham()
                            {
                                KieuDuLieu = item.KieuDuLieu,
                                LoaiHinh = 2,
                                TenThuocTinh = item.TenThuocTinh,
                                ThuocTinhSanPhamID = Guid.NewGuid()
                            };
                            unitofwork.Context.ThuocTinhSanPhams.Add(ttsp);
                            var gt = new GiaTriThuocTinhSanPham()
                            {
                                GiaTri = item.GiaTri,
                                GiaTriThuocTinhID = Guid.NewGuid(),
                                SanPhamID = banghi.SanPhamID,
                                ThuocTinhSanPhamID = ttsp.ThuocTinhSanPhamID
                            };
                            unitofwork.Context.GiaTriThuocTinhSanPhams.Add(gt);
                        }
                    }

                    unitofwork.Save();
                    return banghi.SanPhamID;
                }
                else // sửa
                {
                    var sp = unitofwork.Context.SanPhams.Find(banghi.SanPhamID);
                    sp.MaSanPham = CreateMaSanPham(banghi.Loai.GetValueOrDefault(), banghi.GioiTinh.GetValueOrDefault(), banghi.Size);
                    sp.TenSanPham = banghi.TenSanPham;
                    sp.DonViID = banghi.DonViID;
                    sp.DanhSachHinhAnhs = banghi.DanhSachHinhAnhs;
                    sp.AnhDaiDien = banghi.AnhDaiDien;
                    sp.DonGia = banghi.DonGia;
                    sp.Loai = banghi.Loai;
                    sp.Size = banghi.Size;
                    sp.Mau = banghi.Mau;
                    //sp.MauMayID = banghi.MauMayID;
                    sp.SizeMauGoc = banghi.SizeMauGoc;
                    sp.TiLeSize = banghi.TiLeSize;
                    sp.GioiTinh = banghi.GioiTinh;
                    sp.LoaiHinhSanXuat = banghi.LoaiHinhSanXuat;
                    unitofwork.SanPhams.Update(sp);
                    var lstttchung = unitofwork.Context.GiaTriThuocTinhSanPhams.Where(p => p.SanPhamID == sp.SanPhamID).ToList();
                    var lstkd = (from a in unitofwork.Context.ThuocTinhSanPhams
                                 join b in unitofwork.Context.GiaTriThuocTinhSanPhams on a.ThuocTinhSanPhamID equals b.ThuocTinhSanPhamID
                                 where a.LoaiHinh == 2 && b.SanPhamID == sp.SanPhamID
                                 select a);
                    unitofwork.Context.ThuocTinhSanPhams.RemoveRange(lstkd);
                    var lstkd2 = (from a in unitofwork.Context.ThuocTinhSanPhams
                                  join b in unitofwork.Context.GiaTriThuocTinhSanPhams on a.ThuocTinhSanPhamID equals b.ThuocTinhSanPhamID
                                  where a.LoaiHinh == 2 && b.SanPhamID == sp.SanPhamID
                                  select b);
                    unitofwork.Context.GiaTriThuocTinhSanPhams.RemoveRange(lstkd2);
                    foreach (var item in ts)
                    {
                        if (item.LoaiHinh == 1)
                        {
                            if (lstttchung.Any(p => p.ThuocTinhSanPhamID == item.ThuocTinhSanPhamID))
                            {
                                var it = unitofwork.Context.GiaTriThuocTinhSanPhams.FirstOrDefault(p => p.ThuocTinhSanPhamID == item.ThuocTinhSanPhamID && p.SanPhamID == sp.SanPhamID);
                                it.GiaTri = item.GiaTri;
                            }
                            else
                            {
                                var gt = new GiaTriThuocTinhSanPham()
                                {
                                    GiaTri = item.GiaTri,
                                    GiaTriThuocTinhID = Guid.NewGuid(),
                                    SanPhamID = banghi.SanPhamID,
                                    ThuocTinhSanPhamID = item.ThuocTinhSanPhamID
                                };
                                unitofwork.Context.GiaTriThuocTinhSanPhams.Add(gt);
                            }
                        }
                        else
                        {

                            var ttsp = new ThuocTinhSanPham()
                            {
                                KieuDuLieu = item.KieuDuLieu,
                                LoaiHinh = 2,
                                TenThuocTinh = item.TenThuocTinh,
                                ThuocTinhSanPhamID = Guid.NewGuid()
                            };
                            unitofwork.Context.ThuocTinhSanPhams.Add(ttsp);
                            var gt = new GiaTriThuocTinhSanPham()
                            {
                                GiaTri = item.GiaTri,
                                GiaTriThuocTinhID = Guid.NewGuid(),
                                SanPhamID = banghi.SanPhamID,
                                ThuocTinhSanPhamID = ttsp.ThuocTinhSanPhamID
                            };
                            unitofwork.Context.GiaTriThuocTinhSanPhams.Add(gt);
                        }
                    }
                    unitofwork.Context.SaveChanges();

                    return banghi.SanPhamID;
                }
            }
            catch (Exception ex)
            {
                return Guid.Empty;
            }
        }
        public Guid CapNhatCongDoan(Guid SanPhamID, string lstcongdoan, string congdoancuoi)
        {

            try
            {
                unitofwork = new UnitOfWork();
                var dhid = Guid.Parse(Define.CONGTRINHTONG);
                var qlsx = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.SanPhamID == SanPhamID && p.DonHangID == dhid);

                var congdoan = new JavaScriptSerializer().Deserialize<List<CongDoanSanPhamModel>>(lstcongdoan);
                var cdcuoi = new JavaScriptSerializer().Deserialize<CongDoanSanPhamModel>(congdoancuoi);
                foreach (var cd in congdoan)
                {
                    if (cd.DMHangMucSanXuatID == null)
                    {
                        var hmsx = new DMHangMucSanXuat()
                        {
                            DMHangMucSanXuatID = Guid.NewGuid(),
                            GiaTriDinhMuc = 0,
                            HaoHut = cd.HaoHut,
                            IsActive = true,
                            SanPhamID = SanPhamID,
                            ViTri = cd.ViTri,
                            MoTa = cd.MoTa,
                            GiaTri = cd.GiaTri,
                            DsHinhAnh = cd.DsHinhAnh,
                            ThoiGianHoanThanh = cd.ThoiGianHoanThanh.GetValueOrDefault()
                        };
                        unitofwork.Context.DMHangMucSanXuats.Add(hmsx);
                        var dongdoanmd = new DMCongDoanHangMuc()
                        {
                            DMCongDoanHangMucID = Guid.NewGuid(),
                            DMHangMucSanXuatID = hmsx.DMHangMucSanXuatID,
                            IsActive = true,
                            TenCongDoan = cd.TenCongDoan
                        };
                        unitofwork.Context.DMCongDoanHangMucs.Add(dongdoanmd);
                        foreach (var item2 in cd.item)
                        {
                            var sp2 = new DMCongDoanThanhPham()
                            {
                                DMCongDoanHangMucID = dongdoanmd.DMCongDoanHangMucID,
                                DMCongDoanThanhPhamID = Guid.NewGuid(),
                                IsActive = true,
                                SoLuong = item2.SoLuong.GetValueOrDefault(),
                                ThanhPhamID = item2.ThanhPhamID.GetValueOrDefault()
                            };
                            unitofwork.Context.DMCongDoanThanhPhams.Add(sp2);
                        }
                    }
                    else
                    {
                        var hmsx = unitofwork.Context.DMHangMucSanXuats.FirstOrDefault(p => p.DMHangMucSanXuatID == cd.DMHangMucSanXuatID);
                        hmsx.ViTri = cd.ViTri;
                        hmsx.MoTa = cd.MoTa;
                        hmsx.GiaTri = cd.GiaTri;
                        hmsx.DsHinhAnh = cd.DsHinhAnh;
                        hmsx.HaoHut = cd.HaoHut.GetValueOrDefault();
                        hmsx.ThoiGianHoanThanh = cd.ThoiGianHoanThanh.GetValueOrDefault();
                        var dongdoanmd = unitofwork.Context.DMCongDoanHangMucs.FirstOrDefault(p => p.DMHangMucSanXuatID == cd.DMHangMucSanXuatID);
                        dongdoanmd.TenCongDoan = cd.TenCongDoan;
                        //
                        var lstCongDoanThanhPham = unitofwork.Context.DMCongDoanThanhPhams.Where(p => p.DMCongDoanHangMucID == dongdoanmd.DMCongDoanHangMucID).ToList();
                        // list sản phẩm không có
                        var lstkhongco = cd.item.Where(p => !lstCongDoanThanhPham.Any(p2 => p.ThanhPhamID == p2.ThanhPhamID));
                        foreach (var item in lstkhongco)
                        {
                            var sp2 = new DMCongDoanThanhPham()
                            {
                                DMCongDoanHangMucID = dongdoanmd.DMCongDoanHangMucID,
                                DMCongDoanThanhPhamID = Guid.NewGuid(),
                                IsActive = true,
                                SoLuong = item.SoLuong.GetValueOrDefault(),
                                ThanhPhamID = item.ThanhPhamID.GetValueOrDefault()
                            };
                            unitofwork.Context.DMCongDoanThanhPhams.Add(sp2);
                        }
                        //
                        foreach (var item in lstCongDoanThanhPham)
                        {
                            var sp2 = cd.item.FirstOrDefault(p => p.ThanhPhamID == item.ThanhPhamID);
                            if (sp2 != null)
                            {
                                item.SoLuong = sp2.SoLuong.GetValueOrDefault();
                            }
                            else
                            {
                                unitofwork.Context.DMCongDoanThanhPhams.Remove(item);
                            }
                        }

                    }


                }
                var havecongdoancuoi = unitofwork.Context.DinhNghiaSanPhams.Any(p => p.SanPhamID == SanPhamID);
                if (!havecongdoancuoi)
                {
                    if (cdcuoi != null && cdcuoi.item != null && cdcuoi.item.Any())
                    {
                        foreach (var item in cdcuoi.item)
                        {
                            var dnsp = new DinhNghiaSanPham()
                            {
                                DinhNghiaSanPhamID = Guid.NewGuid(),
                                IsActive = true,
                                SanPhamID = SanPhamID,
                                SoLuongThanhPham = item.SoLuong.GetValueOrDefault(),
                                ThanhPhamID = item.ThanhPhamID.GetValueOrDefault(),
                                ThoiGianHoanThanh = cdcuoi.ThoiGianHoanThanh,
                                MoTa = cdcuoi.MoTa,
                                DsHinhAnh = cdcuoi.DsHinhAnh,
                                GiaTri = cdcuoi.GiaTri
                            };
                            unitofwork.Context.DinhNghiaSanPhams.Add(dnsp);
                        }
                    }
                }
                else
                {
                    var lstdinhghiasanpham = unitofwork.Context.DinhNghiaSanPhams.Where(p => p.SanPhamID == SanPhamID).ToList();
                    // list sản phẩm không có
                    var lstkhongco = cdcuoi.item.Where(p => !lstdinhghiasanpham.Any(p2 => p.ThanhPhamID == p2.ThanhPhamID));
                    if (cdcuoi != null && cdcuoi.item != null && cdcuoi.item.Any())
                    {
                        foreach (var item in lstkhongco)
                        {
                            var dnsp = new DinhNghiaSanPham()
                            {
                                DinhNghiaSanPhamID = Guid.NewGuid(),
                                IsActive = true,
                                SanPhamID = SanPhamID,
                                SoLuongThanhPham = item.SoLuong.GetValueOrDefault(),
                                ThanhPhamID = item.ThanhPhamID.GetValueOrDefault(),
                                ThoiGianHoanThanh = cdcuoi.ThoiGianHoanThanh,
                                MoTa = cdcuoi.MoTa
                            };
                            unitofwork.Context.DinhNghiaSanPhams.Add(dnsp);
                        }

                        //
                        foreach (var item in lstdinhghiasanpham)
                        {
                            var sp2 = cdcuoi.item.FirstOrDefault(p => p.ThanhPhamID == item.ThanhPhamID);
                            if (sp2 != null)
                            {
                                item.SoLuongThanhPham = sp2.SoLuong.GetValueOrDefault();
                                item.ThoiGianHoanThanh = cdcuoi.ThoiGianHoanThanh;
                                item.MoTa = cdcuoi.MoTa;
                                item.GiaTri = cdcuoi.GiaTri;
                                item.DsHinhAnh = cdcuoi.DsHinhAnh;
                            }
                            else
                            {
                                unitofwork.Context.DinhNghiaSanPhams.Remove(item);
                            }
                        }
                        if (qlsx != null)
                        {
                            var dinhnghiasanpham = unitofwork.Context.CongDoanSanPhamDinhNghias.Where(p => p.QuanLySanXuatID == qlsx.QuanLySanXuatID);
                            if (dinhnghiasanpham.Any())
                            {
                                unitofwork.Context.CongDoanSanPhamDinhNghias.RemoveRange(dinhnghiasanpham);
                            }
                            foreach (var item in cdcuoi.item)
                            {
                                var dnsp = new CongDoanSanPhamDinhNghia()
                                {
                                    CongDoanSanPhamDinhNghiaID = Guid.NewGuid(),
                                    QuanLySanXuatID = qlsx.QuanLySanXuatID,
                                    SoLuong = item.SoLuong,
                                    ThanhPhamID = item.ThanhPhamID,
                                };
                                unitofwork.Context.CongDoanSanPhamDinhNghias.Add(dnsp);
                            }
                        }
                    }
                }

                unitofwork.Context.SaveChanges();
                return SanPhamID;

            }
            catch (Exception ex)
            {
                return Guid.Empty;
            }
        }
        public int DeleteDMSanPham(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var banghi = new SanPham();
                unitofwork.Context.SanPhams.Find(Guid.Parse(data)).IsActive = false;
                unitofwork.Context.SaveChanges();
                return 2;
            }
            catch
            {
                return 0;
            }
        }
        public SanXuatModel SanXuatSanPhamChuaBatDau(string SanPhamID, string DonHangID)
        {
            var sanphamid = Guid.Parse(SanPhamID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new SanXuatModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
            var sanpham = unitofwork.Context.SanPhams.FirstOrDefault(p => p.SanPhamID == sanphamid);


            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);
            var khosanpham = unitofwork.KhoSanPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;

            xsmodel.Size = sanpham.Size;
            xsmodel.GioiTinh = sanpham.GioiTinh == 1 ? "Nam" : "Nữ";
            xsmodel.Loai = sanpham.Loai == 1 ? "Người lớn" : "Trẻ con";
            xsmodel.ThoiGian = donhang.ThoiGian.GetValueOrDefault().ToString("dd/MM/yyyy");
            xsmodel.TiLeSize = sanpham.TiLeSize;
            xsmodel.SizeMauGoc = sanpham.SizeMauGoc;


            xsmodel.TenSanPham = sanpham.TenSanPham;
            xsmodel.MaSanPham = sanpham.MaSanPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;
            xsmodel.SanPhamID = sanphamid;

            xsmodel.SoLuong = 1;
            xsmodel.GiaTriDonHang = 0;

            //Lấy công đoạn
            xsmodel.lstCongDoan = new List<CongDoanModel>();
            var cdsanpham = (from a in unitofwork.Context.DMHangMucSanXuats
                             join b in unitofwork.Context.DMCongDoanHangMucs on a.DMHangMucSanXuatID equals b.DMHangMucSanXuatID
                             where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanphamid
                             select new
                             {
                                 a.GiaTriDinhMuc,
                                 a.ThoiGianHoanThanh,
                                 b.TenCongDoan,
                                 a.ViTri,
                                 a.DMHangMucSanXuatID
                             }).ToList();
            var lstthanhpham = db.Web_DinhNghiaSanPham_LayDanhSachThanhPham(sanphamid).ToList();
            foreach (var cd in cdsanpham)
            {
                var lstThanhPham = lstthanhpham.Where(p2 => p2.DMHangMucSanXuatID == cd.DMHangMucSanXuatID).Select(p2 => new CongDoanThanhPhamModel
                {
                    DonVi = p2.TenDonVi,
                    MaThanhPham = p2.MaThanhPham,
                    TenThanhPham = p2.TenThanhPham,
                    SoLuongTong = p2.SoLuong,
                    AnhDaiDien = p2.AnhDaiDien,
                    LoaiHinh = 0,
                    SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID) != null ? khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID).SoLuongTon.GetValueOrDefault() : 0,
                    lstNguyenLieu = db.Web_ThanhPham_DinhNghiaThanhPham(p2.ThanhPhamID).Select(p => new CongDoanNguyenLieuModel()
                    {
                        AnhDaiDien = p.AnhDaiDien,
                        DonVi = p.TenDonVi,
                        LoaiHinh = p.LoaiHinh,
                        MaNguyenLieu = p.MaNguyenLieu,
                        SoLuong = p.SoLuong * p2.SoLuong,
                        SoLuongDaCo = (p.LoaiHinh == 1 ? (khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID) != null ? khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID).SoLuongTon : 0) : (khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID) != null ? khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID).SoLuongTon.GetValueOrDefault() : 0)),
                        TenNguyenLieu = p.TenNguyenLieu
                    }).ToList()
                }).ToList();

                var ncd = new CongDoanModel()
                {
                    TenCongDoan = cd.TenCongDoan,
                    ThoiGianDinhMuc = cd.ThoiGianHoanThanh.GetValueOrDefault(),
                    ThoiGianHienTai = 0,
                    lstThanhPham = lstThanhPham,
                    ViTri = cd.ViTri,
                    HangMucSanXuatID = cd.DMHangMucSanXuatID,
                    TrangThai = 1
                };
                xsmodel.lstCongDoan.Add(ncd);
            }
            // Lấy công đoạn cuối

            var dinhnghiasanpham = unitofwork.Context.DinhNghiaSanPhams.Where(p => p.IsActive == true && p.SanPhamID == sanphamid);
            if (dinhnghiasanpham.Any())
            {
                var lstCongDoan = db.Web_DinhNghiaSanPham_LayDanhSachDinhNghia(sanphamid).ToList();
                var cdc = new CongDoanCuoiModel()
                {
                    SoLuongDaCo = khosanpham.FirstOrDefault(p => p.SanPhamID == sanphamid) != null ? khosanpham.FirstOrDefault(p => p.SanPhamID == sanphamid).SoLuongTon.GetValueOrDefault() : 0,
                    ThoiGianDinhMuc = dinhnghiasanpham.FirstOrDefault().ThoiGianHoanThanh.GetValueOrDefault(),
                    ThoiGianHienTai = 0,
                    lstThanhPham = lstCongDoan.Select(p => new CongDoanThanhPhamModel
                    {
                        AnhDaiDien = p.TenThanhPham,
                        DonVi = p.TenDonVi,
                        LoaiHinh = 0,
                        MaThanhPham = p.MaThanhPham,
                        SoLuongTong = p.SoLuongThanhPham,
                        SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p2 => p2.ThanhPhamID == p.ThanhPhamID) != null ? khothanhphams.FirstOrDefault(p2 => p2.ThanhPhamID == p.ThanhPhamID).SoLuongTon.GetValueOrDefault() : 0,
                        TenThanhPham = p.TenThanhPham,
                        lstNguyenLieu = db.Web_ThanhPham_DinhNghiaThanhPham(p.ThanhPhamID).Select(p3 => new CongDoanNguyenLieuModel
                        {
                            AnhDaiDien = p3.AnhDaiDien,
                            DonVi = p3.TenDonVi,
                            LoaiHinh = p3.LoaiHinh,
                            MaNguyenLieu = p3.MaNguyenLieu,
                            SoLuong = p3.SoLuong,
                            SoLuongDaCo = (p3.LoaiHinh == 1 ? (khonguyenlieus.FirstOrDefault(p4 => p4.NguyenLieuID == p3.NguyenLieuID) != null ? khonguyenlieus.FirstOrDefault(p4 => p4.NguyenLieuID == p3.NguyenLieuID).SoLuongTon : 0) : (khothanhphams.FirstOrDefault(p4 => p4.ThanhPhamID == p3.NguyenLieuID) != null ? khothanhphams.FirstOrDefault(p4 => p4.ThanhPhamID == p3.NguyenLieuID).SoLuongTon.GetValueOrDefault() : 0)),
                        }).ToList(),
                    }).ToList()
                };
                xsmodel.congDoanCuoi = cdc;
            }


            return xsmodel;
        }
        public SanXuatModel SanXuatSanPhamDangSanXuat(string SanPhamID, string DonHangID)
        {
            var sanphamid = Guid.Parse(SanPhamID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new SanXuatModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
            var qlsanxuat = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.DonHangID == donhangid && sanphamid == p.SanPhamID);

            var sanpham = unitofwork.Context.SanPhams.FirstOrDefault(p => p.SanPhamID == sanphamid);


            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);
            var khosanpham = unitofwork.KhoSanPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;



            xsmodel.Size = sanpham.Size;
            xsmodel.GioiTinh = sanpham.GioiTinh == 1 ? "Nam" : "Nữ";
            xsmodel.Loai = sanpham.Loai == 1 ? "Người lớn" : "Trẻ con";
            xsmodel.ThoiGian = donhang.ThoiGian.GetValueOrDefault().ToString("dd/MM/yyyy");
            xsmodel.TiLeSize = sanpham.TiLeSize;
            xsmodel.SizeMauGoc = sanpham.SizeMauGoc;

            xsmodel.TenSanPham = sanpham.TenSanPham;
            xsmodel.MaSanPham = sanpham.MaSanPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;
            xsmodel.SanPhamID = sanphamid;

            xsmodel.SoLuong = 1;
            xsmodel.GiaTriDonHang = 0;

            //Lấy công đoạn
            xsmodel.lstCongDoan = new List<CongDoanModel>();
            var cdsanpham = (from a in unitofwork.Context.HangMucSanXuats
                             join b in unitofwork.Context.CongDoanHangMucs on a.HangMucSanXuatID equals b.HangMucSanXuatID
                             join c in unitofwork.Context.QuanLySanXuats on a.QuanLySanXuatID equals c.QuanLySanXuatID
                             where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanphamid && c.QuanLySanXuatID == qlsanxuat.QuanLySanXuatID
                             select new
                             {
                                 a.GiaTriDinhMuc,
                                 a.ThoiGianHoanThanh,
                                 a.ViTri,
                                 b.TenCongDoan,
                                 a.HangMucSanXuatID,
                                 b.TrangThai,
                                 b.ThoiGianBatDau
                             }).ToList();
            var lstthanhpham = db.Web_QuanLySanXuat_LayDanhSachThanhPham(sanphamid).ToList();
            foreach (var cd in cdsanpham)
            {
                var lstThanhPham = lstthanhpham.Where(p2 => p2.HangMucSanXuatID == cd.HangMucSanXuatID).Select(p2 => new CongDoanThanhPhamModel
                {
                    DonVi = p2.TenDonVi,
                    MaThanhPham = p2.MaThanhPham,
                    TenThanhPham = p2.TenThanhPham,
                    SoLuongTong = p2.SoLuong.GetValueOrDefault(),
                    AnhDaiDien = p2.AnhDaiDien,
                    ThanhPhamID = p2.ThanhPhamID,
                    LoaiHinh = 0,
                    SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID) != null ? khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID).SoLuongTon.GetValueOrDefault() : 0,
                    lstNguyenLieu = db.Web_ThanhPham_DinhNghiaThanhPham(p2.ThanhPhamID).Select(p => new CongDoanNguyenLieuModel()
                    {
                        AnhDaiDien = p.AnhDaiDien,
                        DonVi = p.TenDonVi,
                        LoaiHinh = p.LoaiHinh,
                        MaNguyenLieu = p.MaNguyenLieu,
                        SoLuong = p.SoLuong * p2.SoLuong.GetValueOrDefault(),
                        SoLuongDaCo = (p.LoaiHinh == 1 ? (khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID) != null ? khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID).SoLuongTon : 0) : (khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID) != null ? khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID).SoLuongTon.GetValueOrDefault() : 0)),
                        TenNguyenLieu = p.TenNguyenLieu,
                        NguyenLieuID = p.NguyenLieuID
                    }).ToList()
                }).ToList();

                var ncd = new CongDoanModel()
                {
                    TenCongDoan = cd.TenCongDoan,
                    ThoiGianDinhMuc = cd.ThoiGianHoanThanh.GetValueOrDefault(),
                    ThoiGianHienTai = cd.ThoiGianBatDau.HasValue ? (DateTime.Now - cd.ThoiGianBatDau.GetValueOrDefault()).TotalHours : 0,
                    lstThanhPham = lstThanhPham,
                    ViTri = cd.ViTri,
                    HangMucSanXuatID = cd.HangMucSanXuatID,
                    TrangThai = cd.TrangThai.GetValueOrDefault()
                };
                xsmodel.lstCongDoan.Add(ncd);
            }
            // Lấy công đoạn cuối

            var dinhnghiasanpham = unitofwork.Context.DinhNghiaSanPhams.Where(p => p.IsActive == true && p.SanPhamID == sanphamid);
            if (dinhnghiasanpham.Any())
            {
                var lstCongDoan = db.Web_QuanLySanXuat_LayDanhSachDinhNghiaSanPham(sanphamid).Where(p => p.QuanLySanXuatID == qlsanxuat.QuanLySanXuatID).ToList();
                var thoigianht = qlsanxuat.ThoiGianBatDauCongDoanCuoi.HasValue ? (DateTime.Now - qlsanxuat.ThoiGianBatDauCongDoanCuoi.GetValueOrDefault()).TotalHours : 0;
                var cdc = new CongDoanCuoiModel()
                {
                    SoLuongDaCo = khosanpham.FirstOrDefault(p => p.SanPhamID == sanphamid) != null ? khosanpham.FirstOrDefault(p => p.SanPhamID == sanphamid).SoLuongTon.GetValueOrDefault() : 0,
                    ThoiGianDinhMuc = dinhnghiasanpham.FirstOrDefault().ThoiGianHoanThanh.GetValueOrDefault(),
                    ThoiGianHienTai = thoigianht,
                    TrangThai = qlsanxuat.TrangThaiCongDoanCuoi,
                    lstThanhPham = lstCongDoan.Select(p => new CongDoanThanhPhamModel
                    {
                        AnhDaiDien = p.AnhDaiDien,
                        DonVi = p.TenDonVi,
                        LoaiHinh = 0,
                        MaThanhPham = p.MaThanhPham,
                        ThanhPhamID = p.ThanhPhamID,
                        SoLuongTong = p.SoLuong.GetValueOrDefault(),
                        SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p2 => p2.ThanhPhamID == p.ThanhPhamID) != null ? khothanhphams.FirstOrDefault(p2 => p2.ThanhPhamID == p.ThanhPhamID).SoLuongTon.GetValueOrDefault() : 0,
                        TenThanhPham = p.TenThanhPham,
                        lstNguyenLieu = new List<CongDoanNguyenLieuModel>()
                    }).ToList()
                };
                xsmodel.congDoanCuoi = cdc;
            }


            return xsmodel;
        }

        public bool SanXuatThanhPham(string DonHangID, string SanPhamID, string HangMucSanXuatID, string datanhap, string dataxuat, Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var lstthanhpham = new JavaScriptSerializer().Deserialize<List<SanXuatThanhPhamModel>>(datanhap);
                var lstnguyenlieu = new JavaScriptSerializer().Deserialize<List<SanXuatNguyenLieuModel>>(dataxuat);
                var sanphamid = Guid.Parse(SanPhamID);
                var donhangid = Guid.Parse(DonHangID);
                var hmsxid = Guid.Parse(HangMucSanXuatID);
                var hmsx = unitofwork.Context.HangMucSanXuats.FirstOrDefault(p => p.HangMucSanXuatID == hmsxid);
                var qlsanxuat = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.DonHangID == donhangid && p.SanPhamID == sanphamid);
                var congdoan = unitofwork.Context.CongDoanHangMucs.FirstOrDefault(p => p.HangMucSanXuatID == hmsxid);
                congdoan.TrangThai = 3;
                var congdoansau = (from a in unitofwork.Context.CongDoanHangMucs
                                   join b in unitofwork.Context.HangMucSanXuats on a.HangMucSanXuatID equals b.HangMucSanXuatID
                                   where b.ViTri > hmsx.ViTri && a.HangMucSanXuatID != hmsxid && b.QuanLySanXuatID == qlsanxuat.QuanLySanXuatID
                                   orderby b.ViTri ascending
                                   select a).FirstOrDefault();
                if (congdoansau != null)
                {
                    congdoansau.TrangThai = 2;
                }
                else
                {
                    qlsanxuat.TrangThaiCongDoanCuoi = 2;
                }
                // Tạo phiếu nhập
                var maxDeNghi2 = unitofwork.Context.PhieuNhapThanhPhams.Max(p => p.MaPhieuNhap);
                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNTP");

                var phieunhaptp = new PhieuNhapThanhPham()
                {
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    DonHangID = qlsanxuat.DonHangID,
                    IsActive = true,
                    MaPhieuNhap = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuNhapThanhPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuNhapThanhPhams.Add(phieunhaptp);
                foreach (var item in lstthanhpham)
                {
                    var ct = new ChiTietPhieuNhapThanhPham()
                    {
                        ChiTietPhieuNhapThanhPhamID = Guid.NewGuid(),
                        GiaNhap = 0,
                        IsActive = true,
                        PhieuNhapThanhPhamID = phieunhaptp.PhieuNhapThanhPhamID,
                        SoLuong = item.SoLuongTong.GetValueOrDefault(),
                        ThanhPhamID = item.ThanhPhamID.GetValueOrDefault(),
                        TinhTrang = 1
                    };
                    unitofwork.Context.ChiTietPhieuNhapThanhPhams.Add(ct);
                }
                //Tao phiếu xuất

                if (lstnguyenlieu.Any(p => p.LoaiHinh == 1))
                {
                    maxDeNghi2 = unitofwork.Context.PhieuXuatNguyenLieux.Max(p => p.MaPhieuXuat);
                    maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXNL");
                    var px = new PhieuXuatNguyenLieu()
                    {
                        DonHangID = qlsanxuat.DonHangID,
                        DanhSachHinhAnhs = "",
                        DoiTacID = null,
                        IsActive = true,
                        LoaiHinh = 2,
                        MaPhieuXuat = maDeNghi2,
                        NguoiTaoID = NhanVienID,
                        PhieuXuatNguyenLieuID = Guid.NewGuid(),
                        ThoiGian = DateTime.Now
                    };
                    unitofwork.Context.PhieuXuatNguyenLieux.Add(px);
                    foreach (var item in lstnguyenlieu.Where(p => p.LoaiHinh == 1))
                    {
                        var ct = new ChiTietPhieuXuatNguyenLieu()
                        {
                            ChiTietPhieuXuatNguyenLieuID = Guid.NewGuid(),
                            IsActive = true,
                            PhieuXuatNguyenLieu = px.PhieuXuatNguyenLieuID,
                            SoLuong = item.SoLuongCan,
                            NguyenLieuID = item.NguyenLieuID.GetValueOrDefault(),
                        };
                        unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Add(ct);
                    }
                }
                if (lstnguyenlieu.Any(p => p.LoaiHinh == 2))
                {
                    maxDeNghi2 = unitofwork.Context.PhieuXuatThanhPhams.Max(p => p.MaPhieuXuat);
                    maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXTP");
                    var px = new PhieuXuatThanhPham()
                    {
                        DonHangID = qlsanxuat.DonHangID,
                        DanhSachHinhAnhs = "",
                        DoiTacID = null,
                        IsActive = true,
                        LoaiHinh = 2,
                        MaPhieuXuat = maDeNghi2,
                        NguoiTaoID = NhanVienID,
                        PhieuXuatThanhPhamID = Guid.NewGuid(),
                        ThoiGian = DateTime.Now
                    };
                    unitofwork.Context.PhieuXuatThanhPhams.Add(px);
                    foreach (var item in lstnguyenlieu.Where(p => p.LoaiHinh == 2))
                    {
                        var ct = new ChiTietPhieuXuatThanhPham()
                        {
                            ChiTietPhieuXuatThanhPhamID = Guid.NewGuid(),
                            IsActive = true,
                            PhieuXuatThanhPhamID = px.PhieuXuatThanhPhamID,
                            SoLuong = item.SoLuongCan,
                            ThanhPhamID = item.NguyenLieuID.GetValueOrDefault(),
                        };
                        unitofwork.Context.ChiTietPhieuXuatThanhPhams.Add(ct);
                    }

                }
                unitofwork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SanXuatThanhPhamCongDoan(string DonHangID, string SanPhamID, int SoLuong, string dataxuat, Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var lstthanhpham = new JavaScriptSerializer().Deserialize<List<SanXuatThanhPhamModel>>(dataxuat);
                var sanphamid = Guid.Parse(SanPhamID);
                var donhangid = Guid.Parse(DonHangID);

                //var ctdh = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.DonHangID == donhangid && p.SanPhamID == sanphamid);
                var sanphamton = unitofwork.KhoSanPhams.LayTon(DonHangID).Where(p => p.SanPhamID == sanphamid).FirstOrDefault();
                var soluongton = sanphamton != null ? sanphamton.SoLuongTon : 0;
                var qlsanxuat = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.DonHangID == donhangid && p.SanPhamID == sanphamid);
                qlsanxuat.TrangThaiCongDoanCuoi = 3;
                unitofwork.QuanLySanXuats.Update(qlsanxuat);
                // Tạo phiếu nhập
                var maxDeNghi2 = unitofwork.Context.PhieuNhapSanPhams.Max(p => p.MaPhieuNhap);
                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNSP");

                var phieunhaptp = new PhieuNhapSanPham()
                {
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    DonHangID = donhangid,
                    IsActive = true,
                    MaPhieuNhap = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuNhapSanPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuNhapSanPhams.Add(phieunhaptp);
                var ct = new ChiTietPhieuNhapSanPham()
                {
                    ChiTietPhieuNhapSanPhamID = Guid.NewGuid(),
                    GiaNhap = 0,
                    IsActive = true,
                    PhieuNhapSanPhamID = phieunhaptp.PhieuNhapSanPhamID,
                    SoLuong = SoLuong,
                    SanPhamID = sanphamid,
                    TinhTrang = 1,
                    LoaiHinh = 2,
                };
                unitofwork.Context.ChiTietPhieuNhapSanPhams.Add(ct);
                //Tao phiếu xuất
                maxDeNghi2 = unitofwork.Context.PhieuXuatThanhPhams.Max(p => p.MaPhieuXuat);
                maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXTP");
                var px = new PhieuXuatThanhPham()
                {
                    DonHangID = donhangid,
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    IsActive = true,
                    LoaiHinh = 2,
                    MaPhieuXuat = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuXuatThanhPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuXuatThanhPhams.Add(px);
                foreach (var item in lstthanhpham)
                {
                    var ct2 = new ChiTietPhieuXuatThanhPham()
                    {
                        ChiTietPhieuXuatThanhPhamID = Guid.NewGuid(),
                        IsActive = true,
                        PhieuXuatThanhPhamID = px.PhieuXuatThanhPhamID,
                        SoLuong = item.SoLuongTong.GetValueOrDefault(),
                        ThanhPhamID = item.ThanhPhamID.GetValueOrDefault(),
                    };
                    unitofwork.Context.ChiTietPhieuXuatThanhPhams.Add(ct2);
                }
                unitofwork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool BatDauSanXuat(string SanPhamID, string DonHangID)
        {
            try
            {
                var sanphamid = Guid.Parse(SanPhamID);
                var donhangid = Guid.Parse(DonHangID);
                unitofwork = new UnitOfWork();
                db = new DBMLDFDataContext();
                var cdsanpham = (from a in unitofwork.Context.DMHangMucSanXuats
                                 join b in unitofwork.Context.DMCongDoanHangMucs on a.DMHangMucSanXuatID equals b.DMHangMucSanXuatID
                                 where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanphamid
                                 select new
                                 {
                                     a.GiaTriDinhMuc,
                                     a.ThoiGianHoanThanh,
                                     b.TenCongDoan,
                                     a.DMHangMucSanXuatID,
                                     a.ViTri,
                                     b.DMCongDoanHangMucID,
                                     a.HaoHut,
                                     a.MoTa
                                 }).ToList();
                var quanlysanxuat = new QuanLySanXuat()
                {
                    DonHangID = donhangid,
                    IsActive = true,
                    LoaiHinh = 1,
                    QuanLySanXuatID = Guid.NewGuid(),
                    SanPhamID = sanphamid,
                    TenCongDoan = cdsanpham.OrderBy(p => p.ViTri.GetValueOrDefault()).FirstOrDefault().TenCongDoan,
                    ThoiGian = DateTime.Now,
                    TrangThaiCongDoanCuoi = 1
                };
                unitofwork.Context.QuanLySanXuats.Add(quanlysanxuat);
                foreach (var cd in cdsanpham)
                {
                    var hangmucsanxuat = new HangMucSanXuat()
                    {
                        GiaTriDinhMuc = cd.GiaTriDinhMuc,
                        HangMucSanXuatID = Guid.NewGuid(),
                        HaoHut = cd.HaoHut,
                        IsActive = true,
                        ViTri = cd.ViTri,
                        QuanLySanXuatID = quanlysanxuat.QuanLySanXuatID,
                        SanPhamID = sanphamid,
                        ThoiGianHoanThanh = cd.ThoiGianHoanThanh.GetValueOrDefault(),
                    };
                    unitofwork.Context.HangMucSanXuats.Add(hangmucsanxuat);
                    var congdoan = new CongDoanHangMuc()
                    {
                        CongDoanHangMucID = Guid.NewGuid(),
                        HangMucSanXuatID = hangmucsanxuat.HangMucSanXuatID,
                        IsActive = true,
                        LyDoTamDung = "",
                        TenCongDoan = cd.TenCongDoan,
                        ThoiGianBatDau = DateTime.Now,
                        TrangThai = cdsanpham.OrderBy(p => p.ViTri.GetValueOrDefault()).FirstOrDefault().DMHangMucSanXuatID == cd.DMHangMucSanXuatID ? 2 : 1
                    };
                    unitofwork.Context.CongDoanHangMucs.Add(congdoan);
                    var lstthanhpham = db.Web_DinhNghiaSanPham_LayDanhSachThanhPham(sanphamid).Where(p => p.DMHangMucSanXuatID == cd.DMHangMucSanXuatID).ToList();
                    foreach (var thanhpham in lstthanhpham)
                    {
                        var lstcongthuc = db.Web_ThanhPham_DinhNghiaThanhPham(thanhpham.ThanhPhamID).ToList();
                        var tp = new CongDoanThanhPham()
                        {
                            CongDoanHangMucID = congdoan.CongDoanHangMucID,
                            CongDoanThanhPhamID = Guid.NewGuid(),
                            IsActive = true,
                            SoLuong = thanhpham.SoLuong,
                            ThanhPhamID = thanhpham.ThanhPhamID
                        };
                        unitofwork.Context.CongDoanThanhPhams.Add(tp);
                        foreach (var item in lstcongthuc)
                        {
                            var dcdt = new CongDoanThanhPhamDinhNghia()
                            {
                                CongDoanThanhPhamDinhNghiaID = Guid.NewGuid(),
                                CongDoanThanhPhamID = tp.CongDoanThanhPhamID,
                                ID = item.NguyenLieuID,
                                LoaiHinh = item.LoaiHinh,
                                SoLuong = item.SoLuong,
                                ThanhPhamID = thanhpham.ThanhPhamID
                            };
                            unitofwork.Context.CongDoanThanhPhamDinhNghias.Add(dcdt);
                        }
                    }



                }
                var dinhnghiasanpham = unitofwork.Context.DinhNghiaSanPhams.Where(p => p.IsActive == true && p.SanPhamID == sanphamid);
                foreach (var item in dinhnghiasanpham)
                {
                    var dnsp = new CongDoanSanPhamDinhNghia()
                    {
                        CongDoanSanPhamDinhNghiaID = Guid.NewGuid(),
                        QuanLySanXuatID = quanlysanxuat.QuanLySanXuatID,
                        SoLuong = item.SoLuongThanhPham,
                        ThanhPhamID = item.ThanhPhamID
                    };
                    unitofwork.Context.CongDoanSanPhamDinhNghias.Add(dnsp);
                }
                unitofwork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public int TaoCongDoanTuDong(Guid YeuCauKhachHangID, string data)
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var listsp = db.LayDanhSachSanPhamTrongCungMotYCKH(YeuCauKhachHangID).ToList();
            foreach(var i in listsp)
            {
                var listnl = unitofwork.Context.NguyenLieuYeuCauKhachHangs.Where(x => x.SanPhamID == i.SanPhamID).ToList();
                try
                {

                    var dbid = Guid.Parse(Define.CONGTRINHTONG);

                    var nguyenlieu = new JavaScriptSerializer().Deserialize<List<CongDoanNguyenLieuDinhNghiaModel>>(data);
                    var havesanxuat = unitofwork.Context.QuanLySanXuats.Any(p => p.SanPhamID == i.SanPhamID && p.DonHangID != dbid);
                    if (havesanxuat)
                    {
                        return 2;
                    }
                    else
                    {
                        //Xóa công đoạn cũ
                        var lstcdcu = unitofwork.Context.DMHangMucSanXuats.Where(p => p.SanPhamID == i.SanPhamID);
                        var lstthanhpham = unitofwork.Context.ThanhPhams.Where(p => p.SanPhamID == i.SanPhamID);
                        unitofwork.Context.ThanhPhams.RemoveRange(lstthanhpham);
                        var dnsps = unitofwork.Context.DinhNghiaSanPhams.Where(p => p.SanPhamID == i.SanPhamID);
                        unitofwork.Context.DinhNghiaSanPhams.RemoveRange(dnsps);
                        foreach (var item in lstcdcu)
                        {
                            unitofwork.Context.DMHangMucSanXuats.Remove(item);
                            var congdoan = unitofwork.Context.DMCongDoanHangMucs.FirstOrDefault(p => p.DMHangMucSanXuatID == item.DMHangMucSanXuatID);
                            var cdtp = unitofwork.Context.DMCongDoanThanhPhams.Where(p => p.DMCongDoanHangMucID == congdoan.DMCongDoanHangMucID);

                            unitofwork.Context.DMCongDoanHangMucs.Remove(congdoan);
                            unitofwork.Context.DMCongDoanThanhPhams.RemoveRange(cdtp);
                        }
                        //

                        var lstcongdoan = new List<string>();
                        var sanpham = unitofwork.Context.SanPhams.FirstOrDefault(p => p.SanPhamID == i.SanPhamID);
                        if (sanpham.LoaiHinhSanXuat == 1)
                        {
                            lstcongdoan.Add("Cắt");
                        }
                        else
                        {
                            lstcongdoan.Add("Dệt mảnh");
                        }
                        lstcongdoan.Add("In, Thêu, Phụ kiện");
                        lstcongdoan.Add("May");
                        lstcongdoan.Add("Giặt");
                        var tpcuoiid = Guid.Empty;
                        var vitri = 0;
                        foreach (var item in lstcongdoan)
                        {
                            var hmsx = new DMHangMucSanXuat()
                            {
                                DMHangMucSanXuatID = Guid.NewGuid(),
                                GiaTriDinhMuc = 0,
                                HaoHut = 0,
                                IsActive = true,
                                SanPhamID = i.SanPhamID.GetValueOrDefault(),
                                ViTri = vitri,
                                MoTa = "",
                                ThoiGianHoanThanh = 0,

                            };
                            unitofwork.Context.DMHangMucSanXuats.Add(hmsx);
                            var dongdoanmd = new DMCongDoanHangMuc()
                            {
                                DMCongDoanHangMucID = Guid.NewGuid(),
                                DMHangMucSanXuatID = hmsx.DMHangMucSanXuatID,
                                IsActive = true,
                                TenCongDoan = item
                            };
                            unitofwork.Context.DMCongDoanHangMucs.Add(dongdoanmd);

                            var thanhpham = new ThanhPham()
                            {
                                AnhDaiDien = "",
                                DanhSachHinhAnhs = "",
                                DonGia = 0,
                                DonViID = Guid.Parse("B47164E6-C917-449A-91EA-0919F55376C6"),
                                IsActive = true,
                                MaThanhPham = sanpham.MaSanPham + vitri,
                                TenThanhPham = sanpham.TenSanPham + " - " + item,
                                ThanhPhamID = Guid.NewGuid(),
                                SanPhamID = sanpham.SanPhamID,
                            };
                            unitofwork.Context.ThanhPhams.Add(thanhpham);

                            if (vitri == 0)
                            {
                                foreach (var nn in nguyenlieu)
                                {
                                    foreach (var mm in listnl)
                                    {
                                        if (nn.LoaiVai == mm.LoaiVai)
                                        {
                                            var dinhnghiatp = new DinhNghiaThanhPham()
                                            {
                                                DinhNghiaThanhPhamID = Guid.NewGuid(),
                                                ID = mm.NguyenLieuID.GetValueOrDefault(),
                                                IsActive = true,
                                                LoaiHinh = 1,
                                                SoLuong = nn.SoLuong.GetValueOrDefault(),
                                                ThanhPhamID = thanhpham.ThanhPhamID,
                                                LoaiVai = nn.LoaiVai
                                            };
                                            unitofwork.Context.DinhNghiaThanhPhams.Add(dinhnghiatp);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var dinhnghiatp = new DinhNghiaThanhPham()
                                {
                                    DinhNghiaThanhPhamID = Guid.NewGuid(),
                                    ID = tpcuoiid,
                                    IsActive = true,
                                    LoaiHinh = 2,
                                    SoLuong = 1,
                                    ThanhPhamID = thanhpham.ThanhPhamID,
                                    LoaiVai = 1
                                };
                                unitofwork.Context.DinhNghiaThanhPhams.Add(dinhnghiatp);
                            }

                            var sp2 = new DMCongDoanThanhPham()
                            {
                                DMCongDoanHangMucID = dongdoanmd.DMCongDoanHangMucID,
                                DMCongDoanThanhPhamID = Guid.NewGuid(),
                                IsActive = true,
                                SoLuong = 1,
                                ThanhPhamID = thanhpham.ThanhPhamID
                            };
                            unitofwork.Context.DMCongDoanThanhPhams.Add(sp2);
                            tpcuoiid = thanhpham.ThanhPhamID;
                            vitri++;
                        }
                        //Tạo công đoạn cuối
                        var dnsp = new DinhNghiaSanPham()
                        {
                            DinhNghiaSanPhamID = Guid.NewGuid(),
                            IsActive = true,
                            SanPhamID = i.SanPhamID.GetValueOrDefault(),
                            SoLuongThanhPham = 1,
                            ThanhPhamID = tpcuoiid,
                            ThoiGianHoanThanh = 0
                        };
                        unitofwork.Context.DinhNghiaSanPhams.Add(dnsp);

                    }

                    unitofwork.Context.SaveChanges();

                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
            return 1;
        }
        public string CreateMaDeNghi(string maxDeNghi, string prefix)
        {
            DateTime now = DateTime.Now;
            string dd = "";
            if (now.Day < 10) dd = "0" + now.Day;
            else dd = now.Day.ToString();
            string mm = "";
            if (now.Month < 10) mm = "0" + now.Month;
            else mm = now.Month.ToString();

            string yyyy = now.Year.ToString();

            string finalDate = yyyy + mm + dd;


            int numberMax = 1;
            try
            {
                string n = maxDeNghi.Replace(prefix, "");
                n = n.Substring(finalDate.Length, n.Length - finalDate.Length);//=> number
                numberMax = Convert.ToInt32(n) + 1;
            }
            catch { }
            string max = string.Empty;
            if (numberMax < 10) { max = "000" + numberMax; }
            else if (numberMax < 100) { max = "00" + numberMax; }
            else if (numberMax < 1000) { max = "0" + numberMax; }
            return prefix + finalDate + max; ;
        }
    }
}
