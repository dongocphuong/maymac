﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DF.DBMapping.Models;
using System.Data;
using System.Data.SqlClient;
using DF.DataAccess.DBML;
using DF.DBMapping.ModelsExt;
using System.Web.Script.Serialization;
using System.Transactions;

namespace DF.DataAccess.Repository
{
    public interface INhanVienRepository : IRepository<NhanVien>
    {
        NhanVien GetByNhanVienID(Guid? nhanVienID);
        IQueryable<NhanVien> GetByName(string name);
        //Guid LayNhanVienIDbyUserName(string UserName);
        List<Web_Group_GetRuleTypeByUserNameResult> LayDSRuleTypeByUserName(string username);
        List<Web_NhanVien_LayDanhSachNhanVien_NewResult> LayDSNhanVienNew();
        bool XoaNhanVien(Guid NhanVienID);
        Web_NhanVien_LayDanhChiTietNhanVien_NewResult LayChiTietNhanVienNew(Guid NhanVienID);

    }

    public class NhanVienRepository : EFRepository<NhanVien>, INhanVienRepository
    {
        public NhanVienRepository(DbContext dbContext)
            : base(dbContext)
        {
        }
        protected DBMLDFDataContext db;
        protected UnitOfWork unitofwork;

       
       
        public NhanVien GetByNhanVienID(Guid? nhanVienID)
        {
            return DbSet.FirstOrDefault(o => o.NhanVienID == nhanVienID);
        }
        public IQueryable<NhanVien> GetByName(string name)
        {
            return DbSet.Where(o => o.TenNhanVien.Contains(name));
        }
     

        public List<Web_Group_GetRuleTypeByUserNameResult> LayDSRuleTypeByUserName(string username)
        {
            db = new DBMLDFDataContext();
            return db.Web_Group_GetRuleTypeByUserName(username).ToList();
        }
        public Web_NhanVien_LayDanhChiTietNhanVien_NewResult LayChiTietNhanVienNew(Guid NhanVienID)
        {
            db = new DBMLDFDataContext();
            return db.Web_NhanVien_LayDanhChiTietNhanVien_New(NhanVienID).FirstOrDefault();
        }
        public List<Web_NhanVien_LayDanhSachNhanVien_NewResult> LayDSNhanVienNew()
        {
            db = new DBMLDFDataContext();
            return db.Web_NhanVien_LayDanhSachNhanVien_New().ToList();
        }
        public bool XoaNhanVien(Guid NhanVienID)
        {
            string strcmd = "Update NhanVien set IsActive = 0 where NhanVienID = @NhanVienID";
            List<SqlParameter> lstparams = new List<SqlParameter>();
            lstparams.Add(new SqlParameter("NhanVienID", NhanVienID));
            SQLServerHepler.executeSQLNonQueryWithParam(strcmd, lstparams);
            return true;
        }
    }
}
