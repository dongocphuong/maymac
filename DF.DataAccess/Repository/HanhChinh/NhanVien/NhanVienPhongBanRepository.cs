﻿using DF.DataAccess.DBML;
using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace DF.DataAccess.Repository
{
    public interface INhanVienPhongBanRepository : IRepository<NhanVienPhongBan>
    {

    }
    public class NhanVienPhongBanRepository : EFRepository<NhanVienPhongBan>, INhanVienPhongBanRepository
    {
        public NhanVienPhongBanRepository(DbContext dbContext)
            : base(dbContext)
        {
        }
        protected DBMLDFDataContext db;
        protected UnitOfWork unitofwork;
    }
}
