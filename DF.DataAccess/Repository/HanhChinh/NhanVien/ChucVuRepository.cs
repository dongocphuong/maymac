﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace DF.DataAccess.Repository
{
    public interface IChucVuRepository : IRepository<ChucVu>
    {
    }
    public class ChucVuRepository : EFRepository<ChucVu>, IChucVuRepository
    {
        public ChucVuRepository(DbContext dbContext) : base(dbContext)
        {

        }
    }
}
