﻿using DF.DataAccess.DBML;
using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace DF.DataAccess.Repository
{
    public interface IPhongBanRepository : IRepository<PhongBan>
    {

    }
    public class PhongBanRepository : EFRepository<PhongBan>, IPhongBanRepository
    {
        public PhongBanRepository(DbContext dbContext)
            : base(dbContext)
        {
        }
        protected DBMLDFDataContext db;
        protected UnitOfWork unitofwork;
    }
}
