﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using DF.DataAccess.DBML;
using System.Data.SqlClient;

namespace DF.DataAccess.Repository
{
    public interface IPhanQuyenRepository : IRepository<tblGroupUser>
    {
        void delPhanQuyen(string UserID);
        List<Web_PhanQuyen_GetDanhMucPhanQuyenResult> GetDanhMucPhanQuyen(int LoaiHinh);
        List<Web_PhanQuyen_DMQuyenParentResult> GetDMPhanQuyenParent();
        List<Web_PhanQuyen_GetPhanQuyenByUserResult> GetPhanQuyenByUser(Guid NhanVienID, int LoaiHinh);
        bool delPhanQuyenNew(Guid UserID, int LoaiHinh);
        List<Web_PhanQuyen_GenMenuByUserIDResult> GetMenuByUserID(Guid UserID);
        bool delQuyen(Guid GroupGuiD);

    }
    class PhanQuyenRepository: EFRepository<tblGroupUser>, IPhanQuyenRepository
    {
        public PhanQuyenRepository(DbContext dbContext)
            : base(dbContext)
        {

        }

        public bool delPhanQuyenNew(Guid UserID, int LoaiHinh)
        {
            string sqlcomand = @"delete from tblGroupUser where UserGuid = @UserID and GroupGuid in (select gr.GroupGuid from tblGroup gr where gr.LoaiHinh = @LoaiHinh)";
            List<SqlParameter> lstparams = new List<SqlParameter>();
            lstparams.Add(new SqlParameter("UserID", UserID));
            lstparams.Add(new SqlParameter("LoaiHinh", LoaiHinh));
            SQLServerHepler.executeSQLNonQueryWithParam(sqlcomand, lstparams);
            return true;
        }
        public void delPhanQuyen(string UserID)
        {
            string strcmd = "delete from tblGroupUser where UserGuid = '" + UserID + "'";
            SQLServerHepler.executeSQLNonQuery(strcmd); 
        }

        public List<Web_PhanQuyen_GetDanhMucPhanQuyenResult> GetDanhMucPhanQuyen(int LoaiHinh)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            return db.Web_PhanQuyen_GetDanhMucPhanQuyen(LoaiHinh).ToList();

        }
        public List<Web_PhanQuyen_DMQuyenParentResult> GetDMPhanQuyenParent()
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            return db.Web_PhanQuyen_DMQuyenParent().ToList();
        }

        public List<Web_PhanQuyen_GetPhanQuyenByUserResult> GetPhanQuyenByUser(Guid NhanVienID, int LoaiHinh)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            return db.Web_PhanQuyen_GetPhanQuyenByUser(NhanVienID, LoaiHinh).ToList();
        }
        public List<Web_PhanQuyen_GenMenuByUserIDResult> GetMenuByUserID(Guid UserID)
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            return db.Web_PhanQuyen_GenMenuByUserID(UserID).ToList();
        }

        public bool delQuyen(Guid GroupGuiD)
        {
            string sqlcomand = @"delete from tblGroup where GroupGuid = @GroupGuiD";
            List<SqlParameter> lstparams = new List<SqlParameter>();
            lstparams.Add(new SqlParameter("GroupGuiD", GroupGuiD));
            SQLServerHepler.executeSQLNonQueryWithParam(sqlcomand, lstparams);
            return true;
        }
    }
}
