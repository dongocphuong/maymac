﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DF.DataAccess.Repository
{
    public class DFSException : Exception
    {
        public DFSException(string message)
            : base(message)
        {

        }
    }
}
