﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu;

namespace DF.DataAccess.Repository
{
    public interface IQuanLySanXuatRepository : IRepository<QuanLySanXuat>
    {
      SanXuatModel SanXuatSanPhamChuaBatDau(string SanPhamID, string DonHangID);
      SanXuatModel SanXuatSanPhamDangSanXuat(string SanPhamID, string DonHangID);
      bool BatDauSanXuat(string SanPhamID, string DonHangID,Guid NhanVienID);
      bool DoiTrangThaiCongDoan(string HangMucSanXuatID, int TrangThai, string LyDoTamDung="");
      bool DoiTrangThaiCongDoanCuoi(string SanPhamID, string DonHangID, int TrangThai,string LyDoTamDung="");
      bool SanXuatThanhPham(string DonHangID, string SanPhamID, string HangMucSanXuatID, string datanhap, string dataxuat, Guid NhanVienID);
      bool SanXuatThanhPhamCongDoan(string DonHangID, string SanPhamID, int SoLuong, string dataxuat, Guid NhanVienID);
      PhieuSanXuatThanhPhamModel SanXuatThanhPhamChuaBatDau(string ThanhPhamID, string DonHangID);
      PhieuSanXuatThanhPhamModel SanXuatThanhPhamDangSanXuat(string ThanhPhamID, string DonHangID);
      bool BatDauSanXuatThanhPham(string ThanhPhamID, string DonHangID);
      bool SanXuatThanhPham2(string DonHangID, string ThanhPhamID, int SoLuong, string dataxuat, Guid NhanVienID);
      SanXuatModel SanXuatSanPhamDangSanXuat2(string SanPhamID, string DonHangID);

    }
    public class QuanLySanXuatRepository : EFRepository<QuanLySanXuat>, IQuanLySanXuatRepository
    {
        public QuanLySanXuatRepository(DbContext dbContext) : base(dbContext) { }
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        public SanXuatModel SanXuatSanPhamChuaBatDau(string SanPhamID, string DonHangID)
        {
            var sanphamid = Guid.Parse(SanPhamID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new SanXuatModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p=>p.DonHangID==donhangid);
            var doitac = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == donhang.DoiTacID);
            var sanpham = unitofwork.Context.SanPhams.FirstOrDefault(p=>p.SanPhamID==sanphamid);
            var chitiet = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.SanPhamID == sanphamid && p.DonHangID == donhangid);

            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);
            var khosanpham  = unitofwork.KhoSanPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;
            xsmodel.TenKhachHang = doitac.TenDoiTac;

            xsmodel.Size = sanpham.Size;
            xsmodel.GioiTinh = sanpham.GioiTinh == 1 ? "Nam" : "Nữ";
            xsmodel.Loai = sanpham.Loai == 1 ? "Người lớn" : "Trẻ con";
            xsmodel.ThoiGian = donhang.ThoiGian.GetValueOrDefault().ToString("dd/MM/yyyy");
            xsmodel.TiLeSize = sanpham.TiLeSize;
            xsmodel.SizeMauGoc = sanpham.SizeMauGoc;


            xsmodel.TenSanPham = sanpham.TenSanPham;
            xsmodel.MaSanPham = sanpham.MaSanPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;
            xsmodel.SanPhamID = sanphamid;
            
            xsmodel.SoLuong = chitiet.SoLuong.GetValueOrDefault();
            xsmodel.GiaTriDonHang = chitiet.SoLuong.GetValueOrDefault() * chitiet.DonGia.GetValueOrDefault();

            //Lấy công đoạn
            xsmodel.lstCongDoan = new List<CongDoanModel>();
            var cdsanpham = (from a in unitofwork.Context.DMHangMucSanXuats
                             join b in unitofwork.Context.DMCongDoanHangMucs on a.DMHangMucSanXuatID equals b.DMHangMucSanXuatID
                             where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanphamid
                             select new { 
                                a.GiaTriDinhMuc,
                                a.ThoiGianHoanThanh,
                                b.TenCongDoan,
                                a.ViTri,
                                a.DMHangMucSanXuatID,
                                a.MoTa
                             }).ToList();
            var lstthanhpham = db.Web_DinhNghiaSanPham_LayDanhSachThanhPham(sanphamid).ToList(); 
            foreach (var cd in cdsanpham)
            {
                var lstThanhPham = lstthanhpham.Where(p2 => p2.DMHangMucSanXuatID == cd.DMHangMucSanXuatID).Select(p2 => new CongDoanThanhPhamModel
                    {
                        DonVi = p2.TenDonVi,
                        MaThanhPham = p2.MaThanhPham,
                        TenThanhPham = p2.TenThanhPham,
                        SoLuongTong = p2.SoLuong*chitiet.SoLuong.GetValueOrDefault(),
                        AnhDaiDien = p2.AnhDaiDien,
                        LoaiHinh = 0,
                        SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID)!=null?khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID).SoLuongTon.GetValueOrDefault():0,
                        lstNguyenLieu = db.Web_ThanhPham_DinhNghiaThanhPham(p2.ThanhPhamID).Select(p => new CongDoanNguyenLieuModel() { 
                            AnhDaiDien =p.AnhDaiDien,
                            DonVi= p.TenDonVi,
                            LoaiHinh=p.LoaiHinh,
                            MaNguyenLieu=p.MaNguyenLieu,
                            SoLuong = p.SoLuong * p2.SoLuong * chitiet.SoLuong.GetValueOrDefault(),
                            SoLuongDaCo = (p.LoaiHinh == 1 ? (khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID)!=null?khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID).SoLuongTon:0) : (khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID)!=null?khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID).SoLuongTon.GetValueOrDefault():0)),
                            TenNguyenLieu =p.TenNguyenLieu,
                            LoaiVai = p.LoaiVai
                        }).ToList()
                    }).ToList();

                var ncd = new CongDoanModel(){
                    TenCongDoan =cd.TenCongDoan,
                    ThoiGianDinhMuc=cd.ThoiGianHoanThanh.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                    ThoiGianHienTai = 0,
                    lstThanhPham = lstThanhPham,
                    ViTri =cd.ViTri,
                    MoTa = cd.MoTa,
                    HangMucSanXuatID = cd.DMHangMucSanXuatID,
                    TrangThai=1
                };
                xsmodel.lstCongDoan.Add(ncd);
            }
            // Lấy công đoạn cuối
            
            var dinhnghiasanpham = unitofwork.Context.DinhNghiaSanPhams.Where(p => p.IsActive == true && p.SanPhamID == sanphamid);
            if (dinhnghiasanpham.Any())
            {
                var lstCongDoan = db.Web_DinhNghiaSanPham_LayDanhSachDinhNghia(sanphamid).ToList();
                var cdc = new CongDoanCuoiModel()
                {
                    SoLuong = chitiet.SoLuong.GetValueOrDefault(),
                    SoLuongDaCo = khosanpham.FirstOrDefault(p => p.SanPhamID == sanphamid)!=null?khosanpham.FirstOrDefault(p => p.SanPhamID == sanphamid).SoLuongTon.GetValueOrDefault():0,
                    ThoiGianDinhMuc = dinhnghiasanpham.FirstOrDefault().ThoiGianHoanThanh.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                    ThoiGianHienTai = 0,
                    MoTa = dinhnghiasanpham.FirstOrDefault().MoTa,
                    lstThanhPham = lstCongDoan.Select(p => new CongDoanThanhPhamModel
                    {
                        AnhDaiDien = p.TenThanhPham,
                        DonVi = p.TenDonVi,
                        LoaiHinh = 0,
                        MaThanhPham = p.MaThanhPham,
                        SoLuongTong = p.SoLuongThanhPham * chitiet.SoLuong.GetValueOrDefault(),
                        SoLuongDaHoanThien =  khothanhphams.FirstOrDefault(p2 => p2.ThanhPhamID == p.ThanhPhamID)!=null?khothanhphams.FirstOrDefault(p2 => p2.ThanhPhamID == p.ThanhPhamID).SoLuongTon.GetValueOrDefault():0,
                        TenThanhPham = p.TenThanhPham,
                        lstNguyenLieu = db.Web_ThanhPham_DinhNghiaThanhPham(p.ThanhPhamID).Select(p3 => new CongDoanNguyenLieuModel {
                            AnhDaiDien =p3.AnhDaiDien,
                            DonVi =p3.TenDonVi,
                            LoaiHinh =p3.LoaiHinh ,
                            MaNguyenLieu= p3.MaNguyenLieu,
                            SoLuong=p3.SoLuong,
                            SoLuongDaCo = (p3.LoaiHinh == 1 ? (khonguyenlieus.FirstOrDefault(p4 => p4.NguyenLieuID == p3.NguyenLieuID)!=null?khonguyenlieus.FirstOrDefault(p4 => p4.NguyenLieuID == p3.NguyenLieuID).SoLuongTon:0) : (khothanhphams.FirstOrDefault(p4 => p4.ThanhPhamID == p3.NguyenLieuID)!=null?khothanhphams.FirstOrDefault(p4 => p4.ThanhPhamID == p3.NguyenLieuID).SoLuongTon.GetValueOrDefault():0)),
                        }).ToList(),
                    }).ToList()
                };
                xsmodel.congDoanCuoi = cdc;
            }


            return xsmodel;
        }
        public SanXuatModel SanXuatSanPhamDangSanXuat(string SanPhamID, string DonHangID)
        {
            var sanphamid = Guid.Parse(SanPhamID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new SanXuatModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
            var qlsanxuat = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.DonHangID == donhangid && sanphamid==p.SanPhamID);
            var doitac = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == donhang.DoiTacID);
            var sanpham = unitofwork.Context.SanPhams.FirstOrDefault(p => p.SanPhamID == sanphamid);
            var chitiet = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.SanPhamID == sanphamid && p.DonHangID == donhangid);

            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);
            var khosanpham = unitofwork.KhoSanPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;
            xsmodel.TenKhachHang = doitac.TenDoiTac;



            xsmodel.Size = sanpham.Size;
            xsmodel.GioiTinh = sanpham.GioiTinh==1?"Nam":"Nữ";
            xsmodel.Loai = sanpham.Loai==1?"Người lớn":"Trẻ con";
            xsmodel.ThoiGian = donhang.ThoiGian.GetValueOrDefault().ToString("dd/MM/yyyy");
            xsmodel.TiLeSize = sanpham.TiLeSize;
            xsmodel.SizeMauGoc = sanpham.SizeMauGoc;

            xsmodel.TenSanPham = sanpham.TenSanPham;
            xsmodel.MaSanPham = sanpham.MaSanPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;
            xsmodel.SanPhamID = sanphamid;

            xsmodel.SoLuong = chitiet.SoLuong.GetValueOrDefault();
            xsmodel.GiaTriDonHang = chitiet.SoLuong.GetValueOrDefault() * chitiet.DonGia.GetValueOrDefault();

            //Lấy công đoạn
            xsmodel.lstCongDoan = new List<CongDoanModel>();
            var cdsanpham = (from a in unitofwork.Context.HangMucSanXuats
                             join b in unitofwork.Context.CongDoanHangMucs on a.HangMucSanXuatID equals b.HangMucSanXuatID
                             join c in unitofwork.Context.QuanLySanXuats on a.QuanLySanXuatID equals c.QuanLySanXuatID
                             where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanphamid && c.QuanLySanXuatID == qlsanxuat.QuanLySanXuatID
                             select new
                             {
                                 a.GiaTriDinhMuc,
                                 a.ThoiGianHoanThanh,
                                 a.ViTri,
                                 b.TenCongDoan,
                                 a.HangMucSanXuatID,
                                 b.TrangThai,
                                 b.ThoiGianBatDau,
                                 b.LyDoTamDung,
                             }).ToList();
            var cdsanpham2 = (from a in unitofwork.Context.DMHangMucSanXuats
                             join b in unitofwork.Context.DMCongDoanHangMucs on a.DMHangMucSanXuatID equals b.DMHangMucSanXuatID
                             where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanphamid
                             select new
                             {
                                 a.GiaTriDinhMuc,
                                 a.ThoiGianHoanThanh,
                                 b.TenCongDoan,
                                 a.ViTri,
                                 a.DMHangMucSanXuatID,
                                 a.MoTa
                             }).ToList();
            var lstthanhpham = db.Web_QuanLySanXuat_LayDanhSachThanhPham(sanphamid).ToList();
            foreach (var cd in cdsanpham)
            {
                var lstThanhPham = lstthanhpham.Where(p2 => p2.HangMucSanXuatID == cd.HangMucSanXuatID).Select(p2 => new CongDoanThanhPhamModel
                {
                    DonVi = p2.TenDonVi,
                    MaThanhPham = p2.MaThanhPham,
                    TenThanhPham = p2.TenThanhPham,
                    SoLuongTong = p2.SoLuong.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                    AnhDaiDien = p2.AnhDaiDien,
                    ThanhPhamID =p2.ThanhPhamID,
                    LoaiHinh = 0,
                    SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID) != null ? khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID).SoLuongNhap  : 0,
                    lstNguyenLieu = db.Web_QuanLySanXuat_DinhNghiaThanhPham(p2.ThanhPhamID).Where(p=>p.CongDoanThanhPhamID==p2.CongDoanThanhPhamID).Select(p => new CongDoanNguyenLieuModel()
                    {
                        AnhDaiDien = p.AnhDaiDien,
                        DonVi = p.TenDonVi,
                        LoaiHinh = p.LoaiHinh.GetValueOrDefault(),
                        MaNguyenLieu = p.MaNguyenLieu,
                        SoLuong = p.SoLuong.GetValueOrDefault() * p2.SoLuong.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                        SoLuongDaCo = (p.LoaiHinh == 1 ? (khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID) != null ? khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID).SoLuongTon : 0) : (khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID) != null ? khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID).SoLuongTon.GetValueOrDefault()  : 0)),
                        TenNguyenLieu = p.TenNguyenLieu,
                        NguyenLieuID = p.NguyenLieuID,
                        LoaiVai = p.LoaiVai
                    }).ToList()
                }).ToList();

                var ncd = new CongDoanModel()
                {
                    TenCongDoan = cd.TenCongDoan,
                    ThoiGianDinhMuc = cd.ThoiGianHoanThanh.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                    ThoiGianHienTai = cd.ThoiGianBatDau.HasValue ? (DateTime.Now - cd.ThoiGianBatDau.GetValueOrDefault()).TotalHours : 0,
                    lstThanhPham = lstThanhPham,
                    ViTri =cd.ViTri,
                    LyDoTamDung= cd.LyDoTamDung,
                    MoTa = cdsanpham2.FirstOrDefault(p=>p.ViTri==cd.ViTri)!=null?cdsanpham2.FirstOrDefault(p=>p.ViTri==cd.ViTri).MoTa:"",
                    HangMucSanXuatID =cd.HangMucSanXuatID,
                    TrangThai = cd.TrangThai.GetValueOrDefault()
                };
                xsmodel.lstCongDoan.Add(ncd);
            }
            // Lấy công đoạn cuối

            var dinhnghiasanpham = unitofwork.Context.DinhNghiaSanPhams.Where(p => p.IsActive == true && p.SanPhamID == sanphamid);
            if (dinhnghiasanpham.Any())
            {
                var lstCongDoan = db.Web_QuanLySanXuat_LayDanhSachDinhNghiaSanPham(sanphamid).Where(p=>p.QuanLySanXuatID==qlsanxuat.QuanLySanXuatID).ToList();
                var thoigianht = qlsanxuat.ThoiGianBatDauCongDoanCuoi.HasValue ? (DateTime.Now-qlsanxuat.ThoiGianBatDauCongDoanCuoi.GetValueOrDefault()).TotalHours : 0;
                var cdc = new CongDoanCuoiModel()
                {
                    SoLuong = chitiet.SoLuong.GetValueOrDefault(),
                    SoLuongDaCo = khosanpham.FirstOrDefault(p => p.SanPhamID == sanphamid) != null ? khosanpham.FirstOrDefault(p => p.SanPhamID == sanphamid).SoLuongNhap  : 0,
                    ThoiGianDinhMuc = dinhnghiasanpham.FirstOrDefault().ThoiGianHoanThanh.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                    ThoiGianHienTai = thoigianht,
                    TrangThai =qlsanxuat.TrangThaiCongDoanCuoi,
                    MoTa =dinhnghiasanpham.FirstOrDefault().MoTa,
                    LyDoTamDung = qlsanxuat.LyDoTamDungCongDoanCuoi,
                    lstThanhPham = lstCongDoan.Select(p => new CongDoanThanhPhamModel
                    {
                        AnhDaiDien = p.AnhDaiDien,
                        DonVi = p.TenDonVi,
                        LoaiHinh = 0,
                        MaThanhPham = p.MaThanhPham,
                        ThanhPhamID =p.ThanhPhamID,
                        SoLuongTong = p.SoLuong.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                        SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p2 => p2.ThanhPhamID == p.ThanhPhamID) != null ? khothanhphams.FirstOrDefault(p2 => p2.ThanhPhamID == p.ThanhPhamID).SoLuongTon.GetValueOrDefault() : 0,
                        TenThanhPham = p.TenThanhPham,
                        lstNguyenLieu = new List<CongDoanNguyenLieuModel>()
                    }).ToList()
                };
                xsmodel.congDoanCuoi = cdc;
            }


            return xsmodel;
        }
        public bool BatDauSanXuat(string SanPhamID, string DonHangID,Guid NhanVienID)
        {
            try
            {
                var sanphamid = Guid.Parse(SanPhamID);
                var donhangid = Guid.Parse(DonHangID);
                unitofwork = new UnitOfWork();
                db = new DBMLDFDataContext();
                var lstnguyenlieu = db.Web_NguyenLieu_TonKho_DonHangCanTheoSanPham(donhangid, sanphamid).ToList();
                unitofwork = new UnitOfWork();
                Guid PhieuXuatVatTuID = Guid.NewGuid();
                Guid PhieuNhapVatTuID = Guid.NewGuid();
                var maxDeNghi = unitofwork.Context.PhieuXuatNguyenLieux.Max(p => p.MaPhieuXuat);
                string maDeNghi = CreateMaDeNghi(maxDeNghi, "PXNL");
                var maxDeNghi2 = unitofwork.Context.PhieuNhapNguyenLieux.Max(p => p.MaPhieuNhap);
                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNNL");

                Guid deNghiCongTrinhId = Guid.NewGuid();

                var phieuxuatnguyenlieu = new PhieuXuatNguyenLieu()
                {
                    DonHangID = Guid.Parse(Define.CONGTRINHTONG),
                    DanhSachHinhAnhs = "",
                    IsActive = true,
                    NguoiTaoID = NhanVienID,
                    PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                    ThoiGian = DateTime.Now,
                    DonHangNhanID = donhangid,
                    MaPhieuXuat = maDeNghi,
                    LoaiHinh = 1
                };
                unitofwork.Context.PhieuXuatNguyenLieux.Add(phieuxuatnguyenlieu);
                var phieunhapnguyenlieu = new PhieuNhapNguyenLieu()
                {
                    DonHangID = donhangid,
                    DanhSachHinhAnhs = "",
                    IsActive = true,
                    NguoiTaoID = NhanVienID,
                    PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                    ThoiGian = DateTime.Now,
                    MaPhieuNhap = maDeNghi2,
                    PhieuNhapNguyenLieuID = PhieuNhapVatTuID,
                };
                unitofwork.Context.PhieuNhapNguyenLieux.Add(phieunhapnguyenlieu);


                foreach (var it in lstnguyenlieu)
                {
                    var chitietxuat = new ChiTietPhieuXuatNguyenLieu()
                    {
                        ChiTietPhieuXuatNguyenLieuID = Guid.NewGuid(),
                        IsActive = true,
                        NguyenLieuID = it.NguyenLieuID.GetValueOrDefault(),
                        PhieuXuatNguyenLieu = PhieuXuatVatTuID,
                        SoLuong = it.SoLuongTon >= it.SoLuongCan ? it.SoLuongCan : it.SoLuongTon,
                    };
                    unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Add(chitietxuat);

                    var chiTietNhap = new ChiTietPhieuNhapNguyenLieu()
                    {
                        ChiTietPhieuNhapNguyenLieuID = Guid.NewGuid(),
                        GiaNhap = it.DonGia.GetValueOrDefault(),
                        PhieuNhapNguyenLieuID = PhieuNhapVatTuID,
                        SoLuong = it.SoLuongTon >= it.SoLuongCan ? it.SoLuongCan.GetValueOrDefault() : it.SoLuongTon.GetValueOrDefault(),
                        NguyenLieuID = it.NguyenLieuID.GetValueOrDefault(),
                        IsActive = true,
                    };
                    unitofwork.Context.ChiTietPhieuNhapNguyenLieux.Add(chiTietNhap);
                }
                var cdsanpham = (from a in unitofwork.Context.DMHangMucSanXuats
                             join b in unitofwork.Context.DMCongDoanHangMucs on a.DMHangMucSanXuatID equals b.DMHangMucSanXuatID
                             where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanphamid
                             select new { 
                                a.GiaTriDinhMuc,
                                a.ThoiGianHoanThanh,
                                b.TenCongDoan,
                                a.DMHangMucSanXuatID,
                                a.ViTri,
                                b.DMCongDoanHangMucID,
                                a.HaoHut
                             }).ToList();
                var quanlysanxuat = new QuanLySanXuat()
                {
                    DonHangID = donhangid,
                    IsActive = true,
                    LoaiHinh = 1,
                    QuanLySanXuatID = Guid.NewGuid(),
                    SanPhamID = sanphamid,
                    TenCongDoan = cdsanpham.OrderBy(p=>p.ViTri.GetValueOrDefault()).FirstOrDefault().TenCongDoan,
                    ThoiGian = DateTime.Now,
                    TrangThaiCongDoanCuoi = 1
                };
                unitofwork.Context.QuanLySanXuats.Add(quanlysanxuat);
                foreach (var cd in cdsanpham)
                {
                    var hangmucsanxuat = new HangMucSanXuat()
                    {
                        GiaTriDinhMuc = cd.GiaTriDinhMuc,
                        HangMucSanXuatID = Guid.NewGuid(),
                        HaoHut = cd.HaoHut,
                        IsActive = true,
                        ViTri= cd.ViTri,
                        QuanLySanXuatID = quanlysanxuat.QuanLySanXuatID,
                        SanPhamID=sanphamid,
                        ThoiGianHoanThanh = cd.ThoiGianHoanThanh.GetValueOrDefault(),
                    };
                    unitofwork.Context.HangMucSanXuats.Add(hangmucsanxuat);
                    var congdoan = new CongDoanHangMuc()
                    {
                        CongDoanHangMucID = Guid.NewGuid(),
                        HangMucSanXuatID = hangmucsanxuat.HangMucSanXuatID,
                        IsActive = true,
                        LyDoTamDung = "",
                        TenCongDoan = cd.TenCongDoan,
                        ThoiGianBatDau = DateTime.Now,
                        TrangThai = cdsanpham.OrderBy(p => p.ViTri.GetValueOrDefault()).FirstOrDefault().DMHangMucSanXuatID==cd.DMHangMucSanXuatID?2:1
                    };
                    unitofwork.Context.CongDoanHangMucs.Add(congdoan);
                    var lstthanhpham = db.Web_DinhNghiaSanPham_LayDanhSachThanhPham(sanphamid).Where(p=>p.DMHangMucSanXuatID==cd.DMHangMucSanXuatID).ToList();
                    foreach (var thanhpham in lstthanhpham)
                    {
                        var lstcongthuc = db.Web_ThanhPham_DinhNghiaThanhPham(thanhpham.ThanhPhamID).ToList();
                        var tp = new CongDoanThanhPham()
                        {
                            CongDoanHangMucID = congdoan.CongDoanHangMucID,
                            CongDoanThanhPhamID = Guid.NewGuid(),
                            IsActive=true,
                            SoLuong=thanhpham.SoLuong,
                            ThanhPhamID = thanhpham.ThanhPhamID
                        };
                        unitofwork.Context.CongDoanThanhPhams.Add(tp);
                        foreach (var item in lstcongthuc)
                        {
                            var dcdt = new CongDoanThanhPhamDinhNghia()
                            {
                                CongDoanThanhPhamDinhNghiaID = Guid.NewGuid(),
                                CongDoanThanhPhamID = tp.CongDoanThanhPhamID,
                                ID = item.NguyenLieuID,
                                LoaiHinh = item.LoaiHinh,
                                SoLuong = item.SoLuong,
                                ThanhPhamID = thanhpham.ThanhPhamID
                            };
                            unitofwork.Context.CongDoanThanhPhamDinhNghias.Add(dcdt);
                        }
                    }

                    

                }
                var dinhnghiasanpham = unitofwork.Context.DinhNghiaSanPhams.Where(p => p.IsActive == true && p.SanPhamID == sanphamid);
                foreach (var item in dinhnghiasanpham)
                {
                    var dnsp = new CongDoanSanPhamDinhNghia()
                    {
                        CongDoanSanPhamDinhNghiaID = Guid.NewGuid(),
                        QuanLySanXuatID = quanlysanxuat.QuanLySanXuatID,
                        SoLuong = item.SoLuongThanhPham,
                        ThanhPhamID = item.ThanhPhamID
                    };
                    unitofwork.Context.CongDoanSanPhamDinhNghias.Add(dnsp);
                }
                var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
                donhang.TrangThai = 2;
                var ctsp = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.DonHangID == donhangid && p.SanPhamID == sanphamid);
                ctsp.TrangThai = 2;
                db.Dispose();
                unitofwork.Save();
                return true;
            }catch(Exception ex){
                return false;
            }
        }



        public bool DoiTrangThaiCongDoan(string HangMucSanXuatID, int TrangThai, string LyDoTamDung="")
        {
            try
            {
                var hangmucsanxuatid = Guid.Parse(HangMucSanXuatID);
                unitofwork = new UnitOfWork();
                var congdoan = unitofwork.Context.CongDoanHangMucs.FirstOrDefault(p => p.HangMucSanXuatID == hangmucsanxuatid);
                var hangmucsanxuat = unitofwork.Context.HangMucSanXuats.FirstOrDefault(p => p.HangMucSanXuatID == hangmucsanxuatid);
                var qlsanxuat = (from a in unitofwork.Context.HangMucSanXuats
                                 join b in unitofwork.Context.QuanLySanXuats on a.QuanLySanXuatID equals b.QuanLySanXuatID
                                 where a.HangMucSanXuatID == hangmucsanxuatid
                                 select b).FirstOrDefault();
                congdoan.TrangThai = TrangThai;
                congdoan.LyDoTamDung = LyDoTamDung;
                unitofwork.Save();
                if (qlsanxuat != null)
                {
                    var ctdonhang = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.SanPhamID == qlsanxuat.SanPhamID && p.DonHangID == qlsanxuat.DonHangID);
                    if (TrangThai == 3)
                    {
                        
                        if (ctdonhang != null)
                        {
                            ctdonhang.TenCongDoan = congdoan.TenCongDoan;
                            var congdoansau = (from a in unitofwork.Context.CongDoanHangMucs
                                               join b in unitofwork.Context.HangMucSanXuats on a.HangMucSanXuatID equals b.HangMucSanXuatID
                                               where b.ViTri > hangmucsanxuat.ViTri && a.HangMucSanXuatID != hangmucsanxuatid && b.QuanLySanXuatID == qlsanxuat.QuanLySanXuatID
                                               orderby b.ViTri ascending
                                               select a).FirstOrDefault();
                            if (congdoansau != null)
                            {
                                congdoansau.TrangThai = 2;
                                
                            }
                            else
                            {
                                DoiTrangThaiCongDoanCuoi(qlsanxuat.SanPhamID.ToString(), qlsanxuat.DonHangID.ToString(), 2);
                            }
                        }
                    }
                    if (TrangThai == 2)
                    {
                        if (congdoan != null)
                        {
                            congdoan.ThoiGianBatDau = DateTime.Now;

                            if (ctdonhang != null)
                            {
                                ctdonhang.TenCongDoan = congdoan.TenCongDoan;
                                ctdonhang.TrangThai = 2;
                            }
                        }
                    }
                    if (TrangThai == 1)
                    {
                        if (congdoan != null)
                        {
                            congdoan.ThoiGianBatDau = null;
                        }
                    }
                }
                
                congdoan.TrangThai=TrangThai;
                unitofwork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DoiTrangThaiCongDoanCuoi(string SanPhamID, string DonHangID, int TrangThai, string LyDoTamDung="")
        {
            try
            {
                var sanphamid = Guid.Parse(SanPhamID);
                var donhang = Guid.Parse(DonHangID);
                unitofwork = new UnitOfWork();
                var congdoan = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.DonHangID == donhang && p.SanPhamID==sanphamid);
                var ctdonhang = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.DonHangID == donhang && p.SanPhamID == sanphamid);
                var ctdhs = unitofwork.Context.ChiTietDonHangs.Where(p => p.DonHangID == donhang);
                var donhang2 = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhang);
                if (TrangThai == 2)
                {
                    congdoan.ThoiGianBatDauCongDoanCuoi = DateTime.Now;
                    ctdonhang.TenCongDoan = "Ghép";
                }
                if (TrangThai == 3 || TrangThai==5)
                {
                    ctdonhang.TrangThai = TrangThai;
                    if (TrangThai == 3)
                    {
                        if (ctdhs.Count(p => p.ChiTietDonHangID != ctdonhang.ChiTietDonHangID && p.TrangThai == 3) == (ctdhs.Count() - 1))
                        {
                            donhang2.TrangThai = 3;
                        }
                    }
                }
                if (TrangThai == 1)
                {
                    if (congdoan != null)
                    {
                        congdoan.ThoiGianBatDauCongDoanCuoi = null;
                    }
                }
                congdoan.TrangThaiCongDoanCuoi = TrangThai;
                congdoan.LyDoTamDungCongDoanCuoi = LyDoTamDung;
                unitofwork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SanXuatThanhPham(string DonHangID,string SanPhamID,string HangMucSanXuatID, string datanhap,string dataxuat,Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var lstthanhpham = new JavaScriptSerializer().Deserialize<List<SanXuatThanhPhamModel>>(datanhap);
                var lstnguyenlieu = new JavaScriptSerializer().Deserialize<List<SanXuatNguyenLieuModel>>(dataxuat);
                var sanphamid = Guid.Parse(SanPhamID);
                var donhangid = Guid.Parse(DonHangID);
                var hmsxid = Guid.Parse(HangMucSanXuatID);
                var hmsx = unitofwork.Context.HangMucSanXuats.FirstOrDefault(p=>p.HangMucSanXuatID==hmsxid);
                var qlsanxuat = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p=>p.DonHangID==donhangid && p.SanPhamID==sanphamid);
                var ctdn = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.DonHangID == donhangid && p.SanPhamID == sanphamid);
                // Tạo phiếu nhập
                var maxDeNghi2 = unitofwork.Context.PhieuNhapThanhPhams.Max(p => p.MaPhieuNhap);
                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNTP");

                var phieunhaptp = new PhieuNhapThanhPham()
                {
                    DanhSachHinhAnhs ="",
                    DoiTacID=null,
                    DonHangID = qlsanxuat.DonHangID,
                    IsActive=true,
                    MaPhieuNhap=maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuNhapThanhPhamID = Guid.NewGuid(),
                    ThoiGian=DateTime.Now
                };
                unitofwork.Context.PhieuNhapThanhPhams.Add(phieunhaptp);
                foreach (var item in lstthanhpham)
                {
                    
                    var ct = new ChiTietPhieuNhapThanhPham()
                    {
                        ChiTietPhieuNhapThanhPhamID = Guid.NewGuid(),
                        GiaNhap = 0,
                        IsActive = true,
                        PhieuNhapThanhPhamID = phieunhaptp.PhieuNhapThanhPhamID,
                        SoLuong = item.SoLuongSanXuat.GetValueOrDefault(),
                        ThanhPhamID = item.ThanhPhamID.GetValueOrDefault(),
                        TinhTrang = 1
                    };
                    unitofwork.Context.ChiTietPhieuNhapThanhPhams.Add(ct);
                   
                }
                //Tao phiếu xuất
                 
                 if (lstnguyenlieu.Any(p => p.LoaiHinh == 1))
                 {
                     maxDeNghi2 = unitofwork.Context.PhieuXuatNguyenLieux.Max(p => p.MaPhieuXuat);
                     maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXNL");
                     var px = new PhieuXuatNguyenLieu()
                     {
                         DonHangID = qlsanxuat.DonHangID,
                         DanhSachHinhAnhs = "",
                         DoiTacID = null,
                         IsActive = true,
                         LoaiHinh = 2,
                         MaPhieuXuat = maDeNghi2,
                         NguoiTaoID = NhanVienID,
                         PhieuXuatNguyenLieuID = Guid.NewGuid(),
                         ThoiGian = DateTime.Now
                     };
                     unitofwork.Context.PhieuXuatNguyenLieux.Add(px);
                     foreach (var item in lstnguyenlieu.Where(p => p.LoaiHinh == 1))
                     {
                         var ct = new ChiTietPhieuXuatNguyenLieu()
                         {
                             ChiTietPhieuXuatNguyenLieuID = Guid.NewGuid(),
                             IsActive = true,
                             PhieuXuatNguyenLieu = px.PhieuXuatNguyenLieuID,
                             SoLuong = item.SoLuongCan,
                             NguyenLieuID = item.NguyenLieuID.GetValueOrDefault(),
                         };
                         unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Add(ct);
                     }
                 }
                 if (lstnguyenlieu.Any(p => p.LoaiHinh == 2))
                 {
                     maxDeNghi2 = unitofwork.Context.PhieuXuatThanhPhams.Max(p => p.MaPhieuXuat);
                     maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXTP");
                     var px = new PhieuXuatThanhPham()
                     {
                         DonHangID = qlsanxuat.DonHangID,
                         DanhSachHinhAnhs = "",
                         DoiTacID = null,
                         IsActive = true,
                         LoaiHinh = 2,
                         MaPhieuXuat = maDeNghi2,
                         NguoiTaoID = NhanVienID,
                         PhieuXuatThanhPhamID = Guid.NewGuid(),
                         ThoiGian = DateTime.Now
                     };
                     unitofwork.Context.PhieuXuatThanhPhams.Add(px);
                     foreach (var item in lstnguyenlieu.Where(p => p.LoaiHinh == 2))
                     {
                         var ct = new ChiTietPhieuXuatThanhPham()
                         {
                             ChiTietPhieuXuatThanhPhamID = Guid.NewGuid(),
                             IsActive = true,
                             PhieuXuatThanhPhamID = px.PhieuXuatThanhPhamID,
                             SoLuong = item.SoLuongCan,
                             ThanhPhamID = item.NguyenLieuID.GetValueOrDefault(),
                         };
                         unitofwork.Context.ChiTietPhieuXuatThanhPhams.Add(ct);
                     }

                 }
                 unitofwork.Save();
                 foreach (var item in lstthanhpham)
                 {
                     if (item.SoLuongDaHoanThien + item.SoLuongSanXuat >= item.SoLuongTong)
                     {
                         DoiTrangThaiCongDoan(HangMucSanXuatID, 3);
                     }
                 }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SanXuatThanhPhamCongDoan(string DonHangID, string SanPhamID, int SoLuong, string dataxuat, Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var lstthanhpham = new JavaScriptSerializer().Deserialize<List<SanXuatThanhPhamModel>>(dataxuat);
                var sanphamid = Guid.Parse(SanPhamID);
                var donhangid = Guid.Parse(DonHangID);

                var ctdh = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.DonHangID == donhangid && p.SanPhamID == sanphamid);
                var ctdhs = unitofwork.Context.ChiTietDonHangs.Where(p => p.DonHangID == ctdh.DonHangID);
                var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == ctdh.DonHangID);

                var sanphamton = unitofwork.KhoSanPhams.LayTon(DonHangID).Where(p=>p.SanPhamID==sanphamid).FirstOrDefault();
                var soluongton = sanphamton!=null?sanphamton.SoLuongTon:0;
                
                // Tạo phiếu nhập
                var maxDeNghi2 = unitofwork.Context.PhieuNhapSanPhams.Max(p => p.MaPhieuNhap);
                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNSP");

                var phieunhaptp = new PhieuNhapSanPham()
                {
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    DonHangID = donhangid,
                    IsActive = true,
                    MaPhieuNhap = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuNhapSanPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuNhapSanPhams.Add(phieunhaptp);
                var ct = new ChiTietPhieuNhapSanPham()
                {
                    ChiTietPhieuNhapSanPhamID = Guid.NewGuid(),
                    GiaNhap = 0,
                    IsActive = true,
                    PhieuNhapSanPhamID = phieunhaptp.PhieuNhapSanPhamID,
                    SoLuong = SoLuong,
                    SanPhamID = sanphamid,
                    TinhTrang = 1,
                    LoaiHinh=2,
                };
                unitofwork.Context.ChiTietPhieuNhapSanPhams.Add(ct);
                unitofwork.Save();
                //Tao phiếu xuất
                maxDeNghi2 = unitofwork.Context.PhieuXuatThanhPhams.Max(p => p.MaPhieuXuat);
                maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXTP");
                var px = new PhieuXuatThanhPham()
                {
                    DonHangID = donhangid,
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    IsActive = true,
                    LoaiHinh = 2,
                    MaPhieuXuat = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuXuatThanhPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuXuatThanhPhams.Add(px);
                foreach (var item in lstthanhpham)
                {
                    var ct2 = new ChiTietPhieuXuatThanhPham()
                    {
                        ChiTietPhieuXuatThanhPhamID = Guid.NewGuid(),
                        IsActive = true,
                        PhieuXuatThanhPhamID = px.PhieuXuatThanhPhamID,
                        SoLuong = item.SoLuongSanXuat.GetValueOrDefault(),
                        ThanhPhamID = item.ThanhPhamID.GetValueOrDefault(),
                    };
                    unitofwork.Context.ChiTietPhieuXuatThanhPhams.Add(ct2);
                }
                unitofwork.Save();
                if (soluongton + SoLuong >= ctdh.SoLuong)
                {
                    DoiTrangThaiCongDoanCuoi(SanPhamID, DonHangID, 3);
                    if (ctdhs.Count(p => p.ChiTietDonHangID != ctdh.ChiTietDonHangID && p.TrangThai == 3) == (ctdhs.Count() - 1))
                    {
                        donhang.TrangThai = 3;
                    }
                }
                unitofwork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //Thành phẩm
        public PhieuSanXuatThanhPhamModel SanXuatThanhPhamChuaBatDau(string ThanhPhamID, string DonHangID)
        {
            var sanphamid = Guid.Parse(ThanhPhamID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new PhieuSanXuatThanhPhamModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
            var doitac = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == donhang.DoiTacID);
            var sanpham = unitofwork.Context.ThanhPhams.FirstOrDefault(p => p.ThanhPhamID == sanphamid);
            var chitiet = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.SanPhamID == sanphamid);

            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;
            xsmodel.TenKhachHang = doitac.TenDoiTac;
            xsmodel.SoLuongDaSanXuat = khothanhphams.Any(p=>p.ThanhPhamID==sanphamid)?khothanhphams.FirstOrDefault(p=>p.ThanhPhamID==sanphamid).SoLuongTon.GetValueOrDefault():0;

            xsmodel.TenSanPham = sanpham.TenThanhPham;
            xsmodel.MaSanPham = sanpham.MaThanhPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;
            xsmodel.SanPhamID = sanphamid;

            xsmodel.SoLuong = chitiet.SoLuong.GetValueOrDefault();
            xsmodel.GiaTriDonHang = chitiet.SoLuong.GetValueOrDefault() * chitiet.DonGia.GetValueOrDefault();
            xsmodel.lstNguyenLieu = db.Web_ThanhPham_DinhNghiaThanhPham(sanphamid).Select(p => new CongDoanNguyenLieuModel { 
            AnhDaiDien =p.AnhDaiDien,
            DonVi=p.TenDonVi,
            LoaiHinh= p.LoaiHinh,
            MaNguyenLieu=p.MaNguyenLieu,
            NguyenLieuID =p.NguyenLieuID,
            SoLuong = p.SoLuong * chitiet.SoLuong.GetValueOrDefault(),
            SoLuongDaCo = (p.LoaiHinh == 1 ? (khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID) != null ? khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID).SoLuongTon : 0) : (khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID) != null ? khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID).SoLuongTon.GetValueOrDefault() : 0)),
            TenNguyenLieu = p.TenNguyenLieu
            }).ToList();


            return xsmodel;
        }
        public PhieuSanXuatThanhPhamModel SanXuatThanhPhamDangSanXuat(string ThanhPhamID, string DonHangID)
        {
            var sanphamid = Guid.Parse(ThanhPhamID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new PhieuSanXuatThanhPhamModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
            var doitac = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == donhang.DoiTacID);
            var sanpham = unitofwork.Context.ThanhPhams.FirstOrDefault(p => p.ThanhPhamID == sanphamid);
            var chitiet = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.SanPhamID == sanphamid);

            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;
            xsmodel.TenKhachHang = doitac.TenDoiTac;
            xsmodel.SoLuongDaSanXuat = khothanhphams.Any(p => p.ThanhPhamID == sanphamid) ? khothanhphams.FirstOrDefault(p => p.ThanhPhamID == sanphamid).SoLuongTon.GetValueOrDefault() : 0;

            xsmodel.TenSanPham = sanpham.TenThanhPham;
            xsmodel.MaSanPham = sanpham.MaThanhPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;
            xsmodel.SanPhamID = sanphamid;

            xsmodel.SoLuong = chitiet.SoLuong.GetValueOrDefault();
            xsmodel.GiaTriDonHang = chitiet.SoLuong.GetValueOrDefault() * chitiet.DonGia.GetValueOrDefault();
            xsmodel.lstNguyenLieu = db.Web_SanXuat_DinhNghiaThanhPham(sanphamid, donhangid).Select(p => new CongDoanNguyenLieuModel
            {
                AnhDaiDien = p.AnhDaiDien,
                DonVi = p.TenDonVi,
                LoaiHinh = p.LoaiHinh.GetValueOrDefault(),
                MaNguyenLieu = p.MaNguyenLieu,
                NguyenLieuID = p.NguyenLieuID,
                SoLuong = p.SoLuong.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                SoLuongDaCo = (p.LoaiHinh == 1 ? (khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID) != null ? khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID).SoLuongTon : 0) : (khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID) != null ? khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID).SoLuongTon.GetValueOrDefault() : 0)),
                TenNguyenLieu = p.TenNguyenLieu
            }).ToList();
            return xsmodel;
        }
        public bool BatDauSanXuatThanhPham(string ThanhPhamID, string DonHangID)
        {
            try
            {
                var sanphamid = Guid.Parse(ThanhPhamID);
                var donhangid = Guid.Parse(DonHangID);
                unitofwork = new UnitOfWork();
                db = new DBMLDFDataContext();
                var lst = db.Web_ThanhPham_DinhNghiaThanhPham(sanphamid);
                foreach (var item in lst)
                {
                    var cd = new CongDoanThanhPhamDinhNghia(){
                        CongDoanThanhPhamDinhNghiaID=Guid.NewGuid(),
                        DonHangID=donhangid,
                        ID=item.NguyenLieuID,
                        LoaiHinh=item.LoaiHinh,
                        SoLuong=item.SoLuong,
                        ThanhPhamID=sanphamid,
                    };
                    unitofwork.Context.CongDoanThanhPhamDinhNghias.Add(cd);
                }
                db.Dispose();
                unitofwork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SanXuatThanhPham2(string DonHangID, string ThanhPhamID, int SoLuong, string dataxuat, Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var lstnguyenlieu = new JavaScriptSerializer().Deserialize<List<SanXuatNguyenLieuModel>>(dataxuat);
                var sanphamid = Guid.Parse(ThanhPhamID);
                var donhangid = Guid.Parse(DonHangID);

                var ctdh = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.DonHangID == donhangid && p.SanPhamID == sanphamid);
                var sanphamton = unitofwork.KhoThanhPhams.LayTon(DonHangID).Where(p => p.ThanhPhamID == sanphamid).FirstOrDefault();
                var soluongton = sanphamton != null ? sanphamton.SoLuongTon : 0;

                if (soluongton + SoLuong >= ctdh.SoLuong)
                {
                    ctdh.TrangThai = 3;
                }
                // Tạo phiếu nhập
                var maxDeNghi2 = unitofwork.Context.PhieuNhapThanhPhams.Max(p => p.MaPhieuNhap);
                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNTP");

                var phieunhaptp = new PhieuNhapThanhPham()
                {
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    DonHangID = donhangid,
                    IsActive = true,
                    MaPhieuNhap = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuNhapThanhPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuNhapThanhPhams.Add(phieunhaptp);
                var ct = new ChiTietPhieuNhapThanhPham()
                {
                    ChiTietPhieuNhapThanhPhamID  = Guid.NewGuid(),
                    GiaNhap = 0,
                    IsActive = true,
                    PhieuNhapThanhPhamID = phieunhaptp.PhieuNhapThanhPhamID,
                    SoLuong = SoLuong,
                    ThanhPhamID = sanphamid,
                    TinhTrang = 1,
                };
                unitofwork.Context.ChiTietPhieuNhapThanhPhams.Add(ct);
                //Tao phiếu xuất
                if (lstnguyenlieu.Any(p => p.LoaiHinh == 1))
                {
                    maxDeNghi2 = unitofwork.Context.PhieuXuatNguyenLieux.Max(p => p.MaPhieuXuat);
                    maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXNL");
                    var px = new PhieuXuatNguyenLieu()
                    {
                        DonHangID = donhangid,
                        DanhSachHinhAnhs = "",
                        DoiTacID = null,
                        IsActive = true,
                        LoaiHinh = 2,
                        MaPhieuXuat = maDeNghi2,
                        NguoiTaoID = NhanVienID,
                        PhieuXuatNguyenLieuID = Guid.NewGuid(),
                        ThoiGian = DateTime.Now
                    };
                    unitofwork.Context.PhieuXuatNguyenLieux.Add(px);
                    foreach (var item in lstnguyenlieu.Where(p => p.LoaiHinh == 1))
                    {
                        var ct2 = new ChiTietPhieuXuatNguyenLieu()
                        {
                            ChiTietPhieuXuatNguyenLieuID = Guid.NewGuid(),
                            IsActive = true,
                            PhieuXuatNguyenLieu = px.PhieuXuatNguyenLieuID,
                            SoLuong = item.SoLuongCan,
                            NguyenLieuID = item.NguyenLieuID.GetValueOrDefault(),
                        };
                        unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Add(ct2);
                    }
                }
                if (lstnguyenlieu.Any(p => p.LoaiHinh == 2))
                {
                    maxDeNghi2 = unitofwork.Context.PhieuXuatThanhPhams.Max(p => p.MaPhieuXuat);
                    maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXTP");
                    var px = new PhieuXuatThanhPham()
                    {
                        DonHangID = donhangid,
                        DanhSachHinhAnhs = "",
                        DoiTacID = null,
                        IsActive = true,
                        LoaiHinh = 2,
                        MaPhieuXuat = maDeNghi2,
                        NguoiTaoID = NhanVienID,
                        PhieuXuatThanhPhamID = Guid.NewGuid(),
                        ThoiGian = DateTime.Now
                    };
                    unitofwork.Context.PhieuXuatThanhPhams.Add(px);
                    foreach (var item in lstnguyenlieu.Where(p => p.LoaiHinh == 2))
                    {
                        var ct2 = new ChiTietPhieuXuatThanhPham()
                        {
                            ChiTietPhieuXuatThanhPhamID = Guid.NewGuid(),
                            IsActive = true,
                            PhieuXuatThanhPhamID = px.PhieuXuatThanhPhamID,
                            SoLuong = item.SoLuongCan,
                            ThanhPhamID = item.NguyenLieuID.GetValueOrDefault(),
                        };
                        unitofwork.Context.ChiTietPhieuXuatThanhPhams.Add(ct2);
                    }

                }
                unitofwork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string CreateMaDeNghi(string maxDeNghi, string prefix)
        {
            DateTime now = DateTime.Now;
            string dd = "";
            if (now.Day < 10) dd = "0" + now.Day;
            else dd = now.Day.ToString();
            string mm = "";
            if (now.Month < 10) mm = "0" + now.Month;
            else mm = now.Month.ToString();

            string yyyy = now.Year.ToString();

            string finalDate = yyyy + mm + dd;


            int numberMax = 1;
            try
            {
                string n = maxDeNghi.Replace(prefix, "");
                n = n.Substring(finalDate.Length, n.Length - finalDate.Length);//=> number
                numberMax = Convert.ToInt32(n) + 1;
            }
            catch { }
            string max = string.Empty;
            if (numberMax < 10) { max = "000" + numberMax; }
            else if (numberMax < 100) { max = "00" + numberMax; }
            else if (numberMax < 1000) { max = "0" + numberMax; }
            return prefix + finalDate + max; ;
        }
        public SanXuatModel SanXuatSanPhamDangSanXuat2(string SanPhamID, string DonHangID)
        {
            var sanphamid = Guid.Parse(SanPhamID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new SanXuatModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
            var qlsanxuat = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.DonHangID == donhangid && sanphamid == p.SanPhamID);
            var doitac = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == donhang.DoiTacID);
            var sanpham = unitofwork.Context.SanPhams.FirstOrDefault(p => p.SanPhamID == sanphamid);
            var chitiet = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.SanPhamID == sanphamid && p.DonHangID == donhangid);

            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);
            var khosanpham = unitofwork.KhoSanPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;
            xsmodel.TenKhachHang = doitac.TenDoiTac;



            xsmodel.Size = sanpham.Size;
            xsmodel.GioiTinh = sanpham.GioiTinh == 1 ? "Nam" : "Nữ";
            xsmodel.Loai = sanpham.Loai == 1 ? "Người lớn" : "Trẻ con";
            xsmodel.ThoiGian = donhang.ThoiGian.GetValueOrDefault().ToString("dd/MM/yyyy");
            xsmodel.TiLeSize = sanpham.TiLeSize;
            xsmodel.SizeMauGoc = sanpham.SizeMauGoc;

            xsmodel.TenSanPham = sanpham.TenSanPham;
            xsmodel.MaSanPham = sanpham.MaSanPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;
            xsmodel.SanPhamID = sanphamid;

            xsmodel.SoLuong = chitiet.SoLuong.GetValueOrDefault();
            xsmodel.GiaTriDonHang = chitiet.SoLuong.GetValueOrDefault() * chitiet.DonGia.GetValueOrDefault();

            //Lấy công đoạn
            xsmodel.lstCongDoan = new List<CongDoanModel>();
            var cdsanpham = (from a in unitofwork.Context.HangMucSanXuats
                             join b in unitofwork.Context.CongDoanHangMucs on a.HangMucSanXuatID equals b.HangMucSanXuatID
                             join c in unitofwork.Context.QuanLySanXuats on a.QuanLySanXuatID equals c.QuanLySanXuatID
                             where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanphamid && c.QuanLySanXuatID == qlsanxuat.QuanLySanXuatID
                             select new
                             {
                                 a.GiaTriDinhMuc,
                                 a.ThoiGianHoanThanh,
                                 a.ViTri,
                                 b.TenCongDoan,
                                 a.HangMucSanXuatID,
                                 b.TrangThai,
                                 b.ThoiGianBatDau,
                                 b.LyDoTamDung,
                             }).ToList();
            var cdsanpham2 = (from a in unitofwork.Context.DMHangMucSanXuats
                              join b in unitofwork.Context.DMCongDoanHangMucs on a.DMHangMucSanXuatID equals b.DMHangMucSanXuatID
                              where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanphamid
                              select new
                              {
                                  a.GiaTriDinhMuc,
                                  a.ThoiGianHoanThanh,
                                  b.TenCongDoan,
                                  a.ViTri,
                                  a.DMHangMucSanXuatID,
                                  a.MoTa
                              }).ToList();
            var lstthanhpham = db.Web_QuanLySanXuat_LayDanhSachThanhPham(sanphamid).ToList();
            foreach (var cd in cdsanpham)
            {
                var lstThanhPham = lstthanhpham.Where(p2 => p2.HangMucSanXuatID == cd.HangMucSanXuatID).Select(p2 => new CongDoanThanhPhamModel
                {
                    DonVi = p2.TenDonVi,
                    MaThanhPham = p2.MaThanhPham,
                    TenThanhPham = p2.TenThanhPham,
                    SoLuongTong = p2.SoLuong.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                    AnhDaiDien = p2.AnhDaiDien,
                    ThanhPhamID = p2.ThanhPhamID,
                    LoaiHinh = 0,
                    SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID) != null ? khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID).SoLuongNhap : 0,
                    lstNguyenLieu = new List<CongDoanNguyenLieuModel>(),
                }).ToList();

                var ncd = new CongDoanModel()
                {
                    TenCongDoan = cd.TenCongDoan,
                    ThoiGianDinhMuc = cd.ThoiGianHoanThanh.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                    ThoiGianHienTai = cd.ThoiGianBatDau.HasValue ? (DateTime.Now - cd.ThoiGianBatDau.GetValueOrDefault()).TotalHours : 0,
                    lstThanhPham = lstThanhPham,
                    ViTri = cd.ViTri,
                    LyDoTamDung = cd.LyDoTamDung,
                    MoTa = cdsanpham2.FirstOrDefault(p => p.ViTri == cd.ViTri) != null ? cdsanpham2.FirstOrDefault(p => p.ViTri == cd.ViTri).MoTa : "",
                    HangMucSanXuatID = cd.HangMucSanXuatID,
                    TrangThai = cd.TrangThai.GetValueOrDefault()
                };
                xsmodel.lstCongDoan.Add(ncd);
            }
            // Lấy công đoạn cuối

            var dinhnghiasanpham = unitofwork.Context.DinhNghiaSanPhams.Where(p => p.IsActive == true && p.SanPhamID == sanphamid);
            if (dinhnghiasanpham.Any())
            {
                var lstCongDoan = db.Web_QuanLySanXuat_LayDanhSachDinhNghiaSanPham(sanphamid).Where(p => p.QuanLySanXuatID == qlsanxuat.QuanLySanXuatID).ToList();
                var thoigianht = qlsanxuat.ThoiGianBatDauCongDoanCuoi.HasValue ? (DateTime.Now - qlsanxuat.ThoiGianBatDauCongDoanCuoi.GetValueOrDefault()).TotalHours : 0;
                var cdc = new CongDoanCuoiModel()
                {
                    SoLuong = chitiet.SoLuong.GetValueOrDefault(),
                    SoLuongDaCo = khosanpham.FirstOrDefault(p => p.SanPhamID == sanphamid) != null ? khosanpham.FirstOrDefault(p => p.SanPhamID == sanphamid).SoLuongNhap: 0,
                    ThoiGianDinhMuc = dinhnghiasanpham.FirstOrDefault().ThoiGianHoanThanh.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                    ThoiGianHienTai = thoigianht,
                    TrangThai = qlsanxuat.TrangThaiCongDoanCuoi,
                    MoTa = dinhnghiasanpham.FirstOrDefault().MoTa,
                    LyDoTamDung = qlsanxuat.LyDoTamDungCongDoanCuoi,
                    lstThanhPham = new List<CongDoanThanhPhamModel>()
                };
                xsmodel.congDoanCuoi = cdc;
            }


            return xsmodel;
        }
    }
}
