﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu;

namespace DF.DataAccess.Repository
{
    public interface IQuanLySanXuatNhomRepository : IRepository<QuanLySanXuat>
    {
      SanXuatNhomModel SanXuatSanPhamChuaBatDau(string MauMayID, string DonHangID);
      SanXuatNhomModel SanXuatSanPhamDangSanXuat(string SanPhamID, string DonHangID);
      bool SuaThanhPhamLoi(Guid DonHangID, Guid ThanhPhamID, double SoLuongSua, Guid NhanVienID);
      bool SuaSanPhamLoi(Guid DonHangID, Guid SanPhamID, double SoLuongSua, Guid NhanVienID);
      LenhSanXuatModel InLenh(string MauMayID, string DonHangID);
      bool BatDauSanXuat(string MauMayID, string DonHangID, Guid NhanVienID);
      bool DoiTrangThaiCongDoan(Guid DonHangID, Guid MauMayID, int ViTri, int TrangThai, string LyDoTamDung = "");
      bool DoiTrangThaiCongDoanCuoi(Guid DonHangID,Guid MauMayID, int TrangThai, string LyDoTamDung="");
      bool SanXuatThanhPham(string DonHangID, string MauMayID, int ViTri, string datanhap, string dataxuat, Guid NhanVienID);
      bool SanXuatThanhPhamCongDoan(string DonHangID,string MauMayID, string datanhap, string dataxuat, Guid NhanVienID);
      PhieuSanXuatThanhPhamModel SanXuatThanhPhamChuaBatDau(string ThanhPhamID, string DonHangID);
      PhieuSanXuatThanhPhamModel SanXuatThanhPhamDangSanXuat(string ThanhPhamID, string DonHangID);
      bool BatDauSanXuatThanhPham(string ThanhPhamID, string DonHangID);
      bool SanXuatThanhPham2(string DonHangID, string ThanhPhamID, int SoLuong, string dataxuat, Guid NhanVienID);
      SanXuatModel SanXuatSanPhamDangSanXuat2(string SanPhamID, string DonHangID);

    }
    public class QuanLySanXuatNhomRepository : EFRepository<QuanLySanXuat>, IQuanLySanXuatNhomRepository
    {
        public QuanLySanXuatNhomRepository(DbContext dbContext) : base(dbContext) { }
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        public SanXuatNhomModel SanXuatSanPhamChuaBatDau(string MauMayID, string DonHangID)
        {
            var maumayid = Guid.Parse(MauMayID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new SanXuatNhomModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p=>p.DonHangID==donhangid);
            var sanphams = unitofwork.Context.SanPhams.Where(p=>p.MauMayID==maumayid).ToList();
            var ctdhs = unitofwork.Context.ChiTietDonHangs.Where(p=>p.DonHangID==donhangid).ToList();

            var doitac = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == donhang.DoiTacID);
            var sanpham =  (from b in ctdhs
                             join c in sanphams on b.SanPhamID equals c.SanPhamID
                             where  c.IsActive==true
                             select c
                                 ).FirstOrDefault();
            var chitiet = (from a in ctdhs
                           join b in sanphams on a.SanPhamID equals b.SanPhamID
                           select a);
            var chitiet2 = (from a in ctdhs
                           join b in sanphams on a.SanPhamID equals b.SanPhamID
                           select b);
            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);
            var khosanpham  = unitofwork.KhoSanPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;
            xsmodel.TenKhachHang = doitac.TenDoiTac;

            xsmodel.Size = sanpham.Size;
            xsmodel.GioiTinh = sanpham.GioiTinh == 1 ? "Nam" : "Nữ";
            xsmodel.Loai = sanpham.Loai == 1 ? "Người lớn" : "Trẻ con";
            xsmodel.ThoiGian = donhang.ThoiGian.GetValueOrDefault().ToString("dd/MM/yyyy");
            xsmodel.TiLeSize = sanpham.TiLeSize;
            xsmodel.SizeMauGoc = sanpham.SizeMauGoc;


            xsmodel.TenSanPham = sanpham.TenSanPham;
            xsmodel.MaSanPham = sanpham.MaSanPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;
            xsmodel.MauMayID = maumayid;
            
            xsmodel.SoLuong = chitiet.Sum(p=>p.SoLuong.GetValueOrDefault());
            xsmodel.GiaTriDonHang = chitiet.Sum(p => p.SoLuong.GetValueOrDefault()* p.DonGia.GetValueOrDefault());

            //Lấy công đoạn
            xsmodel.lstCongDoan = new List<CongDoanModel>();
            var cdsanpham = (from a in unitofwork.Context.DMHangMucSanXuats
                             join b in unitofwork.Context.DMCongDoanHangMucs on a.DMHangMucSanXuatID equals b.DMHangMucSanXuatID
                             where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanpham.SanPhamID
                             select new { 
                                a.GiaTriDinhMuc,
                                a.ThoiGianHoanThanh,
                                b.TenCongDoan,
                                a.ViTri,
                                a.DMHangMucSanXuatID,
                                a.MoTa
                             }).ToList();
            var lstthanhpham = db.Web_DinhNghiaSanPhamNhom_LayDanhSachThanhPham(donhangid, maumayid).ToList(); 
            foreach (var cd in cdsanpham)
            {
                var lstThanhPham = lstthanhpham.Where(p2 => p2.ViTri == cd.ViTri).Select(p2 => new CongDoanThanhPhamModel
                    {
                        DonVi = p2.TenDonVi,
                        MaThanhPham = p2.MaThanhPham,
                        TenThanhPham = p2.TenThanhPham,
                        SoLuongTong = p2.SoLuong.GetValueOrDefault(),
                        AnhDaiDien = p2.AnhDaiDien,
                        ThanhPhamID  =p2.ThanhPhamID,
                        Size =p2.Size,
                        Mau = p2.Mau,
                        LoaiHinh = 0,
                        SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID)!=null?khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID).SoLuongTon.GetValueOrDefault():0,
                        lstNguyenLieu = db.Web_ThanhPham_DinhNghiaThanhPham(p2.ThanhPhamID).Select(p => new CongDoanNguyenLieuModel() { 
                            AnhDaiDien =p.AnhDaiDien,
                            DonVi= p.TenDonVi,
                            LoaiHinh=p.LoaiHinh,
                            MaNguyenLieu=p.MaNguyenLieu,
                            SoLuong = p.SoLuong * p2.SoLuong.GetValueOrDefault(),
                            SoLuongDaCo = (p.LoaiHinh == 1 ? (khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID)!=null?khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID).SoLuongTon:0) : (khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID)!=null?khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID).SoLuongTon.GetValueOrDefault():0)),
                            TenNguyenLieu =p.TenNguyenLieu,
                            NguyenLieuID = p.NguyenLieuID,
                            LoaiVai = p.LoaiVai
                        }).ToList()
                    }).ToList();

                var ncd = new CongDoanModel(){
                    TenCongDoan =cd.TenCongDoan,
                    ThoiGianDinhMuc=(from a in chitiet
                                     join b in unitofwork.Context.DMHangMucSanXuats on a.SanPhamID equals b.SanPhamID 
                                         where b.ViTri==cd.ViTri select b).Sum(p=>p.ThoiGianHoanThanh.GetValueOrDefault()),
                    ThoiGianHienTai = 0,
                    lstThanhPham = lstThanhPham,
                    ViTri =cd.ViTri,
                    MoTa = cd.MoTa,
                    HangMucSanXuatID = cd.DMHangMucSanXuatID,
                    TrangThai=1
                };
                xsmodel.lstCongDoan.Add(ncd);
            }
            // Lấy công đoạn cuối
            var dnsp = unitofwork.Context.DinhNghiaSanPhams.ToList();
            var dinhnghiasanpham = dnsp.Where(p => p.IsActive == true && p.SanPhamID == sanpham.SanPhamID);
            if (dinhnghiasanpham.Any())
            {
                var a = dnsp.Where(p => ctdhs.Any(p5 => p5.SanPhamID == p.SanPhamID)).Select(p => new { p.SanPhamID, p.ThoiGianHoanThanh }).Distinct();
                var lstCongDoan = db.Web_DinhNghiaSanPhamNhom_LayDanhSachDinhNghia(donhangid, maumayid).ToList();
                var cdc = new CongDoanCuoiNhomModel()
                {
                    ThoiGianDinhMuc =  (from b in a
                                            join c in ctdhs on b.SanPhamID equals c.SanPhamID
                                            select c.SoLuong * b.ThoiGianHoanThanh).Sum(p=>p.GetValueOrDefault()),
                    ThoiGianHienTai = 0,
                    MoTa = dinhnghiasanpham.FirstOrDefault().MoTa,
                    lstThanhPham = chitiet2.Select(p => new CongDoanThanhPhamCuoiModel
                    {
                        AnhDaiDien = p.AnhDaiDien,
                        DonVi = "",
                        LoaiHinh = 0,
                        Size = p.Size,
                        Mau = p.Mau,
                        SanPhamID = p.SanPhamID,
                        SoLuongTong = chitiet.FirstOrDefault(p2=>p.SanPhamID==p2.SanPhamID).SoLuong.GetValueOrDefault(),
                        //SoLuongTong = 0,
                        SoLuongDaHoanThien = khosanpham.FirstOrDefault(p2 => p2.SanPhamID == p.SanPhamID) != null ? khosanpham.FirstOrDefault(p2 => p2.SanPhamID == p.SanPhamID).SoLuongNhap : 0,
                        TenSanPham = p.TenSanPham,
                        lstNguyenLieu = lstCongDoan.Where(p3 => p3.SanPhamID == p.SanPhamID).Select(p3 => new CongDoanThanhPhamModel
                        {
                            AnhDaiDien =p3.AnhDaiDien,
                            DonVi =p3.TenDonVi,
                            LoaiHinh =2,
                            MaThanhPham= p3.MaThanhPham,
                            TenThanhPham = p3.TenThanhPham,
                            ThanhPhamID  = p3.ThanhPhamID,
                            SoLuongTong=p3.SoLuongThanhPham,
                            SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p4 => p4.ThanhPhamID == p3.ThanhPhamID)!=null?khothanhphams.FirstOrDefault(p4 => p4.ThanhPhamID == p3.ThanhPhamID).SoLuongTon.GetValueOrDefault():0,
                        }).ToList(),
                    }).ToList()
                };
                xsmodel.congDoanCuoi = cdc;
            }


            return xsmodel;
        }
        public SanXuatNhomModel SanXuatSanPhamDangSanXuat(string MauMayID, string DonHangID)
        {
            var maumayid = Guid.Parse(MauMayID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new SanXuatNhomModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
            var sanphams = unitofwork.Context.SanPhams.Where(p => p.MauMayID == maumayid).ToList();
            var ctdhs = unitofwork.Context.ChiTietDonHangs.Where(p => p.DonHangID == donhangid).ToList();

            var doitac = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == donhang.DoiTacID);
            var sanpham = (from b in ctdhs
                           join c in sanphams on b.SanPhamID equals c.SanPhamID
                           where c.IsActive == true
                           select c
                                 ).FirstOrDefault();
            var chitiet = (from a in ctdhs
                           join b in sanphams on a.SanPhamID equals b.SanPhamID
                           select a);
            var chitiet2 = (from a in ctdhs
                            join b in sanphams on a.SanPhamID equals b.SanPhamID
                            select b);

            var slhong = db.Web_ThanhPham_SoLuongHong(donhangid).ToList();
            var slhongsp = db.Web_SanPham_SoLuongHong(donhangid).ToList();

            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);
            var khosanpham = unitofwork.KhoSanPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;
            xsmodel.TenKhachHang = doitac.TenDoiTac;

            xsmodel.Size = sanpham.Size;
            xsmodel.GioiTinh = sanpham.GioiTinh == 1 ? "Nam" : "Nữ";
            xsmodel.Loai = sanpham.Loai == 1 ? "Người lớn" : "Trẻ con";
            xsmodel.ThoiGian = donhang.ThoiGian.GetValueOrDefault().ToString("dd/MM/yyyy");
            xsmodel.TiLeSize = sanpham.TiLeSize;
            xsmodel.SizeMauGoc = sanpham.SizeMauGoc;


            xsmodel.TenSanPham = sanpham.TenSanPham;
            xsmodel.MaSanPham = sanpham.MaSanPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;
            xsmodel.MauMayID = maumayid;

            xsmodel.SoLuong = chitiet.Sum(p => p.SoLuong.GetValueOrDefault());
            xsmodel.GiaTriDonHang = chitiet.Sum(p => p.SoLuong.GetValueOrDefault() * p.DonGia.GetValueOrDefault());

            //Lấy công đoạn
            xsmodel.lstCongDoan = new List<CongDoanModel>();
            var cdsanpham = (from a in unitofwork.Context.HangMucSanXuats
                             join b in unitofwork.Context.CongDoanHangMucs on a.HangMucSanXuatID equals b.HangMucSanXuatID
                             join c in unitofwork.Context.QuanLySanXuats on a.QuanLySanXuatID equals c.QuanLySanXuatID
                             where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanpham.SanPhamID && c.DonHangID==donhangid
                             select new
                             {
                                 a.GiaTriDinhMuc,
                                 a.ThoiGianHoanThanh,
                                 a.ViTri,
                                 b.TenCongDoan,
                                 a.HangMucSanXuatID,
                                 b.TrangThai,
                                 b.ThoiGianBatDau,
                                 b.LyDoTamDung,
                             }).ToList();
            var lstthanhpham = db.Web_DinhNghiaSanPhamNhom_LayDanhSachThanhPham(donhangid, maumayid).ToList();

            var cdsanpham2 = (from a in unitofwork.Context.DMHangMucSanXuats
                              join b in unitofwork.Context.DMCongDoanHangMucs on a.DMHangMucSanXuatID equals b.DMHangMucSanXuatID
                              where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanpham.SanPhamID
                              select new
                              {
                                  a.GiaTriDinhMuc,
                                  a.ThoiGianHoanThanh,
                                  b.TenCongDoan,
                                  a.ViTri,
                                  a.DMHangMucSanXuatID,
                                  a.MoTa
                              }).ToList();

            var lstlydohongs = (from a in unitofwork.Context.ChiTietPhieuNhapThanhPhams
                              join b in unitofwork.Context.PhieuNhapThanhPhams on a.PhieuNhapThanhPhamID equals b.PhieuNhapThanhPhamID
                              where a.IsActive == true && b.IsActive == true && a.TinhTrang == 2 && b.DonHangID == donhangid
                              select a).ToList();
            var lstlydohongs2 = (from a in unitofwork.Context.ChiTietPhieuNhapSanPhams
                                 join b in unitofwork.Context.PhieuNhapSanPhams on a.PhieuNhapSanPhamID equals b.PhieuNhapSanPhamID
                                where a.IsActive == true && b.IsActive == true && a.TinhTrang == 2 && b.DonHangID == donhangid
                                select a).ToList();
            foreach (var cd in cdsanpham)
            {
                var lstThanhPham = lstthanhpham.Where(p2 => p2.ViTri == cd.ViTri).Select(p2 => new CongDoanThanhPhamModel
                {
                    DonVi = p2.TenDonVi,
                    MaThanhPham = p2.MaThanhPham,
                    ThanhPhamID = p2.ThanhPhamID,
                    TenThanhPham = p2.TenThanhPham,
                    SoLuongTong = p2.SoLuong.GetValueOrDefault(),
                    AnhDaiDien = p2.AnhDaiDien,
                    Size = p2.Size,
                    Mau = p2.Mau,
                    LoaiHinh = 0,
                    LyDoHong = String.Join(",", lstlydohongs.Where(p => p.ThanhPhamID == p2.ThanhPhamID).Select(p => p.LyDoHong)),
                    SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID) != null ? khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID).SoLuongNhap : 0,
                    SoLuongHong = slhong.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID) != null ? slhong.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID).SoLuongHong : 0,
                    lstNguyenLieu = db.Web_ThanhPham_DinhNghiaThanhPham(p2.ThanhPhamID).Select(p => new CongDoanNguyenLieuModel()
                    {
                        AnhDaiDien = p.AnhDaiDien,
                        DonVi = p.TenDonVi,
                        LoaiHinh = p.LoaiHinh,
                        MaNguyenLieu = p.MaNguyenLieu,
                        SoLuong = p.SoLuong * p2.SoLuong.GetValueOrDefault(),
                        SoLuongDaCo = (p.LoaiHinh == 1 ? (khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID) != null ? khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID).SoLuongTon : 0) : (khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID) != null ? khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID).SoLuongTon.GetValueOrDefault() : 0)),
                        TenNguyenLieu = p.TenNguyenLieu,
                        NguyenLieuID = p.NguyenLieuID,
                        LoaiVai = p.LoaiVai
                    }).ToList()
                }).ToList();

                var ncd = new CongDoanModel()
                {
                    TenCongDoan = cd.TenCongDoan,
                    ThoiGianDinhMuc = (from a in chitiet
                                       join b in unitofwork.Context.DMHangMucSanXuats on a.SanPhamID equals b.SanPhamID
                                       where b.ViTri == cd.ViTri
                                       select b).Sum(p => p.ThoiGianHoanThanh.GetValueOrDefault()),
                    ThoiGianHienTai = 0,
                    lstThanhPham = lstThanhPham,
                    ViTri = cd.ViTri,
                    LyDoTamDung = cd.LyDoTamDung,
                    MoTa = cdsanpham2.FirstOrDefault(p=>p.ViTri==cd.ViTri)!=null?cdsanpham2.FirstOrDefault(p=>p.ViTri==cd.ViTri).MoTa:"",
                    HangMucSanXuatID = cd.HangMucSanXuatID,
                    TrangThai = cd.TrangThai.GetValueOrDefault()
                };
                xsmodel.lstCongDoan.Add(ncd);
            }
            // Lấy công đoạn cuối
            var dnsp = unitofwork.Context.DinhNghiaSanPhams.ToList();
            var dinhnghiasanpham = dnsp.Where(p => p.IsActive == true && p.SanPhamID == sanpham.SanPhamID);
            if (dinhnghiasanpham.Any())
            {
                var qlsanxuat = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.SanPhamID == sanpham.SanPhamID && p.DonHangID == donhangid);
                var a = dnsp.Where(p => ctdhs.Any(p5 => p5.SanPhamID == p.SanPhamID)).Select(p => new { p.SanPhamID, p.ThoiGianHoanThanh }).Distinct();
                var thoigianht = qlsanxuat.ThoiGianBatDauCongDoanCuoi.HasValue ? (DateTime.Now - qlsanxuat.ThoiGianBatDauCongDoanCuoi.GetValueOrDefault()).TotalHours : 0;
                var lstCongDoan = db.Web_DinhNghiaSanPhamNhom_LayDanhSachDinhNghia(donhangid, maumayid).ToList();
                var cdc = new CongDoanCuoiNhomModel()
                {
                    ThoiGianDinhMuc = (from b in a
                                       join c in ctdhs on b.SanPhamID equals c.SanPhamID
                                       select c.SoLuong * b.ThoiGianHoanThanh).Sum(p => p.GetValueOrDefault()),
                    ThoiGianHienTai = 0,
                    MoTa = dinhnghiasanpham.FirstOrDefault().MoTa,
                    LyDoTamDung = qlsanxuat.LyDoTamDungCongDoanCuoi,
                    TrangThai  = qlsanxuat.TrangThaiCongDoanCuoi,
                    lstThanhPham = chitiet2.Select(p => new CongDoanThanhPhamCuoiModel
                    {
                        AnhDaiDien = p.AnhDaiDien,
                        DonVi = "",
                        LoaiHinh = 0,
                        Size = p.Size,
                        Mau = p.Mau,
                        SanPhamID = p.SanPhamID,
                        SoLuongTong = chitiet.FirstOrDefault(p2 => p.SanPhamID == p2.SanPhamID).SoLuong.GetValueOrDefault(),
                        LyDoHong= String.Join(",",lstlydohongs2.Where(p2=>p2.SanPhamID==p.SanPhamID).Select(p2=>p2.LyDoHong)),
                        //SoLuongTong = 0,
                        SoLuongHong = slhongsp.FirstOrDefault(p2 => p2.SanPhamID == p.SanPhamID) != null ? slhongsp.FirstOrDefault(p2 => p2.SanPhamID == p.SanPhamID).SoLuongHong : 0,
                        SoLuongDaHoanThien = khosanpham.FirstOrDefault(p2 => p2.SanPhamID == p.SanPhamID) != null ? khosanpham.FirstOrDefault(p2 => p2.SanPhamID == p.SanPhamID).SoLuongNhap : 0,
                        TenSanPham = p.TenSanPham,
                        lstNguyenLieu = lstCongDoan.Where(p3 => p3.SanPhamID == p.SanPhamID).Select(p3 => new CongDoanThanhPhamModel
                        {
                            AnhDaiDien = p3.AnhDaiDien,
                            ThanhPhamID = p3.ThanhPhamID,
                            DonVi = p3.TenDonVi,
                            LoaiHinh = 2,
                            MaThanhPham = p3.MaThanhPham,
                            SoLuong = 1,
                            TenThanhPham = p3.TenThanhPham,
                            SoLuongTong = p3.SoLuongThanhPham * chitiet.FirstOrDefault(p2 => p.SanPhamID == p2.SanPhamID).SoLuong.GetValueOrDefault(),
                            SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p4 => p4.ThanhPhamID == p3.ThanhPhamID) != null ? khothanhphams.FirstOrDefault(p4 => p4.ThanhPhamID == p3.ThanhPhamID).SoLuongTon.GetValueOrDefault() : 0,
                        }).ToList(),
                    }).ToList()
                };
                xsmodel.congDoanCuoi = cdc;
            }


            return xsmodel;
        }
        public bool BatDauSanXuat(string MauMayID, string DonHangID, Guid NhanVienID)
        {
            try
            {
                var maumayid = Guid.Parse(MauMayID);
                var donhangid = Guid.Parse(DonHangID);
                unitofwork = new UnitOfWork();
                db = new DBMLDFDataContext();
                var lstnguyenlieu = db.Web_NguyenLieu_TonKho_DonHangCanTheoSanPhamNhom(donhangid,maumayid).ToList();
                unitofwork = new UnitOfWork();
                Guid PhieuXuatVatTuID = Guid.NewGuid();
                Guid PhieuNhapVatTuID = Guid.NewGuid();
                var maxDeNghi = unitofwork.Context.PhieuXuatNguyenLieux.Max(p => p.MaPhieuXuat);
                string maDeNghi = CreateMaDeNghi(maxDeNghi, "PXNL");
                var maxDeNghi2 = unitofwork.Context.PhieuNhapNguyenLieux.Max(p => p.MaPhieuNhap);
                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNNL");

                Guid deNghiCongTrinhId = Guid.NewGuid();

                var phieuxuatnguyenlieu = new PhieuXuatNguyenLieu()
                {
                    DonHangID = Guid.Parse(Define.CONGTRINHTONG),
                    DanhSachHinhAnhs = "",
                    IsActive = true,
                    NguoiTaoID = NhanVienID,
                    PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                    ThoiGian = DateTime.Now,
                    DonHangNhanID = donhangid,
                    MaPhieuXuat = maDeNghi,
                    LoaiHinh = 1
                };
                unitofwork.Context.PhieuXuatNguyenLieux.Add(phieuxuatnguyenlieu);
                var phieunhapnguyenlieu = new PhieuNhapNguyenLieu()
                {
                    DonHangID = donhangid,
                    DanhSachHinhAnhs = "",
                    IsActive = true,
                    NguoiTaoID = NhanVienID,
                    PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                    ThoiGian = DateTime.Now,
                    MaPhieuNhap = maDeNghi2,
                    PhieuNhapNguyenLieuID = PhieuNhapVatTuID,
                };
                unitofwork.Context.PhieuNhapNguyenLieux.Add(phieunhapnguyenlieu);


                foreach (var it in lstnguyenlieu)
                {
                    if ((it.SoLuongTon.GetValueOrDefault() >= it.SoLuongCan.GetValueOrDefault() ? it.SoLuongCan.GetValueOrDefault() : it.SoLuongTon.GetValueOrDefault()) > 0)
                    {
                        var chitietxuat = new ChiTietPhieuXuatNguyenLieu()
                        {
                            ChiTietPhieuXuatNguyenLieuID = Guid.NewGuid(),
                            IsActive = true,
                            NguyenLieuID = it.NguyenLieuID,
                            PhieuXuatNguyenLieu = PhieuXuatVatTuID,
                            //SoLuong = it.SoLuongTon >= it.SoLuongCan ? it.SoLuongCan : it.SoLuongTon,
                            SoLuong = it.SoLuongCan.GetValueOrDefault(),
                        };
                        unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Add(chitietxuat);

                        var chiTietNhap = new ChiTietPhieuNhapNguyenLieu()
                        {
                            ChiTietPhieuNhapNguyenLieuID = Guid.NewGuid(),
                            GiaNhap = 0,
                            PhieuNhapNguyenLieuID = PhieuNhapVatTuID,
                            //SoLuong = it.SoLuongTon >= it.SoLuongCan ? it.SoLuongCan.GetValueOrDefault() : it.SoLuongTon.GetValueOrDefault(),
                            SoLuong = it.SoLuongCan.GetValueOrDefault(),
                            NguyenLieuID = it.NguyenLieuID,
                            IsActive = true,
                        };
                        unitofwork.Context.ChiTietPhieuNhapNguyenLieux.Add(chiTietNhap);
                    }
                }
                var sanphams = unitofwork.Context.SanPhams.Where(p => p.MauMayID == maumayid).ToList();
                var ctdhs = unitofwork.Context.ChiTietDonHangs.Where(p => p.DonHangID == donhangid).ToList();
                var chitiet2 = (from a in ctdhs
                                join b in sanphams on a.SanPhamID equals b.SanPhamID
                                select b);
                foreach (var sp in chitiet2)
                {
                    var cdsanpham = (from a in unitofwork.Context.DMHangMucSanXuats
                                     join b in unitofwork.Context.DMCongDoanHangMucs on a.DMHangMucSanXuatID equals b.DMHangMucSanXuatID
                                     where a.IsActive == true && b.IsActive == true && a.SanPhamID == sp.SanPhamID
                                     select new
                                     {
                                         a.GiaTriDinhMuc,
                                         a.ThoiGianHoanThanh,
                                         b.TenCongDoan,
                                         a.DMHangMucSanXuatID,
                                         a.ViTri,
                                         b.DMCongDoanHangMucID,
                                         a.HaoHut
                                     }).ToList();
                    var quanlysanxuat = new QuanLySanXuat()
                    {
                        DonHangID = donhangid,
                        IsActive = true,
                        LoaiHinh = 1,
                        QuanLySanXuatID = Guid.NewGuid(),
                        SanPhamID = sp.SanPhamID,
                        TenCongDoan = cdsanpham.OrderBy(p => p.ViTri.GetValueOrDefault()).FirstOrDefault().TenCongDoan,
                        ThoiGian = DateTime.Now,
                        TrangThaiCongDoanCuoi = 1
                    };
                    unitofwork.Context.QuanLySanXuats.Add(quanlysanxuat);
                    foreach (var cd in cdsanpham)
                    {
                        var hangmucsanxuat = new HangMucSanXuat()
                        {
                            GiaTriDinhMuc = cd.GiaTriDinhMuc,
                            HangMucSanXuatID = Guid.NewGuid(),
                            HaoHut = cd.HaoHut,
                            IsActive = true,
                            ViTri = cd.ViTri,
                            QuanLySanXuatID = quanlysanxuat.QuanLySanXuatID,
                            SanPhamID = sp.SanPhamID,
                            ThoiGianHoanThanh = cd.ThoiGianHoanThanh.GetValueOrDefault(),
                        };
                        unitofwork.Context.HangMucSanXuats.Add(hangmucsanxuat);
                        var congdoan = new CongDoanHangMuc()
                        {
                            CongDoanHangMucID = Guid.NewGuid(),
                            HangMucSanXuatID = hangmucsanxuat.HangMucSanXuatID,
                            IsActive = true,
                            LyDoTamDung = "",
                            TenCongDoan = cd.TenCongDoan,
                            ThoiGianBatDau = DateTime.Now,
                            TrangThai = cdsanpham.OrderBy(p => p.ViTri.GetValueOrDefault()).FirstOrDefault().DMHangMucSanXuatID == cd.DMHangMucSanXuatID ? 2 : 1
                        };
                        unitofwork.Context.CongDoanHangMucs.Add(congdoan);
                        var lstthanhpham = db.Web_DinhNghiaSanPham_LayDanhSachThanhPham(sp.SanPhamID).Where(p => p.DMHangMucSanXuatID == cd.DMHangMucSanXuatID).ToList();
                        foreach (var thanhpham in lstthanhpham)
                        {
                            var lstcongthuc = db.Web_ThanhPham_DinhNghiaThanhPham(thanhpham.ThanhPhamID).ToList();
                            var tp = new CongDoanThanhPham()
                            {
                                CongDoanHangMucID = congdoan.CongDoanHangMucID,
                                CongDoanThanhPhamID = Guid.NewGuid(),
                                IsActive = true,
                                SoLuong = thanhpham.SoLuong,
                                ThanhPhamID = thanhpham.ThanhPhamID
                            };
                            unitofwork.Context.CongDoanThanhPhams.Add(tp);
                            foreach (var item in lstcongthuc)
                            {
                                var dcdt = new CongDoanThanhPhamDinhNghia()
                                {
                                    CongDoanThanhPhamDinhNghiaID = Guid.NewGuid(),
                                    CongDoanThanhPhamID = tp.CongDoanThanhPhamID,
                                    ID = item.NguyenLieuID,
                                    LoaiHinh = item.LoaiHinh,
                                    SoLuong = item.SoLuong,
                                    ThanhPhamID = thanhpham.ThanhPhamID
                                };
                                unitofwork.Context.CongDoanThanhPhamDinhNghias.Add(dcdt);
                            }
                        }



                    }
                    var dinhnghiasanpham = unitofwork.Context.DinhNghiaSanPhams.Where(p => p.IsActive == true && p.SanPhamID == sp.SanPhamID);
                    foreach (var item in dinhnghiasanpham)
                    {
                        var dnsp = new CongDoanSanPhamDinhNghia()
                        {
                            CongDoanSanPhamDinhNghiaID = Guid.NewGuid(),
                            QuanLySanXuatID = quanlysanxuat.QuanLySanXuatID,
                            SoLuong = item.SoLuongThanhPham,
                            ThanhPhamID = item.ThanhPhamID
                        };
                        unitofwork.Context.CongDoanSanPhamDinhNghias.Add(dnsp);
                    }
                    var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
                    donhang.TrangThai = 2;
                    var ctsp = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.DonHangID == donhangid && p.SanPhamID == sp.SanPhamID);
                    ctsp.TrangThai = 2;
                }
                db.Dispose();
                unitofwork.Save();
                return true;
            }catch(Exception ex){
                return false;
            }
        }
        public LenhSanXuatModel InLenh(string MauMayID, string DonHangID)
        {
            var maumayid = Guid.Parse(MauMayID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new LenhSanXuatModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
            var sanphams = unitofwork.Context.SanPhams.Where(p => p.MauMayID == maumayid).ToList();
            var ctdhs = unitofwork.Context.ChiTietDonHangs.Where(p => p.DonHangID == donhangid).ToList();

            var doitac = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == donhang.DoiTacID);
            var sanpham = (from b in ctdhs
                           join c in sanphams on b.SanPhamID equals c.SanPhamID
                           where c.IsActive == true
                           select c
                                 ).FirstOrDefault();
            var chitiet = (from a in ctdhs
                           join b in sanphams on a.SanPhamID equals b.SanPhamID
                           select a);
            var chitiet2 = (from a in ctdhs
                           join b in sanphams on a.SanPhamID equals b.SanPhamID
                           select new {b.Size,b.Mau,a.SoLuong});
            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);
            var khosanpham = unitofwork.KhoSanPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;
            xsmodel.TenKhachHang = doitac.TenDoiTac;

            xsmodel.Size = sanpham.Size;
            xsmodel.GioiTinh = sanpham.GioiTinh == 1 ? "Nam" : "Nữ";
            xsmodel.Loai = sanpham.Loai == 1 ? "Người lớn" : "Trẻ con";
            xsmodel.ThoiGian = donhang.ThoiGian.GetValueOrDefault().ToString("dd/MM/yyyy");
            xsmodel.TiLeSize = sanpham.TiLeSize;
            xsmodel.SizeMauGoc = sanpham.SizeMauGoc;


            xsmodel.TenSanPham = sanpham.TenSanPham;
            xsmodel.MaSanPham = sanpham.MaSanPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;


            xsmodel.SoLuong = chitiet.Sum(p => p.SoLuong.GetValueOrDefault());
            xsmodel.GiaTriDonHang = chitiet.Sum(p => p.SoLuong.GetValueOrDefault() * p.DonGia.GetValueOrDefault());
            xsmodel.items = db.Web_ThanhPhamNhom_DinhNghiaThanhPham(maumayid).Select(p => new LenhSanXuatChiTietModel(){
                    AnhDaiDien=p.AnhDaiDien,
                    LoaiVai = p.LoaiVai,
                    MaNguyenLieu = p.MaNguyenLieu,
                    TenNguyenLieu = p.TenNguyenLieu,
                    Mau = p.Mau,
                    Size = p.Size,
                    SoLuong = chitiet2.FirstOrDefault(p2=>p2.Size==p.Size && p.Mau==p2.Mau)!=null?chitiet2.FirstOrDefault(p2=>p2.Size==p.Size && p.Mau==p2.Mau).SoLuong.GetValueOrDefault():0
            }).ToList();
            


            return xsmodel;
        }


        public bool DoiTrangThaiCongDoan(Guid DonHangID,Guid MauMayID,int ViTri, int TrangThai, string LyDoTamDung="")
        {
            try
            {
                var maumayid = MauMayID;
                var donhangid = DonHangID;
                unitofwork = new UnitOfWork();
                db = new DBMLDFDataContext();
                var xsmodel = new SanXuatNhomModel();
                var sanphams = unitofwork.Context.SanPhams.Where(p => p.MauMayID == maumayid).ToList();
                var ctdhs = unitofwork.Context.ChiTietDonHangs.Where(p => p.DonHangID == donhangid).ToList();

                var chitiet2 = (from a in ctdhs
                                join b in sanphams on a.SanPhamID equals b.SanPhamID
                                select b);

                var cdhm = unitofwork.Context.CongDoanHangMucs.ToList();
                var hmsx =  unitofwork.Context.HangMucSanXuats.ToList();
                var qlsx =  unitofwork.Context.QuanLySanXuats.ToList();
                foreach (var sp in chitiet2)
                {
                    var qlsanxuat = qlsx.FirstOrDefault(p => p.DonHangID == DonHangID && p.SanPhamID == sp.SanPhamID);
                    var hangmucsx = hmsx.FirstOrDefault(p => p.SanPhamID == sp.SanPhamID && p.QuanLySanXuatID == qlsanxuat.QuanLySanXuatID && p.ViTri==ViTri);
                    var congdoan = cdhm.FirstOrDefault(p => p.HangMucSanXuatID == hangmucsx.HangMucSanXuatID);
                    congdoan.TrangThai = TrangThai;
                    congdoan.LyDoTamDung = LyDoTamDung;
                    unitofwork.Save();
                    if (qlsanxuat != null)
                    {
                        var ctdonhang = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.SanPhamID == qlsanxuat.SanPhamID && p.DonHangID == qlsanxuat.DonHangID);
                        if (TrangThai == 3)
                        {

                            if (ctdonhang != null)
                            {
                                ctdonhang.TenCongDoan = congdoan.TenCongDoan;
                                var congdoansau = (from a in unitofwork.Context.CongDoanHangMucs
                                                   join b in unitofwork.Context.HangMucSanXuats on a.HangMucSanXuatID equals b.HangMucSanXuatID
                                                   where b.ViTri > hangmucsx.ViTri && a.HangMucSanXuatID != hangmucsx.HangMucSanXuatID && b.QuanLySanXuatID == qlsanxuat.QuanLySanXuatID
                                                   orderby b.ViTri ascending
                                                   select a).FirstOrDefault();
                                if (congdoansau != null)
                                {
                                    congdoansau.TrangThai = 2;

                                }
                                else
                                {
                                    DoiTrangThaiCongDoanCuoi(DonHangID,maumayid, 2);
                                }
                            }
                        }
                        if (TrangThai == 2)
                        {
                            qlsanxuat.TenCongDoan = congdoan.TenCongDoan;
                            if (congdoan != null)
                            {
                                congdoan.ThoiGianBatDau = DateTime.Now;

                                if (ctdonhang != null)
                                {
                                    ctdonhang.TenCongDoan = congdoan.TenCongDoan;
                                    ctdonhang.TrangThai = 2;
                                }
                            }
                        }
                        if (TrangThai == 1)
                        {
                            if (congdoan != null)
                            {
                                congdoan.ThoiGianBatDau = null;
                            }
                        }
                    }

                    congdoan.TrangThai = TrangThai;
                    unitofwork.Save();
                }
                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool DoiTrangThaiCongDoanCuoi(Guid DonHangID,Guid MauMayID, int TrangThai, string LyDoTamDung="")
        {
            try
            {
                var maumayid = MauMayID;
                var donhangid = DonHangID;
                unitofwork = new UnitOfWork();
                db = new DBMLDFDataContext();
                var xsmodel = new SanXuatNhomModel();
                var sanphams = unitofwork.Context.SanPhams.Where(p => p.MauMayID == maumayid).ToList();
                var ctdhs = unitofwork.Context.ChiTietDonHangs.Where(p => p.DonHangID == donhangid).ToList();

                var chitiet2 = (from a in ctdhs
                                join b in sanphams on a.SanPhamID equals b.SanPhamID
                                select b);

                var cdhm = unitofwork.Context.CongDoanHangMucs.ToList();
                var hmsx = unitofwork.Context.HangMucSanXuats.ToList();
                var qlsx = unitofwork.Context.QuanLySanXuats.ToList();
                foreach (var sp in chitiet2)
                {
                    var congdoan = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.DonHangID == donhangid && p.SanPhamID == sp.SanPhamID);
                    var ctdonhang = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.DonHangID == donhangid && p.SanPhamID == sp.SanPhamID);
                    var donhang2 = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
                    if (TrangThai == 2)
                    {
                        foreach (var sx in qlsx.Where(p => p.DonHangID == DonHangID).ToList())
                        {
                            sx.TenCongDoan = "Hoàn thiện";
                            unitofwork.QuanLySanXuats.Update(sx);
                        }
                        congdoan.ThoiGianBatDauCongDoanCuoi = DateTime.Now;
                        ctdonhang.TenCongDoan = "Hoàn thiện";
                    }
                    if (TrangThai == 3 || TrangThai == 5)
                    {
                        ctdonhang.TrangThai = TrangThai;
                        if (TrangThai == 3)
                        {
                            if (ctdhs.Count(p => p.ChiTietDonHangID != ctdonhang.ChiTietDonHangID && p.TrangThai == 3) == (ctdhs.Count() - 1))
                            {
                                donhang2.TrangThai = 3;
                            }
                        }
                    }
                    if (TrangThai == 1)
                    {
                        if (congdoan != null)
                        {
                            congdoan.ThoiGianBatDauCongDoanCuoi = null;
                        }
                    }
                    congdoan.TrangThaiCongDoanCuoi = TrangThai;
                    congdoan.LyDoTamDungCongDoanCuoi = LyDoTamDung;
                    unitofwork.Save();
                }
                
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SuaThanhPhamLoi(Guid DonHangID, Guid ThanhPhamID, double SoLuongSua, Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                // Tạo phiếu nhập
                var maxDeNghi2 = unitofwork.Context.PhieuNhapThanhPhams.Max(p => p.MaPhieuNhap);
                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNTP");

                var phieunhaptp = new PhieuNhapThanhPham()
                {
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    DonHangID = DonHangID,
                    IsActive = true,
                    MaPhieuNhap = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuNhapThanhPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuNhapThanhPhams.Add(phieunhaptp);
                var ct = new ChiTietPhieuNhapThanhPham()
                {
                    ChiTietPhieuNhapThanhPhamID = Guid.NewGuid(),
                    GiaNhap = 0,
                    IsActive = true,
                    PhieuNhapThanhPhamID = phieunhaptp.PhieuNhapThanhPhamID,
                    SoLuong = SoLuongSua,
                    ThanhPhamID = ThanhPhamID,
                    TinhTrang = 1
                };
                unitofwork.Context.ChiTietPhieuNhapThanhPhams.Add(ct);
                //Tao phiếu xuất
                maxDeNghi2 = unitofwork.Context.PhieuXuatThanhPhams.Max(p => p.MaPhieuXuat);
                maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXTP");
                var px = new PhieuXuatThanhPham()
                {
                    DonHangID = DonHangID,
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    IsActive = true,
                    LoaiHinh = 2,
                    MaPhieuXuat = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuXuatThanhPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuXuatThanhPhams.Add(px);
                var ct2 = new ChiTietPhieuXuatThanhPham()
                {
                    ChiTietPhieuXuatThanhPhamID = Guid.NewGuid(),
                    IsActive = true,
                    PhieuXuatThanhPhamID = px.PhieuXuatThanhPhamID,
                    SoLuong = SoLuongSua,
                    TinhTrang =2,
                    ThanhPhamID = ThanhPhamID,
                };
                unitofwork.Context.ChiTietPhieuXuatThanhPhams.Add(ct2);
                unitofwork.Save();
               
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SuaSanPhamLoi(Guid DonHangID, Guid SanPhamID, double SoLuongSua, Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();

                // Tạo phiếu nhập
                var maxDeNghi2 = unitofwork.Context.PhieuNhapThanhPhams.Max(p => p.MaPhieuNhap);
                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNSP");

                var phieunhaptp = new PhieuNhapSanPham()
                {
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    DonHangID = DonHangID,
                    IsActive = true,
                    MaPhieuNhap = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuNhapSanPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuNhapSanPhams.Add(phieunhaptp);
                var ct = new ChiTietPhieuNhapSanPham()
                {
                    ChiTietPhieuNhapSanPhamID = Guid.NewGuid(),
                    GiaNhap = 0,
                    IsActive = true,
                    PhieuNhapSanPhamID = phieunhaptp.PhieuNhapSanPhamID,
                    SoLuong = SoLuongSua,
                    SanPhamID = SanPhamID,
                    TinhTrang = 1,
                    LoaiHinh =2
                };
                unitofwork.Context.ChiTietPhieuNhapSanPhams.Add(ct);
                //Tao phiếu xuất
                maxDeNghi2 = unitofwork.Context.PhieuXuatSanPhams.Max(p => p.MaPhieuXuat);
                maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXSP");
                var px = new PhieuXuatSanPham()
                {
                    DonHangID = DonHangID,
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    IsActive = true,
                    LoaiHinh = 2,
                    MaPhieuXuat = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuXuatSanPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuXuatSanPhams.Add(px);
                var ct2 = new ChiTietPhieuXuatSanPham()
                {
                    ChiTietPhieuXuatSanPhamID = Guid.NewGuid(),
                    IsActive = true,
                    PhieuXuatSanPhamID = px.PhieuXuatSanPhamID,
                    SoLuong = SoLuongSua,
                    TinhTrang =2,
                    SanPhamID = SanPhamID,
                };
                unitofwork.Context.ChiTietPhieuXuatSanPhams.Add(ct2);
                unitofwork.Save();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SanXuatThanhPham(string DonHangID,string MauMayID,int ViTri, string datanhap,string dataxuat,Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var lstthanhpham = new JavaScriptSerializer().Deserialize<List<SanXuatThanhPhamModel>>(datanhap);
                var lstnguyenlieu = new JavaScriptSerializer().Deserialize<List<SanXuatNguyenLieuModel>>(dataxuat);
                var maumayid = Guid.Parse(MauMayID);
                var donhangid = Guid.Parse(DonHangID);
                var tontps = (from a in unitofwork.Context.ChiTietPhieuNhapThanhPhams
                              join b in unitofwork.Context.PhieuNhapThanhPhams on a.PhieuNhapThanhPhamID equals b.PhieuNhapThanhPhamID
                              where a.IsActive == true && b.IsActive == true && a.TinhTrang == 2 && b.DonHangID == donhangid
                              select a.SoLuong).ToList().Sum(p => p);
                // Tạo phiếu nhập
                var maxDeNghi2 = unitofwork.Context.PhieuNhapThanhPhams.Max(p => p.MaPhieuNhap);
                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNTP");

                var phieunhaptp = new PhieuNhapThanhPham()
                {
                    DanhSachHinhAnhs ="",
                    DoiTacID=null,
                    DonHangID = donhangid,
                    IsActive=true,
                    MaPhieuNhap=maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuNhapThanhPhamID = Guid.NewGuid(),
                    ThoiGian=DateTime.Now
                };
                unitofwork.Context.PhieuNhapThanhPhams.Add(phieunhaptp);
                foreach (var item in lstthanhpham)
                {

                    if (item.SoLuongSanXuat > 0)
                    {
                        var ct = new ChiTietPhieuNhapThanhPham()
                        {
                            ChiTietPhieuNhapThanhPhamID = Guid.NewGuid(),
                            GiaNhap = 0,
                            IsActive = true,
                            PhieuNhapThanhPhamID = phieunhaptp.PhieuNhapThanhPhamID,
                            SoLuong = item.SoLuongSanXuat.GetValueOrDefault(),
                            ThanhPhamID = item.ThanhPhamID.GetValueOrDefault(),
                            TinhTrang = 1
                        };
                        unitofwork.Context.ChiTietPhieuNhapThanhPhams.Add(ct);
                    }
                    if (item.SoLuongHong > 0)
                    {
                        var ct = new ChiTietPhieuNhapThanhPham()
                        {
                            ChiTietPhieuNhapThanhPhamID = Guid.NewGuid(),
                            GiaNhap = 0,
                            IsActive = true,
                            PhieuNhapThanhPhamID = phieunhaptp.PhieuNhapThanhPhamID,
                            SoLuong = item.SoLuongHong.GetValueOrDefault(),
                            ThanhPhamID = item.ThanhPhamID.GetValueOrDefault(),
                            TinhTrang = 2,
                            LyDoHong = item.LyDoHong
                        };
                        unitofwork.Context.ChiTietPhieuNhapThanhPhams.Add(ct);
                    }
                   
                }
                //Tao phiếu xuất
                 
                 if (lstnguyenlieu.Any(p => p.LoaiHinh == 1))
                 {
                     maxDeNghi2 = unitofwork.Context.PhieuXuatNguyenLieux.Max(p => p.MaPhieuXuat);
                     maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXNL");
                     var px = new PhieuXuatNguyenLieu()
                     {
                         DonHangID = donhangid,
                         DanhSachHinhAnhs = "",
                         DoiTacID = null,
                         IsActive = true,
                         LoaiHinh = 2,
                         MaPhieuXuat = maDeNghi2,
                         NguoiTaoID = NhanVienID,
                         PhieuXuatNguyenLieuID = Guid.NewGuid(),
                         ThoiGian = DateTime.Now
                     };
                     unitofwork.Context.PhieuXuatNguyenLieux.Add(px);
                     foreach (var item in lstnguyenlieu.Where(p => p.LoaiHinh == 1))
                     {
                         var ct = new ChiTietPhieuXuatNguyenLieu()
                         {
                             ChiTietPhieuXuatNguyenLieuID = Guid.NewGuid(),
                             IsActive = true,
                             PhieuXuatNguyenLieu = px.PhieuXuatNguyenLieuID,
                             SoLuong = item.SoLuongCan,
                             NguyenLieuID = item.NguyenLieuID.GetValueOrDefault(),
                         };
                         unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Add(ct);
                     }
                 }
                 if (lstnguyenlieu.Any(p => p.LoaiHinh == 2))
                 {
                     maxDeNghi2 = unitofwork.Context.PhieuXuatThanhPhams.Max(p => p.MaPhieuXuat);
                     maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXTP");
                     var px = new PhieuXuatThanhPham()
                     {
                         DonHangID = donhangid,
                         DanhSachHinhAnhs = "",
                         DoiTacID = null,
                         IsActive = true,
                         LoaiHinh = 2,
                         MaPhieuXuat = maDeNghi2,
                         NguoiTaoID = NhanVienID,
                         PhieuXuatThanhPhamID = Guid.NewGuid(),
                         ThoiGian = DateTime.Now
                     };
                     unitofwork.Context.PhieuXuatThanhPhams.Add(px);
                     foreach (var item in lstnguyenlieu.Where(p => p.LoaiHinh == 2))
                     {
                         var ct = new ChiTietPhieuXuatThanhPham()
                         {
                             ChiTietPhieuXuatThanhPhamID = Guid.NewGuid(),
                             IsActive = true,
                             PhieuXuatThanhPhamID = px.PhieuXuatThanhPhamID,
                             SoLuong = item.SoLuongCan,
                             ThanhPhamID = item.NguyenLieuID.GetValueOrDefault(),
                         };
                         unitofwork.Context.ChiTietPhieuXuatThanhPhams.Add(ct);
                     }

                 }
                 unitofwork.Save();
                if (lstthanhpham.Sum(item=>item.SoLuongDaHoanThien.GetValueOrDefault() + item.SoLuongSanXuat.GetValueOrDefault() + item.SoLuongHong.GetValueOrDefault() + tontps)>= lstthanhpham.Sum(item => item.SoLuongTong.GetValueOrDefault()))
                {
                    DoiTrangThaiCongDoan(donhangid, maumayid, ViTri, 3);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool SanXuatThanhPhamCongDoan(string DonHangID,string MauMayID, string datanhap, string dataxuat, Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var lstthanhpham = new JavaScriptSerializer().Deserialize<List<SanXuatThanhPhamModel>>(dataxuat);
                var lstsp = new JavaScriptSerializer().Deserialize<List<SanXuatSanPhamNhomModel>>(datanhap);
                var maumayid = Guid.Parse(MauMayID);
                var donhangid = Guid.Parse(DonHangID);

                var sanphams = unitofwork.Context.SanPhams.Where(p => p.MauMayID == maumayid).ToList();
                var ctdhs = unitofwork.Context.ChiTietDonHangs.Where(p => p.DonHangID == donhangid).ToList();
                var chitiet2 = (from a in ctdhs
                                join b in sanphams on a.SanPhamID equals b.SanPhamID
                                select a);
                var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);

                var tons = unitofwork.KhoSanPhams.LayTon(DonHangID);
                double tontps;
                try
                {
                    tontps = (from a in unitofwork.Context.ChiTietPhieuNhapThanhPhams
                                  join b in unitofwork.Context.PhieuNhapThanhPhams on a.PhieuNhapThanhPhamID equals b.PhieuNhapThanhPhamID
                                  where a.IsActive == true && b.IsActive == true && a.TinhTrang == 2 && b.DonHangID == donhangid
                                  select a.SoLuong).ToList().Sum(p => p);
                }
                catch (Exception ex)
                {
                    tontps = 0;
                }
                
                
                // Tạo phiếu nhập
                var maxDeNghi2 = unitofwork.Context.PhieuNhapSanPhams.Max(p => p.MaPhieuNhap);
                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNSP");

                var phieunhaptp = new PhieuNhapSanPham()
                {
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    DonHangID = donhangid,
                    IsActive = true,
                    MaPhieuNhap = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuNhapSanPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuNhapSanPhams.Add(phieunhaptp);
                foreach (var sp in lstsp)
                {
                    if (sp.SoLuongSanXuat > 0)
                    {
                        var ct = new ChiTietPhieuNhapSanPham()
                        {
                            ChiTietPhieuNhapSanPhamID = Guid.NewGuid(),
                            GiaNhap = 0,
                            IsActive = true,
                            PhieuNhapSanPhamID = phieunhaptp.PhieuNhapSanPhamID,
                            SoLuong = sp.SoLuongSanXuat,
                            SanPhamID = sp.SanPhamID.GetValueOrDefault(),
                            TinhTrang = 1,
                            LoaiHinh = 2,
                        };
                        unitofwork.Context.ChiTietPhieuNhapSanPhams.Add(ct);
                    }
                    if (sp.SoLuongHong > 0)
                    {
                        var ct = new ChiTietPhieuNhapSanPham()
                        {
                            ChiTietPhieuNhapSanPhamID = Guid.NewGuid(),
                            GiaNhap = 0,
                            IsActive = true,
                            PhieuNhapSanPhamID = phieunhaptp.PhieuNhapSanPhamID,
                            SoLuong = sp.SoLuongHong,
                            SanPhamID = sp.SanPhamID.GetValueOrDefault(),
                            TinhTrang = 2,
                            LoaiHinh = 2,
                            LyDoHong = sp.LyDoHong
                        };
                        unitofwork.Context.ChiTietPhieuNhapSanPhams.Add(ct);
                    }
                    var ctdh = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.DonHangID == donhangid && p.SanPhamID == sp.SanPhamID);
                    var sanphamton = tons.Where(p=>p.SanPhamID==sp.SanPhamID).FirstOrDefault();
                    var soluongton = sanphamton!=null?sanphamton.SoLuongTon:0;
                    if (soluongton + sp.SoLuongSanXuat >= ctdh.SoLuong)
                    {
                        if (ctdhs.Count(p => p.ChiTietDonHangID != ctdh.ChiTietDonHangID && p.TrangThai == 3) == (ctdhs.Count() - 1))
                        {
                            donhang.TrangThai = 3;
                        }
                    }
                }
               
                unitofwork.Save();
                //Tao phiếu xuất
                maxDeNghi2 = unitofwork.Context.PhieuXuatThanhPhams.Max(p => p.MaPhieuXuat);
                maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXTP");
                var px = new PhieuXuatThanhPham()
                {
                    DonHangID = donhangid,
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    IsActive = true,
                    LoaiHinh = 2,
                    MaPhieuXuat = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuXuatThanhPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuXuatThanhPhams.Add(px);
                foreach (var item in lstthanhpham)
                {
                    var ct2 = new ChiTietPhieuXuatThanhPham()
                    {
                        ChiTietPhieuXuatThanhPhamID = Guid.NewGuid(),
                        IsActive = true,
                        PhieuXuatThanhPhamID = px.PhieuXuatThanhPhamID,
                        SoLuong = item.SoLuongCan.GetValueOrDefault(),
                        ThanhPhamID = item.ThanhPhamID.GetValueOrDefault(),
                    };
                    unitofwork.Context.ChiTietPhieuXuatThanhPhams.Add(ct2);
                }
                unitofwork.Save();
                if ((tons.Where(p => chitiet2.Any(p2 => p2.SanPhamID == p.SanPhamID)).Sum(p => p.SoLuongNhap) + lstsp.Sum(p => p.SoLuongSanXuat.GetValueOrDefault() + p.SoLuongHong.GetValueOrDefault()) + tontps) == chitiet2.Sum(p => p.SoLuong))
                {
                    DoiTrangThaiCongDoanCuoi(donhangid, maumayid, 3);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        //Thành phẩm
        public PhieuSanXuatThanhPhamModel SanXuatThanhPhamChuaBatDau(string ThanhPhamID, string DonHangID)
        {
            var sanphamid = Guid.Parse(ThanhPhamID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new PhieuSanXuatThanhPhamModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
            var doitac = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == donhang.DoiTacID);
            var sanpham = unitofwork.Context.ThanhPhams.FirstOrDefault(p => p.ThanhPhamID == sanphamid);
            var chitiet = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.SanPhamID == sanphamid);

            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;
            xsmodel.TenKhachHang = doitac.TenDoiTac;
            xsmodel.SoLuongDaSanXuat = khothanhphams.Any(p=>p.ThanhPhamID==sanphamid)?khothanhphams.FirstOrDefault(p=>p.ThanhPhamID==sanphamid).SoLuongTon.GetValueOrDefault():0;

            xsmodel.TenSanPham = sanpham.TenThanhPham;
            xsmodel.MaSanPham = sanpham.MaThanhPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;
            xsmodel.SanPhamID = sanphamid;

            xsmodel.SoLuong = chitiet.SoLuong.GetValueOrDefault();
            xsmodel.GiaTriDonHang = chitiet.SoLuong.GetValueOrDefault() * chitiet.DonGia.GetValueOrDefault();
            xsmodel.lstNguyenLieu = db.Web_ThanhPham_DinhNghiaThanhPham(sanphamid).Select(p => new CongDoanNguyenLieuModel { 
            AnhDaiDien =p.AnhDaiDien,
            DonVi=p.TenDonVi,
            LoaiHinh= p.LoaiHinh,
            MaNguyenLieu=p.MaNguyenLieu,
            NguyenLieuID =p.NguyenLieuID,
            SoLuong = p.SoLuong * chitiet.SoLuong.GetValueOrDefault(),
            SoLuongDaCo = (p.LoaiHinh == 1 ? (khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID) != null ? khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID).SoLuongTon : 0) : (khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID) != null ? khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID).SoLuongTon.GetValueOrDefault() : 0)),
            TenNguyenLieu = p.TenNguyenLieu
            }).ToList();


            return xsmodel;
        }
        public PhieuSanXuatThanhPhamModel SanXuatThanhPhamDangSanXuat(string ThanhPhamID, string DonHangID)
        {
            var sanphamid = Guid.Parse(ThanhPhamID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new PhieuSanXuatThanhPhamModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
            var doitac = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == donhang.DoiTacID);
            var sanpham = unitofwork.Context.ThanhPhams.FirstOrDefault(p => p.ThanhPhamID == sanphamid);
            var chitiet = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.SanPhamID == sanphamid);

            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;
            xsmodel.TenKhachHang = doitac.TenDoiTac;
            xsmodel.SoLuongDaSanXuat = khothanhphams.Any(p => p.ThanhPhamID == sanphamid) ? khothanhphams.FirstOrDefault(p => p.ThanhPhamID == sanphamid).SoLuongTon.GetValueOrDefault() : 0;

            xsmodel.TenSanPham = sanpham.TenThanhPham;
            xsmodel.MaSanPham = sanpham.MaThanhPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;
            xsmodel.SanPhamID = sanphamid;

            xsmodel.SoLuong = chitiet.SoLuong.GetValueOrDefault();
            xsmodel.GiaTriDonHang = chitiet.SoLuong.GetValueOrDefault() * chitiet.DonGia.GetValueOrDefault();
            xsmodel.lstNguyenLieu = db.Web_SanXuat_DinhNghiaThanhPham(sanphamid, donhangid).Select(p => new CongDoanNguyenLieuModel
            {
                AnhDaiDien = p.AnhDaiDien,
                DonVi = p.TenDonVi,
                LoaiHinh = p.LoaiHinh.GetValueOrDefault(),
                MaNguyenLieu = p.MaNguyenLieu,
                NguyenLieuID = p.NguyenLieuID,
                SoLuong = p.SoLuong.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                SoLuongDaCo = (p.LoaiHinh == 1 ? (khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID) != null ? khonguyenlieus.FirstOrDefault(p3 => p3.NguyenLieuID == p.NguyenLieuID).SoLuongTon : 0) : (khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID) != null ? khothanhphams.FirstOrDefault(p3 => p3.ThanhPhamID == p.NguyenLieuID).SoLuongTon.GetValueOrDefault() : 0)),
                TenNguyenLieu = p.TenNguyenLieu
            }).ToList();
            return xsmodel;
        }
        public bool BatDauSanXuatThanhPham(string ThanhPhamID, string DonHangID)
        {
            try
            {
                var sanphamid = Guid.Parse(ThanhPhamID);
                var donhangid = Guid.Parse(DonHangID);
                unitofwork = new UnitOfWork();
                db = new DBMLDFDataContext();
                var lst = db.Web_ThanhPham_DinhNghiaThanhPham(sanphamid);
                foreach (var item in lst)
                {
                    var cd = new CongDoanThanhPhamDinhNghia(){
                        CongDoanThanhPhamDinhNghiaID=Guid.NewGuid(),
                        DonHangID=donhangid,
                        ID=item.NguyenLieuID,
                        LoaiHinh=item.LoaiHinh,
                        SoLuong=item.SoLuong,
                        ThanhPhamID=sanphamid,
                    };
                    unitofwork.Context.CongDoanThanhPhamDinhNghias.Add(cd);
                }
                db.Dispose();
                unitofwork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool SanXuatThanhPham2(string DonHangID, string ThanhPhamID, int SoLuong, string dataxuat, Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var lstnguyenlieu = new JavaScriptSerializer().Deserialize<List<SanXuatNguyenLieuModel>>(dataxuat);
                var sanphamid = Guid.Parse(ThanhPhamID);
                var donhangid = Guid.Parse(DonHangID);

                var ctdh = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.DonHangID == donhangid && p.SanPhamID == sanphamid);
                var sanphamton = unitofwork.KhoThanhPhams.LayTon(DonHangID).Where(p => p.ThanhPhamID == sanphamid).FirstOrDefault();
                var soluongton = sanphamton != null ? sanphamton.SoLuongTon : 0;

                if (soluongton + SoLuong >= ctdh.SoLuong)
                {
                    ctdh.TrangThai = 3;
                }
                // Tạo phiếu nhập
                var maxDeNghi2 = unitofwork.Context.PhieuNhapThanhPhams.Max(p => p.MaPhieuNhap);
                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNTP");

                var phieunhaptp = new PhieuNhapThanhPham()
                {
                    DanhSachHinhAnhs = "",
                    DoiTacID = null,
                    DonHangID = donhangid,
                    IsActive = true,
                    MaPhieuNhap = maDeNghi2,
                    NguoiTaoID = NhanVienID,
                    PhieuNhapThanhPhamID = Guid.NewGuid(),
                    ThoiGian = DateTime.Now
                };
                unitofwork.Context.PhieuNhapThanhPhams.Add(phieunhaptp);
                var ct = new ChiTietPhieuNhapThanhPham()
                {
                    ChiTietPhieuNhapThanhPhamID  = Guid.NewGuid(),
                    GiaNhap = 0,
                    IsActive = true,
                    PhieuNhapThanhPhamID = phieunhaptp.PhieuNhapThanhPhamID,
                    SoLuong = SoLuong,
                    ThanhPhamID = sanphamid,
                    TinhTrang = 1,
                };
                unitofwork.Context.ChiTietPhieuNhapThanhPhams.Add(ct);
                //Tao phiếu xuất
                if (lstnguyenlieu.Any(p => p.LoaiHinh == 1))
                {
                    maxDeNghi2 = unitofwork.Context.PhieuXuatNguyenLieux.Max(p => p.MaPhieuXuat);
                    maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXNL");
                    var px = new PhieuXuatNguyenLieu()
                    {
                        DonHangID = donhangid,
                        DanhSachHinhAnhs = "",
                        DoiTacID = null,
                        IsActive = true,
                        LoaiHinh = 2,
                        MaPhieuXuat = maDeNghi2,
                        NguoiTaoID = NhanVienID,
                        PhieuXuatNguyenLieuID = Guid.NewGuid(),
                        ThoiGian = DateTime.Now
                    };
                    unitofwork.Context.PhieuXuatNguyenLieux.Add(px);
                    foreach (var item in lstnguyenlieu.Where(p => p.LoaiHinh == 1))
                    {
                        var ct2 = new ChiTietPhieuXuatNguyenLieu()
                        {
                            ChiTietPhieuXuatNguyenLieuID = Guid.NewGuid(),
                            IsActive = true,
                            PhieuXuatNguyenLieu = px.PhieuXuatNguyenLieuID,
                            SoLuong = item.SoLuongCan,
                            NguyenLieuID = item.NguyenLieuID.GetValueOrDefault(),
                        };
                        unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Add(ct2);
                    }
                }
                if (lstnguyenlieu.Any(p => p.LoaiHinh == 2))
                {
                    maxDeNghi2 = unitofwork.Context.PhieuXuatThanhPhams.Max(p => p.MaPhieuXuat);
                    maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PXTP");
                    var px = new PhieuXuatThanhPham()
                    {
                        DonHangID = donhangid,
                        DanhSachHinhAnhs = "",
                        DoiTacID = null,
                        IsActive = true,
                        LoaiHinh = 2,
                        MaPhieuXuat = maDeNghi2,
                        NguoiTaoID = NhanVienID,
                        PhieuXuatThanhPhamID = Guid.NewGuid(),
                        ThoiGian = DateTime.Now
                    };
                    unitofwork.Context.PhieuXuatThanhPhams.Add(px);
                    foreach (var item in lstnguyenlieu.Where(p => p.LoaiHinh == 2))
                    {
                        var ct2 = new ChiTietPhieuXuatThanhPham()
                        {
                            ChiTietPhieuXuatThanhPhamID = Guid.NewGuid(),
                            IsActive = true,
                            PhieuXuatThanhPhamID = px.PhieuXuatThanhPhamID,
                            SoLuong = item.SoLuongCan,
                            ThanhPhamID = item.NguyenLieuID.GetValueOrDefault(),
                        };
                        unitofwork.Context.ChiTietPhieuXuatThanhPhams.Add(ct2);
                    }

                }
                unitofwork.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public string CreateMaDeNghi(string maxDeNghi, string prefix)
        {
            DateTime now = DateTime.Now;
            string dd = "";
            if (now.Day < 10) dd = "0" + now.Day;
            else dd = now.Day.ToString();
            string mm = "";
            if (now.Month < 10) mm = "0" + now.Month;
            else mm = now.Month.ToString();

            string yyyy = now.Year.ToString();

            string finalDate = yyyy + mm + dd;


            int numberMax = 1;
            try
            {
                string n = maxDeNghi.Replace(prefix, "");
                n = n.Substring(finalDate.Length, n.Length - finalDate.Length);//=> number
                numberMax = Convert.ToInt32(n) + 1;
            }
            catch { }
            string max = string.Empty;
            if (numberMax < 10) { max = "000" + numberMax; }
            else if (numberMax < 100) { max = "00" + numberMax; }
            else if (numberMax < 1000) { max = "0" + numberMax; }
            return prefix + finalDate + max; ;
        }
        public SanXuatModel SanXuatSanPhamDangSanXuat2(string SanPhamID, string DonHangID)
        {
            var sanphamid = Guid.Parse(SanPhamID);
            var donhangid = Guid.Parse(DonHangID);
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            var xsmodel = new SanXuatModel();
            var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == donhangid);
            var qlsanxuat = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.DonHangID == donhangid && sanphamid == p.SanPhamID);
            var doitac = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == donhang.DoiTacID);
            var sanpham = unitofwork.Context.SanPhams.FirstOrDefault(p => p.SanPhamID == sanphamid);
            var chitiet = unitofwork.Context.ChiTietDonHangs.FirstOrDefault(p => p.SanPhamID == sanphamid && p.DonHangID == donhangid);

            var khonguyenlieus = unitofwork.KhoNguyenLieus.LayTon(DonHangID);
            var khothanhphams = unitofwork.KhoThanhPhams.LayTon(DonHangID);
            var khosanpham = unitofwork.KhoSanPhams.LayTon(DonHangID);

            xsmodel.MaDonHang = donhang.MaDonHang;
            xsmodel.TenDonHang = donhang.TenDonHang;
            xsmodel.DonHangID = donhangid;
            xsmodel.TenKhachHang = doitac.TenDoiTac;



            xsmodel.Size = sanpham.Size;
            xsmodel.GioiTinh = sanpham.GioiTinh == 1 ? "Nam" : "Nữ";
            xsmodel.Loai = sanpham.Loai == 1 ? "Người lớn" : "Trẻ con";
            xsmodel.ThoiGian = donhang.ThoiGian.GetValueOrDefault().ToString("dd/MM/yyyy");
            xsmodel.TiLeSize = sanpham.TiLeSize;
            xsmodel.SizeMauGoc = sanpham.SizeMauGoc;

            xsmodel.TenSanPham = sanpham.TenSanPham;
            xsmodel.MaSanPham = sanpham.MaSanPham;
            xsmodel.AnhDaiDien = sanpham.AnhDaiDien;
            xsmodel.SanPhamID = sanphamid;

            xsmodel.SoLuong = chitiet.SoLuong.GetValueOrDefault();
            xsmodel.GiaTriDonHang = chitiet.SoLuong.GetValueOrDefault() * chitiet.DonGia.GetValueOrDefault();

            //Lấy công đoạn
            xsmodel.lstCongDoan = new List<CongDoanModel>();
            var cdsanpham = (from a in unitofwork.Context.HangMucSanXuats
                             join b in unitofwork.Context.CongDoanHangMucs on a.HangMucSanXuatID equals b.HangMucSanXuatID
                             join c in unitofwork.Context.QuanLySanXuats on a.QuanLySanXuatID equals c.QuanLySanXuatID
                             where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanphamid && c.QuanLySanXuatID == qlsanxuat.QuanLySanXuatID
                             select new
                             {
                                 a.GiaTriDinhMuc,
                                 a.ThoiGianHoanThanh,
                                 a.ViTri,
                                 b.TenCongDoan,
                                 a.HangMucSanXuatID,
                                 b.TrangThai,
                                 b.ThoiGianBatDau,
                                 b.LyDoTamDung,
                             }).ToList();
            var cdsanpham2 = (from a in unitofwork.Context.DMHangMucSanXuats
                              join b in unitofwork.Context.DMCongDoanHangMucs on a.DMHangMucSanXuatID equals b.DMHangMucSanXuatID
                              where a.IsActive == true && b.IsActive == true && a.SanPhamID == sanphamid
                              select new
                              {
                                  a.GiaTriDinhMuc,
                                  a.ThoiGianHoanThanh,
                                  b.TenCongDoan,
                                  a.ViTri,
                                  a.DMHangMucSanXuatID,
                                  a.MoTa
                              }).ToList();
            var lstthanhpham = db.Web_QuanLySanXuat_LayDanhSachThanhPham(sanphamid).ToList();
            foreach (var cd in cdsanpham)
            {
                var lstThanhPham = lstthanhpham.Where(p2 => p2.HangMucSanXuatID == cd.HangMucSanXuatID).Select(p2 => new CongDoanThanhPhamModel
                {
                    DonVi = p2.TenDonVi,
                    MaThanhPham = p2.MaThanhPham,
                    TenThanhPham = p2.TenThanhPham,
                    SoLuongTong = p2.SoLuong.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                    AnhDaiDien = p2.AnhDaiDien,
                    ThanhPhamID = p2.ThanhPhamID,
                    LoaiHinh = 0,
                    SoLuongDaHoanThien = khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID) != null ? khothanhphams.FirstOrDefault(p => p.ThanhPhamID == p2.ThanhPhamID).SoLuongNhap : 0,
                    lstNguyenLieu = new List<CongDoanNguyenLieuModel>(),
                }).ToList();

                var ncd = new CongDoanModel()
                {
                    TenCongDoan = cd.TenCongDoan,
                    ThoiGianDinhMuc = cd.ThoiGianHoanThanh.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                    ThoiGianHienTai = cd.ThoiGianBatDau.HasValue ? (DateTime.Now - cd.ThoiGianBatDau.GetValueOrDefault()).TotalHours : 0,
                    lstThanhPham = lstThanhPham,
                    ViTri = cd.ViTri,
                    LyDoTamDung = cd.LyDoTamDung,
                    MoTa = cdsanpham2.FirstOrDefault(p => p.ViTri == cd.ViTri) != null ? cdsanpham2.FirstOrDefault(p => p.ViTri == cd.ViTri).MoTa : "",
                    HangMucSanXuatID = cd.HangMucSanXuatID,
                    TrangThai = cd.TrangThai.GetValueOrDefault()
                };
                xsmodel.lstCongDoan.Add(ncd);
            }
            // Lấy công đoạn cuối

            var dinhnghiasanpham = unitofwork.Context.DinhNghiaSanPhams.Where(p => p.IsActive == true && p.SanPhamID == sanphamid);
            if (dinhnghiasanpham.Any())
            {
                var lstCongDoan = db.Web_QuanLySanXuat_LayDanhSachDinhNghiaSanPham(sanphamid).Where(p => p.QuanLySanXuatID == qlsanxuat.QuanLySanXuatID).ToList();
                var thoigianht = qlsanxuat.ThoiGianBatDauCongDoanCuoi.HasValue ? (DateTime.Now - qlsanxuat.ThoiGianBatDauCongDoanCuoi.GetValueOrDefault()).TotalHours : 0;
                var cdc = new CongDoanCuoiModel()
                {
                    SoLuong = chitiet.SoLuong.GetValueOrDefault(),
                    SoLuongDaCo = khosanpham.FirstOrDefault(p => p.SanPhamID == sanphamid) != null ? khosanpham.FirstOrDefault(p => p.SanPhamID == sanphamid).SoLuongNhap: 0,
                    ThoiGianDinhMuc = dinhnghiasanpham.FirstOrDefault().ThoiGianHoanThanh.GetValueOrDefault() * chitiet.SoLuong.GetValueOrDefault(),
                    ThoiGianHienTai = thoigianht,
                    TrangThai = qlsanxuat.TrangThaiCongDoanCuoi,
                    MoTa = dinhnghiasanpham.FirstOrDefault().MoTa,
                    LyDoTamDung = qlsanxuat.LyDoTamDungCongDoanCuoi,
                    lstThanhPham = new List<CongDoanThanhPhamModel>()
                };
                xsmodel.congDoanCuoi = cdc;
            }


            return xsmodel;
        }
    }
}
