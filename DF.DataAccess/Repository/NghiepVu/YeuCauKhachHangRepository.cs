﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu;
using System.Globalization;

namespace DF.DataAccess.Repository
{
    public interface IYeuCauKhachHangRepository : IRepository<YeuCauKhachHang>
    {
        List<Web_YeuCauKhachHang_GetAllDataResult> GetAllDataYeuCauKhachHang();
        int AddOrUpdateYeuCauKhachHang(string data, Guid nvid, string res, string dataChiTiet, string dataSize, string SoLuongDat, string GiaBan, string ThoiGianGiaoHang);
        int DeleteYeuCauKhachHang(string data);
    }
    public class YeuCauKhachHangRepository : EFRepository<YeuCauKhachHang>, IYeuCauKhachHangRepository
    {
        protected UnitOfWork unitofwork;
        public YeuCauKhachHangRepository(DbContext dbContext) : base(dbContext) { }
        protected DBMLDFDataContext db;
        public List<Web_YeuCauKhachHang_GetAllDataResult> GetAllDataYeuCauKhachHang()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            List<Web_YeuCauKhachHang_GetAllDataResult> list = db.Web_YeuCauKhachHang_GetAllData().ToList();
            return list;
        }
        public int AddOrUpdateYeuCauKhachHang(string data, Guid nvid, string res, string dataChiTiet, string dataSize, string SoLuongDat, string GiaBan, string ThoiGianGiaoHang)
        {
            try
            {
                unitofwork = new UnitOfWork();
                db = new DBMLDFDataContext();
                var banghi = new JavaScriptSerializer().Deserialize<YeuCauKhachHang>(data);
                var danhsachchitiet = new JavaScriptSerializer().Deserialize<List<ChiTietYeuCauModel>>(dataChiTiet);
                var danhsachsize = new JavaScriptSerializer().Deserialize<List<DanhSachSizeModel>>(dataSize);
                if (banghi.YeuCauKhachHangID == null || banghi.YeuCauKhachHangID == Guid.Empty)
                {
                    // thêm
                    var vt = new YeuCauKhachHang();
                    vt.MaYeuCau = res;
                    vt.STT = db.Web_YeuCauKhachHang_GetMaxIndex().FirstOrDefault().Index + 1;
                    vt.IsActive = true;
                    vt.YeuCauKhachHangID = Guid.NewGuid();
                    vt.ThoiGian = DateTime.Now;
                    vt.NguoiTaoID = nvid;
                    vt.NoiDung = "";
                    vt.TinhTrang = banghi.TinhTrang;
                    vt.DoiTacID = banghi.DoiTacID;
                    vt.HinhAnh = banghi.HinhAnh;
                    vt.LichSuDuyet = banghi.LichSuDuyet;
                    vt.LoaiHinhSanXuat = banghi.LoaiHinhSanXuat;
                    vt.GiaBan = double.Parse(GiaBan);
                    vt.SoLuongDat = double.Parse(SoLuongDat);
                    vt.ThoiGianGiaoHang = DateTime.ParseExact(ThoiGianGiaoHang, "dd/MM/yyyy", null);
                    foreach (var item in danhsachchitiet)
                    {
                        vt.NoiDung += item.TenSanPham + ": ";
                        vt.NoiDung += " - Đặc điểm: " + item.DacDiem;
                        vt.NoiDung += " - Chất liệu: " + item.ChatLieu;
                        vt.NoiDung += " - Size: ";
                        foreach (var size in danhsachsize)
                        {
                            var ct = new ChiTietYeuCauKhachHang();
                            ct.ChiTietYeuCauID = Guid.NewGuid();
                            ct.YeuCauKhachHangID = vt.YeuCauKhachHangID;
                            ct.TenSanPham = item.TenSanPham;
                            ct.Size = size.Size;
                            ct.Mau = item.Mau;
                            ct.SoLuongDat = (int)(double.Parse(SoLuongDat) / (danhsachchitiet.Count * danhsachsize.Count));
                            ct.GiaBan = double.Parse(GiaBan);
                            ct.YeuCauKhac = item.YeuCauKhac;
                            ct.IsActive = 1;
                            ct.DacDiem = item.DacDiem;
                            ct.ChatLieu = item.ChatLieu;
                            ct.MauPhoi = item.MauPhoi;
                            unitofwork.Context.ChiTietYeuCauKhachHangs.Add(ct);
                            vt.NoiDung += size.Size + ", ";
                        }
                        vt.NoiDung += " - Màu: " + item.Mau;
                        vt.NoiDung += " - Màu phối: " + item.MauPhoi;
                        vt.NoiDung += " - Ghi chú: " + (item.YeuCauKhac == "" ? "Không có! + " : item.YeuCauKhac + " + ");
                    }
                    vt.NoiDung += " - Giá bán: " + GiaBan + " - ";
                    vt.NoiDung += " - Số lượng đặt: " + SoLuongDat + " - ";
                    vt.NoiDung += " - Thời gian giao hàng: " + DateTime.ParseExact(ThoiGianGiaoHang, "dd/MM/yyyy", null).ToString("dd/M/yyyy", CultureInfo.InvariantCulture) + " - ";
                    vt.NoiDung1 = vt.NoiDung;
                    unitofwork.YeuCauKhachHangs.Add(vt);
                    unitofwork.Save();
                    return 2;
                }
                else // sửa
                {
                    var vt = unitofwork.Context.YeuCauKhachHangs.Find(banghi.YeuCauKhachHangID);
                    vt.MaYeuCau = res;
                    vt.NoiDung = "";
                    vt.DoiTacID = banghi.DoiTacID;
                    vt.HinhAnh = banghi.HinhAnh;
                    vt.TinhTrang = banghi.TinhTrang;
                    vt.LoaiHinhSanXuat = banghi.LoaiHinhSanXuat;
                    vt.GiaBan = double.Parse(GiaBan);
                    vt.SoLuongDat = double.Parse(SoLuongDat);
                    vt.ThoiGianGiaoHang = DateTime.ParseExact(ThoiGianGiaoHang, "dd/MM/yyyy", null);
                    List<ChiTietYeuCauKhachHang> ctc = unitofwork.Context.ChiTietYeuCauKhachHangs.Where(x => x.YeuCauKhachHangID == banghi.YeuCauKhachHangID).ToList();
                    foreach (var item in ctc)
                    {
                        var ls = new LichSuSuaChiTietYeuCau();
                        ls.LichSuSuaChiTietYeuCau1 = Guid.NewGuid();
                        ls.YeuCauKhachHangID = item.YeuCauKhachHangID;
                        ls.TenSanPham = item.TenSanPham;
                        ls.Size = item.Size;
                        ls.Mau = item.Mau;
                        ls.SoLuongDat = item.SoLuongDat;
                        ls.GiaBan = item.GiaBan;
                        ls.YeuCauKhac = item.YeuCauKhac;
                        ls.ThoiGian = DateTime.Now.Date;
                        ls.NguoiSuaID = nvid;
                        ls.DacDiem = item.DacDiem;
                        ls.ChatLieu = item.ChatLieu;
                        ls.MauPhoi = item.MauPhoi;
                        var t = unitofwork.Context.LichSuSuaChiTietYeuCaus.Where(x => x.YeuCauKhachHangID == banghi.YeuCauKhachHangID).Max(x => x.Times).GetValueOrDefault();
                        ls.Times = t + 1;
                        unitofwork.Context.LichSuSuaChiTietYeuCaus.Add(ls);
                    }
                    db.Web_ChiTietYeuCau_DeleteOldData(banghi.YeuCauKhachHangID);
                    foreach (var item in danhsachchitiet)
                    {
                        vt.NoiDung += item.TenSanPham + ": ";
                        vt.NoiDung += " - Đặc điểm: " + item.DacDiem;
                        vt.NoiDung += " - Chất liệu: " + item.ChatLieu;
                        vt.NoiDung += " - Size: ";
                        foreach (var size in danhsachsize)
                        {
                            var ct = new ChiTietYeuCauKhachHang();
                            ct.ChiTietYeuCauID = Guid.NewGuid();
                            ct.YeuCauKhachHangID = banghi.YeuCauKhachHangID;
                            ct.TenSanPham = item.TenSanPham;
                            ct.Size = size.Size;
                            ct.Mau = item.Mau;
                            ct.SoLuongDat = (int)(double.Parse(SoLuongDat) / (danhsachchitiet.Count * danhsachsize.Count));
                            ct.GiaBan = double.Parse(GiaBan);
                            ct.YeuCauKhac = item.YeuCauKhac;
                            ct.IsActive = 1;
                            ct.DacDiem = item.DacDiem;
                            ct.ChatLieu = item.ChatLieu;
                            ct.MauPhoi = item.MauPhoi;
                            unitofwork.Context.ChiTietYeuCauKhachHangs.Add(ct);
                            vt.NoiDung += size.Size + ", ";
                        }
                        vt.NoiDung += " - Màu: " + item.Mau;
                        vt.NoiDung += " - Màu phối: " + item.MauPhoi;
                        vt.NoiDung += " - Ghi chú: " + (item.YeuCauKhac == "" ? "Không có! + " : item.YeuCauKhac + " + ");
                    }
                    vt.NoiDung += " - Giá bán: " + GiaBan + " - ";
                    vt.NoiDung += " - Số lượng đặt: " + SoLuongDat + " - ";
                    vt.NoiDung += " - Thời gian giao hàng: " + DateTime.ParseExact(ThoiGianGiaoHang, "dd/MM/yyyy", null).ToString("dd/M/yyyy", CultureInfo.InvariantCulture) + " - ";
                    vt.NoiDung1 = vt.NoiDung;
                    unitofwork.Context.SaveChanges();
                    return 2;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int DeleteYeuCauKhachHang(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var banghi = new YeuCauKhachHang();
                unitofwork.Context.YeuCauKhachHangs.Find(Guid.Parse(data)).IsActive = false;
                unitofwork.Context.SaveChanges();
                return 2;
            }
            catch
            {
                return 0;
            }
        }
    }
}