﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu.QuanLyDuAn;

namespace DF.DataAccess.Repository
{
    public interface IKhoSanPhamRepository : IRepository<SanPham>
    {
        List<Web_SanPham_TonKhoResult> LayTon(string CongTrinhID);
        List<Web_SanPham_LayDanhSachResult> LayDanhSach();
        List<Web_SanPham_LichSuXuatKhoResult> LichSuXuatKho(Guid CongTrinhID, int LoaiHinhXuat, string CongTrinhDenID, string TuNgay = "", string DenNgay = "");
        List<Web_SanPham_LichSuNhapKhoResult> LichSuNhapKho(Guid CongTrinhID, int LoaiHinhNhap, string TuNgay, string DenNgay);
        int Mua(string ThoiGian, Guid DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID);
        int Xuat(string LoaiHinh, string CongTrinhDiID, string CongTrinhNhanID, string NhanVienXuatID, string DataChiTietPhieuXuat, string DSHinhanh, string ThoiGian, Guid NhanVienDuyetID);
        int CapNhatXuat(string PhieuXuatVatTuID, string LoaiHinh, string CongTrinhDiID, string CongTrinhNhanID, string NhanVienXuatID, string DataChiTietPhieuXuat, string DSHinhanh, string NoiDung, string ThoiGian, Guid NhanVienDuyetID);
        int CapNhatPhieuMua(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID);
        int TraSanPham(string PhieuNhanDonHangID, string ThoiGian, string DataChiTietXuat, string DSHinhAnh, Guid NhanVienID);
        List<Web_SanPham_LichSuNhapKhoResult> LichSuNhapKhoTheoDonHang(Guid DonHangID, string TuNgay, string DenNgay);
        List<Web_SanPham_LichSuXuatKhoTheoDonHangResult> LichSuXuatKhoTheoDonHang(Guid DonHangID, string TuNgay = "", string DenNgay = "");
        Guid GiaoHang(string lstSanPham,Guid DonHangXuatID, Guid DonHangID, Guid NhanVienID);
        int CapNhatGiaoHang(string lstSanPham, Guid PhieuNhanDonHangID, Guid NhanVienID);
    }
    public class KhoSanPhamRepository : EFRepository<SanPham>, IKhoSanPhamRepository
    {
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        public KhoSanPhamRepository(DbContext dbContext) : base(dbContext) { }
        public List<Web_SanPham_TonKhoResult> LayTon(string CongTrinhID)
        {
            db = new DBMLDFDataContext();
            List<Web_SanPham_TonKhoResult> list = db.Web_SanPham_TonKho(Guid.Parse(CongTrinhID)).ToList();
            return list;
        }
        public List<Web_SanPham_LayDanhSachResult> LayDanhSach()
        {
            db = new DBMLDFDataContext();
            var list = db.Web_SanPham_LayDanhSach().ToList();
            return list;
        }
        public List<Web_SanPham_LichSuXuatKhoResult> LichSuXuatKho(Guid CongTrinhID, int LoaiHinhXuat, string CongTrinhDenID, string TuNgay = "", string DenNgay = "")
        {
            db = new DBMLDFDataContext();
            DateTime tuNgay = new DateTime(2000, 1, 1);
            DateTime denNgay = DateTime.Now;
            if (TuNgay != "")
            {
                tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            }
            if (DenNgay != "")
            {
                denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
            }
            var data = db.Web_SanPham_LichSuXuatKho(CongTrinhID, tuNgay, denNgay).ToList();
            if (CongTrinhDenID != null && CongTrinhDenID != "")
            {
                data = data.Where(t => t.DonHangID == Guid.Parse(CongTrinhDenID)).ToList();
            }
            if (LoaiHinhXuat == 1)
            {
                data = data.Where(t => t.DonHangID != null).ToList();
            }
            if (LoaiHinhXuat == 2)
            {
                data = data.Where(t => t.DonHangID == null).ToList();
            }

            return data;
        }
        public List<Web_SanPham_LichSuNhapKhoResult> LichSuNhapKho(Guid CongTrinhID, int LoaiHinhNhap, string TuNgay, string DenNgay)
        {
            db = new DBMLDFDataContext();
            DateTime tuNgay = new DateTime(2000, 1, 1);
            DateTime denNgay = DateTime.Now;
            if (TuNgay != "")
            {
                tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            }
            if (DenNgay != "")
            {
                denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
            }
            if (LoaiHinhNhap == 0)
            {
                var data = db.Web_SanPham_LichSuNhapKho(CongTrinhID, tuNgay, denNgay).ToList();
                return data;
            }
            else if (LoaiHinhNhap == 1)
            {
                var data = db.Web_SanPham_LichSuNhapKho(CongTrinhID, tuNgay, denNgay).Where(p=>p.TenDoiTac=="").ToList();
                return data;
            }
            else
            {
                var data = db.Web_SanPham_LichSuNhapKho(CongTrinhID, tuNgay, denNgay).Where(p => p.TenDoiTac != "").ToList();
                return data;
            }
        }
        public int Mua(string ThoiGian, Guid DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID)
        {
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    unitofwork = new UnitOfWork();
                    Guid PhieuNhapSanPhamID = Guid.NewGuid();
                    var maxDeNghi = unitofwork.Context.PhieuNhapSanPhams.Max(p => p.MaPhieuNhap);
                    string maDeNghi = CreateMaDeNghi(maxDeNghi, "PNSP");
                    var thoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null);
                    var phieunhap = new PhieuNhapSanPham()
                    {
                        DonHangID = Guid.Parse(Define.CONGTRINHTONG),
                        DanhSachHinhAnhs = DSHinhAnh,
                        DoiTacID = DoiTacID,
                        IsActive = true,
                        PhieuNhapSanPhamID = PhieuNhapSanPhamID,
                        PhieuXuatSanPhamID = null,
                        ThoiGian = thoiGian,
                        MaPhieuNhap = maDeNghi,
                        NguoiTaoID= NhanVienID,
                    };
                    unitofwork.Context.PhieuNhapSanPhams.Add(phieunhap);
                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuNhapSanPhamRequest>>(DataChiTietNhap);
                    foreach (var it in list)
                    {
                        var chiTietNhap = new ChiTietPhieuNhapSanPham()
                        {
                            ChiTietPhieuNhapSanPhamID = Guid.NewGuid(),
                            GiaNhap = it.DonGia,
                            PhieuNhapSanPhamID = PhieuNhapSanPhamID,
                            SoLuong = it.SoLuong,
                            SanPhamID = it.SanPhamID,
                            IsActive=true,
                            LoaiHinh =1,
                            TinhTrang =1
                        };
                        unitofwork.Context.ChiTietPhieuNhapSanPhams.Add(chiTietNhap);
                        unitofwork.Context.SaveChanges();
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
        public int Xuat(string LoaiHinh, string CongTrinhDiID, string CongTrinhNhanID, string NhanVienXuatID, string DataChiTietPhieuXuat, string DSHinhanh, string ThoiGian, Guid NhanVienDuyetID)
        {
            unitofwork = new UnitOfWork();
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    Guid PhieuXuatVatTuID = Guid.NewGuid();
                    Guid PhieuNhapVatTuID = Guid.NewGuid();
                    var maxDeNghi = unitofwork.Context.PhieuXuatSanPhams.Max(p => p.MaPhieuXuat);
                    string maDeNghi = CreateMaDeNghi(maxDeNghi, "PXSP");
                    var maxDeNghi2 = unitofwork.Context.PhieuNhapSanPhams.Max(p => p.MaPhieuNhap);
                    string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNSP");

                    Guid deNghiCongTrinhId = Guid.NewGuid();

                    var phieuxuatSanPham = new PhieuXuatSanPham()
                    {
                        DonHangID = Guid.Parse(CongTrinhDiID),
                        DanhSachHinhAnhs = DSHinhanh,
                        IsActive = true,
                        NguoiTaoID = Guid.Parse(NhanVienXuatID),
                        PhieuXuatSanPhamID = PhieuXuatVatTuID,
                        ThoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null),
                        DonHangNhanID = Guid.Parse(CongTrinhNhanID),
                        MaPhieuXuat = maDeNghi,
                        LoaiHinh=1
                    };
                    unitofwork.Context.PhieuXuatSanPhams.Add(phieuxuatSanPham);
                    var phieunhapSanPham = new PhieuNhapSanPham()
                    {
                        DonHangID = Guid.Parse(CongTrinhNhanID),
                        DanhSachHinhAnhs = DSHinhanh,
                        IsActive = true,
                        NguoiTaoID = Guid.Parse(NhanVienXuatID),
                        PhieuXuatSanPhamID = PhieuXuatVatTuID,
                        ThoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null),
                        MaPhieuNhap = maDeNghi2,
                        PhieuNhapSanPhamID = PhieuNhapVatTuID,
                    };
                    unitofwork.Context.PhieuNhapSanPhams.Add(phieunhapSanPham);


                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuXuatSanPhamRequest>>(DataChiTietPhieuXuat);
                    foreach (var it in list)
                    {
                        var chitietxuat = new ChiTietPhieuXuatSanPham()
                        {
                            ChiTietPhieuXuatSanPhamID = Guid.NewGuid(),
                            IsActive=true,
                            SanPhamID=it.SanPhamID,
                            PhieuXuatSanPhamID = PhieuXuatVatTuID,
                            SoLuong= it.SoLuong,
                            LoaiHinh =it.LoaiHinh,
                            TinhTrang = it.TinhTrang.GetValueOrDefault()
                        };
                        unitofwork.Context.ChiTietPhieuXuatSanPhams.Add(chitietxuat);

                        var chiTietNhap = new ChiTietPhieuNhapSanPham()
                        {
                            ChiTietPhieuNhapSanPhamID = Guid.NewGuid(),
                            GiaNhap = it.DonGia.GetValueOrDefault(),
                            PhieuNhapSanPhamID = PhieuNhapVatTuID,
                            SoLuong = it.SoLuong,
                            SanPhamID = it.SanPhamID,
                            IsActive = true,
                            LoaiHinh = it.LoaiHinh,
                            TinhTrang = it.TinhTrang
                        };
                        unitofwork.Context.ChiTietPhieuNhapSanPhams.Add(chiTietNhap);
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
        public int CapNhatXuat(string PhieuXuatVatTuID, string LoaiHinh, string CongTrinhDiID, string CongTrinhNhanID, string NhanVienXuatID, string DataChiTietPhieuXuat, string DSHinhanh, string NoiDung, string ThoiGian, Guid NhanVienDuyetID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                using (TransactionScope tran = new TransactionScope())
                {
                    var pxnlid = Guid.Parse(PhieuXuatVatTuID);
                    var phieuxuatnl = unitofwork.Context.PhieuXuatSanPhams.Find(pxnlid);
                    var phieunhapnl = unitofwork.Context.PhieuNhapSanPhams.FirstOrDefault(p => p.PhieuXuatSanPhamID == pxnlid);
                    var thoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null);
                    phieuxuatnl.ThoiGian = thoiGian;
                    phieunhapnl.ThoiGian = thoiGian;
                    phieuxuatnl.DonHangNhanID = Guid.Parse(CongTrinhNhanID);
                    phieunhapnl.DonHangID = Guid.Parse(CongTrinhNhanID);
                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuXuatSanPhamRequest>>(DataChiTietPhieuXuat);
                    var lstchitietphieuxuat = unitofwork.Context.ChiTietPhieuXuatSanPhams.Where(p => p.PhieuXuatSanPhamID == pxnlid).ToList();
                    var lstchitietphieunhap = unitofwork.Context.ChiTietPhieuNhapSanPhams.Where(p => p.PhieuNhapSanPhamID == phieunhapnl.PhieuNhapSanPhamID).ToList();
                    foreach(var item in lstchitietphieuxuat){
                        var item2 = list.FirstOrDefault(p => list.Any(p2 => p2.SanPhamID == p.SanPhamID &&p.TinhTrang == p2.TinhTrang && p.LoaiHinh==p2.LoaiHinh));
                        if (item2 != null)
                        {
                            item.SoLuong = item2.SoLuong;
                        }
                    }
                    foreach (var item in lstchitietphieunhap)
                    {
                        var item2 = list.FirstOrDefault(p => list.Any(p2 => p2.SanPhamID == p.SanPhamID && p.TinhTrang == p2.TinhTrang && p.LoaiHinh == p2.LoaiHinh));
                        if (item2 != null)
                        {
                            item.SoLuong = item2.SoLuong;
                        }
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
            }
            catch
            {
                return 0;
            }

        }
        public int CapNhatPhieuMua(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    var pnvtid = Guid.Parse(PhieuNhapVatTuID);
                    var phieunhapnl = unitofwork.Context.PhieuNhapSanPhams.Find(pnvtid);
                    var thoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null);
                    phieunhapnl.ThoiGian = thoiGian;
                    phieunhapnl.DoiTacID = Guid.Parse(DoiTacID);
                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuNhapSanPhamRequest>>(DataChiTietNhap);
                    var lstchitietphieunhap = unitofwork.Context.ChiTietPhieuNhapSanPhams.Where(p => p.PhieuNhapSanPhamID == pnvtid).ToList();
                   
                    foreach (var item in lstchitietphieunhap)
                    {
                        var item2 = list.FirstOrDefault(p => list.Any(p2 => p2.SanPhamID == p.SanPhamID && p.TinhTrang == p2.TinhTrang && p.LoaiHinh == p2.LoaiHinh));
                        if (item2 != null)
                        {
                            item.SoLuong = item2.SoLuong;
                            item.GiaNhap = item2.DonGia;
                        }
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int TraSanPham(string PhieuNhanDonHangID, string ThoiGian, string DataChiTietXuat, string DSHinhAnh, Guid NhanVienID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    var pndhid = Guid.Parse(PhieuNhanDonHangID);
                    var pnsp = unitofwork.Context.PhieuNhanDonHangs.FirstOrDefault(p => p.PhieuNhanDonHangID == pndhid);
                    var maxDeNghi = unitofwork.Context.PhieuNhapSanPhams.Max(p => p.MaPhieuNhap);
                    string maDeNghi = CreateMaDeNghi(maxDeNghi, "PNSP");

                    var phieunhap = new PhieuNhapSanPham();
                    phieunhap.PhieuNhapSanPhamID = Guid.NewGuid();
                    phieunhap.ThoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null);
                    phieunhap.NguoiTaoID = NhanVienID;
                    phieunhap.MaPhieuNhap = maDeNghi;
                    phieunhap.DonHangID = pnsp.DonHangID.GetValueOrDefault();
                    phieunhap.IsActive = true;
                    phieunhap.DanhSachHinhAnhs = DSHinhAnh;
                    unitofwork.Context.PhieuNhapSanPhams.Add(phieunhap);

                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuNhapSanPhamModel>>(DataChiTietXuat);
                    foreach (var it in list)
                    {
                        var chitietnhap = new ChiTietPhieuNhapSanPham()
                        {
                            ChiTietPhieuNhapSanPhamID = Guid.NewGuid(),
                            IsActive = true,
                            SanPhamID = it.SanPhamID.GetValueOrDefault(),
                            PhieuNhapSanPhamID = phieunhap.PhieuNhapSanPhamID,
                            GiaNhap = it.DonGia,
                            SoLuong = it.SoLuong,
                            TinhTrang = 2,
                            LoaiHinh = 3,
                            LyDoHong = it.LyDoHong
                        };
                        unitofwork.Context.ChiTietPhieuNhapSanPhams.Add(chitietnhap);
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public string CreateMaDeNghi(string maxDeNghi, string prefix)
        {
            DateTime now = DateTime.Now;
            string dd = "";
            if (now.Day < 10) dd = "0" + now.Day;
            else dd = now.Day.ToString();
            string mm = "";
            if (now.Month < 10) mm = "0" + now.Month;
            else mm = now.Month.ToString();

            string yyyy = now.Year.ToString();

            string finalDate = yyyy + mm + dd;


            int numberMax = 1;
            try
            {
                string n = maxDeNghi.Replace(prefix, "");
                n = n.Substring(finalDate.Length, n.Length - finalDate.Length);//=> number
                numberMax = Convert.ToInt32(n) + 1;
            }
            catch { }
            string max = string.Empty;
            if (numberMax < 10) { max = "000" + numberMax; }
            else if (numberMax < 100) { max = "00" + numberMax; }
            else if (numberMax < 1000) { max = "0" + numberMax; }
            return prefix + finalDate + max; ;
        }
        public List<Web_SanPham_LichSuNhapKhoResult> LichSuNhapKhoTheoDonHang(Guid DonHangID, string TuNgay, string DenNgay)
        {
            db = new DBMLDFDataContext();
            DateTime tuNgay = new DateTime(2000, 1, 1);
            DateTime denNgay = DateTime.Now;
            if (TuNgay != "")
            {
                tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            }
            if (DenNgay != "")
            {
                denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
            }
            var data = db.Web_SanPham_LichSuNhapKho(DonHangID, tuNgay, denNgay).ToList();
            return data;
        }
        public List<Web_SanPham_LichSuXuatKhoTheoDonHangResult> LichSuXuatKhoTheoDonHang(Guid DonHangID, string TuNgay = "", string DenNgay = "")
        {
            db = new DBMLDFDataContext();
            DateTime tuNgay = new DateTime(2000, 1, 1);
            DateTime denNgay = DateTime.Now;
            if (TuNgay != "")
            {
                tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            }
            if (DenNgay != "")
            {
                denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
            }
            var data = db.Web_SanPham_LichSuXuatKhoTheoDonHang(DonHangID, tuNgay, denNgay).ToList();
            return data;
        }
        public Guid GiaoHang(string lstSanPham, Guid DonHangXuatID, Guid DonHangID, Guid NhanVienID)
        {
            unitofwork = new UnitOfWork();
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {

                    var maxDeNghi = unitofwork.Context.PhieuNhanDonHangs.Max(p => p.MaPhieu);
                    string maDeNghi = CreateMaDeNghi(maxDeNghi, "PGSP");
                    var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == DonHangID);
                    donhang.TrangThai = 4;
                    var chitietdonhang = unitofwork.Context.ChiTietDonHangs.Where(p => p.DonHangID == DonHangID).ToList();
                    var pndh = new PhieuNhanDonHang()
                    {
                        DonHangID = donhang.DonHangID,
                        DonHangXuatID = DonHangXuatID,
                        DanhSachHinhAnhs = "",
                        DoiTacID = donhang.DoiTacID,
                        TrangThaiDuyet =1,
                        IsActive = true,
                        NhanVienID = NhanVienID,
                        MaPhieu = maDeNghi,
                        PhieuNhanDonHangID = Guid.NewGuid(),
                        ThoiGian = DateTime.Now,
                    };
                    unitofwork.Context.PhieuNhanDonHangs.Add(pndh);


                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuXuatSanPhamRequest>>(lstSanPham);
                    foreach (var it in list)
                    {
                        var gia = chitietdonhang.FirstOrDefault(p => p.SanPhamID == it.SanPhamID);
                        gia.TrangThai = 4;
                        var chitietxuat = new ChiTietPhieuNhanDonHang()
                        {
                            ChiTietNhapDonHangID = Guid.NewGuid(),
                            SanPhamID = it.SanPhamID,
                            PhieuNhanDonHang = pndh.PhieuNhanDonHangID,
                            SoLuong = it.SoLuong,
                            DonGia = it.DonGia.GetValueOrDefault(),
                            LoaiHinh = it.LoaiHinh,
                            TinhTrang = it.TinhTrang.GetValueOrDefault(),
                            DonHangID = donhang.DonHangID
                        };
                        unitofwork.Context.ChiTietPhieuNhanDonHangs.Add(chitietxuat);
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return pndh.PhieuNhanDonHangID;
                }
                catch (Exception ex)
                {
                    return Guid.Empty;
                }
            }
        }
        public int CapNhatGiaoHang(string lstSanPham, Guid PhieuNhanDonHangID, Guid NhanVienID)
        {
            unitofwork = new UnitOfWork();
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {

                    var maxDeNghi = unitofwork.Context.PhieuNhanDonHangs.Max(p => p.MaPhieu);
                    string maDeNghi = CreateMaDeNghi(maxDeNghi, "PGSP");
                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuXuatSanPhamRequest>>(lstSanPham);
                    var lst = unitofwork.Context.ChiTietPhieuNhanDonHangs.Where(p => p.PhieuNhanDonHang == PhieuNhanDonHangID).ToList();
                    foreach (var it in list)
                    {
                       var ct = lst.FirstOrDefault(p=>p.SanPhamID==it.SanPhamID);
                       if (ct != null)
                       {
                           ct.SoLuong = it.SoLuong;
                           ct.DonGia = it.DonGia.GetValueOrDefault();
                       }
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
    }
}
