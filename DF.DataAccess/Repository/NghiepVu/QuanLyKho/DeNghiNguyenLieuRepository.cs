﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DataAccess.DBML;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu.QuanLyDuAn;
using DF.DBMapping.ModelsExt.Enums.CongTrinh;
using DF.DBMapping.ModelsExt.NghiepVu.VatTu;

namespace DF.DataAccess.Repository
{
    public interface IDeNghiNguyenLieuRepository : IRepository<DeNghiNguyenLieu>
    {
        string TaoDeNghi(DeNghiNguyenLieuModel model, Guid NhanVienID);
         string DuyetDeNghi(DeNghiNguyenLieuModel model, Guid NhanVienID);
         string XuatNguyenLieu(DeNghiNguyenLieuModel model, Guid NhanVienID);
         string TuChoi(DeNghiNguyenLieuModel model, Guid NhanVienID);
         List<Web_NguyenLieu_LayDanhSachDeNghiNguyenLieuResult> LayDanhSachDeNghiNguyenLieu(Guid NhanVienID, Guid DonHangID, Guid SanPhamID, DateTime TuNgay, DateTime DenNgay, int trangthai = 1000);
         List<Web_NguyenLieu_LayDanhSachNguyenLieuDuocDeNghiResult> LayDanhSachNguyenLieuDuocDeNghi(Guid DeNghiNguyenLieuID);
         List<Web_NguyenLieu_LayDanhSachDuyetDeNghiNguyenLieuResult> LayDanhSachDuyetDeNghiNguyenLieu(Guid DeNghiNguyenLieuID);
         List<Web_NguyenLieu_LayDanhSachNguyenLieuDaXuatResult> LayDanhSachNguyenLieuDaXuat(Guid DeNghiNguyenLieuID);

    }
    public class DeNghiNguyenLieuRepository : EFRepository<DeNghiNguyenLieu>, IDeNghiNguyenLieuRepository
    {
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        public DeNghiNguyenLieuRepository(DbContext dbContext) : base(dbContext) { }
        public string CreateMaDeNghi(string maxDeNghi, string prefix)
        {
            DateTime now = DateTime.Now;
            string dd = "";
            if (now.Day < 10) dd = "0" + now.Day;
            else dd = now.Day.ToString();
            string mm = "";
            if (now.Month < 10) mm = "0" + now.Month;
            else mm = now.Month.ToString();

            string yyyy = now.Year.ToString();

            string finalDate = yyyy + mm + dd;


            int numberMax = 1;
            try
            {
                string n = maxDeNghi.Replace(prefix, "");
                n = n.Substring(finalDate.Length, n.Length - finalDate.Length);//=> number
                numberMax = Convert.ToInt32(n) + 1;
            }
            catch { }
            string max = string.Empty;
            if (numberMax < 10) { max = "000" + numberMax; }
            else if (numberMax < 100) { max = "00" + numberMax; }
            else if (numberMax < 1000) { max = "0" + numberMax; }
            return prefix + finalDate + max; ;
        }
        #region Luu De Nghi

        //Tạo đề nghị : 0
        public string TaoDeNghi(DeNghiNguyenLieuModel model, Guid NhanVienID)
        {
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    unitofwork = new UnitOfWork();
                    var nhanvien = unitofwork.Context.NhanViens.FirstOrDefault(p => p.NhanVienID == NhanVienID);
                    Guid PhieuNhapNguyenLieuID = Guid.NewGuid();
                    var maxDeNghi = unitofwork.Context.DeNghiNguyenLieux.Max(p => p.MaDeNghi);
                    string maDeNghi = CreateMaDeNghi(maxDeNghi, "DNNL");
                    Guid DeNghiNguyenLieuID = Guid.NewGuid();
                    var thoiGian = DateTime.Now;
                    List<LichSuDeNghiModel> lichSuDeNghis = new List<LichSuDeNghiModel>();
                    lichSuDeNghis.Add(new LichSuDeNghiModel()
                    {
                        NhanVienID = NhanVienID.ToString(),
                        TenNhanVien = nhanvien.TenNhanVien,
                        ThoiGian = thoiGian.ToString("dd-MM-yyyy HH:mm:ss"),
                        TrangThai = 1
                    });
                    var deNghiCongTrinh = new DeNghiNguyenLieu()
                    {
                        DonHangID = model.DonHangID.GetValueOrDefault(),
                        DeNghiNguyenLieuID = DeNghiNguyenLieuID,
                        SanPhamID = model.SanPhamID.GetValueOrDefault(),
                        LichSuDeNghis = JsonConvert.SerializeObject(lichSuDeNghis),
                        LoaiHinh = 1,
                        MaDeNghi = maDeNghi,
                        NhanVienID = NhanVienID,
                        ThoiGian = thoiGian,
                        TrangThai = 1,
                        NoiDung = model.NoiDung
                    };
                    unitofwork.Context.DeNghiNguyenLieux.Add(deNghiCongTrinh);
                    // Tạo chi tiết phiếu đề nghị
                    foreach(var item in model.items){
                        var ctnxvt = new ChiTietDeNghiNguyenLieu()
                        {
                            DonGia = item.DonGia.GetValueOrDefault(),
                            ChiTietDeNghiNguyenLieuID = Guid.NewGuid(),
                            SoLuong = item.SoLuong.GetValueOrDefault(),
                            NguyenLieuID = item.NguyenLieuID.GetValueOrDefault(),
                            DeNghiNguyenLieuID = deNghiCongTrinh.DeNghiNguyenLieuID,
                        };
                        unitofwork.Context.ChiTietDeNghiNguyenLieux.Add(ctnxvt);
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return "success_Đề nghị thành công";
                }
                catch (Exception ex)
                {
                    return "error_Có lỗi trong quá trình xử lý dữ liệu";
                }
            }
        }
        //Duyệt đề nghị :1
        public string DuyetDeNghi(DeNghiNguyenLieuModel model, Guid NhanVienID)
        {
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    unitofwork = new UnitOfWork();
                    var nhanvien = unitofwork.Context.NhanViens.FirstOrDefault(p => p.NhanVienID == NhanVienID);
                    var thoiGian = DateTime.Now;
                    var denghicongtrinh = unitofwork.Context.DeNghiNguyenLieux.FirstOrDefault(p => p.DeNghiNguyenLieuID == model.DeNghiNguyenLieuID);
                    var chitietdenghinguyenlieu = unitofwork.Context.ChiTietDeNghiNguyenLieux.Where(p => p.DeNghiNguyenLieuID == model.DeNghiNguyenLieuID);
                    if (denghicongtrinh.TrangThai == (int)DeNghi_TrangThaiEnums.CHUA_DUYET)
                    {
                        List<LichSuDeNghiModel> lichSuDeNghis = JsonConvert.DeserializeObject<List<LichSuDeNghiModel>>(denghicongtrinh.LichSuDeNghis);
                        lichSuDeNghis.Add(new LichSuDeNghiModel()
                        {
                            NhanVienID = NhanVienID.ToString(),
                            TenNhanVien = nhanvien.TenNhanVien,
                            ThoiGian = thoiGian.ToString("dd-MM-yyyy HH:mm:ss"),
                            TrangThai = 2
                        });
                        denghicongtrinh.TrangThai = 2;
                        denghicongtrinh.LichSuDeNghis = JsonConvert.SerializeObject(lichSuDeNghis);
                        denghicongtrinh.ThoiGianDuyet = DateTime.Now;
                        denghicongtrinh.NguoiDuyet = NhanVienID;
                        // Tạo chi tiết phiếu đề nghị
                        foreach (var item in chitietdenghinguyenlieu)
                        {
                            var it = model.items.FirstOrDefault(p=>p.NguyenLieuID==item.NguyenLieuID);
                            if (it!=null)
                            {
                                item.SoLuongDuyet = it.SoLuong;
                            }
                        }
                    }
                    else
                    {
                        return "error_Phiếu này đã được duyệt";
                    }
                    
                    unitofwork.Save();
                    tran.Complete();
                    return "success_Đề nghị thành công";
                }
                catch (Exception ex)
                {
                     return "error_Có lỗi trong quá trình xử lý dữ liệu";
                }
            }
        }
        public string XuatNguyenLieu(DeNghiNguyenLieuModel model, Guid NhanVienID)
        {
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    unitofwork = new UnitOfWork();
                    var nhanvien = unitofwork.Context.NhanViens.FirstOrDefault(p => p.NhanVienID == NhanVienID);
                    var thoiGian = DateTime.Now;
                    var denghicongtrinh = unitofwork.Context.DeNghiNguyenLieux.FirstOrDefault(p => p.DeNghiNguyenLieuID == model.DeNghiNguyenLieuID);

                    if (denghicongtrinh.TrangThai == (int)DeNghi_TrangThaiEnums.DA_DUYET || denghicongtrinh.TrangThai == (int)DeNghi_TrangThaiEnums.DA_XUAT)
                    {
                        List<LichSuDeNghiModel> lichSuDeNghis = JsonConvert.DeserializeObject<List<LichSuDeNghiModel>>(denghicongtrinh.LichSuDeNghis);
                        lichSuDeNghis.Add(new LichSuDeNghiModel()
                        {
                            NhanVienID = NhanVienID.ToString(),
                            TenNhanVien = nhanvien.TenNhanVien,
                            ThoiGian = thoiGian.ToString("dd-MM-yyyy HH:mm:ss"),
                            TrangThai = (int)DeNghi_TrangThaiEnums.DA_XUAT
                        });
                        denghicongtrinh.TrangThai = (int)DeNghi_TrangThaiEnums.DA_XUAT;
                        denghicongtrinh.LichSuDeNghis = JsonConvert.SerializeObject(lichSuDeNghis);

                        Guid PhieuXuatVatTuID = Guid.NewGuid();
                        Guid PhieuNhapVatTuID = Guid.NewGuid();
                        var maxDeNghi = unitofwork.Context.PhieuXuatNguyenLieux.Max(p => p.MaPhieuXuat);
                        string maDeNghi = CreateMaDeNghi(maxDeNghi, "PXNL");
                        var maxDeNghi2 = unitofwork.Context.PhieuNhapNguyenLieux.Max(p => p.MaPhieuNhap);
                        string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNNL");

                        Guid DeNghiNguyenLieuID = Guid.NewGuid();

                        var phieuxuatnguyenlieu = new PhieuXuatNguyenLieu()
                        {
                            DonHangID = Guid.Parse(Define.CONGTRINHTONG),
                            IsActive = true,
                            NguoiTaoID = NhanVienID,
                            PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                            ThoiGian = DateTime.Now,
                            DonHangNhanID = model.DonHangID.GetValueOrDefault(),
                            MaPhieuXuat = maDeNghi,
                            DeNghiNguyenLieuID =denghicongtrinh.DeNghiNguyenLieuID,
                            LoaiHinh = 1
                        };
                        unitofwork.Context.PhieuXuatNguyenLieux.Add(phieuxuatnguyenlieu);
                        var phieunhapnguyenlieu = new PhieuNhapNguyenLieu()
                        {
                            DonHangID = model.DonHangID.GetValueOrDefault(),
                            IsActive = true,
                            NguoiTaoID = NhanVienID,
                            PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                            ThoiGian = DateTime.Now,
                            DeNghiNguyenLieuID = denghicongtrinh.DeNghiNguyenLieuID,
                            MaPhieuNhap = maDeNghi2,
                            PhieuNhapNguyenLieuID = PhieuNhapVatTuID,
                        };
                        unitofwork.Context.PhieuNhapNguyenLieux.Add(phieunhapnguyenlieu);
                        foreach (var it in model.items)
                        {
                            var chitietxuat = new ChiTietPhieuXuatNguyenLieu()
                            {
                                ChiTietPhieuXuatNguyenLieuID = Guid.NewGuid(),
                                IsActive = true,
                                NguyenLieuID = it.NguyenLieuID.GetValueOrDefault(),
                                PhieuXuatNguyenLieu = PhieuXuatVatTuID,
                                SoLuong = it.SoLuong,
                            };
                            unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Add(chitietxuat);

                            var chiTietNhap = new ChiTietPhieuNhapNguyenLieu()
                            {
                                ChiTietPhieuNhapNguyenLieuID = Guid.NewGuid(),
                                GiaNhap = it.DonGia.GetValueOrDefault(),
                                PhieuNhapNguyenLieuID = PhieuNhapVatTuID,
                                SoLuong = it.SoLuong.GetValueOrDefault(),
                                NguyenLieuID = it.NguyenLieuID.GetValueOrDefault(),
                                IsActive = true,
                            };
                            unitofwork.Context.ChiTietPhieuNhapNguyenLieux.Add(chiTietNhap);

                        }
                    }
                    else
                    {
                        return "error_Phiếu này đã được xuất";
                    }
                   
                    unitofwork.Save();
                    tran.Complete();
                    return "success_Xuất thành công";
                }
                catch (Exception ex)
                {
                    return "error_Có lỗi trong quá trình xử lý dữ liệu";
                }
            }
        }

        public string TuChoi(DeNghiNguyenLieuModel model, Guid NhanVienID)
        {
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    unitofwork = new UnitOfWork();
                    var nhanvien = unitofwork.Context.NhanViens.FirstOrDefault(p => p.NhanVienID == NhanVienID);
                    var thoiGian = DateTime.Now;
                    var denghicongtrinh = unitofwork.Context.DeNghiNguyenLieux.FirstOrDefault(p => p.DeNghiNguyenLieuID == model.DeNghiNguyenLieuID);
                    List<LichSuDeNghiModel> lichSuDeNghis = JsonConvert.DeserializeObject<List<LichSuDeNghiModel>>(denghicongtrinh.LichSuDeNghis);
                    lichSuDeNghis.Add(new LichSuDeNghiModel()
                    {
                        NhanVienID = NhanVienID.ToString(),
                        TenNhanVien = nhanvien.TenNhanVien,
                        ThoiGian = thoiGian.ToString("dd-MM-yyyy HH:mm:ss"),
                        TrangThai = (int)DeNghi_TrangThaiEnums.TU_CHOI
                    });
                    denghicongtrinh.TrangThai = (int)DeNghi_TrangThaiEnums.TU_CHOI;
                    //denghicongtrinh.NoiDungTuChoi = model.NoiDung;
                    denghicongtrinh.LichSuDeNghis = JsonConvert.SerializeObject(lichSuDeNghis);
                    unitofwork.Save();
                    tran.Complete();
                    return "success_Từ chối thành công";
                }
                catch (Exception ex)
                {
                    return "error_Có lỗi trong quá trình xử lý dữ liệu";
                }
            }
        }

        #endregion

        #region Lay Danh Sach

        public List<Web_NguyenLieu_LayDanhSachDeNghiNguyenLieuResult> LayDanhSachDeNghiNguyenLieu(Guid NhanVienID,Guid DonHangID,Guid SanPhamID,DateTime TuNgay,DateTime DenNgay,int trangthai = 1000)
        {

            db = new DBMLDFDataContext();
            var unitofwork = new UnitOfWork();
            var user = unitofwork.Context.Users.FirstOrDefault(p=>p.NhanVienId==NhanVienID);
            var nhanvienquyens = (from a in unitofwork.Context.tblGroupUsers
                                  join b in unitofwork.Context.tblGroups on a.GroupGuid equals b.GroupGuid
                                  where a.UserGuid == user.UserId
                                  select b.RuleType).Distinct();
            return db.Web_NguyenLieu_LayDanhSachDeNghiNguyenLieu(trangthai, DonHangID, SanPhamID, TuNgay, DenNgay).ToList();
        }

        public List<Web_NguyenLieu_LayDanhSachNguyenLieuDuocDeNghiResult> LayDanhSachNguyenLieuDuocDeNghi(Guid DeNghiNguyenLieuID)
        {
            unitofwork = new UnitOfWork();
            var denghicongtrinh = unitofwork.Context.DeNghiNguyenLieux.FirstOrDefault(p => p.DeNghiNguyenLieuID == DeNghiNguyenLieuID);
            unitofwork.Dispose();
            db = new DBMLDFDataContext();
            return db.Web_NguyenLieu_LayDanhSachNguyenLieuDuocDeNghi(denghicongtrinh.DeNghiNguyenLieuID).ToList();

        }
        public List<Web_NguyenLieu_LayDanhSachDuyetDeNghiNguyenLieuResult> LayDanhSachDuyetDeNghiNguyenLieu(Guid DeNghiNguyenLieuID)
        {
            unitofwork = new UnitOfWork();
            var denghicongtrinh = unitofwork.Context.DeNghiNguyenLieux.FirstOrDefault(p => p.DeNghiNguyenLieuID == DeNghiNguyenLieuID);
            unitofwork.Dispose();
            db = new DBMLDFDataContext();
            return db.Web_NguyenLieu_LayDanhSachDuyetDeNghiNguyenLieu(DeNghiNguyenLieuID).ToList();

        }

        public List<Web_NguyenLieu_LayDanhSachNguyenLieuDaXuatResult> LayDanhSachNguyenLieuDaXuat(Guid DeNghiNguyenLieuID)
        {
            unitofwork = new UnitOfWork();
            var denghicongtrinh = unitofwork.Context.DeNghiNguyenLieux.FirstOrDefault(p => p.DeNghiNguyenLieuID == DeNghiNguyenLieuID);
            unitofwork.Dispose();
            db = new DBMLDFDataContext();
            return db.Web_NguyenLieu_LayDanhSachNguyenLieuDaXuat(DeNghiNguyenLieuID).ToList();

        }
       
        #endregion

    }
}
