﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu.QuanLyDuAn;

namespace DF.DataAccess.Repository
{
    public interface IKhoNguyenLieuRepository : IRepository<NguyenLieu>
    {
        List<Web_NguyenLieu_TonKhoResult> LayTon(string CongTrinhID);
        List<Web_NguyenLieu_LayDanhSachResult> LayDanhSach();
        List<Web_NguyenLieu_LichSuXuatKhoResult> LichSuXuatKho(Guid CongTrinhID, int LoaiHinhXuat, string CongTrinhDenID, string TuNgay = "", string DenNgay = "");
        List<Web_NguyenLieu_LichSuNhapKhoResult> LichSuNhapKho(Guid CongTrinhID, int LoaiHinhNhap, string TuNgay, string DenNgay);
        int Mua(string ThoiGian, Guid DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID);
        int Xuat(string LoaiHinh, string CongTrinhDiID, string CongTrinhNhanID, string NhanVienXuatID, string DataChiTietPhieuXuat, string DSHinhanh, string ThoiGian, Guid NhanVienDuyetID);
        int CapNhatXuat(string PhieuXuatVatTuID, string LoaiHinh, string CongTrinhDiID, string CongTrinhNhanID, string NhanVienXuatID, string DataChiTietPhieuXuat, string DSHinhanh, string NoiDung, string ThoiGian, Guid NhanVienDuyetID);
        int CapNhatPhieuMua(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID);
        int TraNguyenLieu(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID);
        int CapNhatTraNguyenLieu(string PhieuXuatVatTuID, string ThoiGian, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID);
        List<Web_NguyenLieu_GetDataLichSuNhapTheoDonHangResult> LichSuNhapKhoTheoDonHang(Guid DonHangID, string TuNgay, string DenNgay);
        List<Web_NguyenLieu_LichSuXuatKhoResult> LichSuXuatKhoTheoDonHang(Guid CongTrinhID, string CongTrinhDenID, string TuNgay = "", string DenNgay = "");
    }
    public class KhoNguyenLieuRepository : EFRepository<NguyenLieu>, IKhoNguyenLieuRepository
    {
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        public KhoNguyenLieuRepository(DbContext dbContext) : base(dbContext) { }
        public List<Web_NguyenLieu_TonKhoResult> LayTon(string CongTrinhID)
        {
            db = new DBMLDFDataContext();
            List<Web_NguyenLieu_TonKhoResult> list = db.Web_NguyenLieu_TonKho(Guid.Parse(CongTrinhID)).ToList();
            return list;
        }
        public List<Web_NguyenLieu_LayDanhSachResult> LayDanhSach()
        {
            db = new DBMLDFDataContext();
            var list = db.Web_NguyenLieu_LayDanhSach().ToList();
            return list;
        }
        public List<Web_NguyenLieu_LichSuXuatKhoResult> LichSuXuatKho(Guid CongTrinhID, int LoaiHinhXuat, string CongTrinhDenID, string TuNgay = "", string DenNgay = "")
        {
            db = new DBMLDFDataContext();
            DateTime tuNgay = new DateTime(2000, 1, 1);
            DateTime denNgay = DateTime.Now;
            if (TuNgay != "")
            {
                tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            }
            if (DenNgay != "")
            {
                denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
            }
            var data = db.Web_NguyenLieu_LichSuXuatKho(CongTrinhID, tuNgay, denNgay).ToList();
            if (CongTrinhDenID != null && CongTrinhDenID != "")
            {
                data = data.Where(t => t.DonHangNhanID == Guid.Parse(CongTrinhDenID)).ToList();
            }
            if (LoaiHinhXuat == 1)
            {
                data = data.Where(t => t.DonHangNhanID != null).ToList();
            }
            if (LoaiHinhXuat == 2)
            {
                data = data.Where(t => t.DonHangNhanID == null).ToList();
            }

            return data;
        }
        public List<Web_NguyenLieu_LichSuNhapKhoResult> LichSuNhapKho(Guid CongTrinhID, int LoaiHinhNhap, string TuNgay, string DenNgay)
        {
            db = new DBMLDFDataContext();
            DateTime tuNgay = new DateTime(2000, 1, 1);
            DateTime denNgay = DateTime.Now;
            if (TuNgay != "")
            {
                tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            }
            if (DenNgay != "")
            {
                denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
            }
            if (LoaiHinhNhap == 0)
            {
                var data = db.Web_NguyenLieu_LichSuNhapKho(CongTrinhID, tuNgay, denNgay).ToList();
                return data;
            }
            else if (LoaiHinhNhap == 1)
            {
                var data = db.Web_NguyenLieu_LichSuNhapKho(CongTrinhID, tuNgay, denNgay).Where(p => p.TenDoiTac == "").ToList();
                return data;
            }
            else
            {
                var data = db.Web_NguyenLieu_LichSuNhapKho(CongTrinhID, tuNgay, denNgay).Where(p => p.TenDoiTac != "").ToList();
                return data;
            }
        }
        public int Mua(string ThoiGian, Guid DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID)
        {
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    unitofwork = new UnitOfWork();
                    Guid PhieuNhapNguyenLieuID = Guid.NewGuid();
                    var maxDeNghi = unitofwork.Context.PhieuNhapNguyenLieux.Max(p => p.MaPhieuNhap);
                    string maDeNghi = CreateMaDeNghi(maxDeNghi, "PNNL");
                    var thoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null);
                    var phieunhap = new PhieuNhapNguyenLieu()
                    {
                        DonHangID = Guid.Parse(Define.CONGTRINHTONG),
                        DanhSachHinhAnhs = DSHinhAnh,
                        DoiTacID = DoiTacID,
                        IsActive = true,
                        PhieuNhapNguyenLieuID = PhieuNhapNguyenLieuID,
                        PhieuXuatNguyenLieuID = null,
                        ThoiGian = thoiGian,
                        MaPhieuNhap = maDeNghi,
                        NguoiTaoID = NhanVienID,
                    };
                    unitofwork.Context.PhieuNhapNguyenLieux.Add(phieunhap);
                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuNhapNguyenLieuRequest>>(DataChiTietNhap);
                    foreach (var it in list)
                    {
                        var chiTietNhap = new ChiTietPhieuNhapNguyenLieu()
                        {
                            ChiTietPhieuNhapNguyenLieuID = Guid.NewGuid(),
                            GiaNhap = it.DonGia,
                            PhieuNhapNguyenLieuID = PhieuNhapNguyenLieuID,
                            SoLuong = it.SoLuong,
                            NguyenLieuID = it.NguyenLieuID,
                            DonViID = it.DonViID,
                            IsActive = true,
                        };
                        unitofwork.Context.ChiTietPhieuNhapNguyenLieux.Add(chiTietNhap);
                        unitofwork.Context.SaveChanges();
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
        public int Xuat(string LoaiHinh, string CongTrinhDiID, string CongTrinhNhanID, string NhanVienXuatID, string DataChiTietPhieuXuat, string DSHinhanh, string ThoiGian, Guid NhanVienDuyetID)
        {
            unitofwork = new UnitOfWork();
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    Guid PhieuXuatVatTuID = Guid.NewGuid();
                    Guid PhieuNhapVatTuID = Guid.NewGuid();
                    var maxDeNghi = unitofwork.Context.PhieuXuatNguyenLieux.Max(p => p.MaPhieuXuat);
                    string maDeNghi = CreateMaDeNghi(maxDeNghi, "PXNL");
                    var maxDeNghi2 = unitofwork.Context.PhieuNhapNguyenLieux.Max(p => p.MaPhieuNhap);
                    string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNNL");

                    Guid deNghiCongTrinhId = Guid.NewGuid();

                    var phieuxuatnguyenlieu = new PhieuXuatNguyenLieu()
                    {
                        DonHangID = Guid.Parse(CongTrinhDiID),
                        DanhSachHinhAnhs = DSHinhanh,
                        IsActive = true,
                        NguoiTaoID = Guid.Parse(NhanVienXuatID),
                        PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                        ThoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null),
                        DonHangNhanID = Guid.Parse(CongTrinhNhanID),
                        MaPhieuXuat = maDeNghi,
                        LoaiHinh = 1
                    };
                    unitofwork.Context.PhieuXuatNguyenLieux.Add(phieuxuatnguyenlieu);
                    var phieunhapnguyenlieu = new PhieuNhapNguyenLieu()
                    {
                        DonHangID = Guid.Parse(CongTrinhNhanID),
                        DanhSachHinhAnhs = DSHinhanh,
                        IsActive = true,
                        NguoiTaoID = Guid.Parse(NhanVienXuatID),
                        PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                        ThoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null),
                        MaPhieuNhap = maDeNghi2,
                        PhieuNhapNguyenLieuID = PhieuNhapVatTuID,
                    };
                    unitofwork.Context.PhieuNhapNguyenLieux.Add(phieunhapnguyenlieu);


                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuXuatNguyenLieuRequest>>(DataChiTietPhieuXuat);
                    foreach (var it in list)
                    {
                        var chitietxuat = new ChiTietPhieuXuatNguyenLieu()
                        {
                            ChiTietPhieuXuatNguyenLieuID = Guid.NewGuid(),
                            IsActive = true,
                            NguyenLieuID = it.NguyenLieuID,
                            PhieuXuatNguyenLieu = PhieuXuatVatTuID,
                            SoLuong = it.SoLuong,
                        };
                        unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Add(chitietxuat);

                        var chiTietNhap = new ChiTietPhieuNhapNguyenLieu()
                        {
                            ChiTietPhieuNhapNguyenLieuID = Guid.NewGuid(),
                            GiaNhap = it.DonGia.GetValueOrDefault(),
                            PhieuNhapNguyenLieuID = PhieuNhapVatTuID,
                            SoLuong = it.SoLuong,
                            NguyenLieuID = it.NguyenLieuID,
                            IsActive = true,
                        };
                        unitofwork.Context.ChiTietPhieuNhapNguyenLieux.Add(chiTietNhap);
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
        public int CapNhatXuat(string PhieuXuatVatTuID, string LoaiHinh, string CongTrinhDiID, string CongTrinhNhanID, string NhanVienXuatID, string DataChiTietPhieuXuat, string DSHinhanh, string NoiDung, string ThoiGian, Guid NhanVienDuyetID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                using (TransactionScope tran = new TransactionScope())
                {
                    var pxnlid = Guid.Parse(PhieuXuatVatTuID);
                    var phieuxuatnl = unitofwork.Context.PhieuXuatNguyenLieux.Find(pxnlid);
                    var phieunhapnl = unitofwork.Context.PhieuNhapNguyenLieux.FirstOrDefault(p => p.PhieuXuatNguyenLieuID == pxnlid);
                    var thoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null);
                    phieuxuatnl.ThoiGian = thoiGian;
                    phieunhapnl.ThoiGian = thoiGian;
                    phieuxuatnl.DonHangNhanID = Guid.Parse(CongTrinhNhanID);
                    phieunhapnl.DonHangID = Guid.Parse(CongTrinhNhanID);
                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuXuatNguyenLieuRequest>>(DataChiTietPhieuXuat);
                    var lstchitietphieuxuat = unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Where(p => p.PhieuXuatNguyenLieu == pxnlid).ToList();
                    var lstchitietphieunhap = unitofwork.Context.ChiTietPhieuNhapNguyenLieux.Where(p => p.PhieuNhapNguyenLieuID == phieunhapnl.PhieuNhapNguyenLieuID).ToList();
                    foreach (var item in lstchitietphieuxuat)
                    {
                        var item2 = list.FirstOrDefault(p => list.Any(p2 => p2.NguyenLieuID == p.NguyenLieuID));
                        if (item2 != null)
                        {
                            item.SoLuong = item2.SoLuong;
                        }
                    }
                    foreach (var item in lstchitietphieunhap)
                    {
                        var item2 = list.FirstOrDefault(p => list.Any(p2 => p2.NguyenLieuID == p.NguyenLieuID));
                        if (item2 != null)
                        {
                            item.SoLuong = item2.SoLuong;
                        }
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
            }
            catch
            {
                return 0;
            }

        }
        public int CapNhatPhieuMua(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    var pnvtid = Guid.Parse(PhieuNhapVatTuID);
                    var phieunhapnl = unitofwork.Context.PhieuNhapNguyenLieux.Find(pnvtid);
                    var thoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null);
                    phieunhapnl.ThoiGian = thoiGian;
                    phieunhapnl.DoiTacID = Guid.Parse(DoiTacID);
                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuNhapNguyenLieuRequest>>(DataChiTietNhap);
                    var lstchitietphieunhap = unitofwork.Context.ChiTietPhieuNhapNguyenLieux.Where(p => p.PhieuNhapNguyenLieuID == pnvtid).ToList();
                    foreach(var item in lstchitietphieunhap)
                    {
                        for(int i = 0; i < list.Count; i++)
                        {
                            if(item.NguyenLieuID == list[i].NguyenLieuID)
                            {
                                var item2 = list.FirstOrDefault(p => p.NguyenLieuID == list[i].NguyenLieuID);
                                item.SoLuong = item2.SoLuong;
                                item.GiaNhap = item2.DonGia;
                                break;
                            }
                        }
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int TraNguyenLieu(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    var pnvtid = Guid.Parse(PhieuNhapVatTuID);
                    var pnnl = unitofwork.Context.PhieuNhapNguyenLieux.FirstOrDefault(p => p.PhieuNhapNguyenLieuID == pnvtid);
                    Guid PhieuXuatVatTuID = Guid.NewGuid();
                    var maxDeNghi = unitofwork.Context.PhieuXuatNguyenLieux.Max(p => p.MaPhieuXuat);
                    string maDeNghi = CreateMaDeNghi(maxDeNghi, "PXNL");

                    Guid deNghiCongTrinhId = Guid.NewGuid();

                    var phieuxuatnguyenlieu = new PhieuXuatNguyenLieu()
                    {
                        DonHangID = pnnl.DonHangID,
                        DanhSachHinhAnhs = DSHinhAnh,
                        IsActive = true,
                        NguoiTaoID = NhanVienID,
                        PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                        ThoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null),
                        DoiTacID = Guid.Parse(DoiTacID),
                        MaPhieuXuat = maDeNghi,
                        PhieuNhapNguyenLieuID = pnvtid,
                        LoaiHinh = 2
                    };
                    unitofwork.Context.PhieuXuatNguyenLieux.Add(phieuxuatnguyenlieu);

                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuXuatNguyenLieuRequest>>(DataChiTietNhap);
                    foreach (var it in list)
                    {
                        var chitietxuat = new ChiTietPhieuXuatNguyenLieu()
                        {
                            ChiTietPhieuXuatNguyenLieuID = Guid.NewGuid(),
                            IsActive = true,
                            NguyenLieuID = it.NguyenLieuID,
                            PhieuXuatNguyenLieu = PhieuXuatVatTuID,
                            SoLuong = it.SoLuong,
                        };
                        unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Add(chitietxuat);
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1; ;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int CapNhatTraNguyenLieu(string PhieuXuatVatTuID, string ThoiGian, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                using (TransactionScope tran = new TransactionScope())
                {
                    var pxnlid = Guid.Parse(PhieuXuatVatTuID);
                    var phieuxuatnl = unitofwork.Context.PhieuXuatNguyenLieux.Find(pxnlid);
                    var thoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null);
                    phieuxuatnl.ThoiGian = thoiGian; ;
                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuXuatNguyenLieuRequest>>(DataChiTietNhap);
                    var lstchitietphieuxuat = unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Where(p => p.PhieuXuatNguyenLieu == pxnlid).ToList();
                    foreach (var item in lstchitietphieuxuat)
                    {
                        var item2 = list.FirstOrDefault(p => list.Any(p2 => p2.NguyenLieuID == p.NguyenLieuID));
                        if (item2 != null)
                        {
                            item.SoLuong = item2.SoLuong;
                        }
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
            }
            catch
            {
                return 0;
            }

        }
        public string CreateMaDeNghi(string maxDeNghi, string prefix)
        {
            DateTime now = DateTime.Now;
            string dd = "";
            if (now.Day < 10) dd = "0" + now.Day;
            else dd = now.Day.ToString();
            string mm = "";
            if (now.Month < 10) mm = "0" + now.Month;
            else mm = now.Month.ToString();

            string yyyy = now.Year.ToString();

            string finalDate = yyyy + mm + dd;


            int numberMax = 1;
            try
            {
                string n = maxDeNghi.Replace(prefix, "");
                n = n.Substring(finalDate.Length, n.Length - finalDate.Length);//=> number
                numberMax = Convert.ToInt32(n) + 1;
            }
            catch { }
            string max = string.Empty;
            if (numberMax < 10) { max = "000" + numberMax; }
            else if (numberMax < 100) { max = "00" + numberMax; }
            else if (numberMax < 1000) { max = "0" + numberMax; }
            return prefix + finalDate + max; ;
        }

        public List<Web_NguyenLieu_GetDataLichSuNhapTheoDonHangResult> LichSuNhapKhoTheoDonHang(Guid DonHangID, string TuNgay, string DenNgay)
        {
            db = new DBMLDFDataContext();
            DateTime tuNgay = new DateTime(2000, 1, 1);
            DateTime denNgay = DateTime.Now;
            if (TuNgay != "")
            {
                tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            }
            if (DenNgay != "")
            {
                denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
            }
            var data = db.Web_NguyenLieu_GetDataLichSuNhapTheoDonHang(DonHangID, tuNgay, denNgay).ToList();
            return data;
        }
        public List<Web_NguyenLieu_LichSuXuatKhoResult> LichSuXuatKhoTheoDonHang(Guid CongTrinhID, string CongTrinhDenID, string TuNgay = "", string DenNgay = "")
        {
            db = new DBMLDFDataContext();
            DateTime tuNgay = new DateTime(2000, 1, 1);
            DateTime denNgay = DateTime.Now;
            if (TuNgay != "")
            {
                tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            }
            if (DenNgay != "")
            {
                denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
            }
            var data = db.Web_NguyenLieu_LichSuXuatKho(Guid.Parse(CongTrinhDenID), tuNgay, denNgay).ToList();
           
            return data;
        }
    }
}
