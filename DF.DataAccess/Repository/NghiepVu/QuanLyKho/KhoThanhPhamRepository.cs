﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu.QuanLyDuAn;

namespace DF.DataAccess.Repository
{
    public interface IKhoThanhPhamRepository : IRepository<ThanhPham>
    {
        List<Web_ThanhPham_TonKhoResult> LayTon(string CongTrinhID);
        List<Web_ThanhPham_LayDanhSachResult> LayDanhSach();
        List<Web_ThanhPham_LichSuXuatKhoResult> LichSuXuatKho(Guid CongTrinhID, int LoaiHinhXuat, string CongTrinhDenID, string TuNgay = "", string DenNgay = "");
        List<Web_ThanhPham_LichSuNhapKhoResult> LichSuNhapKho(Guid CongTrinhID, int LoaiHinhNhap, string TuNgay, string DenNgay);
        int Mua(string ThoiGian, Guid DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID);
        int Xuat(string LoaiHinh, string CongTrinhDiID, string CongTrinhNhanID, string NhanVienXuatID, string DataChiTietPhieuXuat, string DSHinhanh, string ThoiGian, Guid NhanVienDuyetID);
        int CapNhatXuat(string PhieuXuatVatTuID, string LoaiHinh, string CongTrinhDiID, string CongTrinhNhanID, string NhanVienXuatID, string DataChiTietPhieuXuat, string DSHinhanh, string NoiDung, string ThoiGian, Guid NhanVienDuyetID);
        int CapNhatPhieuMua(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID);
        int TraThanhPham(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID);
        int CapNhatTraThanhPham(string PhieuXuatVatTuID, string ThoiGian, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID);
    }
    public class KhoThanhPhamRepository : EFRepository<ThanhPham>, IKhoThanhPhamRepository
    {
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        public KhoThanhPhamRepository(DbContext dbContext) : base(dbContext) { }
        public List<Web_ThanhPham_TonKhoResult> LayTon(string CongTrinhID)
        {
            db = new DBMLDFDataContext();
            List<Web_ThanhPham_TonKhoResult> list = db.Web_ThanhPham_TonKho(Guid.Parse(CongTrinhID)).ToList();
            return list;
        }
        public List<Web_ThanhPham_LayDanhSachResult> LayDanhSach()
        {
            db = new DBMLDFDataContext();
            var list = db.Web_ThanhPham_LayDanhSach().ToList();
            return list;
        }
        public List<Web_ThanhPham_LichSuXuatKhoResult> LichSuXuatKho(Guid CongTrinhID, int LoaiHinhXuat, string CongTrinhDenID, string TuNgay = "", string DenNgay = "")
        {
            db = new DBMLDFDataContext();
            DateTime tuNgay = new DateTime(2000, 1, 1);
            DateTime denNgay = DateTime.Now;
            if (TuNgay != "")
            {
                tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            }
            if (DenNgay != "")
            {
                denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
            }
            var data = db.Web_ThanhPham_LichSuXuatKho(CongTrinhID, tuNgay, denNgay).ToList();
            if (CongTrinhDenID != null && CongTrinhDenID != "")
            {
                data = data.Where(t => t.DonHangNhanID == Guid.Parse(CongTrinhDenID)).ToList();
            }
            if (LoaiHinhXuat == 1)
            {
                data = data.Where(t => t.DonHangNhanID!=null).ToList();
            }
            if (LoaiHinhXuat == 2)
            {
                data = data.Where(t => t.DonHangNhanID == null).ToList();
            }

            return data;
        }
        public List<Web_ThanhPham_LichSuNhapKhoResult> LichSuNhapKho(Guid CongTrinhID, int LoaiHinhNhap, string TuNgay, string DenNgay)
        {
            db = new DBMLDFDataContext();
            DateTime tuNgay = new DateTime(2000, 1, 1);
            DateTime denNgay = DateTime.Now;
            if (TuNgay != "")
            {
                tuNgay = DateTime.ParseExact(TuNgay, "dd/MM/yyyy", null);
            }
            if (DenNgay != "")
            {
                denNgay = DateTime.ParseExact(DenNgay, "dd/MM/yyyy", null).AddDays(1);
            }
            if (LoaiHinhNhap == 0)
            {
                var data = db.Web_ThanhPham_LichSuNhapKho(CongTrinhID, tuNgay, denNgay).ToList();
                return data;
            }
            else if (LoaiHinhNhap == 1)
            {
                var data = db.Web_ThanhPham_LichSuNhapKho(CongTrinhID, tuNgay, denNgay).Where(p=>p.TenDoiTac=="").ToList();
                return data;
            }
            else
            {
                var data = db.Web_ThanhPham_LichSuNhapKho(CongTrinhID, tuNgay, denNgay).Where(p => p.TenDoiTac != "").ToList();
                return data;
            }
        }
        public int Mua(string ThoiGian, Guid DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID)
        {
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    unitofwork = new UnitOfWork();
                    Guid PhieuNhapThanhPhamID = Guid.NewGuid();
                    var maxDeNghi = unitofwork.Context.PhieuNhapThanhPhams.Max(p => p.MaPhieuNhap);
                    string maDeNghi = CreateMaDeNghi(maxDeNghi, "PNTP");
                    var thoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null);
                    var phieunhap = new PhieuNhapThanhPham()
                    {
                        DonHangID = Guid.Parse(Define.CONGTRINHTONG),
                        DanhSachHinhAnhs = DSHinhAnh,
                        DoiTacID = DoiTacID,
                        IsActive = true,
                        PhieuNhapThanhPhamID = PhieuNhapThanhPhamID,
                        PhieuXuatThanhPhamID = null,
                        ThoiGian = thoiGian,
                        MaPhieuNhap = maDeNghi,
                        NguoiTaoID= NhanVienID,
                    };
                    unitofwork.Context.PhieuNhapThanhPhams.Add(phieunhap);
                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuNhapThanhPhamRequest>>(DataChiTietNhap);
                    foreach (var it in list)
                    {
                        var chiTietNhap = new ChiTietPhieuNhapThanhPham()
                        {
                            ChiTietPhieuNhapThanhPhamID = Guid.NewGuid(),
                            GiaNhap = it.DonGia,
                            PhieuNhapThanhPhamID = PhieuNhapThanhPhamID,
                            SoLuong = it.SoLuong,
                            ThanhPhamID = it.ThanhPhamID,
                            IsActive=true,
                        };
                        unitofwork.Context.ChiTietPhieuNhapThanhPhams.Add(chiTietNhap);
                        unitofwork.Context.SaveChanges();
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
        public int Xuat(string LoaiHinh, string CongTrinhDiID, string CongTrinhNhanID, string NhanVienXuatID, string DataChiTietPhieuXuat, string DSHinhanh, string ThoiGian, Guid NhanVienDuyetID)
        {
            unitofwork = new UnitOfWork();
            using (TransactionScope tran = new TransactionScope())
            {
                try
                {
                    Guid PhieuXuatVatTuID = Guid.NewGuid();
                    Guid PhieuNhapVatTuID = Guid.NewGuid();
                    var maxDeNghi = unitofwork.Context.PhieuXuatThanhPhams.Max(p => p.MaPhieuXuat);
                    string maDeNghi = CreateMaDeNghi(maxDeNghi, "PXTP");
                    var maxDeNghi2 = unitofwork.Context.PhieuNhapThanhPhams.Max(p => p.MaPhieuNhap);
                    string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNTP");

                    Guid deNghiCongTrinhId = Guid.NewGuid();

                    var phieuxuatThanhPham = new PhieuXuatThanhPham()
                    {
                        DonHangID = Guid.Parse(CongTrinhDiID),
                        DanhSachHinhAnhs = DSHinhanh,
                        IsActive = true,
                        NguoiTaoID = Guid.Parse(NhanVienXuatID),
                        PhieuXuatThanhPhamID = PhieuXuatVatTuID,
                        ThoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null),
                        DonHangNhanID = Guid.Parse(CongTrinhNhanID),
                        MaPhieuXuat = maDeNghi,
                        LoaiHinh=1
                    };
                    unitofwork.Context.PhieuXuatThanhPhams.Add(phieuxuatThanhPham);
                    var phieunhapThanhPham = new PhieuNhapThanhPham()
                    {
                        DonHangID = Guid.Parse(CongTrinhNhanID),
                        DanhSachHinhAnhs = DSHinhanh,
                        IsActive = true,
                        NguoiTaoID = Guid.Parse(NhanVienXuatID),
                        PhieuXuatThanhPhamID = PhieuXuatVatTuID,
                        ThoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null),
                        MaPhieuNhap = maDeNghi2,
                        PhieuNhapThanhPhamID = PhieuNhapVatTuID,
                    };
                    unitofwork.Context.PhieuNhapThanhPhams.Add(phieunhapThanhPham);


                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuXuatThanhPhamRequest>>(DataChiTietPhieuXuat);
                    foreach (var it in list)
                    {
                        var chitietxuat = new ChiTietPhieuXuatThanhPham()
                        {
                            ChiTietPhieuXuatThanhPhamID = Guid.NewGuid(),
                            IsActive=true,
                            ThanhPhamID=it.ThanhPhamID,
                            PhieuXuatThanhPhamID = PhieuXuatVatTuID,
                            SoLuong= it.SoLuong,
                        };
                        unitofwork.Context.ChiTietPhieuXuatThanhPhams.Add(chitietxuat);

                        var chiTietNhap = new ChiTietPhieuNhapThanhPham()
                        {
                            ChiTietPhieuNhapThanhPhamID = Guid.NewGuid(),
                            GiaNhap = it.DonGia.GetValueOrDefault(),
                            PhieuNhapThanhPhamID = PhieuNhapVatTuID,
                            SoLuong = it.SoLuong,
                            ThanhPhamID = it.ThanhPhamID,
                            IsActive = true,
                        };
                        unitofwork.Context.ChiTietPhieuNhapThanhPhams.Add(chiTietNhap);
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
        public int CapNhatXuat(string PhieuXuatVatTuID, string LoaiHinh, string CongTrinhDiID, string CongTrinhNhanID, string NhanVienXuatID, string DataChiTietPhieuXuat, string DSHinhanh, string NoiDung, string ThoiGian, Guid NhanVienDuyetID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                using (TransactionScope tran = new TransactionScope())
                {
                    var pxnlid = Guid.Parse(PhieuXuatVatTuID);
                    var phieuxuatnl = unitofwork.Context.PhieuXuatThanhPhams.Find(pxnlid);
                    var phieunhapnl = unitofwork.Context.PhieuNhapThanhPhams.FirstOrDefault(p => p.PhieuXuatThanhPhamID == pxnlid);
                    var thoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null);
                    phieuxuatnl.ThoiGian = thoiGian;
                    phieunhapnl.ThoiGian = thoiGian;
                    phieuxuatnl.DonHangNhanID = Guid.Parse(CongTrinhNhanID);
                    phieunhapnl.DonHangID = Guid.Parse(CongTrinhNhanID);
                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuXuatThanhPhamRequest>>(DataChiTietPhieuXuat);
                    var lstchitietphieuxuat = unitofwork.Context.ChiTietPhieuXuatThanhPhams.Where(p => p.PhieuXuatThanhPhamID == pxnlid).ToList();
                    var lstchitietphieunhap = unitofwork.Context.ChiTietPhieuNhapThanhPhams.Where(p => p.PhieuNhapThanhPhamID == phieuxuatnl.PhieuNhapThanhPhamID).ToList();
                    foreach(var item in lstchitietphieuxuat){
                        var item2 = list.FirstOrDefault(p => list.Any(p2 => p2.ThanhPhamID == p.ThanhPhamID));
                        if (item2 != null)
                        {
                            item.SoLuong = item2.SoLuong;
                        }
                    }
                    foreach (var item in lstchitietphieunhap)
                    {
                        var item2 = list.FirstOrDefault(p => list.Any(p2 => p2.ThanhPhamID == p.ThanhPhamID));
                        if (item2 != null)
                        {
                            item.SoLuong = item2.SoLuong;
                        }
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
            }
            catch
            {
                return 0;
            }

        }
        public int CapNhatPhieuMua(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    var pnvtid = Guid.Parse(PhieuNhapVatTuID);
                    var phieunhapnl = unitofwork.Context.PhieuNhapThanhPhams.Find(pnvtid);
                    var thoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null);
                    phieunhapnl.ThoiGian = thoiGian;
                    phieunhapnl.DoiTacID = Guid.Parse(DoiTacID);
                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuNhapThanhPhamRequest>>(DataChiTietNhap);
                    var lstchitietphieunhap = unitofwork.Context.ChiTietPhieuNhapThanhPhams.Where(p => p.PhieuNhapThanhPhamID == pnvtid).ToList();
                   
                    foreach (var item in lstchitietphieunhap)
                    {
                        var item2 = list.FirstOrDefault(p => list.Any(p2 => p2.ThanhPhamID == p.ThanhPhamID));
                        if (item2 != null)
                        {
                            item.SoLuong = item2.SoLuong;
                            item.GiaNhap = item2.DonGia;
                        }
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int TraThanhPham(string PhieuNhapVatTuID, string ThoiGian, string DoiTacID, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID)
        {
            unitofwork = new UnitOfWork();
            try
            {
                using (TransactionScope tran = new TransactionScope())
                {
                    var pnvtid = Guid.Parse(PhieuNhapVatTuID);
                    var pnnl = unitofwork.Context.PhieuNhapThanhPhams.FirstOrDefault(p => p.PhieuNhapThanhPhamID == pnvtid);
                    Guid PhieuXuatVatTuID = Guid.NewGuid();
                    var maxDeNghi = unitofwork.Context.PhieuXuatThanhPhams.Max(p => p.MaPhieuXuat);
                    string maDeNghi = CreateMaDeNghi(maxDeNghi, "PXTP");

                    Guid deNghiCongTrinhId = Guid.NewGuid();

                    var phieuxuatThanhPham = new PhieuXuatThanhPham()
                    {
                        DonHangID = pnnl.DonHangID.GetValueOrDefault(),
                        DanhSachHinhAnhs = DSHinhAnh,
                        IsActive = true,
                        NguoiTaoID = NhanVienID,
                        PhieuXuatThanhPhamID = PhieuXuatVatTuID,
                        ThoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null),
                        DoiTacID = Guid.Parse(DoiTacID),
                        MaPhieuXuat = maDeNghi,
                        PhieuNhapThanhPhamID=pnvtid,
                        LoaiHinh = 2
                    };
                    unitofwork.Context.PhieuXuatThanhPhams.Add(phieuxuatThanhPham);

                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuXuatThanhPhamRequest>>(DataChiTietNhap);
                    foreach (var it in list)
                    {
                        var chitietxuat = new ChiTietPhieuXuatThanhPham()
                        {
                            ChiTietPhieuXuatThanhPhamID = Guid.NewGuid(),
                            IsActive = true,
                            ThanhPhamID = it.ThanhPhamID,
                            PhieuXuatThanhPhamID = PhieuXuatVatTuID,
                            SoLuong = it.SoLuong,
                        };
                        unitofwork.Context.ChiTietPhieuXuatThanhPhams.Add(chitietxuat);
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int CapNhatTraThanhPham(string PhieuXuatVatTuID, string ThoiGian, string DataChiTietNhap, string DSHinhAnh, Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                using (TransactionScope tran = new TransactionScope())
                {
                    var pxnlid = Guid.Parse(PhieuXuatVatTuID);
                    var phieuxuatnl = unitofwork.Context.PhieuXuatThanhPhams.Find(pxnlid);
                    var thoiGian = DateTime.ParseExact(ThoiGian, "dd/MM/yyyy HH:mm", null);
                    phieuxuatnl.ThoiGian = thoiGian;;
                    var list = new JavaScriptSerializer().Deserialize<List<ChiTietPhieuXuatThanhPhamRequest>>(DataChiTietNhap);
                    var lstchitietphieuxuat = unitofwork.Context.ChiTietPhieuXuatThanhPhams.Where(p => p.PhieuXuatThanhPhamID == pxnlid).ToList();
                    foreach (var item in lstchitietphieuxuat)
                    {
                        var item2 = list.FirstOrDefault(p => list.Any(p2 => p2.ThanhPhamID == p.ThanhPhamID));
                        if (item2 != null)
                        {
                            item.SoLuong = item2.SoLuong;
                        }
                    }
                    unitofwork.Save();
                    tran.Complete();
                    return 1;
                }
            }
            catch
            {
                return 0;
            }

        }
        public string CreateMaDeNghi(string maxDeNghi, string prefix)
        {
            DateTime now = DateTime.Now;
            string dd = "";
            if (now.Day < 10) dd = "0" + now.Day;
            else dd = now.Day.ToString();
            string mm = "";
            if (now.Month < 10) mm = "0" + now.Month;
            else mm = now.Month.ToString();

            string yyyy = now.Year.ToString();

            string finalDate = yyyy + mm + dd;


            int numberMax = 1;
            try
            {
                string n = maxDeNghi.Replace(prefix, "");
                n = n.Substring(finalDate.Length, n.Length - finalDate.Length);//=> number
                numberMax = Convert.ToInt32(n) + 1;
            }
            catch { }
            string max = string.Empty;
            if (numberMax < 10) { max = "000" + numberMax; }
            else if (numberMax < 100) { max = "00" + numberMax; }
            else if (numberMax < 1000) { max = "0" + numberMax; }
            return prefix + finalDate + max; ;
        }
    }
}
