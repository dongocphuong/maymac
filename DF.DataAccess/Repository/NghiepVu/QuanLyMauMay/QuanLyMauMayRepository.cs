﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu;

namespace DF.DataAccess.Repository
{
    public interface IQuanLyMauMayRepository : IRepository<MauMay>
    {
        List<Web_MauMay_GetAllDataResult> GetAllData();
        int AddOrUpdate(string data, string chitiet);
    }
    public class QuanLyMauMayRepository : EFRepository<MauMay>, IQuanLyMauMayRepository
    {
        public QuanLyMauMayRepository(DbContext dbContext) : base(dbContext) { }
        protected DBMLDFDataContext db;
        protected UnitOfWork unitofwork;
        public List<Web_MauMay_GetAllDataResult> GetAllData()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            List<Web_MauMay_GetAllDataResult> list = db.Web_MauMay_GetAllData().OrderBy(t => t.ThoiGian).ToList();
            return list;
        }
        public int AddOrUpdate(string data, string chitiet)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var maumay = new JavaScriptSerializer().Deserialize<MauMay>(data);
                var chitietmaumay = new JavaScriptSerializer().Deserialize<List<ChiTietMauMay>>(chitiet);
                if (maumay.MauMayID == null || maumay.MauMayID == Guid.Empty)// thêm
                {
                    var mm = new MauMay();
                    mm.MauMayID = Guid.NewGuid();
                    mm.MaMauMay = maumay.MaMauMay;
                    mm.LoaiHinhSanXuat = maumay.LoaiHinhSanXuat;
                    mm.YeuCauKhachHangID = maumay.YeuCauKhachHangID;
                    mm.TenMauMay = maumay.TenMauMay;
                    mm.ThoiGian = DateTime.Now;
                    mm.DanhSachHinhAnhs = maumay.DanhSachHinhAnhs;
                    mm.IsActive = 1;
                    int i = 1;
                    foreach (var it in chitietmaumay)
                    {
                        var ctmm = new ChiTietMauMay()
                        {
                            ChiTietMauMayID = Guid.NewGuid(),
                            MauMayID = mm.MauMayID,
                            NoiDungChiTiet = it.NoiDungChiTiet,
                            STT = i,
                            NguoiThucHien = it.NguoiThucHien,
                            NguoiPhuTrach = it.NguoiPhuTrach,
                            YeuCauPAThucHien = it.YeuCauPAThucHien,
                            ThoiGianThucHien = it.ThoiGianThucHien,
                            GhiChu = it.GhiChu,
                            IsActive = 1
                        };
                        i++;
                        unitofwork.ChiTietMauMays.Add(ctmm);
                    }
                    unitofwork.MauMays.Add(mm);
                    unitofwork.Save();
                    return 1;
                }
                else // sửa
                {
                    unitofwork = new UnitOfWork();
                    db = new DBMLDFDataContext();
                    var mm = unitofwork.Context.MauMays.Find(maumay.MauMayID);
                    mm.TenMauMay = maumay.TenMauMay;
                    mm.MaMauMay = maumay.MaMauMay;
                    mm.YeuCauKhachHangID = maumay.YeuCauKhachHangID;
                    mm.LoaiHinhSanXuat = maumay.LoaiHinhSanXuat;
                    mm.DanhSachHinhAnhs = maumay.DanhSachHinhAnhs;
                    db.Web_MauMay_DeleteAllChiTietMauMaybyMauMayID(maumay.MauMayID);
                    int i = 1;
                    foreach (var it in chitietmaumay)
                    {
                        var ctmm = new ChiTietMauMay()
                        {
                            ChiTietMauMayID = Guid.NewGuid(),
                            MauMayID = mm.MauMayID,
                            NoiDungChiTiet = it.NoiDungChiTiet,
                            STT = i,
                            NguoiThucHien = it.NguoiThucHien,
                            NguoiPhuTrach = it.NguoiPhuTrach,
                            YeuCauPAThucHien = it.YeuCauPAThucHien,
                            ThoiGianThucHien = it.ThoiGianThucHien,
                            GhiChu = it.GhiChu,
                            IsActive = 1
                        };
                        i++;
                        unitofwork.ChiTietMauMays.Add(ctmm);
                    }
                    unitofwork.Save();
                    unitofwork.Context.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
    }
}
