﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu;

namespace DF.DataAccess.Repository
{
    public interface IQuanLyChiTietMauMayRepository: IRepository<ChiTietMauMay>
    {

    }
    public class QuanLyChiTietMauMayRepository: EFRepository<ChiTietMauMay>, IQuanLyChiTietMauMayRepository
    {
        public QuanLyChiTietMauMayRepository(DbContext dbContext) : base(dbContext) { }
    }
}
