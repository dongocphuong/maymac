﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;

namespace DF.DataAccess.Repository
{
    public interface IQuanLyVanBanDinhKemRepository : IRepository<VanBanDinhKem>
    {
        List<Web_VanBan_GetAllDataResult> GetAllData();
        int AddOrUpdate(string data, Guid nvid);
        int XoaVanBan(string vbid);
    }
    public class QuanLyVanBanDinhKemRepository : EFRepository<VanBanDinhKem>, IQuanLyVanBanDinhKemRepository
    {
        public QuanLyVanBanDinhKemRepository(DbContext dbContext) : base(dbContext) { }
        protected DBMLDFDataContext db;
        protected UnitOfWork unitofwork;
        public List<Web_VanBan_GetAllDataResult> GetAllData()
        {
            unitofwork = new UnitOfWork();
            db = new DBMLDFDataContext();
            List<Web_VanBan_GetAllDataResult> list = db.Web_VanBan_GetAllData().OrderBy(t => t.ThoiGianTao).ToList();
            return list;
        }
        public int AddOrUpdate(string data, Guid nvid)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var banghi = new JavaScriptSerializer().Deserialize<VanBanModels>(data);
                if (banghi.VanBanID == null || banghi.VanBanID == Guid.Empty)
                {// thêm
                    var vb = new VanBanDinhKem();
                    vb.IsActive = true;
                    vb.VanBanID = Guid.NewGuid();
                    vb.ThoiGianTao = DateTime.Now;
                    vb.TieuDe = banghi.TieuDe;
                    vb.NoiDung = banghi.NoiDung;
                    vb.NguoiTaoID = nvid;
                    vb.LoaiVanBan = banghi.LoaiVanBan;
                    vb.FileDinhKem = banghi.ListHinhAnh.Any() ? String.Join(",", banghi.ListHinhAnh.Select(p => p.img)) : "";
                    //
                    if (!banghi.ListHinhAnh.Any())
                    {
                        var hanhkodung = unitofwork.Context.HinhAnhVanBans.Where(p => p.VanBanID == vb.VanBanID);
                        unitofwork.Context.HinhAnhVanBans.RemoveRange(hanhkodung);
                    }
                    foreach (var items in banghi.ListHinhAnh.Where(p => p.img != "" && p.img != null))
                    {
                        var imgHd = unitofwork.Context.HinhAnhVanBans.Any(p => p.FileMaHoa.ToLower() == items.img.ToLower());
                        var hanhkodung = unitofwork.Context.HinhAnhVanBans.Where(p => p.VanBanID  == vb.VanBanID).ToList().Where(p => !banghi.ListHinhAnh.Any(p2 => p2.img.ToLower() == p.FileMaHoa.ToLower()));
                        unitofwork.Context.HinhAnhVanBans.RemoveRange(hanhkodung);
                        if (!imgHd)
                        {
                            var ha = new HinhAnhVanBan()
                            {
                                FileMaHoa = items.img,
                                HinhAnhVanBanID = Guid.NewGuid(),
                                VanBanID = vb.VanBanID,
                                TenFile = items.name,
                                IsActive = true
                            };
                            unitofwork.Context.HinhAnhVanBans.Add(ha);
                        }
                    }
                    unitofwork.VanBanDinhKems.Add(vb);
                    unitofwork.Context.SaveChanges();
                    unitofwork.Save();
                    return 2;
                }
                else // sửa
                {
                    var vt = unitofwork.Context.VanBanDinhKems.FirstOrDefault(p => p.VanBanID == banghi.VanBanID);
                    vt.TieuDe = banghi.TieuDe;
                    vt.NoiDung = banghi.NoiDung;
                    vt.LoaiVanBan = banghi.LoaiVanBan;
                    vt.FileDinhKem = banghi.ListHinhAnh.Any() ? String.Join(",", banghi.ListHinhAnh.Select(p => p.img)) : "";
                    //
                    if (!banghi.ListHinhAnh.Any())
                    {
                        var hanhkodung = unitofwork.Context.HinhAnhVanBans.Where(p => p.VanBanID == vt.VanBanID);
                        unitofwork.Context.HinhAnhVanBans.RemoveRange(hanhkodung);
                    }
                    foreach (var items in banghi.ListHinhAnh.Where(p => p.img != "" && p.img != null))
                    {
                        var imgHd = unitofwork.Context.HinhAnhVanBans.Any(p => p.FileMaHoa.ToLower() == items.img.ToLower());
                        var hanhkodung = unitofwork.Context.HinhAnhVanBans.Where(p => p.VanBanID == vt.VanBanID).ToList().Where(p => !banghi.ListHinhAnh.Any(p2 => p2.img.ToLower() == p.FileMaHoa.ToLower()));
                        unitofwork.Context.HinhAnhVanBans.RemoveRange(hanhkodung);
                        if (!imgHd)
                        {
                            var ha = new HinhAnhVanBan()
                            {
                                FileMaHoa = items.img,
                                HinhAnhVanBanID = Guid.NewGuid(),
                                VanBanID = vt.VanBanID,
                                TenFile = items.name,
                                IsActive = true
                            };
                            unitofwork.Context.HinhAnhVanBans.Add(ha);
                        }
                    }
                    unitofwork.Context.SaveChanges();
                    return 2;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int XoaVanBan(string vbid)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var banghi = new NguyenLieu();
                unitofwork.Context.VanBanDinhKems.Find(Guid.Parse(vbid)).IsActive = false;
                unitofwork.Context.SaveChanges();
                return 2;
            }
            catch
            {
                return 0;
            }
        }
    }
}
