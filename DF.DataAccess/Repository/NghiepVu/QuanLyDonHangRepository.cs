﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;
using DF.DBMapping.ModelsExt;
using DF.DBMapping.ModelsExt.VatTu;
using DF.DataAccess.DBML;
using Newtonsoft.Json;
using DF.DBMapping.ModelsExt.NghiepVu;

namespace DF.DataAccess.Repository
{
    public interface IQuanLyDonHangRepository : IRepository<DonHang>
    {
        int AddOrUpdateDonHang(string data, string sanphams, string chitiet, Guid NhanVienID);
        int DeleteDonHang(string DonHangID);
    }
    public class QuanLyDonHangRepository : EFRepository<DonHang>, IQuanLyDonHangRepository
    {
        public QuanLyDonHangRepository(DbContext dbContext) : base(dbContext) { }
        protected UnitOfWork unitofwork;
        protected DBMLDFDataContext db;
        public int AddOrUpdateDonHang(string data, string sanphams, string chitiet, Guid NhanVienID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var model = new JavaScriptSerializer().Deserialize<DonHangModelAdd>(data);
                //var chitiets = new JavaScriptSerializer().Deserialize<List<HoaDonChiTietModel>>(chitiet);
                var lstsanphams = new JavaScriptSerializer().Deserialize<List<HoaDonSanPhamModel>>(sanphams);
                if (model.DonHangID == null)
                {
                    var khachhang = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == model.DoiTacID);
                    var soluong = unitofwork.Context.DonHangs.Count(p => p.DoiTacID == model.DoiTacID);
                    var mahoadon = khachhang.MaDoiTac + DateTime.Now.ToString("ddMMyy");
                    var sodem = 3 - (soluong.ToString().Length);
                    for (int i = 0; i < sodem; i++)
                    {
                        mahoadon += "0";
                    }
                    mahoadon += (soluong+1).ToString();
                    var dh = new DonHang()
                    {
                        DoiTacID = model.DoiTacID,
                        DonHangID = Guid.NewGuid(),
                        IsActive = true,
                        MaDonHang = mahoadon,
                        TenDonHang = model.TenDonHang,
                        YeuCauKhachHangID = model.YeuCauKhachHangID,
                        TrangThai = model.TrangThai,
                        DiaDiemGiaoHang = model.DiaDiemGiaoHang,
                        EmailKeToan = model.EmailKeToan,
                        SoTienDaThanhToan = model.SoTienDaThanhToan,
                        LoaiHinhThanhToan = model.LoaiHinhThanhToan,
                        YeuCauKhac = model.YeuCauKhac,
                        NhanVienTaoID = NhanVienID,
                        ThoiGianTao = DateTime.Now,
                        HinhAnh = model.ListHinhAnh.Any() ? String.Join(",", model.ListHinhAnh.Select(p => p.img)) : "",
                        ThoiGian = DateTime.ParseExact(model.ThoiGian, "dd/MM/yyyy HH:mm", null)
                    };
                    if (model.ThoiGianThanhToan != "")
                    {
                        dh.ThoiGianThanhToan = DateTime.ParseExact(model.ThoiGianThanhToan, "dd/MM/yyyy HH:mm", null);
                    }
                    if (model.ThoiGianDuKienSanXuat != "")
                    {
                        dh.ThoiGianDuKienSanXuat = DateTime.ParseExact(model.ThoiGianDuKienSanXuat, "dd/MM/yyyy HH:mm", null);
                    }
                    if (model.ThoiGianDuKienGiaoHang != "")
                    {
                        dh.ThoiGianDuKienGiaoHang = DateTime.ParseExact(model.ThoiGianDuKienGiaoHang, "dd/MM/yyyy HH:mm", null);
                    }
                    mahoadon += soluong > 0 ? (soluong + 1).ToString() : "1";
                    if (model.ThoiGianDatCoc!="")
                    {
                        dh.ThoiGianDatCoc = DateTime.ParseExact(model.ThoiGianDatCoc, "dd/MM/yyyy HH:mm", null);
                    }
                    unitofwork.Context.DonHangs.Add(dh);
                    foreach (var item in lstsanphams)
                    {
                        var ct = new ChiTietDonHang()
                        {
                            ChiTietDonHangID = Guid.NewGuid(),
                            DonGia = item.DonGia,
                            DonHangID = dh.DonHangID,
                            IsActive = true,
                            LoaiHinh = item.LoaiHinh,
                            SanPhamID = item.SanPhamID,
                            SoLuong = item.SoLuong,
                            TrangThai = 1,
                            SoLuongGiao = item.SoLuongGiao,
                            GhiChu = item.GhiChu,
                        };
                        var sanpham = unitofwork.Context.SanPhams.Find(item.SanPhamID);
                        sanpham.Mau = item.Mau;
                        unitofwork.Context.ChiTietDonHangs.Add(ct);
                        unitofwork.Context.SaveChanges();
                    }
                    if (!model.ListHinhAnh.Any())
                    {
                        var hanhkodung = unitofwork.Context.HinhAnhDonHangs.Where(p => p.DonHangID == dh.DonHangID);
                        unitofwork.Context.HinhAnhDonHangs.RemoveRange(hanhkodung);
                    }
                    foreach (var items in model.ListHinhAnh.Where(p => p.img != "" && p.img != null))
                    {
                        var imgHd = unitofwork.Context.HinhAnhDonHangs.Any(p => p.FileMaHoa.ToLower() == items.img.ToLower());
                        var hanhkodung = unitofwork.Context.HinhAnhDonHangs.Where(p => p.DonHangID == dh.DonHangID).ToList().Where(p => !model.ListHinhAnh.Any(p2 => p2.img.ToLower() == p.FileMaHoa.ToLower()));
                        unitofwork.Context.HinhAnhDonHangs.RemoveRange(hanhkodung);
                        if (!imgHd)
                        {
                            var ha = new HinhAnhDonHang()
                            {
                                FileMaHoa = items.img,
                                HinhAnhDonHangID = Guid.NewGuid(),
                                DonHangID = dh.DonHangID,
                                TenFile = items.name,
                                IsActive = true
                            };
                            unitofwork.Context.HinhAnhDonHangs.Add(ha);
                        }
                    }
                   
                }
                else
                {
                   

                    var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == model.DonHangID);
                    if (donhang.DoiTacID != model.DoiTacID)
                    {
                        var khachhang = unitofwork.Context.DoiTacs.FirstOrDefault(p => p.DoiTacID == model.DoiTacID);
                        var soluong = unitofwork.Context.DonHangs.Count(p => p.DoiTacID == model.DoiTacID && p.DonHangID != model.DonHangID);
                        var mahoadon = khachhang.MaDoiTac + DateTime.Now.ToString("ddMMyy");
                        var sodem = 3 - (soluong.ToString().Length);
                        for (int i = 0; i < sodem; i++)
                        {
                            mahoadon += "0";
                        }
                        mahoadon += (soluong + 1).ToString();
                        donhang.MaDonHang = mahoadon;
                    }
                    if (model.ThoiGianDatCoc.Length<=6)
                    {
                        donhang.ThoiGianDatCoc = null;
                    }
                    else
                    {
                        donhang.ThoiGianDatCoc = DateTime.ParseExact(model.ThoiGianDatCoc, "dd/MM/yyyy HH:mm", null);
                    }
                    donhang.DoiTacID = model.DoiTacID;
                  
                    donhang.TenDonHang = model.TenDonHang;
                    donhang.ThoiGian = DateTime.ParseExact(model.ThoiGian, "dd/MM/yyyy HH:mm", null);
                    donhang.TrangThai = model.TrangThai;
                    donhang.DiaDiemGiaoHang = model.DiaDiemGiaoHang;
                    donhang.SoTienDaThanhToan = model.SoTienDaThanhToan;
                    donhang.LoaiHinhThanhToan = model.LoaiHinhThanhToan;
                    donhang.YeuCauKhachHangID = model.YeuCauKhachHangID;
                    donhang.EmailKeToan = model.EmailKeToan;
                    donhang.HinhAnh = model.ListHinhAnh.Any() ? String.Join(",", model.ListHinhAnh.Select(p => p.img)) : "";
                    if (model.ThoiGianThanhToan != "")
                    {
                        donhang.ThoiGianThanhToan = DateTime.ParseExact(model.ThoiGianThanhToan, "dd/MM/yyyy HH:mm", null);
                    }
                    if (model.ThoiGianDuKienSanXuat != "")
                    {
                        donhang.ThoiGianDuKienSanXuat = DateTime.ParseExact(model.ThoiGianDuKienSanXuat, "dd/MM/yyyy HH:mm", null);
                    }
                    if (model.ThoiGianDuKienGiaoHang != "")
                    {
                        donhang.ThoiGianDuKienGiaoHang = DateTime.ParseExact(model.ThoiGianDuKienGiaoHang, "dd/MM/yyyy HH:mm", null);
                    }
                    donhang.YeuCauKhac = model.YeuCauKhac;
                    var listchitiet = unitofwork.Context.ChiTietDonHangs.Where(p => p.DonHangID == donhang.DonHangID).ToList();
                    // list sản phẩm không có
                    var lstkhongco = lstsanphams.Where(p => !listchitiet.Any(p2 => p.SanPhamID == p2.SanPhamID && p.LoaiHinh == p2.LoaiHinh));
                    foreach (var item in lstkhongco)
                    {
                        var ct = new ChiTietDonHang()
                        {
                            ChiTietDonHangID = Guid.NewGuid(),
                            DonGia = item.DonGia,
                            DonHangID = donhang.DonHangID,
                            IsActive = true,
                            LoaiHinh = item.LoaiHinh,
                            SanPhamID = item.SanPhamID,
                            SoLuong = item.SoLuong,
                            TrangThai = 1,
                            SoLuongGiao = item.SoLuongGiao,
                            GhiChu = item.GhiChu,
                            Mau = item.Mau
                        };
                        unitofwork.Context.ChiTietDonHangs.Add(ct);
                    }
                    //
                    foreach (var item in listchitiet)
                    {
                        var sp = lstsanphams.FirstOrDefault(p => p.SanPhamID == item.SanPhamID && p.LoaiHinh == item.LoaiHinh);
                        if (sp != null)
                        {
                            var soluongcu = item.SoLuong;
                            item.SoLuong = sp.SoLuong;
                            item.DonGia = sp.DonGia;
                            item.SoLuongGiao = sp.SoLuongGiao;
                            item.GhiChu = sp.GhiChu;
                            var sanpham = unitofwork.Context.SanPhams.Find(item.SanPhamID);
                            sanpham.Mau = sp.Mau;
                            var qlsx = unitofwork.Context.QuanLySanXuats.FirstOrDefault(p => p.DonHangID == donhang.DonHangID && p.SanPhamID == item.SanPhamID);
                            if (qlsx != null && soluongcu != sp.SoLuong)
                            {
                                var hmsxs = unitofwork.Context.HangMucSanXuats.Where(p => p.QuanLySanXuatID == qlsx.QuanLySanXuatID).OrderBy(p => p.ViTri).FirstOrDefault();
                                var cd = unitofwork.Context.CongDoanHangMucs.FirstOrDefault(p => p.HangMucSanXuatID == hmsxs.HangMucSanXuatID);
                               
                                if (cd.TrangThai != 3)
                                {
                                    if (sp.SoLuong>soluongcu)
                                    {
                                        db = new DBMLDFDataContext();
                                        var lstnguyenlieu = db.Web_NguyenLieu_TonKho_DonHangCanTheoSanPham_BySoLuong(donhang.DonHangID, item.SanPhamID, sp.SoLuong - soluongcu).ToList();
                                        db.Dispose();
                                        Guid PhieuXuatVatTuID = Guid.NewGuid();
                                        Guid PhieuNhapVatTuID = Guid.NewGuid();
                                        var maxDeNghi = unitofwork.Context.PhieuXuatNguyenLieux.Max(p => p.MaPhieuXuat);
                                        string maDeNghi = CreateMaDeNghi(maxDeNghi, "PXNL");
                                        var maxDeNghi2 = unitofwork.Context.PhieuNhapNguyenLieux.Max(p => p.MaPhieuNhap);
                                        string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNNL");

                                        Guid deNghiCongTrinhId = Guid.NewGuid();

                                        var phieuxuatnguyenlieu = new PhieuXuatNguyenLieu()
                                        {
                                            DonHangID = Guid.Parse(Define.CONGTRINHTONG),
                                            DanhSachHinhAnhs = "",
                                            IsActive = true,
                                            NguoiTaoID = NhanVienID,
                                            PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                                            ThoiGian = DateTime.Now,
                                            DonHangNhanID = donhang.DonHangID,
                                            MaPhieuXuat = maDeNghi,
                                            LoaiHinh = 1
                                        };
                                        unitofwork.Context.PhieuXuatNguyenLieux.Add(phieuxuatnguyenlieu);
                                        var phieunhapnguyenlieu = new PhieuNhapNguyenLieu()
                                        {
                                            DonHangID = donhang.DonHangID,
                                            DanhSachHinhAnhs = "",
                                            IsActive = true,
                                            NguoiTaoID = NhanVienID,
                                            PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                                            ThoiGian = DateTime.Now,
                                            MaPhieuNhap = maDeNghi2,
                                            PhieuNhapNguyenLieuID = PhieuNhapVatTuID,
                                        };
                                        unitofwork.Context.PhieuNhapNguyenLieux.Add(phieunhapnguyenlieu);


                                        foreach (var it in lstnguyenlieu)
                                        {
                                            var chitietxuat = new ChiTietPhieuXuatNguyenLieu()
                                            {
                                                ChiTietPhieuXuatNguyenLieuID = Guid.NewGuid(),
                                                IsActive = true,
                                                NguyenLieuID = it.NguyenLieuID.GetValueOrDefault(),
                                                PhieuXuatNguyenLieu = PhieuXuatVatTuID,
                                                SoLuong = it.SoLuongCan
                                            };
                                            unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Add(chitietxuat);

                                            var chiTietNhap = new ChiTietPhieuNhapNguyenLieu()
                                            {
                                                ChiTietPhieuNhapNguyenLieuID = Guid.NewGuid(),
                                                GiaNhap = it.DonGia.GetValueOrDefault(),
                                                PhieuNhapNguyenLieuID = PhieuNhapVatTuID,
                                                SoLuong = it.SoLuongCan.GetValueOrDefault(),
                                                NguyenLieuID = it.NguyenLieuID.GetValueOrDefault(),
                                                IsActive = true,
                                            };
                                            unitofwork.Context.ChiTietPhieuNhapNguyenLieux.Add(chiTietNhap);
                                        }
                                    }
                                    else if (soluongcu > sp.SoLuong)
                                    {
                                        var thanhphamcongdoan = unitofwork.Context.CongDoanThanhPhams.FirstOrDefault(p => p.CongDoanHangMucID == cd.CongDoanHangMucID);
                                        var tonkhothanhpham = unitofwork.KhoThanhPhams.LayTon(donhang.DonHangID.ToString()).Where(p => p.ThanhPhamID == thanhphamcongdoan.ThanhPhamID).FirstOrDefault();
                                        if(tonkhothanhpham!=null){
                                            if (sp.SoLuong > tonkhothanhpham.SoLuongNhap)
                                            {
                                                db = new DBMLDFDataContext();
                                                var lstnguyenlieu = db.Web_NguyenLieu_TonKhoDonHang_TheoSanPham_BySoLuong(donhang.DonHangID, item.SanPhamID, soluongcu - sp.SoLuong).ToList();
                                                var lstnguyenlieumoi = db.Web_NguyenLieu_TonKhoDonHang_TheoSanPham_BySoLuong(donhang.DonHangID, item.SanPhamID, sp.SoLuong).ToList();
                                                db.Dispose();
                                                Guid PhieuXuatVatTuID = Guid.NewGuid();
                                                Guid PhieuNhapVatTuID = Guid.NewGuid();
                                                var maxDeNghi = unitofwork.Context.PhieuXuatNguyenLieux.Max(p => p.MaPhieuXuat);
                                                string maDeNghi = CreateMaDeNghi(maxDeNghi, "PXNL");
                                                var maxDeNghi2 = unitofwork.Context.PhieuNhapNguyenLieux.Max(p => p.MaPhieuNhap);
                                                string maDeNghi2 = CreateMaDeNghi(maxDeNghi2, "PNNL");

                                                Guid deNghiCongTrinhId = Guid.NewGuid();

                                                var phieuxuatnguyenlieu = new PhieuXuatNguyenLieu()
                                                {
                                                    DonHangID = donhang.DonHangID,
                                                    DanhSachHinhAnhs = "",
                                                    IsActive = true,
                                                    NguoiTaoID = NhanVienID,
                                                    PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                                                    ThoiGian = DateTime.Now,
                                                    DonHangNhanID = donhang.DonHangID,
                                                    MaPhieuXuat = maDeNghi,
                                                    LoaiHinh = 1
                                                };
                                                unitofwork.Context.PhieuXuatNguyenLieux.Add(phieuxuatnguyenlieu);
                                                var phieunhapnguyenlieu = new PhieuNhapNguyenLieu()
                                                {
                                                    DonHangID = Guid.Parse(Define.CONGTRINHTONG),
                                                    DanhSachHinhAnhs = "",
                                                    IsActive = true,
                                                    NguoiTaoID = NhanVienID,
                                                    PhieuXuatNguyenLieuID = PhieuXuatVatTuID,
                                                    ThoiGian = DateTime.Now,
                                                    MaPhieuNhap = maDeNghi2,
                                                    PhieuNhapNguyenLieuID = PhieuNhapVatTuID,
                                                };
                                                unitofwork.Context.PhieuNhapNguyenLieux.Add(phieunhapnguyenlieu);


                                                foreach (var it in lstnguyenlieu)
                                                {
                                                    if (it.SoLuongTon > lstnguyenlieumoi.FirstOrDefault(p => p.NguyenLieuID == it.NguyenLieuID).SoLuongCan)
                                                    {
                                                        var chitietxuat = new ChiTietPhieuXuatNguyenLieu()
                                                        {
                                                            ChiTietPhieuXuatNguyenLieuID = Guid.NewGuid(),
                                                            IsActive = true,
                                                            NguyenLieuID = it.NguyenLieuID.GetValueOrDefault(),
                                                            PhieuXuatNguyenLieu = PhieuXuatVatTuID,
                                                            SoLuong =   it.SoLuongCan,
                                                        };
                                                        unitofwork.Context.ChiTietPhieuXuatNguyenLieux.Add(chitietxuat);

                                                        var chiTietNhap = new ChiTietPhieuNhapNguyenLieu()
                                                        {
                                                            ChiTietPhieuNhapNguyenLieuID = Guid.NewGuid(),
                                                            GiaNhap = it.DonGia.GetValueOrDefault(),
                                                            PhieuNhapNguyenLieuID = PhieuNhapVatTuID,
                                                            SoLuong =  it.SoLuongCan.GetValueOrDefault(),
                                                            NguyenLieuID = it.NguyenLieuID.GetValueOrDefault(),
                                                            IsActive = true,
                                                        };
                                                        unitofwork.Context.ChiTietPhieuNhapNguyenLieux.Add(chiTietNhap);
                                                    }
                                                }
                                            }
                                            else if (sp.SoLuong < tonkhothanhpham.SoLuongNhap)
                                            {
                                                return 3;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            unitofwork.Context.ChiTietDonHangs.Remove(item);
                        }
                    }
                  
                    if (!model.ListHinhAnh.Any())
                    {
                        var hanhkodung = unitofwork.Context.HinhAnhDonHangs.Where(p => p.DonHangID == donhang.DonHangID);
                        unitofwork.Context.HinhAnhDonHangs.RemoveRange(hanhkodung);
                    }
                    foreach (var items in model.ListHinhAnh.Where(p => p.img != "" && p.img != null))
                    {
                        var imgHd = unitofwork.Context.HinhAnhDonHangs.Any(p => p.FileMaHoa.ToLower() == items.img.ToLower());
                        var hanhkodung = unitofwork.Context.HinhAnhDonHangs.Where(p => p.DonHangID == donhang.DonHangID).ToList().Where(p => !model.ListHinhAnh.Any(p2 => p2.img.ToLower() == p.FileMaHoa.ToLower()));
                        unitofwork.Context.HinhAnhDonHangs.RemoveRange(hanhkodung);
                        if (!imgHd)
                        {
                            var ha = new HinhAnhDonHang()
                            {
                                FileMaHoa = items.img,
                                HinhAnhDonHangID = Guid.NewGuid(),
                                DonHangID = donhang.DonHangID,
                                TenFile = items.name,
                                IsActive = true
                            };
                            unitofwork.Context.HinhAnhDonHangs.Add(ha);
                        }
                    }

                    //

                    var ctdh = unitofwork.Context.ChiTietDonHangKhachHangDats.Where(p => p.DonHangID == donhang.DonHangID);
                    foreach (var it in ctdh)
                    {
                        var ctdhnl = unitofwork.Context.NguyenLieuChiTietDonHangKhachHangDats.Where(p => p.ChiTietDonHangKhachHangDatID == it.ChiTietDonHangKhachHangDatID);
                        unitofwork.Context.ChiTietDonHangKhachHangDats.Remove(it);
                        unitofwork.Context.NguyenLieuChiTietDonHangKhachHangDats.RemoveRange(ctdhnl);
                    }
                    unitofwork.Save();
                    foreach (var item in lstkhongco)
                    {
                        if (donhang.TrangThai == 2)
                        {
                            unitofwork.QuanLySanXuats.BatDauSanXuat(item.SanPhamID.ToString(), donhang.DonHangID.ToString(), NhanVienID);
                        }
                    }
                }
                unitofwork.Context.SaveChanges();
                unitofwork.Save();
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public int DeleteDonHang(string DonHangID)
        {
            try
            {
                unitofwork = new UnitOfWork();
                var hdid = Guid.Parse(DonHangID);
                var donhang = unitofwork.Context.DonHangs.FirstOrDefault(p => p.DonHangID == hdid);
                donhang.IsActive = false;
                unitofwork.Save();
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public string CreateMaDeNghi(string maxDeNghi, string prefix)
        {
            DateTime now = DateTime.Now;
            string dd = "";
            if (now.Day < 10) dd = "0" + now.Day;
            else dd = now.Day.ToString();
            string mm = "";
            if (now.Month < 10) mm = "0" + now.Month;
            else mm = now.Month.ToString();

            string yyyy = now.Year.ToString();

            string finalDate = yyyy + mm + dd;


            int numberMax = 1;
            try
            {
                string n = maxDeNghi.Replace(prefix, "");
                n = n.Substring(finalDate.Length, n.Length - finalDate.Length);//=> number
                numberMax = Convert.ToInt32(n) + 1;
            }
            catch { }
            string max = string.Empty;
            if (numberMax < 10) { max = "000" + numberMax; }
            else if (numberMax < 100) { max = "00" + numberMax; }
            else if (numberMax < 1000) { max = "0" + numberMax; }
            return prefix + finalDate + max; ;
        }
    }
}
