﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DF.DBMapping.Models;
using System.Data.SqlClient;
using DF.DataAccess.DBML;
using System.Web.Script.Serialization;
using System.Transactions;
using DF.DBMapping.ModelsExt;

namespace DF.DataAccess.Repository
{
    public interface IUserRepository : IRepository<User>
    {
        User GetByUserName(string userName);
        bool UpdatePassWord(Guid uid, string pass);

        List<Web_HanhChinh_QuanLyTaiKhoan_LayDanhSachTaiKhoanNVResult> LayDSTaiKhoanNV();
        bool ThemUserNameNV(string UserName, string Password, Guid NhanVienID);

    }

    public class UserRepository : EFRepository<User>, IUserRepository
    {
        public UserRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        public User GetByUserName(string userName)
        {
            return DbSet.FirstOrDefault(o => o.UserName == userName);
        }
        public bool UpdatePassWord(Guid uid, string pass)
        {
            try
            {
                string sqlcomand = @"update [User] set Password = @pa where UserId = @uid";
                List<SqlParameter> lstparams = new List<SqlParameter>();
                lstparams.Add(new SqlParameter("pa", pass));
                lstparams.Add(new SqlParameter("uid", uid));
                SQLServerHepler.executeSQLNonQueryWithParam(sqlcomand, lstparams);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public List<Web_HanhChinh_QuanLyTaiKhoan_LayDanhSachTaiKhoanNVResult> LayDSTaiKhoanNV()
        {
            DBMLDFDataContext db = new DBMLDFDataContext();
            return db.Web_HanhChinh_QuanLyTaiKhoan_LayDanhSachTaiKhoanNV().ToList();
        }
        public bool ThemUserNameNV(string UserName, string Password, Guid NhanVienID)
        {
            UnitOfWork unit = new UnitOfWork();
            User usn = new User();
            usn.UserName = UserName;
            usn.UserId = Guid.NewGuid();
            usn.NhanVienId = NhanVienID;
            usn.Password = Password == "" ? "dfs" : Password;
            usn.IsActive = true;
            unit.Users.Add(usn);
            List<int> lstruleDefault = new List<int>();
            lstruleDefault.Add(1003);
            lstruleDefault.Add(1002);
            lstruleDefault.Add(1001);
            lstruleDefault.Add(990);
            lstruleDefault.Add(995);
            lstruleDefault.Add(3016);
            lstruleDefault.Add(3017);
            try
            {
                var role = (from a in unit.Context.tblGroups where a.LoaiHinh == 2 && lstruleDefault.Contains(a.RuleType.Value) select a.GroupGuid).ToList();

                foreach (var itemrol in role)
                {
                    tblGroupUser tbgu = new tblGroupUser();
                    tbgu.GroupGuid = itemrol;
                    tbgu.UserGuid = usn.UserId;
                    unit.tblGroupUsers.Add(tbgu);
                }
            }
            catch (Exception ex)
            {

            }

            unit.Save();

            return true;
        }
    }
}
