﻿using DF.DBMapping.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using System.Web.Script.Serialization;


namespace DF.DataAccess.Repository
{
    public interface ILanguageRepository : IRepository<tblLanguage>
    {
        List<tblLanguage> getdata();
        int createdata(string data);
        int updatedata(string data);
        int deletedata(string data);
    }

    public class LanguageRepository : EFRepository<tblLanguage>, ILanguageRepository
    {
        protected UnitOfWork unitofwork;
        public LanguageRepository(DbContext dbContext)
            : base(dbContext)
        {

        }
        public List<tblLanguage> getdata()
        {
            unitofwork = new UnitOfWork();
            try
            {
                List<tblLanguage> list = unitofwork.Context.tblLanguages.OrderBy(m => m.langkey).ToList();
                return list;
            }
            catch (Exception e)
            {
                List<tblLanguage> list = unitofwork.Context.tblLanguages.OrderBy(m => m.langkey).ToList();
                return list;
            }

        }

        public int createdata(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                tblLanguage banghi = new JavaScriptSerializer().Deserialize<tblLanguage>(data);
                unitofwork.tblLanguages.Add(banghi);
                unitofwork.Save();
                return 2;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
  
        public int updatedata(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                tblLanguage banghi = new JavaScriptSerializer().Deserialize<tblLanguage>(data);
                unitofwork.tblLanguages.Update(banghi);
                return 2;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int deletedata(string data)
        {
            try
            {
                unitofwork = new UnitOfWork();
                tblLanguage banghi = new JavaScriptSerializer().Deserialize<tblLanguage>(data);
                unitofwork.tblLanguages.Delete(banghi);
                unitofwork.Save();
                unitofwork.Dispose();
                return 2;
            }
            catch
            {
                return 0;
            }
        }
    }
}
