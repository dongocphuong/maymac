﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DF.DBMapping.Models;

namespace DF.DataAccess.Repository
{
    public interface IGroupRepository : IRepository<tblGroup>
    {
        List<tblGroup> GetByGroupID(Guid? userID);
    }

    public class GroupRepository : EFRepository<tblGroup>, IGroupRepository
    {
        public GroupRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        public List<tblGroup> GetByGroupID(Guid? userID)
        {
            return DbSet.Where(o => o.GroupGuid == userID).ToList();
        }
    }
}
