﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DF.DBMapping.Models;

namespace DF.DataAccess.Repository
{
    public interface IGroupUserRepository : IRepository<tblGroupUser>
    {
        List<tblGroupUser> GetByUserID(Guid? userID);
    }

    public class GroupUserRepository : EFRepository<tblGroupUser>, IGroupUserRepository
    {
        public GroupUserRepository(DbContext dbContext)
            : base(dbContext)
        {
        }

        public List<tblGroupUser> GetByUserID(Guid? userID)
        {
            return DbSet.Where(o => o.UserGuid == userID).ToList();
        }
    }
}
