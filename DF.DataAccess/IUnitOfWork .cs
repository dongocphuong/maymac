﻿using System;
/*
 * Được tạo bởi hiepth6
 */
using DF.DBMapping.Models;
using DF.DataAccess.Repository;
namespace DF.DataAccess
{
    /// <summary>
    /// Interface UnitOfWork
    /// UnitOfWork đứng ra quản lý toàn bộ các Repository và các transaction trong 1 phiên làm việc
    /// </summary>
    public interface IUnitOfWork : IDisposable
    {
        void Save();
        ITransaction BeginTransaction();
        void EndTransaction(ITransaction transaction);
        MAYMACNEWContext Context { get; set; }

        #region PHẦN DÀNH CHO KHAI BÁO CÁC IREPOSITORY

        IUserRepository Users { get; }
        INhanVienRepository NhanViens { get; }
        IGroupUserRepository GroupUsers { get; }
        IPhanQuyenRepository tblGroupUsers { get; }
        IChucVuRepository ChucVus { get; }
        IGroupRepository Groups { get; }
        IDonViRepository DonVis { get; }
        ILanguageRepository tblLanguages { get; }
        IKhoNguyenLieuRepository KhoNguyenLieus { get; }
        IDanhMucNguyenLieuRepository NguyenLieus { get; }
        IDanhMucSanPhamRepository SanPhams { get; }
        INhanVienPhongBanRepository NhanVienPhongBans { get; }
        IDanhMucThanhPhamRepository ThanhPhams { get; }
        IPhongBanRepository PhongBans { get; }
        IKhoThanhPhamRepository KhoThanhPhams { get; }
        IKhoSanPhamRepository KhoSanPhams { get; }
        IQuanLyDonHangRepository DonHangs { get; }
        IQuanLySanXuatRepository QuanLySanXuats { get; }
        IDeNghiNguyenLieuRepository DeNghiNguyenLieus { get; }
        IQuanLyMauMayRepository MauMays { get ; }
        IQuanLyChiTietMauMayRepository ChiTietMauMays { get; }
        IDanhMucThuocTinhSanPhamRepository ThuocTinhSanPhams { get; }
        IYeuCauKhachHangRepository YeuCauKhachHangs { get; }
        IQuanLyVanBanDinhKemRepository VanBanDinhKems { get; }
        IQuanLySanXuatNhomRepository QuanLySanXuatNhoms { get; }
        #endregion PHẦN DÀNH CHO KHAI BÁO CÁC IREPOSITORY}
    }
}