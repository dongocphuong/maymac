﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using DF.DBMapping;
using DF.DBMapping.Models;
using System.Data.SqlClient;

namespace DF.DataAccess
{
    public class SQLServerHepler
    {
        /// <summary>
        ///     created by: kienmt
        ///     create date: 17/12/2015
        ///     ham thuc thi cau lenh sql
        /// </summary>
        /// <param name="sql"></param>
        public static void executeSQLNonQuery(string sql)
        {
            var context = new MAYMACNEWContext();
            DbConnection conn = context.Database.Connection;
            try
            {
                using (var connection = new SqlConnection(conn.ConnectionString))
                {
                    var command = new SqlCommand(sql, connection);
                    command.Connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
                context.Dispose();
            }
        }

        /// <summary>
        ///     created by: kienmt
        ///     create date: 19/11/2014
        ///     hàm thực thi câu lệnh với param đi kèm
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="lstparams"></param>
        public static void executeSQLNonQueryWithParam(string sql, List<SqlParameter> lstparams, string connectionstr)
        {
            using (var connection = new SqlConnection(connectionstr))
            {
                try
                {
                    var command = new SqlCommand(sql, connection);
                    command.Connection.Open();
                    if (lstparams.Count > 0)
                    {
                        for (int i = 0, il = lstparams.Count; i < il; i++)
                        {
                            command.Parameters.Add(lstparams[i]);
                        }
                    }
                    command.ExecuteNonQuery();
                    command.Clone();
                    connection.Close();
                }
                catch(Exception ex)
                {
                    //if(connection != null && connection.State == ConnectionState.Open) connection.Close();
                    throw ex;
                }
                finally
                {

                    if (connection != null && connection.State == ConnectionState.Open) connection.Close();
                }
            }
        }

        /// <summary>
        ///     created by: kienmt
        ///     create date: 19/11/2014
        ///     hàm thực thi câu lệnh với param đi kèm, tra ve ket qua dang string
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="lstparams"></param>
        public static string executeSQLScalar(string sql, List<SqlParameter> lstparams, string connectionstr)
        {
            try
            {
                string kq = "";
                using (var connection = new SqlConnection(connectionstr))
                {
                    var command = new SqlCommand(sql, connection);
                    command.Connection.Open();
                    if (lstparams.Count > 0)
                    {
                        for (int i = 0, il = lstparams.Count; i < il; i++)
                        {
                            command.Parameters.Add(lstparams[i]);
                        }
                    }
                    kq = command.ExecuteScalar().ToString();
                    command.Clone();
                    connection.Close();
                }
                return kq;
            }
            catch (Exception ex)
            {
             
                throw ex;
            }
        }

        /// <summary>
        ///     created by: kienmt
        ///     create date: 19/11/2014
        ///     hàm thực thi câu lệnh với param đi kèm, tra ve ket qua dang string
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="lstparams"></param>
        /// <returns></returns>
        public static string executeSQLScalar(string sql, List<SqlParameter> lstparams)
        {
            var context = new MAYMACNEWContext();
            string kq = "";
            DbConnection conn = context.Database.Connection;
            try
            {
                using (var connection = new SqlConnection(conn.ConnectionString))
                {
                    var command = new SqlCommand(sql, connection);
                    command.Connection.Open();
                    if (lstparams.Count > 0)
                    {
                        for (int i = 0, il = lstparams.Count; i < il; i++)
                        {
                            command.Parameters.Add(lstparams[i]);
                        }
                    }
                    kq = command.ExecuteScalar() + "";
                    connection.Close();
                }
                return kq;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
                context.Dispose();
            }
        }

        /// <summary>
        ///     created by: kienmt
        ///     create date: 17/12/2015
        ///     hàm thực thi câu lệnh với param đi kèm
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="lstparams"></param>
        public static void executeSQLNonQueryWithParam(string sql, List<SqlParameter> lstparams)
        {
            var context = new MAYMACNEWContext();
            DbConnection conn = context.Database.Connection;
            try
            {
                using (var connection = new SqlConnection(conn.ConnectionString))
                {
                    var command = new SqlCommand(sql, connection);
                    command.Connection.Open();
                    if (lstparams.Count > 0)
                    {
                        for (int i = 0, il = lstparams.Count; i < il; i++)
                        {
                            command.Parameters.Add(lstparams[i]);
                        }
                    }
                    command.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
                context.Dispose();
            }
        }

        /// <summary>
        ///     created by:kienmt
        ///     create date: 17/12/2015
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="lstparams"></param>
        /// <param name="connectionstr"></param>
        /// <returns></returns>
        public static DataTable executeSQLCommandReturnDatatable(string sql, List<SqlParameter> lstparams,
            string connectionstr)
        {
            var tableResult = new DataTable();
            using (var connection = new SqlConnection(connectionstr))
            {
                //if (connection.State != ConnectionState.Open)
                connection.Open();
                using (SqlCommand cmd = connection.CreateCommand())
                {
                    if (lstparams.Count > 0)
                    {
                        for (int i = 0, il = lstparams.Count; i < il; i++)
                        {
                            cmd.Parameters.Add(lstparams[i]);
                        }
                    }
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        tableResult.Load(reader);
                    }
                    cmd.Clone();
                }
                
                connection.Close();
                connection.Dispose();
            }

            return tableResult;
        }

        /// <summary>
        ///     created by: kienmt
        ///     create date: 17/12/2015
        ///     ham thuc thi cau lenh, tra ve datatable
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="lstparams"></param>
        /// <returns></returns>
        public static DataTable executeSQLCommandReturnDatatable(string sql, List<SqlParameter> lstparams)
        {
            var context = new MAYMACNEWContext();
            DbConnection conn = context.Database.Connection;
            ConnectionState connectionState = conn.State;
            try
            {
                var tableResult = new DataTable();
                using (context)
                {
                    if (connectionState != ConnectionState.Open)
                        conn.Open();
                    using (DbCommand cmd = conn.CreateCommand())
                    {
                        if (lstparams.Count > 0)
                        {
                            for (int i = 0, il = lstparams.Count; i < il; i++)
                            {
                                cmd.Parameters.Add(lstparams[i]);
                            }
                        }
                        cmd.CommandText = sql;
                        cmd.CommandType = CommandType.Text;
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            tableResult.Load(reader);
                        }
                        
                    }
                    
                    conn.Close();
                    context.Dispose();

                }
                return tableResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                
                conn.Close();
                conn.Dispose();
                context.Dispose();
            }
        }

        /// <summary>
        ///     created by: kienmt
        ///     create date: 17/12/2015
        ///     ham thuc thi cau lenh select phan trang, tra ve dataset
        /// </summary>
        /// <param name="sqlCount"></param>
        /// <param name="sqlSelect"></param>
        /// <param name="lstparamscount"></param>
        /// <param name="lstparams"></param>
        /// <returns></returns>
        public static DataSet executeSQLCommandPagingReturnDataset(string sqlCount, string sqlSelect,
            List<SqlParameter> lstparamscount, List<SqlParameter> lstparams)
        {
            var context = new MAYMACNEWContext();
            DbConnection conn = context.Database.Connection;
            ConnectionState connectionState = conn.State;
            var ds = new DataSet();
            try
            {
                using (context)
                {
                    if (connectionState != ConnectionState.Open)
                        conn.Open();
                    using (DbCommand cmd = conn.CreateCommand())
                    {
                        var tableResult = new DataTable();
                        if (lstparamscount.Count > 0)
                        {
                            for (int i = 0, il = lstparamscount.Count; i < il; i++)
                            {
                                cmd.Parameters.Add(lstparamscount[i]);
                            }
                        }
                        cmd.CommandText = sqlCount;
                        cmd.CommandType = CommandType.Text;
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            tableResult.Load(reader);
                        }
                        ds.Tables.Add(tableResult);
                    }
                    using (DbCommand cmd = conn.CreateCommand())
                    {
                        var tableResult = new DataTable();
                        if (lstparams.Count > 0)
                        {
                            for (int i = 0, il = lstparams.Count; i < il; i++)
                            {
                                cmd.Parameters.Add(lstparams[i]);
                            }
                        }
                        cmd.CommandText = sqlSelect;
                        cmd.CommandType = CommandType.Text;
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            tableResult.Load(reader);
                        }
                        ds.Tables.Add(tableResult);
                    }
                }
                conn.Close();
                conn.Dispose();
                context.Dispose();
                return ds;
            }
            catch (Exception ex)
            {
                //conn.Close();
                //conn.Dispose();
                //context.Dispose();
                throw ex;
            }
            finally
            {
                conn.Close();
                conn.Dispose();
                context.Dispose();
            }
        }
        public static DataSet ExecuteSqlDataSet(CommandType type, string mySql, List<SqlParameter> commandParameters)
        {
            var context = new MAYMACNEWContext();
            DbConnection conn = context.Database.Connection;
            ConnectionState connectionState = conn.State;
            var ds = new DataSet();
            DataTable dt;
            try
            {
                using (context)
                {
                    if (connectionState != ConnectionState.Open)
                        conn.Open();
                    using (var cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = mySql;
                        cmd.CommandType = type;
                        if (commandParameters != null)
                        {
                            for (int i = 0; i < commandParameters.Count(); i++)
                            {
                                cmd.Parameters.Add(commandParameters[i]);
                            }
                        }
                        using (var reader = cmd.ExecuteReader())
                        {
                            while (true)
                            {
                                dt = new DataTable();
                                dt.Load(reader);

                                if (dt.Columns.Count == 0)
                                    break;

                                ds.Tables.Add(dt);
                            }
                        }
                    }
                }
            }

            finally
            {
                if (connectionState != ConnectionState.Open)
                    conn.Close();
                conn.Dispose();
                context.Dispose();
                commandParameters = null;

            }
            return ds;
        }
    }

    public static class ExtensionUtility
    {
        /*Converts List To DataTable*/

        public static DataTable ToDataTable<TSource>(this IList<TSource> data)
        {
            var dataTable = new DataTable(typeof (TSource).Name);
            PropertyInfo[] props = typeof (TSource).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in props)
            {
                dataTable.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }

            foreach (TSource item in data)
            {
                var values = new object[props.Length];
                for (int i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        /*Converts DataTable To List*/

        public static List<TSource> ToList<TSource>(this DataTable dataTable) where TSource : new()
        {
            var dataList = new List<TSource>();

            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
            var objFieldNames = (from PropertyInfo aProp in typeof (TSource).GetProperties(flags)
                select new {aProp.Name, Type = Nullable.GetUnderlyingType(aProp.PropertyType) ?? aProp.PropertyType})
                .ToList();
            var dataTblFieldNames = (from DataColumn aHeader in dataTable.Columns
                select new {Name = aHeader.ColumnName, Type = aHeader.DataType}).ToList();
            var commonFields = objFieldNames.Intersect(dataTblFieldNames).ToList();

            foreach (DataRow dataRow in dataTable.AsEnumerable().ToList())
            {
                var aTSource = new TSource();
                foreach (var aField in commonFields)
                {
                    PropertyInfo propertyInfos = aTSource.GetType().GetProperty(aField.Name);
                    if (propertyInfos.PropertyType == typeof (string) && propertyInfos.GetValue(aTSource, null) == null)
                    {
                        propertyInfos.SetValue(aTSource, dataRow[aField.Name] + "", null);
                    }
                    else
                    {
                        propertyInfos.SetValue(aTSource, dataRow[aField.Name], null);
                    }
                }
                dataList.Add(aTSource);
            }
            return dataList;
        }
        public static bool InsertUsingTransaction<T>(List<T> datainsert, string DataTableName)
        {
            var context = new MAYMACNEWContext();
            var fieldcolumn = typeof(T).GetProperties();
            string insqry = "INSERT INTO " + DataTableName + " (";
            for (int i = 0; i < fieldcolumn.Length; i++)
            {
                if (i == fieldcolumn.Length - 1)
                    insqry += fieldcolumn[i].Name + ")";
                else
                    insqry += fieldcolumn[i].Name + ",";
            }
            insqry += "VALUES (";
            for (int i = 0; i < fieldcolumn.Length; i++)
            {
                if (i == fieldcolumn.Length - 1)
                    insqry += ":" + fieldcolumn[i].Name + ")";
                else
                    insqry += ":" + fieldcolumn[i].Name + ",";
            }

            DbConnection stringconn = context.Database.Connection;
            SqlConnection conn = new SqlConnection(stringconn.ConnectionString); conn.Open();
            SqlTransaction trans = conn.BeginTransaction();
            SqlDataAdapter ad = new SqlDataAdapter();
            ad.InsertCommand = new SqlCommand(insqry, conn);
            foreach (var item in datainsert)
            {
                ad.InsertCommand.Parameters.Clear();
                for (int i = 0; i < fieldcolumn.Length; i++)
                {
                    ad.InsertCommand.Parameters.Add(new SqlParameter(":" + fieldcolumn[i].Name, item.GetType().GetProperty(fieldcolumn[i].Name).GetValue(item, null)));
                }
                ad.InsertCommand.ExecuteNonQuery();

            }
            try
            {
                trans.Commit();
                conn.Close();
                return true;
            }
            catch
            {
                trans.Rollback();
                conn.Close();
                return false;
            }
        }
    }
}